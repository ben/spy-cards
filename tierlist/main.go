//go:generate go run .

package main

import (
	"context"
	"html"
	"os"
	"strconv"
	"strings"

	"git.lubar.me/ben/spy-cards/card"
)

// TODO: monitor progress of layout bug in Chrome https://crbug.com/1219772

var set = card.Set{
	Mode: &card.GameMode{
		Fields: []card.Field{
			&card.VanillaVersion{
				Version: card.BugFables111,
			},
		},
	},
	Cards: []*card.Def{
		attackerCard(1),
		attackerCard(2),
		attackerCard(3),
		attackerCard(4),
		attackerCard(5),
		attackerCard(6),
		attackerCard(7),
	},
}

func tierName(code string) string {
	if len(code) == 1 {
		return code + ". Tier"
	}

	if len(code) == 2 && code[1] == '-' {
		return code[:1] + ". Minus Tier"
	}

	if len(code) == 2 && code[1] == '+' {
		return code[:1] + ". Plus Tier"
	}

	if len(code) == 2 && code[0] == code[1] {
		return "Double " + code[:1] + ". Tier"
	}

	if len(code) == 3 && code[0] == code[1] && code[1] == code[2] {
		return "Triple " + code[:1] + ". Tier"
	}

	panic("unhandled tier code: " + code)
}

type AttackerBlurb struct {
	TP   []int
	Text string
}

type Tier struct {
	Code  string
	Label string
	Blurb string
	Cards []TierCard
}

type TierCard struct {
	Card   *card.Def
	Blurb  string
	Shared []*card.Def
}

var anchorifyReplacer = strings.NewReplacer(" ", "_", "'", "")

func anchorify(text string) string {
	return html.EscapeString(anchorifyReplacer.Replace(strings.ToLower(text)))
}

func (tc TierCard) CategoryName() string {
	switch {
	case tc.Card.Rank == card.Attacker:
		return "Attackers"
	case tc.Card.ID == card.LeafbugArcher, tc.Card.ID == card.LeafbugNinja, tc.Card.ID == card.LeafbugClubber:
		return "Leafbugs"
	case tc.Card.ID == card.DeadLanderγ, tc.Card.ID == card.DeadLanderα, tc.Card.ID == card.DeadLanderβ:
		return "Dead Landers"
	default:
		return ""
	}
}

func (tc TierCard) Anchor() string {
	if name := tc.CategoryName(); name != "" {
		return anchorify(name)
	}

	id := tc.Card.ID
	if tc.Blurb == "" && len(tc.Shared) == 1 {
		id = tc.Shared[0].ID
	}

	return anchorify(id.String())
}

type ExampleDeck struct {
	Name  string
	Deck  card.Deck
	Blurb string
}

func main() {
	ctx := context.Background()

	b := []byte(`<!DOCTYPE html>
<html lang="en" id="top-of-page">
<head>
<meta charset="utf-8">
<title>Spy Cards Online Strategy Guide</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="author" content="Miles">
<meta name="description" content="`)
	b = appendText(b, MetaDescription)
	b = append(b, `">
<link rel="manifest" href="/spy-cards.webmanifest">
<meta name="theme-color" content="#023">
<link rel="apple-touch-icon" href="/img/logo512.png">
<link rel="stylesheet" href="/style/layout.css?">
<link rel="stylesheet" href="/style/cards.css?">
<link rel="icon" href="/favicon.ico">
<meta name="color-scheme" content="dark light">
<link rel="preload" href="/fonts/bubblegumsans-regular-webfont.woff2" as="font" type="font/woff2" crossorigin="anonymous">
<link rel="preload" href="/fonts/d3streetism-webfont.woff2" as="font" type="font/woff2" crossorigin="anonymous">
</head>
<body>
<article class="readme">
<a href="/" rel="home" class="back-link">&larr; Back</a>
<h1>Strategy Guide</h1>
<nav class="table-of-contents">
<p>On this page:</p>
<ul>
<li><a href="#the-basics">The Basics</a></li>
<li><a href="#effects">Effects</a></li>
<li><a href="#tier-list">Card Tier List</a></li>
<li><a href="#example-decks">Example Decks</a></li>
</ul>
</nav>
<article id="the-basics">
<h2>The Basics</h2>
`...)
	b = appendBlurb(b, Basics)
	b = append(b, `</article>
</article>
<article id="effects" class="readme">
<a href="#top-of-page" class="back-link">&uarr; Back to Top</a>
<h2>Effects</h2>
`...)
	b = appendBlurb(b, EffectsBlurb)
	b = append(b, `<ul>
`...)

	for _, e := range Effects {
		b = append(b, `<li>
`...)
		b = appendBlurb(b, e)
		b = append(b, `</li>
`...)
	}

	b = append(b, `</ul>
`...)
	b = append(b, `</article>
<article id="tier-list" class="readme">
<a href="#top-of-page" class="back-link">&uarr; Back to Top</a>
<h2>The Cards</h2>
`...)
	b = appendBlurb(b, BeforeTierList)
	b = append(b, `<div class="tier-list">
`...)

	for _, tier := range Tiers {
		b = append(b, `<section class="tier tier-`...)
		b = appendText(b, tier.Code)
		b = append(b, `">
<h3><span class="tier-code" aria-label="`...)
		b = appendText(b, tierName(tier.Code))
		b = append(b, `">`...)
		b = appendText(b, tier.Code)
		b = append(b, `</span> (`...)
		b = appendText(b, tier.Label)
		b = append(b, `)</h3>
<div class="card-display wrap" role="group">`...)

		for _, c := range tier.Cards {
			b = append(b, `<a href="#`...)
			b = append(b, c.Anchor()...)
			b = append(b, `">`...)
			b = append(b, c.Card.ToHTML(ctx, &set)...)
			b = append(b, `</a>`...)
		}

		b = append(b, `</div>
</section>
`...)
	}

	b = append(b, `</div>
`...)
	b = appendBlurb(b, AfterTierList)

	b = append(b, `</article>
<article id="attackers" class="readme">
<a href="#tier-list" class="back-link">&uarr; Back to Tier List</a>
<h2>Attackers</h2>
`...)

	for _, a := range Attackers {
		if len(a.TP) != 0 {
			b = append(b, `<div class="card-display wrap x-small" aria-hidden="true">`...)

			for _, id := range card.VanillaOrder(card.Enemy) {
				c := set.Card(id)
				if c == nil || c.Rank != card.Attacker {
					continue
				}

				for _, tp := range a.TP {
					if c.TP == int64(tp) {
						b = append(b, c.ToHTML(ctx, &set)...)

						break
					}
				}
			}

			b = append(b, `</div>
`...)
		}

		b = appendBlurb(b, a.Text)
	}

	for _, tier := range Tiers {
		b = append(b, `</article>
<article id="`...)
		b = appendText(b, tier.Code)
		b = append(b, `_tier" class="readme sections tier-`...)
		b = appendText(b, tier.Code)
		b = append(b, `">
<a href="#tier-list" class="back-link">&uarr; Back to Tier List</a>
<h2 aria-label="`...)
		b = appendText(b, tierName(tier.Code))
		b = append(b, `">`...)
		b = appendText(b, tier.Code)
		b = append(b, ` Tier</h2>
`...)
		b = appendBlurb(b, tier.Blurb)

		b = append(b, `<div class="no-grid">
`...)

		for _, c := range tier.Cards {
			if c.Card.Rank == card.Attacker || (c.Blurb == "" && len(c.Shared) == 1) {
				continue
			}

			b = append(b, `<section id="`...)
			b = append(b, c.Anchor()...)
			b = append(b, `">
<h3>`...)

			switch {
			case c.Card.ID == card.LeafbugArcher && len(c.Shared) == 2 && c.Shared[0].ID == card.LeafbugNinja && c.Shared[1].ID == card.LeafbugClubber:
				b = append(b, `Leafbugs`...)
			case c.Card.ID == card.DeadLanderγ && len(c.Shared) == 2 && c.Shared[0].ID == card.DeadLanderα && c.Shared[1].ID == card.DeadLanderβ:
				b = append(b, `Dead Landers`...)
			default:
				b = appendText(b, c.Card.DisplayName())

				for _, s := range c.Shared {
					b = append(b, ` &amp; `...)
					b = appendText(b, s.DisplayName())
				}
			}

			b = append(b, `</h3>
<div class="card-display small">`...)
			b = append(b, c.Card.ToHTML(ctx, &set)...)

			for _, s := range c.Shared {
				b = append(b, s.ToHTML(ctx, &set)...)
			}

			b = append(b, `</div>
`...)
			b = appendBlurb(b, c.Blurb)
			b = append(b, `</section>
`...)
		}

		b = append(b, `</div>
`...)
	}

	b = append(b, `</article>
<article id="example-decks" class="readme">
<a href="#top-of-page" class="back-link">&uarr; Back to Top</a>
<h2>Example Decks</h2>
`...)
	appendBlurb(b, DeckBlurb)

	for i, d := range Decks {
		if i != 0 {
			b = append(b, `<hr>
`...)
		}

		b = append(b, `<h3 id="deck-`...)
		b = append(b, anchorify(d.Name)...)
		b = append(b, `">`...)
		b = appendText(b, d.Name)
		b = append(b, `</h3>
<a href="../game.html?deck=`...)

		deckCode, err := d.Deck.MarshalText()
		if err != nil {
			panic(err)
		}

		b = append(b, deckCode...)
		b = append(b, `"><div class="card-display small wrap">`...)

		for _, c := range d.Deck {
			b = append(b, set.Card(c).ToHTML(ctx, &set)...)
		}

		b = append(b, `</div></a>
`...)
		b = appendBlurb(b, d.Blurb)
	}

	b = append(b, `</article>
<!-- for service worker updater -->
<script src="/script/config.js?"></script>
<script src="/script/spy-cards.js?"></script>
<script defer src="https://static.cloudflareinsights.com/beacon.min.js?token=c4c7f809a6a04e51875bf72d5d396dc4" crossorigin></script>
</body>
</html>
`...)

	if err := os.WriteFile("../www/docs/strategy-guide.html", b, 0600); err != nil {
		panic(err)
	}
}

func vanilla(id card.ID, blurb string, shared ...card.ID) TierCard {
	sharedCards := make([]*card.Def, len(shared))

	for i, id := range shared {
		sharedCards[i] = set.Card(id)
	}

	return TierCard{
		Card:   set.Card(id),
		Blurb:  blurb,
		Shared: sharedCards,
	}
}

func attacker(tp int64) TierCard {
	return vanilla(128+card.ID(tp), "(this text should not be visible in the document - if it is, let Ben know)")
}

func attackerCard(tp int64) *card.Def {
	return &card.Def{
		ID:       128 + card.ID(tp),
		Name:     strconv.FormatInt(tp, 10) + "TP Attacker",
		Portrait: 246 + uint8(tp),
		Rank:     card.Attacker,
		TP:       tp,
		Tribes: []card.TribeDef{
			{
				Tribe: card.TribeUnknown,
			},
		},
		Effects: []*card.EffectDef{
			{
				Priority: 65,
				Type:     card.EffectStat,
				Amount:   tp,
			},
		},
	}
}

func appendText(b []byte, text string) []byte {
	return append(b, html.EscapeString(text)...)
}

func appendBlurb(b []byte, text string) []byte {
	if text == "" {
		return b
	}

	for _, p := range strings.Split(text, "\n\n") {
		b = append(b, "<p>\n"...)

		for i, line := range strings.Split(p, "\n") {
			if i != 0 {
				b = append(b, "<br>\n"...)
			}

			b = appendText(b, line)
		}

		b = append(b, "\n</p>\n"...)
	}

	return b
}
