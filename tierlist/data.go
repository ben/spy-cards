package main

import (
	"git.lubar.me/ben/spy-cards/card"
)

// Data for tier list page generator.
var (
	MetaDescription = `A comprehensive guide to Spy Cards Online by Miles. Features a tier list with explanations of why each card is as strong as it is.`

	Basics = `If you've played Spy Cards in-game before, you can likely skip most of this section. It'll go over the basic rules, which you probably already know.

Both players start the match with 2 TP, which they can spend on playing cards. Each turn, both players gain 1 extra TP to spend, and spare TP does not rollover between turns. From turn 9, you both hit the maximum of 10 TP per turn.

You each build a deck with one powerful Boss card, two unique and also strong Mini-boss cards, and 12 other cards, either Attackers or Effects. Attackers provide raw ATK power while Effects have unique abilities.

Each turn, you both draw two cards, or, if your hand is empty, three cards. You can have no more than five cards in hand at a time. In order to draw cards and cycle through your deck efficiently to find the cards you want, you should either try to end most turns with 2-3 cards (meaning you'll draw two and have 4-5 available next turn) or empty your entire hand (so you can draw three per turn, but have less cards available to work with each turn). It's very bad to end a turn with 4 or 5 cards since you'll only draw 1, or no cards next turn.

As for actually winning the game, each player starts with 5 HP. If your opponent wins a round by having more ATK than you, you lose 1 HP. If you have more ATK than them, you win the round and they lose 1 HP. Cards with DEF can reduce the ATK of the opponent, but can also be pierced by a couple of cards with pierce. If you both have the same ATK, you tie and no one loses HP. Reduce your opponent's HP to 0, and you win.`

	EffectsBlurb = `Effects, Mini-bosses, and Bosses can all have some common traits that provide specific effects, and it's useful to familiarise yourself with all of them.

Even if you've played the ingame Spy Cards, there may still be useful information here, particularly with regards to the often confusing Unity effect.`

	BeforeTierList = `Now onto the most important part: the cards! One of the most important aspects of any card game is building a good deck. For a quick reference of how good the cards are, you can consult this tier list:`

	AfterTierList = `Of course, a tier list doesn't tell the entire story, as you can't just pick every high-tier card and make a good deck. So this strategy guide will go in-depth on every card, except individual Attackers, to explain their placements. However, take it all with a grain of salt, as while this tier list roughly shows how good cards are, the Spy Cards metagame isn't set in stone.

If there are any particular cards you'd like to build a deck around, feel free to skip to those cards.

Alternatively, you can skip to the end of the document to see a list of example decks.`

	DeckBlurb = `A list of some example decks for various archetypes. Remember, these are example decks, so feel free to experiment and try different combinations of cards.

These decks were made before Delilah existed. Delilah is a fair substitute for Scarlet in most of them.`

	Effects = [...]string{
		`ATK/DEF/Pierce: ATK is simple, it gives you power and if your ATK is higher than the opponent, you win the round. DEF reduces the opponent's ATK, and Pierce reduces the opponent's DEF.`,
		`Coin: Cards with "Coin" will flip a coin, meaning the listed effect has a 50% chance of happening. Coin (X) means that X number of coins are flipped. Two cards (Bee-Boop and Dead Lander β) instead have the coinflip determine which of two effects they give.`,
		`Lifesteal/Heal: Heal X means that if the condition is met, you Heal X amount of HP. Lifesteal is the same, but only applies when you win the round. Healing effects are extremely powerful, and often game-swinging when successful. However, keep in mind that you cannot heal above 5 HP.`,
		`If/Vs. Card/Tribe: If Card effects trigger if a certain card or tribe is played alongside it. They are common among Mini-bosses, forming pairs that are weak individually, but strong together. Vs. Card effects trigger if a certain card or tribe is present on the enemy side of the field. Sometimes these effects may require a certain amount of a card to trigger, for example Leafbug Clubbers have an "If Leafbug (3)" effect, meaning you need to play 3 Leafbugs that round, including the Clubber itself.`,
		`If ATK: Only used by one card in vanilla Spy Cards, this triggers if your ATK is high enough that turn.`,
		`Setup: Also only used by one vanilla card, Setup will trigger an effect next turn, making you much stronger than usual on that turn.`,
		`Numb: Disables an attacker card on that turn, which removes all of the ATK it has. This is an extremely powerful and versatile effect. In-game, it targets an attacker based on the order that cards are played. Online however, it simply targets the lowest power attacker card of your opponent, so you don't have to worry about playing cards in a specific order.`,
		`Empower: Restricted to Bosses and Mini-bosses, Empower X will give X ATK to all cards of a specific tribe that you played that turn (or in some cases, it targets a specific card rather than a tribe). Empowered attacker cards can be numbed to lose their Empower boost (NOTE: This is different to ingame, where Empower boosts do not get numbed). Empower can affect attackers, effects, and the boss or mini-boss itself that has the Empower.`,
		`Unity: Unity can be extremely confusing to understand. Essentially, it's Empower, but only works for one copy of that card each turn. Therefore, if you play two of the same Leafbug, only one triggers the Unity effect.`,
	}

	Attackers = [...]AttackerBlurb{
		{
			Text: `Rather than going over attackers individually, I'll discuss them here, as they're the staple of most decks.`,
		},
		{
			TP:   []int{2, 3, 4},
			Text: `The best attacker cards are typically in the range of 2-4 TP. These ones are useful early-game for staying on the power curve, while also remaining good in the late-game where you can play a mix of three and usually hit around 8-10 ATK most turns. 2-costs are less useful than the others late game, but combo nicely with Empower bosses, as the more cards you can afford to play with the Empower bosses, the more power you can get from them.`,
		},
		{
			TP:   []int{1},
			Text: `1 and 5-costs are slightly more niche. 1-costs have the unique role of "blocking" numbs, by making your opponent's numbs be very low value. No one wants to numb a 1-cost, they want to numb at least a 2-cost. 1-costs are also amazing with Empowers.`,
		},
		{
			TP:   []int{5},
			Text: `5-costs are bad early-game but can come in very handy for staying on curve late-game. Just beware of numbs, and keep in mind that they are fairly bad with Empowers.`,
		},
		{
			TP:   []int{6},
			Text: `6-costs are incredibly niche, only two existing and both being functionally identical, as although one is a thug, you can't afford to play Astotheles alongside a 6-cost. The risk of playing them is quite high because of numbs, so they usually aren't used very often.`,
		},
		{
			TP:   []int{9},
			Text: `Lastly, Golden Seedling. A 9-cost might seem tempting, but don't assume it's automatically a good choice. It is countered by a single numb, making it very high-risk. However, if you're running a deck that already has 1-costs, it can be very worthwhile, making for an easy way to achieve 10 ATK with only 2 cards, while the 1-cost blocks a numb.`,
		},
		{
			Text: `Also, remember, if you don't need attackers of a specific tribe, such as for an empower synergy, you should pick attackers that aren't plants or bugs, just in case your opponent has Weevils or Devourer.`,
		},
	}

	Tiers = [...]Tier{
		{
			Code:  "SS",
			Label: "Ban-worthy",
			Blurb: `This tier contains cards of such immense power that people often agree not to use them. Cards that aren't fun to fight against and are significantly more powerful than all other options.`,
			Cards: []TierCard{
				vanilla(card.TheEverlastingKing, `The Everlasting King is overpowered. It's pretty obviously overpowered since it guarantees a win the round that it's played unless the opponent also plays ELK. What tips it even further over the edge however, is the fact that you can play it alongside a 1 TP lifesteal card, and gain a 2 HP advantage over your opponent by damaging them and healing yourself at the same time.

On top of that, it has mindgame potential. Your opponent doesn't want to throw away cards on the turn you play ELK, so they might just play nothing, giving you the chance to win with regular cards instead of ELK, and then use ELK the round after instead.`),
				vanilla(card.Delilah, `Whereas ELK's insane power is pretty obvious, Delilah's strength might not be so clear to everyone. What makes her an incredibly effective card is the fact that she is essentially two lifesteals and a 2-cost attacker, a total of 4 TP of value, for only 3 TP, along with being numb-immune and only being 1 card, instead of 3.

She has the same risk of playing a single lifesteal (having 1 ATK less than the standard curve) but with double the reward, healing 2 instead of 1.

Essentially, she's a powerful healing card that outclasses all other healing. Additionally, she can perform scary combos with ULTIMAX Tank or Riz.`),
			},
		},
		{
			Code:  "S",
			Label: "Overpowered",
			Blurb: `These are some of the best, most powerful cards in the game, and are the cards you should always be prepared to see and deal with. Many of them have little or no real weaknesses.`,
			Cards: []TierCard{
				vanilla(card.MotherChomper, `Mother Chomper is an incredibly powerful Empower boss, due to its unique advantage of having Lifesteal in addition to its Empower effect. When played alongside enough Chompers, it can easily win most turns and heal 2 in the process. Chomper decks suffer the weakness of not having any reliable 3-costs however, so you may want to patch that up with cards such as Scarlet or Delilah.

Depending on how well you draw, you can sometimes be nearly unkillable when combining Mother Chomper with some good healing minibosses. This puts Chomper decks solidly as one of the best decks in the game, potentially the best that people don't ban altogether.`),
				vanilla(card.Kali, `Here, we have the single best miniboss pair in the game. Individually, each of the two cards is bad. Kabbu's Pierce is situational, and Kali gives DEF equal to only half her TP cost. However, healing 3 is incredibly powerful, as even though you usually lose the turn that you play these two, you still have a net profit of 2 HP afterwards. They are perfect in decks that struggle early-game or just any deck that's defensive and wants to heal a lot. However, you should never play them individually unless you have no other choice to try and save yourself, and carrying a pair can lock you out of other useful minibosses too.`, card.Kabbu),
				vanilla(card.Kabbu, ``, card.Kali),
				vanilla(card.LeafbugArcher, `Leafbugs are top tiers for a variety of reasons. First and foremost, they are numb-immune cards that can make up the entirety of your deck's attack power. If you run a Leafbug deck, all of your opponent's numbing cards, which are good against every other deck in the entire game, become dead weights.

This isn't all they have going for them, though. Leafbugs also get above the power curve without the help of minibosses or bosses. Leafbug Ninja + Archer gets 6 ATK for 5 TP, and any mix of three Archers and Ninjas (as long as it's not three of the same Leafbug) gets 9 ATK for 7 or 8 TP. If you play the full trio of Leafbugs, one of each, you get 11 ATK and numb a card for only 9 TP, beating even some boss and miniboss combos.

Leafbugs are not without their flaws however, as they have a weak early-game and can be inconsistent. All three are substantially below the power curve when played alone, and the Clubber is almost worthless outside of the full trio combo. If you draw too many of one Leafbug and not enough of others, you'll be left with a weak hand. In general, expect to lose a lot of rounds in the early game and still have some weak turns in the late game. Leafbugs generally use healing support such as Kabbu Kali to stay alive, and can utilise lifesteal cards on their above curve turns to recover some HP.`, card.LeafbugNinja, card.LeafbugClubber),
				vanilla(card.LeafbugNinja, ``, card.LeafbugArcher),
				vanilla(card.LeafbugClubber, ``, card.LeafbugArcher),
				vanilla(card.MonsieurScarlet, `Monsieur Scarlet is a miniboss that most people immediately recognise as good. There's not much to say really, hitting 7 ATK is easy to do in the late-game, and he provides ATK equal to his cost, so he's essentially a free, practically unconditional heal in the late game. The vast majority of decks that don't run a miniboss pair will run Scarlet. Delilah can outclass Scarlet, but you do have the option of running both.`),
				vanilla(card.Astotheles, `Astotheles is essentially an Empower boss card… except a miniboss, which means he can be run alongside other powerful boss cards, ones that don't rely on empowering cards, such as Tidal Wyrm or ELK. In addition, thugs have a 2, 3, and 4-cost attacker, which are the generally best cost values for attacker cards, making Astotheles an incredibly solid card.`),
				vanilla(card.Riz, `Riz is 2 ATK below the curve on the turn you play him, but gives you 2 ATK next turn. Losing the turn to win the next turn is often very powerful, especially if combined with a Bud (or Delilah) on the second turn to offset the downside. Not to mention, you can always try to predict your opponent having a weak turn and use that to play Riz, or you can play Riz on a turn you know you can't win.

He can be a solid choice in a lot of decks, and only got better with the addition of Delilah. The combo of Riz, followed by Delilah on the next turn, is absolutely terrifying, due to the high reward of winning a Delilah turn.`),
			},
		},
		{
			Code:  "A",
			Label: "Strong",
			Blurb: `A Tier cards are solidly powerful, but not to the same extent as the S tiers, usually due to having bigger or more exploitable weaknesses.`,
			Cards: []TierCard{
				vanilla(card.Mothiva, `Zasp and Mothiva are both on the power curve alone, making them solid attacking cards. Then you couple that with the fact that together, they provide 7 ATK and a 1 HP heal for 5 TP, and you have a solid combo that can win the round while also restoring some HP.`, card.Zasp),
				vanilla(card.Zasp, ``, card.Mothiva),
				vanilla(card.Ironnail, `Ironnails are Numbnails, but with 1 DEF and 1 extra TP cost. 3 TP is a magic number, with 3 of them bringing you very close to the power curve in the lategame. As such, Ironnails are usually preferred over Numbnails, but some decks can prefer Numbnails instead.`),
				attacker(3),
				vanilla(card.VenusGuardian, `Venus' Guardian is like Mother Chomper, but more extreme in a way. It has potentially superior attack power when played with enough Buds, and can often FULL heal due to the huge amount of lifesteal from those Buds. On top of that, it has flexibility to run any attackers and effects it wants rather than being stuck running Chompers as the bulk of the deck.

However, Aria is a bit of a weak link, as you're often at the mercy of her coinflips to get high enough ATK to win a turn, and bad luck can cost you the entire game by tying or losing a round, and thus missing the lifesteal healing. Not to mention, outside of VeGu turns, Aria is bad. The full heals can definitely be worth it though.

Side Note: Venus' Guardian, Aria, and 3 Buds can achieve 21 ATK, making it the highest power combo in the game. Though, this does require Aria to get 3 heads.`, card.AcolyteAria),
				vanilla(card.AcolyteAria, ``, card.VenusGuardian),
				vanilla(card.VenusBud, `The two lifesteal cards are both pretty good, but can be tricky to use. You're sacrificing attack power to use them, but if you think your opponent has a weak turn or you have an especially strong combo to play, they can capitalise on it and be well worth the deck space they take up.`, card.Flowerling),
				vanilla(card.Flowerling, ``, card.VenusBud),
				vanilla(card.Numbnail, `Numbs are just really good in basically any deck. If it numbs a 2-cost, it's effectively on the power curve, and if it numbs something costing more than 2, it's above the curve. However, be careful using it against decks that run 1-costs, and don't run too many in one deck in case your opponent has Leafbugs. Ironnails are often preferred, but Numbnails are still solid, particularly in decks that lack a standard 2-cost like Mothflies.`),
				vanilla(card.TidalWyrm, `Tidal Wyrm is a discount ELK. It beats almost any round unless the opponent runs Leafbugs, and does so for a lower cost. But then again, it's somewhat weak to other bosses and minibosses, since they can't be numbed. Effect cards that provide stats can also potentially help deal with Tidal, but not a lot of those are actually good.`),
				attacker(2),
				attacker(4),
			},
		},
		{
			Code:  "B",
			Label: "Viable",
			Blurb: `B Tiers are good cards, but often held back by other cards potentially being stronger or more reliable options. You shouldn't overlook cards in the B Tier though, they can definitely prove useful in a well-built deck.`,
			Cards: []TierCard{
				vanilla(card.UltimaxTank, `Tank is a solid card, though ELK often outclasses it. If ELK is banned, then it has a lot more versatility. Simply being 1 above curve, it's simple but powerful and can fit into most decks that don't rely on a specific boss. It also combos very well with Scarlet, spending 10 TP for an easy 11 ATK and healing, or alternatively Delilah, for 10 ATK and a stronger heal.`),
				vanilla(card.DuneScorpion, `There are no 7-cost attackers, so this can fill an interesting niche, especially alongside Scarlet for a solid 10 TP healing turn. Not to mention, it's numb immune because it's a miniboss.`),
				vanilla(card.FalseMonarch, `False Monarch is limited to only boosting 1 and 3-costs, but has a unique advantage over other Empower bosses: It's the only one with both Empower 3 and a 1-cost to use. What does this mean? Well, to put it simply, False Monarch can achieve a whopping 19 ATK at most, for only 9 TP, overpowering almost every other combo in the game, besides ELK and a perfect luck VeGu combo.

Outside of boss turns however, it's rather weak, and tricky to use effectively due to the lack of 2 and 4-costs. Consider running a Golden Seedling to patch up the lategame, and Numbnails as 2-costs. Alternatively, you can even run a hybrid deck with Mothflies + Thugs. Having two separate empower bosses can be strong, but leaves you with little room for healing cards.`),
				attacker(1),
				vanilla(card.Devourer, `Leafbugs, Thugs, and almost every miniboss are Bugs. This means that unlike Weevil, Devourer is very likely to have at least some enemy card capable of triggering it. And when triggered, it's a cheap and powerful card. However, taking up your boss slot for a counter card can sometimes be tough to justify, as if your opponent doesn't play a bug the turn you play Devourer, it's one of the few bosses that can just end up being weaker than the TP you spent on it.

Still, it counters 2 of the top decks and is essentially a better Watcher when it does counter them. Even if you're not facing one of the decks that it counters, you can use it to try and counter miniboss turns. For example, it can tie against Zaspiva if you play it with a 1-cost.

Lastly, Devourer works well in Leafbug decks, as Leafbugs can both compensate for situations where Devourer fails to trigger, and Devourer can compensate for turns where you failed to draw a good Leafbug combo.`),
				vanilla(card.WildChomper, `Wild Chomper is either 1 ATK or 3 ATK. It's less consistent than simple 3-cost attackers, but can receive double the Empower from Mother Chomper, if you're lucky, while also filling a cost role that Chompers otherwise lack. It's not an amazing card, but it can still work decently well with an amazing boss.`),
				vanilla(card.TheWatcher, `Watcher is like a discount Tank. It costs 5, gives 6 ATK, like Tank costs 7, gives 8 ATK. The big difference is Watcher's attack power can be numbed, but it also leaves room to be played with a wider variety of cards, like a Leafbug Ninja + Archer combo to get 12 ATK for 10 TP. Plus, it can be played quite early. Unfortunately, it usually loses to Tidal Wyrm, which is a big downside compared to Tank.

It can also be compared to Devourer. Watcher is often good in a lot of the same situations as Devourer, but is more consistent at the cost of less reward. 6 ATK all the time for 5 TP, as opposed to Devourer being 3 or 6 ATK for 4 TP.`),
				vanilla(card.GoldenSeedling, ``),
				vanilla(card.SeedlingKing, `Seedling King's only real strength is that it can combo with Flowerlings, but VeGu does that better with Buds. Having 1-cost attackers helps as it can also achieve high attack power like Zommoth, but it lacks 2s, 4s, and 5s, and the coinflip Seedlings are mostly pretty bad cards that you don't want to rely on. That being said, it's still an Empower boss, and working with Flowerlings means it's at least got something to stand out from the others.`),
				vanilla(card.Zommoth, `Zommoth has 1, 3, 4, and 5-costs. Almost every worthwhile cost, except 2s. This is an advantage over every other Empower card except Astotheles, but it lacks utility. No lifesteal, no effect cards to boost, nothing else to stand out really. It can achieve high damage, but not as high as Monarch. It's kind of the most middle-of-the-road Empower boss around.`),
				attacker(5),
			},
		},
		{
			Code:  "C",
			Label: "Niche",
			Blurb: `C Tier cards are niche surprise picks that most people won't expect seeing. However, that may make them threatening if used well, because of the surprise factor.`,
			Cards: []TierCard{
				vanilla(card.HeavyDroneB33, `B-33 is mediocre among empower bosses. Heavy Drone has the unique advantage of being worth 1 TP less than its cost alone, where most Empower bosses are worth 2 TP less than their cost when played alone. On top of that, it has a variety of low-cost bots to empower. However what really holds it back is that it struggles somewhat from a lack of 4-costs, which holds it back and makes it hard to win turns in the lategame. This, coupled with not really having a useful niche since the nerf to Menders, means that other Empowers are usually better.`),
				vanilla(card.BeeBoop, `Bee-Boops lack the strength of being able to numb-block like a 1-cost attacker, but are still solid choices in a B-33 deck for helping to get big B-33 turns.`),
				vanilla(card.Weevil, `Weevils are very situational counter cards. They work well at countering the lifesteal cards, as well as Chomper and VeGu decks in general, but if your opponent has no plants, they're a dead weight. Think of them like Numbnails, except where Numbnails are good against every deck except Leafbugs, Weevils are good against only a specific few decks. They can be a powerful option if you correctly predict your opponent's deck choice, but they're very risky.`),
				vanilla(card.Spider, `Spider looks to be a pretty average card at first, but it has a few noteworthy strengths. For one, it summons a 1-cost attacker, which allows it to numb-block without putting rather mediocre 1-costs into your deck. For another thing, it's the cheapest boss, and with the Inichas it summons, can get you 5 value for 3 TP if you're lucky. This certainly has a surprise factor as players won't expect you to play a boss on say, turn 2 or 3, or even 4, but this early game factor is the only edge it really has over other bosses.`),
				vanilla(card.Cenn, `Cenn and Pisci lack the healing of other miniboss pairs, but make up for it by being 8 power and a numb for 6 TP. It's not terrible, but has a poor matchup against Leafbugs, only provides 2 ATK, the rest of the power being defence, and they're both bad cards without the other.`, card.Pisci),
				vanilla(card.Pisci, ``, card.Cenn),
				vanilla(card.Denmuki, `Denmuki is a less consistent Ironnail, with the advantage being it gives ATK instead of DEF, and can therefore win you some rounds without support. Effectively, when an Ironnail would typically tie, a Denmuki will either win you or lose you the round. If you have the means to reliably heal back up, it can be worth the risk.`),
				vanilla(card.Plumpling, `Plumpling, once upon a time, was utter garbage. 6 TP for a 50% chance of 6 DEF.

Now it's only 4 TP, which gives it actual potential value. While it will lose you the round half the time, the other half, it gets you 6 value for 4 TP.

Is it risky? Yes.

Is it worth the payoff when it works? Sometimes.

Really however, it should only be considered for Seedling decks, as it's a high-end card to help support a deck that otherwise mostly contains 1-3 costs, while receiving the empower boost from the boss card.`),
				vanilla(card.Acornling, `Acornling gets you 1 DEF above its cost if the coinflip is successful, but if unsuccessful, you wasted 2 TP. It's basically low-end Plumpling, and like Plumpling, it can at least find some use in Seedling King decks. The Empower from Seedling King can ensure it's not a complete dead weight, and it fills in the role of a 2-cost, which Seedling King sorely lacks.`),
				attacker(6),
				vanilla(card.Stratos, `Stratos is much, much less useful than Delilah… but not worthless. Granted, pierce is situational, only being particularly useful against Ironnails and Seedling decks, but playing Stratos + Delilah together isn't actually a terrible combo. You get 8 value for 9 TP, with lifesteal 2, and all that value is numb-immune.

Delilah usually prefers Riz, or to be paired up with another healing miniboss like Scarlet. Running the pair isn't necessarily bad, just not as good as a lot of other options.`),
			},
		},
		{
			Code:  "D",
			Label: "Bad",
			Blurb: `D Tier cards are just bad. No matter how hard you try, they're the cards that aren't going to work in a serious deck. It's just not worth it really.`,
			Cards: []TierCard{
				vanilla(card.Broodmother, `Empowering only a single card rather than a tribe is not a good start. Couple that with having the same base ATK power as any other 4-cost Empower 2, and the card it empowers only being a 1-cost, and you have a recipe for disaster. The sad part? This isn't even the worst Empower boss in the game.`),
				vanilla(card.Inichas, `Coinflip defence cards are usually pretty poor, but Inichas runs into an additional problem. It's a 1-cost. 1-costs are rarely good unless they can numb-block, which effect cards can't do. The only reason 1-cost lifesteal cards are so good is because healing is very powerful. And on top of everything, it's a coinflip defence card with no Seedling King synergy. There's no reason to run it.`),
				vanilla(card.PeacockSpider, `Peacock Spider is the worst of the Empower bosses, as the cards it buffs are only 3 or 5-costs, and it costs 5 itself. This means that a single card will be the only one it can empower, and that leaves it very vulnerable to numbs. You can mitigate this by carrying 2-costs of another tribe to block the numbs, but it's still poor and lacks any special niche that other Empower bosses have.`),
				vanilla(card.GeneralUltimax, `Ultimax is essentially a much weaker version of Astotheles. Even with 2 other Wasps, he only gets 1 ATK above the power curve, where Astotheles would get 1 ATK above with a single Thug, and 4 ATK above with two. And on top of that, Wasps aren't exactly a good tribe, as you'll see further down this list.`),
				vanilla(card.TheBeast, `The Beast is really bad alone, requiring Kabbu to be useful. And even then… it's just 8 ATK for 7 TP with some pierce. Ultimax Tank is usually going to be better and is a single card, while also not requiring a miniboss slot. Kabbu is also so bad alone, that the only way to justify bringing it is to also bring Kali, but unfortunately, you can't play Beast, Kabbu, and Kali all in one turn, which would be a rather powerful combo.`),
				vanilla(card.DeadLanderγ, `Dead Landers fill the role of 4 or 6-costs that can't be numbed and… that's just not good compared to other minibosses, especially as they all use some amount of defence, which means they're vulnerable to pierce. Yes, pierce is rare, but it's still technically a downside to already mediocre cards.`, card.DeadLanderα, card.DeadLanderβ),
				vanilla(card.DeadLanderα, ``, card.DeadLanderγ),
				vanilla(card.DeadLanderβ, ``, card.DeadLanderγ),
				vanilla(card.Yin, `Maki has no special, powerful, boss-like qualities alone.

The only way to make Maki do anything of value is to play him with Kina or Yin. The combo with Yin is not entirely trash, simply because of the value of healing. That being said, it's a 9 TP, probably lose the turn combo for heal 2, that uses your boss slot. Massively outclassed by Kabbu Kali which offer a heal 3 for lower cost without using your boss slot.

As for Kina… well, Kina's even worse, but that's for later.`, card.Maki),
				vanilla(card.Maki, ``, card.Yin),
				vanilla(card.Cactiling, `Literally a worse version of Acornling, and it doesn't even have a TP cost that's unique among Seedlings like Acornling does, since there's already a 3-cost attacker Seedling.`),
			},
		},
		{
			Code:  "F",
			Label: "Unusable",
			Blurb: `D Tier cards are bad, F Tier cards are just worthless. The cons of F Tiers outweigh the pros so heavily that running absolutely any of them is actively harming your deck.`,
			Cards: []TierCard{
				vanilla(card.Cross, `Cross and Poi are just a worse version of Cenn and Pisci, since Cenn and Pisci have the same power together, and numb something too.`, card.Poi),
				vanilla(card.Poi, ``, card.Cross),
				vanilla(card.Kina, `Maki + Yin was barely usable. But Maki + Kina cost 10 TP for 11 ATK, way worse than most boss combos, especially ones that use a miniboss. Kina is worthless without Maki, and you can't play the full Team Maki trio in one turn. If for whatever reason you do want to use Maki, only run Maki + Yin, and use the other miniboss slot for something better. Kina is not worth the slot.`),
				vanilla(card.WaspBomber, `Wasp Bomber is just only worth its price if you get both coinflips. It's almost always bad for its price.`),
				vanilla(card.Madesphy, `This… poor thing. Like Wasp Bomber, it needs both coinflips to be good. But at least Bomber provides ATK, and a Numb, and combos with Wasps (although Wasps are bad). Madesphy lacks all of those slightly redeeming qualities.`),
				vanilla(card.Carmina, `Most minibosses aren't worth 4 TP, especially since many require specific synergies or pairs to work well.`),
				vanilla(card.WaspDriller, `If your opponent has at least 2 DEF, then this is worth its cost. Otherwise, it's not. Most defensive cards aren't very good, so most of the time, it's not worth the cost. On the plus side, it can beat Tidal Wyrm, but it's not exactly worth running this terrible card for that one situation.`),
				vanilla(card.Ahoneynation, `You need both coinflips to come up heads to get good value from this. And it's a miniboss. A miniboss should not have this kind of ridiculous luck attached to it.`),
				vanilla(card.WaspKing, `Wasp King might not seem bad, but he's the worst boss in the game, and the reason is he suffers from the effect Wasps being among the worst cards in the game.

The average real value of the cards he summons is 6.75, despite the fact that he costs 7 TP. Now, how does this change if you play Ultimax alongside the Wasp King? 10.75 average value for 10 TP. At least it's above the curve, barely, but the Maki + Kina combo is more consistent and valuable, and that combo is already terrible.`),
				vanilla(card.PrimalWeevil, `This card is so horribly overpriced that it's not worth it. Play it with a single Weevil and you get either 6 or 8 ATK for 8 TP.  Yep, even if the Weevil gets a bonus from the opponent playing a plant, Primal Weevil is so expensive that it won't matter. You have to play two Weevils with Primal while your opponent plays a plant to get any remotely good value out of it. Primal Weevil is only good if the stars and planets align on a blue moon.`),
			},
		},
		{
			Code:  "F-",
			Label: "The Fallen",
			Blurb: `Oh, how the mighty have fallen.`,
			Cards: []TierCard{
				vanilla(card.Mender, `Menders, once upon a time, were extremely overpowered. You could play 5 of them, and heal to full. Then they got nerfed, a little too much. It's impossible to play 7 bots in a single turn, therefore the nerfed Menders can never trigger their heal. They are essentially worthless now.`),
			},
		},
	}

	Decks = [...]ExampleDeck{
		{
			Name: "Venus' Guardian",
			Deck: card.Deck{
				card.VenusGuardian,
				card.AcolyteAria,
				card.MonsieurScarlet,
				card.VenusBud,
				card.VenusBud,
				card.VenusBud,
				card.SecurityTurret,
				card.SecurityTurret,
				card.SecurityTurret,
				card.Warden,
				card.Warden,
				card.Abomihoney,
				card.Abomihoney,
				card.Numbnail,
				card.Numbnail,
			},
		},
		{
			Name: "Leafbugs",
			Deck: card.Deck{
				card.TidalWyrm,
				card.Kabbu,
				card.Kali,
				card.LeafbugArcher,
				card.LeafbugArcher,
				card.LeafbugArcher,
				card.LeafbugArcher,
				card.LeafbugNinja,
				card.LeafbugNinja,
				card.LeafbugNinja,
				card.LeafbugClubber,
				card.LeafbugClubber,
				card.VenusBud,
				card.Numbnail,
				card.Numbnail,
			},
			Blurb: `ELK is usually preferred, but if ELK is banned, Tidal is a good substitute.`,
		},
		{
			Name: "Mother Chomper",
			Deck: card.Deck{
				card.MotherChomper,
				card.Riz,
				card.MonsieurScarlet,
				card.Chomper,
				card.Chomper,
				card.Chomper,
				card.Chomper,
				card.ChomperBrute,
				card.ChomperBrute,
				card.ChomperBrute,
				card.ChomperBrute,
				card.Ironnail,
				card.Ironnail,
				card.Ironnail,
				card.VenusBud,
			},
		},
		{
			Name: "Thugs",
			Deck: card.Deck{
				card.UltimaxTank,
				card.Astotheles,
				card.MonsieurScarlet,
				card.Thief,
				card.Thief,
				card.Thief,
				card.Bandit,
				card.Bandit,
				card.Bandit,
				card.Burglar,
				card.Burglar,
				card.Burglar,
				card.Numbnail,
				card.Numbnail,
				card.VenusBud,
			},
			Blurb: `As with Leafbugs, ELK is preferred.`,
		},
		{
			Name: "Zommoth",
			Deck: card.Deck{
				card.Zommoth,
				card.Mothiva,
				card.Zasp,
				card.Zombee,
				card.Zombee,
				card.Zombee,
				card.Bloatshroom,
				card.Bloatshroom,
				card.Bloatshroom,
				card.Zombiant,
				card.Jellyshroom,
				card.Zombeetle,
				card.Numbnail,
				card.Numbnail,
				card.VenusBud,
			},
		},
		{
			Name: "Venus Thug Hybrid",
			Deck: card.Deck{
				card.VenusGuardian,
				card.Astotheles,
				card.AcolyteAria,
				card.Thief,
				card.Thief,
				card.Thief,
				card.Bandit,
				card.Bandit,
				card.Bandit,
				card.Burglar,
				card.Burglar,
				card.VenusBud,
				card.VenusBud,
				card.VenusBud,
				card.Numbnail,
			},
		},
		{
			Name: "Mothfly Thug Hybrid",
			Deck: card.Deck{
				card.FalseMonarch,
				card.Astotheles,
				card.MonsieurScarlet,
				card.Mothfly,
				card.Mothfly,
				card.Mothfly,
				card.Thief,
				card.Thief,
				card.MothflyCluster,
				card.MothflyCluster,
				card.Bandit,
				card.Bandit,
				card.Burglar,
				card.Burglar,
				card.VenusBud,
			},
		},
		{
			Name: "Chomper Thug Hybrid",
			Deck: card.Deck{
				card.MotherChomper,
				card.Astotheles,
				card.MonsieurScarlet,
				card.Chomper,
				card.Chomper,
				card.Chomper,
				card.Thief,
				card.Thief,
				card.Thief,
				card.Bandit,
				card.Bandit,
				card.Bandit,
				card.ChomperBrute,
				card.ChomperBrute,
				card.ChomperBrute,
			},
		},
	}
)
