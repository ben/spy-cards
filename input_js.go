//go:build !headless && js && wasm
// +build !headless,js,wasm

package main

import (
	"log"
	"strings"
	"syscall/js"

	"git.lubar.me/ben/spy-cards/arcade/touchcontroller"
	"git.lubar.me/ben/spy-cards/audio"
	"git.lubar.me/ben/spy-cards/gfx"
	"git.lubar.me/ben/spy-cards/input"
	"git.lubar.me/ben/spy-cards/internal"
	"git.lubar.me/ben/spy-cards/spoilerguard"
	"golang.org/x/mobile/event/key"
	"golang.org/x/mobile/event/mouse"
	"golang.org/x/mobile/event/size"
	"golang.org/x/mobile/event/touch"
	"golang.org/x/mobile/geom"
	"golang.org/x/mobile/gl"
)

var defaultKeyButton = [input.NumButtons]string{
	input.BtnUp:      "ArrowUp",
	input.BtnDown:    "ArrowDown",
	input.BtnLeft:    "ArrowLeft",
	input.BtnRight:   "ArrowRight",
	input.BtnConfirm: "KeyC",
	input.BtnCancel:  "KeyX",
	input.BtnSwitch:  "KeyZ",
	input.BtnToggle:  "KeyV",
	input.BtnPause:   "Escape",
	input.BtnHelp:    "Enter",
}

func registerInputListeners(canvas js.Value) {
	onResize(js.Global(), nil)
	js.Global().Call("addEventListener", "keydown", js.FuncOf(onKeyDown))
	js.Global().Call("addEventListener", "keyup", js.FuncOf(onKeyUp))
	js.Global().Call("addEventListener", "resize", js.FuncOf(onResize))
	js.Global().Call("addEventListener", "mouseup", js.FuncOf(onMouseUp))
	canvas.Call("addEventListener", "touchstart", js.FuncOf(onTouchStart), map[string]interface{}{
		"passive": false,
	})
	canvas.Call("addEventListener", "touchend", js.FuncOf(onTouchEnd))
	canvas.Call("addEventListener", "touchcancel", js.FuncOf(onTouchCancel))
	canvas.Call("addEventListener", "touchmove", js.FuncOf(onTouchMove), map[string]interface{}{
		"passive": false,
	})
	canvas.Call("addEventListener", "contextmenu", js.FuncOf(onContextMenu))
	canvas.Call("addEventListener", "mousedown", js.FuncOf(onMouseDown))
	canvas.Call("addEventListener", "mousemove", js.FuncOf(onMouseMove))
	canvas.Call("addEventListener", "wheel", js.FuncOf(onWheel), map[string]interface{}{
		"passive": false,
	})
}

func isAllowedKeyCode(code string) bool {
	// don't grab OS/Browser special keys
	if strings.HasPrefix(code, "Alt") ||
		strings.HasPrefix(code, "Control") ||
		strings.HasPrefix(code, "Shift") ||
		strings.HasPrefix(code, "Meta") ||
		strings.HasPrefix(code, "OS") ||
		strings.HasPrefix(code, "Browser") ||
		strings.HasPrefix(code, "Launch") ||
		strings.HasPrefix(code, "Audio") ||
		strings.HasPrefix(code, "Media") ||
		strings.HasPrefix(code, "Volume") ||
		strings.HasPrefix(code, "Lang") ||
		strings.HasPrefix(code, "Page") ||
		strings.HasSuffix(code, "Mode") ||
		strings.HasSuffix(code, "Lock") ||
		isFunctionKeyCode(code) ||
		code == "" ||
		code == "Unidentified" ||
		code == "PrintScreen" ||
		code == "Power" ||
		code == "Pause" ||
		code == "ContextMenu" ||
		code == "Help" ||
		code == "Fn" ||
		code == "Tab" ||
		code == "Home" ||
		code == "End" {
		return false
	}

	// allow letters, numbers, and some symbols.
	if (len(code) == 4 && code[:3] == "Key" && code[3] >= 'A' && code[3] <= 'Z') ||
		(len(code) == 6 && code[:5] == "Digit" && code[5] >= '0' && code[5] <= '9') ||
		strings.HasPrefix(code, "Numpad") ||
		strings.HasPrefix(code, "Intl") ||
		strings.HasSuffix(code, "Up") ||
		strings.HasSuffix(code, "Down") ||
		strings.HasSuffix(code, "Left") ||
		strings.HasSuffix(code, "Right") {
		return true
	}

	// additional symbols and special keys listed by name:
	switch code {
	case "Escape",
		"Minus",
		"Equal",
		"Backspace",
		"Enter",
		"Semicolon",
		"Quote",
		"Backquote",
		"Backslash",
		"Comma",
		"Period",
		"Slash",
		"Space",
		"Insert",
		"Delete":
		return true
	default:
		log.Printf("DEBUG: unhandled keycode %q; ignoring for safety.", code)

		return false
	}
}

func isFunctionKeyCode(s string) bool {
	if len(s) < 2 {
		return false
	}

	if s[0] != 'F' {
		return false
	}

	for i := 1; i < len(s); i++ {
		if s[i] < '0' || s[i] > '9' {
			return false
		}
	}

	return true
}

func keyEvent(e js.Value, down bool) {
	if e.Get("ctrlKey").Truthy() || e.Get("altKey").Truthy() || e.Get("metaKey").Truthy() {
		return
	}

	if !e.Get("target").Get("value").IsUndefined() {
		return
	}

	if code := e.Get("code").String(); isAllowedKeyCode(code) {
		e.Call("preventDefault")

		dir := key.DirPress

		if !down {
			dir = key.DirRelease
		} else if keyHeld[code] {
			dir = key.DirNone
		}

		var c key.Code

		switch code {
		case "KeyA":
			c = key.CodeA
		case "KeyB":
			c = key.CodeB
		case "KeyC":
			c = key.CodeC
		case "KeyD":
			c = key.CodeD
		case "KeyE":
			c = key.CodeE
		case "KeyF":
			c = key.CodeF
		case "KeyG":
			c = key.CodeG
		case "KeyH":
			c = key.CodeH
		case "KeyI":
			c = key.CodeI
		case "KeyJ":
			c = key.CodeJ
		case "KeyK":
			c = key.CodeK
		case "KeyL":
			c = key.CodeL
		case "KeyM":
			c = key.CodeM
		case "KeyN":
			c = key.CodeN
		case "KeyO":
			c = key.CodeO
		case "KeyP":
			c = key.CodeP
		case "KeyQ":
			c = key.CodeQ
		case "KeyR":
			c = key.CodeR
		case "KeyS":
			c = key.CodeS
		case "KeyT":
			c = key.CodeT
		case "KeyU":
			c = key.CodeU
		case "KeyV":
			c = key.CodeV
		case "KeyW":
			c = key.CodeW
		case "KeyX":
			c = key.CodeX
		case "KeyY":
			c = key.CodeY
		case "KeyZ":
			c = key.CodeZ

		case "Digit0":
			c = key.Code0
		case "Digit1":
			c = key.Code1
		case "Digit2":
			c = key.Code2
		case "Digit3":
			c = key.Code3
		case "Digit4":
			c = key.Code4
		case "Digit5":
			c = key.Code5
		case "Digit6":
			c = key.Code6
		case "Digit7":
			c = key.Code7
		case "Digit8":
			c = key.Code8
		case "Digit9":
			c = key.Code9

		case "Enter":
			c = key.CodeReturnEnter
		case "Escape":
			c = key.CodeEscape
		case "Backspace":
			c = key.CodeDeleteBackspace
		case "Tab":
			c = key.CodeTab
		case "Space":
			c = key.CodeSpacebar
		case "Minus":
			c = key.CodeHyphenMinus
		case "Equal":
			c = key.CodeEqualSign
		case "BracketLeft":
			c = key.CodeLeftSquareBracket
		case "BracketRight":
			c = key.CodeRightSquareBracket
		case "Backslash":
			c = key.CodeBackslash
		case "Semicolon":
			c = key.CodeSemicolon
		case "Quote":
			c = key.CodeApostrophe
		case "Backquote":
			c = key.CodeGraveAccent
		case "Comma":
			c = key.CodeComma
		case "Period":
			c = key.CodeFullStop
		case "Slash":
			c = key.CodeSlash
		case "Delete":
			c = key.CodeDeleteForward

		case "ArrowRight":
			c = key.CodeRightArrow
		case "ArrowLeft":
			c = key.CodeLeftArrow
		case "ArrowDown":
			c = key.CodeDownArrow
		case "ArrowUp":
			c = key.CodeUpArrow

		case "NumpadDivide":
			c = key.CodeKeypadSlash
		case "NumpadMultiply":
			c = key.CodeKeypadAsterisk
		case "NumpadSubtract":
			c = key.CodeKeypadHyphenMinus
		case "NumpadAdd":
			c = key.CodeKeypadPlusSign
		case "NumpadEnter":
			c = key.CodeKeypadEnter
		case "Numpad0":
			c = key.CodeKeypad0
		case "Numpad1":
			c = key.CodeKeypad1
		case "Numpad2":
			c = key.CodeKeypad2
		case "Numpad3":
			c = key.CodeKeypad3
		case "Numpad4":
			c = key.CodeKeypad4
		case "Numpad5":
			c = key.CodeKeypad5
		case "Numpad6":
			c = key.CodeKeypad6
		case "Numpad7":
			c = key.CodeKeypad7
		case "Numpad8":
			c = key.CodeKeypad8
		case "Numpad9":
			c = key.CodeKeypad9
		case "NumpadDecimal":
			c = key.CodeKeypadFullStop
		case "NumpadEqual":
			c = key.CodeKeypadEqualSign
		}

		var r rune = -1

		if keyName := []rune(e.Get("key").String()); len(keyName) == 1 {
			r = keyName[0]
		}

		ke := key.Event{
			Code:      c,
			Rune:      r,
			Direction: dir,
		}

		inputContext.OnKey(ke)
		spoilerguard.OnKey(ke)

		keyHeld[code] = down
	}
}

func onKeyDown(_ js.Value, args []js.Value) interface{} {
	keyEvent(args[0], true)

	return js.Undefined()
}

func onKeyUp(_ js.Value, args []js.Value) interface{} {
	keyEvent(args[0], false)

	return js.Undefined()
}

func onResize(_ js.Value, _ []js.Value) interface{} {
	width := js.Global().Get("innerWidth").Float()
	height := js.Global().Get("innerHeight").Float()

	lastDPR = 1.0
	if dprJS := js.Global().Get("devicePixelRatio"); dprJS.Truthy() {
		lastDPR = dprJS.Float()
	}

	if s := internal.LoadSettings(); s.DPIScale > 0 {
		lastDPR *= s.DPIScale
	}

	sz := size.Event{
		WidthPx:     int(lastDPR * width),
		HeightPx:    int(lastDPR * height),
		WidthPt:     geom.Pt(width),
		HeightPt:    geom.Pt(height),
		PixelsPerPt: float32(lastDPR),
		Orientation: size.OrientationUnknown,
	}

	canvas := gfx.GL.(gl.JSWrapper).JSValue().Get("canvas")

	cw := canvas.Get("width").Int()
	ch := canvas.Get("height").Int()

	if cw != sz.WidthPx || ch != sz.HeightPx {
		canvas.Set("width", sz.WidthPx)
		canvas.Set("height", sz.HeightPx)
	}

	gfx.SetSize(sz)
	touchcontroller.Size(sz)
	inputContext.OnSize(sz)

	return js.Undefined()
}

var touches = make(map[int]touch.Sequence)

func touchEvent(e js.Value, ty touch.Type) {
	e.Call("preventDefault")

	audio.UnsuspendContext()

	changed := e.Get("changedTouches")
	for i := 0; i < changed.Length(); i++ {
		t := changed.Index(i)

		id := t.Get("identifier").Int()

		sequence, ok := touches[id]
		if !ok {
			sequence = touch.Sequence(len(touches))
			touches[id] = sequence
		}

		te := touch.Event{
			X:        float32(t.Get("clientX").Float() * lastDPR),
			Y:        float32(t.Get("clientY").Float() * lastDPR),
			Sequence: sequence,
			Type:     ty,
		}

		touchcontroller.Touch(te)
		inputContext.OnTouch(te)
	}
}

func onTouchStart(_ js.Value, args []js.Value) interface{} {
	e := args[0]

	touchEvent(e, touch.TypeBegin)

	return js.Undefined()
}

func onTouchEnd(_ js.Value, args []js.Value) interface{} {
	e := args[0]

	touchEvent(e, touch.TypeEnd)

	return js.Undefined()
}

func onTouchCancel(_ js.Value, args []js.Value) interface{} {
	e := args[0]

	touchEvent(e, touch.TypeEnd)

	return js.Undefined()
}

func onTouchMove(_ js.Value, args []js.Value) interface{} {
	e := args[0]

	touchEvent(e, touch.TypeMove)

	return js.Undefined()
}

func newMouseEvent(e js.Value, dir mouse.Direction) mouse.Event {
	var mod key.Modifiers

	if e.Get("altKey").Truthy() {
		mod |= key.ModAlt
	}

	if e.Get("ctrlKey").Truthy() {
		mod |= key.ModControl
	}

	if e.Get("metaKey").Truthy() {
		mod |= key.ModMeta
	}

	if e.Get("shiftKey").Truthy() {
		mod |= key.ModShift
	}

	return mouse.Event{
		X: float32(e.Get("clientX").Float() * lastDPR),
		Y: float32(e.Get("clientY").Float() * lastDPR),

		Direction: dir,
		Modifiers: mod,
	}
}

func mouseEvent(e js.Value, dir mouse.Direction, button bool) {
	me := newMouseEvent(e, dir)

	if button {
		me.Button = mouse.Button(e.Get("button").Int() + 1)
	}

	inputContext.OnMouse(me)
}

func onMouseDown(_ js.Value, args []js.Value) interface{} {
	e := args[0]

	mouseEvent(e, mouse.DirPress, true)

	return js.Undefined()
}

func onMouseUp(_ js.Value, args []js.Value) interface{} {
	e := args[0]

	mouseEvent(e, mouse.DirRelease, true)

	return js.Undefined()
}

func onMouseMove(_ js.Value, args []js.Value) interface{} {
	e := args[0]

	mouseEvent(e, mouse.DirNone, false)

	return js.Undefined()
}

func onContextMenu(_ js.Value, args []js.Value) interface{} {
	e := args[0]

	// prevent long press from causing context menus to appear
	e.Call("preventDefault")

	return js.Undefined()
}

func onWheel(_ js.Value, args []js.Value) interface{} {
	e := args[0]

	me := newMouseEvent(e, mouse.DirStep)

	dx := e.Get("deltaX").Float()
	dy := e.Get("deltaY").Float()

	switch {
	case dy < 0:
		me.Button = mouse.ButtonWheelUp
	case dy > 0:
		me.Button = mouse.ButtonWheelDown
	case dx < 0:
		me.Button = mouse.ButtonWheelLeft
	case dx > 0:
		me.Button = mouse.ButtonWheelRight
	default:
		return js.Undefined()
	}

	inputContext.OnMouse(me)

	return js.Undefined()
}
