//go:build ((linux && !android) || windows || (darwin && !ios)) && !headless
// +build linux,!android windows darwin,!ios
// +build !headless

package main

import (
	"context"
	"log"
	"time"

	"git.lubar.me/ben/spy-cards/arcade/touchcontroller"
	"git.lubar.me/ben/spy-cards/gfx"
	"git.lubar.me/ben/spy-cards/input"
	"git.lubar.me/ben/spy-cards/spoilerguard"
	"golang.org/x/exp/shiny/driver/gldriver"
	"golang.org/x/exp/shiny/screen"
	"golang.org/x/mobile/event/key"
	"golang.org/x/mobile/event/lifecycle"
	"golang.org/x/mobile/event/mouse"
	"golang.org/x/mobile/event/paint"
	"golang.org/x/mobile/event/size"
	"golang.org/x/mobile/event/touch"
	"golang.org/x/mobile/exp/audio/al"
	"golang.org/x/mobile/gl"
)

const startPath = "/game/home/vanilla"

func appMain(ictx *input.Context) {
	log.SetFlags(log.LstdFlags | log.Lshortfile)

	var (
		startedGLDriver bool
		isVisible       bool

		frameSyncFinished = make(chan struct{})
	)

	gldriver.Main(func(s screen.Screen) {
		if startedGLDriver {
			log.Println("WARNING: gldriver.Main callback called multiple times")

			return
		}

		startedGLDriver = true

		defer al.CloseDevice()

		w, err := s.NewWindow(&screen.NewWindowOptions{
			Title: "Spy Cards Online",
		})
		if err != nil {
			log.Fatalln("could not create window:", err)
		}
		defer w.Release()

		for {
			switch e := w.NextEvent().(type) {
			case lifecycle.Event:
				if e.To == lifecycle.StageDead {
					return
				}

				switch e.Crosses(lifecycle.StageVisible) {
				case lifecycle.CrossOn:
					gfx.Init(e.DrawContext.(gl.Context))

					isVisible = true

					w.SendFirst(paint.Event{})
				case lifecycle.CrossOff:
					isVisible = false

					gfx.Release()
				}

			case size.Event:
				gfx.SetSize(e)
				touchcontroller.Size(e)
				ictx.OnSize(e)

			case touch.Event:
				touchcontroller.Touch(e)
				ictx.OnTouch(e)

			case mouse.Event:
				ictx.OnMouse(e)

			case key.Event:
				if e.Modifiers&(key.ModControl|key.ModAlt|key.ModMeta) != 0 {
					continue
				}

				ictx.OnKey(e)
				spoilerguard.OnKey(e)

				switch e.Direction {
				case key.DirPress:
					keyHeld[e.Code.String()] = true
				case key.DirRelease:
					delete(keyHeld, e.Code.String())
				}

				lastKeyboard = time.Now()

			case paint.Event:
				if e.External || !isVisible {
					continue
				}

				gfx.FrameSync <- frameSyncFinished
				<-frameSyncFinished
				touchcontroller.Render()
				gfx.Lock.Lock()
				w.Publish()
				gfx.Lock.Unlock()
				w.Send(paint.Event{})
			}
		}
	})
}

func doPreload(ctx context.Context, s preloadSet) <-chan struct{} {
	return s.do(ctx, false)
}

func doRedirect(u string) {
	panic("TODO: doRedirect")
}

func pushURL(u string)        {}
func onPopURL(f func(string)) {}
