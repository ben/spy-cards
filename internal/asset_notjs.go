//go:build !js || !wasm
// +build !js !wasm

package internal

import (
	"context"
	"fmt"
	"io"
	"net/http"
	"strings"

	"golang.org/x/mobile/asset"
)

// OpenAsset opens a local asset or performs an HTTP request.
func OpenAsset(name string) (io.ReadCloser, error) {
	if strings.Contains(name, ":") {
		req, err := http.NewRequestWithContext(context.Background(), http.MethodGet, name, nil)
		if err != nil {
			return nil, fmt.Errorf("loading asset %q: %w", name, err)
		}

		req = ModifyRequest(req)

		resp, err := http.DefaultClient.Do(req)
		if err != nil {
			return nil, fmt.Errorf("loading asset %q: %w", name, err)
		}

		return resp.Body, nil
	}

	f, err := asset.Open(name)
	if err != nil {
		return nil, fmt.Errorf("loading asset %q: %w", name, err)
	}

	return f, nil
}
