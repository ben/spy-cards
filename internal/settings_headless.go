//go:build headless
// +build headless

package internal

func PrefersReducedMotion() bool {
	return false
}

func LikelyTouch() bool {
	return false
}
