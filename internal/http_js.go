//go:build js && wasm
// +build js,wasm

package internal

import (
	"context"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"reflect"
	"strconv"
	"strings"
	"syscall/js"
)

// FetchOpts is the options dictionary passed to fetch.
var FetchOpts = js.ValueOf(map[string]interface{}{
	"mode": "cors",
	// see https://crbug.com/1215429
	/*"credentials": "omit",*/
})

func DoPutRequest(_ context.Context, url, contentType string, body []byte) ([]byte, error) {
	WarnIfCalledFromRenderLoop("DoPutRequest %q", url)

	u8 := Uint8Array.New(len(body))
	js.CopyBytesToJS(u8, body)

	resp, err := Await(Fetch.Invoke(url, map[string]interface{}{
		"method":      "PUT",
		"mode":        "cors",
		"credentials": "omit",
		"headers": map[string]interface{}{
			"Content-Type": contentType,
		},
		"body": u8,
	}))
	if err != nil {
		return nil, fmt.Errorf("internal: PUT %q: %w", url, err)
	}

	jsBuf, err := Await(resp.Call("arrayBuffer"))
	if err != nil {
		return nil, fmt.Errorf("internal: PUT %q: %w", url, err)
	}

	u8 = Uint8Array.New(jsBuf, 0)
	buf := make([]byte, u8.Length())
	js.CopyBytesToGo(buf, u8)

	// Response must be HTTP 201 Created for success.
	if err := ExpectStatus(resp, 201); err != nil {
		return nil, fmt.Errorf("internal: PUT %q: server returned %q: %w", url, buf, err)
	}

	return buf, nil
}

func ExpectStatus(resp js.Value, expected int) error {
	if status := resp.Get("status").Int(); status != expected {
		return ErrUnexpectedStatus{Status: strconv.Itoa(status) + " " + resp.Get("statusText").String()}
	}

	return nil
}

func FetchBytes(_ context.Context, url string) ([]byte, error) {
	WarnIfCalledFromRenderLoop("FetchBytes %q", url)

	native := js.Global().Get("SpyCards").Get("Native")

	promise := native.Get("preloadedGeneric").Get(url)
	if promise.Truthy() {
		native.Get("preloadedGeneric").Delete(url)
	} else {
		promise = Fetch.Invoke(url, FetchOpts)
	}

	resp, err := Await(promise)
	if err != nil {
		return nil, fmt.Errorf("internal: GET %q: %w", url, err)
	}

	if status := resp.Get("status").Int(); status == 503 {
		text, err := Await(resp.Call("text"))
		if err != nil {
			return nil, fmt.Errorf("internal: GET %q: %w", url, err)
		}

		return nil, fmt.Errorf("server returned error: %q", strings.TrimSpace(text.String()))
	}

	if err := ExpectStatus(resp, 200); err != nil {
		return nil, fmt.Errorf("internal: GET %q: %w", url, err)
	}

	buf, err := Await(resp.Call("arrayBuffer"))
	if err != nil {
		return nil, fmt.Errorf("internal: GET %q: %w", url, err)
	}

	u8 := Uint8Array.New(buf, 0)
	b := make([]byte, u8.Length())
	js.CopyBytesToGo(b, u8)

	return b, nil
}

func FetchJSON(_ context.Context, url string, v interface{}) error {
	WarnIfCalledFromRenderLoop("FetchJSON %q", url)

	native := js.Global().Get("SpyCards").Get("Native")

	promise := native.Get("preloadedGeneric").Get(url)
	if promise.Truthy() {
		native.Get("preloadedGeneric").Delete(url)
	} else {
		promise = Fetch.Invoke(url, FetchOpts)
	}

	resp, err := Await(promise)
	if err != nil {
		return fmt.Errorf("internal: GET %q: %w", url, err)
	}

	if err := ExpectStatus(resp, 200); err != nil {
		return fmt.Errorf("internal: GET %q: %w", url, err)
	}

	jsV, err := Await(resp.Call("json"))
	if err != nil {
		return fmt.Errorf("internal: GET %q: %w", url, err)
	}

	return convertFromJS(reflect.ValueOf(v).Elem(), jsV)
}

func MarshalJSON(v interface{}) ([]byte, error) {
	// for now, until further work is done
	return json.Marshal(v) //nolint:wrapcheck
}

func UnmarshalJSON(b []byte, v interface{}) error {
	jsV := js.Global().Get("JSON").Call("parse", string(b))

	return convertFromJS(reflect.ValueOf(v).Elem(), jsV)
}

func convertFromJS(dst reflect.Value, src js.Value) error {
	var err error

	if src.IsUndefined() {
		return nil
	}

	switch dst.Kind() {
	case reflect.Slice:
		if dst.Type().Elem().Kind() == reflect.Uint8 {
			if src.Type() == js.TypeNull {
				dst.SetBytes(nil)

				return nil
			}

			if src.Type() != js.TypeString {
				return fmt.Errorf("internal: unexpected type for base64 data: %v", src.Type())
			}

			b, err := base64.StdEncoding.DecodeString(src.String())
			if err != nil {
				return fmt.Errorf("internal: decoding base64 found in JSON: %w", err)
			}

			dst.SetBytes(b)

			return nil
		}

		l := src.Length()
		dst.Set(reflect.MakeSlice(dst.Type(), l, l))

		for i := 0; i < l; i++ {
			err = convertFromJS(dst.Index(i), src.Index(i))
			if err != nil {
				return fmt.Errorf("at index %d: %w", i, err)
			}
		}

		return nil
	case reflect.Struct:
		typ := dst.Type()

		for i := 0; i < typ.NumField(); i++ {
			f := typ.Field(i)
			tag := f.Tag.Get("json")

			if tag == "-" {
				continue
			}

			if tag == "" {
				tag = f.Name
			}

			if strings.Contains(tag, ",") {
				panic("internal: unhandled JSON tag: " + tag)
			}

			err = convertFromJS(dst.Field(i), src.Get(tag))
			if err != nil {
				return fmt.Errorf("in field %q: %w", f.Name, err)
			}
		}

		return nil
	case reflect.String:
		dst.SetString(src.String())

		return nil
	case reflect.Int, reflect.Int32, reflect.Int64:
		dst.SetInt(int64(src.Int()))

		return nil
	case reflect.Float32, reflect.Float64:
		dst.SetFloat(src.Float())

		return nil
	case reflect.Bool:
		dst.SetBool(src.Bool())

		return nil
	default:
		panic("internal: unhandled JSON type: " + dst.Kind().String() + " " + dst.Type().String() + " (" + src.Type().String() + ")")
	}
}
