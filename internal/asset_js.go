//go:build js && wasm
// +build js,wasm

package internal

import (
	"bytes"
	"context"
	"fmt"
	"io"
	"strings"
)

// OpenAsset opens a local asset or performs an HTTP request.
func OpenAsset(name string) (io.ReadCloser, error) {
	if !strings.Contains(name, ":") {
		name = "/" + name
	}

	b, err := FetchBytes(context.Background(), name)
	if err != nil {
		return nil, fmt.Errorf("loading asset %q: %w", name, err)
	}

	return io.NopCloser(bytes.NewReader(b)), nil
}
