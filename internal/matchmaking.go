package internal

// Matchmaking websocket close codes.
const (
	NormalClosure     = 1000
	MMServerError     = 3000
	MMClientError     = 3001
	MMConnectionError = 3002
	MMTimeout         = 3003
	MMNotFound        = 3004
)
