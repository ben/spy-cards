//go:build !js || !wasm
// +build !js !wasm

package internal

import (
	"context"
	"errors"
)

func PerformanceMark(name string) {
	// do nothing
}

func PerformanceMeasure(name, start, end string) {
	// do nothing
}

func OpenURL(ctx context.Context, u string) error {
	return errors.New("unfortunately, opening URLs is not yet implemented for this platform")
}
