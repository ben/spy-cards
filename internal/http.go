package internal

import "fmt"

type ErrUnexpectedStatus struct {
	Status string
}

func (err ErrUnexpectedStatus) Error() string {
	return fmt.Sprintf("internal: unexpected HTTP status %q", err.Status)
}
