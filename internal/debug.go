package internal

import (
	"bytes"
	"crypto/rand"
	"errors"
	"fmt"
	"log"
	"runtime"
	"runtime/debug"

	"git.lubar.me/ben/spy-cards/internal/router"
)

// RenderLoopID is the goroutine ID of the render loop.
var RenderLoopID string

// GoRoutineID returns the ID of the current goroutine.
//
// This is a very bad idea. You should never, ever do this.
func GoRoutineID() string {
	var buf [32]byte
	id := buf[len("goroutine "):runtime.Stack(buf[:], false)]

	return string(id[:bytes.IndexByte(id, ' ')])
}

func WarnIfCalledFromRenderLoop(message string, args ...interface{}) {
	if GoRoutineID() == RenderLoopID {
		log.Printf("WARNING: %s called from render loop goroutine\n%s", fmt.Sprintf(message, args...), debug.Stack())
	}
}

// ErrRandomFailure is returned 1/16 of the time for certain operations in random failure mode.
var ErrRandomFailure = errors.New("internal: synthesized error for testing")

func RandomlyFail() error {
	if !router.FlagBreakShitForTesting.IsSet() {
		return nil
	}

	var b [1]byte

	_, err := rand.Read(b[:])
	if err != nil {
		return fmt.Errorf("internal: generating random data for failure: %w", err)
	}

	if b[0] > 15 {
		return nil
	}

	return ErrRandomFailure
}
