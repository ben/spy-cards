//go:build cgo
// +build cgo

package webp

// #include <webp/encode.h> // install on Ubuntu with: sudo apt install libwebp-dev
// #include <webp/decode.h>
// #cgo LDFLAGS: -lwebp
import "C"
import (
	"errors"
	"image"
	"image/draw"
	"unsafe"
)

var (
	errDecodingFailed = errors.New("webp: decoding failed")
	errEncodingFailed = errors.New("webp: encoding failed")
)

func DecodeNRGBA(b []byte) (*image.NRGBA, error) {
	var width, height C.int

	pix := C.WebPDecodeRGBA((*C.uint8_t)(&b[0]), C.size_t(len(b)), &width, &height)
	if pix == nil {
		return nil, errDecodingFailed
	}
	defer C.WebPFree(unsafe.Pointer(pix))

	return &image.NRGBA{
		Pix:    C.GoBytes(unsafe.Pointer(pix), width*height*4),
		Stride: int(width * 4),
		Rect:   image.Rect(0, 0, int(width), int(height)),
	}, nil
}

func EncodeRGBA(img image.Image, quality float32) ([]byte, error) {
	nrgba, ok := img.(*image.NRGBA)
	if !ok {
		nrgba = image.NewNRGBA(img.Bounds())
		draw.Draw(nrgba, nrgba.Rect, img, image.Point{}, draw.Src)
	}

	var b *C.uint8_t

	size := C.WebPEncodeRGBA((*C.uint8_t)(&nrgba.Pix[0]), C.int(nrgba.Rect.Dx()), C.int(nrgba.Rect.Dy()), C.int(nrgba.Stride), C.float(quality), &b)
	if size == 0 {
		return nil, errEncodingFailed
	}
	defer C.WebPFree(unsafe.Pointer(b))

	return C.GoBytes(unsafe.Pointer(b), C.int(size)), nil
}
