//go:build !cgo
// +build !cgo

package webp

import (
	"errors"
	"image"
)

var errNoCgo = errors.New("webp: cgo is required to use libwebp")

func DecodeNRGBA(b []byte) (*image.NRGBA, error) {
	return nil, errNoCgo
}

func EncodeRGBA(img image.Image, quality float32) ([]byte, error) {
	return nil, errNoCgo
}
