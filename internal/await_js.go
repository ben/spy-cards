//go:build js && wasm
// +build js,wasm

package internal

import (
	"syscall/js"
)

// Await implements the JavaScript await operator.
func Await(promise js.Value) (js.Value, error) {
	v, err := AwaitNoRandomFail(promise)
	if err != nil {
		return js.Undefined(), err
	}

	if err := RandomlyFail(); err != nil {
		return js.Undefined(), err
	}

	return v, nil
}

// AwaitNoRandomFail is like Await but never returns a random failure for testing.
func AwaitNoRandomFail(promise js.Value) (js.Value, error) {
	type resultPair struct {
		v   js.Value
		err error
	}

	WarnIfCalledFromRenderLoop("await")

	ch := make(chan resultPair, 1)

	thenFunc := js.FuncOf(func(_ js.Value, args []js.Value) interface{} {
		ch <- resultPair{args[0], nil}

		return js.Undefined()
	})
	defer thenFunc.Release()

	catchFunc := js.FuncOf(func(_ js.Value, args []js.Value) interface{} {
		ch <- resultPair{js.Undefined(), js.Error{Value: args[0]}}

		return js.Undefined()
	})
	defer catchFunc.Release()

	promise.Call("then", thenFunc, catchFunc)

	result := <-ch

	return result.v, result.err
}

// TryToDetachBuffer is a JavaScript hack to try to lose access to an ArrayBuffer, thus making it garbage-collectable when it might be a GC root for whatever reason.
var TryToDetachBuffer = Function.New("", `var disposeWorker;
return function tryToDetachBuffer(buffer) {
	if (!disposeWorker) {
		// script doesn't matter, as it doesn't do anything
		disposeWorker = new Worker("/script/config.js", {name: "buffer-dispose-helper"});
	}
	disposeWorker.postMessage("", [buffer]);
}`).Invoke()
