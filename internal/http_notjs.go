//go:build !js || !wasm
// +build !js !wasm

package internal

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"runtime"
)

var userAgent = fmt.Sprintf("SpyCardsOnline/%d.%d.%d (%s; %s) Go/%s", Version[0], Version[1], Version[2], runtime.GOOS, runtime.GOARCH, runtime.Version())

// ModifyRequest modifies an HTTP request before it is sent.
func ModifyRequest(r *http.Request) *http.Request {
	r.Header.Set("User-Agent", userAgent)

	return r
}

func DoPutRequest(ctx context.Context, url, contentType string, body []byte) ([]byte, error) {
	req, err := http.NewRequestWithContext(ctx, http.MethodPut, url, bytes.NewReader(body))
	if err != nil {
		return nil, fmt.Errorf("internal: %w", err)
	}

	req.Header.Set("Content-Type", contentType)

	req = ModifyRequest(req)

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return nil, fmt.Errorf("internal: %w", err)
	}
	defer resp.Body.Close()

	b, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, fmt.Errorf("internal: GET %q: %w", url, err)
	}

	if resp.StatusCode != http.StatusCreated {
		return nil, fmt.Errorf("internal: GET %q: %q %w", url, b, ErrUnexpectedStatus{Status: resp.Status})
	}

	return b, nil
}

func FetchBytes(ctx context.Context, url string) ([]byte, error) {
	req, err := http.NewRequestWithContext(ctx, http.MethodGet, url, nil)
	if err != nil {
		return nil, fmt.Errorf("internal: %w", err)
	}

	req = ModifyRequest(req)

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return nil, fmt.Errorf("internal: %w", err)
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("internal: GET %q: %w", url, ErrUnexpectedStatus{Status: resp.Status})
	}

	b, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, fmt.Errorf("internal: GET %q: %w", url, err)
	}

	return b, nil
}

func FetchJSON(ctx context.Context, url string, v interface{}) error {
	b, err := FetchBytes(ctx, url)
	if err != nil {
		return err
	}

	err = json.Unmarshal(b, v)
	if err != nil {
		return fmt.Errorf("internal: GET %q: %w", url, err)
	}

	return nil
}

func MarshalJSON(v interface{}) ([]byte, error) {
	return json.Marshal(v) //nolint:wrapcheck
}

func UnmarshalJSON(b []byte, v interface{}) error {
	return json.Unmarshal(b, v) //nolint:wrapcheck
}
