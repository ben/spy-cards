//go:build (!js || !wasm) && !headless
// +build !js !wasm
// +build !headless

package internal

func PrefersReducedMotion() bool {
	if s := LoadSettings(); s.PrefersReducedMotion {
		return true
	}

	return false // TODO: is there an API for this?
}

func LikelyTouch() bool {
	return false
}
