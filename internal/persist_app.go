//go:build (!js || !wasm) && !headless
// +build !js !wasm
// +build !headless

package internal

import (
	"fmt"
	"os"
	"path/filepath"
	"runtime"
)

func dataStoragePath() (string, error) {
	if t := os.TempDir(); runtime.GOOS == "android" && filepath.Base(t) == "cache" {
		// HACK
		return filepath.Join(filepath.Dir(t), "files"), nil
	}

	dir, err := os.UserConfigDir()
	if err != nil {
		return "", fmt.Errorf("internal: cannot determine data storage path: %w", err)
	}

	dir = filepath.Join(dir, "spy-cards.lubar.me")

	return dir, nil
}

func LoadData(key string) ([]byte, error) {
	base, err := dataStoragePath()
	if err != nil {
		return nil, err
	}

	b, err := os.ReadFile(filepath.Join(base, key+".dat")) /*#nosec*/
	if os.IsNotExist(err) {
		return nil, nil
	}

	if err != nil {
		return b, fmt.Errorf("internal: loading data: %w", err)
	}

	return b, nil
}

func StoreData(key string, val []byte) error {
	base, err := dataStoragePath()
	if err != nil {
		return err
	}

	if err := os.MkdirAll(base, 0700); err != nil {
		return fmt.Errorf("internal: storing data: %w", err)
	}

	if err := os.WriteFile(filepath.Join(base, key+".dat"), val, 0600); err != nil {
		return fmt.Errorf("internal: storing data: %w", err)
	}

	return nil
}
