//go:build js && wasm
// +build js,wasm

package internal

import (
	"context"
	"syscall/js"
)

// Useful JavaScript globals.
var (
	Fetch         = js.Global().Get("fetch")
	Uint8Array    = js.Global().Get("Uint8Array")
	Float32Array  = js.Global().Get("Float32Array")
	Blob          = js.Global().Get("Blob")
	Promise       = js.Global().Get("Promise")
	Function      = js.Global().Get("Function")
	Worker        = js.Global().Get("Worker")
	Location      = js.Global().Get("location")
	History       = js.Global().Get("history")
	jsURL         = js.Global().Get("URL")
	jsPerformance = js.Global().Get("performance")
)

func CreateObjectURL(blob js.Value) string {
	return jsURL.Call("createObjectURL", blob).String()
}

func RevokeObjectURL(s string) {
	jsURL.Call("revokeObjectURL", s)
}

func PerformanceMark(name string) {
	jsPerformance.Call("mark", name)
}

func PerformanceMeasure(name, start, end string) {
	defer func() {
		// because sometimes we can't have nice things
		_ = recover()
	}()

	jsPerformance.Call("measure", name, start, end)
}

func OpenURL(_ context.Context, u string) error {
	js.Global().Call("open", u, "_blank", "noopener")

	return nil
}
