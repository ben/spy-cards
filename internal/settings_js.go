//go:build js && wasm && !headless
// +build js,wasm,!headless

package internal

import (
	"strings"
	"syscall/js"
)

func init() {
	OnSettingsChanged = append(OnSettingsChanged, func(*Settings) {
		sw := js.Global().Get("navigator").Get("serviceWorker")
		if sw.Truthy() {
			sw = sw.Get("controller")
		}
		if sw.Truthy() {
			sw.Call("postMessage", map[string]interface{}{"type": "settings-changed"})
		}

		prm = initPRM()
	})

	js.Global().Call("addEventListener", "spy-cards-settings-changed", js.FuncOf(func(_ js.Value, _ []js.Value) interface{} {
		s := LoadSettings()

		for _, f := range OnSettingsChanged {
			f(s)
		}

		return js.Undefined()
	}))
}

func initPRM() bool {
	if s := LoadSettings(); s.PrefersReducedMotion {
		return true
	}

	if !js.Global().Get("matchMedia").Truthy() {
		return false
	}

	return js.Global().Call("matchMedia", "(prefers-reduced-motion: reduce)").Get("matches").Bool()
}

var prm = initPRM()

func PrefersReducedMotion() bool {
	return prm
}

func LikelyTouch() bool {
	if uad := js.Global().Get("navigator").Get("userAgentData"); uad.Truthy() {
		return uad.Get("mobile").Bool()
	}

	ua := js.Global().Get("navigator").Get("userAgent").String()

	return strings.Contains(ua, " Mobile ")
}
