//go:build !js || !wasm || headless
// +build !js !wasm headless

package internal

// SetActive sets the active flag for the UI, which hides some UI elements.
func SetActive(b bool) {
	// do nothing
}

// SetWindowTitle is set to a callback that sets the window title.
var SetWindowTitle func(string)

// SetTitle sets the window title.
func SetTitle(title string) {
	if SetWindowTitle != nil {
		SetWindowTitle(title)
	}
}
