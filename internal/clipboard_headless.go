//go:build headless
// +build headless

package internal

import "errors"

func ReadClipboard() (string, error) {
	return "", errors.New("internal: cannot read clipboard in headless mode")
}

func WriteClipboard(s string) error {
	return errors.New("internal: cannot write clipboard in headless mode")
}
