//go:build js && wasm && !headless
// +build js,wasm,!headless

package router

import (
	"net/url"
	"strings"
	"syscall/js"
)

func initFlags() map[Flag]bool {
	q, err := url.ParseQuery(strings.TrimPrefix(js.Global().Get("location").Get("search").String(), "?"))
	if err != nil {
		panic(err)
	}

	set := make(map[Flag]bool)

	for k := range q {
		set[Flag(k)] = true
	}

	return set
}
