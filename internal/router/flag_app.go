//go:build (!js || !wasm) && !headless
// +build !js !wasm
// +build !headless

package router

func initFlags() map[Flag]bool {
	return make(map[Flag]bool)
}
