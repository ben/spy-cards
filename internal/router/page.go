//go:generate stringer -type Page -trimprefix Page

package router

type Page int

// Constants for Page.
const (
	PageNotFound Page = iota
	PageStatic
	PageServerDynamic
	PageAphidFestival
	PageELF
	PageBugTables
	PageArcadeMain
	PageArcadeGame
	PageArcadeHighScores
	PageArcadeRecording
	PageCardsHome
	PageCardsJoin
	PageCardsModes
	PageCardsDeck
	PageCardsDeckEditor
	PageCardsRecording
	PageCardsEditor
)
