//go:build js && wasm
// +build js,wasm

package router

import "syscall/js"

func Replace(info *PageInfo) {
	b, err := info.MarshalText()
	if err != nil {
		panic(err)
	}

	js.Global().Get("history").Call("replaceState", js.Null(), "", string(b))
}
