package router

import (
	"bytes"
	"fmt"
	"net/url"
	"strconv"
	"strings"

	"git.lubar.me/ben/spy-cards/arcade"
)

type PageInfo struct {
	Page      Page
	Raw       string
	Game      arcade.Game
	HSMode    arcade.HighScoresMode
	Code      string
	Seed      string
	Mode      string
	Custom    string
	Variant   uint64
	Revision  int
	PageNum   int
	AsNPC     string
	VersusNPC string
}

func (p *PageInfo) MarshalText() ([]byte, error) {
	b := p.marshalText()

	next := byte('?')
	if bytes.Contains(b, []byte{'?'}) {
		next = '&'
	}

	var hash []byte

	if i := bytes.Index(b, []byte{'#'}); i != -1 {
		hash = b[i:]
		b = b[:i:i]
	}

	for _, d := range definedFlags {
		if d.IsSet() {
			b = append(b, next)
			b = append(b, url.QueryEscape(string(d))...)

			next = '&'
		}
	}

	return append(b, hash...), nil
}

func (p *PageInfo) marshalText() []byte {
	switch p.Page {
	case PageNotFound, PageStatic, PageServerDynamic:
		return []byte(p.Raw)
	case PageAphidFestival:
		return []byte("/festival.html")
	case PageELF:
		return []byte("/elf.html")
	case PageBugTables:
		return []byte("/bugtables.html")
	case PageArcadeMain:
		return []byte("/arcade.html")
	case PageArcadeGame:
		var u []byte

		switch p.Game {
		case arcade.MiteKnight:
			u = []byte("/arcade/mite-knight")
		case arcade.FlowerJourney:
			u = []byte("/arcade/flower-journey")
		default:
			panic("router: unhandled arcade game: " + p.Game.String())
		}

		if p.Seed != "" {
			u = append(u, "?seed="...)
			u = append(u, url.QueryEscape(p.Seed)...)
		}

		if p.Custom != "" {
			u = append(u, '#')
			u = append(u, p.Custom...)
		}

		return u
	case PageArcadeHighScores:
		var u []byte

		switch p.Game {
		case arcade.MiteKnight:
			u = []byte("/arcade/high-scores/mite-knight")
		case arcade.FlowerJourney:
			u = []byte("/arcade/high-scores/flower-journey")
		default:
			panic("router: unhandled arcade game: " + p.Game.String())
		}

		switch p.HSMode {
		case arcade.AllTime:
			// no suffix
		case arcade.Weekly:
			u = append(u, "/weekly"...)
		case arcade.Quarterly:
			u = append(u, "/quarterly"...)
		case arcade.Recent:
			u = append(u, "/recent"...)
		default:
			panic("router: unhandled high scores mode: " + p.HSMode.String())
		}

		if p.PageNum > 0 {
			u = append(u, "?page="...)
			u = strconv.AppendInt(u, int64(p.PageNum)+1, 10)
		}

		return u
	case PageArcadeRecording:
		return append([]byte("/arcade/recording/"), p.Code...)
	case PageCardsHome:
		u := append([]byte("/game/home/"), p.Mode...)

		if p.Mode == "" {
			u = append(u, "vanilla"...)
		}

		queryNext := byte('?')

		if p.Revision != 0 {
			u = append(u, queryNext)
			u = append(u, "rev="...)
			u = strconv.AppendInt(u, int64(p.Revision), 10)

			queryNext = '&'
		}

		if p.Variant != 0 {
			u = append(u, queryNext)
			u = append(u, "variant="...)
			u = strconv.AppendUint(u, p.Variant, 10)

			queryNext = '&'
		}

		if p.AsNPC != "" {
			u = append(u, queryNext)
			u = append(u, "npc="...)
			u = append(u, url.QueryEscape(p.AsNPC)...)

			queryNext = '&'
		}

		if p.VersusNPC != "" {
			u = append(u, queryNext)
			u = append(u, "vs-npc="...)
			u = append(u, url.QueryEscape(p.VersusNPC)...)
		}

		if p.Mode == "custom" && p.Custom != "" {
			u = append(u, '#')
			u = append(u, p.Custom...)
		}

		return u
	case PageCardsJoin:
		return append([]byte("/game/join/"), p.Code...)
	case PageCardsModes:
		return []byte("/game/modes")
	case PageCardsDeck:
		u := append(append(append([]byte("/game/deck/"), p.Mode...), '/'), p.Code...)

		if p.Mode == "custom" && p.Custom != "" {
			u = append(u, '#')
			u = append(u, p.Custom...)
		}

		return u
	case PageCardsDeckEditor:
		u := append([]byte("/game/deck/"), p.Mode...)

		if p.Mode == "custom" && p.Custom != "" {
			u = append(u, '#')
			u = append(u, p.Custom...)
		}

		return u
	case PageCardsRecording:
		if p.Code == "auto" && p.Mode != "" {
			return append([]byte("/game/recording/auto/"), p.Mode...)
		}

		return append([]byte("/game/recording/"), url.PathEscape(p.Code)...)
	case PageCardsEditor:
		u := []byte("/game.html?custom=editor")

		if p.Mode != "" {
			u = append(u, "&edit="...)
			u = append(u, p.Mode...)
		}

		if p.Custom != "" {
			u = append(u, '#')
			u = append(u, p.Custom...)
		}

		return u
	default:
		panic("router: unhandled page type: " + p.Page.String())
	}
}

func (p *PageInfo) UnmarshalText(b []byte) error {
	u, err := url.Parse(string(b))
	if err != nil {
		return fmt.Errorf("router: parsing URL: %w", err)
	}

	q := u.Query()

	p.Raw = string(b)

	n, err := strconv.Atoi(q.Get("page"))
	if err == nil && n >= 1 {
		p.PageNum = n - 1
	} else {
		p.PageNum = 0
	}

	switch {
	case u.Path == "/elf.html":
		p.Page = PageELF
	case u.Path == "/festival.html":
		p.Page = PageAphidFestival
	case u.Path == "/bugtables.html":
		p.Page = PageBugTables
	case u.Path == "/game.html":
		switch {
		case q.Get("join") != "":
			p.Page = PageCardsJoin
			p.Code = q.Get("join")
		case q.Get("state") == "select":
			p.Page = PageCardsModes
		case q.Get("deck") != "":
			p.Page = PageCardsDeck
			p.Code = q.Get("deck")
		case len(q["deck"]) != 0 || q.Get("state") == "deck":
			p.Page = PageCardsDeckEditor
		case q.Get("custom") == "editor":
			p.Page = PageCardsEditor
			p.Mode = q.Get("edit")
			p.Custom = u.Fragment

			return nil
		case q.Get("recording") != "":
			p.Page = PageCardsRecording
			p.Code = q.Get("recording")
		default:
			p.Page = PageCardsHome
			p.AsNPC = q.Get("npc")
			p.VersusNPC = q.Get("vs-npc")
		}

		p.Mode = q.Get("mode")

		p.Custom = u.Fragment

		if q.Get("variant") != "" {
			p.Variant, err = strconv.ParseUint(q.Get("variant"), 10, 64)
			if err != nil {
				p.Page = PageNotFound

				break
			}
		} else {
			p.Variant = 0
		}

		switch {
		case q.Get("rev") != "":
			p.Revision, err = strconv.Atoi(q.Get("rev"))
			if err != nil {
				p.Page = PageNotFound

				break
			}
		case q.Get("ver") != "":
			p.Revision, err = strconv.Atoi(q.Get("ver"))
			if err != nil {
				p.Page = PageNotFound

				break
			}
		default:
			p.Revision = 0
		}
	case strings.HasPrefix(u.Path, "/game/join/"):
		p.Page = PageCardsJoin
		p.Code = u.Path[len("/game/join/"):]
	case u.Path == "/game/modes":
		p.Page = PageCardsModes
	case u.Path == "/arcade.html":
		switch {
		case len(q["recording"]) != 0:
			p.Page = PageArcadeRecording
			p.Code = q.Get("recording")
		case len(q["highscores"]) != 0:
			p.Page = PageArcadeHighScores

			switch q.Get("highscores") {
			case "Recent":
				p.HSMode = arcade.Recent
			case "Weekly":
				p.HSMode = arcade.Weekly
			case "Quarterly":
				p.HSMode = arcade.Quarterly
			default:
				p.HSMode = arcade.AllTime
			}

			switch q.Get("game") {
			default:
				p.Game = arcade.FlowerJourney
			case "MiteKnight":
				p.Game = arcade.MiteKnight
			}
		case q.Get("autoPlay") == "0":
			p.Page = PageArcadeGame
			p.Game = arcade.FlowerJourney
			p.Seed = q.Get("seed")
		case q.Get("autoPlay") == "1":
			p.Page = PageArcadeGame
			p.Game = arcade.MiteKnight
			p.Seed = q.Get("seed")
		default:
			p.Page = PageArcadeMain
		}
	case u.Path == "/arcade/mite-knight":
		p.Page = PageArcadeGame
		p.Game = arcade.MiteKnight
		p.Seed = q.Get("seed")
		p.Custom = u.Fragment
	case u.Path == "/arcade/flower-journey":
		p.Page = PageArcadeGame
		p.Game = arcade.FlowerJourney
		p.Seed = q.Get("seed")
		p.Custom = u.Fragment
	case u.Path == "/arcade/high-scores":
		p.Page = PageArcadeHighScores
		p.HSMode = arcade.AllTime
		p.Game = arcade.FlowerJourney
	case u.Path == "/arcade/high-scores/flower-journey":
		p.Page = PageArcadeHighScores
		p.HSMode = arcade.AllTime
		p.Game = arcade.FlowerJourney
	case u.Path == "/arcade/high-scores/mite-knight":
		p.Page = PageArcadeHighScores
		p.HSMode = arcade.AllTime
		p.Game = arcade.MiteKnight
	case u.Path == "/arcade/high-scores/flower-journey/recent":
		p.Page = PageArcadeHighScores
		p.HSMode = arcade.Recent
		p.Game = arcade.FlowerJourney
	case u.Path == "/arcade/high-scores/mite-knight/recent":
		p.Page = PageArcadeHighScores
		p.HSMode = arcade.Recent
		p.Game = arcade.MiteKnight
	case u.Path == "/arcade/high-scores/flower-journey/weekly":
		p.Page = PageArcadeHighScores
		p.HSMode = arcade.Weekly
		p.Game = arcade.FlowerJourney
	case u.Path == "/arcade/high-scores/mite-knight/weekly":
		p.Page = PageArcadeHighScores
		p.HSMode = arcade.Weekly
		p.Game = arcade.MiteKnight
	case u.Path == "/arcade/high-scores/flower-journey/quarterly":
		p.Page = PageArcadeHighScores
		p.HSMode = arcade.Quarterly
		p.Game = arcade.FlowerJourney
	case u.Path == "/arcade/high-scores/mite-knight/quarterly":
		p.Page = PageArcadeHighScores
		p.HSMode = arcade.Quarterly
		p.Game = arcade.MiteKnight
	case strings.HasPrefix(u.Path, "/arcade/recording/"):
		p.Page = PageArcadeRecording
		p.Code = u.Path[len("/arcade/recording/"):]
	case strings.HasPrefix(u.Path, "/game/home/"):
		p.Page = PageCardsHome
		p.Mode = u.Path[len("/game/home/"):]
		p.Revision = 0
		p.Variant = 0
		p.Custom = u.Fragment

		if q.Get("rev") != "" {
			p.Revision, err = strconv.Atoi(q.Get("rev"))
			if err != nil {
				p.Page = PageNotFound

				break
			}
		}

		if q.Get("variant") != "" {
			p.Variant, err = strconv.ParseUint(q.Get("variant"), 10, 64)
			if err != nil {
				p.Page = PageNotFound

				break
			}
		}

		p.AsNPC = q.Get("npc")
		p.VersusNPC = q.Get("vs-npc")
	case strings.HasPrefix(u.Path, "/game/deck/"):
		p.Page = PageCardsDeck
		p.Mode = u.Path[len("/game/deck/"):]

		if i := strings.Index(p.Mode, "/"); i != -1 {
			p.Code = p.Mode[i+1:]
			p.Mode = p.Mode[:i]
		} else {
			p.Page = PageCardsDeckEditor
		}
	case strings.HasPrefix(u.Path, "/game/recording/auto/"):
		p.Page = PageCardsRecording
		p.Code = "auto"
		p.Mode = u.Path[len("/game/recording/auto/"):]
	case strings.HasPrefix(u.Path, "/game/recording/"):
		code, err := url.PathUnescape(u.Path[len("/game/recording/"):])
		if err != nil {
			p.Page = PageNotFound

			break
		}

		p.Page = PageCardsRecording
		p.Code = code
		p.Mode = ""
	case strings.HasPrefix(u.Path, "/spy-cards/"):
		p.Page = PageServerDynamic
	case strings.HasSuffix(u.Path, ".html"):
		p.Page = PageStatic
	case strings.HasPrefix(u.Path, "/docs/"):
		p.Page = PageStatic
	default:
		p.Page = PageNotFound
	}

	return nil
}

func (p *PageInfo) ShouldRedirect() bool {
	switch p.Page {
	case PageStatic, PageServerDynamic, PageCardsEditor:
		return true
	default:
		return false
	}
}
