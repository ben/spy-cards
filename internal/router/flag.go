package router

import "net/url"

type Flag string

// Secret flag constants.
const (
	FlagAltColors           Flag = "altColors"
	FlagAudienceTest        Flag = "audienceTest"
	FlagAutoPlay            Flag = "autoPlay"
	FlagAutoRematch         Flag = "autoRematch"
	FlagBreakShitForTesting Flag = "breakShitForTesting"
	FlagCheapDebug          Flag = "cheapDebug"
	FlagCrashOnDroppedFrame Flag = "crashOnDroppedFrame"
	FlagDebugGameLog        Flag = "debugGameLog"
	FlagDebugRevealModified Flag = "debugRevealModified"
	FlagDisableExploits     Flag = "disableExploits"
	FlagDontForceGC         Flag = "dontForceGC"
	FlagForceFirefoxCompat  Flag = "forceFirefoxCompat"
	FlagForceLowGPU         Flag = "forceLowGPU"
	FlagForceMediumGPU      Flag = "forceMediumGPU"
	FlagForceSafariCompat   Flag = "forceSafariCompat"
	FlagForceSoftOpus       Flag = "forceSoftOpus"
	FlagForceSoftWebP       Flag = "forceSoftWebP"
	FlagGfxPerf             Flag = "gfxPerf"
	FlagInHandDebug         Flag = "inHandDebug"
	FlagMKAssist            Flag = "mkAssist"
	FlagMMORPG              Flag = "mmorpg"
	FlagMockRTC             Flag = "mockRTC"
	FlagNo3d                Flag = "no3d"
	FlagNoBatchCaching      Flag = "noBatchCaching"
	FlagNoExternalTextures  Flag = "noExternalTextures"
	FlagNoFancyBorders      Flag = "noFancyBorders"
	FlagNoTextures          Flag = "noTextures"
	FlagNoWindows           Flag = "noWindows"
	FlagPeelMyCards         Flag = "peelMyCards"
	FlagRTCStats            Flag = "rtcStats"
	FlagRTCSpew             Flag = "rtcSpew"
	FlagShowHitbox          Flag = "showHitbox"
	FlagSlow                Flag = "slow"
	FlagSpookyEnemyCards    Flag = "spookyEnemyCards"
	FlagSpriteFlex          Flag = "spriteFlex"
	FlagSuperSpeed          Flag = "superSpeed"
	FlagTP2Debug            Flag = "tp2Debug"
	FlagUseAI               Flag = "useAI"
)

var definedFlags = [...]Flag{
	FlagAltColors,
	FlagAudienceTest,
	FlagAutoPlay,
	FlagAutoRematch,
	FlagBreakShitForTesting,
	FlagCheapDebug,
	FlagCrashOnDroppedFrame,
	FlagDebugGameLog,
	FlagDebugRevealModified,
	FlagDisableExploits,
	FlagDontForceGC,
	FlagForceFirefoxCompat,
	FlagForceLowGPU,
	FlagForceMediumGPU,
	FlagForceSafariCompat,
	FlagForceSoftOpus,
	FlagForceSoftWebP,
	FlagGfxPerf,
	FlagInHandDebug,
	FlagMKAssist,
	FlagMMORPG,
	FlagMockRTC,
	FlagNo3d,
	FlagNoBatchCaching,
	FlagNoExternalTextures,
	FlagNoFancyBorders,
	FlagNoTextures,
	FlagNoWindows,
	FlagPeelMyCards,
	FlagRTCStats,
	FlagRTCSpew,
	FlagShowHitbox,
	FlagSlow,
	FlagSpookyEnemyCards,
	FlagSpriteFlex,
	FlagSuperSpeed,
	FlagTP2Debug,
	FlagUseAI,
}

var setFlags = initFlags()

func SetFlagsFromQuery(q url.Values) {
	for _, d := range definedFlags {
		if _, ok := q[string(d)]; ok {
			setFlags[d] = true
		}
	}
}

func (f Flag) ensureDefined() {
	for _, d := range definedFlags {
		if d == f {
			return
		}
	}

	panic("router: use of undefined flag " + string(f))
}

func (f Flag) IsSet() bool {
	set, ok := setFlags[f]
	if !ok {
		f.ensureDefined()

		setFlags[f] = false
	}

	return set
}

func (f Flag) ForceSet(on bool) {
	f.ensureDefined()

	setFlags[f] = on
}
