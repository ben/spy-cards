//go:build (!js || !wasm) && !headless
// +build !js !wasm
// +build !headless

package internal

import (
	"fmt"
	"runtime"
)

func ReadClipboard() (string, error) {
	return "", fmt.Errorf("internal: not implemented: read clipboard on %s", runtime.GOOS)
}

func WriteClipboard(s string) error {
	return fmt.Errorf("internal: not implemented: write clipboard on %s", runtime.GOOS)
}
