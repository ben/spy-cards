module SpyCards.UI {
	export function remove(el: Element) {
		if (el && el.parentNode) {
			el.parentNode.removeChild(el);
		}
	}
	export function clear(el: Element) {
		if (!el) {
			return;
		}
		el.textContent = "";
	}

	export function button(label: string, classes: string[], click: () => void): HTMLButtonElement {
		const btn = document.createElement("button");
		btn.classList.add.apply(btn.classList, classes);
		btn.textContent = label;
		btn.addEventListener("click", (e) => {
			e.preventDefault();
			click();
		});
		return btn;
	}
	export function option(label: string, value: string): HTMLOptionElement {
		const opt = document.createElement("option");
		opt.textContent = label;
		opt.value = value;
		return opt;
	}
}
