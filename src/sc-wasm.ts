module SpyCards.Native {
	export var profiler: any;

	export function onGLContextFail(message: string) {
		const container = document.createElement("div");
		container.classList.add("error-report");
		const h1 = document.createElement("h1");
		h1.textContent = "Uh oh!";
		container.appendChild(h1);
		const flavor = document.createElement("p");
		flavor.classList.add("flavor");
		flavor.textContent = "Spy Cards Online was unable to start up. The error it reported was:";
		container.appendChild(flavor);
		const msg = document.createElement("p");
		msg.classList.add("message");
		msg.textContent = message;
		container.appendChild(msg);
		const h2 = document.createElement("h2");
		h2.textContent = "Next Steps";
		container.appendChild(h2);
		const ul = document.createElement("ul");
		function addHint(text: string) {
			const li = document.createElement("li");
			li.textContent = text;
			ul.appendChild(li);
		}
		addHint("This error most commonly occurs right after installing new graphics card drivers.");
		addHint("If you haven't restarted your web browser recently, doing so might fix this.");
		addHint("Your browser might have more information about what went wrong in the developer console (ctrl+shift+i).");
		addHint("If all else fails, ask for help on the Discord linked to on the home page.");
		container.appendChild(ul);
		document.body.appendChild(container);
		debugger;
	}

	export let go = new (window as any).Go();
	let inst: WebAssembly.Instance;
	let mod: WebAssembly.Module;
	let startFetch: () => void;
	let ready = new Promise<void>((resolve) => startFetch = resolve).then(function () {
		performance.mark("fetchWASMStart");

		const loading = document.getElementById("loading");
		const loadingSub = document.createElement("div");
		loadingSub.id = "loading-sub";
		if (loading) {
			loading.appendChild(loadingSub);
		}

		loadingSub.innerHTML = "Fetching WebAssembly&hellip;";

		return WebAssembly.instantiateStreaming(fetch("/spy-cards.wasm"), go.importObject).then(function (result) {
			performance.mark("fetchWASMFinish");
			performance.measure("fetchWASM", "fetchWASMStart", "fetchWASMFinish");

			inst = result.instance;
			mod = result.module;

			loadingSub.innerHTML = "Starting up&hellip;";
		});
	});

	let failedUpdate = false;

	function checkUpdate(force: boolean): Promise<void> {
		if (failedUpdate) {
			return Promise.resolve();
		}

		if (!navigator.serviceWorker) {
			return Promise.resolve();
		}

		const loading = document.getElementById("loading");
		return navigator.serviceWorker.ready.then(function (sw) {
			if (sw && force) {
				performance.mark("checkSWStart");
				sw.update().then(function () {
					performance.mark("checkSWFinish");
					performance.measure("checkSW", "checkSWStart", "checkSWFinish");
				});
			}

			return sw;
		}).then(function (sw) {
			if (!sw || !sw.installing) {
				return Promise.resolve();
			}

			loading.innerHTML = "Updating&hellip;";
			return new Promise<void>(function (resolve, reject) {
				navigator.serviceWorker.addEventListener("controllerchange", function () {
					resolve();
				});
				navigator.serviceWorker.addEventListener("message", function (e) {
					if (e.data.type === "update-error") {
						reject();
					}
				});
			}).then(function () {
				setTimeout(function () { location.reload() }, 1000);
				return new Promise<void>(() => { });
			});
		}).catch(function () {
			failedUpdate = true;
			loading.innerHTML = "Loading&hellip;";
		});
	}

	export const preloadedGeneric: { [name: string]: Promise<Response>; } = {};
	export const preloadedAudio: { [name: string]: Promise<AudioBuffer>; } = {};
	export const preloadedTextures: { [name: string]: Promise<CanvasImageSource>; } = {};

	export function preloadHint(...urls: string[]) {
		for (let i = 0; i < urls.length; i++) {
			const url = urls[i];
			if (url.endsWith(".webp")) {
				preloadedTextures[url] = new Promise<CanvasImageSource>((resolve, reject) => {
					const img = new Image();
					img.crossOrigin = "anonymous";
					img.src = url;
					(img as any).close = () => { };
					if (img.decode) {
						img.decode().then(() => resolve(img), reject);
					} else {
						img.onload = () => resolve(img);
						img.onerror = reject;
					}
				});
			} else {
				// TODO: if opus decoding is supported, do that here
				preloadedGeneric[url] = fetch(url, {
					mode: "cors",
					credentials: "omit",
				});
			}
		}
	}

	export function run(...argv: string[]): Promise<void> {
		let argvLength = 0;
		for (let i = 0; i < argv.length; i++) {
			argvLength += argv[i].length;
		}

		if (argvLength > (1 << 16)) {
			return Promise.reject(new Error("URL too long (approximately " + Math.round(argvLength / 1000) + "k characters)"));
		}

		startFetch();

		const checkingReady = checkUpdate(true);
		(function () {
			if (localStorage["spy-cards-settings-v0"]) {
				return Promise.resolve();
			}

			const loading = document.getElementById("loading");
			ready = (function (orig: Promise<void>) {
				document.querySelectorAll(".hide-on-wasm-ready").forEach((el) => el.parentNode.removeChild(el));

				Audio.Sounds.Confirm.preload();

				const form = document.createElement("form");
				form.classList.add("first-run", "readme");
				const h1 = document.createElement("h1");
				h1.textContent = "It looks like it's your first time here.";
				form.appendChild(h1);
				const p = document.createElement("p");
				p.textContent = "These choices can be changed at any time on ";
				const settingsLink = document.createElement("a");
				settingsLink.href = "/settings.html";
				settingsLink.textContent = "the Settings page";
				p.appendChild(settingsLink);
				p.appendChild(document.createTextNode("."));
				form.appendChild(p);
				const l1 = document.createElement("label");
				l1.textContent = "Music and Sound Effects";
				const c1 = document.createElement("input");
				c1.type = "checkbox";
				c1.addEventListener("input", () => {
					if (c1.checked) {
						Audio.setVolume(0.6, 0.6);
						Audio.Sounds.Confirm.play();
					} else {
						Audio.setVolume(0, 0);
					}
				});
				if (!SpyCards.Audio.actx) {
					c1.disabled = true;
					l1.innerHTML = "<del>" + l1.innerHTML + "</del><br><small>(does not work in this browser)</small>";
				}
				l1.insertBefore(c1, l1.firstChild);
				form.appendChild(l1);
				const l2 = document.createElement("label");
				l2.textContent = "3D Backgrounds";
				const c2 = document.createElement("input");
				c2.type = "checkbox";
				c2.addEventListener("input", () => {
					const settings = SpyCards.loadSettings();
					settings.disable3D = !c2.checked;
					SpyCards.saveSettings(settings);
					Audio.Sounds.Confirm.play();
				});
				l2.insertBefore(c2, l2.firstChild);
				form.appendChild(l2);
				const sg = document.createElement("label");
				sg.className = "spoiler-guard-link";
				const sga = document.createElement("a");
				sga.href = "/spoiler-guard.html";
				sga.target = "_blank";
				sga.textContent = "Spoiler Guard";
				sg.appendChild(sga);
				form.appendChild(sg);
				const btn = document.createElement("button");
				btn.disabled = true;
				checkingReady.then(() => btn.disabled = false);
				btn.textContent = "Continue";
				const formDone = new Promise<void>((resolve) => {
					btn.addEventListener("click", (e) => {
						e.preventDefault();

						let isMobile: boolean;
						if ((navigator as any).userAgentData) {
							isMobile = (navigator as any).userAgentData.mobile;
						} else {
							isMobile = navigator.userAgent.indexOf(" Mobile ") !== -1;
						}

						// make sure there is a settings object for next time
						const settings = SpyCards.loadSettings();
						if (isMobile) {
							if (window.devicePixelRatio > 1) {
								// set sampling to subsample by default on phones
								settings.dpiScale = 0.5;
							}

							// CRT looks terrible at non-native sampling so get rid of that too
							settings.disableCRT = true;

							// reduce GPU power usage to extend battery life
							// (you can't see the graphical differences at that size anyway)
							settings.limitGPULevel = 1;
						}
						SpyCards.saveSettings(settings);

						resolve();
					});
				});
				form.appendChild(btn);
				document.body.appendChild(form);

				if (loading) {
					loading.style.display = "none";
				}
				return formDone.then(function () {
					document.body.removeChild(form);
					if (loading) {
						loading.style.display = "";
					}

					return orig;
				});
			})(ready);

			return ready;
		})().then(() => checkingReady).then(function () {
			ready = ready.then(function () {
				return checkUpdate(false);
			}).then(function () {
				const params = new URLSearchParams(location.search);
				if (params.has("GOGC")) {
					go.env.GOGC = params.get("GOGC");
				}
				if (params.has("GODEBUG")) {
					go.env.GODEBUG = params.get("GODEBUG");
				}
				if (params.has("GOMAXPROCS")) {
					go.env.GOMAXPROCS = params.get("GOMAXPROCS");
				}
				if (params.has("GOTRACEBACK")) {
					go.env.GOTRACEBACK = params.get("GOTRACEBACK");
				}
				go.argv = ["js"].concat(argv);
				(window as any).SIGQUIT = () => {
					go._pendingEvent = { id: 0 };
					go._resume();
				};
				performance.mark("goInitStart");
				return go.run(inst);
			}).then(function () {
				return WebAssembly.instantiate(mod, go.importObject);
			}).then(function (i) {
				inst = i;
			}).catch(errorHandler);

			return ready;
		});

		return ready;
	}
}
