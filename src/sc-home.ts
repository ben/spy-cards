document.querySelectorAll("#screenshot-gallery a").forEach(function (el) {
	el.addEventListener("click", function (e) {
		e.preventDefault();

		const big = document.querySelector("#big-screenshot");
		big.innerHTML = "";
		const img = new Image();
		img.width = 1920;
		img.height = 1080;
		img.crossOrigin = "anonymous";
		img.src = (<HTMLAnchorElement>el).href;
		img.alt = (<HTMLImageElement>el.firstChild).alt;
		img.title = (<HTMLImageElement>el.firstChild).alt;
		big.appendChild(img);
	});
});

addEventListener("load", function () {
	const po = document.getElementById("play-online");
	if (po) {
		const versionLink = document.createElement("a");
		versionLink.className = "version-number";
		versionLink.textContent = "v" + spyCardsVersionPrefix;
		versionLink.href = "/docs/changelog.html#v" + spyCardsVersionPrefix;
		po.appendChild(versionLink);
	}
});
