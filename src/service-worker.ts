const cacheHash = "%CACHE_HASH%";
(self as any).importScripts("/script/config.js?%CONFIG_HASH%", "/script/sc-base32.js?%BASE32_HASH%");

self.addEventListener("install", (e: any) => {
	(self as any).skipWaiting();
	e.waitUntil(installCache());
});

self.addEventListener("activate", (e: any) => {
	e.waitUntil(Promise.all([
		caches.keys().then((names) => {
			return Promise.all(names.filter((name) => {
				return name !== "spy-cards-" + cacheHash && name !== "spy-cards-static-v3" && !name.startsWith("spy-cards-user-");
			}).map((name) => {
				console.log("deleting out-of-date cache collection: ", name);
				return caches.delete(name);
			}));
		}),
		(self as any).clients.claim()
	]));
});

self.addEventListener("fetch", (e: any) => {
	let req: Request = e.request;
	if (req.url.indexOf("cloudflareinsights.com") !== -1) {
		// just forward the request to the browser
		e.respondWith(fetch(req));
		return;
	}
	if (req.url.endsWith(".png") || req.url.endsWith(".webp") || req.url.endsWith(".jpeg") || req.url.endsWith(".opus") || req.url.endsWith(".stage")) {
		req = new Request(req, {
			credentials: "omit",
			mode: "cors"
		});
	} else {
		const url = new URL(req.url);
		// server-built game and arcade pages are equivalent to game.html
		// and arcade.html if javascript is enabled
		if (url.pathname.startsWith("/game/")) {
			req = new Request("/game.html");
		} else if (url.pathname.startsWith("/arcade/")) {
			req = new Request("/arcade.html");
		}
	}
	e.respondWith(caches.match(req, {
		ignoreSearch: true,
	}).then(async (resp) => {
		if (resp) {
			// have cached file
			return resp;
		}

		try {
			const realReq = req;
			req = req.clone();

			// fetch from network
			resp = await fetch(realReq);
		} catch (ex) {
			console.error("fetch for", req, "threw exception:", ex);

			// rethrow
			throw ex;
		}

		if (req.url.startsWith(custom_card_api_base_url) || req.url.startsWith(mode_changelog_base_url)) {
			// rely on browser cache for custom modes
			return resp;
		}
		if (req.url.startsWith(arcade_api_base_url) || req.url.startsWith(match_recording_base_url)) {
			// rely on browser cache for recordings
			return resp;
		}
		if (req.url.startsWith(user_image_base_url)) {
			if (req.url.endsWith(".png") || req.url.endsWith(".webp")) {
				const clonedResp = resp.clone();
				caches.open("spy-cards-user-img").then((cache) => cache.put(req, clonedResp));
			}
			return resp;
		}
		if (req.url.startsWith(ipfs_base_url)) {
			const clonedResp = resp.clone();
			caches.open("spy-cards-user-extensions").then((cache) => cache.put(req, clonedResp));
			return resp;
		}
		if (req.url.endsWith(".opus") || req.url.endsWith(".stage")) {
			const clonedResp = resp.clone();
			caches.open("spy-cards-static-v3").then((cache) => cache.put(req, clonedResp));
			return resp;
		}
		if (req.url.indexOf("/admin/") !== -1) {
			// Don't warn about admin panel pages not being precached.
			return resp;
		}
		if (req.method === "GET") {
			console.warn("Uncached request: ", req.url);
		}
		return resp;
	}));
});

async function installCache() {
	let total = 0;
	let current = 0;

	async function updateProgress() {
		current++;

		for (let client of await (self as any).clients.matchAll({ includeUncontrolled: true })) {
			client.postMessage({ type: "update-progress", current: current, total: total });
		}
	}

	async function cacheURL(cache: Cache, url: RequestInfo, always?: boolean, hash?: Uint8Array, removeOnly?: boolean) {
		if (!always) {
			const existing = await cache.match(url);
			if (existing) {
				if (!hash) {
					return;
				}

				const content = await existing.arrayBuffer();
				const digest = new Uint8Array(await crypto.subtle.digest("SHA-256", content));
				if (hash.every((b, i) => digest[i] === b)) {
					return;
				}

				console.log("deleting out-of-date static resource: ", (url as Request).url || url);
				await cache.delete(url, {
					ignoreSearch: true,
				});
			}

			if (removeOnly) {
				return;
			}

			total++;
		}

		try {
			await cache.add(url);
		} catch (ex) {
			throw new Error("for " + (url instanceof Request ? url.url : url) + ": " + (ex.message || ex));
		}

		await updateProgress();
	}

	try {
		if (cacheHash === "%" + "CACHE_HASH%") {
			throw new Error("build was not completed");
		}

		const cacheDataResp = await fetch("/cache-data.txt?" + cacheHash, { credentials: "omit" });
		const cacheDataText = await cacheDataResp.text();

		const staticCacheURLs: {
			req: Request;
			hash: Uint8Array;
			removeOnly: boolean;
		}[] = [];
		const cacheURLs: RequestInfo[] = ["/", "/docs/"];
		for (let line of cacheDataText.split("\n")) {
			if (!line) {
				continue;
			}
			const [hash, name] = line.split("  ");
			const hashBytes = new Uint8Array(hash.split(/(?=(?:..)*$)/g).map((b) => parseInt(b, 16)));

			if ((name.startsWith("script/sc-") && name !== "script/sc-error-handler.js") || name === "service-worker.js") {
				// ignore
			} else if (name.endsWith(".ico") || name.endsWith(".woff") || name.endsWith(".woff2") || name.endsWith(".ttf") || name.endsWith(".svg") || name.endsWith(".png") || name.endsWith(".jpeg") || name.endsWith(".webp") || name.startsWith(".well-known/") || name.endsWith("-decoder-worker.wasm") || name.endsWith("-decoder-worker.js")) {
				// these change much less often, if ever
				staticCacheURLs.push({
					req: new Request(name, {
						cache: "reload",
						credentials: "omit",
						integrity: "sha256-" + Base64.encode(hashBytes)
					}),
					hash: hashBytes,
					removeOnly: false,
				});
			} else if (name.endsWith(".opus") || name.endsWith(".stage")) {
				// these go in the static cache *on first request*.
				staticCacheURLs.push({
					req: new Request(name, {
						cache: "reload",
						credentials: "omit",
						integrity: "sha256-" + Base64.encode(hashBytes)
					}),
					hash: hashBytes,
					removeOnly: true,
				});
			} else if (name.endsWith(".html") || name.endsWith(".webmanifest") || name.endsWith(".wasm") || name.indexOf("godot") !== -1) {
				cacheURLs.push(new Request(name, {
					cache: "reload",
					credentials: "omit",
					integrity: "sha256-" + Base64.encode(hashBytes)
				}));
			} else if (name.startsWith("script/") && name.endsWith("-worker.js")) {
				cacheURLs.push(new Request(name, {
					cache: "reload",
					integrity: "sha256-" + Base64.encode(hashBytes)
				}));
			} else if (name.startsWith("script/") || name.startsWith("style/")) {
				cacheURLs.push(new Request(name + "?" + hash.substr(0, 8), {
					cache: "reload",
					integrity: "sha256-" + Base64.encode(hashBytes)
				}));
			} else if (name === "service-worker-status") {
				// skip; added manually with different content
			} else if (name.endsWith(".md") || name.endsWith(".txt") || name.startsWith("docs/") || name.endsWith(".br") || name.endsWith(".gz")) {
				// skip; not used by game
			} else {
				console.error("unclassified URL:", name);
				debugger;
			}
		}

		total += cacheURLs.length;

		await Promise.all([
			self.caches.open("spy-cards-static-v3").then((cache) =>
				Promise.all(staticCacheURLs.map((url) => cacheURL(cache, url.req, false, url.hash, url.removeOnly)))),
			self.caches.open("spy-cards-" + cacheHash).then((cache) =>
				Promise.all(cacheURLs.map((url) => cacheURL(cache, url, true)).concat([
					cache.put("service-worker-status", new Response("service worker active with cache hash " + cacheHash, {
						headers: [
							["Content-Type", "text/plain; charset=utf-8"]
						]
					}))
				]))),
			self.caches.open("spy-cards-user-img"),
			self.caches.open("spy-cards-user-extensions")
		]);
	} catch (ex) {
		for (let client of await (self as any).clients.matchAll({ includeUncontrolled: true })) {
			client.postMessage({ type: "update-error", message: (ex.message || ex.toString()) + "\n" + (ex.stack || "(no stack trace)") });
		}

		throw ex;
	}
}

self.addEventListener("message", async (e) => {
	switch (e.data.type) {
		case "settings-changed":
			for (let client of await (self as any).clients.matchAll()) {
				if (client.id !== (e.source as any).id) {
					client.postMessage({ type: "settings-changed" });
				}
			}
			break;
		default:
			debugger;
			break;
	}
});
