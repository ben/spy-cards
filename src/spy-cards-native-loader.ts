const heightTester = document.createElement("div");
heightTester.style.position = "fixed";
heightTester.style.height = "100vh";

function sampleViewportHeight() {
	document.body.appendChild(heightTester);
	try {
		return parseFloat(getComputedStyle(heightTester).height.replace(/px$/, ""));
	} finally {
		document.body.removeChild(heightTester);
	}
}

(function () {
	const params = new URLSearchParams(location.search);
	if (location.pathname.startsWith("/game")) {
		if (!location.pathname.startsWith("/game/recording/") && !location.pathname.startsWith("/game/join/")) {
			SpyCards.Native.preloadHint(custom_card_api_base_url + "modes");
		}

		SpyCards.Native.preloadHint(
			"/img/arcade/gui.webp",
			"/img/room/cardbattle.webp",
			"/img/customcard.webp",
			"/img/portraits2.webp",
			"/img/room/characters1.webp",
			"/img/room/characters2.webp",
			"/img/room/audience.webp",
			"/img/room/audience-blur.webp",
		);

		if (!params.has("no3d") && !SpyCards.loadSettings().disable3D) {
			SpyCards.Native.preloadHint(
				"/img/room/stage0001.stage",
				"/img/room/stage0002.stage",
				"/img/room/stage0003.stage",
				"/img/room/stage0004.stage",
				"/img/room/mine.stage",
			);
		}

		SpyCards.Audio.isEnabled().then((ok) => {
			if (ok) {
				SpyCards.Native.preloadHint(
					(location.pathname.startsWith("/game/join/CSTM") || (params.get("join") || "").startsWith("CSTM")) ? "/audio/Bounty.opus" : "/audio/Miniboss.opus",
					"/audio/Confirm.opus",
					"/audio/Confirm1.opus",
					"/audio/Inside2.opus",
					"/audio/BattleStart0.opus",
					"/audio/PageFlip.opus",
					"/audio/Buzzer.opus",
					"/audio/Coin.opus",
					"/audio/CardSound2.opus",
					"/audio/Lazer.opus",
					"/audio/AtkSuccess.opus",
					"/audio/AtkFail.opus",
					"/audio/CrowdClap.opus",
					"/audio/CrowdGasp.opus",
					"/audio/CrowdCheer2.opus",
					"/audio/Toss11.opus",
					"/audio/Damage0.opus",
					"/audio/Death3.opus",
					"/audio/Fail.opus",
					"/audio/Heal.opus",
					"/audio/StatUp.opus",
					"/audio/StatDown.opus",
				);
			}
		});
	} else if (location.pathname.startsWith("/arcade")) {
		SpyCards.Native.preloadHint(
			"/img/arcade/gui.webp",
			"/img/arcade/dungeongame.webp",
			"/img/arcade/game1.webp",
			"/img/arcade/game2.webp",
		);

		SpyCards.Audio.isEnabled().then((ok) => {
			if (ok) {
				SpyCards.Native.preloadHint(
					"/audio/Buzzer.opus",
					"/audio/Confirm.opus",
					"/audio/Confirm1.opus",
					"/audio/TermiteLoop.opus",
					"/audio/MiteKnight.opus",
					"/audio/MiteKnightIntro.opus",
					"/audio/PeacockSpiderNPCSummonSuccess.opus",
					"/audio/Shot2.opus",
					"/audio/MKDeath.opus",
					"/audio/MKGameOver.opus",
					"/audio/MKHit.opus",
					"/audio/MKHit2.opus",
					"/audio/MKKey.opus",
					"/audio/MKOpen.opus",
					"/audio/MKPotion.opus",
					"/audio/MKStairs.opus",
					"/audio/MKWalk.opus",
					"/audio/FlyingBee.opus",
					"/audio/FBCountdown.opus",
					"/audio/FBDeath.opus",
					"/audio/FBFlower.opus",
					"/audio/FBGameOver.opus",
					"/audio/FBPoint.opus",
					"/audio/FBStart.opus",
				);
			}
		});
	} else if (location.pathname === "/elf.html") {
		if (!params.has("no3d") && !SpyCards.loadSettings().disable3D) {
			SpyCards.Native.preloadHint("/img/room/bugaria-main.stage");
		}

		SpyCards.Audio.isEnabled().then((ok) => {
			if (ok) {
				SpyCards.Native.preloadHint(
					"/audio/Battle4.opus",
					"/audio/PageFlip.opus",
					"/audio/Damage0.opus",
				);
			}
		});
	} else if (location.pathname === "/festival.html") {
		if (!params.has("no3d") && !SpyCards.loadSettings().disable3D) {
			SpyCards.Native.preloadHint("/img/room/festival.stage");
		}

		SpyCards.Audio.isEnabled().then((ok) => {
			if (ok) {
				SpyCards.Native.preloadHint(
					"/audio/Battle0.opus",
					"/audio/Chef1.opus",
					"/audio/Field1.opus",
					"/audio/Inn.opus",
					"/audio/AtkSuccess.opus",
					"/audio/Kut1.opus",
					"/audio/Kut2.opus",
					"/audio/Damage1.opus",
					"/audio/Damage2.opus",
					"/audio/FunnyStep.opus",
				);
			}
		});
	}

	const testedHeight = sampleViewportHeight();

	Promise.resolve().then(function () {
		if (Math.abs(innerHeight - testedHeight) < 1) {
			return Promise.resolve();
		}

		console.warn("WARNING: reported viewport height (" + innerHeight + ") differs from actual viewport height (" + testedHeight + ")");

		if (!document.documentElement.requestFullscreen && !(document.documentElement as any).webkitRequestFullScreen) {
			console.warn("WARNING: no supported fullscreen API");

			return Promise.resolve();
		}

		const loading = document.getElementById("loading");

		const fullscreenButton = document.createElement("button");
		const nahButton = document.createElement("button");

		return new Promise<void>(resolve => {
			document.querySelectorAll(".hide-on-wasm-ready").forEach((el) => el.parentNode.removeChild(el));

			fullscreenButton.textContent = "Full Screen";
			fullscreenButton.addEventListener("click", () => {
				if ((document.documentElement as any).webkitRequestFullScreen) {
					Promise.resolve((document.documentElement as any).webkitRequestFullScreen()).then(resolve);
				} else {
					Promise.resolve(document.documentElement.requestFullscreen()).then(resolve);
				}
			});

			nahButton.textContent = "Nah";
			nahButton.addEventListener("click", () => resolve());

			loading.classList.add("question");
			loading.textContent = " or ";
			loading.insertBefore(fullscreenButton, loading.firstChild);
			loading.appendChild(nahButton);
		}).then(function () {
			loading.classList.remove("question");
			loading.innerHTML = "Loading&hellip;";
		});
	}).then(function () {
		if ((window as any).Profiler) {
			console.debug("trying experimental js-profiler API...");
			try {
				SpyCards.Native.profiler = new ((window as any).Profiler)({
					sampleInterval: 1,
					maxBufferSize: 1e6,
				});
				new Promise<void>(function (resolve) {
					SpyCards.Native.profiler.addEventListener("samplebufferfull", resolve);
					setTimeout(resolve, 15 * 60 * 1000);
				}).then(function () {
					return SpyCards.Native.profiler.stop();
				}).then(function (profile: any) {
					console.log("profile:", profile);
				});
			} catch (ex) {
				console.error(ex);
			}
		}

		return SpyCards.Native.run();
	});
})();
