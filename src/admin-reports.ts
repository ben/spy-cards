function removeCardNulls(obj: any, isCard: boolean) {
	if (Array.isArray(obj)) {
		for (let c of obj) {
			removeCardNulls(c, isCard);
		}
		return;
	}

	if (typeof obj !== "object" || !obj) {
		return;
	}

	for (let key of Object.keys(obj)) {
		if (isCard && obj[key] === null) {
			delete obj[key];
			continue;
		}
		removeCardNulls(obj[key], isCard || key === "card");
	}

	return obj;
}
document.querySelectorAll(".state-data").forEach(function (el) {
	const obj = removeCardNulls(JSON.parse(el.textContent), false);
	const btn = document.createElement("button");
	btn.textContent = "Log to console";
	btn.addEventListener("click", function (e) {
		e.preventDefault();

		console.log(obj);
	});
	el.parentNode.insertBefore(btn, el);
	el.textContent = JSON.stringify(obj, null, "\t");
});
document.querySelectorAll(".dev-comment").forEach(function (el) {
	el.addEventListener("input", function () {
		el.parentElement.style.backgroundColor = "#ff0";
	});
});
