// in case of debugging emergency (browser can't handle error handler, etc.)
// go to game.html?alertError and crash report will be auto-submitted
if (location.search.indexOf("alertError") !== -1) {
	addEventListener("error", function (e) {
		alert("ERROR HANDLER: " + e.message + " at " + e.filename + ":" + e.lineno + ":" + e.colno);
	});
}

var matchmaking_server = "wss://spy-cards.lubar.me/spy-cards/ws";
var issue_report_handler = "https://spy-cards.lubar.me/spy-cards/report-issue";
var user_image_base_url = "https://spy-cards.lubar.me/spy-cards/user-img/";
var custom_card_api_base_url = "https://spy-cards.lubar.me/spy-cards/custom/api/";
var mode_changelog_base_url = "https://spy-cards.lubar.me/spy-cards/changelog/";
var match_recording_base_url = "https://spy-cards.lubar.me/spy-cards/recording/";
var arcade_api_base_url = "https://spy-cards.lubar.me/spy-cards/arcade/api/";
var ipfs_base_url = "https://spy-cards.lubar.me/ipfs/";
