module SpyCards.Audio {
	export let actx: AudioContext;
	if (window.AudioContext) {
		actx = new AudioContext();
	} else if ((window as any).webkitAudioContext) {
		actx = new (window as any).webkitAudioContext();
	}
	let soundsGain: GainNode;

	const t0 = actx && actx.currentTime;
	if (t0 !== t0) {
		actx = null;
		// give up. just give up. nothing will work. how is safari like this.
	}

	export var soundsVolume: number = 0;
	export var musicVolume: number = 0;

	export var startFetchAudio: () => void;
	const fetchAudioPromise = new Promise<void>((resolve) => startFetchAudio = actx ? resolve : () => { }).then(() => {
		const settings = loadSettings();
		soundsVolume = settings.audio.sounds;
		musicVolume = settings.audio.music;

		soundsGain = actx.createGain();
		soundsGain.connect(actx.destination);
		soundsGain.gain.setValueAtTime(soundsVolume, actx.currentTime);

		addEventListener("click", function () {
			if (actx.state === "suspended") {
				actx.resume();
			}
		}, true);
	});

	export function isEnabled(): Promise<boolean> {
		startFetchAudio();

		return fetchAudioPromise.then(function () {
			return soundsVolume > 0 || musicVolume > 0;
		});
	}

	export function setVolume(music: number, sounds: number) {
		soundsVolume = sounds;
		musicVolume = music;

		const settings = loadSettings();
		settings.audio.music = music;
		settings.audio.sounds = sounds;
		saveSettings(settings);

		if (soundsGain) {
			soundsGain.gain.setValueAtTime(sounds, actx.currentTime);
		}
	}

	addEventListener("spy-cards-settings-changed", () => {
		const s = loadSettings().audio;
		soundsVolume = s.sounds;
		musicVolume = s.music;
		if (soundsGain) {
			soundsGain.gain.setValueAtTime(soundsVolume, actx.currentTime);
		}
	});

	function fetchAudio(name: string): Promise<AudioBuffer> {
		return fetchAudioPromise.then(function () {
			return fetch(name.indexOf("/") === -1 ? "/audio/" + name + ".opus" : name);
		}).then(function (response) {
			return response.arrayBuffer();
		}).then(function (body) {
			return (actx.state === "suspended" ? actx.resume() : Promise.resolve()).then(function () {
				return body;
			})
		}).then(function (body) {
			if (actx.decodeAudioData.length > 1) {
				// Safari compat path
				return new Promise<AudioBuffer>(function(resolve, reject) {
					const w = new Worker("/script/opus-decoder-worker.js", {
						name: "opus-decoder"
					});
					w.onmessage = function(e) {
						w.terminate();

						if (e.data.config) {
							const abuf = actx.createBuffer(e.data.config.numberOfChannels, e.data.config.length, e.data.config.sampleRate);

							for (let i = 0; i < e.data.channels.length; i++) {
								abuf.getChannelData(i).set(new Float32Array(e.data.channels[i], 0), 0);
							}

							return resolve(abuf);
						}

						if (e.data.error) {
							return reject(new Error("audio: opusfile decoding error " + (-e.data.error)));
						}

						reject(new Error("audio: unknown error"));
					};
					w.postMessage({
						id: 0,
						name: name,
						buffer: body
					}, [body]);
				});
			}

			return actx.decodeAudioData(body);
		});
	}

	export class Sound {
		readonly name: string;
		readonly buffer: Promise<AudioBuffer>;
		readonly pitch: number;
		readonly volume: number;
		private gain: GainNode;

		constructor(name: string, pitch: number = 1, volume: number = 1) {
			this.name = name;
			this.buffer = fetchAudio(name);
			this.pitch = pitch;
			this.volume = volume;
		}

		createSource(): Promise<AudioBufferSourceNode> {
			startFetchAudio();

			const sound = this;

			return this.buffer.then(function (buffer) {
				const source = actx.createBufferSource();
				source.buffer = buffer;
				source.playbackRate.setValueAtTime(sound.pitch, actx.currentTime);
				return source;
			});
		}

		preload(): Promise<void> {
			startFetchAudio();

			return this.buffer.then(function () { });
		}

		play(delay: number = 0, overridePitch?: number, overrideVolume?: number): Promise<void> {
			if (!soundsVolume) {
				return Promise.resolve();
			}

			const sound = this;
			return this.createSource().then(function (src) {
				let dest = soundsGain;
				if (sound.volume !== 1) {
					if (!sound.gain) {
						sound.gain = actx.createGain();
						sound.gain.connect(dest);
						sound.gain.gain.setValueAtTime(sound.volume, actx.currentTime);
					}
					dest = sound.gain;
				}
				if (overrideVolume) {
					const gain = actx.createGain();
					gain.connect(dest);
					gain.gain.setValueAtTime(overrideVolume, actx.currentTime);
					dest = gain;
				}

				if (overridePitch) {
					src.playbackRate.setValueAtTime(overridePitch * sound.pitch, actx.currentTime);
				}
				src.connect(dest);
				src.start(actx.currentTime + delay);
			});
		}
	}

	export const Sounds = {
		Confirm: new Sound("Confirm"),
	};
}
