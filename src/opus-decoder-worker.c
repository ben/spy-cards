/*
emcc -Oz --no-entry -o opus-decoder-worker.wasm opus-decoder-worker.c \
	-isystem /opt/opus/local/include \
	-isystem /opt/opus/local/include/opus \
	-L /opt/opus/local/lib -lopusfile -lopus -logg \
	-s ALLOW_MEMORY_GROWTH=1 -s FILESYSTEM=0 \
	-s EXPORTED_FUNCTIONS='[_op_open_memory,_op_free,_op_read_float,_op_channel_count]'
*/

#include <opus/opusfile.h>

// there's not actually any code here, it's all in the compile command
