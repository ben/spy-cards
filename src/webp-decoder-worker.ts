module WebPDecoderWorker {
	let module: WebAssembly.Module;
	let instance: any;
	let resetTimeout: number = null;
	const importObject = {
		env: {
			emscripten_notify_memory_growth: function (index: number) { },
		},
	};
	if (!WebAssembly.instantiateStreaming) { // polyfill
		WebAssembly.instantiateStreaming = function (resp, importObject) {
			return Promise.resolve(resp).then(function (r) {
				return r.arrayBuffer();
			}).then(function (source) {
				return WebAssembly.instantiate(source, importObject);
			});
		};
	}
	const wasmReady = WebAssembly.instantiateStreaming(fetch("/webp-decoder-worker.wasm"), importObject).then(function (r) {
		module = r.module;
		instance = r.instance;

		instance.exports._initialize();
	});

	function waitReady(): Promise<void> {
		return wasmReady.then(function () {
			clearTimeout(resetTimeout);
			resetTimeout = setTimeout(function () {
				instance = null;
			}, 60000);

			if (instance === null) {
				return WebAssembly.instantiate(module, importObject).then(function (i) {
					(i.exports._initialize as () => void)();

					instance = i;
				});
			}

			return Promise.resolve();
		});
	}

	self.onmessage = function (e: MessageEvent) {
		const request = e.data;
		const rawData = new Uint8Array(request.buffer, 0);
		request.buffer = null;

		waitReady().then(function () {
			try {
				const inputDataPtr = instance.exports.malloc(rawData.length);

				try {
					new Uint8Array(instance.exports.memory.buffer, inputDataPtr, rawData.length).set(rawData, 0);

					const decodedDataPtr = instance.exports.decode_rgba(inputDataPtr, rawData.length, request.downscale, request.premultiply ? 1 : 0);
					try {
						const size = new Int32Array(instance.exports.memory.buffer, decodedDataPtr, 2);
						request.width = size[0];
						request.height = size[1];
						request.buffer = new Uint8Array(new Uint8Array(instance.exports.memory.buffer, decodedDataPtr + 8, request.width * request.height * 4)).buffer;
					} finally {
						instance.exports.free(decodedDataPtr);
					}
				} finally {
					instance.exports.free(inputDataPtr);
				}
			} finally {
				(self as any).postMessage(request, [request.buffer]);
			}
		});
	};
}
