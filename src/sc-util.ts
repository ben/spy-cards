var disableDoSquish = false;
function doSquish(el: ParentNode) {
	if (disableDoSquish) {
		return;
	}
	el.querySelectorAll<HTMLElement>(".squish").forEach((s) => {
		const max = parseInt(getComputedStyle(s.parentElement).width.replace("px", ""), 10);
		s.style.transform = "";
		const current = s.clientWidth;
		if (current > max) {
			s.style.transform = "scaleX(calc(" + max + " / " + current + "))";
		}
	});
	el.querySelectorAll<HTMLElement>(".squishy").forEach((s) => {
		const max = parseInt(getComputedStyle(s.parentElement).height.replace("px", ""), 10);
		s.style.transform = "";
		const current = s.clientHeight;
		if (current > max) {
			s.style.transform = "scaleY(calc(" + max + " / " + current + "))";
		}
	});
}

addEventListener("load", function () {
	doSquish(document);
});

function sleep(ms: number): Promise<void> {
	return new Promise(function (resolve) {
		setTimeout(function () {
			resolve();
		}, ms);
	});
}

module SpyCards {
	export interface Settings {
		audio: {
			music: number;
			sounds: number;
		};
		character?: string;

		disable3D?: boolean;
		disableCRT?: boolean;
		prefersReducedMotion?: boolean;
		noStandardSize?: boolean;
		alternateColors?: boolean;
		screenReader?: boolean;

		lastTermacadeOption?: number;
		lastTermacadeName?: string;

		autoUploadRecording?: boolean;
		displayTermacadeButtons?: boolean;

		theme?: "light" | "dark";
		dpiScale?: number;
		limitGPULevel?: 1 | 2 | 3;

		shadows?: boolean;
		cardFaceResolution?: number;

		controls?: {
			keyboard?: number;
			gamepad?: { [id: string]: number };
			customKB?: {
				name: string;
				code: {
					[btn: number]: string;
				};
			}[];
			customGP?: {
				name: string;
				button: {
					[btn: number]: number | [number, boolean];
				};
				style: ButtonStyle;
			}[];
		}
	}

	export function loadSettings(): Settings {
		if (localStorage["spy-cards-settings-v0"]) {
			return JSON.parse(localStorage["spy-cards-settings-v0"]);
		}

		const settings: Settings = {
			audio: {
				music: 0,
				sounds: 0
			},
			disable3D: true
		};

		let anyLegacy = false;
		if (localStorage["spy-cards-audio-settings-v0"]) {
			const legacyAudio: {
				enabled?: boolean;
				musicEnabled?: boolean;
				sfxEnabled?: boolean;
			} = JSON.parse(localStorage["spy-cards-audio-settings-v0"]);

			settings.audio.music = legacyAudio.enabled || legacyAudio.musicEnabled ? 0.6 : 0;
			settings.audio.sounds = legacyAudio.enabled || legacyAudio.sfxEnabled ? 0.6 : 0;
			anyLegacy = true;
		}

		if (localStorage["spy-cards-player-sprite-v0"]) {
			settings.character = localStorage["spy-cards-player-sprite-v0"];
			anyLegacy = true;
		}

		if (anyLegacy) {
			saveSettings(settings);
			delete localStorage["spy-cards-audio-settings-v0"];
			delete localStorage["spy-cards-player-sprite-v0"];
		}

		return settings;
	}

	export function saveSettings(settings: Settings) {
		localStorage["spy-cards-settings-v0"] = JSON.stringify(settings);

		if (navigator.serviceWorker && navigator.serviceWorker.controller) {
			navigator.serviceWorker.controller.postMessage({ type: "settings-changed" });
		}

		window.dispatchEvent(new Event("spy-cards-settings-changed"));
	}
}

(function () {
	function setColorScheme() {
		const setting = SpyCards.loadSettings().theme;
		let light = setting === "light";
		if (!setting && window.matchMedia) {
			light = window.matchMedia("(prefers-color-scheme: light)").matches;
		}

		document.documentElement.classList.toggle("use-light-theme", light);
		const themeColor = document.querySelector("meta[name=\"theme-color\"]");
		if (themeColor) {
			themeColor.setAttribute("content", light ? "#acc" : "#023");
		}
	}

	window.addEventListener("spy-cards-settings-changed", setColorScheme);
	const prefersLight = window.matchMedia("(prefers-color-scheme: light)");
	if (prefersLight && prefersLight.addEventListener) {
		prefersLight.addEventListener("change", setColorScheme);
	}
	setColorScheme();
})();
