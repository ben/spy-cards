module OpusDecoderWorker {
	let module: WebAssembly.Module;
	let instance: any;
	let resetTimeout: number = null;
	const importObject = {
		env: {
			emscripten_notify_memory_growth: function (index: number) { },
		},
	};
	if (!WebAssembly.instantiateStreaming) { // polyfill
		WebAssembly.instantiateStreaming = function (resp, importObject) {
			return Promise.resolve(resp).then(function (r) {
				return r.arrayBuffer();
			}).then(function (source) {
				return WebAssembly.instantiate(source, importObject);
			});
		};
	}
	const wasmReady = WebAssembly.instantiateStreaming(fetch("/opus-decoder-worker.wasm"), importObject).then(function (r) {
		module = r.module;
		instance = r.instance;

		instance.exports._initialize();
	});

	function waitReady(): Promise<void> {
		return wasmReady.then(function () {
			clearTimeout(resetTimeout);
			resetTimeout = setTimeout(function () {
				instance = null;
			}, 60000);

			if (instance === null) {
				return WebAssembly.instantiate(module, importObject).then(function (i) {
					(i.exports._initialize as () => void)();

					instance = i;
				});
			}

			return Promise.resolve();
		});
	}

	self.onmessage = function (e: MessageEvent) {
		const request = e.data;
		const rawData = new Uint8Array(request.buffer, 0);
		request.buffer = null;

		waitReady().then(function () {

			try {
				const pcmBufferLength = 5760 * 2;
				const inputBufferPtr = instance.exports.malloc(rawData.length);
				const outputBufferPtr = instance.exports.malloc(4 + pcmBufferLength * 4);
				try {
					new Uint8Array(instance.exports.memory.buffer, inputBufferPtr, rawData.length).set(rawData, 0);

					const opusFile = instance.exports.op_open_memory(inputBufferPtr, rawData.length, outputBufferPtr);
					if (!opusFile) {
						request.error = new Int32Array(instance.exports.memory.buffer, outputBufferPtr, 1)[0];
						return;
					}

					const pcmBuffers: Float32Array[] = [];

					try {
						let read = instance.exports.op_read_float(opusFile, outputBufferPtr + 4, pcmBufferLength, outputBufferPtr);
						if (read < 0) {
							request.error = read;
							return;
						}

						const numChannels = instance.exports.op_channel_count(opusFile, new Int32Array(instance.exports.memory.buffer, outputBufferPtr, 1)[0]);

						while (read !== 0) {
							pcmBuffers.push(new Float32Array(new Float32Array(instance.exports.memory.buffer, outputBufferPtr + 4, read * numChannels)));

							read = instance.exports.op_read_float(opusFile, outputBufferPtr + 4, pcmBufferLength, outputBufferPtr);
							if (read < 0) {
								request.error = read;
								return;
							}
						}

						const combinedPCM = new Float32Array(pcmBuffers.reduce((prev, buf) => prev + buf.length, 0));
						let offset = 0;
						pcmBuffers.forEach(buf => {
							combinedPCM.set(buf, offset);
							offset += buf.length;
						});

						request.config = {
							length: combinedPCM.length / numChannels,
							numberOfChannels: numChannels,
							sampleRate: 48000,
						};
						request.channels = [];

						for (let channel = 0; channel < numChannels; channel++) {
							const channelBuf = new Float32Array(combinedPCM.length / numChannels);
							for (let i = 0, j = channel; i < channelBuf.length; i++, j += numChannels) {
								channelBuf[i] = combinedPCM[j];
							}
							request.channels[channel] = channelBuf.buffer;
						}
					} finally {
						instance.exports.op_free(opusFile);
					}
				} finally {
					instance.exports.free(inputBufferPtr);
					instance.exports.free(outputBufferPtr);
				}
			} finally {
				(self as any).postMessage(request, request.channels);
			}
		});
	};
}
