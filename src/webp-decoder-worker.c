/*
emcc -Oz --no-entry -o webp-decoder-worker.wasm webp-decoder-worker.c \
	-isystem /opt/webp/local/include \
	-L /opt/webp/local/lib -lwebp \
	-s ALLOW_MEMORY_GROWTH=1 -s FILESYSTEM=0 \
	-s EXPORTED_FUNCTIONS='[_decode_rgba]'
*/

#include <stdlib.h>
#include <string.h>
#include <webp/decode.h>

void *decode_rgba(uint8_t *data, size_t data_size, int downscale_level, int premultiply)
{
	WebPDecoderConfig config;

	WebPInitDecoderConfig(&config);
	WebPGetFeatures(data, data_size, &config.input);

	config.output.colorspace = premultiply ? MODE_rgbA : MODE_RGBA;

	if (downscale_level)
	{
		config.options.scaled_width = config.input.width;
		config.options.scaled_height = config.input.height;

		if (config.input.width > 2048 || config.input.height > 2048)
		{
			config.options.use_scaling = 1;
			config.options.scaled_width >>= 1;
			config.options.scaled_height >>= 1;
		}

		if (downscale_level > 1)
		{
			while (config.options.scaled_width > 1024 || config.options.scaled_height > 1024)
			{
				config.options.use_scaling = 1;
				config.options.scaled_width >>= 1;
				config.options.scaled_height >>= 1;
			}
		}
	}

	WebPDecode(data, data_size, &config);

	void *output = malloc(8 + config.output.u.RGBA.size);
	*(int *)output = config.output.width;
	*((int *)output + 1) = config.output.height;
	memcpy((void *)((char *)output + 8), config.output.u.RGBA.rgba, config.output.u.RGBA.size);

	WebPFreeDecBuffer(&config.output);

	return output;
}
