const spyCardsVersionPrefix = "0.3.27";

const spyCardsClientMode = location.pathname === "/game.html" ? new URLSearchParams(location.search).get("mode") : "";

const spyCardsVersionSuffix = (function () {
	const path = location.pathname.substr(1);
	if (spyCardsClientMode) {
		return "-" + spyCardsClientMode;
	}
	if (path.substr(path.length - ".html".length) === ".html") {
		return "-" + path.substr(0, path.length - 5);
	}
	return "";
})();

const spyCardsVersion = spyCardsVersionPrefix + spyCardsVersionSuffix;
var spyCardsVersionVariableSuffix: string = "";
