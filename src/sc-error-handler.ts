addEventListener("error", function (e) {
	if (SpyCards.disableErrorHandler) {
		return;
	}

	if (e.error && e.error.message === "Go program has already exited") {
		// don't mask real error
		return;
	}

	if (!e.error && e.target instanceof HTMLScriptElement) {
		const scriptName = e.target.src.replace(/^[^\?]*\/|\?.*$/g, "");
		console.debug("TODO: error screen for script that failed to load:", scriptName);
	} else if (e.error) {
		errorHandler(e.error);
	} else if (e.message && e.lineno) {
		errorHandler(new Error(e.message + " at " + e.filename + ":" + e.lineno + ":" + e.colno));
	} else {
		console.error("unhandled type of ErrorEvent", e);
		debugger;
	}
}, true);

module SpyCards {
	// prevent keyboard inputs from being eaten
	export var disableKeyboard = false;

	export var disableErrorHandler = false;

	export var serviceWorkerVersion: string = "";
	fetch("/service-worker-status").then((r) => r.text()).then((v) => serviceWorkerVersion = v);

	export var errLogBuf: string[] = [];

	export var crashCleanup: (() => void)[] = [];
}

function errorHandler(ex: Error) {
	SpyCards.crashCleanup.filter(function (f) {
		try {
			f();
		} catch (ex) {
			debugger;
		}

		return false;
	});

	console.error("Error handler triggered:", ex, (ex as any).goStack || ex.stack);
	debugger;

	if (document.documentElement.classList.contains("fatal-error")) {
		return;
	}

	SpyCards.disableKeyboard = true;

	const form = document.createElement("form");
	form.classList.add("error-report");

	const h1 = document.createElement("h1");
	h1.textContent = "Uh oh!";
	form.appendChild(h1);

	const flavor = document.createElement("p");
	flavor.classList.add("flavor");
	flavor.textContent = "It looks like Spy Cards Online crashed due to a bug! But that's impossible because Spy Cards Online contains no... oh.";
	form.appendChild(flavor);

	const message = document.createElement("p");
	message.classList.add("message");
	if (/^PANIC:/.test(ex.message)) {
		message.textContent = ex.message;
	} else {
		message.textContent = "Error: " + ex.message;
	}
	form.appendChild(message);

	const h2 = document.createElement("h2");
	h2.textContent = "Report Error";
	form.appendChild(h2);

	const commentField = document.createElement("textarea");
	commentField.name = "u";
	const commentLabel = document.createElement("label");
	commentLabel.textContent = "What happened right before the crash?";
	commentLabel.appendChild(commentField);
	form.appendChild(commentLabel);

	const submit = document.createElement("button");
	submit.type = "submit";
	submit.textContent = "Submit Report";
	form.appendChild(submit);

	const disclaimer = document.createElement("p");
	disclaimer.classList.add("disclaimer");
	disclaimer.textContent = "Submitting this form will send the following data:";
	form.appendChild(disclaimer);

	const dataList = document.createElement("ul");
	dataList.classList.add("disclaimer");
	function addItem(text: string) {
		const item = document.createElement("li");
		item.textContent = text;
		dataList.appendChild(item);
	}
	addItem("The version of Spy Cards Online (" + spyCardsVersion + ")");
	addItem("The error message (" + ex.message + ")");
	addItem("The location in the code where the error occurred");
	if (location.hash && location.hash.length > 1) {
		addItem("Custom card definitions (from the address bar)");
	}
	if (SpyCards.errLogBuf.length) {
		addItem("The last " + SpyCards.errLogBuf.length + " lines from the game log");
	}
	addItem("Your comment entered above (if any)");
	form.appendChild(dataList);

	const isAlertError = location.search.indexOf("alertError") !== -1;
	const onSubmit = function (e: Event) {
		e.preventDefault();

		submit.disabled = true;
		submit.textContent = "Sending...";

		const data = new FormData(form);
		data.append("v", spyCardsVersion);
		data.append("v2", SpyCards.serviceWorkerVersion);
		data.append("m", ex.message);
		data.append("t", (ex as any).goStack || ex.stack);
		data.append("s", JSON.stringify({
			h: location.host,
			p: location.pathname,
			s: location.search,
		}));
		data.append("c", location.hash ? location.hash.substr(1) : "");
		data.append("l", SpyCards.errLogBuf.join("\n"));

		function closeForm(message: string) {
			form.textContent = "";
			const thanks = document.createElement("p");
			thanks.classList.add("thanks");
			thanks.textContent = message;
			form.appendChild(thanks);
		}

		const xhr = new XMLHttpRequest();
		xhr.open("POST", issue_report_handler, !isAlertError);
		xhr.addEventListener("load", function () {
			if (xhr.status === 202) {
				closeForm("Report submitted. Thanks for helping to make Spy Cards Online a little less buggy. Or more buggy.");
				return;
			}
			closeForm("Form submission failed due to an error. Oh, the irony! (remote code " + xhr.status + ")");
		});
		xhr.addEventListener("error", function () {
			closeForm("Form submission failed due to an error. Oh, the irony! (local code " + xhr.status + ")");
		});
		xhr.send(data);
	};
	form.addEventListener("submit", onSubmit);
	if (isAlertError) {
		onSubmit(new Event("submit"));
		alert((ex as any).goStack || ex.stack);
	}

	document.documentElement.classList.add("fatal-error");
	document.body.appendChild(form);
	document.scrollingElement.scrollTop = 0;
}

if ("serviceWorker" in navigator) {
	if (navigator.serviceWorker.controller) {
		navigator.serviceWorker.addEventListener("controllerchange", () => {
			const updateDialog = document.createElement("div");
			updateDialog.classList.add("update-available");
			updateDialog.setAttribute("role", "alertdialog");

			const updateTitle = document.createElement("h1");
			updateTitle.id = "service-worker-update-available-title";
			updateDialog.setAttribute("aria-labelledby", updateTitle.id);
			updateTitle.textContent = "Update Available";
			updateDialog.appendChild(updateTitle);

			const updateMessage = document.createElement("p");
			updateMessage.id = "service-worker-update-available-message";
			updateDialog.setAttribute("aria-describedby", updateMessage.id);
			updateMessage.textContent = "An update to Spy Cards Online is available.";
			updateDialog.appendChild(updateMessage);

			const updateAccept = document.createElement("button");
			updateAccept.textContent = "Apply Now";
			updateAccept.addEventListener("click", (e) => {
				e.preventDefault();

				location.reload();
			});
			updateDialog.appendChild(updateAccept);

			const updateDecline = document.createElement("button");
			updateDecline.textContent = "Ignore";
			updateDecline.addEventListener("click", (e) => {
				e.preventDefault();

				if (updateDialog.parentNode) {
					updateDialog.parentNode.removeChild(updateDialog);
				}
			});
			updateDialog.appendChild(updateDecline);

			document.querySelectorAll(".update-available").forEach((el) => {
				if (el.parentNode) {
					el.parentNode.removeChild(el);
				}
			});

			const showUpdateDialog = () => {
				document.body.appendChild(updateDialog);
				updateAccept.focus();
			};

			if (document.documentElement.classList.contains("active")) {
				const matchWaiter = setInterval(() => {
					if (document.documentElement.classList.contains("active")) {
						return;
					}

					clearInterval(matchWaiter);
					showUpdateDialog();
				}, 1000);
			} else {
				showUpdateDialog();
			}
		});
	}

	const hadController = !!navigator.serviceWorker.controller;
	const registerSW = function () {
		navigator.serviceWorker.register("/service-worker.js").then((reg) => {
			reg.addEventListener("updatefound", (e) => {
				console.log("serviceworker update available");
				if (!hadController) {
					// we didn't load via a serviceworker, so we don't need to tell the user
					return;
				}

				const updateDialog = document.createElement("div");
				updateDialog.classList.add("update-available");
				updateDialog.setAttribute("role", "alert");

				const updateTitle = document.createElement("h1");
				updateTitle.id = "service-worker-update-found-title";
				updateDialog.setAttribute("aria-labelledby", updateTitle.id);
				updateTitle.textContent = "Update Found";
				updateDialog.appendChild(updateTitle);

				const updateMessage = document.createElement("p");
				updateMessage.id = "service-worker-update-found-message";
				updateDialog.setAttribute("aria-describedby", updateMessage.id);
				updateMessage.textContent = "An update to Spy Cards Online is being downloaded…";
				updateDialog.appendChild(updateMessage);

				const progress = document.createElement("progress");
				progress.value = 0;
				progress.max = 1;
				updateDialog.appendChild(progress);

				document.body.appendChild(updateDialog);
			});

			let sw: ServiceWorker;
			if (reg.installing) {
				sw = reg.installing;
			} else if (reg.waiting) {
				sw = reg.waiting;
			} else if (reg.active) {
				sw = reg.active;
			}

			if (sw) {
				console.log("serviceworker initial state", sw.state);
				sw.addEventListener("statechange", (e) => {
					console.log("serviceworker state change", sw.state);
				});
			}
		}).catch((err) => {
			console.info("registering ServiceWorker rejected:", err);
		});
	}

	if (location.pathname.indexOf("/game") === 0 || !window.requestIdleCallback) {
		registerSW();
	} else {
		requestIdleCallback(registerSW);
	}

	navigator.serviceWorker.addEventListener("message", (e) => {
		switch (e.data.type) {
			case "settings-changed":
				window.dispatchEvent(new Event("spy-cards-settings-changed"));
				break;
			case "update-progress":
				const progress = document.querySelector<HTMLProgressElement>(".update-available progress");
				if (progress) {
					progress.value = e.data.current;
					progress.max = e.data.total;
				}

				break;
			case "update-error":
				console.error("service worker update failed with error: " + e.data.message);

				document.querySelectorAll(".update-available").forEach((el) => {
					if (el.parentNode) {
						el.parentNode.removeChild(el);
					}
				});

				break;
			default:
				debugger;
				break;
		}
	});
	if (navigator.serviceWorker.startMessages) {
		navigator.serviceWorker.startMessages();
	}
}
