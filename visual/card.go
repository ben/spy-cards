//go:build !headless
// +build !headless

package visual

import (
	"context"
	"log"
	"math"
	"math/bits"
	"strings"
	"time"

	"git.lubar.me/ben/spy-cards/card"
	"git.lubar.me/ben/spy-cards/format"
	"git.lubar.me/ben/spy-cards/gfx"
	"git.lubar.me/ben/spy-cards/gfx/sprites"
	"git.lubar.me/ben/spy-cards/gui"
	"git.lubar.me/ben/spy-cards/internal"
	"git.lubar.me/ben/spy-cards/internal/router"
	"git.lubar.me/ben/spy-cards/match"
	"git.lubar.me/ben/spy-cards/spoilerguard"
)

const (
	cardHoverInSpeed  = 30
	cardHoverOutSpeed = 20
	deckHoverInSpeed  = 60
	deckHoverOutSpeed = 25
	cardNearThreshold = 3
	flipSpeed         = 20
	cardDepth         = 0.05
	portraitDepth     = 0.1
	coinDistance      = 0.5
	coinDepth         = 0.025
)

var (
	cardWidth  = sprites.CardFront.X1 - sprites.CardFront.X0
	cardHeight = sprites.CardFront.Y1 - sprites.CardFront.Y0

	effectHighlightColor = sprites.Color{R: 68, G: 170, B: 255, A: 255}
)

type Card struct {
	*match.Card
	Active *match.ActiveCard
	ModTP  card.Number

	noWindow       bool
	noFancyBorder  bool
	peeled         bool
	banned         bool
	unpickable     bool
	flip           uint8
	flipDir        int8
	cachedDef      *card.Def
	cachedDesc     *card.RichDescription
	portraitTex    *gfx.AssetTexture
	portraitSprite *sprites.Sprite
	hover          *gui.HoverTilt
	coins          []*coin
	batchSnapshot  *sprites.BatchSnapshot
}

type coin struct {
	dx, dy         float32
	invisibleTimer uint64
	revealTimer    uint64
	stat, heads    bool
}

var cardBack = [...]*sprites.Sprite{
	card.Attacker: sprites.BackAttacker,
	card.Effect:   sprites.BackEffect,
	card.MiniBoss: sprites.BackMiniBoss,
	card.Boss:     sprites.BackBoss,
	card.Token:    sprites.BackToken,
	card.Enemy:    sprites.BackAttacker,
}

func init() {
	if _, m, d := time.Now().Date(); (m == time.October && d == 31) || router.FlagSpookyEnemyCards.IsSet() {
		cardBack[card.Enemy] = sprites.BackEffect
	}
}

var cardFront = [...]*sprites.Sprite{
	card.Attacker: sprites.FrontAttacker,
	card.Effect:   sprites.FrontEffect,
	card.MiniBoss: sprites.FrontMiniBoss,
	card.Boss:     sprites.FrontBoss,
	card.Token:    sprites.FrontToken,
}

var cardColorVanilla = [...]sprites.Color{
	card.Attacker: {R: 0, G: 178, B: 0, A: 255},
	card.Effect:   {R: 255, G: 117, B: 2, A: 255},
	card.MiniBoss: {R: 127, G: 127, B: 127, A: 255},
	card.Boss:     {R: 255, G: 164, B: 2, A: 255},
	card.Token:    {R: 69, G: 96, B: 130, A: 255},
}

var cardColorAlternate = [...]sprites.Color{
	card.Attacker: {R: 0, G: 178, B: 0, A: 255},
	card.Effect:   {R: 255, G: 245, B: 190, A: 255},
	card.MiniBoss: {R: 95, G: 95, B: 95, A: 255},
	card.Boss:     {R: 123, G: 0, B: 71, A: 255},
	card.Token:    {R: 69, G: 96, B: 130, A: 255},
}

var cardColor = cardColorVanilla

func init() {
	if internal.LoadSettings().AlternateColors || router.FlagAltColors.IsSet() {
		cardColor = cardColorAlternate
	}

	internal.OnSettingsChanged = append(internal.OnSettingsChanged, func(s *internal.Settings) {
		if s.AlternateColors || router.FlagAltColors.IsSet() {
			cardColor = cardColorAlternate
		} else {
			cardColor = cardColorVanilla
		}
	})
}

type cardRenderHelper struct {
	batch          *sprites.Batch
	flags          sprites.RenderFlag
	flip           float32
	flipXX, flipZX float32
	flipXZ, flipZZ float32
}

func (rh *cardRenderHelper) drawText(font sprites.FontID, text string, x, y, z, sx, sy, maxWidth float32, tint sprites.Color, extraFlags sprites.RenderFlag, centered bool) {
	width := float32(0.0)

	for _, letter := range text {
		width += sprites.TextAdvance(font, letter, sx)
	}

	if width > maxWidth {
		sx *= maxWidth / width
		width = maxWidth
	}

	if centered {
		x -= width / 2
	}

	for _, letter := range text {
		sprites.DrawLetter(rh.batch, font, letter, rh.flipXX*x+rh.flipZX*z, y, rh.flipXZ*x+rh.flipZZ*z, sx, sy, tint, rh.flags|extraFlags, 0, rh.flip, 0)
		x += sprites.TextAdvance(font, letter, sx)
	}
}

func (rh *cardRenderHelper) drawTextBorder(font sprites.FontID, text string, x, y, z, sx, sy, maxWidth float32, textTint, borderTint sprites.Color, extraFlags sprites.RenderFlag, centered bool) { //nolint:unparam
	if borderTint == sprites.Black {
		rh.drawText(font, text, x, y, z, sx, sy, maxWidth, textTint, extraFlags|sprites.FlagBorder|sprites.FlagIsMTSDF, centered)
	} else {
		rh.drawText(font, text, x, y, z, sx, sy, maxWidth, borderTint, extraFlags|sprites.FlagBorder, centered)
		rh.drawText(font, text, x, y, z, sx, sy, maxWidth, textTint, extraFlags|sprites.FlagIsMTSDF, centered)
	}
}

func (c *Card) renderHelper(batch *sprites.Batch) *cardRenderHelper {
	rh := &cardRenderHelper{batch: batch, flags: sprites.FlagNoDiscard}

	rh.flip = -float32(c.flip) / 255 * math.Pi
	flipS, flipC := math.Sincos(float64(rh.flip))
	rh.flipXX, rh.flipZX = float32(flipC), -float32(flipS)
	rh.flipXZ, rh.flipZZ = float32(flipS), float32(flipC)

	if c.Active != nil && c.Active.Mode.Setup() {
		rh.flags |= sprites.FlagHalfGlow
	}

	return rh
}

// Render draws a card to the screen.
func (c *Card) Render(ctx context.Context, batch *sprites.Batch, highlightEffect *card.EffectDef) {
	if c.Def == nil {
		c.flip = 255
	}

	if c.Active != nil && c.Active.Mode == match.ModeNumb {
		c.SetFlip(true)
	}

	if router.FlagNoWindows.IsSet() {
		c.noWindow = true
	}

	if router.FlagNoFancyBorders.IsSet() {
		c.noFancyBorder = true
	}

	if router.FlagPeelMyCards.IsSet() {
		c.peeled = true
	}

	if c.Def == nil || c.Def != c.cachedDef {
		c.cachedDef = c.Def
		c.cachedDesc = nil
		c.portraitSprite = nil
		c.portraitTex = nil
		c.batchSnapshot = nil
		c.banned = false
		c.unpickable = false
	}

	if c.Def != nil && c.cachedDesc == nil {
		if c.Active != nil {
			c.cachedDesc = c.Active.Desc

			if c.cachedDesc == nil {
				log.Println("WARNING: missing card description for", c.Active.Mode, c.Def.DisplayName())
			}
		} else {
			c.cachedDesc = c.Def.Description(c.Set)
		}

		c.banned = false
		c.unpickable = false

		if c.Def.Rank == card.Token && c.Def.TP == 0 && c.ModTP == card.Num(0) {
			c.banned = true
			c.unpickable = true
		} else {
			for _, f := range c.Set.Mode.GetAll(card.FieldBannedCards) {
				bc := f.(*card.BannedCards)

				listed := false

				if len(bc.Cards) == 0 {
					listed = c.Def.ID < 128
				} else {
					for _, id := range bc.Cards {
						if c.Def.ID == id {
							listed = true

							break
						}
					}
				}

				if listed {
					switch bc.Flags & card.BannedCardTypeMask {
					case card.BannedCardTypeBanned:
						c.banned = true
					case card.BannedCardTypeUnpickable:
						c.unpickable = true
					}
				}
			}
		}

		c.batchSnapshot = nil
	}

	rh := c.renderHelper(batch)

	if c.flip >= 128 {
		c.renderOverlays(batch, sprites.White)
	}

	var finishSnapshot func() *sprites.BatchSnapshot

	canUseSnapshot := c.flip == 0 && (c.Active == nil || c.Active.ActiveEffect == nil) && (c.hover == nil || c.hover.Falloff >= 1000)

	if !canUseSnapshot || !c.batchSnapshot.Replay(batch) {
		if canUseSnapshot {
			finishSnapshot = batch.Record()
		}

		batch.AppendEx(cardBack[c.Back], rh.flipZX*cardDepth, 0, rh.flipZZ*cardDepth, 1, 1, sprites.White, rh.flags, 0, rh.flip+math.Pi, 0)

		if c.Def != nil {
			cost := card.Num(c.Def.TP)
			cost.Add(c.ModTP, false) // TP can't start as infinity, so subtraction flag doesn't matter

			batch.AppendEx(sprites.CardFront, 0, 0, 0, 1, 1, cardColor[c.Def.Rank], rh.flags, 0, rh.flip, 0)

			c.initPortraitSprite(ctx)

			if c.portraitSprite != nil {
				batch.AppendEx(c.portraitSprite, rh.flipZX*-portraitDepth, 0.45, rh.flipZZ*-portraitDepth, 1, 1, sprites.White, sprites.FlagNoDiscard|rh.flags, 0, rh.flip, 0)
			}

			if !c.noWindow {
				batch.AppendEx(sprites.CardFrontWindow, 0, 0, 0, 1, 1, cardColor[c.Def.Rank], rh.flags, 0, rh.flip, 0)
			}

			if !c.noFancyBorder {
				batch.AppendEx(cardFront[c.Def.Rank], rh.flipXX*0.83, 0, rh.flipXZ*0.83, 1, 1, cardColor[c.Def.Rank], rh.flags, 0, rh.flip, 0)
				batch.AppendEx(cardFront[c.Def.Rank], rh.flipXX*-0.83, 0, rh.flipXZ*-0.83, -1, 1, cardColor[c.Def.Rank], rh.flags, 0, rh.flip, 0)
			}

			if !c.peeled {
				batch.AppendEx(sprites.WhiteBar.Crop(0, 0, 0.1, 0), 0, 1.3, 0, 0.37, 0.6, sprites.White, rh.flags, 0, rh.flip, 0)

				nameWidth := float32(1.65)

				switch {
				case c.Active != nil && c.Active.Mode == match.ModeStackedToken:
					batch.AppendEx(sprites.WhiteBar.Crop(0.7, 0, 0, 0), 0, 1.3, 0, 0.37, 0.6, sprites.LGray, rh.flags, 0, rh.flip, 0)
					batch.AppendEx(sprites.WhiteBar.Crop(0.95, 0, 0, 0), rh.flipXX*-0.55, 1.3, rh.flipXZ*-0.55, 0.37, 0.6, sprites.White, rh.flags, 0, rh.flip, 0)

					rh.drawTextBorder(sprites.FontD3Streetism, "x"+cost.String(), 0.8, 1.175, cardDepth/2, 0.4, 0.5, 0.6, sprites.White, sprites.Black, 0, true)

					nameWidth = 1.4
				case cost.AmountInf > 0:
					batch.AppendEx(sprites.TPInf, rh.flipXX*0.9+rh.flipZX*cardDepth/2, 1.3, rh.flipXZ*0.9+rh.flipZZ*cardDepth/2, 0.4, 0.4, sprites.White, rh.flags, 0, rh.flip, 0)
				case c.unpickable && c.banned:
					batch.AppendEx(sprites.WhiteBar.Crop(0.9, 0, 0, 0), 0, 1.3, 0, 0.37, 0.6, sprites.White, rh.flags, 0, rh.flip, 0)

					nameWidth = 1.95
				default:
					batch.AppendEx(sprites.TP, rh.flipXX*0.9+rh.flipZX*cardDepth/2, 1.3, rh.flipXZ*0.9+rh.flipZZ*cardDepth/2, 0.4, 0.4, sprites.White, rh.flags, 0, rh.flip, 0)

					tpColor := sprites.White
					if cost.Less(card.Number{}) {
						tpColor = sprites.Green
					}

					rh.drawTextBorder(sprites.FontD3Streetism, cost.String(), 0.9, 1.175, cardDepth/2, 0.4, 0.5, 0.4, tpColor, sprites.Black, 0, true)
				}

				rh.drawTextBorder(sprites.FontBubblegumSans, c.Def.DisplayName(), -1, 1.175, 0, 0.5, 0.5, nameWidth, sprites.Black, sprites.White, 0, false)

				batch.AppendEx(sprites.Description, 0, -0.825, 0, 0.8, 0.75, sprites.White, sprites.FlagNoDiscard|rh.flags, 0, rh.flip, 0)

				switch {
				case len(c.Def.Effects) == 1 && c.Def.Effects[0] != nil && c.Def.Effects[0].Type == card.EffectStat:
					// simple (1-stat)
					sprite := sprites.ATK
					if c.Def.Effects[0].Flags&card.FlagStatDEF != 0 {
						sprite = sprites.DEF
					}

					batch.AppendEx(sprite, rh.flipXX*-0.35+rh.flipZX*-cardDepth, -0.8, rh.flipXZ*-0.35+rh.flipZZ*-cardDepth, 0.45, 0.45, sprites.White, rh.flags, 0, rh.flip, 0)

					stat := card.Num(c.Def.Effects[0].Amount)
					if c.Def.Effects[0].Flags&card.FlagStatInfinity != 0 {
						stat.Amount, stat.AmountInf = 0, stat.Amount
					}

					statColor, statBorder := sprites.White, sprites.Black
					if stat.Less(card.Number{}) {
						statColor = sprites.Red
					}

					if highlightEffect == c.Def.Effects[0] {
						statBorder = effectHighlightColor
					}

					rh.drawTextBorder(sprites.FontD3Streetism, stat.String(), 0.5, -1.1125, -cardDepth/4, 1, 1.25, 1, statColor, statBorder, 0, true)
				case len(c.Def.Effects) == 2 && c.Def.Effects[0] != nil && c.Def.Effects[1] != nil && c.Def.Effects[0].Type == card.EffectStat && c.Def.Effects[1].Type == card.EffectStat && c.Def.Effects[0].Flags&card.FlagStatDEF == 0 && c.Def.Effects[1].Flags&card.FlagStatDEF != 0:
					// simple (atk-def)
					batch.AppendEx(sprites.ATK, rh.flipXX*-0.4+rh.flipZX*-cardDepth, -0.8, rh.flipXZ*-0.4+rh.flipZZ*-cardDepth, 0.45, 0.45, sprites.White, rh.flags, 0, rh.flip, 0)
					batch.AppendEx(sprites.DEF, rh.flipXX*0.5+rh.flipZX*-cardDepth, -0.8, rh.flipXZ*0.5+rh.flipZZ*-cardDepth, 0.45, 0.45, sprites.White, rh.flags, 0, rh.flip, 0)

					atk := card.Num(c.Def.Effects[0].Amount)
					if c.Def.Effects[0].Flags&card.FlagStatInfinity != 0 {
						atk.Amount, atk.AmountInf = 0, atk.Amount
					}

					def := card.Num(c.Def.Effects[1].Amount)
					if c.Def.Effects[1].Flags&card.FlagStatInfinity != 0 {
						def.Amount, def.AmountInf = 0, def.Amount
					}

					atkColor, defColor := sprites.White, sprites.White
					atkBorder, defBorder := sprites.Black, sprites.Black

					if atk.Less(card.Number{}) {
						atkColor = sprites.Red
					}

					if def.Less(card.Number{}) {
						defColor = sprites.Red
					}

					if highlightEffect == c.Def.Effects[0] {
						atkBorder = effectHighlightColor
					}

					if highlightEffect == c.Def.Effects[1] {
						defBorder = effectHighlightColor
					}

					rh.drawTextBorder(sprites.FontD3Streetism, atk.String(), -0.25, -1.1125, -cardDepth/4, 1, 1.25, 1, atkColor, atkBorder, 0, true)
					rh.drawTextBorder(sprites.FontD3Streetism, def.String(), 0.65, -1.1125, -cardDepth/4, 1, 1.25, 1, defColor, defBorder, 0, true)
				default:
					c.cachedDesc.Typeset(sprites.FontBubblegumSans, -0.9, -0.5, 0, 0.3, 0.375, 1.85, 0.95, highlightEffect, false, func(x, y, z, sx, sy float32, text string, tint sprites.Color, isEffect bool) {
						if highlightEffect != nil && isEffect {
							rh.drawText(sprites.FontBubblegumSans, text, x, y, z, sx, sy, float32(math.Inf(1)), effectHighlightColor, sprites.FlagBorder, false)
						}

						rh.drawText(sprites.FontBubblegumSans, text, x, y, z, sx, sy, float32(math.Inf(1)), tint, sprites.FlagIsMTSDF, false)
					})
				}

				visibleTribes := 0
				wideTribe := false

				for _, t := range c.Def.Tribes {
					if t.Tribe == card.TribeDeadLander || t.Tribe == card.TribeWideCustom {
						wideTribe = true
					}

					if t.Tribe != card.TribeCustom || !strings.HasPrefix(t.CustomName, "_") {
						visibleTribes++
					}
				}

				var tribeScale float32

				tribeSprite := sprites.TribeBubble
				tribeWide := float32(1.0)

				switch {
				case visibleTribes > 2:
					tribeScale = 2.0 / float32(visibleTribes)
				case visibleTribes == 1 && wideTribe:
					tribeScale = 1.0
					tribeWide = 2.0
					tribeSprite = sprites.TribeBubbleWide
				default:
					tribeScale = 1.0
				}

				tribeIndex := 0

				for _, t := range c.Def.Tribes {
					if t.Tribe == card.TribeCustom && strings.HasPrefix(t.CustomName, "_") {
						continue
					}

					tx := ((float32(tribeIndex)+0.5)/float32(visibleTribes) - 0.5) * 2.2

					batch.AppendEx(tribeSprite, rh.flipXX*tx+rh.flipZX*cardDepth/2, -1.4, rh.flipXZ*tx+rh.flipZZ*cardDepth/2, 0.65*tribeScale, 0.65, t.Color(), rh.flags, 0, rh.flip, 0)

					sx := float32(0.3)

					if t.Tribe == card.TribeDeadLander {
						sx = 1
					}

					rh.drawTextBorder(sprites.FontD3Streetism, t.Name(), tx, -1.475, cardDepth/2, sx, 0.3, 0.9*tribeScale*tribeWide, sprites.White, sprites.Black, 0, true)

					tribeIndex++
				}
			}
		}

		if finishSnapshot != nil {
			c.batchSnapshot = finishSnapshot()
		}
	}

	if c.flip < 128 {
		c.renderOverlays(batch, sprites.White)
	}
}

func (c *Card) initPortraitSprite(ctx context.Context) {
	if c.portraitSprite != nil {
		return
	}

	portrait, custom := spoilerguard.TranslatePortrait(ctx, c.Def.Portrait, c.Def.CustomPortrait)

	switch portrait {
	case card.PortraitCustomEmbedded:
		if len(custom) == 0 {
			return
		}

		tex := gfx.NewEmbeddedTexture("(embedded portrait:"+c.Def.DisplayName()+")", false, false, custom, "image/png", true)
		c.portraitTex = tex
		c.portraitSprite = sprites.New(tex, 100, 128, 128, 0, 0, 128, 128, 0.5, 0.5)
	case card.PortraitCustomExternal:
		if i, ok := c.Set.PortraitIndex[string(custom)]; ok && c.Set.PortraitSheet != nil {
			size := bits.Len(uint(len(c.Set.PortraitIndex) - 1))
			w, h := 1<<((size+1)/2), 1<<(size/2)
			x, y := i%w, i/w
			c.portraitSprite = sprites.New(c.Set.PortraitSheet, 100, w*128, h*128, float32(x*128), float32((h-y-1)*128), 128, 128, 0.5, 0.5)
		} else {
			tex := gfx.NewCachedAssetTexture(internal.GetConfig(ctx).UserImageBaseURL + format.Encode32(custom) + ".webp")
			c.portraitTex = tex
			c.portraitSprite = sprites.New(tex, 100, 128, 128, 0, 0, 128, 128, 0.5, 0.5)
		}
	default:
		c.portraitSprite = sprites.Portraits[portrait]

		if c.portraitSprite == nil {
			log.Printf("WARNING: setting unknown portrait %d in card %d (%q) to blank", portrait, c.Def.ID, c.Def.DisplayName())
			c.portraitSprite = sprites.Portraits[0x80]
		}
	}
}

func (c *Card) renderOverlays(batch *sprites.Batch, tint sprites.Color) {
	rh := c.renderHelper(batch)

	if c.Active != nil && c.Active.UnNumb != 0 {
		batch.AppendEx(sprites.Tablet, rh.flipXX*1+rh.flipZX*-cardDepth/4, -1.4, rh.flipXZ*1+rh.flipZZ*-cardDepth/4, 0.65, 0.65, tint, rh.flags, 0, rh.flip, 0)

		n := card.Num(c.Active.UnNumb)
		if n.Amount > 1e12 {
			n.Amount, n.AmountInf = 0, 1
		}

		rh.drawTextBorder(sprites.FontD3Streetism, n.String(), 1, -1.55625, -cardDepth/2, 0.5, 0.625, 0.5, tint, sprites.Black, 0, true)
	}

	backTint := tint
	backTint.R, backTint.G, backTint.B = backTint.R/2, backTint.G/2, backTint.B/2

	for _, coin := range c.coins {
		if coin.invisibleTimer != 0 {
			continue
		}

		const coinScale = float32(0.45)

		face1, face2 := sprites.Happy, sprites.Sad
		scale1, scale2 := float32(0.375), float32(0.375)

		if coin.stat {
			face1, face2 = sprites.ATK, sprites.DEF
			scale1, scale2 = 0.1875, 0.2625
		}

		if coin.heads {
			face1, face2 = face2, face1
			scale1, scale2 = scale2, scale1
		}

		spin := rh.flip - float32(coin.revealTimer)/3
		sin, cos := math.Sincos(float64(spin))

		if cos < 0 {
			face2 = face1
			scale2 = scale1
			sin, cos = -sin, -cos
			spin -= math.Pi
		}

		batch.AppendEx(sprites.CheapCoin, rh.flipXX*coin.dx+rh.flipZX*coinDistance+float32(sin)*coinDepth, coin.dy, rh.flipXZ*coin.dx+rh.flipZZ*coinDistance-float32(cos)*coinDepth, coinScale, coinScale, backTint, rh.flags, 0, spin, 0)

		batch.AppendEx(sprites.CheapCoin, rh.flipXX*coin.dx+rh.flipZX*coinDistance-float32(sin)*coinDepth, coin.dy, rh.flipXZ*coin.dx+rh.flipZZ*coinDistance+float32(cos)*coinDepth, coinScale, coinScale, tint, rh.flags, 0, spin, 0)
		batch.AppendEx(face2, rh.flipXX*coin.dx+rh.flipZX*coinDistance-float32(sin)*coinDepth, coin.dy, rh.flipXZ*coin.dx+rh.flipZZ*coinDistance+float32(cos)*coinDepth, scale2, scale2, tint, rh.flags, 0, spin, 0)
	}
}

// Flip flips the card from face-up to face-down or vice versa.
func (c *Card) Flip() {
	c.SetFlip(!c.IsFlip())
}

// SetFlip flips the card to a specified side.
func (c *Card) SetFlip(down bool) {
	if down {
		c.flipDir = flipSpeed
	} else {
		c.flipDir = -flipSpeed
	}
}

// IsFlip returns true if the card is face-down.
func (c *Card) IsFlip() bool {
	return c.flipDir >= 0 && c.flip > 0
}
