//go:build js && wasm && !headless
// +build js,wasm,!headless

package visual

import (
	"strconv"
	"strings"
	"syscall/js"

	"git.lubar.me/ben/spy-cards/internal"
	"git.lubar.me/ben/spy-cards/internal/router"
	"git.lubar.me/ben/spy-cards/match"
)

func (v *Visual) onModeChange() {
	info := &router.PageInfo{
		Page: router.PageCardsHome,
	}

	switch v.Match.Init.Mode {
	case "":
		info.Mode = "vanilla"
		info.Revision = int(v.Match.Init.Variant)
	case match.ModeCustom:
		info.Mode = "custom"
		info.Custom = v.Match.Init.Custom
		info.Variant = v.Match.Init.Variant
	default:
		i := strings.Index(v.Match.Init.Mode, ".")
		info.Mode = v.Match.Init.Mode[:i]
		info.Variant = v.Match.Init.Variant

		if !v.Match.Init.Latest {
			info.Revision, _ = strconv.Atoi(v.Match.Init.Mode[i+1:])
		}
	}

	switch v.State {
	case StatePlaybackInit, StatePlaybackIdle, StatePlaybackNext, StatePlaybackTurnWait:
		info.Page = router.PageCardsRecording
		info.Code = v.recordingCode
	case StateSelectMode:
		info.Page = router.PageCardsModes
	case StateDeckEditor:
		info.Page = router.PageCardsDeckEditor

		if v.deck != nil && v.deck.builder != nil && v.deck.builder.picked == nil && v.deck.builder.deckCode != "" {
			info.Page = router.PageCardsDeck
			info.Code = v.deck.builder.deckCode
		}
	}

	info.AsNPC = AsNPC
	info.VersusNPC = VersusNPC

	router.Replace(info)

	v.updateAgain = true
}

func redirectToWebCardEditor(cardCodes string) bool {
	internal.Location.Set("href", "/game.html?custom=editor#"+cardCodes)

	return true
}

func checkForUpdates() {
	sw := js.Global().Get("navigator").Get("serviceWorker")
	if !sw.Truthy() {
		return
	}

	ready, _ := internal.Await(sw.Get("ready"))
	if !ready.Truthy() {
		return
	}

	ready.Call("update")
}
