//go:generate stringer -type State -trimprefix State

package visual

type State uint64

// Constants for State.
const (
	StateHome State = iota
	StateConnecting
	StateWait
	StateMatchmakingWait
	StateCharacterSelect
	StateDeckSelect
	StateReadyToStart
	StateNextRound
	StateNextRoundWait
	StateTurnSelect
	StateTurnProcess
	StateTurnEnd
	StateMatchCompleteWait
	StateMatchComplete
	StatePlaybackFetch
	StatePlaybackInit
	StatePlaybackIdle
	StatePlaybackNext
	StatePlaybackTurnWait
	StateSelectMode
	StateDeckEditor
)
