package visual

import (
	"context"
	"runtime"

	"git.lubar.me/ben/spy-cards/gfx"
	"git.lubar.me/ben/spy-cards/gfx/sprites"
	"golang.org/x/mobile/gl"
)

type CheapCardSheet struct {
	cards [10 * 8]*Card
	atex  *gfx.AssetTexture
	tex   gl.Texture
	dirty bool
}

func NewCheapCardSheet() *CheapCardSheet {
	gfx.Lock.Lock()
	defer gfx.Lock.Unlock()

	s := &CheapCardSheet{
		tex: gfx.GL.CreateTexture(),
	}

	s.atex = gfx.DummyAssetTexture("(cheap-card-sheet)", s.tex)

	runtime.SetFinalizer(s, (*CheapCardSheet).Release)

	gfx.GL.BindTexture(gl.TEXTURE_2D, s.tex)
	gfx.GL.TexImage2D(gl.TEXTURE_2D, 0, gl.RGBA, 2048, 2048, gl.RGBA, gl.UNSIGNED_BYTE, nil)
	gfx.GL.TexParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE)
	gfx.GL.TexParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE)
	gfx.GL.TexParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.LINEAR)
	gfx.GL.TexParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR_MIPMAP_NEAREST)
	gfx.GL.BindTexture(gl.TEXTURE_2D, gl.Texture{})

	return s
}

func (s *CheapCardSheet) Release() {
	gfx.GL.DeleteTexture(s.tex)
	runtime.SetFinalizer(s, nil)
}

func (s *CheapCardSheet) Ready(ctx context.Context) bool {
	if !s.dirty {
		return true
	}

	for _, s := range []*sprites.Sprite{sprites.CardFront, sprites.CardFrontWindow} {
		if !s.Ready() {
			return false
		}
	}

	if !sprites.TextReady() {
		return false
	}

	for _, c := range s.cards {
		if c == nil {
			continue
		}

		if c.portraitSprite == nil {
			c.initPortraitSprite(ctx)
		}

		if c.portraitSprite != nil && !c.portraitSprite.Ready() {
			return false
		}
	}

	var cam gfx.Camera

	cam.SetDefaults()
	cam.Perspective.Scale(0.74, 0.585, 0.01)
	cam.Offset.Identity()
	cam.Rotation.Identity()
	cam.Position.Identity()

	gfx.Lock.Lock()

	fb := gfx.GL.CreateFramebuffer()
	gfx.GL.BindTexture(gl.TEXTURE_2D, s.tex)
	gfx.GL.BindFramebuffer(gl.FRAMEBUFFER, fb)
	gfx.GL.FramebufferTexture2D(gl.FRAMEBUFFER, gl.COLOR_ATTACHMENT0, gl.TEXTURE_2D, s.tex, 0)

	gfx.Lock.Unlock()

	var prevClearColor [4]float32

	gfx.GL.GetFloatv(prevClearColor[:], gl.COLOR_CLEAR_VALUE)

	gfx.GL.ClearColor(0, 0, 0, 0)
	gfx.GL.Clear(gl.COLOR_BUFFER_BIT)

	gfx.GL.ClearColor(prevClearColor[0], prevClearColor[1], prevClearColor[2], prevClearColor[3])

	batch := sprites.NewBatch(&cam)
	batch.AvoidTextureFeedback = true

	for i, c := range s.cards {
		if c == nil {
			continue
		}

		x, y := i%10, 7-i/10
		gfx.GL.Viewport(x*204, y*256, 204, 256)

		batch.Reset(&cam)
		c.Render(ctx, batch, nil)
		batch.Render()
	}

	gfx.Lock.Lock()

	gfx.GL.BindTexture(gl.TEXTURE_2D, s.tex)
	gfx.GL.GenerateMipmap(gl.TEXTURE_2D)
	gfx.GL.BindFramebuffer(gl.FRAMEBUFFER, gl.Framebuffer{})
	gfx.GL.DeleteFramebuffer(fb)

	gfx.Lock.Unlock()

	w, h := gfx.PixelSize()
	gfx.GL.Viewport(0, 0, w, h)

	s.dirty = false

	return true
}

func (s *CheapCardSheet) SetCard(i int, c *Card) {
	s.cards[i] = c
	s.dirty = true
}

func (s *CheapCardSheet) Sprite(i int) *sprites.Sprite {
	if s.dirty || s.cards[i] == nil {
		return nil
	}

	x, y := float32(i%10), float32(i/10)
	sprite := sprites.New(
		s.atex, 75,
		2048, 2048,
		x*204+14.5, y*256+9.5,
		175, 237,
		0.5, 0.5,
	)

	return sprite
}
