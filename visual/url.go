package visual

import (
	"strconv"
	"strings"

	"git.lubar.me/ben/spy-cards/match"
)

func (v *Visual) encodeModeState() (search, fragment string) {
	switch v.Match.Init.Mode {
	case "":
		if v.Match.Init.Variant != 0 {
			search = "?variant=" + strconv.FormatUint(v.Match.Init.Variant, 10)
		}
	case match.ModeCustom:
		search = "?custom"

		if v.Match.Init.CachedVariant != nil {
			search += "&variant=" + strconv.FormatUint(v.Match.Init.Variant, 10)
		}

		if v.Match.Init.Custom != "" {
			fragment = "#" + v.Match.Init.Custom
		}
	default:
		i := strings.IndexByte(v.Match.Init.Mode, '.')

		search = "?mode=" + v.Match.Init.Mode[:i]

		if !v.Match.Init.Latest {
			search += "&rev=" + v.Match.Init.Mode[i+1:]
		}

		if v.Match.Init.CachedVariant != nil {
			search += "&variant=" + strconv.FormatUint(v.Match.Init.Variant, 10)
		}
	}

	return search, fragment
}
