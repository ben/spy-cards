package visual

import (
	"context"
	"log"

	"git.lubar.me/ben/spy-cards/card"
	"git.lubar.me/ben/spy-cards/gui"
	"git.lubar.me/ben/spy-cards/internal"
	"git.lubar.me/ben/spy-cards/internal/router"
	"git.lubar.me/ben/spy-cards/match"
)

type recentMode struct {
	Client string `json:"s"`
}

var noWindowModes = map[string]bool{
	"hunters_journal": true,
	"pvz":             true,
	"star-crossed":    true,
}

func (v *Visual) loadModes(ctx context.Context) {
	var modes match.AvailableModes

	err := internal.FetchJSON(ctx, internal.GetConfig(ctx).CustomCardAPIBaseURL+"modes", &modes)
	if err != nil {
		log.Printf("ERROR: loading game mode list: %+v", err)

		return
	}

	recentBytes, err := internal.LoadData("spy-cards-recent-modes-v0")
	if err != nil {
		log.Printf("WARNING: failed to load recent modes: %+v", err)
	}

	var recent []recentMode

	if len(recentBytes) != 0 {
		if err := internal.UnmarshalJSON(recentBytes, &recent); err != nil {
			log.Printf("WARNING: failed to load recent modes: %+v", err)
		}

		for i := len(recent) - 1; i >= 0; i-- {
			for j, a := range modes.Modes {
				if a.Client == recent[i].Client {
					copy(modes.Modes[1:], modes.Modes[:j])
					modes.Modes[0] = a

					break
				}
			}
		}
	}

	modeCards := make([]*Card, 0, int(card.LatestVersion)+4+len(modes.Modes))

	var set card.Set

	match.MakeModesPseudoSet(&set, &modes, func(c *card.Def, md *card.Metadata, am *match.AvailableMode) {
		mc := &match.Card{
			Set:  &set,
			Def:  c,
			Back: c.Rank.Back(false),
		}

		modeCards = append(modeCards, &Card{
			Card: mc,
			Active: &match.ActiveCard{
				Card: mc,
				Desc: c.Description(&set),
			},
			noFancyBorder: c.Rank == card.Boss,
			noWindow:      (am != nil && noWindowModes[am.Client]) || (md != nil && md.Portrait == card.PortraitCustomEmbedded),
		})
	})

	preloadPortraits(ctx, &set, match.ModeCustom)

	v.runSync <- func() {
		v.modeData = &modes
		focus := new(*gui.FocusHandler)
		v.modeChoice = NewLayout(ctx)
		v.modeChoice.DefaultFocus = true
		v.modeChoice.scroll.StealFocus = true

		v.modeChoice.OnSelect = func(c *Card) {
			switch {
			case c.Def.ID == 0:
				v.Match.Init.Mode = match.ModeCustom
				v.Match.Init.Variant = 0
			case c.Def.ID == 1:
				v.replacePage = &router.PageInfo{
					Page: router.PageStatic,
					Raw:  "/settings.html",
				}

				return
			case c.Def.ID == 2:
				v.replacePage = &router.PageInfo{
					Page: router.PageArcadeMain,
				}

				return
			case c.Def.ID == 3:
				v.replacePage = &router.PageInfo{
					Page: router.PageAphidFestival,
				}

				return
			case c.Def.ID >= 128:
				v.Match.Init.Mode = modes.Modes[c.Def.ID-128].Client + ".0"
				v.Match.Init.Custom = ""
				v.Match.Init.Variant = 0

				for i, r := range recent {
					if r.Client == modes.Modes[c.Def.ID-128].Client {
						recent = append(recent[:i], recent[i+1:]...)
					}
				}

				recent = append([]recentMode{{Client: modes.Modes[c.Def.ID-128].Client}}, recent...)
				recentBytes, err := internal.MarshalJSON(recent)
				if err != nil {
					log.Printf("WARNING: encoding recently selected modes: %+v", err)
				}

				err = internal.StoreData("spy-cards-recent-modes-v0", recentBytes)
				if err != nil {
					log.Printf("WARNING: saving recently selected modes: %+v", err)
				}
			default:
				version := card.BugFablesVersion(c.Def.ID - 4)

				v.Match.Init.Mode = ""
				v.Match.Init.Custom = ""

				if version == card.LatestVersion {
					v.Match.Init.Variant = 0
				} else {
					v.Match.Init.Variant = uint64(version + 1)
				}
			}

			v.Match.Init.External = nil
			v.Match.Init.CachedVariant = nil
			v.Match.Init.Latest = true

			v.State = StateHome
			v.modeLoad = make(chan struct{})
			go v.loadMode(ctx)
			v.resizeHome(ctx)
			v.onModeChange()
		}

		*focus = &v.modeChoice.FocusHandler
		v.modeChoice.InputFocus = focus
		v.modeChoice.Cards = modeCards
		v.modeChoice.noCheap = true
	}
}

func (v *Visual) selectModeUpdate(ctx context.Context) {
	if !v.loadedModes {
		go v.loadModes(ctx)

		v.loadedModes = true
	}

	if v.modeData == nil {
		v.message = "Loading game mode list..."
	} else {
		v.message = ""
	}

	if v.modeChoice != nil {
		v.modeChoice.Update(nil)
	}
}

func (v *Visual) selectModeRender(ctx context.Context) error {
	v.stage.Render()

	if v.modeChoice != nil {
		for pass := 0; pass < 3; pass++ {
			v.modeChoice.Render(ctx, pass)
		}
	}

	return nil
}
