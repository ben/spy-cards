//go:build !headless
// +build !headless

package visual

import (
	"context"
	"errors"
	"fmt"
	"log"
	"math"
	"strconv"
	"strings"

	"git.lubar.me/ben/spy-cards/arcade/touchcontroller"
	"git.lubar.me/ben/spy-cards/audio"
	"git.lubar.me/ben/spy-cards/card"
	"git.lubar.me/ben/spy-cards/gfx"
	"git.lubar.me/ben/spy-cards/gfx/sprites"
	"git.lubar.me/ben/spy-cards/gui"
	"git.lubar.me/ben/spy-cards/input"
	"git.lubar.me/ben/spy-cards/internal"
	"git.lubar.me/ben/spy-cards/internal/router"
	"git.lubar.me/ben/spy-cards/match"
	"git.lubar.me/ben/spy-cards/match/minimal"
	"git.lubar.me/ben/spy-cards/match/npc"
	"git.lubar.me/ben/spy-cards/room"
	"git.lubar.me/ben/spy-cards/scnet"
	"git.lubar.me/ben/spy-cards/spoilerguard"
)

var (
	// AsNPC causes an NPC to play the game instead of the user.
	AsNPC string
	// VersusNPC forces the game into singleplayer mode.
	VersusNPC string
)

type homeLayout struct {
	modeLoad   <-chan struct{}
	scroll     gui.Scroll
	vert       gui.Vertical
	title      gui.Button
	variant    gui.Toggle
	host       gui.Button
	hostDesc   gui.Button
	watch      gui.Button
	watchDesc  gui.Button
	quick      gui.Button
	quickDesc  gui.Button
	changelog  gui.Button
	deckEditor gui.Button
	cards      []*Card
	sg         *spoilerguard.Data
	scroller1  gui.FocusHandler
	scroller2  gui.FocusHandler
	scrollY    float32
	sgTimer    uint8
}

func (h *homeLayout) HasFocus() bool {
	return h.title.HasFocus() || h.variant.HasFocus() || h.host.HasFocus() || h.watch.HasFocus() || h.quick.HasFocus()
}

func (h *homeLayout) Width() float32 {
	w, _ := gfx.Size()

	return float32(w) / 64
}

func (h *homeLayout) Height() float32 {
	_, height := gfx.Size()

	return float32(height)/64 + 2
}

func (h *homeLayout) Update(cam *gfx.Camera, ictx *input.Context) {
	h.title.Update(cam, ictx)
	h.variant.Update(cam, ictx)
	h.host.Update(cam, ictx)
	h.hostDesc.Update(cam, ictx)
	h.watch.Update(cam, ictx)
	h.watchDesc.Update(cam, ictx)
	h.quick.Update(cam, ictx)
	h.quickDesc.Update(cam, ictx)

	if h.sgTimer == 0 {
		prevMenu := uint32(0)
		if h.sg != nil {
			prevMenu = h.sg.Menu
		}

		h.sg = spoilerguard.LoadData()

		if (h.sg == nil && prevMenu != 0) || (h.sg != nil && h.sg.Menu != prevMenu) {
			for _, c := range h.cards {
				c.portraitSprite = nil
				c.portraitTex = nil
				c.initPortraitSprite(context.Background())
			}
		}

		h.sgTimer = 60
	} else {
		h.sgTimer--
	}
}

func (h *homeLayout) Render(b *sprites.Batch) {
	if h.sg != nil {
		width, height := gfx.Size()

		scale := float32(1)
		if width < 800 || height < 600 {
			scale = 0.5
		}

		x := float32(width)/128 - 0.5*scale
		y := float32(height)/128 - 0.5*scale

		for i := len(sprites.MenuIcon) - 1; i >= 0; i-- {
			if h.sg.Menu&(1<<i) != 0 {
				b.Append(sprites.MenuIcon[i], x, y, 0, scale, scale, sprites.White)
				x -= 0.7 * scale
			}
		}
	}

	h.variant.Render(b)
	h.title.Render(b)
	h.hostDesc.Render(b)
	h.watchDesc.Render(b)
	h.quickDesc.Render(b)
	h.host.Render(b)
	h.watch.Render(b)
	h.quick.Render(b)
}

func (h *homeLayout) SetY(y float32) {
	_, height := gfx.Size()

	y -= float32(height) / 128
	h.scrollY = y
	h.title.SetY(y)
	h.variant.SetY(y)
	h.host.SetY(y)
	h.hostDesc.SetY(y)
	h.watch.SetY(y)
	h.watchDesc.SetY(y)
	h.quick.SetY(y)
	h.quickDesc.SetY(y)
}

func (v *Visual) initHome(ctx context.Context) {
	focus := new(*gui.FocusHandler)

	v.home.vert.AlwaysFocus = true
	v.home.vert.Gap = 0.5
	v.home.vert.Controls = []gui.Control{
		&v.home,
	}

	v.home.scroll.Control = &v.home.vert
	v.home.scroll.Screen = 64
	v.home.scroll.Size = 1

	v.home.title.Sx = 2
	v.home.title.Sy = 2
	v.home.title.Font = sprites.FontD3Streetism
	v.home.title.Centered = true
	v.home.title.Style = gui.BtnShadow
	v.home.title.InputFocus = focus

	// sx and sy set with mode data
	v.home.variant.Button.Font = sprites.FontD3Streetism
	v.home.variant.Button.Centered = true
	v.home.variant.Button.Style = gui.BtnShadow
	v.home.variant.Button.InputFocus = focus

	v.home.host.Sx = 2.5
	v.home.host.Sy = 2.5
	v.home.host.Font = sprites.FontBubblegumSans
	v.home.host.Centered = true
	v.home.host.Style = gui.BtnShadow
	v.home.host.InputFocus = focus
	v.home.host.DefaultFocus = true

	v.home.hostDesc.Sx = 0.5
	v.home.hostDesc.Sy = 0.5
	v.home.hostDesc.Font = sprites.FontBubblegumSans
	v.home.hostDesc.Centered = true
	v.home.hostDesc.Style = gui.BtnShadow

	v.home.watch.Sx = 2.5
	v.home.watch.Sy = 2.5
	v.home.watch.Font = sprites.FontBubblegumSans
	v.home.watch.Centered = true
	v.home.watch.Style = gui.BtnShadow
	v.home.watch.InputFocus = focus

	v.home.watchDesc.Sx = 0.5
	v.home.watchDesc.Sy = 0.5
	v.home.watchDesc.Font = sprites.FontBubblegumSans
	v.home.watchDesc.Centered = true
	v.home.watchDesc.Style = gui.BtnShadow

	v.home.quick.Sx = 1.5
	v.home.quick.Sy = 1.5
	v.home.quick.Font = sprites.FontBubblegumSans
	v.home.quick.Centered = true
	v.home.quick.Style = gui.BtnShadow
	v.home.quick.InputFocus = focus
	v.home.quick.InputFocusDown = v.home.scroller1.Focus

	v.home.quickDesc.Sx = 0.5
	v.home.quickDesc.Sy = 0.5
	v.home.quickDesc.Font = sprites.FontBubblegumSans
	v.home.quickDesc.Centered = true
	v.home.quickDesc.Style = gui.BtnShadow

	v.home.variant.OnChange = func(i int, _ gui.ToggleOption) {
		v.switchVariant(ctx, i)
	}
	v.home.variant.Button.InputFocusDown = v.home.host.Focus

	v.home.scroller1.InputFocus = focus
	v.home.scroller2.InputFocus = focus
}

func (v *Visual) resizeHome(ctx context.Context) {
	select {
	case <-v.modeLoad:
		v.home.modeLoad = nil
	default:
		v.home.modeLoad = v.modeLoad
	}

	if !v.loadedModes {
		go v.loadModes(ctx)

		v.loadedModes = true
	}

	if v.home.modeLoad != nil {
		return
	}

	w, h := gfx.Size()

	v.home.vert.BaseY = float32(h)/128 + 1

	v.home.title.MaxWidth = float32(w)/64 - 2
	v.home.variant.Button.MaxWidth = float32(w)/64 - 4

	v.home.title.X, v.home.title.Y = 0, float32(h)/128-2
	v.home.variant.Button.X, v.home.variant.Button.Y = 0, float32(h)/128-3

	switch {
	case v.Match.Init.External != nil:
		v.home.title.Label = v.Match.Init.External.Name

		internal.SetTitle(v.Match.Init.External.Name + " - Spy Cards Online")
	case v.Match.Init.Mode == match.ModeCustom:
		v.home.title.Label = "Spy Cards Online (Custom)"

		internal.SetTitle("Custom Cards - Spy Cards Online")
	default:
		v.home.title.Label = "Spy Cards Online"

		internal.SetTitle("Play Online - Spy Cards Online")
	}

	if AsNPC != "" {
		v.home.title.Label += " (NPC)"
	}

	variant := v.Match.Init.CachedVariant
	isSingleplayer := VersusNPC != "" || (variant != nil && variant.NPC != "")

	var quickStacked, horizontalLayout, verticalLayout bool

	switch aspect := float32(w) / float32(h); {
	case aspect > 2.1:
		v.home.host.X, v.home.host.Y = -float32(w)/128/1.5, float32(h)/128-5.5
		v.home.watch.X, v.home.watch.Y = 0, float32(h)/128-5.5
		v.home.quick.X, v.home.quick.Y = float32(w)/128/1.5, float32(h)/128-4.75
		quickStacked = true

		if h < 450 {
			v.home.host.Y++
			v.home.watch.Y++
			v.home.quick.Y++
		}

		horizontalLayout = true

		v.home.host.MaxWidth = float32(w) / 64
		v.home.hostDesc.MaxWidth = float32(w)/64 - 0.5
		v.home.watch.MaxWidth = float32(w) / 64
		v.home.watchDesc.MaxWidth = float32(w)/64 - 0.5
		v.home.quick.MaxWidth = float32(w) / 64
		v.home.quickDesc.MaxWidth = float32(w)/64 - 0.5
	case aspect < 0.75:
		v.home.host.X, v.home.host.Y = 0, float32(h)/128-5
		v.home.watch.X, v.home.watch.Y = 0, float32(h)/128-7.25
		v.home.quick.X, v.home.quick.Y = 0, float32(h)/128-9
		quickStacked = true

		if h > 750 {
			v.home.host.Y--
			v.home.watch.Y--
			v.home.quick.Y--
		}

		if variant == nil {
			v.home.host.Y += 0.75
			v.home.watch.Y += 0.75
			v.home.quick.Y += 0.75
		}

		verticalLayout = true

		v.home.host.MaxWidth = float32(w) / 64
		v.home.hostDesc.MaxWidth = float32(w)/64 - 0.5
		v.home.watch.MaxWidth = float32(w) / 64
		v.home.watchDesc.MaxWidth = float32(w)/64 - 0.5
		v.home.quick.MaxWidth = float32(w) / 64
		v.home.quickDesc.MaxWidth = float32(w)/64 - 0.5
	default:
		v.home.host.X, v.home.host.Y = -float32(w)/128/2, float32(h)/128-5
		v.home.watch.X, v.home.watch.Y = float32(w)/128/2, float32(h)/128-5
		v.home.quick.X, v.home.quick.Y = 0, float32(h)/128-7.25
		quickStacked = false

		if variant == nil {
			v.home.host.Y += 0.75
			v.home.watch.Y += 0.75
			v.home.quick.Y += 0.75
		}

		v.home.host.MaxWidth = float32(w) / 128
		v.home.hostDesc.MaxWidth = float32(w)/128 - 0.5
		v.home.watch.MaxWidth = float32(w) / 128
		v.home.watchDesc.MaxWidth = float32(w)/128 - 0.5
		v.home.quick.MaxWidth = float32(w) / 64
		v.home.quickDesc.MaxWidth = float32(w)/64 - 0.5
	}

	if variant != nil {
		v.home.variant.Button.Sx = 1.25
		v.home.variant.Button.Sy = 1.25
		v.home.variant.Button.Color = sprites.White
		v.home.variant.Button.Style = gui.BtnShadow
		v.home.variant.Button.Label = variant.Title

		variants := v.Match.Set.Mode.GetAll(card.FieldVariant)

		v.home.variant.Options = make([]gui.ToggleOption, len(variants))

		for i, f := range variants {
			o := f.(*card.Variant)

			if o == variant {
				v.home.variant.Selected = i
			}

			v.home.variant.Options[i].Label = o.Title
		}

		v.home.variant.Button.InputFocus = v.home.title.InputFocus
	} else {
		v.home.variant.Button.Sx = 0.5
		v.home.variant.Button.Sy = 0.5
		v.home.variant.Button.Color = sprites.Gray
		v.home.variant.Button.Style = gui.BtnBorder
		v.home.variant.Button.Y += 0.5

		if internal.LikelyTouch() {
			v.home.variant.Button.Label = "(Tap To Change Mode)"
		} else {
			v.home.variant.Button.Label = "(Click To Change Mode)"
		}

		v.home.variant.Button.InputFocus = nil
	}

	v.home.host.Label = "Host"
	v.home.hostDesc.Label = "Invite a friend to play."

	if isSingleplayer {
		v.home.host.X = (v.home.host.X + v.home.watch.X) / 2
		v.home.host.Y = (v.home.host.Y + v.home.watch.Y) / 2
		v.home.host.Label = "Singleplayer"
		v.home.hostDesc.Label = "Face off against a computer-controlled opponent."

		v.home.watch.Label = ""
		v.home.watchDesc.Label = ""

		v.stage.SetPlayer(2)
	} else {
		v.home.watch.Label = "Watch"
		v.home.watchDesc.Label = "View recordings of past matches."

		v.stage.SetPlayer(1)
	}

	quickWordSep := " "
	if quickStacked {
		quickWordSep = "\n"
	}

	switch {
	default:
		v.home.quick.Label = "Auto" + quickWordSep + "Join"
		v.home.quickDesc.Label = "Play against a stranger."
	case v.Match.Init.Mode == match.ModeCustom:
		v.home.quick.Label = "Card" + quickWordSep + "Editor"
		v.home.quickDesc.Label = "Create cards and game modes."
	case isSingleplayer:
		v.home.host.X = 0

		fallthrough
	case !v.allowQuickJoin:
		v.home.quick.Label = ""
		v.home.quickDesc.Label = ""
	}

	v.home.hostDesc.X = v.home.host.X
	v.home.hostDesc.Y = v.home.host.Y - 0.5
	v.home.watchDesc.X = v.home.watch.X
	v.home.watchDesc.Y = v.home.watch.Y - 0.5
	v.home.quickDesc.X = v.home.quick.X
	v.home.quickDesc.Y = v.home.quick.Y - 0.5

	if isSingleplayer {
		v.home.hostDesc.Y -= 0.35
	}

	if quickStacked {
		v.home.quickDesc.Y--
	}

	quickButtonVisible := v.allowQuickJoin || v.Match.Init.Mode == match.ModeCustom
	isBlankCustom := v.Match.Init.Mode == match.ModeCustom && v.Match.Init.Custom == ""

	switch {
	case variant != nil:
		v.home.host.InputFocusUp = v.home.variant.Focus
		v.home.variant.Button.InputFocusUp = v.home.title.Focus
		v.home.title.InputFocusDown = v.home.variant.Focus
	default:
		v.home.host.InputFocusUp = v.home.title.Focus
		v.home.title.InputFocusDown = v.home.host.Focus
	}

	switch {
	case verticalLayout && !isSingleplayer:
		v.home.host.InputFocusDown = v.home.watch.Focus
	case !horizontalLayout && quickButtonVisible:
		v.home.host.InputFocusDown = v.home.quick.Focus
	default:
		v.home.host.InputFocusDown = v.home.scroller1.Focus
	}

	switch {
	case !verticalLayout && !isSingleplayer:
		v.home.host.InputFocusRight = v.home.watch.Focus
	case horizontalLayout && quickButtonVisible:
		v.home.host.InputFocusRight = v.home.quick.Focus
	default:
		v.home.host.InputFocusRight = nil
	}

	switch {
	case verticalLayout && !isBlankCustom:
		v.home.watch.InputFocusUp = v.home.host.Focus
	case variant != nil:
		v.home.watch.InputFocusUp = v.home.variant.Focus
	default:
		v.home.watch.InputFocusUp = v.home.title.Focus
	}

	switch {
	case quickButtonVisible && !horizontalLayout:
		v.home.watch.InputFocusDown = v.home.quick.Focus
	default:
		v.home.watch.InputFocusDown = v.home.scroller1.Focus
	}

	switch {
	case !isBlankCustom && !verticalLayout:
		v.home.watch.InputFocusLeft = v.home.host.Focus
	default:
		v.home.watch.InputFocusLeft = nil
	}

	switch {
	case quickButtonVisible && horizontalLayout:
		v.home.watch.InputFocusRight = v.home.quick.Focus
	default:
		v.home.watch.InputFocusRight = nil
	}

	switch {
	case !horizontalLayout && isBlankCustom, verticalLayout && !isSingleplayer:
		v.home.quick.InputFocusUp = v.home.watch.Focus
	case !horizontalLayout:
		v.home.quick.InputFocusUp = v.home.host.Focus
	case variant != nil:
		v.home.quick.InputFocusUp = v.home.variant.Focus
	default:
		v.home.quick.InputFocusUp = v.home.title.Focus
	}

	switch {
	case isSingleplayer && horizontalLayout:
		v.home.quick.InputFocusLeft = v.home.host.Focus
	case horizontalLayout:
		v.home.quick.InputFocusLeft = v.home.watch.Focus
	default:
		v.home.quick.InputFocusLeft = nil
	}

	switch {
	case !horizontalLayout && quickButtonVisible:
		v.home.scroller1.InputFocusUp = v.home.quick.Focus
	default:
		v.home.scroller1.InputFocusUp = v.home.host.Focus
	}

	switch {
	case !isSingleplayer && router.FlagMockRTC.IsSet():
		v.home.host.Color = sprites.Gray
		v.home.host.OnSelect = nil
		v.home.host.InputFocus = nil

		if v.Match.Init.Mode != match.ModeCustom {
			v.home.quick.Color = sprites.Gray
			v.home.quick.OnSelect = nil
			v.home.quick.InputFocus = nil
		}
	case isBlankCustom:
		v.home.host.Color = sprites.Gray
		v.home.host.OnSelect = nil
		v.home.host.InputFocus = nil
	default:
		v.home.host.Color = sprites.White
		v.home.host.OnSelect = func() {
			v.State = StateWait

			variant := v.Match.Init.CachedVariant
			isSingleplayer := VersusNPC != "" || (variant != nil && variant.NPC != "")

			if isSingleplayer {
				v.Match.Perspective = 2
				v.Match.UnknownDeck = 1

				v.stage.SetPlayer(2)

				go v.initSinglePlayer(ctx)
			} else {
				v.Match.Perspective = 1
				v.Match.UnknownDeck = 2

				v.stage.SetPlayer(1)

				go v.initPlayer(ctx, scnet.Player1)
			}
		}
		v.home.host.InputFocus = v.home.title.InputFocus
	}

	v.home.title.OnSelect = func() {
		v.State = StateSelectMode
		v.onModeChange()
	}
	v.home.variant.Button.OnSelect = func() {
		if v.home.variant.Button.InputFocusRight != nil {
			v.home.variant.Button.InputFocusRight()
		}
	}
	v.home.watch.OnSelect = func() {
		mode := v.Match.Init.Mode
		if mode == "" {
			mode = match.ModeVanilla
		} else {
			mode = mode[:strings.Index(mode, ".")]
		}

		v.replacePage = &router.PageInfo{
			Page: router.PageCardsRecording,
			Mode: mode,
			Code: "auto",
		}
	}
	v.home.quick.OnSelect = func() {
		v.State = StateWait

		if v.Match.Init.Mode == match.ModeCustom {
			if redirectToWebCardEditor(v.Match.Init.Custom) {
				return
			}

			if err := v.InitCardEditor(ctx, v.Match.Init.Custom); err != nil {
				panic(err)
			}
		} else {
			go v.initPlayer(ctx, scnet.UnknownPerspective)
		}
	}

	if !v.anyRecording {
		v.home.watch.Color = sprites.Gray
		v.home.watchDesc.Color = sprites.Gray
		v.home.watch.OnSelect = nil
		v.home.watch.InputFocus = nil
	} else {
		v.home.watch.Color = sprites.White
		v.home.watchDesc.Color = sprites.White
		v.home.watch.InputFocus = v.home.title.InputFocus
	}

	v.home.scroller1.InputFocusDown = v.home.scroller2.Focus
	v.home.scroller2.InputFocusUp = v.home.scroller1.Focus

	if v.Match.Set != nil {
		preloadAssets(ctx, v.Match.Set, v.Match.Init.Mode)

		v.home.vert.Controls = v.home.vert.Controls[:1]

		width := float32(w)/64 - 2
		if width > 12 {
			width = 12
		}

		first := true

		for _, f := range v.Match.Set.Mode.GetAll(card.FieldMetadata) {
			md := f.(*card.Metadata)

			title := md.Title

			if first {
				first = false

				if title == "" && v.Match.Init.External != nil {
					title = v.Match.Init.External.Name + " (v" + strconv.Itoa(v.Match.Init.External.Revision) + ")"
				}
			}

			if title != "" {
				v.home.vert.Controls = append(v.home.vert.Controls, &gui.Button{
					Label:    title,
					Centered: true,
					Style:    gui.BtnShadow,
					Font:     sprites.FontD3Streetism,
					Y:        -0.75,
					Sx:       1.5,
					Sy:       1.5,
					MaxWidth: width,
				})
			}

			if md.Portrait != card.PortraitCustomEmbedded {
				c := Card{
					Card: &match.Card{
						Set: v.Match.Set,
						Def: &card.Def{
							Portrait:       md.Portrait,
							CustomPortrait: md.CustomPortrait,
						},
					},
				}

				c.initPortraitSprite(ctx)

				if title != "" {
					titleBtn := v.home.vert.Controls[len(v.home.vert.Controls)-1].(*gui.Button)
					titleBtn.MaxWidth -= 2

					v.home.vert.Controls[len(v.home.vert.Controls)-1] = &gui.Horizontal{
						Controls: []gui.Control{
							titleBtn,
							&gui.Sprite{
								Sprite: c.portraitSprite,
								X:      width / 2,
								Y:      -0.5,
								Sx:     1,
								Sy:     1,
							},
						},
					}
				} else {
					v.home.vert.Controls = append(v.home.vert.Controls, &gui.Sprite{
						Sprite: c.portraitSprite,
						Sx:     1,
						Sy:     1,
					})
				}
			}

			if md.Author != "" {
				v.home.vert.Controls = append(v.home.vert.Controls, &gui.Button{
					Label:    "by " + md.Author,
					Centered: true,
					Style:    gui.BtnShadow,
					Font:     sprites.FontD3Streetism,
					Sx:       0.65,
					Sy:       0.65,
					MaxWidth: width,
				})
			}

			if md.Description != "" {
				var lines []string

				var lastY float32

				(&card.RichDescription{
					Text: md.Description,
				}).Typeset(sprites.FontBubblegumSans, 0, 0, 0, 1, 1, width, 1e9, nil, false, func(_, y, _, _, _ float32, text string, _ sprites.Color, _ bool) {
					if len(lines) != 0 && lastY == y {
						lines[len(lines)-1] += text
					} else {
						lines = append(lines, text)
						lastY = y
					}
				})

				v.home.vert.Controls = append(v.home.vert.Controls, &gui.Button{
					Label:    strings.Join(lines, "\n"),
					X:        -width / 2,
					Style:    gui.BtnShadow,
					Font:     sprites.FontBubblegumSans,
					Sx:       1,
					Sy:       1,
					MaxWidth: width,
				})
			}

			if md.LatestChanges != "" {
				v.home.changelog = gui.Button{
					Label:    "Latest Changes",
					Centered: true,
					Style:    gui.BtnShadow,
					Font:     sprites.FontD3Streetism,
					Y:        -0.75,
					Sx:       1.5,
					Sy:       1.5,
					MaxWidth: width,
					FocusHandler: gui.FocusHandler{
						InputFocus:     v.home.host.InputFocus,
						InputFocusUp:   v.home.scroller1.Focus,
						InputFocusDown: v.home.scroller2.Focus,
					},

					OnSelect: func() {
						if v.Match.Init.External == nil {
							return
						}

						i := strings.IndexByte(v.Match.Init.Mode, '.')
						url := internal.GetConfig(ctx).ModeChangelogBaseURL + v.Match.Init.Mode[:i] + "#v" + strconv.Itoa(v.Match.Init.External.Revision)
						_ = internal.OpenURL(ctx, url)
					},
				}

				v.home.scroller1.InputFocusDown = v.home.changelog.Focus
				v.home.scroller2.InputFocusUp = v.home.changelog.Focus

				v.home.vert.Controls = append(v.home.vert.Controls, &v.home.changelog)

				var lines []string

				var lastY float32

				(&card.RichDescription{
					Text: md.LatestChanges,
				}).Typeset(sprites.FontBubblegumSans, 0, 0, 0, 1, 1, width, 1e9, nil, false, func(_, y, _, _, _ float32, text string, _ sprites.Color, _ bool) {
					if len(lines) != 0 && lastY == y {
						lines[len(lines)-1] += text
					} else {
						lines = append(lines, text)
						lastY = y
					}
				})

				v.home.vert.Controls = append(v.home.vert.Controls, &gui.Button{
					Label:    strings.Join(lines, "\n"),
					X:        -width / 2,
					Style:    gui.BtnShadow,
					Font:     sprites.FontBubblegumSans,
					Sx:       1,
					Sy:       1,
					MaxWidth: width,
				})
			}
		}

		layout := NewLayout(ctx)
		layout.InputFocus = v.home.title.InputFocus
		layout.Cards = make([]*Card, 0, 92+len(v.Match.Set.Cards))
		layout.noCheap = true

		for _, id := range match.CardsForHome(v.Match.Set) {
			def := v.Match.Set.Card(id)

			c := &Card{
				Card: &match.Card{
					Set:  v.Match.Set,
					Def:  def,
					Back: def.Rank.Back(false),
				},
			}

			if def.Rank == card.Token && def.TP == 0 {
				isBanned := false

				for _, f := range v.Match.Set.Mode.GetAll(card.FieldBannedCards) {
					bc := f.(*card.BannedCards)

					found := len(bc.Cards) == 0 && id < 128

					for _, banned := range bc.Cards {
						if id == banned {
							found = true

							break
						}
					}

					if !found {
						continue
					}

					if bc.Flags&card.BannedCardTypeMask == card.BannedCardTypeBanned {
						isBanned = true

						break
					}
				}

				if !isBanned {
					c.Active = &match.ActiveCard{
						Card: c.Card,
						Mode: match.ModeStackedToken,
						Desc: c.Card.Def.Description(c.Set),
					}
					c.ModTP = card.Num(1)
				}
			}

			layout.Cards = append(layout.Cards, c)
		}

		v.home.cards = layout.Cards

		if guard := spoilerguard.LoadData(); guard != nil && len(guard.SeenSpiedEnemies) == 32 {
			for _, c := range layout.Cards {
				if c.Def.ID < 128 && guard.SeenSpiedEnemies[c.Def.ID/4]&(1<<((c.Def.ID&3)<<1)) == 0 {
					c.Def = nil
				}
			}
		}

		v.home.deckEditor = gui.Button{
			Label:    "Deck Editor",
			Centered: true,
			Style:    gui.BtnShadow,
			Font:     sprites.FontBubblegumSans,
			Sx:       1,
			Sy:       1,
			MaxWidth: width,
			OnSelect: func() {
				v.initDeckSelect(ctx)
				v.State = StateDeckEditor
				v.deck.IsEditor = v
				v.onModeChange()
			},
			FocusHandler: gui.FocusHandler{
				InputFocus:     v.home.title.InputFocus,
				InputFocusUp:   v.home.scroller2.Focus,
				InputFocusDown: layout.Focus,
			},
		}

		v.home.scroller2.InputFocusDown = v.home.deckEditor.Focus
		layout.InputFocusUp = v.home.deckEditor.Focus

		v.home.vert.Controls = append(v.home.vert.Controls, &gui.Button{
			Label:    "Cards",
			Centered: true,
			Style:    gui.BtnShadow,
			Font:     sprites.FontD3Streetism,
			Y:        -0.75,
			Sx:       1.5,
			Sy:       1.5,
			MaxWidth: width,
		}, &v.home.deckEditor, &InlineLayout{
			Layout:  layout,
			Context: ctx,
			Scroll:  &v.home.scroll,
		})

		v.home.scroll.SetY(0)
	} else {
		v.home.scroller2.InputFocusDown = nil
	}
}

var errCardEditorIsWebOnly = errors.New("visual: card editor is only available on web version")

func (v *Visual) InitCardEditor(ctx context.Context, codes string) error {
	return errCardEditorIsWebOnly
}

func (v *Visual) updateHome(ctx context.Context) {
	select {
	case <-v.home.modeLoad:
		v.resizeHome(ctx)
		v.updateAgain = true
	default:
	}

	if v.home.deckEditor.Dy > 0 {
		v.home.host.DefaultFocus = false
		v.home.deckEditor.DefaultFocus = true
	} else {
		v.home.host.DefaultFocus = true
		v.home.deckEditor.DefaultFocus = false
	}

	if v.home.scroller1.HasFocus() || v.home.scroller2.HasFocus() {
		switch {
		case v.home.scroller1.HasFocus() && v.home.scroll.Scroll <= 0 && v.home.scroller1.InputFocusUp != nil && v.ictx.ConsumeAllowRepeat(input.BtnUp, 60, 5):
			v.home.scroller1.InputFocusUp()
		case v.home.scroller1.HasFocus() && (v.home.changelog.Dy >= 0 || v.home.deckEditor.Dy >= 0) && v.home.scroller1.InputFocusDown != nil && v.ictx.ConsumeAllowRepeat(input.BtnDown, 60, 5):
			v.home.scroller1.InputFocusDown()
		case v.home.scroller2.HasFocus() && (v.home.changelog.Dy <= 0 || v.home.deckEditor.Dy <= 0) && v.home.scroller2.InputFocusUp != nil && v.ictx.ConsumeAllowRepeat(input.BtnUp, 60, 5):
			v.home.scroller2.InputFocusUp()
		case v.home.scroller2.HasFocus() && v.home.deckEditor.Dy >= 0 && v.home.scroller2.InputFocusDown != nil && v.ictx.ConsumeAllowRepeat(input.BtnDown, 60, 5):
			v.home.scroller2.InputFocusDown()
		default:
			up := v.ictx.ConsumeAllowRepeat(input.BtnUp, 5, 0)
			down := v.ictx.ConsumeAllowRepeat(input.BtnDown, 5, 0)

			switch {
			case v.ictx.Held(input.BtnUp) && v.ictx.Held(input.BtnDown):
				v.home.scroll.Momentum /= 2
			case up:
				v.home.scroll.Wheel -= 0.25
			case down:
				v.home.scroll.Wheel += 0.25
			}
		}
	}

	v.home.scroll.Update(&v.cam, v.ictx)
}

func (v *Visual) switchVariant(ctx context.Context, i int) {
	variants := v.Match.Set.Mode.GetAll(card.FieldVariant)

	v.Match.Init.Variant = uint64(i)

	v.Match.Init.CachedVariant = variants[v.Match.Init.Variant].(*card.Variant)
	v.Match.Set.Variant = int(v.Match.Init.Variant)

	v.onModeChange()
	v.reloadDefaultStage(ctx)
	v.resizeHome(ctx)

	audio.Confirm1.PlaySoundGlobal(0, 0, 0)

	v.State = StateHome
}

func (v *Visual) updateConnecting(ctx context.Context) {
	cs, ls := v.game.State()

	if cs != scnet.StateMatchmakingWait {
		audio.Inside2.PlayMusic(0, false)
	}

	switch code, reason := v.signal.IsClosed(); code {
	case 0, internal.NormalClosure:
		switch cs {
		case scnet.StateMatchmakingWait:
			// do nothing
		case scnet.StateConnecting:
			v.message = "Connecting to opponent..."
		case scnet.StateConnected:
			switch ls {
			case scnet.StateUninitialized, scnet.StateSecureInitializing:
				v.message = "Negotiating game session..."
			case scnet.StateSecureReady:
				v.message = "Waiting for card data..."
			case scnet.StateMatchReady:
				if v.audience == nil {
					v.reloadDefaultStage(ctx)

					v.audience = room.NewAudience(v.game.SCG.Seed(scnet.RNGShared), v.Match.Rematches, v.Match.Init.Mode)
					v.stage.AddAudience(v.audience...)
				}

				v.Match.IndexSet()

				preloadAssets(ctx, v.Match.Set, v.Match.Init.Mode)

				onPlayer := func(c *room.Character) {
					p := room.NewPlayer(c)
					v.player[v.Match.Perspective-1] = p
					v.stage.AddPlayer(p, int(v.Match.Perspective))
					v.Match.Cosmetic[v.Match.Perspective-1].CharacterName = c.Name

					if err := v.game.SendCosmeticData(v.Match.Cosmetic[v.Match.Perspective-1]); err != nil {
						log.Println("WARNING: failed to send cosmetic data:", err)
					}

					v.animationTimer = 1.5 * 60
				}

				if v.npc != nil {
					onPlayer(v.npc.PickPlayer())
					v.animationTimer = 0

					v.initDeckSelect(ctx)

					return
				}

				internal.SetActive(true)

				v.message = ""
				v.mine = room.NewMine(ctx, onPlayer)
				v.State = StateCharacterSelect
			default:
				log.Println("ERROR: unhandled logical state:", cs, ls)
			}
		case scnet.StateConnectionLost:
			v.message = "Lost connection to opponent..."
		default:
			log.Println("ERROR: unhandled connection state:", cs, ls)
		}
	case internal.MMServerError:
		v.message = "Matchmaking server error."
		v.smallMessage = reason
	case internal.MMClientError:
		v.message = "Matchmaking client error."
		v.smallMessage = reason
	case internal.MMConnectionError:
		v.message = "Matchmaking connection error."
		v.smallMessage = reason
	case internal.MMTimeout:
		v.message = "Matchmaking connection timed out."
		v.smallMessage = reason
	case internal.MMNotFound:
		v.message = "Match not found."
		v.smallMessage = ""
	default:
		v.message = "Unexpected matchmaking connection status code " + strconv.Itoa(code)
		v.smallMessage = reason
	}
}

func (v *Visual) initPlayer(ctx context.Context, playerNumber scnet.Perspective) {
	if playerNumber == scnet.UnknownPerspective {
		v.message = "Enrolling in auto join..."
	} else {
		v.message = "Contacting matchmaking server..."
	}

	if AsNPC != "" {
		n, err := npc.Get(AsNPC)
		if err != nil {
			v.message = "Invalid NPC"
			v.smallMessage = err.Error()
			v.State = StateWait

			return
		}

		v.npc = n
	}

	var err error

	v.signal, err = scnet.NewSignal(ctx)
	if err != nil {
		panic(err)
	}

	err = v.signal.Init(playerNumber, v.Match.Init.Suffix())
	if err != nil {
		panic(err)
	}

	v.game.Cards = v.Match.Set

	v.game.UseSignal(v.signal, &v.Match.Init.Mode)

	switch playerNumber {
	case scnet.Player1:
		sid := v.signal.WaitSession()

		v.runSync <- func() {
			v.State = StateMatchmakingWait
			v.message = "Send this match code\nto your opponent."
			v.hostCode.Label = sid
			v.hostCode.OnSelect = func() {
				go func() {
					if err := internal.WriteClipboard(internal.GetConfig(ctx).BaseURL + "game/join/" + strings.ReplaceAll(sid, " ", "")); err != nil {
						log.Printf("WARNING: failed to write to clipboard: %+v", err)
					} else {
						audio.Confirm.PlaySoundGlobal(0, 0, 0)
					}
				}()
			}
		}
	case scnet.Player2:
		v.runSync <- func() {
			v.State = StateMatchmakingWait
			v.message = "Enter match code\nfrom opponent:"
			v.matchCode.OnChange = func(code *[]rune) {
				digits := make([]byte, 0, 25)

				for i, c := range *code {
					switch {
					case c == 'i', c == 'I', c == 'l', c == 'L':
						(*code)[i] = '1'
						digits = append(digits, '1')
					case c == 'o', c == 'O':
						(*code)[i] = '0'
						digits = append(digits, '0')
					case c >= '0' && c <= '9':
						digits = append(digits, byte(c))
					case c == 'u', c == 'U':
						(*code)[i] = ' '
					case c >= 'a' && c <= 'z':
						c += 'A' - 'a'
						(*code)[i] = c
						digits = append(digits, byte(c))
					case c >= 'A' && c <= 'Z':
						digits = append(digits, byte(c))
					case c != ' ':
						(*code)[i] = ' '
					}
				}

				if len(digits) == 24 {
					v.State = StateConnecting
					v.message = "Searching for match..."

					if err := v.signal.SetSession(string(digits)); err != nil {
						v.State = StateWait
						v.message = "Error setting session ID:"
						v.smallMessage = err.Error()
					}
				}
			}
			v.matchCode.Text = []rune(v.joinCode)
			v.matchCode.OnChange(&v.matchCode.Text)
			v.joinCode = ""
		}

	case scnet.UnknownPerspective:
		v.signal.WaitSession()

		switch p := v.signal.Perspective(); p {
		case scnet.Player1:
			v.Match.Perspective = 1
			v.Match.UnknownDeck = 2
		case scnet.Player2:
			v.Match.Perspective = 2
			v.Match.UnknownDeck = 1
		default:
			panic(fmt.Sprintf("match: unexpected player number: %d", p))
		}

		v.message = "Waiting for opponent..."

		v.stage.SetPlayer(int(v.Match.Perspective))
		v.State = StateConnecting
	}
}

func (v *Visual) initSinglePlayer(ctx context.Context) {
	v.message = "Initializing..."

	if AsNPC != "" {
		n, err := npc.Get(AsNPC)
		if err != nil {
			v.message = "Invalid NPC"
			v.smallMessage = err.Error()
			v.State = StateWait

			return
		}

		v.npc = n
	}

	npcID := VersusNPC
	if v.Match.Init.CachedVariant != nil && v.Match.Init.CachedVariant.NPC != "" {
		npcID = v.Match.Init.CachedVariant.NPC
	}

	n, err := npc.Get(npcID)
	if err != nil {
		v.message = err.Error()

		return
	}

	sc1, sc2 := scnet.MockSignal(ctx)
	v.signal = sc2

	npcInit := *v.Match.Init
	fakeSessionID := make(chan string, 1)

	go func() {
		g, err := scnet.NewGame(ctx)
		if err != nil {
			log.Panicln("ERROR: creating game conn:", err)
		}

		m := &match.Match{
			Init:        &npcInit,
			Perspective: 1,
			UnknownDeck: 2,
		}

		m.Set, err = npcInit.Cards(ctx)
		if err != nil {
			log.Panicln("ERROR: getting cards:", err)
		}

		g.Cards = m.Set
		g.UseSignal(sc1, &npcInit.Mode)

		if err = sc1.Init(scnet.Player1, npcInit.Suffix()); err != nil {
			log.Panicln("ERROR: init player 1:", err)
		}

		sid := sc1.WaitSession()
		fakeSessionID <- sid

		for {
			_, err = minimal.PlayMatch(ctx, m, g, n)
			if err != nil {
				log.Println("ERROR: from NPC:", err)
			}

			if !g.WaitAcceptRematch() {
				break
			}

			g.ClearRematchStatus()
			m.Rematches++
			m.IndexSet() // reset modified rules
			m.State = match.State{}
			m.Log = match.GameLog{}
		}
	}()

	v.game.Cards = v.Match.Set
	v.game.UseSignal(v.signal, &v.Match.Init.Mode)

	if err = sc2.Init(scnet.Player2, v.Match.Init.Suffix()); err != nil {
		log.Panicln("ERROR: init player 2:", err)
	}

	if err := sc2.SetSession(<-fakeSessionID); err != nil {
		log.Panicln("ERROR: set session id:", err)
	}

	v.stage.SetPlayer(int(v.Match.Perspective))
	v.State = StateConnecting
}

func (v *Visual) renderHome() {
	touchcontroller.SkipFrame = true

	if v.stage != nil {
		v.stage.Render()
	}

	select {
	case <-v.modeLoad:
		_, h := gfx.Size()
		y := v.home.scrollY / float32(h) * 64
		fade := uint8(0)

		if y > 0.75 {
			y = 0.75
		}

		if y > 0 {
			fade = uint8(y * 256)
		}

		v.tb.Append(sprites.Blank, 0, 0, 0, 1000, 1000, sprites.Color{R: 0, G: 0, B: 0, A: fade})

		if y >= -0.0625 && y < 0.25 && len(v.home.vert.Controls) > 1 && sprites.WideArrow.Ready() {
			fade2a := 255 - uint8(math.Abs(float64(y))*1024)
			fade2r, fade2g, fade2b := fade2a, fade2a, fade2a

			if v.home.scroller1.HasFocus() {
				fade2r = uint8(uint16(fade2a) * uint16(sprites.Yellow.R) / 255)
				fade2g = uint8(uint16(fade2a) * uint16(sprites.Yellow.G) / 255)
				fade2b = uint8(uint16(fade2a) * uint16(sprites.Yellow.B) / 255)
			}

			v.tb.Append(sprites.WideArrow, 0, -float32(h)/128+0.25, 0, 1, 1, sprites.Color{R: fade2r, G: fade2g, B: fade2b, A: fade2a})
		}

		v.home.scroll.Render(v.tb)
	default:
		if v.message == "" {
			sprites.DrawTextCentered(v.tb, sprites.FontBubblegumSans, "Fetching game mode data...", 0, 0, 0, 1.5, 1.5, sprites.White, true)
		}
	}
}

func (v *Visual) JoinMatch(ctx context.Context, code string) error {
	v.resizeHome(ctx)

	v.State = StateWait

	v.Match.Perspective = 2
	v.Match.UnknownDeck = 1

	v.stage.SetPlayer(2)

	v.joinCode = code

	go v.initPlayer(ctx, scnet.Player2)

	return nil
}
