//go:build !headless
// +build !headless

package visual

import "git.lubar.me/ben/spy-cards/card"

func (v *Visual) setReady() bool {
	mask := v.turnData.Ready[v.Match.Perspective-1]

	tp := v.Match.State.Sides[v.Match.Perspective-1].TP

	if !card.Num(-1).Less(tp) && mask != 0 {
		return false
	}

	if tp.AmountInf == 0 && tp.Amount >= 0 && !tp.NaN {
		for i, c := range v.myHand.Cards {
			if mask&(1<<i) == 0 {
				continue
			}

			modTP := v.Match.State.Sides[v.Match.Perspective-1].ModTP[c.Def.ID]
			if modTP.AmountInf < 0 {
				return false
			}

			if modTP.AmountInf > 0 {
				tp.Amount = 0

				break
			}

			tp.Amount -= c.Def.TP + modTP.Amount
		}

		if tp.Amount < 0 {
			return false
		}
	}

	v.myReady = true

	v.turnData.InHand[v.Match.Perspective-1] = 0
	v.turnData.HandID[v.Match.Perspective-1] = nil

	for i, c := range v.Match.State.Sides[v.Match.Perspective-1].Hand {
		for _, e := range c.Def.Effects {
			if e.Type == card.CondInHand {
				v.turnData.InHand[v.Match.Perspective-1] |= 1 << i
				v.turnData.HandID[v.Match.Perspective-1] = append(v.turnData.HandID[v.Match.Perspective-1], c.Def.ID)

				break
			}
		}
	}

	v.game.SendReady(v.turnData)

	return true
}
