package visual

import (
	"git.lubar.me/ben/spy-cards/gfx"
	"git.lubar.me/ben/spy-cards/gfx/sprites"
	"git.lubar.me/ben/spy-cards/input"
)

type scrollHelper struct {
	y       float32
	width   float32
	height  float32
	focused bool
}

func (sh *scrollHelper) Update(*gfx.Camera, *input.Context) {}
func (sh *scrollHelper) Render(*sprites.Batch)              {}
func (sh *scrollHelper) HasFocus() bool                     { return sh.focused }
func (sh *scrollHelper) Width() float32                     { return sh.width }
func (sh *scrollHelper) Height() float32                    { return sh.height }
func (sh *scrollHelper) SetY(y float32)                     { sh.y = y }
