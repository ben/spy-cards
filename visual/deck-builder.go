//go:build !headless
// +build !headless

package visual

import (
	"context"
	"log"
	"strings"

	"git.lubar.me/ben/spy-cards/audio"
	"git.lubar.me/ben/spy-cards/card"
	"git.lubar.me/ben/spy-cards/gfx"
	"git.lubar.me/ben/spy-cards/gfx/sprites"
	"git.lubar.me/ben/spy-cards/gui"
	"git.lubar.me/ben/spy-cards/input"
	"git.lubar.me/ben/spy-cards/internal"
	"git.lubar.me/ben/spy-cards/match"
	"git.lubar.me/ben/spy-cards/match/npc"
)

type simpleButton struct {
	gui.FocusHandler
	x, y float32
}

// DeckBuilder constructs or edits a deck.
type DeckBuilder struct {
	cam      gfx.Camera
	set      *card.Set
	rules    *card.GameRules
	picked   *Card
	picker   *Layout
	deck     *Layout
	tb       *sprites.Batch
	fh       gui.FocusHandler
	reset    simpleButton
	delete   simpleButton
	fill     simpleButton
	deckCode string
	perDeck  int

	IsEditor  *Visual
	Confirmed bool
	saved     bool
}

// NewDeckBuilder creates a deck builder.
func NewDeckBuilder(ctx context.Context, set *card.Set) *DeckBuilder {
	d := &DeckBuilder{}

	d.cam.SetDefaults()
	d.cam.Offset.Identity()
	d.cam.Rotation.Identity()
	d.cam.Position.Identity()

	d.set = set

	d.picker = NewLayout(ctx)
	d.picker.ShowRelated = true
	d.picker.noCheap = true
	d.picker.scroll.StealFocus = true
	d.picker.sh.y = -1
	d.picker.MarginTop = 0.225
	d.picker.OnSelect = func(c *Card) {
		d.picked.Def = c.Def
		d.picked.SetFlip(false)

		d.resetPicker()
	}
	d.picker.InputFocus = new(*gui.FocusHandler)
	*d.picker.InputFocus = &d.picker.FocusHandler

	rules, ok := set.Mode.Get(card.FieldGameRules).(*card.GameRules)
	if !ok {
		rules = &card.DefaultGameRules
	}

	d.perDeck = int(rules.CardsPerDeck)

	d.deck = NewLayout(ctx)
	d.deck.noCheap = true
	d.deck.MinPerRow = d.perDeck
	d.deck.MaxPerRow = d.perDeck
	d.deck.MinScale = 0.25
	d.deck.BaseScale = 0.75
	d.deck.MaxScale = 0.75
	d.deck.HoverScale = 0.5
	d.deck.CenterLastRow = true
	d.deck.AllowScroll = false
	d.deck.OnSelect = func(c *Card) {
		if c.Def == nil {
			return
		}

		c.SetFlip(true)
		c.Def = nil

		d.resetPicker()
	}
	d.deck.InputFocus = d.picker.InputFocus
	d.picker.InputFocusUp = func() {
		d.deck.Focus()
	}
	d.deck.InputFocusDown = func() {
		d.picker.Focus()
	}

	d.rules, _ = set.Mode.Get(card.FieldGameRules).(*card.GameRules)
	if d.rules == nil {
		d.rules = &card.DefaultGameRules
	}

	d.deck.Cards = make([]*Card, d.rules.CardsPerDeck)
	for i := range d.deck.Cards {
		c := &Card{Card: &match.Card{Set: set}}

		switch {
		case i < int(d.rules.BossCards):
			c.Back = card.Boss
		case i < int(d.rules.BossCards+d.rules.MiniBossCards):
			c.Back = card.MiniBoss
		default:
			c.Back = card.Enemy
		}

		d.deck.Cards[i] = c
	}

	d.resetPicker()

	return d
}

func (d *DeckBuilder) resetPicker() {
	d.picker.Cards = d.picker.Cards[:0]

	d.picked = nil

	for _, c := range d.deck.Cards {
		if c.Def == nil {
			d.picked = c

			break
		}
	}

	if d.picked != nil {
		pickedDeck := make(card.Deck, 0, len(d.deck.Cards))

		for _, c := range d.deck.Cards {
			if c.Card.Def != nil {
				pickedDeck = append(pickedDeck, c.Card.Def.ID)
			}
		}

		for _, c := range d.set.ByBack(d.picked.Back, pickedDeck) {
			d.picker.Cards = append(d.picker.Cards, &Card{
				Card: &match.Card{
					Set:  d.set,
					Def:  c,
					Back: c.Rank.Back(false),
				},
			})
		}

		d.deck.MinPerRow = d.perDeck
		d.deck.MaxPerRow = d.perDeck
		d.deck.BaseScale = 0.75
		d.deck.MaxScale = 0.75
		d.deck.HoverScale = 0.5
		d.deck.MarginBottom = 0.725
		d.deck.DefaultFocus = false
		d.picker.DefaultFocus = true
		d.picker.Focus()
		d.deck.InputFocusLeft = func() {
			*d.deck.InputFocus = &d.reset.FocusHandler
		}
		d.deck.InputFocusUp = func() {
			*d.deck.InputFocus = &d.reset.FocusHandler
		}
		d.deck.InputFocusDown = func() {
			d.picker.Focus()
		}

		d.deckCode = ""
	} else {
		d.deck.MinPerRow = 8
		d.deck.MaxPerRow = 8
		d.deck.HoverScale = 1.5
		d.deck.MarginBottom = 0.2
		d.deck.DefaultFocus = true
		d.picker.DefaultFocus = false
		d.deck.Focus()
		d.deck.InputFocusLeft = nil
		d.deck.InputFocusUp = nil
		d.deck.InputFocusDown = func() {
			*d.deck.InputFocus = &d.fh
		}

		deck := make(card.Deck, len(d.deck.Cards))
		for i, c := range d.deck.Cards {
			deck[i] = c.Def.ID
		}

		d.saved = false

		for _, s := range match.LoadDecks() {
			if len(s) != len(deck) {
				continue
			}

			match := true

			for i, c := range deck {
				if s[i] != c {
					match = false

					break
				}
			}

			if match {
				d.saved = true

				break
			}
		}

		b, err := deck.MarshalText()
		if err != nil {
			log.Printf("ERROR: deck failed to encode: %+v", err)
		}

		d.deckCode = string(b)
	}

	d.picker.Update(nil)

	if d.IsEditor != nil {
		d.IsEditor.onModeChange()
	}
}

// Update processes one frame of input.
func (d *DeckBuilder) Update(ctx context.Context) {
	d.picker.Update(nil)
	d.deck.Update(nil)

	w, h := gfx.Size()
	d.cam.Perspective.Scale(-64/float32(w), -64/float32(h), 0.01)

	if d.picked == nil {
		d.deck.BaseScale = d.deck.BaseScale*0.9 + 1.5*0.1
		d.deck.MaxScale = d.deck.MaxScale*0.9 + 2*0.1

		if *d.deck.InputFocus == &d.fh {
			switch {
			case d.deck.ictx.Consume(input.BtnConfirm):
				d.Confirmed = true
			case d.deck.ictx.Consume(input.BtnUp):
				d.deck.Focus()
			}
		}

		mx, my, click := d.deck.ictx.Mouse()
		lastX, lastY, lastClick := d.deck.ictx.LastMouse()

		switch {
		case d.IsEditor != nil && d.saved && sprites.TextHitTest(&d.cam, sprites.FontD3Streetism, d.deckCode, 0, -7.1, 0, 3, 3, mx, my, true):
			fallthrough
		case d.IsEditor != nil && !d.saved && sprites.TextHitTest(&d.cam, sprites.FontBubblegumSans, "Save This Deck", 0, -7.1, 0, 3, 3, mx, my, true):
			fallthrough
		case d.IsEditor == nil && sprites.TextHitTest(&d.cam, sprites.FontBubblegumSans, "Play With This Deck", 0, -7.1, 0, 3, 3, mx, my, true):
			if mx != lastX || my != lastY {
				*d.deck.InputFocus = &d.fh
			}

			if lastClick && !click && !d.deck.ictx.IsMouseDrag() {
				d.deck.ictx.ConsumeClick()

				d.Confirmed = true
			}
		case *d.deck.InputFocus == &d.fh && (mx != lastX || my != lastY):
			*d.deck.InputFocus = nil
		}

		if d.IsEditor != nil && d.Confirmed {
			d.Confirmed = false

			if d.saved {
				search, fragment := d.IsEditor.encodeModeState()

				mode := "vanilla"

				if strings.HasPrefix(search, "?mode=") {
					i := strings.Index(search, "&")

					if i == -1 {
						mode = search[len("?mode="):]
						search = ""
					} else {
						mode = search[len("?mode="):i]
						search = "?" + search[i+1:]
					}
				}

				basePage := "game/deck/" + mode + "/"

				go func(code string) {
					// spin up a new goroutine to avoid having the frame get delayed
					if err := internal.WriteClipboard(code); err != nil {
						log.Printf("WARNING: failed to copy deck code: %+v", err)
					} else {
						audio.Confirm.PlaySoundGlobal(0, 0, 0)
					}
				}(internal.GetConfig(ctx).BaseURL + basePage + d.deckCode + search + fragment)
			} else {
				var deck card.Deck

				if err := deck.UnmarshalText([]byte(d.deckCode)); err != nil {
					log.Printf("ERROR: failed to unmarshal deck: %+v", err)
				} else {
					match.SaveDeck(deck)

					d.deck.Cards = nil
					d.Confirmed = true
				}
			}
		}
	}

	if d.picked != nil || d.IsEditor != nil {
		if *d.deck.InputFocus == &d.reset.FocusHandler {
			switch {
			case d.deck.ictx.Consume(input.BtnConfirm):
				d.deck.Cards = nil
				d.Confirmed = true
			case d.deck.ictx.Consume(input.BtnDown):
				d.deck.Focus()
			case d.deck.ictx.Consume(input.BtnRight):
				d.deck.Focus()
			}
		}

		if *d.deck.InputFocus == &d.delete.FocusHandler {
			switch {
			case d.deck.ictx.Consume(input.BtnConfirm):
				deck := make(card.Deck, len(d.deck.Cards))
				for i, c := range d.deck.Cards {
					deck[i] = c.Def.ID
				}

				match.DeleteDeck(deck)

				d.deck.Cards = nil
				d.Confirmed = true
			case d.deck.ictx.Consume(input.BtnDown):
				d.deck.Focus()
			case d.deck.ictx.Consume(input.BtnLeft):
				d.deck.Focus()
			}
		}

		if *d.deck.InputFocus == &d.fill.FocusHandler {
			switch {
			case d.deck.ictx.Consume(input.BtnConfirm):
				d.fillDeck()
			case d.deck.ictx.Consume(input.BtnDown):
				d.deck.Focus()
			case d.deck.ictx.Consume(input.BtnLeft):
				d.deck.Focus()
			}
		}

		d.reset.x = -float32(w)/64 + 0.5
		d.reset.y = float32(h)/64 - 1.2

		d.delete.x = float32(w)/64 - 0.5
		d.delete.y = float32(h)/64 - 1.2

		d.fill.x = float32(w)/64 - 0.5
		d.fill.y = float32(h)/64 - 1.2

		for _, ch := range "Delete" {
			d.delete.x -= sprites.TextAdvance(sprites.FontBubblegumSans, ch, 1.5)
		}

		for _, ch := range "Complete My Deck" {
			d.fill.x -= sprites.TextAdvance(sprites.FontBubblegumSans, ch, 1.5)
		}

		mx, my, click := d.deck.ictx.Mouse()
		lastX, lastY, lastClick := d.deck.ictx.LastMouse()

		text := "Reset"
		if d.IsEditor != nil {
			text = "Back"
		}

		if lastX != mx || lastY != my {
			if sprites.TextHitTest(&d.cam, sprites.FontBubblegumSans, text, d.reset.x, d.reset.y, 0, 1.5, 1.5, mx, my, false) {
				*d.deck.InputFocus = &d.reset.FocusHandler
			} else if *d.deck.InputFocus == &d.reset.FocusHandler {
				*d.deck.InputFocus = nil
			}

			if d.saved {
				if sprites.TextHitTest(&d.cam, sprites.FontBubblegumSans, "Delete", d.delete.x, d.delete.y, 0, 1.5, 1.5, mx, my, false) {
					*d.deck.InputFocus = &d.delete.FocusHandler
				} else if *d.deck.InputFocus == &d.delete.FocusHandler {
					*d.deck.InputFocus = nil
				}
			}

			if d.picked != nil {
				if sprites.TextHitTest(&d.cam, sprites.FontBubblegumSans, "Complete My Deck", d.fill.x, d.fill.y, 0, 1.5, 1.5, mx, my, false) {
					*d.deck.InputFocus = &d.fill.FocusHandler
				} else if *d.deck.InputFocus == &d.fill.FocusHandler {
					*d.deck.InputFocus = nil
				}
			}
		}

		if lastClick && !click && !d.deck.ictx.IsMouseDrag() {
			switch {
			case sprites.TextHitTest(&d.cam, sprites.FontBubblegumSans, text, d.reset.x, d.reset.y, 0, 1.5, 1.5, mx, my, false):
				d.deck.ictx.ConsumeClick()

				d.deck.Cards = nil
				d.Confirmed = true
			case d.saved && sprites.TextHitTest(&d.cam, sprites.FontBubblegumSans, "Delete", d.delete.x, d.delete.y, 0, 1.5, 1.5, mx, my, false):
				d.deck.ictx.ConsumeClick()

				deck := make(card.Deck, len(d.deck.Cards))
				for i, c := range d.deck.Cards {
					deck[i] = c.Def.ID
				}

				match.DeleteDeck(deck)

				d.deck.Cards = nil
				d.Confirmed = true
			case d.picked != nil && sprites.TextHitTest(&d.cam, sprites.FontBubblegumSans, "Complete My Deck", d.fill.x, d.fill.y, 0, 1.5, 1.5, mx, my, false):
				d.deck.ictx.ConsumeClick()

				d.fillDeck()
			}
		}
	}
}

func (d *DeckBuilder) fillDeck() {
	tp2 := &npc.TourneyPlayer2{AdjustWeight: func(c *card.Def, weight float32, d card.Deck, set *card.Set) float32 { return weight }}

	totalBoss, needBoss := d.rules.BossCards, d.rules.BossCards
	totalMini, needMini := d.rules.MiniBossCards, d.rules.MiniBossCards

	deck := make(card.Deck, 0, len(d.deck.Cards))
	for _, c := range d.deck.Cards {
		if c == nil || c.Def == nil {
			continue
		}

		switch c.Def.Rank {
		case card.Boss:
			needBoss--
		case card.MiniBoss:
			needMini--
		}

		deck = append(deck, c.Def.ID)
	}

	for needBoss > 0 {
		id := tp2.SelectCard(deck, d.set, d.set.ByBack(card.Boss, deck))
		deck = append(deck[:totalBoss-needBoss], append(card.Deck{id}, deck[totalBoss-needBoss:]...)...)

		needBoss--

		for _, c := range d.deck.Cards {
			if c.Def == nil {
				c.Def = d.set.Card(id)
				c.SetFlip(false)

				break
			}
		}
	}

	for needMini > 0 {
		id := tp2.SelectCard(deck, d.set, d.set.ByBack(card.MiniBoss, deck))
		deck = append(deck[:totalBoss+totalMini-needMini], append(card.Deck{id}, deck[totalBoss+totalMini-needMini:]...)...)

		needMini--

		for _, c := range d.deck.Cards {
			if c.Def == nil {
				c.Def = d.set.Card(id)
				c.SetFlip(false)

				break
			}
		}
	}

	for len(deck) < len(d.deck.Cards) {
		id := tp2.SelectCard(deck, d.set, d.set.ByBack(card.Enemy, deck))
		deck = append(deck, id)

		for _, c := range d.deck.Cards {
			if c.Def == nil {
				c.Def = d.set.Card(id)
				c.SetFlip(false)

				break
			}
		}
	}

	d.resetPicker()
}

// Render draws the deck builder to the screen.
func (d *DeckBuilder) Render(ctx context.Context) {
	for pass := 0; pass < 3; pass++ {
		d.picker.Render(ctx, pass)
	}

	for pass := 0; pass < 3; pass++ {
		d.deck.Render(ctx, pass)
	}

	if d.tb == nil {
		d.tb = sprites.NewBatch(&d.cam)
	} else {
		d.tb.Reset(&d.cam)
	}

	switch {
	case d.picked == nil && d.IsEditor != nil && d.saved:
		sprites.DrawTextBorder(d.tb, sprites.FontBubblegumSans, "Saved Deck", 0, -5, 0, 3, 3, sprites.White, sprites.Black, true)

		borderColor := sprites.Black

		if *d.deck.InputFocus == &d.fh {
			borderColor = sprites.Rainbow
		}

		sprites.DrawTextBorder(d.tb, sprites.FontD3Streetism, d.deckCode, 0, -7.1, 0, 2, 2, effectHighlightColor, borderColor, true)

		borderColor = sprites.Black
		textColor := sprites.Gray

		if *d.deck.InputFocus == &d.reset.FocusHandler {
			borderColor = sprites.Rainbow
			textColor = sprites.White
		}

		sprites.DrawTextBorder(d.tb, sprites.FontBubblegumSans, "Back", d.reset.x, d.reset.y, 0, 1.5, 1.5, textColor, borderColor, false)

		borderColor = sprites.Black
		textColor = sprites.Gray

		if *d.deck.InputFocus == &d.delete.FocusHandler {
			borderColor = sprites.Rainbow
			textColor = sprites.White
		}

		sprites.DrawTextBorder(d.tb, sprites.FontBubblegumSans, "Delete", d.delete.x, d.delete.y, 0, 1.5, 1.5, textColor, borderColor, false)
	case d.picked == nil:
		sprites.DrawTextBorder(d.tb, sprites.FontBubblegumSans, "Tap a card to replace it, or", 0, -5, 0, 3, 3, sprites.White, sprites.Black, true)

		borderColor := sprites.Black

		if *d.deck.InputFocus == &d.fh {
			borderColor = sprites.Rainbow
		}

		label := "Play With This Deck"
		if d.IsEditor != nil {
			label = "Save This Deck"
		}

		sprites.DrawTextBorder(d.tb, sprites.FontBubblegumSans, label, 0, -7.1, 0, 3, 3, effectHighlightColor, borderColor, true)
	default:
		borderColor := sprites.Black
		textColor := sprites.Gray

		if *d.deck.InputFocus == &d.reset.FocusHandler {
			borderColor = sprites.Rainbow
			textColor = sprites.White
		}

		sprites.DrawTextBorder(d.tb, sprites.FontBubblegumSans, "Reset", d.reset.x, d.reset.y, 0, 1.5, 1.5, textColor, borderColor, false)

		borderColor = sprites.Black
		textColor = sprites.Gray

		if *d.deck.InputFocus == &d.fill.FocusHandler {
			borderColor = sprites.Rainbow
			textColor = sprites.White
		}

		sprites.DrawTextBorder(d.tb, sprites.FontBubblegumSans, "Complete My Deck", d.fill.x, d.fill.y, 0, 1.5, 1.5, textColor, borderColor, false)
	}

	d.tb.Render()
}
