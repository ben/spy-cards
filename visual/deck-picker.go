//go:build !headless
// +build !headless

package visual

import (
	"context"

	"git.lubar.me/ben/spy-cards/arcade/touchcontroller"
	"git.lubar.me/ben/spy-cards/card"
	"git.lubar.me/ben/spy-cards/gfx"
	"git.lubar.me/ben/spy-cards/gfx/sprites"
	"git.lubar.me/ben/spy-cards/gui"
	"git.lubar.me/ben/spy-cards/input"
	"git.lubar.me/ben/spy-cards/match"
)

type DeckPicker struct {
	cam     gfx.Camera
	ictx    *input.Context
	set     *card.Set
	decks   [][]*Card
	builder *DeckBuilder
	tb      *sprites.Batch

	scroll gui.Scroll
	sh     scrollHelper
	active int
	aspect float32

	reset struct {
		x, y float32
	}

	fromKB bool

	AllowCreate bool
	IsEditor    *Visual
	Selected    card.Deck
}

func NewDeckPicker(ctx context.Context, set *card.Set) *DeckPicker {
	d := &DeckPicker{
		ictx: input.GetContext(ctx),
		set:  set,
	}

	d.cam.SetDefaults()
	d.cam.Position.Identity()
	d.cam.Rotation.Identity()
	d.cam.Offset.Identity()

	d.scroll.Control = &d.sh
	d.scroll.Screen = 32

	return d
}

func (d *DeckPicker) Populate() {
	rules, _ := d.set.Mode.Get(card.FieldGameRules).(*card.GameRules)
	if rules == nil {
		rules = &card.DefaultGameRules
	}

	d.decks = d.decks[:0]

	if d.AllowCreate {
		unknownDeck := make([]*Card, rules.CardsPerDeck)

		for i := range unknownDeck {
			back := card.Enemy

			if i < int(rules.BossCards) {
				back = card.Boss
			} else if i < int(rules.BossCards+rules.MiniBossCards) {
				back = card.MiniBoss
			}

			unknownDeck[i] = &Card{
				Card: &match.Card{
					Set:  d.set,
					Back: back,
				},
			}
		}

		d.decks = append(d.decks, unknownDeck)
	}

	savedDecks := match.LoadDecks()

	for _, deck := range savedDecks {
		if deck.Validate(d.set) == nil {
			visDeck := make([]*Card, len(deck))

			for i, id := range deck {
				def := d.set.Card(id)

				visDeck[i] = &Card{
					Card: &match.Card{
						Set:  d.set,
						Def:  def,
						Back: def.Rank.Back(false),
					},
				}
			}

			d.decks = append(d.decks, visDeck)
		}
	}

	if d.IsEditor != nil {
		d.IsEditor.onModeChange()
	}
}

func (d *DeckPicker) Update(ctx context.Context) {
	touchcontroller.SkipFrame = true

	if d.builder != nil {
		d.builder.IsEditor = d.IsEditor

		if d.builder.Confirmed {
			if len(d.builder.deck.Cards) == 0 {
				d.builder = nil

				d.Populate()
			} else {
				d.Selected = make(card.Deck, len(d.builder.deck.Cards))

				for i, c := range d.builder.deck.Cards {
					d.Selected[i] = c.Def.ID
				}

				return
			}

			if d.IsEditor != nil {
				d.IsEditor.onModeChange()
			}
		} else {
			d.builder.Update(ctx)

			return
		}
	}

	if len(d.decks) == 1 && d.AllowCreate && d.IsEditor == nil {
		d.builder = NewDeckBuilder(ctx, d.set)
		d.builder.Update(ctx)

		return
	}

	mx, my, click := d.ictx.Mouse()
	lastX, lastY, lastClick := d.ictx.LastMouse()

	w, h := gfx.Size()
	d.aspect = float32(w) / float32(h)
	d.cam.Perspective.Scale(-64/float32(w), -64/float32(h), 0.01)

	if d.IsEditor != nil {
		d.reset.x = -float32(w)/64 + 0.5
		d.reset.y = float32(h)/64 - 1.2

		if sprites.TextHitTest(&d.cam, sprites.FontBubblegumSans, "Back", d.reset.x, d.reset.y, 0, 1.5, 1.5, mx, my, false) && !click && lastClick && !d.ictx.IsMouseDrag() {
			d.ictx.ConsumeClick()

			d.IsEditor.State = StateHome
			d.IsEditor.resizeHome(ctx)
			d.IsEditor.onModeChange()

			return
		}
	}

	scale := float32(1)
	top := float32(-1)

	if len(d.decks) != 0 {
		scale = float32(w) / 32 / float32(len(d.decks[0])+3) / cardWidth
		top = float32(h)/64 - cardHeight*scale
	}

	top += d.sh.y * cardHeight * 1.05 * scale

	switch {
	case d.ictx.ConsumeAllowRepeat(input.BtnUp, 30, 30):
		if d.active > 0 {
			d.active--
		} else {
			d.active = 0
		}

		d.fromKB = true
	case d.ictx.ConsumeAllowRepeat(input.BtnDown, 30, 30):
		if d.active+1 < len(d.decks) {
			if d.active < 0 {
				d.active = 0
			}

			d.active++
		}

		d.fromKB = true
	case (!d.ictx.IsMouseDrag() && lastClick && !click) || d.ictx.Consume(input.BtnConfirm):
		if d.active >= 0 && d.active < len(d.decks) {
			d.ictx.ConsumeClick()

			d.builder = NewDeckBuilder(ctx, d.set)
			d.builder.deck.ictx = d.ictx
			d.decks[d.active][0].hover = nil
			d.builder.deck.Cards = d.decks[d.active]
			d.builder.resetPicker()
			d.builder.Update(ctx)

			if d.IsEditor != nil {
				d.IsEditor.onModeChange()
			}

			return
		}
	}

	if d.fromKB {
		d.scroll.Target = float32(d.active+1) - float32(h)/64/cardHeight/scale/1.05
		d.scroll.HasTarget = true
	} else {
		d.scroll.HasTarget = false
	}

	if mx != lastX || my != lastY {
		d.fromKB = false
	}

	d.sh.height = float32(len(d.decks)) + 0.75
	d.sh.focused = true
	d.scroll.Size = cardHeight * scale * 1.05

	d.scroll.Update(&d.cam, d.ictx)

	var hovering bool

	for i := range d.decks {
		c := d.decks[i][0]
		if c.hover == nil {
			c.hover = &gui.HoverTilt{}
			c.hover.X0 = -scale * cardWidth * (float32(len(d.decks[0])) - 0.5) / 2
			c.hover.Y0 = sprites.CardFront.Y0
			c.hover.X1 = scale * cardWidth * (float32(len(d.decks[0])) - 0.5) / 2
			c.hover.Y1 = sprites.CardFront.Y1
		}

		c.hover.Y = top - scale*cardHeight*1.05*float32(i)
		c.hover.Scale = scale * (1 + float32(c.hover.Hover)/255*0.125)
		c.hover.Update(&d.cam, mx, my)

		c.hover.OffX *= 0.05
		c.hover.OffY *= 0.05

		dist := c.hover.Falloff
		if d.fromKB {
			dist = 1000

			if d.active == i {
				dist = 1
			}
		}

		if !hovering && dist < cardNearThreshold {
			if c.hover.Hover > 255-deckHoverInSpeed {
				c.hover.Hover = 255
			} else {
				c.hover.Hover += deckHoverInSpeed
				d.active = i
			}

			hovering = true
		} else {
			if c.hover.Hover > deckHoverOutSpeed {
				c.hover.Hover -= deckHoverOutSpeed
			} else {
				c.hover.Hover = 0
			}
		}
	}

	if !hovering {
		d.active = -1
	}
}

func (d *DeckPicker) Render(ctx context.Context) {
	touchcontroller.SkipFrame = true

	if d.builder != nil {
		d.builder.IsEditor = d.IsEditor
		d.builder.Render(ctx)

		return
	}

	if d.tb == nil {
		d.tb = sprites.NewBatch(&d.cam)
	} else {
		d.tb.Reset(&d.cam)
	}

	if d.IsEditor != nil {
		borderColor := sprites.Black
		textColor := sprites.Gray

		mx, my, _ := d.ictx.Mouse()
		if sprites.TextHitTest(&d.cam, sprites.FontBubblegumSans, "Back", d.reset.x, d.reset.y, 0, 1.5, 1.5, mx, my, false) {
			borderColor = sprites.Rainbow
			textColor = sprites.White
		}

		sprites.DrawTextBorder(d.tb, sprites.FontBubblegumSans, "Back", d.reset.x, d.reset.y, 0, 1.5, 1.5, textColor, borderColor, false)
	}

	for i := range d.decks {
		c0 := d.decks[i][0]
		if c0.hover == nil || c0.hover.Mins[1] > 1.5 || c0.hover.Maxs[1] < -1.5 {
			continue
		}

		if d.active != i {
			d.renderDeck(ctx, i)
		}
	}

	if d.active >= 0 && d.active < len(d.decks) {
		d.renderDeck(ctx, d.active)
	}

	d.tb.Render()
}

func (d *DeckPicker) renderDeck(ctx context.Context, i int) {
	c0 := d.decks[i][0]

	if c0 == nil || c0.hover == nil {
		return
	}

	left := float32(len(d.decks[i])-1) * cardWidth / 2

	c0.hover.SetFunHouse(d.tb)

	for j, c := range d.decks[i] {
		d.tb.BaseX = float32(j)*cardWidth - left
		c.Render(ctx, d.tb, nil)
	}

	d.tb.BaseX = 0

	if i == 0 && d.AllowCreate {
		c := sprites.Color{R: 171, G: 171, B: 171, A: 255}
		if d.active == 0 {
			c = sprites.White
		}

		sprites.DrawTextBorder(d.tb, sprites.FontBubblegumSans, "Create New Deck", 0, -0.5, 0, 2, 2, c, sprites.Black, true)
	}

	d.tb.SetFunHouse(nil)
}
