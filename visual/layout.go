//go:build !headless
// +build !headless

package visual

import (
	"context"
	"log"

	"git.lubar.me/ben/spy-cards/card"
	"git.lubar.me/ben/spy-cards/gfx"
	"git.lubar.me/ben/spy-cards/gfx/sprites"
	"git.lubar.me/ben/spy-cards/gui"
	"git.lubar.me/ben/spy-cards/input"
	"git.lubar.me/ben/spy-cards/internal"
	"git.lubar.me/ben/spy-cards/internal/router"
	"git.lubar.me/ben/spy-cards/match"
)

// Layout handles arranging a grid of cards on the screen.
type Layout struct {
	Cards []*Card

	MinPerRow int
	MaxPerRow int
	OnSelect  func(*Card)
	gui.FocusHandler
	MinScale       float32
	BaseScale      float32
	MaxScale       float32
	HoverScale     float32
	MarginTop      float32
	MarginLeft     float32
	MarginBottom   float32
	MarginRight    float32
	PaddingX       float32
	PaddingY       float32
	ScrollPadding  float32
	GrowDx, GrowDy float32

	scale  float32
	rx, ry float32
	aspect float32
	yScale float32
	sh     scrollHelper

	cam    gfx.Camera
	ictx   *input.Context
	scroll gui.Scroll

	perRow  int
	active  int
	clicked int

	cheap *CheapCardSheet
	batch *sprites.Batch

	CenterLastRow bool
	AllowScroll   bool
	Reverse       bool
	PinCorner     uint8
	Slide         bool
	SquishY       bool
	ShowRelated   bool
	DefaultFocus  bool
	fromKB        bool

	cheapUse [10 * 8]uint8
	noCheap  bool

	oneBatchPerCard bool
}

// NewLayout creates a new card layout.
func NewLayout(ctx context.Context) *Layout {
	sprites.CardFront.StartPreload()
	sprites.CardFrontWindow.StartPreload()
	sprites.Portraits[0].StartPreload()

	l := &Layout{
		MinPerRow:     3,
		MaxPerRow:     5,
		MinScale:      1.75,
		BaseScale:     2,
		MaxScale:      3.5,
		HoverScale:    1,
		PaddingX:      0.05,
		PaddingY:      0.05,
		ScrollPadding: 1,
		yScale:        1,
		AllowScroll:   true,

		ictx:    input.GetContext(ctx),
		active:  -1,
		clicked: -1,
	}

	l.cam.SetDefaults()
	l.cam.Position.Identity()
	l.cam.Rotation.Identity()
	l.cam.Offset.Identity()

	l.scroll.Control = &l.sh
	l.scroll.Screen = 32

	return l
}

// Focus attaches the input focus to this Layout.
func (l *Layout) Focus() {
	l.FocusHandler.Focus()

	if l.active < 0 || l.active >= len(l.Cards) {
		l.active = 0
		l.fromKB = true
	}
}

// Update processes one frame of input.
func (l *Layout) Update(modTP map[card.ID]card.Number) {
	if l.active >= len(l.Cards) {
		l.active = -1
	}

	realW, realH := gfx.Size()
	l.aspect = float32(realW) / float32(realH)
	l.cam.Perspective.Scale(-64.0/float32(realW), -64.0/float32(realH), -0.05)

	w := int(float32(realW) * (1 - l.MarginLeft - l.MarginRight))
	h := int(float32(realH) * (1 - l.MarginTop - l.MarginBottom))

	perRow := float32(w) / 100 / l.BaseScale
	l.scale = float32(w) / 100

	switch {
	case perRow <= float32(l.MinPerRow):
		l.scale /= float32(l.perRow)
		l.perRow = l.MinPerRow
	case perRow >= float32(l.MaxPerRow):
		l.scale /= float32(l.perRow)
		l.perRow = l.MaxPerRow
	default:
		l.scale = l.BaseScale
		l.perRow = int(perRow)
	}

	if l.scale > l.MaxScale {
		l.scale = l.MaxScale
	}

	if l.scale < l.MinScale {
		l.scale = l.MinScale
	}

	if l.PinCorner != 0 && len(l.Cards) > l.perRow {
		l.scale *= float32(l.perRow) / float32(len(l.Cards))
		l.perRow = len(l.Cards)
	}

	if l.SquishY && len(l.Cards) != 0 {
		l.yScale = (float32(h)/32/cardHeight/l.scale - 1) / float32((len(l.Cards)+l.perRow-1)/l.perRow)

		if l.yScale > 1 {
			l.yScale = 1
		}
	} else {
		l.yScale = 1
	}

	mx, my, click := l.ictx.Mouse()
	lastX, lastY, lastClick := l.ictx.LastMouse()

	allowClick := true

	if my < l.MarginTop {
		allowClick = false
	}

	if my > 1-l.MarginBottom {
		allowClick = false
	}

	if mx < l.MarginLeft {
		allowClick = false
	}

	if mx > 1-l.MarginRight {
		allowClick = false
	}

	if mx != lastX || my != lastY || mx != l.rx || my != l.ry {
		l.fromKB = false
	}

	l.rx = mx
	l.ry = my

	if l.InputFocus != nil && l.DefaultFocus && *l.InputFocus == nil {
		for _, btn := range []input.Button{input.BtnLeft, input.BtnRight, input.BtnUp, input.BtnDown} {
			if l.ictx.Held(btn) {
				l.Focus()

				break
			}
		}
	}

	if l.InputFocus == nil || *l.InputFocus == &l.FocusHandler {
		switch {
		case !l.Reverse && l.ictx.ConsumeAllowRepeat(input.BtnLeft, 30, 30),
			l.Reverse && l.ictx.ConsumeAllowRepeat(input.BtnRight, 30, 30):
			switch {
			case l.active > 0:
				l.active--
			case !l.Reverse && l.InputFocusLeft != nil:
				l.InputFocusLeft()
			case l.Reverse && l.InputFocusRight != nil:
				l.InputFocusRight()
			}

			l.fromKB = true
		case !l.Reverse && l.ictx.ConsumeAllowRepeat(input.BtnRight, 30, 30),
			l.Reverse && l.ictx.ConsumeAllowRepeat(input.BtnLeft, 30, 30):
			switch {
			case l.active+1 < len(l.Cards):
				l.active++
			case !l.Reverse && l.InputFocusRight != nil:
				l.InputFocusRight()
			case l.Reverse && l.InputFocusLeft != nil:
				l.InputFocusLeft()
			}

			l.fromKB = true
		case l.ictx.ConsumeAllowRepeat(input.BtnUp, 30, 30):
			switch {
			case l.active > l.perRow:
				l.active -= l.perRow
			case l.active > 0:
				l.active = 0
			case l.InputFocusUp != nil:
				l.InputFocusUp()
			}

			l.fromKB = true
		case l.ictx.ConsumeAllowRepeat(input.BtnDown, 30, 30):
			switch {
			case l.active+l.perRow < len(l.Cards):
				if l.active < 0 {
					l.active = 0
				}

				l.active += l.perRow
			case l.active < len(l.Cards)-1:
				l.active = len(l.Cards) - 1
			case l.InputFocusDown != nil:
				l.InputFocusDown()
			}

			l.fromKB = true
		case l.ictx.Consume(input.BtnConfirm):
			if l.active >= 0 && l.OnSelect != nil {
				l.fromKB = true
				l.OnSelect(l.Cards[l.active])
			}
		}
	}

	var (
		hoveredCard      *Card
		hoveredCardIndex int
	)

	cw := cardWidth * l.scale * (1 + l.PaddingX)
	ch := l.scale * (cardHeight + cardWidth*l.PaddingY)
	canFocus := l.InputFocus == nil || *l.InputFocus == nil || *l.InputFocus == &l.FocusHandler || !l.AllowScroll

	for i, c := range l.Cards {
		if c == nil {
			continue
		}

		if c.Active == nil || c.Active.Mode != match.ModeStackedToken {
			if c.Def != nil {
				if c.ModTP != modTP[c.Def.ID] {
					c.cachedDef = nil
				}

				c.ModTP = modTP[c.Def.ID]
			} else {
				c.ModTP = card.Num(0)
			}
		}

		numInRow := l.perRow
		if l.CenterLastRow && i/l.perRow == (len(l.Cards)-1)/l.perRow && len(l.Cards)%l.perRow != 0 {
			numInRow = len(l.Cards) % l.perRow
		}

		var baseX, baseY float32

		rowWidth := float32(numInRow-1) * cw

		switch l.PinCorner {
		case 0:
			// no pin
			centerX := (l.MarginLeft + (1 - l.MarginRight)) - 1
			baseX = -rowWidth/2 + float32(realW)/64*centerX
			baseY = l.sh.y*ch + float32(h)/64 - ch/2 - float32(realH)/64*l.MarginTop + float32(realH)/64*l.MarginBottom - l.ScrollPadding
		case 1:
			// top left
			baseX = -float32(realW)/64 + cw/2
			baseY = float32(realH)/64 - ch/2 - float32(realH)/64*l.MarginTop
		case 2:
			// top right
			baseX = float32(realW)/64 - rowWidth - cw/2
			baseY = float32(realH)/64 - ch/2 - float32(realH)/64*l.MarginTop
		case 3:
			// bottom right
			baseX = float32(realW)/64 - rowWidth - cw/2
			baseY = -float32(realH)/64 + ch/2 + float32(realH)/64*l.MarginBottom
		case 4:
			// bottom left
			baseX = -float32(realW)/64 + cw/2
			baseY = -float32(realH)/64 + ch/2 + float32(realH)/64*l.MarginBottom
		}

		if c.hover == nil {
			c.hover = &gui.HoverTilt{}
			c.hover.X0 = sprites.CardFront.X0
			c.hover.Y0 = sprites.CardFront.Y0
			c.hover.X1 = sprites.CardFront.X1
			c.hover.Y1 = sprites.CardFront.Y1
		}

		dx := i % l.perRow
		if l.Reverse {
			dx = numInRow - dx - 1
		}

		cx := float32(dx)*cw + baseX + c.hover.Dx*cw
		cy := -float32(i/l.perRow)*ch*l.yScale + baseY + c.hover.Dy*ch
		scale := l.scale + float32(c.hover.Hover)*l.HoverScale/255

		if l.Slide && (c.hover.X != 0 || c.hover.Y != 0) {
			c.hover.X = c.hover.X*0.9 + cx*0.1
			c.hover.Y = c.hover.Y*0.9 + cy*0.1

			if c.hover.Hover == 0 {
				c.hover.Scale = c.hover.Scale*0.9 + scale*0.1
			} else {
				c.hover.Scale = scale
			}
		} else {
			c.hover.X = cx
			c.hover.Y = cy
			c.hover.Scale = scale
		}

		c.hover.Update(&l.cam, l.rx, l.ry)
		dist := c.hover.Falloff

		if c.hover.Hover != 0 && (l.GrowDx != 0 || l.GrowDy != 0) {
			c.hover.X += float32(c.hover.Hover) * l.GrowDx * cw / 255
			c.hover.Y += float32(c.hover.Hover) * l.GrowDy * ch / 255
			c.hover.Update(&l.cam, l.rx, l.ry)
		}

		if !l.Slide && (l.fromKB || c.hover.Falloff >= 1000) {
			c.hover.OffX = 0
			c.hover.OffY = 0
			c.hover.Falloff = 1000
		}

		if lastClick && !click && l.clicked == l.active && l.clicked == i && dist < cardNearThreshold && c.hover.Hover != 0 && !l.ictx.IsMouseDrag() && l.OnSelect != nil && allowClick {
			l.ictx.ConsumeClick()
			l.OnSelect(c)
		}

		if l.fromKB {
			dist = 1000

			if i == l.active && (l.InputFocus == nil || *l.InputFocus == &l.FocusHandler) {
				dist = 1
			}
		}

		if dist >= cardNearThreshold {
			if internal.PrefersReducedMotion() || c.hover.Hover < cardHoverOutSpeed {
				c.hover.Hover = 0
			} else {
				c.hover.Hover -= cardHoverOutSpeed
			}
		} else if canFocus && (hoveredCard == nil || hoveredCard.hover.Hover == 0) {
			hoveredCard = c
			hoveredCardIndex = i
		}

		switch {
		case c.flipDir < 0 && c.flip < uint8(-c.flipDir):
			c.flipDir = 0
			c.flip = 0
		case c.flipDir > 0 && c.flip > 255-uint8(c.flipDir):
			c.flipDir = 0
			c.flip = 255
		default:
			c.flip += uint8(c.flipDir)
		}

		var visibleCoins int

		for _, coin := range c.coins {
			if coin.invisibleTimer != 0 {
				coin.invisibleTimer--
			}

			if coin.invisibleTimer == 0 {
				visibleCoins++
			}

			if coin.revealTimer != 0 {
				coin.revealTimer--
			}
		}

		var positionedCoins int

		for _, coin := range c.coins {
			if coin.invisibleTimer != 0 {
				continue
			}

			cols := 3

			if visibleCoins <= 4 {
				cols = 2
			}

			rows := (visibleCoins + cols - 1) / cols

			col := positionedCoins % cols
			row := positionedCoins / cols

			if row == rows-1 && visibleCoins%cols != 0 {
				cols = visibleCoins % cols
			}

			dx := (float32(col) - float32(cols)/2 + 0.5) * 0.75
			dy := -(float32(row) - float32(rows)/2 + 0.5) * 0.75

			coin.dx = coin.dx*0.9 + dx*0.1
			coin.dy = coin.dy*0.9 + dy*0.1

			positionedCoins++
		}
	}

	if !click {
		l.clicked = -1
	}

	if hoveredCard != nil {
		if l.InputFocus != nil && allowClick {
			*l.InputFocus = &l.FocusHandler
		}

		if internal.PrefersReducedMotion() || hoveredCard.hover.Hover > 255-cardHoverInSpeed {
			hoveredCard.hover.Hover = 255
			l.active = hoveredCardIndex
		} else {
			hoveredCard.hover.Hover += cardHoverInSpeed
			l.active = hoveredCardIndex
		}

		if l.clicked == -1 && click {
			l.clicked = l.active
		}
	} else if l.InputFocus != nil && *l.InputFocus == &l.FocusHandler {
		*l.InputFocus = nil
		l.active = -1
	}

	l.sh.height = float32((len(l.Cards)-1)/l.perRow)*l.yScale + 1 + l.ScrollPadding*2/ch
	l.sh.focused = l.AllowScroll && l.InputFocus != nil && *l.InputFocus == &l.FocusHandler
	l.scroll.Margin = l.MarginTop + l.MarginBottom
	l.scroll.Size = ch

	if l.fromKB && l.AllowScroll {
		l.scroll.Target = -float32(h)/32/ch/2 + 0.5 + float32(l.active/l.perRow)
		l.scroll.HasTarget = true
	} else {
		l.scroll.HasTarget = false
	}

	l.scroll.Update(&l.cam, l.ictx)
}

// Render draws the card layout to the screen.
func (l *Layout) Render(ctx context.Context, pass int) {
	cheapReady := false

	if l.batch == nil {
		l.batch = sprites.NewBatch(&l.cam)
	} else {
		l.batch.Reset(&l.cam)
	}

	// reserve some textures to hopefully keep batches in a consistent order
	l.batch.ReserveTexture(sprites.TextTexture())
	l.batch.ReserveTexture(sprites.CardFront.AssetTexture)
	l.batch.ReserveTexture(sprites.BackAttacker.AssetTexture)
	l.batch.ReserveTexture(sprites.Portraits[0x80].AssetTexture)

	for _, c := range l.Cards {
		if c != nil {
			if c.Set.PortraitSheet != nil {
				l.batch.ReserveTexture(c.Set.PortraitSheet)
			}

			break
		}
	}

	if pass == 0 && !l.noCheap {
		if l.cheap == nil {
			l.cheap = NewCheapCardSheet()
		}

		cheapReady = l.cheap.Ready(ctx)
		if cheapReady {
			for _, c := range l.cheap.cards {
				if c != nil {
					l.batch.ReserveTexture(l.cheap.atex)

					break
				}
			}

			for i := range l.cheapUse {
				if l.cheapUse[i] != 0 {
					l.cheapUse[i]--
				}
			}
		}
	}

	checkedSheet := l.oneBatchPerCard

	for _, c := range l.Cards {
		if c == nil || c.hover == nil {
			continue
		}

		if !checkedSheet {
			if c.Set.PortraitSheet == nil && len(c.Set.PortraitIndex) > 0 {
				// ad-hoc portrait sheet generation failed; use (very) slow path
				l.oneBatchPerCard = true
			}

			checkedSheet = true
		}

		if c.hover.Mins[0] > 1.5 || c.hover.Maxs[0] < -1.5 || c.hover.Mins[1] > 1.5 || c.hover.Maxs[1] < -1.5 {
			continue
		}

		switch {
		case c.hover.Falloff >= cardNearThreshold && c.hover.Hover == 0:
			if c.Active == nil || c.Active.ActiveEffect == nil {
				// render inactive cards on pass 0
				if pass == 0 {
					if !cheapReady || !l.cheapRenderCard(c) {
						l.renderCard(ctx, c)
					}
				}
			} else {
				// ...except for cards being acted upon
				if pass == 1 {
					l.renderCard(ctx, c)
				}
			}
		case c.hover.Falloff < cardNearThreshold && c.hover.Hover == 0:
			// render card behind currently active card
			if pass == 1 {
				l.renderCard(ctx, c)
			}
		case c.hover.Falloff >= cardNearThreshold && c.hover.Hover != 0:
			// render card in front of inactive cards
			if pass == 1 {
				l.renderCard(ctx, c)
			}
		case c.hover.Falloff < cardNearThreshold:
			// render active card last
			if pass == 2 {
				l.renderCard(ctx, c)
			}
		}
	}

	l.batch.Render()
}

func (l *Layout) cheapRenderCard(c *Card) bool {
	if len(l.Cards) < int(gfx.GPULevel)*10+5 {
		return false
	}

	if c.Active != nil && c.Active.ActiveEffect != nil {
		return false
	}

	if c.Active != nil && c.Active.Mode == match.ModeSetup {
		return false
	}

	if c.hover != nil && c.hover.Hover != 0 {
		return false
	}

	if c.hover != nil && c.hover.Falloff < 10 {
		return false
	}

	if c.flip != 0 && c.flip != 255 {
		return false
	}

	if c.Active != nil && c.Active.Mode == match.ModeStackedToken {
		return false
	}

	if c.Active != nil && c.Active.Mode == match.ModeNumb && c.flip == 0 {
		return false
	}

	for _, coin := range c.coins {
		if coin.revealTimer != 0 {
			return false
		}
	}

	index := -1
	blankIndex := -1

	if c.Def != nil {
		for i := range l.cheap.cards {
			if l.cheap.cards[i] != nil && l.cheap.cards[i].Set == c.Set && l.cheap.cards[i].Def.ID == c.Def.ID {
				index = i

				break
			}

			if (l.cheap.cards[i] == nil || l.cheapUse[i] == 0) && blankIndex == -1 {
				blankIndex = i
			}
		}

		if index == -1 {
			if blankIndex != -1 {
				l.cheap.SetCard(blankIndex, &Card{
					Card: &match.Card{
						Set:  c.Set,
						Def:  c.Def,
						Back: c.Back,
					},
					ModTP: c.ModTP,
				})

				l.cheapUse[blankIndex] = 31
			}

			return false
		}

		if l.cheapUse[index] == 31 {
			return false
		}

		l.cheapUse[index] = 30

		if l.cheap.Sprite(index) == nil {
			return false
		}

		if c.ModTP != l.cheap.cards[index].ModTP {
			l.cheap.SetCard(index, &Card{
				Card: &match.Card{
					Set:  c.Set,
					Def:  c.Def,
					Back: c.Back,
				},
				ModTP: c.ModTP,
			})

			l.cheapUse[index] = 31

			return false
		}
	}

	flag := sprites.RenderFlag(0)
	if c.Active != nil && c.Active.Mode.Setup() {
		flag |= sprites.FlagHalfGlow
	}

	tint := sprites.White
	if router.FlagCheapDebug.IsSet() {
		tint = sprites.Color{R: 255, G: 127, B: 255, A: 255}
	}

	c.hover.SetFunHouse(l.batch)

	if c.flip == 0 && c.Def != nil {
		l.batch.AppendEx(l.cheap.Sprite(index), 0, 0, 0, -1, 1, tint, flag, 0, 0, 0)
		c.renderOverlays(l.batch, tint)
	} else {
		c.renderOverlays(l.batch, tint)
		l.batch.AppendEx(cardBack[c.Back], 0, 0, 0, 1, 1, tint, flag, 0, 0, 0)
	}

	l.batch.SetFunHouse(nil)

	return true
}

func (l *Layout) renderCard(ctx context.Context, c *Card) {
	var e *card.EffectDef
	if c.Active != nil {
		e = c.Active.ActiveEffect
	}

	c.hover.SetFunHouse(l.batch)

	c.Render(ctx, l.batch, e)

	l.batch.SetFunHouse(nil)

	if l.oneBatchPerCard {
		l.batch.Render()
		l.batch.Reset(&l.cam)
	}
}

type InlineLayout struct {
	*Layout
	Context context.Context
	Scroll  *gui.Scroll
	Dy      float32
	cam     *gfx.Camera
}

func (l *InlineLayout) Height() float32 {
	return l.sh.height*l.scroll.Size/2 - l.scale
}

func (l *InlineLayout) Width() float32 {
	return l.sh.width
}

func (l *InlineLayout) SetY(y float32) {
	l.Dy = y
}

func (l *InlineLayout) Update(cam *gfx.Camera, ictx *input.Context) {
	l.cam = cam
	l.AllowScroll = false
	_, h := gfx.Size()
	l.sh.SetY((l.Dy*2 - float32(h)/64 + l.scale) / l.scroll.Size)
	l.Layout.Update(nil)

	if l.fromKB && l.HasFocus() {
		l.Scroll.Target = float32(l.active/l.perRow)*l.scroll.Size/2 - l.Dy + l.Scroll.Scroll
		l.Scroll.HasTarget = true
	} else {
		l.Scroll.HasTarget = false
	}
}

func (l *InlineLayout) Render(sb *sprites.Batch) {
	if l.cam != nil {
		sb.Render()
		sb.Reset(l.cam)
	} else {
		log.Println("WARNING: InlineLayout camera was nil")

		return
	}

	for pass := 0; pass < 3; pass++ {
		l.Layout.Render(l.Context, pass)
	}
}
