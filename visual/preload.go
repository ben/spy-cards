package visual

import (
	"context"
	"log"
	"net/url"
	"runtime"

	"git.lubar.me/ben/spy-cards/audio"
	"git.lubar.me/ben/spy-cards/card"
	"git.lubar.me/ben/spy-cards/format"
	"git.lubar.me/ben/spy-cards/gfx"
	"git.lubar.me/ben/spy-cards/internal"
	"git.lubar.me/ben/spy-cards/match"
	"git.lubar.me/ben/spy-cards/room"
)

func preloadAssets(ctx context.Context, set *card.Set, mode string) {
	preloadPortraits(ctx, set, mode)
	runtime.Gosched()
	preloadMusic(ctx, set, mode == match.ModeCustom)
	runtime.Gosched()
	preloadStages(ctx, set)
}

func preloadPortraits(ctx context.Context, set *card.Set, mode string) {
	set.BuildPortraitIndex()

	if set.PortraitSheet == nil && len(set.PortraitIndex) > 0 {
		switch mode {
		case "":
			// no external sheet for vanilla
		case match.ModeCustom:
			portraits := make([]string, len(set.PortraitIndex))
			for id, index := range set.PortraitIndex {
				portraits[index] = format.Encode32([]byte(id))
			}

			set.PortraitSheet = adHocPortraitSheet(ctx, portraits)
		default:
			set.PortraitSheet = gfx.NewCachedAssetTexture(internal.GetConfig(ctx).UserImageBaseURL + "mode/" + mode + ".webp")
			set.PortraitSheet.StartPreload()
		}
	}

	for _, def := range set.Cards {
		c := Card{
			Card: &match.Card{
				Set:  set,
				Def:  def,
				Back: def.Rank.Back(false),
			},
		}

		c.initPortraitSprite(ctx)

		if c.portraitSprite != nil {
			c.portraitSprite.StartPreload()
		}
	}
}

func preloadMusic(ctx context.Context, set *card.Set, isCustom bool) {
	anySummon := false

	for _, f := range set.Mode.GetAll(card.FieldSummonCard) {
		sc := f.(*card.SummonCard)
		c := set.Card(sc.ID)

		for _, e := range c.Extensions {
			if e.Type == card.ExtensionSetMusic {
				_, err := getCustomMusic(ctx, e.CID, e.LoopStart, e.LoopEnd)
				if err != nil {
					log.Println("ERROR: fetching custom music for card", c.DisplayName(), "failed:", err)
				}

				anySummon = true
			}
		}
	}

	if !anySummon {
		if isCustom {
			audio.Bounty.StartPreload()
		} else {
			audio.Miniboss.StartPreload()
		}
	}

	// fetch music that isn't summoned as a pre-game setup last
	for _, c := range set.Cards {
		for _, e := range c.Extensions {
			switch e.Type {
			case card.ExtensionSetMusic:
				_, err := getCustomMusic(ctx, e.CID, e.LoopStart, e.LoopEnd)
				if err != nil {
					log.Println("ERROR: fetching custom music for card", c.DisplayName(), "failed:", err)
				}
			case card.ExtensionPlaySound:
				_, err := getCustomSound(ctx, e.CID)
				if err != nil {
					log.Println("ERROR: fetching custom sound for card", c.DisplayName(), "failed:", err)
				}
			}
		}
	}
}

func preloadStages(ctx context.Context, set *card.Set) {
	anySummon := false

	for _, f := range set.Mode.GetAll(card.FieldSummonCard) {
		sc := f.(*card.SummonCard)
		c := set.Card(sc.ID)

		for _, e := range c.Extensions {
			switch e.Type {
			case card.ExtensionSetStageIPFS:
				go func(c *card.Def, e *card.Extension) {
					err := room.PreloadStage(ctx, "img/room/stage0001.stage", e.CID)
					if err != nil {
						log.Println("ERROR: fetching custom stage for card", c.DisplayName(), "failed:", err)
					}
				}(c, e)

				anySummon = true
			case card.ExtensionSetStage:
				go func(c *card.Def, e *card.Extension) {
					err := room.PreloadStage(ctx, "img/room/"+url.PathEscape(e.Name)+".stage", nil)
					if err != nil {
						log.Println("ERROR: fetching custom stage for card", c.DisplayName(), "failed:", err)
					}
				}(c, e)

				anySummon = true
			}
		}
	}

	if !anySummon {
		go func() {
			for _, name := range []string{
				"img/room/stage0001.stage",
				"img/room/stage0002.stage",
				"img/room/stage0003.stage",
				"img/room/stage0004.stage",
			} {
				err := room.PreloadStage(ctx, name, nil)
				if err != nil {
					log.Println("ERROR: fetching default stage:", err)
				}
			}
		}()
	}

	// fetch stages that aren't summoned as a pre-game setup last
	for _, c := range set.Cards {
		for _, e := range c.Extensions {
			switch e.Type {
			case card.ExtensionSetStageIPFS:
				go func(c *card.Def, e *card.Extension) {
					err := room.PreloadStage(ctx, "img/room/stage0001.stage", e.CID)
					if err != nil {
						log.Println("ERROR: fetching custom stage for card", c.DisplayName(), "failed:", err)
					}
				}(c, e)
			case card.ExtensionSetStage:
				go func(c *card.Def, e *card.Extension) {
					err := room.PreloadStage(ctx, "img/room/"+url.PathEscape(e.Name)+".stage", nil)
					if err != nil {
						log.Println("ERROR: fetching custom stage for card", c.DisplayName(), "failed:", err)
					}
				}(c, e)
			}
		}
	}
}
