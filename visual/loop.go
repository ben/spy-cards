package visual

import (
	"context"
	"fmt"
	"log"
	"time"

	"git.lubar.me/ben/gamepad"
	"git.lubar.me/ben/spy-cards/gfx"
	"git.lubar.me/ben/spy-cards/input"
	"git.lubar.me/ben/spy-cards/internal"
	"git.lubar.me/ben/spy-cards/internal/router"
	"git.lubar.me/ben/spy-cards/screenreader"
)

type Looper interface {
	Resize(context.Context) error
	Update(context.Context, bool) (*router.PageInfo, error)
	Render(context.Context) error
	MakeScreenReaderUI() *screenreader.UI
}

// Loop handles the graphical loop, running Update every 1/60th of a second,
// and Render every graphical frame. If the loop gets too backlogged, it will
// drop frames rather than building up pending frames infinitely.
//
// Resize is called once at the start, then once every time the screen
// resolution changes.
func Loop(ctx context.Context, looper Looper) (*router.PageInfo, error) {
	internal.RenderLoopID = internal.GoRoutineID()

	var lastW, lastH int

	ictx := input.GetContext(ctx)
	ictx.OnMouseImmediate = func() {
		redirect, err := looper.Update(ctx, false)
		if err != nil {
			panic(err)
		}

		if redirect != nil {
			panic("visual: cannot redirect from non-frame advance update")
		}
	}

	defer func() {
		ictx.OnMouseImmediate = nil
	}()

	nextUpdate := time.Now()
	skipRender := false

	var screenReaderUI *screenreader.UI

	if internal.LoadSettings().ScreenReader {
		screenReaderUI = looper.MakeScreenReaderUI()
	} else {
		screenReaderUI = screenreader.InactiveUI
	}

	screenReaderUI.Init()
	defer screenReaderUI.Release()

	for {
		outstandingFrames := 0

		now := time.Now()

		if nextUpdate.Before(now.Add(-5 * time.Second)) {
			log.Println("DEBUG: significant frame drops; skipping", now.Sub(nextUpdate).Truncate(time.Millisecond), "of frame updates to catch up")
			nextUpdate = now

			if router.FlagCrashOnDroppedFrame.IsSet() {
				go func() {
					panic("frame was dropped. crashing as requested.")
				}()
			}
		}

		for !nextUpdate.After(now) && outstandingFrames < 20 {
			if w, h := gfx.Size(); lastW != w || lastH != h {
				lastW, lastH = w, h

				if err := looper.Resize(ctx); err != nil {
					return nil, fmt.Errorf("visual: in resize: %w", err)
				}
			}

			if err := gamepad.Update(); err != nil {
				return nil, fmt.Errorf("visual: updating gamepad state: %w", err)
			}

			ictx.Tick()

			if redirect, err := looper.Update(ctx, true); err != nil {
				return nil, fmt.Errorf("visual: loop logic error: %w", err)
			} else if redirect != nil {
				return redirect, nil
			}

			nextUpdate = nextUpdate.Add(time.Second / 60)
			outstandingFrames++
		}

		if !skipRender {
			if err := looper.Render(ctx); err != nil {
				return nil, fmt.Errorf("visual: render error: %w", err)
			}

			screenReaderUI.Update()
		}

		skipRender = !gfx.NextFrame()

		if err := ctx.Err(); err != nil {
			return nil, fmt.Errorf("visual: loop cancelled: %w", err)
		}
	}
}
