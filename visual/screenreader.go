package visual

import (
	"git.lubar.me/ben/spy-cards/gfx/sprites"
	"git.lubar.me/ben/spy-cards/screenreader"
)

func (v *Visual) MakeScreenReaderUI() *screenreader.UI {
	return &screenreader.UI{
		Elements: []screenreader.Element{
			&screenreader.Wrapper{
				ElementBase: screenreader.ElementBase{
					ComputeVisible: func() bool {
						return v.State == StateHome
					},
				},
				Elements: []screenreader.Element{
					&screenreader.Label{
						ElementBase: screenreader.ElementBase{
							ComputeVisible: func() bool {
								select {
								case <-v.modeLoad:
									return false
								default:
									return v.message == ""
								}
							},
						},
						ElementPosition: screenreader.ElementPosition{
							Camera: &v.cam,
							Sx:     1.5,
							Sy:     1.5,
						},
						Font:   sprites.FontBubblegumSans,
						Center: true,
						Text: func() string {
							return "Fetching game mode data..."
						},
						VisText: func() string {
							return "Fetching game mode data..."
						},
					},
					&screenreader.TextButton{
						ElementBase: screenreader.ElementBase{
							ComputeVisible: func() bool {
								select {
								case <-v.modeLoad:
									return true
								default:
									return false
								}
							},
						},
						ElementPosition: screenreader.ElementPosition{
							Camera: &v.cam,
							Sx:     2,
							Sy:     2,
							UpdatePosition: func(pos *screenreader.ElementPosition) {
								pos.X = v.home.title.X
								pos.Y = v.home.title.Y + v.home.title.Dy
								pos.Z = v.home.title.Z
							},
						},
						Font:   sprites.FontD3Streetism,
						Center: true,
						Text: func() string {
							return v.home.title.Label + " (click to change mode)"
						},
						VisText: func() string {
							return v.home.title.Label
						},
						Click: func() {
							if v.home.title.OnSelect != nil {
								v.home.title.OnSelect()
							}
						},
					},
					&screenreader.TextButton{
						ElementBase: screenreader.ElementBase{
							ComputeVisible: func() bool {
								return v.home.variant.Button.Sx >= 1
							},
						},
						ElementPosition: screenreader.ElementPosition{
							Camera: &v.cam,
							Sx:     1.25,
							Sy:     1.25,
							UpdatePosition: func(pos *screenreader.ElementPosition) {
								pos.X = v.home.variant.Button.X
								pos.Y = v.home.variant.Button.Y + v.home.variant.Button.Dy
								pos.Z = v.home.variant.Button.Z
							},
						},
						Font:   sprites.FontD3Streetism,
						Center: true,
						Text: func() string {
							return v.home.variant.Button.Label + " (click to change mode variant)"
						},
						VisText: func() string {
							return v.home.variant.Button.Label
						},
						Click: func() {
							if v.home.variant.Button.OnSelect != nil {
								v.home.variant.Button.OnSelect()
							}
						},
					},
					&screenreader.TextButton{
						ElementBase: screenreader.ElementBase{
							ComputeVisible: func() bool {
								return v.home.host.Label != ""
							},
						},
						ElementPosition: screenreader.ElementPosition{
							Camera: &v.cam,
							Sx:     1.5,
							Sy:     1.5,
							UpdatePosition: func(pos *screenreader.ElementPosition) {
								pos.X = v.home.host.X
								pos.Y = v.home.host.Y + v.home.host.Dy
								pos.Z = v.home.host.Z
								pos.Sx = v.home.host.Sx
								pos.Sy = v.home.host.Sy
							},
						},
						Font:   sprites.FontBubblegumSans,
						Center: true,
						Text: func() string {
							return v.home.host.Label + " — " + v.home.hostDesc.Label
						},
						VisText: func() string {
							return v.home.host.Label
						},
						Click: func() {
							if v.home.host.OnSelect != nil {
								v.home.host.OnSelect()
							}
						},
					},
					&screenreader.TextButton{
						ElementBase: screenreader.ElementBase{
							ComputeVisible: func() bool {
								return v.home.watch.Label != ""
							},
						},
						ElementPosition: screenreader.ElementPosition{
							Camera: &v.cam,
							Sx:     1.5,
							Sy:     1.5,
							UpdatePosition: func(pos *screenreader.ElementPosition) {
								pos.X = v.home.watch.X
								pos.Y = v.home.watch.Y + v.home.watch.Dy
								pos.Z = v.home.watch.Z
								pos.Sx = v.home.watch.Sx
								pos.Sy = v.home.watch.Sy
							},
						},
						Font:   sprites.FontBubblegumSans,
						Center: true,
						Text: func() string {
							return v.home.watch.Label + " — " + v.home.watchDesc.Label
						},
						VisText: func() string {
							return v.home.watch.Label
						},
						Click: func() {
							if v.home.watch.OnSelect != nil {
								v.home.watch.OnSelect()
							}
						},
					},
					&screenreader.TextButton{
						ElementBase: screenreader.ElementBase{
							ComputeVisible: func() bool {
								return v.home.quick.Label != ""
							},
						},
						ElementPosition: screenreader.ElementPosition{
							Camera: &v.cam,
							Sx:     1.5,
							Sy:     1.5,
							UpdatePosition: func(pos *screenreader.ElementPosition) {
								pos.X = v.home.quick.X
								pos.Y = v.home.quick.Y + v.home.quick.Dy
								pos.Z = v.home.quick.Z
								pos.Sx = v.home.quick.Sx
								pos.Sy = v.home.quick.Sy
							},
						},
						Font:   sprites.FontBubblegumSans,
						Center: true,
						Text: func() string {
							return v.home.quick.Label + " — " + v.home.quickDesc.Label
						},
						VisText: func() string {
							return v.home.quick.Label
						},
						Click: func() {
							if v.home.quick.OnSelect != nil {
								v.home.quick.OnSelect()
							}
						},
					},
				},
			},
			&screenreader.Label{
				ElementBase: screenreader.ElementBase{
					ComputeVisible: func() bool {
						return v.message != ""
					},
				},
				ElementPosition: screenreader.ElementPosition{
					Camera: &v.cam,
					Sx:     1.5,
					Sy:     1.5,
				},
				Font:   sprites.FontBubblegumSans,
				Center: true,
				Text: func() string {
					return v.message
				},
				VisText: func() string {
					return v.message
				},
			},
			&screenreader.Label{
				ElementBase: screenreader.ElementBase{
					ComputeVisible: func() bool {
						return v.smallMessage != ""
					},
				},
				ElementPosition: screenreader.ElementPosition{
					Camera: &v.cam,
					Y:      -1,
					Sx:     0.75,
					Sy:     0.75,
				},
				Font:   sprites.FontBubblegumSans,
				Center: true,
				Text: func() string {
					return v.smallMessage
				},
				VisText: func() string {
					return v.smallMessage
				},
			},
			// TODO
		},
	}
}
