//go:build js && wasm
// +build js,wasm

package visual

import (
	"context"
	"fmt"
	"log"
	"math/bits"
	"syscall/js"

	"git.lubar.me/ben/spy-cards/gfx"
	"git.lubar.me/ben/spy-cards/internal"
	"golang.org/x/sync/errgroup"
)

func adHocPortraitSheet(ctx context.Context, ids []string) *gfx.AssetTexture {
	size := bits.Len(uint(len(ids) - 1))
	w, h := 1<<((size+1)/2), 1<<(size/2)

	canvas := js.Global().Get("document").Call("createElement", "canvas")
	canvas.Set("width", w*128)
	canvas.Set("height", h*128)

	draw := canvas.Call("getContext", "2d")

	wg, ctx := errgroup.WithContext(ctx)

	wg.Go(func() error {
		for i, id := range ids {
			x, y := i%w, i/w
			url := internal.GetConfig(ctx).UserImageBaseURL + id + ".webp"

			wg.Go(func() error {
				resp, err := internal.Await(internal.Fetch.Invoke(url, internal.FetchOpts))
				if err != nil {
					return fmt.Errorf("visual: fetching sprite %q: %w", url, err)
				}

				blob, err := internal.Await(resp.Call("blob"))
				if err != nil {
					return fmt.Errorf("visual: fetching sprite %q: %w", url, err)
				}

				img, err := gfx.CreateImageBitmap(blob, false)
				if err != nil {
					return fmt.Errorf("visual: decoding sprite %q: %w", url, err)
				}
				defer img.Call("close")

				if img.InstanceOf(js.Global().Get("ImageData")) {
					canvas := js.Global().Get("document").Call("createElement", "canvas")
					canvas.Set("width", img.Get("width"))
					canvas.Set("height", img.Get("height"))

					canvasCtx := canvas.Call("getContext", "2d")
					canvasCtx.Call("putImageData", img, 0, 0)

					defer func() {
						canvas.Set("width", 1)
						canvas.Set("height", 1)
					}()

					img = canvas
				}

				draw.Call("drawImage", img, x*128, y*128, 128, 128)

				return nil
			})
		}

		return nil
	})

	if err := wg.Wait(); err != nil {
		log.Println("WARNING: creating ad-hoc portrait sprite sheet:", err)

		// try to free up some memory
		canvas.Set("width", 1)
		canvas.Set("height", 1)

		return nil
	}

	ch := make(chan js.Value, 1)

	f := js.FuncOf(func(_ js.Value, args []js.Value) interface{} {
		ch <- args[0]

		return js.Undefined()
	})
	defer f.Release()

	canvas.Call("toBlob", f)

	blob := <-ch

	// try to free up some memory
	canvas.Set("width", 1)
	canvas.Set("height", 1)

	blobURL := internal.CreateObjectURL(blob)
	defer internal.RevokeObjectURL(blobURL)

	tex := gfx.NewAssetTexture(blobURL, false, false)

	if err := tex.Preload(); err != nil {
		log.Println("WARNING: finalizing ad-hoc portrait sprite sheet:", err)

		return nil
	}

	return tex
}
