//go:build !headless
// +build !headless

// Package visual implements the graphical interface for Spy Cards Online.
package visual

import (
	"context"
	"fmt"
	"log"
	"math"
	"math/bits"
	"net/url"
	"runtime"
	"strconv"
	"strings"
	"sync"
	"time"

	"git.lubar.me/ben/spy-cards/arcade/touchcontroller"
	"git.lubar.me/ben/spy-cards/audio"
	"git.lubar.me/ben/spy-cards/card"
	"git.lubar.me/ben/spy-cards/gfx"
	"git.lubar.me/ben/spy-cards/gfx/sprites"
	"git.lubar.me/ben/spy-cards/gui"
	"git.lubar.me/ben/spy-cards/input"
	"git.lubar.me/ben/spy-cards/internal"
	"git.lubar.me/ben/spy-cards/internal/router"
	"git.lubar.me/ben/spy-cards/match"
	"git.lubar.me/ben/spy-cards/match/npc"
	"git.lubar.me/ben/spy-cards/room"
	"git.lubar.me/ben/spy-cards/scnet"
	"git.lubar.me/ben/spy-cards/spoilerguard"
)

type Visual struct {
	cam     gfx.Camera
	ictx    *input.Context
	State   State
	runSync chan func()

	modeLoad   chan struct{}
	turnReady  chan *card.TurnData
	modeData   *match.AvailableModes
	modeChoice *Layout

	signal *scnet.Signal
	game   *scnet.Game

	mine     room.Room
	stage    *room.Stage
	audience []*room.Audience
	player   [2]*room.Player
	npc      npc.NPC

	turnData  *card.TurnData
	timerTurn uint64
	timerBank uint64

	rec   *match.Recording
	Match *match.Match
	deck  *DeckPicker
	sb    *sprites.Batch
	tb    *sprites.Batch

	home           homeLayout
	matchCode      gui.Text
	hostCode       gui.Button
	joinCode       string
	activeCard     map[*match.ActiveCard]*Card
	stackedCard    map[*match.ActiveCard]*match.ActiveCard
	myHand         *Layout
	theirHand      *Layout
	myField        *Layout
	theirField     *Layout
	mySetup        *Layout
	theirSetup     *Layout
	myDeck         *Layout
	theirDeck      *Layout
	recTimeText    string
	recVersionText string
	recordingAuto  []string
	recordingMode  string
	recordingCode  string
	recordingCh    chan string
	rematchText    string
	recordingText  string
	recordingData  []byte
	endFocus       gui.FocusHandler
	gameLogBatch   *sprites.Batch
	gameLogSH      scrollHelper
	gameLogScroll  *gui.Scroll
	syncHandResult chan error
	replacePage    *router.PageInfo

	message            string
	smallMessage       string
	nextRecordingError string
	animationTimer     uint64
	cardAnimation      []match.CardEffect
	healAmount         [2]float32
	tokenStacks        [2]map[card.ID]float32
	myHPAnim           float32
	theirHPAnim        float32
	recordingAutoNext  int
	matchComplete      chan *card.Recording
	nextRecording      chan nextRecording
	nextEffect         chan []match.CardEffect
	queuedSound        map[*match.ActiveCard]*audio.Sound
	turnLock           sync.Mutex
	networkErrorTimer  uint8
	allowQuickJoin     bool
	anyRecording       bool
	myReady            bool
	theirReady         bool
	wantUndoReady      bool
	usingCustomMusic   bool
	usingCustomStage   bool
	loadedModes        bool
	recTurn0Processed  bool
	viewingGameLog     bool
	syncHandReady      bool
	updateAgain        bool
}

func New(ctx context.Context, init *match.Init) (*Visual, error) {
	v := &Visual{
		Match: &match.Match{
			Init: init,
		},
	}

	v.Match.Log.Debug = router.FlagDebugGameLog.IsSet()

	v.ictx = input.GetContext(ctx)

	v.cam.SetDefaults()
	v.cam.Offset.Identity()
	v.cam.Rotation.Identity()
	v.cam.Position.Identity()

	v.modeLoad = make(chan struct{})
	v.turnReady = make(chan *card.TurnData)
	v.runSync = make(chan func(), 64)
	v.syncHandResult = make(chan error, 1)

	v.matchCode.Font = sprites.FontD3Streetism
	v.matchCode.Sx = 1
	v.matchCode.Sy = 1
	v.matchCode.X = -5
	v.matchCode.Y = -1.5
	v.matchCode.MaxWidth = 10
	v.matchCode.InputFocus = new(*gui.FocusHandler)
	*v.matchCode.InputFocus = &v.matchCode.FocusHandler

	v.hostCode.Font = sprites.FontD3Streetism
	v.hostCode.Sx = 1
	v.hostCode.Sy = 1
	v.hostCode.Centered = true
	v.hostCode.Y = -1.5
	v.hostCode.MaxWidth = 10
	v.hostCode.Style = gui.BtnBorder
	v.hostCode.InputFocus = new(*gui.FocusHandler)
	*v.hostCode.InputFocus = &v.hostCode.FocusHandler

	v.stage = room.NewStage(ctx, "img/room/stage0001.stage", nil)

	go v.loadMode(ctx)

	var err error

	v.game, err = scnet.NewGame(ctx)
	if err != nil {
		return nil, fmt.Errorf("visual: creating match networking connection: %w", err)
	}

	v.myHand = NewLayout(ctx)
	v.theirHand = NewLayout(ctx)
	v.myHand.InputFocus = new(*gui.FocusHandler)
	v.theirHand.InputFocus = v.myHand.InputFocus
	v.myHand.InputFocusRight = v.theirHand.Focus
	v.theirHand.InputFocusLeft = v.myHand.Focus
	v.myHand.DefaultFocus = true

	v.theirHand.Reverse = true
	v.myHand.AllowScroll = false
	v.theirHand.AllowScroll = false
	v.myHand.PaddingX = -0.125
	v.theirHand.PaddingX = -0.35
	v.myHand.MinScale = 2
	v.theirHand.MinScale = 1
	v.myHand.BaseScale = 3
	v.theirHand.BaseScale = 1.5
	v.myHand.MaxScale = 3.5
	v.theirHand.MaxScale = 2
	v.theirHand.HoverScale = 0.5
	v.myHand.MinPerRow = 50
	v.theirHand.MinPerRow = 50
	v.myHand.MaxPerRow = 50
	v.theirHand.MaxPerRow = 50
	v.myHand.PinCorner = 4
	v.theirHand.PinCorner = 3
	v.myHand.GrowDx = 0.125
	v.myHand.GrowDy = 0.25

	v.myHand.OnSelect = func(c *Card) {
		if v.State != StateTurnSelect || v.myReady {
			return
		}

		wasSelected := false
		remainingTP := v.Match.State.Sides[v.Match.Perspective-1].TP

		for i, hc := range v.myHand.Cards {
			if hc == c {
				wasSelected = v.turnData.Ready[v.Match.Perspective-1]&(1<<i) != 0

				if perRound := int(v.Match.Rules[v.Match.Perspective-1].CardsPerRound); !wasSelected && perRound != 0 && bits.OnesCount64(v.turnData.Ready[v.Match.Perspective-1]) >= perRound {
					audio.Buzzer.PlaySound(0, 0, 0, c.hover.X, c.hover.Y, 0)

					return
				}

				v.turnData.Ready[v.Match.Perspective-1] ^= 1 << i

				v.Match.RecomputePassives(v.turnData)
			}

			if v.turnData.Ready[v.Match.Perspective-1]&(1<<i) != 0 {
				cost := card.Num(hc.Def.TP)
				cost.Add(v.Match.State.Sides[v.Match.Perspective-1].ModTP[hc.Def.ID], v.Match.SpecialFlags[card.SpecialSubtractInf])
				cost.Negate()

				remainingTP.Add(cost, v.Match.SpecialFlags[card.SpecialSubtractInf])
			}
		}

		switch {
		case wasSelected:
			audio.PageFlip.PlaySound(0, 0.7, 0, c.hover.X, c.hover.Y, 0)
		case remainingTP.AmountInf < 0 || remainingTP.NaN:
			audio.Buzzer.PlaySound(0, 0, 0, c.hover.X, c.hover.Y, 0)
		case remainingTP.AmountInf > 0:
			audio.Confirm.PlaySound(0, 0, 0, c.hover.X, c.hover.Y, 0)
		case remainingTP.Amount < 0:
			audio.Buzzer.PlaySound(0, 0, 0, c.hover.X, c.hover.Y, 0)
		default:
			audio.Confirm.PlaySound(0, 0, 0, c.hover.X, c.hover.Y, 0)
		}

		played := &v.turnData.Played[v.Match.Perspective-1]
		*played = (*played)[:0]

		for i, c := range v.myHand.Cards {
			if v.turnData.Ready[v.Match.Perspective-1]&(1<<i) != 0 {
				*played = append(*played, c.Def.ID)
			}
		}

		if v.Match.SpecialFlags[card.SpecialRevealSelection] {
			v.game.SendPreviewReady(v.turnData.Ready[v.Match.Perspective-1])
		}
	}

	v.activeCard = make(map[*match.ActiveCard]*Card)
	v.stackedCard = make(map[*match.ActiveCard]*match.ActiveCard)
	v.queuedSound = make(map[*match.ActiveCard]*audio.Sound)

	v.myField = NewLayout(ctx)
	v.theirField = NewLayout(ctx)
	v.mySetup = NewLayout(ctx)
	v.theirSetup = NewLayout(ctx)
	v.myField.SquishY = true
	v.theirField.SquishY = true
	v.mySetup.SquishY = true
	v.theirSetup.SquishY = true
	v.myField.InputFocus = v.myHand.InputFocus
	v.theirField.InputFocus = v.myHand.InputFocus
	v.myField.InputFocusRight = v.theirField.Focus
	v.theirField.InputFocusLeft = v.myField.Focus
	v.myField.InputFocusDown = v.myHand.Focus
	v.theirField.InputFocusDown = v.theirHand.Focus
	v.myHand.InputFocusUp = v.myField.Focus
	v.theirHand.InputFocusUp = v.theirField.Focus

	v.myField.Slide = true
	v.theirField.Slide = true
	v.mySetup.Slide = true
	v.theirSetup.Slide = true
	v.myField.AllowScroll = false
	v.theirField.AllowScroll = false
	v.mySetup.AllowScroll = false
	v.theirSetup.AllowScroll = false
	v.myField.CenterLastRow = true
	v.theirField.CenterLastRow = true
	v.mySetup.CenterLastRow = true
	v.theirSetup.CenterLastRow = true
	v.theirField.Reverse = true
	v.theirSetup.Reverse = true
	v.myField.MarginRight = 0.5
	v.theirField.MarginLeft = 0.5
	v.mySetup.MarginRight = 0.5
	v.theirSetup.MarginLeft = 0.5
	v.myField.MinPerRow = 2
	v.theirField.MinPerRow = 2
	v.mySetup.MinPerRow = 2
	v.theirSetup.MinPerRow = 2

	v.initHome(ctx)

	return v, nil
}

func (v *Visual) loadMode(ctx context.Context) {
	set, err := v.Match.Init.Cards(ctx)
	if err != nil {
		v.message = "Failed to load game mode!"
		v.smallMessage = err.Error()
		log.Println("failed to load cards for mode:", err)

		return
	}

	v.Match.Set = set
	v.Match.IndexSet()

	switch {
	case v.Match.Init.Mode == "":
		v.allowQuickJoin = true
		v.anyRecording = true
	case v.Match.Init.External != nil:
		v.allowQuickJoin = v.Match.Init.External.QuickJoin
		v.anyRecording = v.Match.Init.External.AnyRecording
	default:
		v.allowQuickJoin = false
		v.anyRecording = false
	}

	v.runSync <- func() {
		v.reloadDefaultStage(ctx)
	}

	close(v.modeLoad)
}

type nextRecording struct {
	code string
	rec  *match.Recording
}

func (v *Visual) fetchNextRecording(ctx context.Context) {
retry:
	code, err := card.RandomRecording(ctx, v.recordingMode, v.recordingAuto...)

	if err != nil {
		log.Println("ERROR: getting next recording id:", err)

		v.runSync <- func() {
			v.nextRecordingError = err.(interface{ Unwrap() error }).Unwrap().Error()
		}

		time.Sleep(5 * time.Second)

		goto retry
	}

	v.runSync <- func() {
		v.nextRecordingError = ""
	}

	if len(v.recordingAuto) != cap(v.recordingAuto) {
		v.recordingAuto = append(v.recordingAuto, code)
	} else {
		copy(v.recordingAuto, v.recordingAuto[1:])
		v.recordingAuto[len(v.recordingAuto)-1] = code
	}

	rec, err := card.FetchRecording(ctx, code)
	if err != nil {
		log.Println("ERROR: fetching next recording:", err)

		time.Sleep(5 * time.Second)

		goto retry
	}

	matchRec, err := match.NewRecording(ctx, rec)
	if err != nil {
		log.Println("ERROR: preparing next recording:", err)

		time.Sleep(5 * time.Second)

		goto retry
	}

	v.nextRecording <- nextRecording{
		code: code,
		rec:  matchRec,
	}

	m := matchRec.CreateMatch()

	preloadAssets(ctx, m.Set, m.Init.Mode)
}

func (v *Visual) SetRecording(ctx context.Context, code string) {
	<-v.modeLoad

	v.State = StatePlaybackFetch

	v.Match.Log.Disabled = true

	go func() {
		var matchRec *match.Recording

		if code == "auto" {
			if v.nextRecording != nil {
				v.Match = &match.Match{
					Init: &match.Init{
						Version: internal.Version,
					},
					Log: match.GameLog{
						Disabled: true,
					},
				}

				next := <-v.nextRecording

				code = next.code
				matchRec = next.rec
			} else {
				v.recordingAuto = make([]string, 0, 50)
				v.nextRecording = make(chan nextRecording, 1)
				v.recordingMode = v.Match.Init.Mode

				if i := strings.Index(v.recordingMode, "."); i != -1 {
					v.recordingMode = v.recordingMode[:i]
				}

				randomID, err := card.RandomRecording(ctx, v.recordingMode, v.recordingAuto...)
				if err != nil {
					v.runSync <- func() {
						v.State = StateWait
						v.message = "Failed to get next recording ID"
						v.smallMessage = err.Error()
					}

					return
				}

				if len(v.recordingAuto) != cap(v.recordingAuto) {
					v.recordingAuto = append(v.recordingAuto, randomID)
				} else {
					copy(v.recordingAuto, v.recordingAuto[1:])
					v.recordingAuto[len(v.recordingAuto)-1] = randomID
				}

				code = randomID
			}

			log.Println("INFO: random recording ID:", code)

			go v.fetchNextRecording(ctx)
		}

		if matchRec == nil {
			rec, err := card.FetchRecording(ctx, code)
			if err != nil {
				v.runSync <- func() {
					v.State = StateWait
					v.message = "Failed to fetch recording"
					v.smallMessage = err.Error()
				}

				return
			}

			matchRec, err = match.NewRecording(ctx, rec)
			if err != nil {
				v.runSync <- func() {
					v.State = StateWait
					v.message = "Failed to preprocess recording"
					v.smallMessage = err.Error()
				}

				return
			}
		}

		v.runSync <- func() {
			var err error

			v.recordingCode = code

			rec := matchRec.Recording
			v.rec = matchRec

			v.game.SCG, err = scnet.NewSecureCardGame(&scnet.SecureCardGameOptions{
				ForReplay: &rec.Version,
			})
			if err != nil {
				v.runSync <- func() {
					v.State = StateWait
					v.message = "Failed to prepare RNG for recording"
					v.smallMessage = err.Error()
				}

				return
			}

			copy(v.game.SCG.Seed(scnet.RNGShared), rec.SharedSeed[:])

			spoilerguard.Apply(ctx, matchRec.Set, nil, true)

			v.Match.Set = matchRec.Set
			v.Match.Init = matchRec.CreateMatch().Init

			v.Match.IndexSet()

			preloadAssets(ctx, v.Match.Set, rec.ModeName)

			v.player[0] = room.NewPlayer(room.CharacterByName[rec.Cosmetic[0].CharacterName])
			v.player[1] = room.NewPlayer(room.CharacterByName[rec.Cosmetic[1].CharacterName])

			v.stage.AddPlayer(v.player[0], 1)
			v.stage.AddPlayer(v.player[1], 2)

			player := 0

			if rec.Perspective == 2 {
				player = 1

				v.stage.SetPlayer(2)
			} else {
				v.stage.SetPlayer(0)
			}

			v.Match.Perspective = uint8(player) + 1

			v.audience = room.NewAudience(rec.SharedSeed[:], rec.RematchCount, rec.ModeName)
			v.stage.ClearAudience()
			v.stage.AddAudience(v.audience...)

			v.stage.UpdateCamera = room.DefaultUpdateCamera

			v.initDecksForRecording(ctx, matchRec, player)

			if !rec.Start.IsZero() {
				v.recTimeText = "Recorded " + rec.Start.Format("2 Jan 2006")
			}

			version := []byte("v")
			version = strconv.AppendUint(version, rec.Version[0], 10)
			version = append(version, '.')
			version = strconv.AppendUint(version, rec.Version[1], 10)
			version = append(version, '.')
			version = strconv.AppendUint(version, rec.Version[2], 10)

			if rec.ModeName != "" {
				version = append(version, '-')
				version = append(version, rec.ModeName...)
			}

			v.recVersionText = string(version)

			v.State = StatePlaybackInit
			v.recTurn0Processed = false

			v.myHand.MinPerRow = int(v.Match.Rules[v.Match.Perspective-1].HandMaxSize)
			v.theirHand.MinPerRow = int(v.Match.Rules[2-v.Match.Perspective].HandMaxSize)
			v.myHand.MaxPerRow = int(v.Match.Rules[v.Match.Perspective-1].HandMaxSize)
			v.theirHand.MaxPerRow = int(v.Match.Rules[2-v.Match.Perspective].HandMaxSize)

			v.usingCustomMusic = false
			v.usingCustomStage = false
		}
	}()
}

func (v *Visual) initDecksForRecording(ctx context.Context, rec *match.Recording, player int) {
	v.myDeck = NewLayout(ctx)
	v.theirDeck = NewLayout(ctx)
	v.myDeck.ShowRelated = true
	v.theirDeck.ShowRelated = true

	rules, _ := rec.Set.Mode.Get(card.FieldGameRules).(*card.GameRules)
	if rules == nil {
		rules = &card.DefaultGameRules
	}

	v.myDeck.InputFocus = new(*gui.FocusHandler)
	*v.myDeck.InputFocus = &v.myDeck.FocusHandler
	v.theirDeck.InputFocus = v.myDeck.InputFocus
	v.myDeck.InputFocusDown = v.theirDeck.Focus
	v.theirDeck.InputFocusUp = v.myDeck.Focus

	v.myDeck.MinPerRow = int(rules.CardsPerDeck+1) / 2
	v.myDeck.MaxPerRow = v.myDeck.MinPerRow
	v.theirDeck.MinPerRow = v.myDeck.MinPerRow
	v.theirDeck.MaxPerRow = v.myDeck.MinPerRow
	v.myDeck.CenterLastRow = true
	v.theirDeck.CenterLastRow = true
	v.myDeck.AllowScroll = false
	v.theirDeck.AllowScroll = false
	v.myDeck.PaddingX = 0
	v.theirDeck.PaddingX = 0
	v.myDeck.MinScale = 1
	v.myDeck.BaseScale = 1
	v.myDeck.MaxScale = 1
	v.myDeck.HoverScale = 2
	v.theirDeck.MinScale = 1
	v.theirDeck.BaseScale = 1
	v.theirDeck.MaxScale = 1
	v.theirDeck.HoverScale = 2
	v.myDeck.MarginBottom = 0.5
	v.theirDeck.MarginTop = 0.5
	v.myDeck.SquishY = true
	v.theirDeck.SquishY = true

	v.myDeck.Cards = make([]*Card, len(rec.Recording.InitialDeck[player]))
	v.theirDeck.Cards = make([]*Card, len(rec.Recording.InitialDeck[1-player]))

	for i, id := range rec.Recording.InitialDeck[player] {
		c := rec.Set.Card(id)

		v.myDeck.Cards[i] = &Card{
			Card: &match.Card{
				Set:  rec.Set,
				Def:  c,
				Back: c.Rank.Back(false),
			},
		}
	}

	for i, id := range rec.Recording.InitialDeck[1-player] {
		c := rec.Set.Card(id)

		v.theirDeck.Cards[i] = &Card{
			Card: &match.Card{
				Set:  rec.Set,
				Def:  c,
				Back: c.Rank.Back(false),
			},
		}
	}
}

func (v *Visual) Resize(ctx context.Context) error {
	w, h := gfx.Size()
	v.cam.Perspective.Scale(-128/float32(w), -128/float32(h), -0.01)

	switch v.State {
	case StateHome:
		v.resizeHome(ctx)
	case StateDeckEditor:
		internal.SetTitle("Deck Editor - Spy Cards Online")
	}

	if w < h {
		v.theirHand.MarginBottom = 0.45
	} else {
		v.theirHand.MarginBottom = 0
	}

	return nil
}

func (v *Visual) Update(ctx context.Context, frameAdvance bool) (*router.PageInfo, error) {
	if !frameAdvance {
		if v.State == StateHome {
			v.ictx.Tick()

			v.updateHome(ctx)
		}

		if v.State == StateMatchComplete {
			v.ictx.Tick()

			return v.Update(ctx, true)
		}

		return nil, nil
	}

	touchcontroller.SkipFrame = true

	for {
		select {
		case f := <-v.runSync:
			f()

			continue
		default:
		}

		break
	}

	w, h := gfx.Size()

	curX, curY, click := v.ictx.Mouse()
	lastX, lastY, lastClick := v.ictx.LastMouse()

	if v.stage != nil {
		v.stage.Update()
	}

	if v.Match.Perspective != 0 && v.game != nil && v.player[2-v.Match.Perspective] == nil {
		if c := v.game.RecvCosmeticData(); c != nil {
			v.Match.Cosmetic[2-v.Match.Perspective] = *c
			p := room.NewPlayer(room.CharacterByName[c.CharacterName])
			v.player[2-v.Match.Perspective] = p
			v.stage.AddPlayer(p, int(3-v.Match.Perspective))
		}
	}

again:
	switch v.State {
	case StateHome:
		v.updateHome(ctx)
	case StateConnecting:
		v.updateConnecting(ctx)
	case StateWait:
		// do nothing
	case StateMatchmakingWait:
		if v.hostCode.Label == "" {
			v.matchCode.Update(&v.cam, v.ictx)
		} else {
			v.hostCode.Update(&v.cam, v.ictx)
		}

		if cs, _ := v.game.State(); cs != scnet.StateMatchmakingWait {
			v.State = StateConnecting

			goto again
		}
	case StateCharacterSelect:
		v.mine.Update()

		if v.animationTimer > 0 {
			v.animationTimer--

			if v.animationTimer == 0 {
				v.mine = nil

				v.initDeckSelect(ctx)

				// run next update immediately
				goto again
			}
		}
	case StateDeckSelect:
		v.deck.Update(ctx)

		if v.deck.Selected != nil {
			match.SaveDeck(v.deck.Selected)

			v.game.SendDeck(v.deck.Selected, v.Match.SpecialFlags[card.SpecialEffectBack])

			deck := make([]*match.Card, len(v.deck.Selected))

			for i, id := range v.deck.Selected {
				c := v.Match.Set.Card(id)

				deck[i] = &match.Card{
					Set:  v.Match.Set,
					Def:  c,
					Back: c.Rank.Back(v.Match.SpecialFlags[card.SpecialEffectBack]),
				}
			}

			v.Match.State.Sides[v.Match.Perspective-1].Deck = deck

			v.deck = nil
			v.State = StateReadyToStart
		}
	case StateReadyToStart:
		if v.updateReadyToStart(ctx) {
			goto again
		}
	case StateNextRound:
		if v.rec != nil {
			v.State = StatePlaybackNext
			v.Match.Log = match.GameLog{
				Disabled: true,
				Debug:    router.FlagDebugGameLog.IsSet(),
			}

			goto again
		}

		if w := v.Match.Winner(); w != 0 {
			v.State = StateMatchCompleteWait

			v.matchComplete = make(chan *card.Recording, 1)

			go func() {
				rec, err := v.Match.Finalize(v.game)
				if err != nil {
					v.abortMatch(err.Error())
				}

				v.matchComplete <- rec
			}()

			goto again
		}

		go v.game.BeginTurn(v.turnReady)
		v.Match.State.Round++
		v.State = StateNextRoundWait
		v.turnData = nil
		v.animationTimer = 1.5 * 60
		v.myReady = false
		v.theirReady = false
		v.wantUndoReady = false

		if v.goFast() {
			v.animationTimer = 1
		}

		fallthrough
	case StateNextRoundWait:
		if v.updateGameLog() {
			break
		}

		if v.turnData == nil {
			select {
			case t := <-v.turnReady:
				if v.Match.State.Round == 1 {
					v.Match.State.Round--
					v.Match.InitState(v.game.SCG.Rand(scnet.RNGShared), v.game, func(ac *match.ActiveCard) {
						v.checkExtensionFields(ctx, ac)
					})
					v.Match.Start = time.Now()
					v.Match.State.Round++
				}

				v.turnData = t
				v.syncHandReady = false

				v.Match.ShuffleAndDraw(v.game.SCG, false)

				go func() {
					v.syncHandResult <- v.Match.SyncInHand(v.game.ExchangeInHand, v.turnData)
				}()

				v.Match.RecomputePassives(t)

				perspective := v.Match.Perspective
				if v.Match.SpecialFlags[card.SpecialBothPlayersAreP1] {
					perspective = 1
				}

				v.myHPAnim = float32(v.Match.State.Sides[perspective-1].HP.Amount)
				v.theirHPAnim = float32(v.Match.State.Sides[2-perspective].HP.Amount)
			default:
			}
		}

		select {
		case err := <-v.syncHandResult:
			if err != nil {
				log.Panicf("ERROR: failed to sync cards in hand: %+v", err)
			}

			v.syncHandReady = true
		default:
		}

		if v.animationTimer != 0 {
			v.animationTimer--

			v.mySetup.Update(v.Match.State.Sides[v.Match.Perspective-1].ModTP)
			v.theirSetup.Update(v.Match.State.Sides[2-v.Match.Perspective].ModTP)
		} else if v.turnData != nil && v.syncHandReady {
			v.resetHandSetup()

			for i := 0; i < 120; i++ {
				// try to get cards to a stable position
				v.updateStackedTokens()
				v.myField.Update(v.Match.State.Sides[v.Match.Perspective-1].ModTP)
				v.theirField.Update(v.Match.State.Sides[2-v.Match.Perspective].ModTP)
			}

			v.animationTimer = 0.5 * 60

			v.State = StateTurnSelect

			v.timerBank += v.timerTurn
			if v.timerBank > v.Match.Timer.MaxTime*60 {
				v.timerBank = v.Match.Timer.MaxTime * 60
			}

			v.timerTurn = v.timerBank
			if v.timerTurn > v.Match.Timer.MaxPerTurn*60 {
				v.timerTurn = v.Match.Timer.MaxPerTurn * 60
			}

			v.timerBank -= v.timerTurn
			v.timerBank += v.Match.Timer.PerTurn * 60

			v.myHand.Focus()

			goto again
		}
	case StateTurnSelect:
		if v.updateGameLog() {
			break
		}

		if v.npc != nil && !v.myReady {
			ready := v.npc.PlayRound(v.Match)
			v.turnData.Ready[v.Match.Perspective-1] = ready
			v.turnData.Played[v.Match.Perspective-1] = make([]card.ID, 0, bits.OnesCount64(ready))

			for i, c := range v.Match.State.Sides[v.Match.Perspective-1].Hand {
				if ready&(1<<i) != 0 {
					v.turnData.Played[v.Match.Perspective-1] = append(v.turnData.Played[v.Match.Perspective-1], c.Def.ID)
				}
			}

			if !v.setReady() {
				log.Println("ERROR: NPC returned invalid play; playing nothing instead")

				v.turnData.Played[v.Match.Perspective-1] = nil
				v.turnData.Ready[v.Match.Perspective-1] = 0

				v.setReady()
			}
		}

		for i, c := range v.myHand.Cards {
			if c.hover == nil {
				continue
			}

			if v.turnData.Ready[v.Match.Perspective-1]&(1<<i) != 0 {
				c.hover.Dy = c.hover.Dy*0.8 + 0.25*0.2
			} else {
				c.hover.Dy *= 0.8
			}
		}

		if v.Match.SpecialFlags[card.SpecialRevealSelection] {
			tap := v.game.PreviewReady()

			for i, c := range v.theirHand.Cards {
				if c.hover == nil {
					continue
				}

				if tap&(1<<i) != 0 {
					c.hover.Dy = c.hover.Dy*0.8 + 0.25*0.2
				} else {
					c.hover.Dy *= 0.8
				}
			}
		}

		v.updateStackedTokens()
		v.myHand.Update(v.Match.State.Sides[v.Match.Perspective-1].ModTP)
		v.theirHand.Update(v.Match.State.Sides[2-v.Match.Perspective].ModTP)
		v.myField.Update(v.Match.State.Sides[v.Match.Perspective-1].ModTP)
		v.theirField.Update(v.Match.State.Sides[2-v.Match.Perspective].ModTP)

		endY := float32(h)/128 - 1
		if v.Match.Timer.MaxTime != 0 {
			endY -= 0.5
		}

		wantEndTurn := false

		endTurnText := " End Turn"
		if v.myReady {
			endTurnText = " Undo End Turn"
		}

		if sprites.TextHitTest(&v.cam, sprites.FontBubblegumSans, sprites.Button(input.BtnSwitch)+endTurnText, 0, endY, 0, 1, 1, curX, curY, true) {
			if curX != lastX || curY != lastY {
				*v.myHand.InputFocus = &v.endFocus
			}

			if lastClick && !click && !v.ictx.IsMouseDrag() {
				v.ictx.ConsumeClick()
				wantEndTurn = true
			}
		} else if *v.myHand.InputFocus == &v.endFocus && (curX != lastX || curY != lastY) {
			*v.myHand.InputFocus = nil
		}

		wantEndTurn = wantEndTurn || v.myHand.ictx.Consume(input.BtnSwitch)

		if !v.myReady && v.Match.SpecialFlags[card.SpecialAutoReady] && !card.Num(-1).Less(v.Match.State.Sides[v.Match.Perspective-1].TP) {
			v.setReady()

			wantEndTurn = false
		}

		if wantEndTurn {
			switch {
			case !v.myReady && v.setReady():
				audio.Confirm1.PlaySoundGlobal(0, 0, 0)
			case !v.myReady:
				audio.Buzzer.PlaySoundGlobal(0, 0, 0)
			case !v.wantUndoReady:
				v.game.UndoReady()
			}
		}

		if !v.myReady && v.timerTurn != 0 {
			v.timerTurn--

			if v.timerTurn == 0 {
				log.Println("DEBUG: timer ran out")

				if !v.setReady() {
					log.Println("DEBUG: not enough TP to play current hand; playing nothing")

					v.turnData.Played[v.Match.Perspective-1] = nil
					v.turnData.Ready[v.Match.Perspective-1] = 0

					v.setReady()
				} else {
					log.Println("DEBUG: playing current hand as-is")
				}
			}
		}

		v.myReady, v.wantUndoReady = v.game.SentReady()
		v.theirReady = v.game.OpponentReady()

		if v.animationTimer > 0 {
			v.animationTimer--
		} else if v.game.RecvReady(v.turnData) {
			v.beginProcessTurn(ctx)

			goto again
		}
	case StateTurnProcess:
		if v.updateGameLog() {
			break
		}

		if v.updateTurn(ctx) {
			goto again
		}
	case StateTurnEnd:
		if v.updateGameLog() {
			break
		}

		if v.animationTimer == 0 {
			v.State = StateNextRound

			v.myField.Slide = true
			v.theirField.Slide = true

			goto again
		}

		animProgress := (1 - float32(v.animationTimer)/60/2)

		if v.animationTimer >= 90 {
			v.updateStackedTokens()
		}

		v.myField.Update(v.Match.State.Sides[v.Match.Perspective-1].ModTP)
		v.theirField.Update(v.Match.State.Sides[2-v.Match.Perspective].ModTP)

		if v.animationTimer == 90 {
			for i, c := range v.myField.Cards {
				if c != nil && c.Active != nil && c.Active.StayOnField && c.Active.Mode.Setup() {
					v.myField.Cards[i] = nil
					v.mySetup.Cards = append(v.mySetup.Cards, c)
				}
			}

			for i, c := range v.theirField.Cards {
				if c != nil && c.Active != nil && c.Active.StayOnField && c.Active.Mode.Setup() {
					v.theirField.Cards[i] = nil
					v.theirSetup.Cards = append(v.theirSetup.Cards, c)
				}
			}

			for i, c := range v.myField.Cards {
				if c != nil && c.Active != nil && c.Active.StayOnField {
					v.myField.Cards[i] = nil
					v.mySetup.Cards = append(v.mySetup.Cards, c)
				}
			}

			for i, c := range v.theirField.Cards {
				if c != nil && c.Active != nil && c.Active.StayOnField {
					v.theirField.Cards[i] = nil
					v.theirSetup.Cards = append(v.theirSetup.Cards, c)
				}
			}
		}

		if v.animationTimer == 60 {
			if v.Match.SpecialFlags[card.SpecialBothPlayersAreP1] {
				if v.Match.State.RoundWinner == 2 && v.player[2-v.Match.Perspective] != nil {
					v.player[2-v.Match.Perspective].BecomeAngry(5 * time.Second)
				}

				if v.Match.State.RoundWinner == 2 && v.player[v.Match.Perspective-1] != nil {
					v.player[v.Match.Perspective-1].BecomeAngry(5 * time.Second)
				}
			} else {
				if v.Match.State.RoundWinner == v.Match.Perspective && v.player[2-v.Match.Perspective] != nil {
					v.player[2-v.Match.Perspective].BecomeAngry(5 * time.Second)
				}

				if v.Match.State.RoundWinner == 3-v.Match.Perspective && v.player[v.Match.Perspective-1] != nil {
					v.player[v.Match.Perspective-1].BecomeAngry(5 * time.Second)
				}
			}
		}

		v.mySetup.Update(v.Match.State.Sides[v.Match.Perspective-1].ModTP)
		v.theirSetup.Update(v.Match.State.Sides[2-v.Match.Perspective].ModTP)

		switch {
		case animProgress >= 0.25:
			// sweep
			animProgress = (animProgress - 0.25) / 0.75

			perspective := v.Match.Perspective
			if v.Match.SpecialFlags[card.SpecialBothPlayersAreP1] {
				perspective = 1
			}

			if perspective == v.Match.State.RoundWinner {
				for _, c := range v.myField.Cards {
					if c == nil || c.hover == nil {
						continue
					}

					c.hover.X += float32(w)/128 + animProgress*animProgress*animProgress*float32(w)/4
				}
			} else {
				for _, c := range v.myField.Cards {
					if c == nil || c.hover == nil {
						continue
					}

					c.hover.X -= animProgress*float32(w)/16 - float32(w)/128
					c.hover.Y -= animProgress * animProgress * float32(w) / 8
					c.hover.OffX = c.hover.X * (1 + 4*animProgress)
					c.hover.OffY = c.hover.Y * (1 + 4*animProgress)
					c.hover.Falloff = 1.125 - animProgress
				}
			}

			if 3-perspective == v.Match.State.RoundWinner {
				for _, c := range v.theirField.Cards {
					if c == nil || c.hover == nil {
						continue
					}

					c.hover.X -= float32(w)/128 + animProgress*animProgress*animProgress*float32(w)/4
				}
			} else {
				for _, c := range v.theirField.Cards {
					if c == nil || c.hover == nil {
						continue
					}

					c.hover.X += animProgress*float32(w)/16 - float32(w)/128
					c.hover.Y -= animProgress * animProgress * float32(w) / 8
					c.hover.OffX = c.hover.X * (1 + 4*animProgress)
					c.hover.OffY = c.hover.Y * (1 + 4*animProgress)
					c.hover.Falloff = 1.125 - animProgress
				}
			}
		case animProgress >= 0:
			// move towards
			for _, c := range v.myField.Cards {
				c.hover.X += animProgress * animProgress * float32(w) / 8
			}

			for _, c := range v.theirField.Cards {
				c.hover.X -= animProgress * animProgress * float32(w) / 8
			}
		}

		v.animationTimer--
	case StateMatchCompleteWait:
		if v.updateGameLog() {
			break
		}

		select {
		case rec := <-v.matchComplete:
			b, err := rec.MarshalBinary()
			if err != nil {
				log.Printf("ERROR: encoding match recording: %+v", err)
			}

			v.recordingData = b

			if internal.LoadSettings().AutoUploadRecording {
				v.uploadRecording(ctx)
			}

			go checkForUpdates()

			audio.StopMusic()

			if v.Match.SpecialFlags[card.SpecialBothPlayersAreP1] {
				if v.Match.Winner() == 2 {
					v.player[0].BecomeAngry(-1)
					v.player[1].BecomeAngry(-1)
				}

				if v.Match.Winner() == 1 {
					audio.CrowdClap.PlaySoundGlobal(0, 0, 0)
					v.Match.Wins++
				} else {
					audio.CrowdGasp.PlaySoundGlobal(0, 0.75, 1)
					v.Match.Losses++
				}
			} else {
				v.player[2-v.Match.Winner()].BecomeAngry(-1)

				if v.Match.Perspective == v.Match.Winner() {
					audio.CrowdClap.PlaySoundGlobal(0, 0, 0)
					v.Match.Wins++
				} else {
					audio.CrowdGasp.PlaySoundGlobal(0, 0.75, 1)
					v.Match.Losses++
				}

				if winPlayer := v.player[v.Match.Winner()-1]; winPlayer != nil && !internal.PrefersReducedMotion() {
					var m gfx.Matrix

					m.Translation(winPlayer.X, winPlayer.Character.Sprites[0].Y1-(winPlayer.Character.Sprites[0].X1-winPlayer.Character.Sprites[0].X0)/2+winPlayer.Y, winPlayer.Z-5)

					v.stage.UpdateCamera = room.ZoomCamera(m, 3.5)
				}
			}

			for _, a := range v.audience {
				a.StartCheer(-1)
			}

			v.initDecksForRecording(ctx, &match.Recording{
				Recording: rec,
				Set:       v.Match.Set,
			}, int(v.Match.Perspective-1))

			v.mySetup.Cards = v.mySetup.Cards[:0]
			v.theirSetup.Cards = v.theirSetup.Cards[:0]
			v.myField.Cards = v.myField.Cards[:0]
			v.theirField.Cards = v.theirField.Cards[:0]

			v.game.BetweenMatches = true

			internal.SetActive(false)

			v.State = StateMatchComplete

			goto again
		default:
		}
	case StateMatchComplete:
		if v.updateGameLog() {
			break
		}

		height := 300 / float32(h)

		v.myDeck.MarginTop = 0.5 - height
		v.myDeck.MarginBottom = 0.5
		v.theirDeck.MarginTop = 0.5
		v.theirDeck.MarginBottom = 0.5 - height

		v.myDeck.Update(v.Match.State.Sides[v.Match.Perspective-1].ModTP)
		v.theirDeck.Update(v.Match.State.Sides[2-v.Match.Perspective].ModTP)

		if v.recordingCode == "" {
			select {
			case c := <-v.recordingCh:
				v.recordingCode = c
			default:
			}
		}

		if sprites.TextHitTest(&v.cam, sprites.FontD3Streetism, v.recordingText, 0, 4.5, 0, 1, 1, curX, curY, true) && lastClick && !click && !v.ictx.IsMouseDrag() {
			v.ictx.ConsumeClick()

			var err error

			if v.recordingCode != "" {
				err = internal.OpenURL(ctx, internal.GetConfig(ctx).BaseURL+"game/recording/"+v.recordingCode)
			} else if v.recordingCh == nil {
				// remember for next time
				settings := internal.LoadSettings()
				settings.AutoUploadRecording = true
				internal.SaveSettings(settings)

				v.uploadRecording(ctx)
			}

			if err != nil {
				log.Printf("WARNING: recording upload error: %+v", err)
			}
		}

		if connected, recv, sent := v.game.RematchStatus(); connected && recv && sent {
			if _, ls := v.game.State(); ls == scnet.StateMatchReady {
				v.Match.IndexSet() // reset modified rules
				v.Match.Rematches++

				v.game.ClearRematchStatus()

				v.stage = room.NewStage(ctx, "img/room/stage0001.stage", nil)
				v.reloadDefaultStage(ctx)

				for i, p := range v.player {
					if p != nil {
						p.BecomeAngry(0)
						v.stage.AddPlayer(p, i+1)
					}
				}

				v.stage.SetPlayer(int(v.Match.Perspective))

				v.audience = room.NewAudience(v.game.SCG.Seed(scnet.RNGShared), v.Match.Rematches, v.Match.Init.Mode)
				v.stage.ClearAudience()
				v.stage.AddAudience(v.audience...)

				v.Match.State = match.State{}
				v.Match.Log = match.GameLog{
					Debug: router.FlagDebugGameLog.IsSet(),
				}

				v.recordingCh = nil
				v.recordingCode = ""

				v.initDeckSelect(ctx)

				goto again
			}
		} else if connected && !sent && (router.FlagAutoRematch.IsSet() || v.ictx.Consume(input.BtnSwitch) || (sprites.TextHitTest(&v.cam, sprites.FontBubblegumSans, v.rematchText, 0, -4.75, 0, 1, 1, curX, curY, true) && lastClick && !click && !v.ictx.IsMouseDrag())) {
			v.ictx.ConsumeClick()

			if err := v.game.OfferRematch(); err != nil {
				log.Printf("ERROR: offering rematch: %+v", err)
			}
		}
	case StatePlaybackFetch:
		// another goroutine will update us
	case StatePlaybackInit:
		height := 300 / float32(h)

		if v.recordingAuto != nil && v.ictx.Consume(input.BtnPause) {
			v.replacePage = &router.PageInfo{
				Page: router.PageCardsHome,
				Mode: v.recordingMode,
			}
		}

		round0Ready := v.rec.RoundProcessed(0)
		if round0Ready && !v.recTurn0Processed {
			v.recTurn0Processed = true

			for _, c := range v.rec.Turn0Summon() {
				v.checkExtensionFields(ctx, c)
			}

			if !v.usingCustomMusic {
				v.playDefaultMusic(ctx)
			}

			if !v.usingCustomStage {
				v.reloadDefaultStage(ctx)
			}
		}

		v.myDeck.MarginTop = 0.5 - height
		v.myDeck.MarginBottom = 0.5
		v.theirDeck.MarginTop = 0.5
		v.theirDeck.MarginBottom = 0.5 - height

		v.myDeck.Update(v.Match.State.Sides[v.Match.Perspective-1].ModTP)
		v.theirDeck.Update(v.Match.State.Sides[2-v.Match.Perspective].ModTP)

		if v.recordingAuto != nil || router.FlagAutoPlay.IsSet() {
			v.recordingAutoNext++
		}

		if round0Ready && (v.ictx.Consume(input.BtnSwitch) || (sprites.TextHitTest(&v.cam, sprites.FontBubblegumSans, sprites.Button(input.BtnSwitch)+" Play Recording", 0, -4.75, 0, 1, 1, curX, curY, true) && lastClick && !click && !v.ictx.IsMouseDrag()) || v.recordingAutoNext > 300) {
			v.ictx.ConsumeClick()
			v.State = StatePlaybackIdle
			state, turnData, _ := v.rec.Round(0)
			v.turnData = turnData
			v.Match.IndexSet()
			v.Match.State = *state
			v.recordingAutoNext = 0

			v.resetHandSetup()

			goto again
		}
	case StatePlaybackIdle:
		if v.updateGameLog() {
			break
		}

		if v.recordingAuto != nil && v.ictx.Consume(input.BtnPause) {
			v.replacePage = &router.PageInfo{
				Page: router.PageCardsHome,
				Mode: v.recordingMode,
			}
		}

		if v.ictx.Consume(input.BtnLeft) {
			state, turnData, _ := v.rec.Round(v.Match.State.Round - 2)
			if state != nil {
				v.State = StatePlaybackIdle
				v.turnData = turnData
				v.Match.State = *state

				v.stage.UpdateCamera = room.DefaultUpdateCamera

				v.resetHandSetup()

				goto again
			}
		}

		if v.ictx.Consume(input.BtnRight) {
			state, turnData, _ := v.rec.Round(v.Match.State.Round)
			if state != nil {
				v.State = StatePlaybackIdle
				v.turnData = turnData
				v.Match.State = *state

				v.resetHandSetup()

				goto again
			}
		}

		if v.turnData == nil {
			for _, a := range v.audience {
				a.StartCheer(time.Second)
			}

			if w := v.Match.Winner(); w != 0 && v.player[2-w] != nil {
				v.player[2-w].BecomeAngry(time.Second)
			}
		}

		if v.recordingAuto != nil || (v.turnData != nil && router.FlagAutoPlay.IsSet()) {
			v.recordingAutoNext++

			if v.turnData == nil && v.recordingAutoNext > 300 {
				v.SetRecording(ctx, "auto")
				v.recordingAutoNext = 0

				goto again
			}
		}

		if v.turnData != nil && (v.ictx.Consume(input.BtnSwitch) || (sprites.TextHitTest(&v.cam, sprites.FontBubblegumSans, sprites.Button(input.BtnSwitch)+" Next Round", 0, float32(h)/128-1, 0, 1, 1, curX, curY, true) && lastClick && !click && !v.ictx.IsMouseDrag()) || v.recordingAutoNext > 120) {
			v.ictx.ConsumeClick()

			v.recordingAutoNext = 0

			local, remote := v.rec.Recording.PrivateSeed[0][:], v.rec.Recording.PrivateSeed[1][:]
			if v.Match.Perspective == 2 {
				local, remote = remote, local
			}

			v.game.SCG.InitTurnSeed(v.rec.Recording.Rounds[v.Match.State.Round-1].TurnSeed[:], local, remote)

			if v.rec.Recording.FormatVersion >= 1 {
				v.game.SCG.WhenConfirmedTurn(v.rec.Recording.Rounds[v.Match.State.Round-1].TurnSeed2[:])
			}

			v.animationTimer = 1.5 * 60
			if v.goFast() {
				v.animationTimer = 1
			}

			v.State = StatePlaybackTurnWait
			v.Match.Log.Disabled = false

			goto again
		}

		v.updateStackedTokens()
		v.myField.Update(v.Match.State.Sides[v.Match.Perspective-1].ModTP)
		v.theirField.Update(v.Match.State.Sides[2-v.Match.Perspective].ModTP)
		v.mySetup.Update(v.Match.State.Sides[v.Match.Perspective-1].ModTP)
		v.theirSetup.Update(v.Match.State.Sides[2-v.Match.Perspective].ModTP)
		v.myHand.Update(v.Match.State.Sides[v.Match.Perspective-1].ModTP)
		v.theirHand.Update(v.Match.State.Sides[2-v.Match.Perspective].ModTP)

		if v.turnData != nil {
			for i, c := range v.myHand.Cards {
				if v.turnData.Ready[v.Match.Perspective-1]&(1<<i) != 0 {
					c.hover.Dy = 0.25
				} else {
					c.hover.Dy = 0
				}
			}

			for i, c := range v.theirHand.Cards {
				if v.turnData.Ready[2-v.Match.Perspective]&(1<<i) != 0 {
					c.hover.Dy = 0.25
				} else {
					c.hover.Dy = 0
				}
			}
		}
	case StatePlaybackNext:
		if v.recordingAuto != nil && v.ictx.Consume(input.BtnPause) {
			v.replacePage = &router.PageInfo{
				Page: router.PageCardsHome,
				Mode: v.recordingMode,
			}
		}

		state, turnData, _ := v.rec.Round(v.Match.State.Round)
		if state != nil {
			v.State = StatePlaybackIdle
			v.turnData = turnData
			v.Match.State = *state

			v.resetHandSetup()

			for i := 0; i < 120; i++ {
				// try to get cards to a stable position
				v.updateStackedTokens()
				v.myField.Update(v.Match.State.Sides[v.Match.Perspective-1].ModTP)
				v.theirField.Update(v.Match.State.Sides[2-v.Match.Perspective].ModTP)
			}

			goto again
		}
	case StatePlaybackTurnWait:
		if v.updateGameLog() {
			break
		}

		if v.recordingAuto != nil && v.ictx.Consume(input.BtnPause) {
			v.replacePage = &router.PageInfo{
				Page: router.PageCardsHome,
				Mode: v.recordingMode,
			}
		}

		v.myField.Update(v.Match.State.Sides[v.Match.Perspective-1].ModTP)
		v.theirField.Update(v.Match.State.Sides[2-v.Match.Perspective].ModTP)
		v.myHand.Update(v.Match.State.Sides[v.Match.Perspective-1].ModTP)
		v.theirHand.Update(v.Match.State.Sides[2-v.Match.Perspective].ModTP)

		v.animationTimer--

		if v.animationTimer == 0 {
			v.Match.Log.Disabled = false
			v.beginProcessTurn(ctx)

			goto again
		}
	case StateSelectMode:
		v.selectModeUpdate(ctx)
	case StateDeckEditor:
		if v.deck == nil {
			if err := v.SetDeck(ctx, ""); err != nil {
				return nil, err
			}
		}

		v.deck.Update(ctx)
	}

	if v.updateAgain {
		v.updateAgain = false

		goto again
	}

	return v.replacePage, nil
}

func (v *Visual) Render(ctx context.Context) error {
	touchcontroller.SkipFrame = true

	audio.SetListenerPosition(0, 0, -15, 0, 0, 0)

	w, h := gfx.Size()

	if v.sb == nil {
		v.sb = sprites.NewBatch(&v.cam)
	} else {
		v.sb.Reset(&v.cam)
	}

	if v.tb == nil {
		v.tb = sprites.NewBatch(&v.cam)
	} else {
		v.tb.Reset(&v.cam)
	}

	switch v.State {
	case StateHome:
		v.renderHome()
	case StateConnecting, StateWait:
		v.stage.Render()
	case StateMatchmakingWait:
		v.stage.Render()

		if v.hostCode.Label == "" {
			v.matchCode.Render(v.tb)
		} else {
			v.hostCode.Render(v.tb)
			sprites.DrawTextCentered(v.tb, sprites.FontBubblegumSans, "(click to copy)", 0, -2.5, 0, 0.75, 0.75, sprites.White, true)
		}
	case StateCharacterSelect:
		v.mine.Render()
	case StateDeckSelect:
		v.stage.Render()

		v.deck.Render(ctx)
	case StateReadyToStart:
		v.stage.Render()

		sprites.DrawTextCentered(v.tb, sprites.FontBubblegumSans, "Waiting for opponent", 0, 2.5, 0, 1.5, 1.5, sprites.White, true)
		sprites.DrawTextCentered(v.tb, sprites.FontBubblegumSans, "to select a deck...", 0, 2.5-0.7*1.5, 0, 1.5, 1.5, sprites.White, true)
	case StateNextRoundWait:
		v.stage.Render()

		for pass := 0; pass < 3; pass++ {
			v.theirSetup.Render(ctx, pass)
			v.mySetup.Render(ctx, pass)
		}

		sprites.DrawTextCentered(v.tb, sprites.FontBubblegumSans, "ROUND "+strconv.FormatUint(v.Match.State.Round, 10), 0, -1, 0, 3, 3, sprites.Rainbow, true)

		v.drawMatchOverlays()
	case StateTurnSelect:
		v.stage.Render()

		if v.Match.Timer.MaxTime != 0 {
			v.sb.Append(sprites.Tab, 0, float32(h)/128-0.3, 0, 0.5, -0.5, sprites.Color{R: 63, G: 63, B: 63, A: 255})

			mins := v.timerTurn / 3600
			secs := v.timerTurn / 60 % 60

			timer := strconv.AppendUint(nil, mins, 10)
			timer = append(timer, ':')

			if secs < 10 {
				timer = append(timer, '0')
			}

			timer = strconv.AppendUint(timer, secs, 10)

			r, gb := uint8(255), uint8(255)

			if (mins == 0 && secs <= 10) || secs == 0 || secs == 59 {
				if secs&1 == 0 && v.timerTurn%60 < 30 {
					gb = 127 + uint8((v.timerTurn%30)*128/30)
				} else if secs&1 == 1 && v.timerTurn%60 >= 30 {
					gb = 127 + uint8((29-v.timerTurn%30)*128/30)
				}
			}

			if v.myReady {
				r, gb = 127, 127
			}

			sprites.DrawTextCentered(v.tb, sprites.FontD3Streetism, string(timer), 0, float32(h)/128-0.55, 0, 0.75, 0.75, sprites.Color{R: r, G: gb, B: gb, A: 255}, false)
		}

		v.drawHP(0)
		v.drawHP(1)

		canPlay := !v.myReady

		if v.Match.State.Sides[v.Match.Perspective-1].TP.AmountInf < 0 || v.Match.State.Sides[v.Match.Perspective-1].TP.NaN {
			v.sb.Append(sprites.TPInf, -float32(w)/128*0.75+1.5, float32(h)/128-0.75, 0, 1, 1, sprites.White)

			if v.turnData.Ready[v.Match.Perspective-1] != 0 {
				canPlay = false
			}
		} else {
			v.sb.Append(sprites.TP, -float32(w)/128*0.75+1.5, float32(h)/128-0.75, 0, 1, 1, sprites.White)

			tp := card.PosInf
			tpColor := sprites.White

			if v.Match.State.Sides[v.Match.Perspective-1].TP.AmountInf == 0 {
				remainingTP := v.Match.State.Sides[v.Match.Perspective-1].TP

				for i, c := range v.myHand.Cards {
					if v.turnData.Ready[v.Match.Perspective-1]&(1<<i) != 0 {
						cost := card.Num(c.Def.TP)
						cost.Add(v.Match.State.Sides[v.Match.Perspective-1].ModTP[c.Def.ID], v.Match.SpecialFlags[card.SpecialSubtractInf])
						cost.Negate()

						remainingTP.Add(cost, v.Match.SpecialFlags[card.SpecialSubtractInf])
					}
				}

				tp = remainingTP.String()

				if remainingTP.Amount < 0 {
					tpColor = sprites.Color{R: 255, G: 63, B: 63, A: 255}

					if v.turnData.Ready[v.Match.Perspective-1] != 0 {
						canPlay = false
					}
				}
			}

			sprites.DrawTextBorder(v.tb, sprites.FontD3Streetism, tp, -float32(w)/128*0.75+1.5, float32(h)/128-1.03125, 0, 1, 1.25, tpColor, sprites.Black, true)
		}

		for pass := 0; pass < 3; pass++ {
			v.theirField.Render(ctx, pass)
			v.myField.Render(ctx, pass)
			v.theirHand.Render(ctx, pass)
			v.myHand.Render(ctx, pass)
		}

		endTurnText := " End Turn"
		endColor := sprites.White

		if *v.myHand.InputFocus == &v.endFocus {
			endColor = sprites.Rainbow
		}

		switch {
		case v.myReady && (v.wantUndoReady || (v.Match.SpecialFlags[card.SpecialAutoReady] && !card.Num(-1).Less(v.Match.State.Sides[v.Match.Perspective-1].TP))):
			endColor = sprites.Gray
			endTurnText = " Undo End Turn"
		case v.myReady:
			endTurnText = " Undo End Turn"
		case !canPlay:
			endColor = sprites.Gray
		}

		if v.myReady {
			tint := sprites.Color{R: 0, G: 191, B: 0, A: 255}
			if v.wantUndoReady {
				tint = sprites.Gray
			}

			sprites.DrawTextCentered(v.tb, sprites.FontBubblegumSans, "Ready", -float32(w)/128*0.75, float32(h)/128-2, 0, 0.75, 0.75, tint, true)
		}

		if v.theirReady {
			sprites.DrawTextCentered(v.tb, sprites.FontBubblegumSans, "Ready", float32(w)/128*0.75, float32(h)/128-2, 0, 0.75, 0.75, sprites.Color{R: 0, G: 191, B: 0, A: 255}, true)
		}

		endY := float32(h)/128 - 1
		if v.Match.Timer.MaxTime != 0 {
			endY -= 0.5
		}

		sprites.DrawTextCentered(v.tb, sprites.FontBubblegumSans, sprites.Button(input.BtnSwitch)+endTurnText, 0, endY, 0, 1, 1, endColor, true)

		v.drawMatchOverlays()
	case StateTurnProcess:
		v.turnLock.Lock()
		defer v.turnLock.Unlock()

		v.stage.Render()

		v.drawHP(0)
		v.drawHP(1)

		for pass := 0; pass < 3; pass++ {
			v.myField.Render(ctx, pass)
			v.theirField.Render(ctx, pass)
		}

		if len(v.cardAnimation) != 0 && v.cardAnimation[0].Card.Mode == match.ModeInHand {
			ac := v.activeCard[v.cardAnimation[0].Card]
			if ac != nil && ac.hover != nil {
				ac.hover.SetFunHouse(v.sb)
				ac.Render(ctx, v.sb, v.cardAnimation[0].Effect)
				v.sb.SetFunHouse(nil)
			}
		}

		if v.Match.State.MaxPriority >= 64 {
			v.drawStats()
		}

		mx, my, _ := v.ictx.Mouse()
		textColor := sprites.White

		if sprites.TextHitTest(&v.cam, sprites.FontBubblegumSans, sprites.Button(input.BtnToggle)+" Speed Up", 0, float32(h)/128-1, 0, 1, 1, mx, my, true) {
			textColor = sprites.Rainbow
		}

		sprites.DrawTextCentered(v.tb, sprites.FontBubblegumSans, sprites.Button(input.BtnToggle)+" Speed Up", 0, float32(h)/128-1, 0, 1, 1, textColor, true)

		v.drawMatchOverlays()
	case StateTurnEnd:
		v.stage.Render()

		animProgress := (1 - (float32(v.animationTimer)-2*60)/60/1)
		if animProgress > 1 {
			animProgress = 1
		}

		v.drawHP(0)
		v.drawHP(1)

		w, h := gfx.Size()

		perspective := v.Match.Perspective
		if v.Match.SpecialFlags[card.SpecialBothPlayersAreP1] {
			perspective = 1
		}

		myATK := v.Match.State.Sides[perspective-1].FinalATK
		theirATK := v.Match.State.Sides[2-perspective].FinalATK
		myDEF := v.Match.State.Sides[perspective-1].FinalDEF
		theirDEF := v.Match.State.Sides[2-perspective].FinalDEF

		for _, stat := range []*card.Number{&myATK, &theirATK, &myDEF, &theirDEF} {
			if stat.Less(card.Num(0)) {
				*stat = card.Num(0)
			}
		}

		if animProgress > 0.5 {
			negTheirDEF := theirDEF
			negTheirDEF.Negate()
			myATK.Add(negTheirDEF, v.Match.SpecialFlags[card.SpecialSubtractInf])

			negMyDEF := myDEF
			negMyDEF.Negate()
			theirATK.Add(negMyDEF, v.Match.SpecialFlags[card.SpecialSubtractInf])

			v.drawStat(sprites.ATK, -float32(w)/256, float32(h)/256*(1-(animProgress-0.5)/0.5), myATK)
			v.drawStat(sprites.ATK, float32(w)/256, float32(h)/256*(1-(animProgress-0.5)/0.5), theirATK)

			v.drawStat(sprites.DEF, -float32(w)/256*(3-animProgress*8), -float32(h)/256*(3-animProgress*8), myDEF)
			v.drawStat(sprites.DEF, float32(w)/256*(3-animProgress*8), -float32(h)/256*(3-animProgress*8), theirDEF)
		} else {
			v.drawStat(sprites.ATK, -float32(w)/256, float32(h)/256, myATK)
			v.drawStat(sprites.ATK, float32(w)/256, float32(h)/256, theirATK)

			v.drawStat(sprites.DEF, -float32(w)/256*(1-animProgress*4), -float32(h)/256*(1-animProgress*4), myDEF)
			v.drawStat(sprites.DEF, float32(w)/256*(1-animProgress*4), -float32(h)/256*(1-animProgress*4), theirDEF)
		}

		for pass := 0; pass < 3; pass++ {
			v.mySetup.Render(ctx, pass)
			v.theirSetup.Render(ctx, pass)
			v.myField.Render(ctx, pass)
			v.theirField.Render(ctx, pass)
		}

		v.drawMatchOverlays()
	case StateMatchCompleteWait:
		v.stage.Render()

		sprites.DrawTextCentered(v.tb, sprites.FontBubblegumSans, "Verifying Match...", 0, 0, 0, 3, 3, sprites.White, true)
	case StateMatchComplete:
		v.stage.Render()

		flip := v.Match.Perspective == 2

		leftPlayer := 0
		if flip {
			leftPlayer = 1
		}

		v.sb.MatchOverview(0, 0, 0, 4.75, 4, flip)

		if v.player[leftPlayer] != nil && v.player[leftPlayer].Character != nil {
			v.sb.Append(v.player[leftPlayer].Character.Portrait(), -4, 0, 0, -1.5, 1.5, sprites.White)
		}

		if v.player[1-leftPlayer] != nil && v.player[1-leftPlayer].Character != nil {
			v.sb.Append(v.player[1-leftPlayer].Character.Portrait(), 4, 0, 0, 1.5, 1.5, sprites.White)
		}

		winnerName := "Player " + string(v.Match.Winner()+'0')
		if winner := v.player[v.Match.Winner()-1]; winner != nil {
			winnerName = winner.Character.DisplayName
		}

		if v.Match.SpecialFlags[card.SpecialBothPlayersAreP1] {
			winnerName = "Nobody"

			if v.Match.Winner() == 1 {
				winnerName = "Players 1 and 2"

				if v.player[0] != nil && v.player[1] != nil {
					winnerName = v.player[0].Character.DisplayName + " and " + v.player[1].Character.DisplayName
				}
			}
		}

		sprites.DrawTextBorder(v.tb, sprites.FontBubblegumSans, winnerName+" Wins!", 0, -0.5, 0, 1.5, 1.5, sprites.White, sprites.Black, true)

		opponentConnected, receivedOffer, offeredRematch := v.game.RematchStatus()

		switch {
		case !opponentConnected:
			v.rematchText = "Opponent Left"
		case offeredRematch && receivedOffer:
			v.rematchText = "Setting Up Rematch..."
		case offeredRematch:
			v.rematchText = "Waiting for Opponent to Accept"
		case receivedOffer:
			v.rematchText = sprites.Button(input.BtnSwitch) + " Accept Rematch"
		default:
			v.rematchText = sprites.Button(input.BtnSwitch) + " Offer Rematch"
		}

		borderColor := sprites.Black

		mx, my, _ := v.ictx.Mouse()
		if opponentConnected && !offeredRematch && sprites.TextHitTest(&v.cam, sprites.FontBubblegumSans, v.rematchText, 0, -4.75, 0, 1, 1, mx, my, true) {
			borderColor = sprites.Rainbow
		}

		textColor := sprites.White
		if !opponentConnected || offeredRematch {
			textColor = sprites.Gray
		}

		sprites.DrawTextBorder(v.tb, sprites.FontBubblegumSans, v.rematchText, 0, -4.75, 0, 1, 1, textColor, borderColor, true)

		v.recordingText = "Saving Recording..."
		textColor = sprites.Gray

		if v.recordingCode != "" {
			v.recordingText = "Recording: " + v.recordingCode
			textColor = effectHighlightColor
		} else if v.recordingCh == nil {
			v.recordingText = "Save Recording"
			textColor = sprites.White
		}

		borderColor = sprites.Black

		if (v.recordingCode != "" || v.recordingCh == nil) && sprites.TextHitTest(&v.cam, sprites.FontD3Streetism, v.recordingText, 0, 4.5, 0, 1, 1, mx, my, true) {
			borderColor = sprites.Rainbow
		}

		sprites.DrawTextBorder(v.tb, sprites.FontD3Streetism, v.recordingText, 0, 4.5, 0, 1, 1, textColor, borderColor, true)

		v.drawMatchOverlays()
	case StatePlaybackFetch:
		v.stage.Render()

		sprites.DrawTextCentered(v.tb, sprites.FontBubblegumSans, "Loading...", 0, 0, 0, 1.5, 1.5, sprites.White, true)
		sprites.DrawTextCentered(v.tb, sprites.FontBubblegumSans, v.nextRecordingError, 0, -1, 0, 0.75, 0.75, sprites.White, true)
	case StatePlaybackInit:
		v.stage.Render()

		flip := v.rec.Recording.Perspective == 2

		leftPlayer := 0
		if flip {
			leftPlayer = 1
		}

		v.Match.Perspective = uint8(leftPlayer) + 1

		v.sb.MatchOverview(0, 0, 0, 4.75, 4, flip)

		if v.player[leftPlayer] != nil && v.player[leftPlayer].Character != nil {
			v.sb.Append(v.player[leftPlayer].Character.Portrait(), -4, 0, 0, -1.5, 1.5, sprites.White)
		}

		if v.player[1-leftPlayer] != nil && v.player[1-leftPlayer].Character != nil {
			v.sb.Append(v.player[1-leftPlayer].Character.Portrait(), 4, 0, 0, 1.5, 1.5, sprites.White)
		}

		sprites.DrawTextBorder(v.tb, sprites.FontBubblegumSans, "VS", 0, -0.5, 0, 1.5, 1.5, sprites.White, sprites.Black, true)

		var width float32

		for _, l := range v.recVersionText {
			width += sprites.TextAdvance(sprites.FontBubblegumSans, l, 0.45)
		}

		sprites.DrawTextBorder(v.tb, sprites.FontBubblegumSans, v.recTimeText, -4.75, 4.25, 0, 0.45, 0.45, sprites.White, sprites.Black, false)

		sprites.DrawTextBorder(v.tb, sprites.FontBubblegumSans, v.recVersionText, 4.75-width, 4.25, 0, 0.45, 0.45, sprites.White, sprites.Black, false)

		borderColor := sprites.Black

		mx, my, _ := v.ictx.Mouse()
		if v.rec.RoundProcessed(0) && sprites.TextHitTest(&v.cam, sprites.FontBubblegumSans, sprites.Button(input.BtnSwitch)+" Play Recording", 0, -4.75, 0, 1, 1, mx, my, true) {
			borderColor = sprites.Rainbow
		}

		textColor := sprites.White
		if !v.rec.RoundProcessed(0) {
			textColor = sprites.Gray
		}

		sprites.DrawTextBorder(v.tb, sprites.FontBubblegumSans, sprites.Button(input.BtnSwitch)+" Play Recording", 0, -4.75, 0, 1, 1, textColor, borderColor, true)
	case StatePlaybackIdle:
		v.stage.Render()

		v.drawHP(0)
		v.drawHP(1)

		if w := v.Match.Winner(); w == 0 || v.turnData != nil {
			for pass := 0; pass < 3; pass++ {
				v.theirField.Render(ctx, pass)
				v.myField.Render(ctx, pass)
				v.theirHand.Render(ctx, pass)
				v.myHand.Render(ctx, pass)
			}
		}

		textColor := sprites.White

		mx, my, _ := v.ictx.Mouse()
		if v.turnData != nil && sprites.TextHitTest(&v.cam, sprites.FontBubblegumSans, sprites.Button(input.BtnSwitch)+" Next Round", 0, float32(h)/128-1, 0, 1, 1, mx, my, true) {
			textColor = sprites.Rainbow
		}

		if v.turnData == nil {
			textColor = sprites.Gray
		}

		sprites.DrawTextCentered(v.tb, sprites.FontBubblegumSans, sprites.Button(input.BtnSwitch)+" Next Round", 0, float32(h)/128-1, 0, 1, 1, textColor, true)

		if w := v.Match.Winner(); w != 0 && v.player[w-1] != nil && v.turnData == nil && !v.Match.SpecialFlags[card.SpecialBothPlayersAreP1] {
			winPlayer := v.player[w-1]

			sprites.DrawTextCentered(v.tb, sprites.FontBubblegumSans, winPlayer.Character.DisplayName+" Wins!", 0, -float32(h)/256, 0, 1.5, 1.5, sprites.Rainbow, true)

			if !internal.PrefersReducedMotion() {
				var m gfx.Matrix

				m.Translation(winPlayer.X, winPlayer.Character.Sprites[0].Y1-(winPlayer.Character.Sprites[0].X1-winPlayer.Character.Sprites[0].X0)/2+winPlayer.Y, winPlayer.Z-5)

				v.stage.UpdateCamera = room.ZoomCamera(m, 3.5)
			}
		}

		v.drawMatchOverlays()
	case StatePlaybackNext:
		v.stage.Render()

		sprites.DrawTextCentered(v.tb, sprites.FontBubblegumSans, "Processing...", 0, 0, 0, 3, 3, sprites.White, true)
	case StatePlaybackTurnWait:
		v.stage.Render()

		v.drawHP(0)
		v.drawHP(1)

		for pass := 0; pass < 3; pass++ {
			v.theirField.Render(ctx, pass)
			v.myField.Render(ctx, pass)
			v.theirHand.Render(ctx, pass)
			v.myHand.Render(ctx, pass)
		}

		sprites.DrawTextCentered(v.tb, sprites.FontBubblegumSans, "ROUND "+strconv.FormatUint(v.Match.State.Round, 10), 0, -1, 0, 3, 3, sprites.Rainbow, true)

		v.drawMatchOverlays()
	case StateSelectMode:
		if err := v.selectModeRender(ctx); err != nil {
			return err
		}
	case StateDeckEditor:
		v.stage.Render()
		v.deck.Render(ctx)
	}

	lines := strings.Split(v.message, "\n")
	for i, line := range lines {
		scale := float32(1.5)
		width := float32(0)

		for _, ch := range line {
			width += sprites.TextAdvance(sprites.FontBubblegumSans, ch, scale)
		}

		if maxWidth := float32(w)/64 - 1; width > maxWidth {
			scale *= maxWidth / width
		}

		sprites.DrawTextCentered(v.tb, sprites.FontBubblegumSans, line, 0, 0.7*1.5*float32(len(lines)-1-i), 0, scale, 1.5, sprites.White, true)
	}

	scale := float32(0.75)
	width := float32(0)

	for _, ch := range v.smallMessage {
		width += sprites.TextAdvance(sprites.FontBubblegumSans, ch, scale)
	}

	if maxWidth := float32(w)/64 - 1; width > maxWidth {
		scale *= maxWidth / width
	}

	sprites.DrawTextCentered(v.tb, sprites.FontBubblegumSans, v.smallMessage, 0, -1, 0, scale, 0.75, sprites.White, true)

	v.updateConnectionState()

	v.sb.Render()
	v.tb.Render()

	if v.State == StateMatchComplete || v.State == StatePlaybackInit {
		for pass := 0; pass < 3; pass++ {
			v.myDeck.Render(ctx, pass)
			v.theirDeck.Render(ctx, pass)
		}
	}

	if v.viewingGameLog && v.gameLogBatch != nil {
		v.tb.Reset(&v.cam)
		v.tb.Append(sprites.Blank, 0, 0, 0, float32(w)/64-0.125, float32(h)/64-0.125, sprites.Color{R: 15, G: 31, B: 95, A: 191})
		v.tb.Render()

		v.gameLogBatch.Render()

		x := 0.25 - float32(w)/128
		y := float32(h)/128 - 0.55

		mx, my, _ := v.ictx.Mouse()

		textColor := sprites.White

		if sprites.TextHitTest(&v.cam, sprites.FontBubblegumSans, "Close", x, y, 0, 0.65, 0.65, mx, my, false) {
			textColor = sprites.Yellow
		}

		v.tb.Reset(&v.cam)
		sprites.DrawTextBorder(v.tb, sprites.FontBubblegumSans, "Close", x, y, 0, 0.65, 0.65, textColor, sprites.Black, false)
		v.tb.Render()
	}

	return nil
}

func (v *Visual) playDefaultMusic(ctx context.Context) {
	for _, f := range v.Match.Set.Mode.GetAll(card.FieldSummonCard) {
		sc := f.(*card.SummonCard)
		c := v.Match.Set.Card(sc.ID)

		for _, e := range c.Extensions {
			if e.Type == card.ExtensionSetMusic {
				v.playCustomMusic(ctx, e.CID, e.LoopStart, e.LoopEnd)

				return
			}
		}
	}

	v.usingCustomMusic = false

	if v.Match.Init.Mode == match.ModeCustom {
		audio.Bounty.PlayMusic(0, false)
	} else {
		audio.Miniboss.PlayMusic(0, false)
	}
}

var musicCache = internal.Cache{MaxEntries: 32}

func getCustomMusic(ctx context.Context, cid card.ContentIdentifier, start, end float32) (*audio.Sound, error) {
	path := internal.GetConfig(ctx).IPFSBaseURL + cid.String()

	music, err := musicCache.Do(path, func() (interface{}, error) {
		s := &audio.Sound{
			Name:  path,
			Loop:  true,
			Start: float64(start),
			End:   float64(end),
		}

		s.StartPreload()

		return s, nil
	})
	if err != nil {
		return nil, fmt.Errorf("match: failed to load custom music: %w", err)
	}

	return music.(*audio.Sound), nil
}

func getCustomSound(ctx context.Context, cid card.ContentIdentifier) (*audio.Sound, error) {
	path := internal.GetConfig(ctx).IPFSBaseURL + cid.String()

	sound, err := musicCache.Do(path, func() (interface{}, error) {
		s := &audio.Sound{
			Name: path,
		}

		s.StartPreload()

		return s, nil
	})
	if err != nil {
		return nil, fmt.Errorf("match: failed to load custom music: %w", err)
	}

	return sound.(*audio.Sound), nil
}

func (v *Visual) playCustomMusic(ctx context.Context, cid card.ContentIdentifier, start, end float32) {
	v.usingCustomMusic = true

	music, err := getCustomMusic(ctx, cid, start, end)
	if err != nil {
		log.Println("ERROR: can't play music:", err)

		return
	}

	music.PlayMusic(0, false)
}

func (v *Visual) abortMatch(reason string) {
	audio.StopMusic()

	v.State = StateWait
	v.message = "Desync Detected"
	v.smallMessage = reason
}

func (v *Visual) reloadDefaultStage(ctx context.Context) {
	var (
		cid  card.ContentIdentifier
		name string
	)

	if variant := v.Match.Init.CachedVariant; variant != nil {
		for _, f := range variant.Rules {
			if sc, ok := f.(*card.SummonCard); ok {
				c := v.Match.Set.Card(sc.ID)

				for _, e := range c.Extensions {
					if e.Type == card.ExtensionSetStageIPFS {
						cid = e.CID

						break
					}

					if e.Type == card.ExtensionSetStage {
						name = e.Name

						break
					}
				}
			}

			if cid != nil || name != "" {
				break
			}
		}
	}

	if cid == nil && name == "" {
		for _, f := range v.Match.Set.Mode.GetAll(card.FieldSummonCard) {
			sc := f.(*card.SummonCard)
			c := v.Match.Set.Card(sc.ID)

			for _, e := range c.Extensions {
				if e.Type == card.ExtensionSetStageIPFS {
					cid = e.CID

					break
				}

				if e.Type == card.ExtensionSetStage {
					name = e.Name

					break
				}
			}

			if cid != nil || name != "" {
				break
			}
		}
	}

	v.usingCustomStage = cid != nil || name != ""

	stageMonth := time.Now().Month()
	if v.rec != nil {
		stageMonth = v.rec.Recording.Start.Month()
	}

	stageIndex := v.game.SCG.Seed(scnet.RNGShared)[0] >> 4

	defaultStage := "img/room/stage0001.stage"

	for _, ver := range room.DefaultStages {
		if v.Match.Init.Version[0] < ver.Version[0] {
			break
		}

		if v.Match.Init.Version[0] == ver.Version[0] {
			if v.Match.Init.Version[1] < ver.Version[1] {
				break
			}

			if v.Match.Init.Version[1] == ver.Version[1] {
				if v.Match.Init.Version[2] < ver.Version[2] {
					break
				}
			}
		}

		defaultStage = ver.ByMonth[stageMonth-1][stageIndex]
	}

	if name != "" {
		defaultStage = "img/room/" + url.PathEscape(name) + ".stage"
	}

	v.stage.SetStage(ctx, defaultStage, cid)
}

func (v *Visual) drawHP(player uint8) {
	w, h := gfx.Size()

	side := float32(1)
	smooth := &v.theirHPAnim

	perspective := v.Match.Perspective
	if v.Match.SpecialFlags[card.SpecialBothPlayersAreP1] {
		perspective = 1
	}

	if player == perspective-1 {
		side = -1
		smooth = &v.myHPAnim
	}

	hp := v.Match.State.Sides[player].HP

	if v.State == StateTurnProcess && 2-v.Match.State.RoundWinner == player && !v.Match.SpecialFlags[card.SpecialNoDamageOnWin] {
		// hide lost HP until actual turn end
		hp.Amount++
	}

	if v.State != StateTurnEnd || v.animationTimer < 65 {
		*smooth = *smooth*0.9 + float32(hp.Amount)*0.1
	}

	hp.Amount = int64(math.Round(float64(*smooth)))

	var tilt float32

	switch {
	case len(v.cardAnimation) != 0 && v.cardAnimation[0].Effect.Type == card.EffectHeal:
		healPlayer := v.cardAnimation[0].Player
		if v.cardAnimation[0].Effect.Flags&card.FlagHealOpponent != 0 {
			healPlayer = 1 - healPlayer
		}

		if player == healPlayer {
			v.healAmount[player] *= 0.8

			tilt = v.healAmount[player]
		}
	case v.State == StateTurnEnd && v.animationTimer == 60 && v.Match.State.RoundWinner != 0:
		v.healAmount[2-v.Match.State.RoundWinner] = -1
	case v.State == StateTurnEnd && v.animationTimer > 30 && v.animationTimer < 60 && 2-v.Match.State.RoundWinner == player:
		v.healAmount[player] *= 0.8

		tilt = v.healAmount[player]
	default:
		v.healAmount[player] = 0
	}

	shade := sprites.Color{R: 255, G: 255, B: 255, A: 255}

	animTimer := uint8((1 - float32(v.animationTimer)/60/0.5) * 255)

	if tilt > 0 {
		shade.R = animTimer
		shade.B = animTimer
	} else if tilt < 0 {
		shade.G = animTimer
		shade.B = animTimer
	}

	scale := float32(math.Abs(float64(tilt))) / 10

	if tilt >= 0 {
		tilt = 0
	} else {
		tilt /= -5
	}

	v.sb.AppendEx(sprites.HP, side*float32(w)/128*0.75, float32(h)/128-0.75, 0, 0.75+scale, 0.75+scale, sprites.White, 0, 0, 0, side*tilt)

	sprites.DrawTextBorder(v.tb, sprites.FontD3Streetism, hp.String(), side*float32(w)/128*0.75, float32(h)/128-1.03125, 0, 1, 1.25, shade, sprites.Black, true)
}

func (v *Visual) drawStats() {
	w, h := gfx.Size()

	perspective := v.Match.Perspective
	if v.Match.SpecialFlags[card.SpecialBothPlayersAreP1] {
		perspective = 1
	}

	v.drawStat(sprites.ATK, -float32(w)/256, float32(h)/256, v.Match.State.Sides[perspective-1].ATK)
	v.drawStat(sprites.ATK, float32(w)/256, float32(h)/256, v.Match.State.Sides[2-perspective].ATK)
	v.drawStat(sprites.DEF, -float32(w)/256, -float32(h)/256, v.Match.State.Sides[perspective-1].DEF)
	v.drawStat(sprites.DEF, float32(w)/256, -float32(h)/256, v.Match.State.Sides[2-perspective].DEF)
}

func (v *Visual) drawStat(sprite *sprites.Sprite, x, y float32, num card.Number) {
	if v.Match.NoStats {
		return
	}

	v.sb.Append(sprite, x, y, 0, 1, 1, sprites.White)

	if num.Less(card.Num(0)) {
		num = card.Num(0)
	}

	str := num.String()

	sprites.DrawTextBorder(v.tb, sprites.FontD3Streetism, str, x, y-0.5, 0, 2, 2, sprites.White, sprites.Black, true)
}

func (v *Visual) checkExtensionFields(ctx context.Context, c *match.ActiveCard) {
	var song, stage *card.Extension

	for _, e := range c.Card.Def.Extensions {
		override := e.Flags&card.ExtensionOverrideCustom != 0

		switch e.Type {
		case card.ExtensionSetStageIPFS:
			if override || !v.usingCustomStage {
				stage = e
			}
		case card.ExtensionSetMusic:
			if override || !v.usingCustomMusic {
				song = e
			}
		case card.ExtensionSetStage:
			if override || !v.usingCustomStage {
				stage = e
			}
		case card.ExtensionPlaySound:
			s, err := getCustomSound(ctx, e.CID)
			if err != nil {
				log.Println("WARNING: failed to load custom sound for", c.Card.Def.DisplayName())
			} else {
				v.queuedSound[c] = s
			}
		}
	}

	if song != nil {
		v.playCustomMusic(ctx, song.CID, song.LoopStart, song.LoopEnd)
	}

	if stage != nil {
		v.usingCustomStage = true

		if stage.Type == card.ExtensionSetStageIPFS {
			v.stage.SetStage(ctx, "img/room/stage0001.stage", stage.CID)
		} else {
			v.stage.SetStage(ctx, "img/room/"+url.PathEscape(stage.Name)+".stage", nil)
		}
	}
}

func (v *Visual) updateConnectionState() {
	w, h := gfx.Size()

	var ping time.Duration

	if v.game != nil {
		ping = v.game.LastPing()
	}

	if ping != 0 {
		text := "Ping: " + ping.String()

		if ping >= 5*time.Second {
			text = "No response for " + ping.String()
		}

		x := float32(w)/128 - 0.175
		y := float32(h)/128 - 0.35

		for _, letter := range text {
			x -= sprites.TextAdvance(sprites.FontD3Streetism, letter, 0.35)
		}

		sprites.DrawTextBorder(v.sb, sprites.FontD3Streetism, text, x, y, 0, 0.35, 0.35, sprites.White, sprites.Black, false)
	}

	if v.State != StateConnecting && v.game != nil {
		var message string

		switch cs, _ := v.game.State(); cs {
		case scnet.StateConnecting:
			message = "Checking connection to opponent..."
		case scnet.StateConnectionLost:
			message = "Lost connection to opponent. Attempting to reestablish..."
		}

		if message != "" {
			if v.networkErrorTimer >= 255-20 {
				v.networkErrorTimer = 255
			} else {
				v.networkErrorTimer += 20
			}

			v.sb.Append(sprites.Description, float32(w)/128-2, -float32(h)/128+2.6*float32(v.networkErrorTimer)/255-1.3, 0, 1.5, 1.5, sprites.Color{R: 255, G: 95, B: 63, A: 255})

			(&card.RichDescription{
				Text:  message,
				Color: sprites.White,
			}).Typeset(sprites.FontBubblegumSans, float32(w)/128-3.7, -float32(h)/128+2.6*float32(v.networkErrorTimer)/255-0.7, 0, 0.65, 0.65, 3.5, 2, nil, false, func(x, y, z, sx, sy float32, text string, tint sprites.Color, _ bool) {
				sprites.DrawText(v.tb, sprites.FontBubblegumSans, text, x, y, z, sx, sy, tint)
			})
		} else {
			if v.networkErrorTimer <= 30 {
				v.networkErrorTimer = 0
			} else {
				v.networkErrorTimer -= 30
			}

			v.sb.Append(sprites.Description, float32(w)/128-2, -float32(h)/128+2.6*float32(v.networkErrorTimer)/255-1.3, 0, 1.5, 1.5, sprites.LGray)
		}
	}
}

func (v *Visual) resetHandSetup() {
	v.myHand.Cards = make([]*Card, len(v.Match.State.Sides[v.Match.Perspective-1].Hand))
	v.theirHand.Cards = make([]*Card, len(v.Match.State.Sides[2-v.Match.Perspective].Hand))
	v.myField.Cards = make([]*Card, 0, len(v.Match.State.Sides[v.Match.Perspective-1].Setup))
	v.theirField.Cards = make([]*Card, 0, len(v.Match.State.Sides[2-v.Match.Perspective].Setup))
	v.mySetup.Cards = v.mySetup.Cards[:0]
	v.theirSetup.Cards = v.theirSetup.Cards[:0]

	for i, c := range v.Match.State.Sides[v.Match.Perspective-1].Hand {
		v.myHand.Cards[i] = &Card{Card: c.Card}
	}

	for i, c := range v.Match.State.Sides[2-v.Match.Perspective].Hand {
		v.theirHand.Cards[i] = &Card{Card: c.Card}

		if v.Match.UnknownDeck != 0 && c.Card.Def != nil {
			visible := false

			for _, e := range c.Card.Def.Effects {
				if e.Type == card.CondInHand && e.Flags&card.FlagCondInHandHide == 0 && e.Amount < c.InHandTurns {
					visible = true

					break
				}
			}

			if !visible {
				v.theirHand.Cards[i].flip = 255
				v.theirHand.Cards[i].flipDir = 0
			} else if c.InHandTurns == 1 {
				v.theirHand.Cards[i].flip = 255
				v.theirHand.Cards[i].SetFlip(false)
			}
		}
	}

	for _, c := range v.Match.State.Sides[v.Match.Perspective-1].Setup {
		if c.Mode.Hide() {
			continue
		}

		if c.Mode == match.ModeSetup && c.Effects[0].Effect.Type != card.CondApply {
			c.Desc = &card.RichDescription{
				Effect: c.Effects[0].Effect,

				Content: c.Effects[0].Effect.Description(c.Card.Def, v.Match.Set),
				Color:   sprites.Black,
			}

			c.StayOnField = false
		}

		vc := &Card{Card: c.Card, Active: c}
		v.myField.Cards = append(v.myField.Cards, vc)
		v.activeCard[c] = vc
	}

	for _, c := range v.Match.State.Sides[2-v.Match.Perspective].Setup {
		if c.Mode.Hide() {
			continue
		}

		if c.Mode == match.ModeSetup && c.Effects[0].Effect.Type != card.CondApply {
			c.Desc = &card.RichDescription{
				Effect: c.Effects[0].Effect,

				Content: c.Effects[0].Effect.Description(c.Card.Def, v.Match.Set),
				Color:   sprites.Black,
			}

			c.StayOnField = false
		}

		vc := &Card{Card: c.Card, Active: c}
		v.theirField.Cards = append(v.theirField.Cards, vc)
		v.activeCard[c] = vc
	}
}

func (v *Visual) processTurn() {
	defer close(v.nextEffect)

	v.turnLock.Lock()

	for {
		rng := v.game.SCG.Rand(scnet.RNGShared)

		ce := v.Match.ProcessQueuedEffect(rng, &networkWrapper{v.game, &v.turnLock})
		if len(ce) == 0 {
			break
		}

		v.turnLock.Unlock()
		v.nextEffect <- ce
		v.turnLock.Lock()
	}

	v.turnLock.Unlock()
}

type networkWrapper struct {
	network match.Network
	lock    *sync.Mutex
}

func (n *networkWrapper) SendModifiedCards(mc []card.ModifiedCardPosition, reveal bool) error {
	n.lock.Unlock()

	err := n.network.SendModifiedCards(mc, reveal)

	n.lock.Lock()

	return err //nolint:wrapcheck
}

func (n *networkWrapper) RecvModifiedCards(reveal bool) []card.ModifiedCardPosition {
	n.lock.Unlock()

	mc := n.network.RecvModifiedCards(reveal)

	n.lock.Lock()

	return mc
}

func (v *Visual) updateTurn(ctx context.Context) bool {
	v.turnLock.Lock()
	defer v.turnLock.Unlock()

	perspective := v.Match.Perspective
	if v.Match.SpecialFlags[card.SpecialBothPlayersAreP1] {
		perspective = 1
	}

	_, _, click := v.ictx.Mouse()

	if v.goFast() {
		v.animationTimer = 0
	}

	if v.animationTimer != 0 {
		if !click {
			v.animationTimer--
		}

		v.updateStackedTokens()
		v.myField.Update(v.Match.State.Sides[v.Match.Perspective-1].ModTP)
		v.theirField.Update(v.Match.State.Sides[2-v.Match.Perspective].ModTP)

		return false
	}

	if v.cardAnimation != nil {
		v.cardAnimation[0].Card.ActiveEffect = nil
	}

	var ce []match.CardEffect

	select {
	case ce = <-v.nextEffect:
	default:
		v.updateStackedTokens()
		v.myField.Update(v.Match.State.Sides[v.Match.Perspective-1].ModTP)
		v.theirField.Update(v.Match.State.Sides[2-v.Match.Perspective].ModTP)

		return false
	}

	if len(ce) != 0 && ((ce[0].Effect.Type.IsCondition() && ce[0].Effect.Type != card.CondCoin && ce[0].Effect.Type != card.CondApply) || (ce[0].Effect.Priority == 0 && ce[0].Effect.Type != card.EffectSummon) || (len(ce) == 1 && v.activeCard[ce[0].Card] == nil && v.activeCard[v.stackedCard[ce[0].Card]] == nil)) {
		if ce[0].Effect.IsPassive(ce[0].Card.Card.Def) {
			ce[0].Card.StayOnField = true

			if s := v.stackedCard[ce[0].Card]; s != nil {
				s.StayOnField = true
			}
		}

		runtime.Gosched()

		return true
	}

	v.cardAnimation = ce

	if len(ce) == 0 {
		v.animationTimer = 60 * 3
		v.State = StateTurnEnd

		sfxDelay := 1.5

		if v.Match.NoStats {
			v.animationTimer = 60 * 2.5
			sfxDelay = 1.25
		}

		if v.goFast() {
			v.animationTimer = 1
			sfxDelay = 0.5
		}

		audio.Toss11.PlaySoundGlobal(0, 0, 0)

		switch {
		case v.Match.State.RoundWinner == perspective:
			audio.CrowdCheer2.PlaySoundGlobal(sfxDelay, 0, 0)
			audio.Damage0.PlaySound(sfxDelay+0.5, 0, 0, -5, 0, 0)
		case v.Match.State.RoundWinner == 0:
			audio.Fail.PlaySound(sfxDelay, 0, 0, 0, 0, 0)
		default:
			audio.CrowdGasp.PlaySoundGlobal(sfxDelay+0.7, 0, 0)
			audio.Death3.PlaySound(sfxDelay+0.5, 0, 2, 5, 0, 0)
		}

		v.myField.Slide = false
		v.theirField.Slide = false

		if v.Match.State.RoundWinner == perspective || v.Match.State.Sides[0].HP.Less(card.Num(int64(v.Match.Rules[0].MaxHP)/2)) || v.Match.State.Sides[1].HP.Less(card.Num(int64(v.Match.Rules[1].MaxHP)/2)) {
			for _, a := range v.audience {
				a.StartCheer(5 * time.Second)
			}
		}

		return true
	}

	v.animationTimer = 0.5 * 60

	ce[0].Card.ActiveEffect = ce[0].Effect

	visual := v.activeCard[ce[0].Card]
	if visual == nil {
		visual = v.activeCard[v.stackedCard[ce[0].Card]]
	}

	var x, y float32

	if visual != nil && visual.hover != nil {
		x, y = -visual.hover.X, visual.hover.Y
	}

	if s, ok := v.queuedSound[ce[0].Card]; ok {
		delete(v.queuedSound, ce[0].Card)

		s.PlaySound(0, 0, 0, x, y, 0)
	}

	switch ce[0].Effect.Type {
	case card.EffectStat, card.EffectEmpower, card.EffectRawStat, card.EffectMultiplyHealing:
		audio.CardSound2.PlaySound(0, 0, 0, x, y, 0)

		v.animationTimer = 0.25 * 60
	case card.EffectSummon, card.CondApply:
		if ce[0].Effect.Type == card.EffectSummon {
			for i := 1; i < len(ce); i++ {
				v.checkExtensionFields(ctx, ce[i].Card)
			}
		}

		if ce[0].Effect.Type == card.CondApply && ce[0].Effect.Flags&(card.FlagCondApplyNextRound|card.FlagCondApplyOpponent|card.FlagCondApplyShowMask) == card.FlagCondApplyNextRound|card.FlagCondApplyShowOriginalText && ce[0].Effect.Result != nil && ((ce[0].Effect.Result.Type == card.CondApply && ce[0].Effect.Result.Flags == ce[0].Effect.Flags) || (ce[0].Effect.Result.Type == card.EffectSummon && ce[0].Effect.Result.Priority == 0 && ce[0].Effect.Result.Flags&(card.FlagSummonInvisible|card.FlagSummonReplace|card.FlagSummonOpponent) == card.FlagSummonReplace && ce[0].Effect.Result.Amount == 1 && ce[0].Effect.Result.Filter.IsSingleCard())) {
			v.animationTimer = 0
			ce[0].Card.StayOnField = true

			if s, ok := v.stackedCard[ce[0].Card]; ok {
				s.StayOnField = true
			}

			break
		}

		if ce[0].Effect.Type == card.CondApply && ce[0].Effect.Flags&card.FlagCondApplyShowMask == card.FlagCondApplyShowNothing {
			v.animationTimer = 0

			break
		}

		if ce[0].Effect.Type == card.EffectSummon && ce[0].Effect.Flags&(card.FlagSummonInvisible|card.FlagSummonReplace) == card.FlagSummonInvisible {
			v.animationTimer = 0

			break
		}

		isBoringSetup := ce[0].Effect.Type == card.CondApply && (ce[0].Card.Mode == match.ModeSetup || ce[0].Card.Mode == match.ModeSetupOriginal) && (len(v.Match.State.Queue) != 0 && v.Match.State.Queue[0].Effect.Type == card.CondApply && (v.Match.State.Queue[0].Card.Mode == match.ModeSetup || v.Match.State.Queue[0].Card.Mode == match.ModeSetupOriginal))

		if (ce[0].Effect.Type == card.EffectSummon && ce[0].Effect.Flags&card.FlagSummonReplace != 0) || (ce[0].Effect.Type == card.CondApply && ce[0].Card.Mode == match.ModeSetup) {
			if ce[0].Effect.Priority != 0 && !isBoringSetup {
				audio.PageFlip.PlaySound(0, 0, 0, x, y, 0)
			}

			field := v.theirField
			if ce[0].Player == perspective-1 {
				field = v.myField
			}

			for i := 0; i < len(field.Cards); i++ {
				if field.Cards[i].Active == ce[0].Card {
					if ((ce[0].Effect.Type == card.EffectSummon && ce[0].Effect.Flags&(card.FlagSummonInvisible|card.FlagSummonOpponent) == 0) || (ce[0].Effect.Type == card.CondApply && ce[0].Effect.Flags&card.FlagCondApplyOpponent == 0)) && len(ce) > 1 {
						var h *gui.HoverTilt

						if field.Cards[i].hover != nil {
							h = new(gui.HoverTilt)
							*h = *field.Cards[i].hover
						}

						delete(v.activeCard, ce[0].Card)

						vc := &Card{
							Card:   ce[1].Card.Card,
							Active: ce[1].Card,
							hover:  h,
						}

						v.activeCard[ce[1].Card] = vc
						field.Cards[i] = vc

						field.Cards[i].flip = 255
						field.Cards[i].SetFlip(false)
					} else {
						delete(v.activeCard, ce[0].Card)
						field.Cards = append(field.Cards[:i], field.Cards[i+1:]...)
					}

					break
				}
			}
		}

		var hover *gui.HoverTilt

		if vc, ok := v.activeCard[ce[0].Card]; ok {
			if vc.hover != nil {
				hover = vc.hover
			}
		} else if vc, ok := v.activeCard[v.stackedCard[ce[0].Card]]; ok {
			if vc.hover != nil {
				hover = vc.hover
			}
		}

		for i := 1; i < len(ce); i++ {
			if ce[0].Effect.Priority != 0 && !isBoringSetup {
				audio.CardSound2.PlaySound((float64(i)-1)*0.05, 1+(float64(i)-1)*0.1, 0, x, y, 0)
			}

			if ce[i].Card.Mode.Hide() {
				continue
			}

			if i == 1 && ce[0].Effect.Type == card.EffectSummon && ce[0].Effect.Flags&(card.FlagSummonReplace|card.FlagSummonInvisible|card.FlagSummonOpponent) == card.FlagSummonReplace {
				continue
			}

			_, found := v.activeCard[ce[i].Card]
			if found {
				continue
			}

			_, found = v.activeCard[v.stackedCard[ce[i].Card]]
			if found {
				continue
			}

			field := v.theirField
			if ce[i].Player == perspective-1 {
				field = v.myField
			}

			var h *gui.HoverTilt

			if hover != nil {
				h = new(gui.HoverTilt)
				*h = *hover
			}

			vc := &Card{
				Card:   ce[i].Card.Card,
				Active: ce[i].Card,
				hover:  h,
			}

			v.activeCard[ce[i].Card] = vc
			field.Cards = append(field.Cards, vc)
		}

		if ce[0].Effect.Priority == 0 || isBoringSetup {
			v.animationTimer = 0
		} else {
			v.animationTimer = 0.75 * 60
		}
	case card.EffectDelaySetup:
		for i := 1; i < len(ce); i++ {
			a := v.activeCard[ce[i].Card]
			if a != nil {
				a.cachedDef = nil
				a.cachedDesc = nil
			}
		}

		if ce[0].Effect.Priority == 0 || len(ce) == 1 {
			v.animationTimer = 0
		} else {
			v.animationTimer = 0.75 * 60

			if ce[0].Effect.Amount > 0 {
				audio.StatDown.PlaySound(0, 0.75, 0, x, y, 0)
			} else if ce[0].Effect.Amount < 0 {
				audio.StatUp.PlaySound(0, 0.75, 0, x, y, 0)
			}
		}
	case card.EffectModifyAvailableCards:
		for i := 1; i < len(ce); i++ {
			if ce[0].Effect.Priority != 0 {
				audio.CardSound2.PlaySound((float64(i)-1)*0.05, 1+(float64(i)-1)*0.1, 0, x, y, 0)
			}

			_, found := v.activeCard[ce[i].Card]
			if found {
				continue
			}

			field := v.theirField
			if ce[i].Player == perspective-1 {
				field = v.myField
			}

			v.checkExtensionFields(ctx, ce[i].Card)

			vc := &Card{
				Card:   ce[i].Card.Card,
				Active: ce[i].Card,
			}

			v.activeCard[ce[i].Card] = vc
			field.Cards = append(field.Cards, vc)
		}

		if ce[0].Effect.Priority == 0 {
			v.animationTimer = 0
		} else {
			v.animationTimer = 0.75 * 60
		}
	case card.EffectHeal:
		player := ce[0].Player
		if ce[0].Effect.Flags&card.FlagHealOpponent != 0 {
			player = 1 - player
		}

		v.healAmount[player] = float32(ce[0].Effect.Amount)

		if ce[0].Effect.Amount < 0 {
			v.healAmount[player] *= float32(v.Match.State.Sides[player].HealMulN)
		} else {
			v.healAmount[player] *= float32(v.Match.State.Sides[player].HealMulP)
		}

		if v.healAmount[player] > 0 {
			audio.Heal.PlaySound(0, 0, 0, x, y, 0)
		} else {
			audio.Damage0.PlaySound(0, 0, 0, x, y, 0)
		}

		if ce[0].Effect.Flags&card.FlagHealInfinity != 0 {
			v.healAmount[player] *= 5
		}
	case card.EffectNumb:
		count := len(ce)
		if ce[0].Effect.Flags&card.FlagNumbSummon != 0 {
			count /= 2

			for i := 2; i < len(ce); i += 2 {
				var hover *gui.HoverTilt

				if vc, ok := v.activeCard[ce[i-1].Card]; ok {
					if vc.hover != nil {
						hover = vc.hover
					}
				}

				_, found := v.activeCard[ce[i].Card]
				if found {
					continue
				}

				field := v.theirField
				if ce[i].Player == perspective-1 {
					field = v.myField
				}

				var h *gui.HoverTilt

				if hover != nil {
					h = new(gui.HoverTilt)
					*h = *hover
				}

				v.checkExtensionFields(ctx, ce[i].Card)

				vc := &Card{
					Card:   ce[i].Card.Card,
					Active: ce[i].Card,
					hover:  h,
				}

				v.activeCard[ce[i].Card] = vc
				field.Cards = append(field.Cards, vc)
			}
		}

		for i := 1; i < count; i++ {
			var cx, cy float32

			if a := v.activeCard[ce[i].Card]; a != nil && a.hover != nil {
				cx, cy = -a.hover.X, a.hover.Y
			} else if a := v.activeCard[v.stackedCard[ce[i].Card]]; a != nil && a.hover != nil {
				cx, cy = -a.hover.X, a.hover.Y
			} else {
				continue
			}

			audio.Lazer.PlaySound(0.1*float64(i-1), 1+0.05*float64(i-1), 0, cx, cy, 0)
		}

		if count == 1 {
			v.animationTimer = 0

			return true
		}
	case card.EffectDrawCard:
		audio.PageFlip.PlaySound(0, 0, 0, x, y, 0)

		v.animationTimer = 0.5 * 60
	case card.EffectPreventNumb:
		audio.CardSound2.PlaySound(0, 0, 0, x, y, 0)

		v.animationTimer = 0.5 * 60
	case card.CondCoin:
		stat := ce[0].Effect.Flags&card.FlagCondCoinStat != 0

		for i := 1; i < len(ce); i++ {
			heads := ce[i].Effect == ce[0].Effect.Result2
			if ce[0].Effect.Flags&card.FlagCondCoinNegative != 0 {
				heads = !heads
			}

			if v.goFast() {
				audio.Coin.PlaySound(float64(i-1)/60, 1+0.05*float64(i-1), 0, x, y, 0)

				if !stat {
					if heads {
						audio.AtkFail.PlaySound(float64(i)/60, 0, 0, x, y, 0)
					} else {
						audio.AtkSuccess.PlaySound(float64(i)/60, 0, 0, x, y, 0)
					}
				}
			} else {
				audio.Coin.PlaySound(0.1*float64(i-1), 1+0.05*float64(i-1), 0, x, y, 0)

				if !stat {
					if heads {
						audio.AtkFail.PlaySound(0.5+float64(i)*0.25, 0, 0, x, y, 0)
					} else {
						audio.AtkSuccess.PlaySound(0.5+float64(i)*0.25, 0, 0, x, y, 0)
					}
				}
			}

			if visual != nil {
				heads := ce[i].Effect != ce[0].Effect.Result2
				if ce[0].Effect.Flags&card.FlagCondCoinNegative != 0 {
					heads = !heads
				}

				if v.goFast() {
					visual.coins = append(visual.coins, &coin{
						invisibleTimer: uint64(i - 1),
						revealTimer:    uint64(i),
						stat:           ce[0].Effect.Flags&card.FlagCondCoinStat != 0,
						heads:          heads,
					})
				} else {
					visual.coins = append(visual.coins, &coin{
						invisibleTimer: 60 * 0.1 * uint64(i-1),
						revealTimer:    60*0.5 + 60*0.25*uint64(i),
						stat:           ce[0].Effect.Flags&card.FlagCondCoinStat != 0,
						heads:          heads,
					})
				}
			}
		}

		v.animationTimer = 0.25*60 + 0.25*60*uint64(len(ce))
	default:
		log.Printf("TODO: animate effect: %+v %+v", ce[1:], *ce[0].Effect)
	}

	anyActive := false

	for _, c := range ce {
		if c.Card != nil && (v.activeCard[c.Card] != nil || v.activeCard[v.stackedCard[c.Card]] != nil) {
			anyActive = true

			break
		}
	}

	if !anyActive {
		v.animationTimer = 0
	}

	v.updateStackedTokens()
	v.myField.Update(v.Match.State.Sides[v.Match.Perspective-1].ModTP)
	v.theirField.Update(v.Match.State.Sides[2-v.Match.Perspective].ModTP)

	return false
}

func (v *Visual) beginProcessTurn(ctx context.Context) {
	v.State = StateTurnProcess
	v.animationTimer = 0.5 * 60

	*v.myHand.InputFocus = nil

	if v.goFast() {
		v.animationTimer = 1
	}

	handBefore := [2][]match.HandCard{
		append([]match.HandCard(nil), v.Match.State.Sides[0].Hand...),
		append([]match.HandCard(nil), v.Match.State.Sides[1].Hand...),
	}

	if err := v.Match.BeginTurn(v.turnData); err != nil {
		v.abortMatch(err.Error())
	}

	for c := range v.queuedSound {
		delete(v.queuedSound, c)
	}

	for player := range v.Match.State.Sides {
		for _, c := range v.Match.State.Sides[player].Field {
			v.checkExtensionFields(ctx, c)
		}
	}

	for k := range v.activeCard {
		delete(v.activeCard, k)
	}

	for k := range v.stackedCard {
		delete(v.stackedCard, k)
	}

	perspective := v.Match.Perspective
	if v.Match.SpecialFlags[card.SpecialBothPlayersAreP1] {
		perspective = 1
	}

	v.myField.Cards = make([]*Card, 0, len(v.Match.State.Sides[perspective-1].Field))
	v.theirField.Cards = make([]*Card, 0, len(v.Match.State.Sides[2-perspective].Field))

	for player := range v.Match.State.Sides {
		for _, c := range v.Match.State.Sides[player].Field {
			if c.Mode.Hide() {
				continue
			}

			vc := &Card{
				Card:   c.Card,
				Active: c,
			}

			v.activeCard[c] = vc

			if c.Mode == match.ModeDefault && v.Match.UnknownDeck == uint8(player)+1 {
				vc.flip = 255
				vc.SetFlip(false)
			}

			if perspective == uint8(player)+1 {
				v.myField.Cards = append(v.myField.Cards, vc)
			} else {
				v.theirField.Cards = append(v.theirField.Cards, vc)
			}

			if hc, ok := c.CreatedBy.(match.HandCard); ok {
				for i := range handBefore {
					for j, hc2 := range handBefore[i] {
						if hc == hc2 {
							handLayout := v.theirHand
							if v.Match.Perspective == uint8(i)+1 {
								handLayout = v.myHand
							}

							if len(handLayout.Cards) > j && handLayout.Cards[j] != nil && handLayout.Cards[j].hover != nil {
								hover := &gui.HoverTilt{}
								*hover = *handLayout.Cards[j].hover

								hover.Y += hover.Dy
								hover.Dy = 0

								vc.hover = hover
							}

							break
						}
					}

					if vc.hover != nil {
						break
					}
				}
			}
		}
	}

	v.nextEffect = make(chan []match.CardEffect)
	go v.processTurn()
}

func (v *Visual) initDeckSelect(ctx context.Context) {
	internal.SetActive(true)

	if v.npc != nil {
		d := v.npc.CreateDeck(v.Match.Set)
		v.game.SendDeck(d, v.Match.SpecialFlags[card.SpecialEffectBack])

		deck := make([]*match.Card, len(d))

		for i, id := range d {
			c := v.Match.Set.Card(id)

			deck[i] = &match.Card{
				Set:  v.Match.Set,
				Def:  c,
				Back: c.Rank.Back(v.Match.SpecialFlags[card.SpecialEffectBack]),
			}
		}

		v.Match.State.Sides[v.Match.Perspective-1].Deck = deck
		v.message = ""
		v.State = StateReadyToStart

		return
	}

	v.State = StateDeckSelect

	v.deck = NewDeckPicker(ctx, v.Match.Set)
	v.deck.AllowCreate = true
	v.deck.Populate()
}

func (v *Visual) SetDeck(ctx context.Context, code string) error {
	if v.Match.Set == nil {
		var err error

		v.Match.Set, err = v.Match.Init.Cards(ctx)
		if err != nil {
			return fmt.Errorf("parsing card set: %w", err)
		}

		v.reloadDefaultStage(ctx)
	}

	if v.deck == nil {
		preloadAssets(ctx, v.Match.Set, v.Match.Init.Mode)

		v.deck = NewDeckPicker(ctx, v.Match.Set)
	}

	v.deck.AllowCreate = true
	v.deck.IsEditor = v

	if code == "" {
		v.deck.builder = nil

		v.deck.Populate()

		return nil
	}

	v.deck.builder = NewDeckBuilder(ctx, v.Match.Set)

	var d card.Deck

	if err := d.UnmarshalText([]byte(code)); err != nil {
		return fmt.Errorf("decoding deck: %w", err)
	}

	v.deck.builder.deck.Cards = make([]*Card, len(d))

	for i, id := range d {
		c := v.Match.Set.Card(id)

		v.deck.builder.deck.Cards[i] = &Card{
			Card: &match.Card{
				Set:  v.Match.Set,
				Def:  c,
				Back: c.Rank.Back(false),
			},
		}
	}

	v.deck.builder.resetPicker()

	return nil
}

func (v *Visual) uploadRecording(ctx context.Context) {
	ch := make(chan string, 1)
	v.recordingCh = ch

	go func(ch chan<- string, recordingData []byte) {
		b, err := internal.DoPutRequest(ctx, internal.GetConfig(ctx).MatchRecordingBaseURL+"upload", "application/vnd.spycards.matchdata", recordingData)
		if err != nil {
			log.Printf("ERROR: uploading match recording: %+v", err)

			return
		}

		ch <- string(b)
	}(ch, v.recordingData)
}

func (v *Visual) goFast() bool {
	_, h := gfx.Size()

	curX, curY, click := v.ictx.Mouse()
	if click {
		return sprites.TextHitTest(&v.cam, sprites.FontBubblegumSans, sprites.Button(input.BtnToggle)+" Speed Up", 0, float32(h)/128-1, 0, 1, 1, curX, curY, true)
	}

	if v.Match.Set.FastAnimation {
		return true
	}

	if v.ictx.Held(input.BtnToggle) {
		return true
	}

	return false
}

func (v *Visual) updateStackedTokens() {
	for k := range v.stackedCard {
		delete(v.stackedCard, k)
	}

	tokens := make(map[card.ID][]*match.ActiveCard)

	perspective := v.Match.Perspective
	if v.Match.SpecialFlags[card.SpecialBothPlayersAreP1] {
		perspective = 1
	}

	for player := range v.Match.State.Sides {
		for id := range tokens {
			delete(tokens, id)
		}

		field := v.myField
		if player == int(2-perspective) {
			field = v.theirField
		}

		if v.tokenStacks[player] == nil {
			v.tokenStacks[player] = make(map[card.ID]float32)
		} else {
			for id := range v.tokenStacks[player] {
				tokens[id] = nil
			}
		}

		for _, c := range v.Match.State.Sides[player].Field {
			if c.Mode != match.ModeStackedToken {
				continue
			}

			tokens[c.Card.Def.ID] = append(tokens[c.Card.Def.ID], c)
		}

		for id, cards := range tokens {
			x := v.tokenStacks[player][id]
			x = x*0.8 + float32(len(cards))*0.2
			v.tokenStacks[player][id] = x

			rn := card.Num(int64(math.Round(float64(x))))
			if rn.Amount == 0 && len(cards) == 0 {
				delete(v.tokenStacks[player], id)

				for i, c := range field.Cards {
					if c.Active != nil && c.Active.Mode == match.ModeStackedToken && c.Def.ID == id {
						field.Cards = append(field.Cards[:i], field.Cards[i+1:]...)

						delete(v.activeCard, c.Active)

						break
					}
				}

				continue
			}

			found := false

			for _, c := range field.Cards {
				if c.Active != nil && c.Active.Mode == match.ModeStackedToken && c.Def.ID == id {
					if c.ModTP != rn {
						c.ModTP = rn
						c.cachedDef = nil
					}

					for _, t := range cards {
						v.stackedCard[t] = c.Active
					}

					found = true

					break
				}
			}

			if !found {
				def := v.Match.Set.Card(id)

				ac := &match.ActiveCard{
					Card: &match.Card{
						Set:  v.Match.Set,
						Def:  def,
						Back: card.Token,
					},
					Desc: def.Description(v.Match.Set),
					Mode: match.ModeStackedToken,
				}

				c := &Card{
					Card:   ac.Card,
					Active: ac,
					ModTP:  rn,
				}

				v.activeCard[ac] = c

				for _, t := range cards {
					v.stackedCard[t] = ac

					if created, ok := t.CreatedBy.(*match.ActiveCard); ok {
						if summoner := v.activeCard[created]; summoner != nil && summoner.hover != nil && c.hover == nil {
							c.hover = &gui.HoverTilt{}
							*c.hover = *summoner.hover
						}
					}
				}

				field.Cards = append(field.Cards, c)
			}
		}
	}
}

func (v *Visual) drawMatchOverlays() {
	w, h := gfx.Size()

	if audio.IsCurrentMusicLoading() {
		sprites.DrawTextBorder(v.tb, sprites.FontBubblegumSans, "Loading Song...", 0, 1-float32(h)/128, 0, 1, 1, sprites.MKBlue, sprites.Black, true)
	}

	x := 0.25 - float32(w)/128
	y := float32(h)/128 - 0.55

	mx, my, _ := v.ictx.Mouse()

	if !v.Match.SpecialFlags[card.SpecialHideGameLog] || router.FlagDebugGameLog.IsSet() {
		if !v.viewingGameLog {
			if sprites.TextHitTest(&v.cam, sprites.FontBubblegumSans, "Game Log", x, y, 0, 0.65, 0.65, mx, my, false) {
				textColor := sprites.Yellow

				if v.Match.Log.Root == nil {
					textColor = sprites.Gray
				}

				sprites.DrawTextBorder(v.tb, sprites.FontBubblegumSans, "Game Log", x, y, 0, 0.65, 0.65, textColor, sprites.Black, false)
			}
		} else {
			v.cam.PushTranslation(0, v.gameLogSH.y, 0)
			v.gameLogBatch.SetCamera(&v.cam)
			v.cam.PopTransform()
		}
	}
}

func (v *Visual) updateGameLog() bool {
	w, h := gfx.Size()
	x := 0.25 - float32(w)/128
	y := float32(h)/128 - 0.55
	mx, my, click := v.ictx.Mouse()
	_, _, lastClick := v.ictx.LastMouse()

	if !v.viewingGameLog {
		if (!v.Match.SpecialFlags[card.SpecialHideGameLog] || router.FlagDebugGameLog.IsSet()) && v.Match.Log.Root != nil && sprites.TextHitTest(&v.cam, sprites.FontBubblegumSans, "Game Log", x, y, 0, 0.65, 0.65, mx, my, false) && lastClick && !click && !v.ictx.IsMouseDrag() {
			v.viewingGameLog = true

			v.gameLogBatch = sprites.NewBatch(&v.cam)
			v.gameLogSH.width = float32(w)/64 - 0.5
			v.gameLogSH.height = -v.logGroupToBatch(v.gameLogBatch, v.Match.Log.Root, 0, -0.5, v.gameLogSH.width)

			v.gameLogScroll = &gui.Scroll{
				Control:    &v.gameLogSH,
				StealFocus: true,
				Screen:     128,
				Size:       1,
				Margin:     0.5,
				Scroll:     v.gameLogSH.height,
			}

			return true
		}

		return false
	}

	audio.MusicVolumeTemp(0.5)

	if v.ictx.Consume(input.BtnPause) || (sprites.TextHitTest(&v.cam, sprites.FontBubblegumSans, "Close", x, y, 0, 0.65, 0.65, mx, my, false) && lastClick && !click && !v.ictx.IsMouseDrag()) {
		v.viewingGameLog = false
		v.gameLogScroll = nil
		v.gameLogBatch = nil

		audio.MusicVolumeTemp(1)

		return false
	}

	v.gameLogScroll.Update(&v.cam, v.ictx)

	return true
}

func (v *Visual) appendLogBatch(b *sprites.Batch, y *float32, padding, width, size float32, font sprites.FontID, text string, tint sprites.Color) {
	*y -= 0.2 * size
	(&card.RichDescription{Text: text}).Typeset(font, padding-width/2, *y, 0, size, size, width-padding, 1e10, nil, false, func(x, _, z, sx, sy float32, text string, _ sprites.Color, _ bool) {
		*y -= 0.7 * size
		sprites.DrawTextBorder(b, font, text, x, *y, z, sx, sy, tint, sprites.Black, false)
	})
}

func (v *Visual) logGroupToBatch(b *sprites.Batch, group *match.LogGroup, y, padding, width float32) float32 {
	if group == nil {
		v.appendLogBatch(b, &y, padding, width, 0.65, sprites.FontD3Streetism, "(error)", sprites.Red)

		return y
	}

	v.appendLogBatch(b, &y, padding, width, 0.65, sprites.FontD3Streetism, group.Title, sprites.White)

	padding += 0.25

	for i, j := 0, 0; i < len(group.Messages); i++ {
		if j < len(group.SubGroups) && group.SubGroups[j].Index == i {
			y = v.logGroupToBatch(b, group.SubGroups[j], y, padding+0.25, width)

			j++
		} else {
			v.appendLogBatch(b, &y, padding, width, 0.65, sprites.FontBubblegumSans, group.Messages[i], sprites.White)
		}
	}

	return y
}
