//go:build (!js || !wasm) && !headless
// +build !js !wasm
// +build !headless

package visual

func (v *Visual) onModeChange() {
	// do nothing
}

func redirectToWebCardEditor(cardCodes string) bool {
	return false
}

func checkForUpdates() {
	// do nothing
}
