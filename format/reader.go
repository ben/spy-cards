package format

import (
	"bytes"
	"encoding/binary"
	"errors"
	"fmt"
	"math/bits"
	"unicode/utf8"
)

// Reader decodes data from a binary format.
//
// There should be a deferred call to Catch when Reader is in use.
type Reader struct {
	buf []byte
	pos int
}

// Init sets the Reader's buffer to b.
func (r *Reader) Init(b []byte) {
	r.buf = b
	r.pos = 0
}

// Len returns the remaining number of bytes in the buffer.
func (r *Reader) Len() int {
	return len(r.buf) - r.pos
}

// Peek returns the next n bytes in the buffer without consuming them.
//
// Peek panics if n is greater than Len.
func (r *Reader) Peek(n int) []byte {
	return r.buf[:len(r.buf):len(r.buf)][r.pos:][:n]
}

var errDataTooBig = errors.New("format: data exceeds maximum size")

// SVarInt decodes a signed varint from the Reader.
func (r *Reader) SVarInt() int64 {
	n, count := binary.Varint(r.buf[r.pos:])
	if count == 0 {
		Throw(fmt.Errorf("format: reading signed varint: %w", ErrReachedEnd))
	}

	if count < 0 {
		Throw(fmt.Errorf("format: reading signed varint: %w", errDataTooBig))
	}

	r.pos += count

	return n
}

// UVarInt decodes an unsigned varint from the Reader.
func (r *Reader) UVarInt() uint64 {
	n, count := binary.Uvarint(r.buf[r.pos:])
	if count == 0 {
		Throw(fmt.Errorf("format: reading unsigned varint: %w", ErrReachedEnd))
	}

	if count < 0 {
		Throw(fmt.Errorf("format: reading unsigned varint: %w", errDataTooBig))
	}

	r.pos += count

	return n
}

// Length calls UVarInt and verifies that at least length*minSize
// bytes remain in the stream, and that the length fits within an int.
func (r *Reader) Length(minSize, divisor uint64) int {
	length := r.UVarInt()

	if int(length) < 0 || uint64(int(length)) != length {
		Throw(fmt.Errorf("format: length %d: %w", length, errDataTooBig))
	}

	hi, lo := bits.Mul64(length/divisor, minSize)
	if hi != 0 {
		Throw(fmt.Errorf("format: length %d exceeds size of remaining data: %w", length, errDataTooBig))
	}

	if uint64(r.Len()) < lo {
		Throw(fmt.Errorf("format: length %d exceeds size of remaining data: %w", length, errDataTooBig))
	}

	return int(length)
}

// Byte consumes the first byte remaining in the buffer.
func (r *Reader) Byte() byte {
	if r.pos == len(r.buf) {
		Throw(fmt.Errorf("format: reading byte: %w", ErrReachedEnd))
	}

	r.pos++

	return r.buf[r.pos-1]
}

// SubReader returns a Reader for a length-prefixed section of r.
func (r *Reader) SubReader() *Reader {
	length := r.Length(1, 1)

	return &Reader{
		buf: r.Bytes(length),
	}
}

var errNegativeCount = errors.New("format: count is negative")

// Bytes returns the next count bytes from the Reader.
func (r *Reader) Bytes(count int) []byte {
	if count < 0 {
		Throw(fmt.Errorf("format: cannot read %d bytes: %w", count, errNegativeCount))
	}

	if r.pos+count > len(r.buf) {
		Throw(fmt.Errorf("format: cannot read %d bytes with %d available: %w", count, len(r.buf)-r.pos, ErrReachedEnd))
	}

	start := r.pos
	r.pos += count

	return r.buf[start:][:count:count]
}

// String decodes a length-prefixed string from the Reader.
func (r *Reader) String() string {
	length := r.Length(1, 1)
	b := r.Bytes(length)

	if !utf8.Valid(b) {
		Throw(fmt.Errorf("format: invalid UTF-8 string: %q", b))
	}

	return string(b)
}

// String1 decodes a length-prefixed string from the Reader.
//
// The maximum length of a string in this legacy format is 255 characters.
func (r *Reader) String1() string {
	var length uint8

	r.Read(&length)
	b := r.Bytes(int(length))

	if !utf8.Valid(b) {
		Throw(fmt.Errorf("format: invalid UTF-8 string: %q", b))
	}

	return string(b)
}

// Read decodes a fixed-size data structure from the Reader.
func (r *Reader) Read(v interface{}) {
	size := binary.Size(v)
	if size == -1 {
		panic(fmt.Errorf("format: cannot read type %T: %w", v, errNonConstantSize))
	}

	b := r.Bytes(size)
	if err := binary.Read(bytes.NewReader(b), binary.LittleEndian, v); err != nil {
		Throw(fmt.Errorf("format: failed to read type %T: %w", v, err))
	}
}
