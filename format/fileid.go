package format

import (
	"bytes"
	"crypto/sha256"
	"encoding/binary"
	"errors"
	"fmt"
)

// FileID is the decoded form of a file identifier for Spy Cards Online.FileID
//
// It contains the first 8 bytes of the SHA-256 hash of the file, along with
// an opaque integer identifier.
type FileID struct {
	Hash [8]byte
	ID   uint64
	Type uint64
}

// String implements fmt.Stringer.
func (f FileID) String() string {
	b, err := f.MarshalText()
	if err != nil {
		panic(err) // should never happen
	}

	return string(b)
}

// IsMatch verifies that the file ID matches the hash of the data.
func (f FileID) IsMatch(data []byte) bool {
	hash := sha256.Sum256(data)

	// we don't need to use subtle here because the data is not secret
	return bytes.Equal(f.Hash[:], hash[:8])
}

// MarshalText implements encoding.TextMarshaler.
func (f FileID) MarshalText() ([]byte, error) {
	b, err := f.MarshalBinary()
	if err != nil {
		return nil, err
	}

	return []byte(Encode32(b)), nil
}

// UnmarshalText implements encoding.TextUnmarshaler.
func (f *FileID) UnmarshalText(b []byte) error {
	p, err := Decode32(string(b))
	if err != nil {
		return fmt.Errorf("format: parsing file ID: %w", err)
	}

	return f.UnmarshalBinary(p)
}

// MarshalBinary implements encoding.BinaryMarshaler.
func (f FileID) MarshalBinary() ([]byte, error) {
	var buf [8 + binary.MaxVarintLen64*2]byte

	copy(buf[:], f.Hash[:])
	n := binary.PutUvarint(buf[8:], f.ID)

	n2 := 0
	if f.Type != 0 {
		n2 = binary.PutUvarint(buf[8+n:], f.Type)
	}

	return buf[:8+n+n2], nil
}

// Errors that can be returned by FileID unmarshaling.
var (
	ErrShortFileID = errors.New("format: too short to be a file ID")
	ErrFileID      = errors.New("format: invalid file ID")
)

// UnmarshalBinary implements encoding.BinaryUnmarshaler.
func (f *FileID) UnmarshalBinary(b []byte) error {
	if len(b) <= 8 {
		return ErrShortFileID
	}

	id, n := binary.Uvarint(b[8:])
	if 8+n == len(b) {
		copy(f.Hash[:], b)
		f.ID = id
		f.Type = 0

		return nil
	}

	typ, n2 := binary.Uvarint(b[8+n:])
	if typ == 0 {
		return ErrFileID
	}
	if 8+n+n2 == len(b) {
		copy(f.Hash[:], b)
		f.ID = id
		f.Type = typ

		return nil
	}

	return ErrFileID
}
