package format

import (
	"errors"
	"strconv"
)

type uncaught struct{ err error }

func (u uncaught) String() string {
	return "uncaught error: " + u.err.Error()
}

// Catch is a deferred function that recovers an error passed to Throw in *err.
func Catch(err *error) {
	if r := recover(); r != nil {
		if ex, ok := r.(uncaught); ok {
			*err = ex.err
		} else {
			panic(r)
		}
	}
}

// Throw panics with the given error, which can then be recovered using Catch.
func Throw(err error) {
	panic(uncaught{err})
}

// Errors that can be returned by format.Reader and format.Writer.
var (
	ErrReachedEnd  = errors.New("format: reached end of buffer while reading")
	ErrExpectedEOF = errors.New("format: expected end of buffer, but additional data is present")
)

type ErrVersion uint64

func (err ErrVersion) Error() string {
	return "format: unexpected version " + strconv.FormatUint(uint64(err), 10)
}
