//go:build !headless
// +build !headless

package main

import (
	"context"
	"encoding/base64"
	"errors"
	"flag"
	"net/url"
	"os"
	"runtime/debug"
	"strconv"

	"git.lubar.me/ben/spy-cards/arcade"
	"git.lubar.me/ben/spy-cards/arcade/flowerjourney"
	"git.lubar.me/ben/spy-cards/arcade/game"
	"git.lubar.me/ben/spy-cards/arcade/highscores"
	"git.lubar.me/ben/spy-cards/arcade/mainmenu"
	"git.lubar.me/ben/spy-cards/arcade/miteknight"
	"git.lubar.me/ben/spy-cards/audio"
	"git.lubar.me/ben/spy-cards/editor"
	"git.lubar.me/ben/spy-cards/festival"
	"git.lubar.me/ben/spy-cards/fighters"
	"git.lubar.me/ben/spy-cards/format"
	"git.lubar.me/ben/spy-cards/gfx"
	"git.lubar.me/ben/spy-cards/gfx/sprites"
	"git.lubar.me/ben/spy-cards/input"
	"git.lubar.me/ben/spy-cards/internal"
	"git.lubar.me/ben/spy-cards/internal/router"
	"git.lubar.me/ben/spy-cards/match"
	"git.lubar.me/ben/spy-cards/rng"
	"git.lubar.me/ben/spy-cards/visual"
)

var flagPath = flag.String("path", startPath, "domain-relative URL of the page to run on")

func main() {
	internal.PerformanceMark("goInitFinish")
	internal.PerformanceMeasure("goInit", "goInitStart", "goInitFinish")

	flag.Parse()

	debug.SetTraceback("all")

	u, err := url.Parse(*flagPath)
	if err != nil {
		panic(err)
	}

	router.SetFlagsFromQuery(u.Query())

	ctx, ictx := input.NewContext(context.Background(), getInputs)

	go run(ctx)

	appMain(ictx)
}

func run(ctx context.Context) {
	info := &router.PageInfo{}

	err := info.UnmarshalText([]byte(*flagPath))
	if err != nil {
		panic(err)
	}

	internal.PerformanceMark("preloadStart")

	// start loading assets without waiting for graphics context
	var toPreload preloadSet

	switch info.Page {
	case router.PageAphidFestival:
		toPreload = preloadFestival()
	case router.PageELF:
		toPreload = preloadELF()
	case router.PageBugTables:
		toPreload = preloadTables()
	case router.PageArcadeMain, router.PageArcadeGame, router.PageArcadeHighScores, router.PageArcadeRecording:
		toPreload = preloadTermacade()
	case router.PageCardsHome, router.PageCardsJoin, router.PageCardsModes, router.PageCardsDeck, router.PageCardsDeckEditor, router.PageCardsRecording:
		toPreload = preloadCard(info)
	}

	toPreload.Page = info.Page

	ch := doPreload(ctx, toPreload)

	internal.PerformanceMark("preloadFinish")
	internal.PerformanceMeasure("preload", "preloadStart", "preloadFinish")

	gfx.NextFrame()

	<-ch

	ctx1 := ctx
	cancel := context.CancelFunc(func() {})

	var poppedURL string

	onPopURL(func(u string) {
		poppedURL = u

		cancel()
	})

	first := true

	for {
		cancel()
		ctx, cancel = context.WithCancel(ctx1)

		switch {
		case poppedURL != "":
			info = &router.PageInfo{}
			err = info.UnmarshalText([]byte(poppedURL))

			poppedURL = ""
		case first:
			first = false

			router.Replace(info)
		case err == nil && info != nil:
			audio.StopMusic()

			b, err := info.MarshalText()
			if err != nil {
				panic(err)
			}

			if info.ShouldRedirect() {
				cancel()

				doRedirect(string(b))

				return
			}

			pushURL(string(b))
		}

		if err != nil {
			panic(err)
		}

		if info == nil {
			cancel()

			os.Exit(0)

			return
		}

		switch info.Page {
		case router.PageNotFound:
			var cam gfx.Camera

			cam.SetDefaults()

			batch := sprites.NewBatch(&cam)
			sprites.DrawTextCentered(batch, sprites.FontD3Streetism, "Not Found", 0, 72, 0, 3, 3, sprites.White, false)

			batch.Render()

			os.Exit(0)
		case router.PageAphidFestival:
			var g *festival.Game

			g, err = festival.New(ctx)
			if err != nil {
				break
			}

			info, err = visual.Loop(ctx, g)
		case router.PageELF:
			var g *fighters.Game

			g, err = fighters.New(ctx)
			if err != nil {
				break
			}

			info, err = visual.Loop(ctx, g)
		case router.PageBugTables:
			err = errors.New("main: Bug Tables is currently not configured to build")
			/*
				var s *tables.State

				s, err = tables.New(ctx)
				if err != nil {
					break
				}

				info, err = visual.Loop(ctx, s)
			*/
		case router.PageArcadeMain:
			var mm mainmenu.MainMenu

			info, err = mm.Run(ctx)
		case router.PageArcadeGame:
			seed := info.Seed
			if seed == "" {
				seed = rng.RandomSeed()
			}

			var create game.CreateFunc

			switch info.Game {
			case arcade.MiteKnight:
				rules := arcade.DefaultMiteKnightRules
				decodeRules(&rules, info.Custom)
				create = miteknight.New(rules)
			case arcade.FlowerJourney:
				rules := arcade.DefaultFlowerJourneyRules
				decodeRules(&rules, info.Custom)
				create = flowerjourney.New(rules)
			default:
				panic("main: unhandled arcade game: " + info.Game.String())
			}

			_, err = game.RunGame(ctx, create, seed)

			if !router.FlagUseAI.IsSet() {
				info = &router.PageInfo{
					Page: router.PageArcadeMain,
				}
			}
		case router.PageArcadeHighScores:
			var hs highscores.HighScores

			hs.Game = info.Game
			hs.Mode = info.HSMode
			hs.Page = info.PageNum

			info, err = hs.Run(ctx)
		case router.PageArcadeRecording:
			var rv highscores.RecordingViewer

			rv.Code = info.Code

			err = rv.Run(ctx)

			info = &router.PageInfo{
				Page: router.PageArcadeMain,
			}
		case router.PageCardsHome:
			var v *visual.Visual

			visual.AsNPC = info.AsNPC
			visual.VersusNPC = info.VersusNPC

			v, err = visual.New(ctx, makeInit(info))
			if err != nil {
				break
			}

			info, err = visual.Loop(ctx, v)
		case router.PageCardsJoin:
			var v *visual.Visual

			v, err = visual.New(ctx, makeInit(info))
			if err != nil {
				break
			}

			err = v.JoinMatch(ctx, info.Code)
			if err != nil {
				break
			}

			info, err = visual.Loop(ctx, v)
		case router.PageCardsModes:
			var v *visual.Visual

			v, err = visual.New(ctx, makeInit(info))
			if err != nil {
				break
			}

			v.State = visual.StateSelectMode

			info, err = visual.Loop(ctx, v)
		case router.PageCardsDeck:
			var v *visual.Visual

			v, err = visual.New(ctx, makeInit(info))
			if err != nil {
				break
			}

			v.State = visual.StateDeckEditor

			err = v.SetDeck(ctx, info.Code)
			if err != nil {
				break
			}

			info, err = visual.Loop(ctx, v)
		case router.PageCardsDeckEditor:
			var v *visual.Visual

			v, err = visual.New(ctx, makeInit(info))
			if err != nil {
				break
			}

			v.State = visual.StateDeckEditor

			info, err = visual.Loop(ctx, v)
		case router.PageCardsRecording:
			var v *visual.Visual

			isVanilla := info.Mode == match.ModeVanilla

			i := makeInit(info)
			if isVanilla {
				i.Mode = match.ModeVanilla
			}

			v, err = visual.New(ctx, i)
			if err != nil {
				break
			}

			v.SetRecording(ctx, info.Code)

			info, err = visual.Loop(ctx, v)
		case router.PageCardsEditor:
			info, err = editor.Run(ctx, info.Custom, info.Mode)
			if err != nil {
				panic(err)
			}

			b, err := info.MarshalText()
			if err != nil {
				panic(err)
			}

			doRedirect(string(b))

			info = nil
		default:
			panic("main: unhandled page: " + info.Page.String())
		}
	}
}

func makeInit(info *router.PageInfo) *match.Init {
	m := info.Mode
	if m == match.ModeVanilla {
		if info.Revision != 0 {
			info.Variant = uint64(info.Revision)
			info.Revision = 0
		}

		info.Mode = ""
		m = ""
	} else if m != "" && m != match.ModeCustom {
		m += "." + strconv.Itoa(info.Revision)
	}

	return &match.Init{
		Mode:    m,
		Version: internal.Version,
		Variant: info.Variant,
		Custom:  info.Custom,
	}
}

func decodeRules(rules arcade.GameRules, custom string) {
	if custom == "" {
		return
	}

	b, err := base64.StdEncoding.DecodeString(custom)
	if err != nil {
		panic(err)
	}

	var r format.Reader

	r.Init(b)

	arcade.UnmarshalRules(&r, rules)
}
