FROM golang

ENV DEBIAN_FRONTEND=noninteractive

RUN apt-get update \
 && apt-get install -y libegl1-mesa-dev libgles2-mesa-dev libwebp-dev libx11-dev \
 && wget -Oktx.deb https://github.com/KhronosGroup/KTX-Software/releases/download/v4.3.2/KTX-Software-4.3.2-Linux-x86_64.deb \
 && echo '43c71a9a82a23cb2acaecf493a45771139ec08be00ad6c67830e89bd5d3a7f0bd8112053518b370c3d0c4043b1faa530dd1d73e1a7712906538d657df659c10b  ktx.deb' | sha512sum -c \
 && apt-get install -y ./ktx.deb \
 && rm -rf ktx.deb /var/lib/apt/lists/*

COPY go.* /go/src/spy-cards/

RUN cd /go/src/spy-cards \
 && go mod download \
 && go build -v \
        github.com/BenLubar/convnet/... \
        github.com/davecgh/go-spew/spew \
        github.com/google/uuid \
        github.com/jackc/pgx/v4/stdlib \
        github.com/pion/webrtc/v3 \
        golang.org/x/crypto/bcrypt \
        golang.org/x/image/draw \
        golang.org/x/mobile/asset \
        golang.org/x/mobile/event/... \
        golang.org/x/mobile/gl \
        golang.org/x/sync/singleflight \
        golang.org/x/text/secure/precis \
        github.com/coder/websocket

COPY . /go/src/spy-cards/

RUN cd /go/src/spy-cards \
 && go build -tags headless -v -o /usr/local/bin/spy-cards

EXPOSE 8335

CMD ["spy-cards"]
