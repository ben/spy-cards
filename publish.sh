#!/bin/bash -e

# This relies on some scripts I have locally.
# You're probably looking for build.sh.

#cid=$(ipfs add --quieter --recursive --hidden --pin=false www)
#ipfs files rm -r /data/spy-cards.lubar.me
#ipfs files cp "/ipfs/$cid" /data/spy-cards.lubar.me

#publish-ipfs spy-cards.lubar.me

rsync -avz --delete www/ /storage/spy-cards/httproot
(cd /storage/spy-cards && docker compose up --build -d)
