//go:build ((linux && !android) || windows || (darwin && !ios)) && !headless
// +build linux,!android windows darwin,!ios
// +build !headless

package main

import (
	"git.lubar.me/ben/spy-cards/input"
	"golang.org/x/mobile/event/key"
)

var defaultKeyButton = [input.NumButtons]string{
	input.BtnUp:      key.CodeUpArrow.String(),
	input.BtnDown:    key.CodeDownArrow.String(),
	input.BtnLeft:    key.CodeLeftArrow.String(),
	input.BtnRight:   key.CodeRightArrow.String(),
	input.BtnConfirm: key.CodeC.String(),
	input.BtnCancel:  key.CodeX.String(),
	input.BtnSwitch:  key.CodeZ.String(),
	input.BtnToggle:  key.CodeV.String(),
	input.BtnPause:   key.CodeEscape.String(),
	input.BtnHelp:    key.CodeReturnEnter.String(),
}
