class_name MatchSetup
extends Control

@export var handler: MatchmakingHandler

@onready var _all_states: Array[Control] = [
	%StateInvalid,
	%StateJoiningLobby,
	%StateWaitingForData,
	%StateWaitingForAssets,
	%StateLastFrame,
	%StateConsent,
]
@onready var _display_name: LineEdit = %DisplayName
@onready var _display_name_arcade: LineEdit = %DisplayNameArcade
@onready var _ready_button: Button = %ReadyButton

func _process(_delta: float) -> void:
	var selected_state := %StateInvalid

	if handler:
		if handler.state == MatchmakingHandler.ABORTED:
			# we're in an error state; make sure we're not covering up the crash dialog
			queue_free()
			return

		if handler.state == MatchmakingHandler.CONSENT:
			var last_frame: Control = %StateLastFrame
			var consent: Control = %StateConsent
			if not last_frame.visible and not consent.visible:
				selected_state = last_frame
			else:
				selected_state = consent
				if not consent.visible:
					if handler.max_players == 0:
						# special case: zero player (all bots) game mode
						handler.on_consent("", "")
						queue_free()
						return

					# time to set up!
					(%ContentWarningContainer as Control).visible = not handler.global.selected_variant.content_warning.is_empty()
					(%ContentWarningLabel as Label).text = handler.global.selected_variant.content_warning

					_display_name.text = PlayerPreferences.display_name
					_display_name.visible = (handler.global.selected_variant.flags & VariantDef.ARCADE_NAMES) == 0
					_display_name_arcade.text = PlayerPreferences.display_name_arcade
					_display_name_arcade.visible = (handler.global.selected_variant.flags & VariantDef.ARCADE_NAMES) != 0
					(%ArcadeNameRulesLabel as Control).visible = (handler.global.selected_variant.flags & VariantDef.ARCADE_NAMES) != 0

					if (handler.global.selected_variant.flags & VariantDef.ARCADE_NAMES) == 0:
						_on_display_name_text_changed(_display_name.text)
						if _ready_button.disabled:
							_display_name.grab_focus.call_deferred()
						else:
							_ready_button.grab_focus.call_deferred()
					else:
						_on_display_name_arcade_text_changed(_display_name_arcade.text)
						if _ready_button.disabled:
							_display_name_arcade.grab_focus.call_deferred()
						else:
							_ready_button.grab_focus.call_deferred()

					if (handler.global.selected_variant.flags & VariantDef.HAS_SAVED_DATA) != 0:
						(%PersistentDataContainer as Control).visible = true
						# TODO: actual selection UI
					else:
						(%PersistentDataContainer as Control).visible = false
		elif len(handler.connections) > 0 and handler.connections[0]:
			var p1conn := handler.connections[0]
			if p1conn.remote_id == 1:
				# we are joining a lobby
				if p1conn.multiplayer.multiplayer_peer is OfflineMultiplayerPeer:
					selected_state = %StateJoiningLobby
				elif handler.state == MatchmakingHandler.ASSETS:
					var assets_remaining: Label = %AssetsRemainingLabel
					assets_remaining.text = tr(&"Downloading required files... %d remaining") % FileRequester.count_pending_requests()
					selected_state = %StateWaitingForAssets
				else:
					var player_number: Label = %PlayerNumberLabel
					player_number.text = tr(&"You are player number %d.") % handler.multiplayer.get_unique_id()
					var connection_latency: Label = %ConnectionLatencyLabel
					var latency := p1conn.get_average_ping()
					if is_nan(latency):
						connection_latency.text = tr(&"Measuring connection latency...")
					else:
						connection_latency.text = tr(&"Measuring connection latency... %0.1f ms") % latency
					var waiting_for_data: Label = %WaitingForDataLabel
					waiting_for_data.visible = not is_nan(latency)
					selected_state = %StateWaitingForData

	assert(selected_state in _all_states)
	for state in _all_states:
		state.visible = state == selected_state

static var _ARCADE_NAME_INVALID := RegEx.create_from_string("[^A-Z0-9]")

func _on_display_name_arcade_text_changed(new_text: String) -> void:
	var cleaned := new_text.to_upper()
	cleaned = _ARCADE_NAME_INVALID.sub(cleaned, "", true)
	if cleaned != new_text:
		var cursor := _display_name_arcade.caret_column if len(cleaned) == len(new_text) else len(cleaned)
		_display_name_arcade.text = cleaned
		_display_name_arcade.caret_column = cursor

	_ready_button.disabled = len(cleaned) != 3

func _on_display_name_arcade_text_submitted(_new_text: String) -> void:
	_on_ready_button_pressed()

func _on_display_name_text_changed(new_text: String) -> void:
	_ready_button.disabled = len(new_text.strip_edges()) == 0

func _on_display_name_text_submitted(_new_text: String) -> void:
	_on_ready_button_pressed()

func _on_ready_button_pressed() -> void:
	if _ready_button.disabled:
		return

	if handler.global.selected_variant.flags & VariantDef.ARCADE_NAMES:
		PlayerPreferences.display_name_arcade = _display_name_arcade.text
		PlayerPreferences.save()
		handler.on_consent(PlayerPreferences.display_name_arcade, "")
	else:
		PlayerPreferences.display_name = _display_name.text.strip_edges()
		PlayerPreferences.save()
		handler.on_consent(PlayerPreferences.display_name, "")

	queue_free()
