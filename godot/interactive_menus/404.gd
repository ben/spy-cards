extends Control

func _ready() -> void:
	var random_anger_sprite: TextureRect = %RandomAngerSprite
	random_anger_sprite.texture = Predefined.ICON[CrashHandler.ANGER_SPRITES.pick_random()]

	var button: Button = %Button
	button.grab_focus()

func _on_button_pressed() -> void:
	Router.goto("/game/modes")
