class_name CrashHandler
extends Control

const ANGER_SPRITES: Array[IconDef.Icon] = [
	IconDef.CHARACTER_AMBER_ANGRY,
	IconDef.CHARACTER_ARIA_ANGRY,
	IconDef.CHARACTER_ARIE_ANGRY,
	IconDef.CHARACTER_ASTOTHELES_ANGRY,
	IconDef.CHARACTER_BOMBY_ANGRY,
	IconDef.CHARACTER_BUGI_ANGRY,
	IconDef.CHARACTER_CARMINA_ANGRY,
	IconDef.CHARACTER_CELIA_ANGRY,
	IconDef.CHARACTER_CENN_ANGRY,
	IconDef.CHARACTER_CERISE_ANGRY,
	IconDef.CHARACTER_CHOMPY_ANGRY,
	IconDef.CHARACTER_CHUBEE_ANGRY,
	IconDef.CHARACTER_CHUCK_ANGRY,
	IconDef.CHARACTER_CRISBEE_ANGRY,
	IconDef.CHARACTER_CROW_ANGRY,
	IconDef.CHARACTER_DIANA_ANGRY,
	IconDef.CHARACTER_EREMI_ANGRY,
	IconDef.CHARACTER_FUTES_ANGRY,
	IconDef.CHARACTER_GENOW_ANGRY,
	IconDef.CHARACTER_HB_ANGRY,
	IconDef.CHARACTER_HONEYCOMB_ANGRY,
	IconDef.CHARACTER_JANET_ANGRY,
	IconDef.CHARACTER_JAUNE_ANGRY,
	IconDef.CHARACTER_JAYDE_ANGRY,
	IconDef.CHARACTER_JOHNNY_ANGRY,
	IconDef.CHARACTER_KABBU_ANGRY,
	IconDef.CHARACTER_KAGE_ANGRY,
	IconDef.CHARACTER_KALI_ANGRY,
	IconDef.CHARACTER_KENNY_ANGRY,
	IconDef.CHARACTER_KINA_ANGRY,
	IconDef.CHARACTER_LAYNA_ANGRY,
	IconDef.CHARACTER_LEIF_ANGRY,
	IconDef.CHARACTER_LEVI_ANGRY,
	IconDef.CHARACTER_MAKI_ANGRY,
	IconDef.CHARACTER_MALBEE_ANGRY,
	IconDef.CHARACTER_MAR_ANGRY,
	IconDef.CHARACTER_MOTHIVA_ANGRY,
	IconDef.CHARACTER_NEOLITH_ANGRY,
	IconDef.CHARACTER_NERO_ANGRY,
	IconDef.CHARACTER_PIBU_ANGRY,
	IconDef.CHARACTER_PISCI_ANGRY,
	IconDef.CHARACTER_RITCHEE_ANGRY,
	IconDef.CHARACTER_RIZ_ANGRY,
	IconDef.CHARACTER_SAMIRA_ANGRY,
	IconDef.CHARACTER_SCARLET_ANGRY,
	IconDef.CHARACTER_SERENE_ANGRY,
	IconDef.CHARACTER_SHAY_ANGRY,
	IconDef.CHARACTER_SKIRBY_ANGRY,
	IconDef.CHARACTER_TANJERIN_ANGRY,
	IconDef.CHARACTER_ULTIMAX_ANGRY,
	IconDef.CHARACTER_VANESSA_ANGRY,
	IconDef.CHARACTER_VI_ANGRY,
	IconDef.CHARACTER_YIN_ANGRY,
	IconDef.CHARACTER_ZARYANT_ANGRY,
	IconDef.CHARACTER_ZASP_ANGRY,
]

static func show_generic_error(err: String) -> void:
	var tree: SceneTree = Engine.get_main_loop()
	tree.change_scene_to_packed(Predefined.CRASH_HANDLER)

	await tree.process_frame

	var handler: CrashHandler = tree.current_scene
	handler.set_generic_error(err)

static func show_jigsaw_error(err: JigsawError) -> void:
	var tree: SceneTree = Engine.get_main_loop()
	tree.change_scene_to_packed(Predefined.CRASH_HANDLER)

	await tree.process_frame

	var handler: CrashHandler = tree.current_scene
	handler.set_jigsaw_error(err)

signal cleanup

func set_jigsaw_error(err: JigsawError) -> void:
	push_error(err.message)

	var error_message: Label = %ErrorMessage
	var error_code: TextEdit = %ErrorCode
	error_message.text = err.message
	error_code.visible = true
	error_code.text = Base32.encode_crockford(err.marshal())

	breakpoint

func set_generic_error(err: String) -> void:
	push_error(err)

	var error_message: Label = %ErrorMessage
	var error_code: TextEdit = %ErrorCode
	error_message.text = err
	error_code.visible = false

func _ready() -> void:
	var random_anger_sprite: TextureRect = %RandomAngerSprite
	random_anger_sprite.texture = Predefined.ICON[ANGER_SPRITES.pick_random()]

func _on_quit_button_pressed() -> void:
	cleanup.emit()
	Router.goto("/game/modes")
	queue_free()
