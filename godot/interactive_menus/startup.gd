extends Control

func _ready() -> void:
	if OS.has_feature("dedicated_server"):
		add_child(DedicatedServerRPC.new())
		return

	FileRequester.fetch_predefined()

	if OS.get_cmdline_args().has("--run-tests"):
		get_tree().change_scene_to_file.call_deferred("res://interactive_menus/test_grid.tscn")
		return

	Router.init.call_deferred()
