class_name Router

static var query: Dictionary[String, PackedStringArray] = {}
static var fragment: String = ""
static var _should_redirect: bool
static var _popstate_callback: JavaScriptObject

static func get_query(key: String) -> String:
	if key in query:
		return query[key][0]

	return ""

static func record_in_history_directly(path: String, replace: bool = false) -> void:
	assert(path.begins_with("/") and not path.begins_with("//"), "path should be domain-relative (start with a / but not two)")

	if OS.has_feature("web"):
		var history := JavaScriptBridge.get_interface("history")
		history.call(&"replaceState" if replace else &"pushState", null, "", path)

static func goto(path: String, record_in_history: bool = true) -> void:
	assert(path.begins_with("/") and not path.begins_with("//"), "path should be domain-relative (start with a / but not two)")

	_should_redirect = false

	var path_search_hash := path.split("#", false, 2)
	if len(path_search_hash) > 1:
		fragment = "#" + path_search_hash[1]
	else:
		fragment = ""

	var path_search := path_search_hash[0].split("?", false, 2)

	query.clear()
	if len(path_search) > 1:
		for arg in path_search[1].split("&", false):
			var kv := arg.split("=", true, 2)
			var key := kv[0]
			var value := kv[1] if len(kv) > 1 else ""
			if key in query:
				query[key].append(value)
			else:
				query[key] = PackedStringArray([value])

	var route := _router
	for component in path_search[0].split("/", false):
		route = route.call(component)

	if path_search[0].ends_with(".html") and route == _not_found:
		_should_redirect = true

	if OS.has_feature("web"):
		if _should_redirect:
			var location := JavaScriptBridge.get_interface("location")
			location.set(&"href", path)
			return

		if record_in_history:
			var history := JavaScriptBridge.get_interface("history")
			history.call(&"pushState", null, "", path)
	elif _should_redirect:
		OS.shell_open(APIEndpoints.ORIGIN + path)
		return

	route.call("")

static func init() -> void:
	var path := "/game/modes"
	if OS.has_feature("web"):
		var location := JavaScriptBridge.get_interface("location")
		path = location.get(&"pathname") + location.get(&"search") + location.get(&"hash")

		_popstate_callback = JavaScriptBridge.create_callback(_on_popstate)
		JavaScriptBridge.get_interface("window").call(&"addEventListener", "popstate", _popstate_callback)

		# TODO: this is temporary for testing
		if path == "/godot/":
			path = "/game/modes"

	goto(path, false)

const _BLANK := preload("res://interactive_menus/blank.tscn")
static func create_lobby(game_mode_container: DataContainer, selected_variant: int, mode_public_name: String, mode_public_revision: int) -> void:
	_set_scene(_BLANK)
	var handler := MatchmakingHandler.create_lobby(game_mode_container, selected_variant, mode_public_name, mode_public_revision)
	handler.lobby_created.connect(func(lobby_id: String) -> void:
		record_in_history_directly("/game/join/" + lobby_id))

static func _on_popstate(_args: Array) -> void:
	var location := JavaScriptBridge.get_interface("location")
	var path: String = location.get(&"pathname") + location.get(&"search") + location.get(&"hash")

	goto(path, false)

static func _router(path_component: String) -> Callable:
	match path_component:
		"":
			return _not_found("")
		"elf.html":
			return _elf
		"festival.html":
			return _festival
		"game.html":
			return _legacy_game
		"game":
			return _game
		"arcade":
			return _arcade
		"spy-cards", "docs":
			_should_redirect = true
			return _not_found
		_:
			return _not_found

static func _set_scene(scene: PackedScene) -> void:
	var tree: SceneTree = Engine.get_main_loop()
	tree.change_scene_to_packed(scene)

const _NOT_FOUND := preload("res://interactive_menus/404.tscn")
static func _not_found(path_component: String) -> Callable:
	if not path_component:
		_set_scene(_NOT_FOUND)
		return _done

	return _not_found

static func _done() -> void:
	assert(false, "should not have been called")

static func _elf(path_component: String) -> Callable:
	match path_component:
		"":
			breakpoint # TODO
			return _not_found("")
		_:
			return _not_found

static func _festival(path_component: String) -> Callable:
	match path_component:
		"":
			breakpoint # TODO
			return _not_found("")
		_:
			return _not_found

static func _legacy_game(path_component: String) -> Callable:
	match path_component:
		"":
			var mode := get_query("mode")
			if not mode:
				mode = "vanilla"

			if get_query("join"):
				return _join(get_query("join")).call("")
			elif get_query("state") == "select":
				return _modes("")
			elif get_query("deck"):
				@warning_ignore("unsafe_method_access")
				return _deck(mode).call(get_query("deck")).call("")
			elif "deck" in query or get_query("state") == "deck":
				return _deck(mode).call("")
			elif get_query("custom") == "editor":
				if get_query("edit"):
					return _legacy_card_editor(get_query("edit")).call("")
				if fragment:
					return _legacy_card_editor("legacy_custom").call("")
				return _legacy_card_editor("")
			elif get_query("recording"):
				return _recording(get_query("recording")).call("")
			else:
				breakpoint # TODO
				return _not_found("")
		_:
			return _not_found

static func _game(path_component: String) -> Callable:
	match path_component:
		"":
			return _not_found("")
		"join":
			return _join
		"modes":
			return _modes
		"recording":
			return _recording
		"editor":
			return _editor
		"preferences", "settings", "options":
			return _preferences
		"credits":
			return _credits
		_:
			return _not_found

const _MODE_SELECT := preload("res://interactive_menus/mode_select.tscn")
static func _modes(path_component: String) -> Callable:
	match path_component:
		"":
			_set_scene(_MODE_SELECT)

			return _done
		_:
			return _not_found

static func _deck(path_component: String) -> Callable:
	match path_component:
		"":
			breakpoint # TODO
			return _not_found("")
		_:
			breakpoint # TODO
			return _not_found

static func _legacy_card_editor(path_component: String) -> Callable:
	match path_component:
		"":
			breakpoint # TODO
			return _not_found("")
		_:
			breakpoint # TODO
			return _not_found

const _EDITOR_MAIN := preload("res://editor/editor_main.tscn")
static func _editor(path_component: String, path: String = "/game/editor") -> Callable:
	match path_component:
		"":
			EditorMain.navigate_to_on_ready = path
			_set_scene(_EDITOR_MAIN)
			return _done
		_:
			return _editor.bind(path + "/" + path_component)

const _PREFERENCES := preload("res://interactive_menus/preferences.tscn")
static func _preferences(path_component: String) -> Callable:
	match path_component:
		"":
			_set_scene(_PREFERENCES)
			return _done
		_:
			return _not_found

const _CREDITS := preload("res://interactive_menus/credits.tscn")
static func _credits(path_component: String) -> Callable:
	match path_component:
		"":
			_set_scene(_CREDITS)
			return _done
		_:
			return _not_found

const _RECORDING_PLAYER := preload("res://interactive_menus/recording_player.tscn")
static func _recording(path_component: String, match_id: String = "", mode: String = "") -> Callable:
	if path_component:
		if mode:
			return _not_found

		if match_id:
			if match_id != "auto":
				return _not_found

			return _recording.bind(match_id, path_component)

		return _recording.bind(path_component)

	if match_id:
		RecordingPlayer.match_id = match_id
		RecordingPlayer.match_id_history.clear()
		RecordingPlayer.mode_name = mode

		_set_scene(_RECORDING_PLAYER)

		return _done

	return _not_found("")

static func _join(path_component: String) -> Callable:
	match path_component:
		"":
			return _not_found("")
		_:
			return _join_lobby.bind(path_component)

static func _join_lobby(path_component: String, lobby_code: String) -> Callable:
	match path_component:
		"":
			_set_scene(_BLANK)

			MatchmakingHandler.join_lobby(lobby_code)

			return _done
		_:
			return _not_found

static func _arcade(path_component: String) -> Callable:
	match path_component:
		"":
			return _not_found("")
		_:
			return _not_found
