class_name TestGrid
extends Control

@onready var _fetch_data: HTTPRequest = %FetchData
@onready var _test_progress: ProgressBar = %TestProgress
@onready var _test_progress_text: Label = %TestProgressText
@onready var _sub_viewport_containers: Array[SubViewportContainer] = [
	%SubViewportContainer0, %SubViewportContainer1, %SubViewportContainer2, %SubViewportContainer3,
	%SubViewportContainer4, %SubViewportContainer5, %SubViewportContainer6, %SubViewportContainer7,
	%SubViewportContainer8, %SubViewportContainer9, %SubViewportContainer10, %SubViewportContainer11,
	%SubViewportContainer12, %SubViewportContainer13, %SubViewportContainer14, %SubViewportContainer15,
]
var _is_downloading := false
var _container_ready: Array[bool] = [
	true, true, true, true,
	true, true, true, true,
	true, true, true, true,
	true, true, true, true,
]
signal container_becomes_ready

func _ready() -> void:
	if OS.get_cmdline_args().has("--run-tests"):
		_on_confirmation_button_pressed()

func _on_confirmation_button_pressed() -> void:
	var confirmation: HBoxContainer = %Confirmation
	confirmation.visible = false
	confirmation.queue_free()

	_test_progress.visible = true
	_is_downloading = true

	var headers: PackedStringArray = []
	if not FileAccess.file_exists("user://testdata_dl.zip") and FileAccess.file_exists("user://testdata.zip") and FileAccess.file_exists("user://testdata_lastmodified.txt"):
		headers.append("If-Modified-Since: " + FileAccess.get_file_as_string("user://testdata_lastmodified.txt"))
	_fetch_data.download_file = "user://testdata_dl.zip"
	_fetch_data.request("https://spy-cards.lubar.me/spy-cards/oh_yeah_name_every_spy_card.zip", headers)

func _process(_delta: float) -> void:
	if _is_downloading:
		var body_size := _fetch_data.get_body_size()
		if body_size == -1:
			_test_progress.indeterminate = true
			_test_progress_text.text = ""
		else:
			_test_progress.indeterminate = false
			_test_progress.value = _fetch_data.get_downloaded_bytes()
			_test_progress.max_value = body_size
			_test_progress_text.text = "%.1f MiB / %.1f MiB" % [_test_progress.value / 1024.0 / 1024.0, body_size / 1024.0 / 1024.0]

func _on_fetch_test_data_request_completed(result: int, response_code: int, headers: PackedStringArray, _body: PackedByteArray) -> void:
	_is_downloading = false

	if result != HTTPRequest.RESULT_SUCCESS:
		push_error("failed to load test data: request returned " + WhyIsntThisInGodot.find_builtin_enum_key_name(&"HTTPRequest", &"Result", result))
		get_tree().quit()
		return

	if response_code == HTTPClient.RESPONSE_NOT_MODIFIED:
		DirAccess.remove_absolute("user://testdata_dl.zip")
	elif response_code == HTTPClient.RESPONSE_OK:
		DirAccess.remove_absolute("user://testdata.zip")
		DirAccess.rename_absolute("user://testdata_dl.zip", "user://testdata.zip")

		for header in headers:
			if header.get_slice(": ", 0).to_lower() == "last-modified":
				var last_modified := FileAccess.open("user://testdata_lastmodified.txt", FileAccess.WRITE)
				last_modified.store_string(header.get_slice(": ", 1))
				last_modified.close()
				break
	else:
		push_error("failed to load test data: request returned " + WhyIsntThisInGodot.find_builtin_enum_key_name(&"HTTPClient", &"ResponseCode", response_code))
		get_tree().quit()
		return

	var zip := ZIPReader.new()
	zip.open("user://testdata.zip")

	var files := zip.get_files()
	_test_progress.indeterminate = false
	_test_progress.value = 0
	_test_progress.max_value = len(files)

	for file in files:
		var container_index := _container_ready.find(true)
		if container_index == -1:
			await container_becomes_ready
			container_index = _container_ready.find(true)
			assert(container_index != -1)

		_test_progress.value += 1
		_test_progress_text.text = "%d / %d" % [_test_progress.value, _test_progress.max_value]

		var category := file.get_slice("/", 0)
		var recording_id := file.get_slice("/", 1)
		if recording_id == "":
			continue

		play_recording(container_index, category, recording_id, zip.read_file(file))

		# wait a frame between starting each recording so we don't freeze
		await get_tree().process_frame

	while _container_ready.has(false):
		# wait for all recordings to finish
		await container_becomes_ready

	zip.close()
	get_tree().quit()

static func get_recording_legacy_mode(buf: PackedByteArray) -> DataContainer:
	var card_codes := buf.get_string_from_utf8().split(",")
	return LegacyParse.card_set(card_codes, Predefined.VANILLA_1_2_1.mode)

static func get_recording_card(buf: PackedByteArray) -> DataContainer:
	return LegacyParse.card_recording(buf)

static func get_recording_arcade(buf: PackedByteArray) -> DataContainer:
	return LegacyParse.arcade_recording(buf)

func play_recording(index: int, category: String, recording_id: String, buf: PackedByteArray) -> void:
	assert(_container_ready[index])
	_container_ready[index] = false

	var container := _sub_viewport_containers[index]
	var sub_viewport: SubViewport = container.find_child("SubViewport")
	assert(sub_viewport.get_children().is_empty())

	var loading_screen := LoadingScreen.create("fetching assets")
	loading_screen.starting_request_count = FileRequester.total_requests

	var rec: DataContainer

	if category == "game_mode":
		var _client_slug := recording_id.get_slice(".", 0)
		var _revision := recording_id.get_slice(".", 1).to_int()

		rec = get_recording_legacy_mode(buf)
	elif category == "card_recording":
		rec = get_recording_card(buf)
	elif category == "arcade_recording":
		rec = get_recording_arcade(buf)
	else:
		assert(false)

	FileRequester.fetch_container(rec)

	if not rec:
		push_error("an error occurred parsing %s '%s'" % [category, recording_id])
		await get_tree().process_frame
		_container_ready[index] = true
		container_becomes_ready.emit()
		return

	var errors := rec.validate()
	assert(errors.is_empty())

	loading_screen.free_on_completed = true
	sub_viewport.add_child(loading_screen)
	await loading_screen.done

	for child in sub_viewport.get_children():
		sub_viewport.remove_child(child)
		child.queue_free()

	_container_ready[index] = true
	container_becomes_ready.emit()
