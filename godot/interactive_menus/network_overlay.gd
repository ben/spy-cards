class_name NetworkOverlay
extends Control

const _PLAYER_CONNECTION_INDICATOR := preload("res://interactive_menus/player_connection_indicator.tscn")

@export var handler: MatchmakingHandler

@onready var _big_wait: Control = %BigWait
@onready var _big_wait_label: Label = %BigWaitLabel

var _all_connected := false

func _ready() -> void:
	assert(handler)

	handler.lobby_created.connect(_update_lobby_info)
	_update_lobby_info()

func _process(_delta: float) -> void:
	assert(handler)

	_big_wait.visible = handler.state > MatchmakingHandler.CONSENT

	if handler.max_players == 0:
		return

	var big_check: GridContainer = %BigCheck
	if big_check.get_child_count() != handler.max_players:
		for child in big_check.get_children():
			big_check.remove_child(child)
			child.queue_free()

		for i in handler.max_players:
			var indicator: PlayerConnectionIndicator = _PLAYER_CONNECTION_INDICATOR.instantiate()
			big_check.add_child(indicator)
			indicator.handler = handler
			for conn in handler.connections:
				if conn.remote_id == i + 1:
					indicator.conn = conn
					break

	if not _all_connected:
		var all_connected_now := true
		for conn in handler.connections:
			if not conn.was_fully_connected:
				all_connected_now = false
				break

		if all_connected_now:
			_all_connected = true
			(%BigConnectLabel as Control).visible = false
			(%BigLobbyButton as Control).visible = false
			(%BigCopyLabel as Control).visible = false

	_big_wait_label.visible = _all_connected and handler.state not in [MatchmakingHandler.RESOLVE, MatchmakingHandler.REALTIME]

func _update_lobby_info(lobby_id := "") -> void:
	assert(handler)

	var code_button: Button = %BigLobbyButton
	if handler.lobby_id:
		code_button.text = tr(&"(hidden)") if PlayerPreferences.hide_lobby_code else handler.lobby_id
		code_button.disabled = false
	else:
		code_button.text = tr(&"(creating lobby)")
		code_button.disabled = true

func _on_lobby_button_pressed() -> void:
	DisplayServer.clipboard_set(APIEndpoints.ORIGIN + "/game/join/" + handler.lobby_id)
