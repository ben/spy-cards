class_name PlayerConnectionIndicator
extends Control

@export var handler: MatchmakingHandler
@export var conn: MatchmakingConnection

@onready var _portrait: MeshInstance2D = %Portrait
@onready var _player_number: Label = %PlayerNumber
@onready var _ready_check: TextureRect = %ReadyCheck

var _set_portrait := false

func _process(_delta: float) -> void:
	assert(handler)

	var player_number := conn.remote_id if conn else handler.multiplayer.get_unique_id()
	_player_number.text = str(player_number)

	if not _set_portrait and handler.recording and handler.recording.player_data[player_number - 1]:
		var character_id := handler.recording.player_data[player_number - 1].character
		var character_def := handler.recording.mode.characters[character_id - CharacterDef.FIRST_CUSTOM] if character_id != CharacterDef.NONE else null
		if character_def:
			character_def.set_portrait_2d(handler.recording.mode, _portrait)
		_set_portrait = true

	_portrait.visible = handler.state > MatchmakingHandler.CONSENT

	if handler.state <= MatchmakingHandler.START_WAIT:
		_ready_check.visible = conn.deck_ready if conn else handler.state == MatchmakingHandler.START_WAIT
	else:
		_ready_check.visible = false # TODO
