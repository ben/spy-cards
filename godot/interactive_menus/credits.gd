class_name Credits
extends Control

func _ready() -> void:
	var godot_before: Label = %GodotBefore
	godot_before.text = tr(&"Spy Cards Online v%s is made using the Godot engine (version %s).") % [ProjectSettings.get("application/config/version"), Engine.get_version_info()["string"]]
	var godot_license: Label = %GodotLicense
	godot_license.text = Engine.get_license_text().rstrip("\n")

	var close_button: Button = %CloseButton
	close_button.grab_focus.call_deferred()

func _on_close_button_pressed() -> void:
	Router.goto("/game/modes")

func _on_godot_show_more_pressed() -> void:
	var show_more_button: Button = %GodotShowMore
	show_more_button.disabled = true
	show_more_button.visible = false
	show_more_button.queue_free()

	var texts: PackedStringArray = []

	for info: Dictionary in Engine.get_copyright_info():
		var text: String = info["name"]

		for part: Dictionary in info["parts"]:
			# don't need part["files"]
			for copyright: String in part["copyright"]:
				text += "\nCopyright (c) " + copyright
			text += "\nLicense: " + part["license"]

		texts.append(text)

	for license: String in Engine.get_license_info().values():
		texts.append(license)

	var godot_license: Label = %GodotLicense
	godot_license.text = "\n\n".join(texts)
