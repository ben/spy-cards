class_name ModeSelect
extends Control

static var _INVALID_LOBBY_CODE_CHAR := RegEx.create_from_string("[^A-HJKMNP-TV-Z0-9]")
const _LOBBY_CODE_LENGTH := 6

@onready var _lobby_code_entry: LineEdit = %LobbyCodeEntry
@onready var _join_lobby_button: Button = %JoinLobbyButton

func _ready() -> void:
	var settings_button: Button = %SettingsButton
	settings_button.text = [
		tr(&"Preferences"),
		tr(&"Settings"),
		tr(&"Options"),
	][PlayerPreferences.preferences_label]
	_lobby_code_entry.secret = PlayerPreferences.hide_lobby_code
	_lobby_code_entry.grab_focus.call_deferred()
	_lobby_code_entry.unedit.call_deferred()

func _on_editor_button_pressed() -> void:
	Router.goto("/game/editor")

func _on_settings_button_pressed() -> void:
	Router.goto(PackedStringArray([
		"/game/preferences",
		"/game/settings",
		"/game/options",
	])[PlayerPreferences.preferences_label])

func _on_credits_button_pressed() -> void:
	Router.goto("/game/credits")

func _on_lobby_code_entry_text_changed(new_text: String) -> void:
	var cleaned := Base32.clean_crockford(new_text.trim_prefix(APIEndpoints.ORIGIN + "/game/join/"))
	cleaned = _INVALID_LOBBY_CODE_CHAR.sub(cleaned, "", true)
	if cleaned != new_text:
		var cursor := _lobby_code_entry.caret_column if len(cleaned) == len(new_text) else len(cleaned)
		_lobby_code_entry.text = cleaned
		_lobby_code_entry.caret_column = cursor

	_join_lobby_button.disabled = len(cleaned) != _LOBBY_CODE_LENGTH

func _on_lobby_code_entry_text_submitted(new_text: String) -> void:
	var cleaned := Base32.clean_crockford(new_text)
	if len(cleaned) != _LOBBY_CODE_LENGTH:
		return

	Router.goto("/game/join/" + cleaned)

func _on_join_lobby_button_pressed() -> void:
	_on_lobby_code_entry_text_submitted(_lobby_code_entry.text)

func _on_test_button_pressed() -> void:
	Router.create_lobby(Predefined.VANILLA_1_2_1, 0, "vanilla", 4)
