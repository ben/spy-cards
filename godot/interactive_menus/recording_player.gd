class_name RecordingPlayer
extends Control

const AUTO_REPEAT_LENGTH := 50
const MAX_TASKS_FOR_QUICK_LOAD := 500

static var match_id: String
static var match_id_history: PackedStringArray
static var mode_name: String

@onready var _requester: SimpleHTTPRequest = %SimpleHTTPRequest
@onready var _match_id_label: Label = %MatchIDLabel
@onready var _card_p1: Card3D = %CardP1
@onready var _card_p2: Card3D = %CardP2
@onready var _outstanding_tasks: Label = %OutstandingTasks
var global: JigsawGlobal
var visual: JigsawVisual

func _ready() -> void:
	visual = JigsawVisual.new()
	add_child(visual)

	if match_id == "auto":
		var starting_request_count := FileRequester.total_requests
		var next_match_id := await _fetch_next_auto_id()
		var recording := await _fetch_match(next_match_id)
		while recording:
			FileRequester._instance._check_async_tasks(MAX_TASKS_FOR_QUICK_LOAD)
			if FileRequester.count_pending_requests() != 0:
				var loading_screen := LoadingScreen.create("Loading match %s" % [next_match_id])
				loading_screen.starting_request_count = starting_request_count
				loading_screen.free_on_completed = true
				add_child(loading_screen)
				await loading_screen.done
			play_recording(next_match_id, recording, true)
			starting_request_count = FileRequester.total_requests
			next_match_id = await _fetch_next_auto_id()
			recording = await _fetch_match(next_match_id)
			await get_tree().create_timer(5.0).timeout
	else:
		play_recording(match_id, await _fetch_match(match_id), false)

func _process(_delta: float) -> void:
	_outstanding_tasks.text = "waiting for cache: %d\nwaiting for request slot: %d\nlive requests: %d\nwaiting for decoding: %d" % [len(FileRequester._instance._pre_queued_requests), len(FileRequester._instance._queued_requests), len(FileRequester._instance._live_requests) - FileRequester._instance._live_requests.count(null), len(FileRequester._instance._post_live_requests)]

	if Input.is_action_just_pressed(&"temp_wait"):
		var start := Time.get_ticks_usec()
		FileRequester._instance._check_async_tasks(1 << 30)
		print("took %.3f ms to force-wait for async tasks" % [(Time.get_ticks_usec() - start) * 0.001])

func _fetch_next_auto_id() -> String:
	_requester.request(APIEndpoints.random_match_recording(mode_name, match_id_history))
	var resp: SimpleHTTPRequest.Response = await _requester.response
	# TODO: error handling

	var next_match_id := resp.body.get_string_from_utf8()
	match_id_history.append(next_match_id)
	if len(match_id_history) > AUTO_REPEAT_LENGTH:
		match_id_history.remove_at(0)

	return next_match_id

func _fetch_match(id: String) -> DataContainer:
	_requester.request(APIEndpoints.get_match_recording(id))
	var resp: SimpleHTTPRequest.Response = await _requester.response
	# TODO: error handling

	var rec := LegacyParse.card_recording(resp.body)
	FileRequester.fetch_container(rec)

	return rec

func play_recording(_match_id: String, recording: DataContainer, _auto_advance: bool) -> void:
	if global:
		_card_p1.global = null
		_card_p1.card_index = -1
		_card_p2.global = null
		_card_p2.card_index = -1
		global.free()

	global = JigsawGlobal.new()
	global.mode = recording.mode
	global.state = JigsawState.new()
	global.state.audience = Audience.new()
	global.state.audience.meshes = AudienceMesh.create_multi_meshes(global.mode)
	global.visual = visual
	global.visual.audience = global.state.audience
	global.selected_variant = recording.mode.variants[recording.selected_variant]
	global.init_sides()
	add_child(global)

	_card_p1.global = global
	_card_p2.global = global

	var err := JigsawContext.make(global).set_persistent_variable(JigsawParameterVariable.make(VariableDef.REMATCHES), JigsawParameterAmount.make(recording.rematches), "rematches")
	if err:
		CrashHandler.show_jigsaw_error(err)
		return

	#@warning_ignore("integer_division")
	#var datetime := Time.get_datetime_dict_from_unix_time(recording.timestamp / 1000)
	#err = global.run_variant_triggers(JigsawTriggerVariant.COSMETIC_INIT, [
	#	JigsawParameterAmount.make(datetime["year"] as int),
	#	JigsawParameterAmount.make(datetime["month"] as int),
	#	JigsawParameterAmount.make(datetime["day"] as int),
	#	JigsawParameterAmount.make(recording.shared_seed.decode_u32(0)),
	#], RNG.with_seed(recording.shared_seed), true)
	#if err:
	#	CrashHandler.show_jigsaw_error(err)
	#	return

	for i in len(recording.player_data):
		global.state.sides[i + 1].player_name = recording.player_data[i].display_name
		global.state.sides[i + 1].character = recording.player_data[i].character
		global.state.sides[i + 1].initial_deck = Array(recording.player_data[i].initial_deck)

		#err = global.run_variant_triggers(JigsawTriggerVariant.CHARACTER_INIT, [
		#	JigsawParameterAmount.make(i + 1),
		#	JigsawParameterString.make(recording.player_data[i].display_name),
		#	JigsawParameterCharacter.make(recording.player_data[i].character),
		#], null, false)
		#if err:
		#	CrashHandler.show_jigsaw_error(err)
		#	return

		#var deck: Array[JigsawParameter] = Array(recording.player_data[i].initial_deck).map(JigsawParameterCard.make)
		#err = global.run_variant_triggers(JigsawTriggerVariant.DECK_INIT, [
		#	JigsawParameterAmount.make(i + 1),
		#	JigsawParameterOrderedList.make(deck),
		#], null, false)
		#if err:
		#	CrashHandler.show_jigsaw_error(err)
		#	return

	var match_text := tr(&"Match %s\nVersion %d.%d.%d\n%s.%d") % [_match_id, recording.game_version.x, recording.game_version.y, recording.game_version.z, recording.mode_public_name, recording.mode_public_revision]
	if recording.timestamp:
		@warning_ignore("integer_division")
		match_text += tr(&"\nPlayed %s") % [Time.get_date_string_from_unix_time(recording.timestamp / 1000)]
	_match_id_label.text = match_text

	# match start can contain pauses, so the easy mode ends here
	err = await run_pausable_triggers(collect_variant_triggers(JigsawTriggerVariant.MATCH_START), [], null, false)
	if err:
		CrashHandler.show_jigsaw_error(err)
		return

	var max_cards := maxi(len(recording.player_data[0].initial_deck), len(recording.player_data[1].initial_deck))
	var dur := 5.0 / max_cards
	for i in max_cards:
		var tween := get_tree().create_tween()
		if len(recording.player_data[0].initial_deck) > i:
			tween.tween_property(_card_p1, ^"rotation", Vector3(randf(), randf(), randf()) * 0.2 - Vector3(0.1, 0.1, 0.1), dur * 0.75)
			global.state.cards.append(CardInstance.make(global, recording.mode.get_card(recording.player_data[0].initial_deck[i])))
			_card_p1.card_index = len(global.state.cards) - 1
		if len(recording.player_data[1].initial_deck) > i:
			tween.tween_property(_card_p2, ^"rotation", Vector3(randf(), randf(), randf()) * 0.2 - Vector3(0.1, 0.1, 0.1), dur * 0.75)
			global.state.cards.append(CardInstance.make(global, recording.mode.get_card(recording.player_data[1].initial_deck[i])))
			_card_p2.card_index = len(global.state.cards) - 1
		await get_tree().create_timer(dur).timeout
	# TODO

func collect_variant_triggers(type: JigsawTriggerVariant.Type) -> Array[JigsawTrigger]:
	var triggers: Array[JigsawTrigger] = []

	for trigger in global.mode.base_triggers:
		if trigger.type == type:
			triggers.append(trigger)

	for trigger in global.selected_variant.triggers:
		if trigger.type == type:
			triggers.append(trigger)

	return triggers

func run_pausable_triggers(triggers: Array[JigsawTrigger], args: Array[JigsawParameter], rng: RNG, copy_rng: bool) -> JigsawError:
	for trigger in triggers:
		var context := JigsawContext.make(global)
		context.rng = rng.duplicate() if rng and copy_rng else rng

		assert(global.context_stack.is_empty())
		global.context_stack.push_back(context)

		var err := context.run(trigger, args)
		if err:
			return err

		while context.is_in_progress():
			await get_tree().create_timer(global.pause_frames * (1000.0 / 60.0)).timeout

			err = context.continue_run()
			if err:
				return err

		assert(len(global.context_stack) == 1)
		assert(global.context_stack[0] == context)
		global.context_stack.pop_back()

	return null

const _LEGACY_DEFAULT_STAGE_0_3_16: Array[JigsawParameter] = [
	Predefined.FORSAKEN_LANDS,
	Predefined.FORSAKEN_LANDS,
	Predefined.FORSAKEN_LANDS,
	Predefined.FORSAKEN_LANDS,
	Predefined.FORSAKEN_LANDS,
	Predefined.FORSAKEN_LANDS,
	Predefined.FORSAKEN_LANDS,
	Predefined.FORSAKEN_LANDS,
	Predefined.FORSAKEN_LANDS,
	Predefined.FORSAKEN_LANDS,
	Predefined.FORSAKEN_LANDS,
	Predefined.FORSAKEN_LANDS,
	Predefined.GOLDEN_HILLS,
	Predefined.METAL_ISLAND_AUDITORIUM,
	Predefined.METAL_ISLAND_AUDITORIUM,
	Predefined.METAL_ISLAND_AUDITORIUM,
	Predefined.FORSAKEN_LANDS,
	Predefined.FORSAKEN_LANDS,
	Predefined.FORSAKEN_LANDS,
	Predefined.FORSAKEN_LANDS,
	Predefined.GOLDEN_HILLS,
	Predefined.METAL_ISLAND_AUDITORIUM,
	Predefined.METAL_ISLAND_AUDITORIUM,
	Predefined.METAL_ISLAND_AUDITORIUM,
	Predefined.METAL_ISLAND_AUDITORIUM,
	Predefined.METAL_ISLAND_AUDITORIUM,
	Predefined.METAL_ISLAND_AUDITORIUM,
	Predefined.METAL_ISLAND_AUDITORIUM,
	Predefined.METAL_ISLAND_AUDITORIUM,
	Predefined.METAL_ISLAND_AUDITORIUM,
	Predefined.METAL_ISLAND_AUDITORIUM,
	Predefined.METAL_ISLAND_AUDITORIUM,
	Predefined.GOLDEN_HILLS,
	Predefined.FORSAKEN_LANDS,
	Predefined.METAL_ISLAND_AUDITORIUM,
	Predefined.METAL_ISLAND_AUDITORIUM,
	Predefined.METAL_ISLAND_AUDITORIUM,
	Predefined.METAL_ISLAND_AUDITORIUM,
	Predefined.METAL_ISLAND_AUDITORIUM,
	Predefined.METAL_ISLAND_AUDITORIUM,
	Predefined.METAL_ISLAND_AUDITORIUM,
	Predefined.METAL_ISLAND_AUDITORIUM,
	Predefined.METAL_ISLAND_AUDITORIUM,
	Predefined.METAL_ISLAND_AUDITORIUM,
	Predefined.METAL_ISLAND_AUDITORIUM,
	Predefined.METAL_ISLAND_AUDITORIUM,
	Predefined.METAL_ISLAND_AUDITORIUM,
	Predefined.METAL_ISLAND_AUDITORIUM,
	Predefined.GOLDEN_HILLS,
	Predefined.FORSAKEN_LANDS,
	Predefined.METAL_ISLAND_AUDITORIUM,
	Predefined.METAL_ISLAND_AUDITORIUM,
	Predefined.METAL_ISLAND_AUDITORIUM,
	Predefined.METAL_ISLAND_AUDITORIUM,
	Predefined.METAL_ISLAND_AUDITORIUM,
	Predefined.METAL_ISLAND_AUDITORIUM,
	Predefined.METAL_ISLAND_AUDITORIUM,
	Predefined.METAL_ISLAND_AUDITORIUM,
	Predefined.METAL_ISLAND_AUDITORIUM,
	Predefined.METAL_ISLAND_AUDITORIUM,
	Predefined.METAL_ISLAND_AUDITORIUM,
	Predefined.METAL_ISLAND_AUDITORIUM,
	Predefined.METAL_ISLAND_AUDITORIUM,
	Predefined.METAL_ISLAND_AUDITORIUM,
	Predefined.GOLDEN_HILLS,
	Predefined.FORSAKEN_LANDS,
	Predefined.METAL_ISLAND_AUDITORIUM,
	Predefined.METAL_ISLAND_AUDITORIUM,
	Predefined.METAL_ISLAND_AUDITORIUM,
	Predefined.METAL_ISLAND_AUDITORIUM,
	Predefined.METAL_ISLAND_AUDITORIUM,
	Predefined.METAL_ISLAND_AUDITORIUM,
	Predefined.METAL_ISLAND_AUDITORIUM,
	Predefined.METAL_ISLAND_AUDITORIUM,
	Predefined.METAL_ISLAND_AUDITORIUM,
	Predefined.METAL_ISLAND_AUDITORIUM,
	Predefined.METAL_ISLAND_AUDITORIUM,
	Predefined.METAL_ISLAND_AUDITORIUM,
	Predefined.METAL_ISLAND_AUDITORIUM,
	Predefined.METAL_ISLAND_AUDITORIUM,
	Predefined.FORSAKEN_LANDS,
	Predefined.FORSAKEN_LANDS,
	Predefined.FORSAKEN_LANDS,
	Predefined.FORSAKEN_LANDS,
	Predefined.GOLDEN_HILLS,
	Predefined.METAL_ISLAND_AUDITORIUM,
	Predefined.METAL_ISLAND_AUDITORIUM,
	Predefined.METAL_ISLAND_AUDITORIUM,
	Predefined.METAL_ISLAND_AUDITORIUM,
	Predefined.METAL_ISLAND_AUDITORIUM,
	Predefined.METAL_ISLAND_AUDITORIUM,
	Predefined.METAL_ISLAND_AUDITORIUM,
	Predefined.METAL_ISLAND_AUDITORIUM,
	Predefined.METAL_ISLAND_AUDITORIUM,
	Predefined.METAL_ISLAND_AUDITORIUM,
	Predefined.METAL_ISLAND_AUDITORIUM,
	Predefined.FORSAKEN_LANDS,
	Predefined.FORSAKEN_LANDS,
	Predefined.FORSAKEN_LANDS,
	Predefined.FORSAKEN_LANDS,
	Predefined.FORSAKEN_LANDS,
	Predefined.FORSAKEN_LANDS,
	Predefined.FORSAKEN_LANDS,
	Predefined.FORSAKEN_LANDS,
	Predefined.FORSAKEN_LANDS,
	Predefined.FORSAKEN_LANDS,
	Predefined.FORSAKEN_LANDS,
	Predefined.FORSAKEN_LANDS,
	Predefined.GOLDEN_HILLS,
	Predefined.METAL_ISLAND_AUDITORIUM,
	Predefined.METAL_ISLAND_AUDITORIUM,
	Predefined.METAL_ISLAND_AUDITORIUM,
]
const _LEGACY_DEFAULT_STAGE_0_3_17: Array[JigsawParameter] = [
	Predefined.FORSAKEN_LANDS,
	Predefined.FORSAKEN_LANDS,
	Predefined.FORSAKEN_LANDS,
	Predefined.FORSAKEN_LANDS,
	Predefined.FORSAKEN_LANDS,
	Predefined.FORSAKEN_LANDS,
	Predefined.FORSAKEN_LANDS,
	Predefined.FORSAKEN_LANDS,
	Predefined.FORSAKEN_LANDS,
	Predefined.FORSAKEN_LANDS,
	Predefined.FORSAKEN_LANDS,
	Predefined.FORSAKEN_LANDS,
	Predefined.GOLDEN_HILLS,
	Predefined.UNDERGROUND_TAVERN,
	Predefined.METAL_ISLAND_AUDITORIUM,
	Predefined.METAL_ISLAND_AUDITORIUM,
	Predefined.FORSAKEN_LANDS,
	Predefined.FORSAKEN_LANDS,
	Predefined.FORSAKEN_LANDS,
	Predefined.FORSAKEN_LANDS,
	Predefined.GOLDEN_HILLS,
	Predefined.UNDERGROUND_TAVERN,
	Predefined.UNDERGROUND_TAVERN,
	Predefined.UNDERGROUND_TAVERN,
	Predefined.METAL_ISLAND_AUDITORIUM,
	Predefined.METAL_ISLAND_AUDITORIUM,
	Predefined.METAL_ISLAND_AUDITORIUM,
	Predefined.METAL_ISLAND_AUDITORIUM,
	Predefined.METAL_ISLAND_AUDITORIUM,
	Predefined.METAL_ISLAND_AUDITORIUM,
	Predefined.METAL_ISLAND_AUDITORIUM,
	Predefined.METAL_ISLAND_AUDITORIUM,
	Predefined.GOLDEN_HILLS,
	Predefined.FORSAKEN_LANDS,
	Predefined.UNDERGROUND_TAVERN,
	Predefined.UNDERGROUND_TAVERN,
	Predefined.UNDERGROUND_TAVERN,
	Predefined.UNDERGROUND_TAVERN,
	Predefined.UNDERGROUND_TAVERN,
	Predefined.METAL_ISLAND_AUDITORIUM,
	Predefined.METAL_ISLAND_AUDITORIUM,
	Predefined.METAL_ISLAND_AUDITORIUM,
	Predefined.METAL_ISLAND_AUDITORIUM,
	Predefined.METAL_ISLAND_AUDITORIUM,
	Predefined.METAL_ISLAND_AUDITORIUM,
	Predefined.METAL_ISLAND_AUDITORIUM,
	Predefined.METAL_ISLAND_AUDITORIUM,
	Predefined.METAL_ISLAND_AUDITORIUM,
	Predefined.GOLDEN_HILLS,
	Predefined.FORSAKEN_LANDS,
	Predefined.UNDERGROUND_TAVERN,
	Predefined.UNDERGROUND_TAVERN,
	Predefined.UNDERGROUND_TAVERN,
	Predefined.UNDERGROUND_TAVERN,
	Predefined.UNDERGROUND_TAVERN,
	Predefined.METAL_ISLAND_AUDITORIUM,
	Predefined.METAL_ISLAND_AUDITORIUM,
	Predefined.METAL_ISLAND_AUDITORIUM,
	Predefined.METAL_ISLAND_AUDITORIUM,
	Predefined.METAL_ISLAND_AUDITORIUM,
	Predefined.METAL_ISLAND_AUDITORIUM,
	Predefined.METAL_ISLAND_AUDITORIUM,
	Predefined.METAL_ISLAND_AUDITORIUM,
	Predefined.METAL_ISLAND_AUDITORIUM,
	Predefined.GOLDEN_HILLS,
	Predefined.FORSAKEN_LANDS,
	Predefined.UNDERGROUND_TAVERN,
	Predefined.UNDERGROUND_TAVERN,
	Predefined.UNDERGROUND_TAVERN,
	Predefined.UNDERGROUND_TAVERN,
	Predefined.UNDERGROUND_TAVERN,
	Predefined.METAL_ISLAND_AUDITORIUM,
	Predefined.METAL_ISLAND_AUDITORIUM,
	Predefined.METAL_ISLAND_AUDITORIUM,
	Predefined.METAL_ISLAND_AUDITORIUM,
	Predefined.METAL_ISLAND_AUDITORIUM,
	Predefined.METAL_ISLAND_AUDITORIUM,
	Predefined.METAL_ISLAND_AUDITORIUM,
	Predefined.METAL_ISLAND_AUDITORIUM,
	Predefined.METAL_ISLAND_AUDITORIUM,
	Predefined.GOLDEN_HILLS,
	Predefined.GOLDEN_HILLS,
	Predefined.GOLDEN_HILLS,
	Predefined.GOLDEN_HILLS,
	Predefined.FORSAKEN_LANDS,
	Predefined.UNDERGROUND_TAVERN,
	Predefined.UNDERGROUND_TAVERN,
	Predefined.UNDERGROUND_TAVERN,
	Predefined.METAL_ISLAND_AUDITORIUM,
	Predefined.METAL_ISLAND_AUDITORIUM,
	Predefined.METAL_ISLAND_AUDITORIUM,
	Predefined.METAL_ISLAND_AUDITORIUM,
	Predefined.METAL_ISLAND_AUDITORIUM,
	Predefined.METAL_ISLAND_AUDITORIUM,
	Predefined.METAL_ISLAND_AUDITORIUM,
	Predefined.METAL_ISLAND_AUDITORIUM,
	Predefined.GOLDEN_HILLS,
	Predefined.GOLDEN_HILLS,
	Predefined.GOLDEN_HILLS,
	Predefined.GOLDEN_HILLS,
	Predefined.FORSAKEN_LANDS,
	Predefined.UNDERGROUND_TAVERN,
	Predefined.UNDERGROUND_TAVERN,
	Predefined.UNDERGROUND_TAVERN,
	Predefined.METAL_ISLAND_AUDITORIUM,
	Predefined.METAL_ISLAND_AUDITORIUM,
	Predefined.METAL_ISLAND_AUDITORIUM,
	Predefined.METAL_ISLAND_AUDITORIUM,
	Predefined.METAL_ISLAND_AUDITORIUM,
	Predefined.METAL_ISLAND_AUDITORIUM,
	Predefined.METAL_ISLAND_AUDITORIUM,
	Predefined.METAL_ISLAND_AUDITORIUM,
	Predefined.GOLDEN_HILLS,
	Predefined.GOLDEN_HILLS,
	Predefined.GOLDEN_HILLS,
	Predefined.GOLDEN_HILLS,
	Predefined.FORSAKEN_LANDS,
	Predefined.UNDERGROUND_TAVERN,
	Predefined.UNDERGROUND_TAVERN,
	Predefined.UNDERGROUND_TAVERN,
	Predefined.METAL_ISLAND_AUDITORIUM,
	Predefined.METAL_ISLAND_AUDITORIUM,
	Predefined.METAL_ISLAND_AUDITORIUM,
	Predefined.METAL_ISLAND_AUDITORIUM,
	Predefined.METAL_ISLAND_AUDITORIUM,
	Predefined.METAL_ISLAND_AUDITORIUM,
	Predefined.METAL_ISLAND_AUDITORIUM,
	Predefined.METAL_ISLAND_AUDITORIUM,
	Predefined.GOLDEN_HILLS,
	Predefined.GOLDEN_HILLS,
	Predefined.GOLDEN_HILLS,
	Predefined.GOLDEN_HILLS,
	Predefined.GOLDEN_HILLS,
	Predefined.GOLDEN_HILLS,
	Predefined.GOLDEN_HILLS,
	Predefined.GOLDEN_HILLS,
	Predefined.GOLDEN_HILLS,
	Predefined.GOLDEN_HILLS,
	Predefined.GOLDEN_HILLS,
	Predefined.GOLDEN_HILLS,
	Predefined.FORSAKEN_LANDS,
	Predefined.UNDERGROUND_TAVERN,
	Predefined.METAL_ISLAND_AUDITORIUM,
	Predefined.METAL_ISLAND_AUDITORIUM,
	Predefined.GOLDEN_HILLS,
	Predefined.GOLDEN_HILLS,
	Predefined.GOLDEN_HILLS,
	Predefined.GOLDEN_HILLS,
	Predefined.GOLDEN_HILLS,
	Predefined.GOLDEN_HILLS,
	Predefined.GOLDEN_HILLS,
	Predefined.GOLDEN_HILLS,
	Predefined.GOLDEN_HILLS,
	Predefined.GOLDEN_HILLS,
	Predefined.GOLDEN_HILLS,
	Predefined.GOLDEN_HILLS,
	Predefined.FORSAKEN_LANDS,
	Predefined.UNDERGROUND_TAVERN,
	Predefined.METAL_ISLAND_AUDITORIUM,
	Predefined.METAL_ISLAND_AUDITORIUM,
	Predefined.FORSAKEN_LANDS,
	Predefined.FORSAKEN_LANDS,
	Predefined.FORSAKEN_LANDS,
	Predefined.FORSAKEN_LANDS,
	Predefined.GOLDEN_HILLS,
	Predefined.UNDERGROUND_TAVERN,
	Predefined.UNDERGROUND_TAVERN,
	Predefined.UNDERGROUND_TAVERN,
	Predefined.METAL_ISLAND_AUDITORIUM,
	Predefined.METAL_ISLAND_AUDITORIUM,
	Predefined.METAL_ISLAND_AUDITORIUM,
	Predefined.METAL_ISLAND_AUDITORIUM,
	Predefined.METAL_ISLAND_AUDITORIUM,
	Predefined.METAL_ISLAND_AUDITORIUM,
	Predefined.METAL_ISLAND_AUDITORIUM,
	Predefined.METAL_ISLAND_AUDITORIUM,
	Predefined.FORSAKEN_LANDS,
	Predefined.FORSAKEN_LANDS,
	Predefined.FORSAKEN_LANDS,
	Predefined.FORSAKEN_LANDS,
	Predefined.FORSAKEN_LANDS,
	Predefined.FORSAKEN_LANDS,
	Predefined.FORSAKEN_LANDS,
	Predefined.FORSAKEN_LANDS,
	Predefined.FORSAKEN_LANDS,
	Predefined.FORSAKEN_LANDS,
	Predefined.FORSAKEN_LANDS,
	Predefined.FORSAKEN_LANDS,
	Predefined.GOLDEN_HILLS,
	Predefined.UNDERGROUND_TAVERN,
	Predefined.METAL_ISLAND_AUDITORIUM,
	Predefined.METAL_ISLAND_AUDITORIUM,
]

#func _legacy_default_stage(recording: Recording) -> JigsawParameterFileIDGLTF:
#	# if the recording is newer than v0.3.27, the last pre-Godot version, just use the original default (we handle this in the actual mode scripting now)
#	# if the recording is older than v0.3.16, the first version to have multiple stages, use the original default
#	if recording.game_version.x != 0 or recording.game_version.y != 3 or recording.game_version.z < 16:
#		return Predefined.METAL_ISLAND_AUDITORIUM
#
#	@warning_ignore("integer_division")
#	var month := Time.get_date_dict_from_unix_time(recording.match_start_time_ms / 1000)["month"] as Time.Month
#	var legacy_default_stage_index := (month - Time.MONTH_JANUARY) * 16 + (recording.shared_seed[0] >> 4)
#
#	if recording.game_version.z == 16:
#		return _LEGACY_DEFAULT_STAGE_0_3_16[legacy_default_stage_index]
#
#	return _LEGACY_DEFAULT_STAGE_0_3_17[legacy_default_stage_index]
#
#func _legacy_default_music(recording: Recording) -> JigsawParameterFileIDOpus:
#	if recording.mode_name == "custom":
#		return Predefined.MUSIC_BOUNTY
#
#	return Predefined.MUSIC_MINIBOSS
