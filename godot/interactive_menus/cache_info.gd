class_name CacheInfo
extends VBoxContainer

const _SPECIFIC_CACHE_FILES: PackedStringArray = [
	"user://testdata.zip",
	"user://testdata_lastmodified.txt",
]

func _ready() -> void:
	init_cache_info()

func _enter_tree() -> void:
	FileRequester._instance.cache_written.connect(init_cache_info)

func _exit_tree() -> void:
	FileRequester._instance.cache_written.disconnect(init_cache_info)

func init_cache_info() -> void:
	var cache_size := WhyIsntThisInGodot.get_directory_size(OS.get_user_data_dir().path_join("fileid_cache"))
	if DirAccess.dir_exists_absolute("user://legacy_cid_cache"):
		cache_size += WhyIsntThisInGodot.get_directory_size(OS.get_user_data_dir().path_join("legacy_cid_cache"))
	if DirAccess.dir_exists_absolute("user://shader_cache"):
		cache_size += WhyIsntThisInGodot.get_directory_size(OS.get_user_data_dir().path_join("shader_cache"))
	for filename in _SPECIFIC_CACHE_FILES:
		if FileAccess.file_exists(filename):
			cache_size += FileAccess.open(filename, FileAccess.READ).get_length()

	var total_size := WhyIsntThisInGodot.get_directory_size(OS.get_user_data_dir())

	var desc: Label = %CacheDescLabel
	desc.text = tr(&"Spy Cards Online stores local copies of files like icons and sounds that are used by game modes you play or watch so you don't need to re-download them every time. You can clear the cache, which will remove all of the files from the cache and then re-download each file the next time it is needed. Clearing your cache here will not affect your local data or settings.\nYour file cache is currently %s.\nIn total, Spy Cards Online is using (for cache and persistent data combined) %s on your device.") % [String.humanize_size(cache_size), String.humanize_size(total_size)]

	if OS.has_feature("web"):
		var navigator := JavaScriptBridge.get_interface("navigator")
		var storage: JavaScriptObject = navigator.get(&"storage") if navigator else null
		if storage and storage.get(&"persist"):
			var persisted: bool = await storage.call(&"persisted")
			var estimate: JavaScriptObject = await storage.call(&"estimate")
			var quota: int = estimate.get(&"quota") if estimate else -1
			var usage: int = estimate.get(&"usage") if estimate else -1

			var persist_button: Button = %CachePersistButton
			persist_button.visible = not persisted

			var desc2: Label = %CacheDescLabel2
			desc2.visible = true
			if persisted:
				desc2.text = tr(&"Your browser reports that Spy Cards Online data is stored persistently. This means that it will not be removed unless you tell your browser to remove it.\nSpy Cards Online is using %s of the %s your browser says it will allow.") % [String.humanize_size(usage), String.humanize_size(quota)]
			else:
				desc2.text = tr(&"Your browser reports that Spy Cards Online data is stored in \"best-effort\" mode. This means that it may be deleted if your device is running low on space or you haven't visited Spy Cards Online in a while.\nSpy Cards Online is using %s of the %s your browser says it will allow.") % [String.humanize_size(usage), String.humanize_size(quota)]

func _on_clear_cache_button_pressed() -> void:
	for filename in DirAccess.get_files_at("user://fileid_cache"):
		DirAccess.remove_absolute("user://fileid_cache".path_join(filename))
	if DirAccess.dir_exists_absolute("user://legacy_cid_cache"):
		for filename in DirAccess.get_files_at("user://legacy_cid_cache"):
			DirAccess.remove_absolute("user://legacy_cid_cache".path_join(filename))
	if DirAccess.dir_exists_absolute("user://shader_cache"):
		# shader_cache/ShaderName/hash1/hash2.cache
		for dirname in DirAccess.get_directories_at("user://shader_cache"):
			for dir2name in DirAccess.get_directories_at("user://shader_cache".path_join(dirname)):
				for filename in DirAccess.get_files_at("user://shader_cache".path_join(dirname).path_join(dir2name)):
					DirAccess.remove_absolute("user://shader_cache".path_join(dirname).path_join(dir2name))
				DirAccess.remove_absolute("user://shader_cache".path_join(dirname).path_join(dir2name))
			DirAccess.remove_absolute("user://shader_cache".path_join(dirname))
	for filename in _SPECIFIC_CACHE_FILES:
		if FileAccess.file_exists(filename):
			DirAccess.remove_absolute(filename)

	init_cache_info()

func _on_cache_persist_button_pressed() -> void:
	var persist_button: Button = %CachePersistButton
	persist_button.disabled = true

	var navigator := JavaScriptBridge.get_interface("navigator")
	var storage: JavaScriptObject = navigator.get(&"storage")
	await storage.call(&"persist")

	persist_button.disabled = false
	persist_button.visible = false
	init_cache_info()
