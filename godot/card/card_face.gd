class_name CardFace
extends SubViewport

var global: JigsawGlobal:
	set(new_value):
		if global != new_value:
			if global:
				global.current_effect_changed.disconnect(_update_if_current_effect)
			global = new_value
			if global:
				global.current_effect_changed.connect(_update_if_current_effect)

var card_index: int = -1:
	set(new_value):
		if card_index != new_value:
			card_index = new_value
			_reset_display()

var _card_name := SquishLabel.new()
var _card_description := SquishLabel.new()
var _simple_description_icon: Array[TextureRect]
var _simple_description_text: Array[SquishLabel]
var _stickers: Array[TextureRect]
var _costs: Array[CardFaceCost]
var _tribes: Array[CardFaceTribe]
var _was_active: bool

func _ready() -> void:
	EditorHooks.connect_action(_reset_display)
	get_window().size_changed.connect(_reset_display)
	size_2d_override_stretch = true
	disable_3d = true
	transparent_bg = true

	render_target_clear_mode = SubViewport.CLEAR_MODE_NEVER
	render_target_update_mode = SubViewport.UPDATE_DISABLED

	_card_name.theme_type_variation = &"CardNameLabel"
	_card_name.z_index = 3
	add_child(_card_name, false, Node.INTERNAL_MODE_BACK)

	_card_description.theme_type_variation = &"CardDescriptionLabel"
	_card_description.z_index = 3
	add_child(_card_description, false, Node.INTERNAL_MODE_BACK)

func get_card() -> CardInstance:
	if global and card_index >= 0 and card_index < len(global.state.cards):
		return global.state.cards[card_index]
	return null

func _reset_display() -> void:
	var card := get_card()
	if not card:
		_card_name.clear()
		_card_description.clear()
		for i in _simple_description_icon:
			i.visible = false
			i.queue_free()
		_simple_description_icon.clear()
		for t in _simple_description_text:
			t.visible = false
			t.queue_free()
		_simple_description_text.clear()
		for s in _stickers:
			s.visible = false
			s.queue_free()
		_stickers.clear()
		for c in _costs:
			c.visible = false
			c.queue_free()
		_costs.clear()
		for t in _tribes:
			t.visible = false
			t.queue_free()
		_tribes.clear()
		return
	if not get_window():
		return

	var mode := global.mode
	var design := card.get_design(global)

	var window_size: Vector2 = get_window().size
	var reference_window_size: Vector2 = get_window().content_scale_size
	var viewport_scale := window_size / reference_window_size
	var scaling_factor := maxf(minf(viewport_scale.x, viewport_scale.y), 1.0) * PlayerPreferences.card_face_resolution

	var card_size := design.base_transform.basis * Vector3.ONE * design.pixel_size
	var card_size_2d := Vector2(card_size.x, card_size.y)
	var pixel_size := Vector2i((card_size_2d * scaling_factor).round()).maxi(2)

	set_deferred(&"size", pixel_size)
	set_deferred(&"size_2d_override", Vector2i(card_size_2d.round()))

	var name_pos := design.card_name_pos

	if design.card_costs_shrink_name and len(card.costs) > 0:
		match design.card_costs_grow_mode:
			CardDesign.GrowMode.GROW_LEFT:
				var distance := minf(design.card_costs_pos.size.x * len(card.costs) + design.card_costs_spacing * (len(card.costs) - 1), name_pos.size.x)
				name_pos.size.x -= distance
			CardDesign.GrowMode.GROW_RIGHT:
				var distance := minf(design.card_costs_pos.size.x * len(card.costs) + design.card_costs_spacing * (len(card.costs) - 1), name_pos.size.x)
				name_pos.position.x += distance
				name_pos.size.x -= distance
			CardDesign.GrowMode.GROW_UP:
				var distance := minf(design.card_costs_pos.size.y * len(card.costs) + design.card_costs_spacing * (len(card.costs) - 1), name_pos.size.y)
				name_pos.size.y -= distance
			CardDesign.GrowMode.GROW_DOWN:
				var distance := minf(design.card_costs_pos.size.y * len(card.costs) + design.card_costs_spacing * (len(card.costs) - 1), name_pos.size.y)
				name_pos.position.y += distance
				name_pos.size.y -= distance

	var card_tribes: Array[TribeDef.Tribe] = Array(Array(card.tribes).filter(func(t: TribeDef.Tribe) -> bool: return mode.get_tribe(t) and mode.get_tribe(t).display != TribeDef.DisplayMode.HIDDEN), TYPE_INT, "", null)
	if design.card_tribes_shrink_name and len(card_tribes) > 0:
		match design.card_tribes_grow_mode:
			CardDesign.GrowMode.GROW_LEFT:
				var distance := minf(design.card_tribes_pos.size.x * len(card_tribes) + design.card_tribes_spacing * (len(card_tribes) - 1), name_pos.size.x)
				name_pos.size.x -= distance
			CardDesign.GrowMode.GROW_RIGHT:
				var distance := minf(design.card_tribes_pos.size.x * len(card_tribes) + design.card_tribes_spacing * (len(card_tribes) - 1), name_pos.size.x)
				name_pos.position.x += distance
				name_pos.size.x -= distance
			CardDesign.GrowMode.GROW_UP:
				var distance := minf(design.card_tribes_pos.size.y * len(card_tribes) + design.card_tribes_spacing * (len(card_tribes) - 1), name_pos.size.y)
				name_pos.size.y -= distance
			CardDesign.GrowMode.GROW_DOWN:
				var distance := minf(design.card_tribes_pos.size.y * len(card_tribes) + design.card_tribes_spacing * (len(card_tribes) - 1), name_pos.size.y)
				name_pos.position.y += distance
				name_pos.size.y -= distance

	_card_name.position = name_pos.position
	_card_name.text_scale = design.card_name_scale
	_card_name.max_size = name_pos.size
	_card_name.set_formatted_text(card.name, global, card_index, Color(), 0)
	if name_pos.size.y != 0.0:
		_card_name.size = Vector2(design.card_name_scale.x * name_pos.size.x, 0.0)
	else:
		_card_name.size = Vector2.ZERO

	_card_description.position = design.card_description_pos.position
	_card_description.text_scale = design.card_description_scale
	_card_description.max_size = design.card_description_pos.size
	_card_description.set_formatted_text(card.description, global, card_index, design.description_background_color, design.effect_highlight_width)
	if design.card_description_pos.size.y != 0.0:
		_card_description.size = Vector2(design.card_description_scale.x * design.card_description_pos.size.x, 0.0)
	else:
		_card_description.size = Vector2.ZERO

	var simple_description_length := len(_simple_description_icon)
	assert(simple_description_length == len(_simple_description_text))
	while len(card.simple_description) < simple_description_length:
		simple_description_length -= 1
		_simple_description_icon[simple_description_length].visible = false
		_simple_description_icon[simple_description_length].queue_free()
		_simple_description_icon.remove_at(simple_description_length)
		_simple_description_text[simple_description_length].visible = false
		_simple_description_text[simple_description_length].queue_free()
		_simple_description_text.remove_at(simple_description_length)

	while len(card.simple_description) > simple_description_length:
		simple_description_length += 1

		var icon := TextureRect.new()
		_simple_description_icon.append(icon)
		icon.expand_mode = TextureRect.EXPAND_IGNORE_SIZE
		icon.stretch_mode = TextureRect.STRETCH_KEEP_ASPECT_CENTERED
		icon.z_index = 3
		add_child(icon)

		var text := SquishLabel.new()
		_simple_description_text.append(text)
		text.theme_type_variation = &"CardSimpleDescriptionLabel"
		text.z_index = 4
		add_child(text)

	for i in simple_description_length:
		var bounds := CardFace.compute_grow_bounds(i, simple_description_length, design.simple_description_pos, design.simple_description_grow_mode, design.simple_description_spacing, true)
		_simple_description_icon[i].position = bounds.position
		_simple_description_icon[i].size = bounds.size
		if design.simple_description_icon_align < 0:
			_simple_description_icon[i].size.x = lerpf(bounds.size.x, bounds.size.y, -design.simple_description_icon_align)
		elif design.simple_description_icon_align > 0:
			var offset := (bounds.size.x - bounds.size.y) * design.simple_description_icon_align
			_simple_description_icon[i].position.x += offset
			_simple_description_icon[i].size.x -= offset

		_simple_description_icon[i].texture = mode.get_icon_texture(card.simple_description[i].icon)

		var text_bounds := bounds
		if design.simple_description_text_align < 0:
			text_bounds.size.x = lerpf(bounds.size.x, bounds.size.y, -design.simple_description_text_align)
		elif design.simple_description_text_align > 0:
			var offset := (bounds.size.x - bounds.size.y) * design.simple_description_text_align
			text_bounds.position.x += offset
			text_bounds.size.x -= offset
		text_bounds.position += design.simple_description_text_padding.position
		text_bounds.size -= design.simple_description_text_padding.position + design.simple_description_text_padding.size

		var text := _simple_description_text[i]

		# layout problems when reusing; just kill the label and re-make it every time
		text.visible = false
		text.queue_free()
		text = SquishLabel.new()
		_simple_description_text[i] = text
		text.theme_type_variation = &"CardSimpleDescriptionLabel"
		text.z_index = 4
		add_child(text)

		text.text_scale = design.simple_description_text_scale
		text.max_size = Vector2(text_bounds.size.x, 0)
		var update_position := func() -> void:
			text.position.x = text_bounds.position.x + (text_bounds.size.x - text.size.x * text.scale.x) * (design.simple_description_text_align + 1) / 2
			text.position.y = text_bounds.position.y
		text.resized.connect(update_position, CONNECT_DEFERRED | CONNECT_ONE_SHOT)
		text.finished.connect(update_position, CONNECT_DEFERRED | CONNECT_ONE_SHOT)
		text.set_formatted_text(card.simple_description[i].text, global, card_index, design.simple_description_border_color, design.simple_description_border_thickness)

	if len(_stickers) > len(design.stickers):
		for i in range(len(design.stickers), len(_stickers)):
			_stickers[i].visible = false
			_stickers[i].queue_free()
		_stickers.resize(len(design.stickers))

	for i in len(design.stickers):
		if len(_stickers) <= i:
			var rect := TextureRect.new()
			rect.expand_mode = TextureRect.EXPAND_IGNORE_SIZE
			rect.z_index = 1
			add_child(rect, false, Node.INTERNAL_MODE_BACK)
			_stickers.append(rect)

		var sticker := design.stickers[i]
		_stickers[i].texture = mode.get_icon_texture(sticker.icon)
		_stickers[i].position = sticker.position.position
		_stickers[i].size = sticker.position.size

		if sticker.should_show:
			var context := JigsawContext.make(global)
			var err := context.evaluate(sticker.should_show, [JigsawParameterCardInstance.make(card_index)], [JigsawParameterBoolean.make(true)])
			if err:
				push_warning("computing sticker visibility: ", err) # TODO: error handling
			else:
				var should_show: JigsawParameterBoolean = context.results[0]
				_stickers[i].visible = should_show.boolean
		else:
			_stickers[i].visible = true

	if len(_costs) > len(card.costs):
		for i in range(len(card.costs), len(_costs)):
			_costs[i].visible = false
			_costs[i].queue_free()
		_costs.resize(len(card.costs))

	for i in len(card.costs):
		if len(_costs) <= i:
			var node := CardFaceCost.new()
			node.z_index = 2
			add_child(node, false, Node.INTERNAL_MODE_BACK)
			_costs.append(node)

		_costs[i].card_index = -1
		_costs[i].stat = card.costs[i].stat
		_costs[i].amount = card.costs[i].amount
		_costs[i].amount_inf = card.costs[i].amount_inf
		_costs[i].nan = card.costs[i].nan
		_costs[i].global = global
		_costs[i].card_index = card_index # assign card last so it doesn't run the setup code repeatedly
		var bounds := CardFace.compute_grow_bounds(i, len(card.costs), design.card_costs_pos, design.card_costs_grow_mode, design.card_costs_spacing, true)
		_costs[i].position = bounds.position
		_costs[i].size = bounds.size
		_costs[i]._update()

	if len(_tribes) > len(card_tribes):
		for i in range(len(card_tribes), len(_tribes)):
			_tribes[i].visible = false
			_tribes[i].queue_free()
		_tribes.resize(len(card_tribes))

	for i in len(card_tribes):
		if len(_tribes) <= i:
			var node := CardFaceTribe.new()
			node.z_index = 2
			add_child(node, false, Node.INTERNAL_MODE_BACK)
			_tribes.append(node)

		var tribe := mode.get_tribe(card_tribes[i])
		var wide := (tribe.display == TribeDef.DisplayMode.WIDE or tribe.display == TribeDef.DisplayMode.WIDER) and len(card_tribes) == 1
		_tribes[i].global = global
		_tribes[i].card_index = card_index
		_tribes[i].tribe_name = FormattedText.make_plain(tribe.name)
		_tribes[i].color = tribe.color
		_tribes[i].wide = wide
		_tribes[i].wider = tribe.display == TribeDef.DisplayMode.WIDER
		var bounds := CardFace.compute_grow_bounds(i, len(card_tribes), design.card_tribes_pos, design.card_tribes_grow_mode, design.card_tribes_spacing, wide)
		_tribes[i].position = bounds.position
		_tribes[i].size = bounds.size
		_tribes[i]._update()

	if card.description_requires_update():
		render_target_clear_mode = SubViewport.CLEAR_MODE_ALWAYS
		render_target_update_mode = SubViewport.UPDATE_ALWAYS
	else:
		render_target_clear_mode = SubViewport.CLEAR_MODE_ONCE
		render_target_update_mode = SubViewport.UPDATE_ONCE

func _update_if_current_effect() -> void:
	if render_target_update_mode == SubViewport.UPDATE_ALWAYS:
		return # already updating every frame

	var active := global.current_card_instance == get_card()
	if _was_active and not active:
		active = true
		_was_active = false
	else:
		_was_active = active

	render_target_clear_mode = SubViewport.CLEAR_MODE_ONCE
	render_target_update_mode = SubViewport.UPDATE_ONCE

static func compute_grow_bounds(i: int, count: int, base_pos: Rect2, grow_mode: CardDesign.GrowMode, spacing: float, wide: bool) -> Rect2:
	match grow_mode:
		CardDesign.GrowMode.GROW_LEFT:
			if i != 0:
				base_pos.position.x -= i * (base_pos.size.x + spacing)
			return base_pos
		CardDesign.GrowMode.GROW_RIGHT:
			if i != 0:
				base_pos.position.x += i * (base_pos.size.x + spacing)
			return base_pos
		CardDesign.GrowMode.GROW_UP:
			if i != 0:
				base_pos.position.y -= i * (base_pos.size.y + spacing)
			return base_pos
		CardDesign.GrowMode.GROW_DOWN:
			if i != 0:
				base_pos.position.y += i * (base_pos.size.y + spacing)
			return base_pos
		CardDesign.GrowMode.GROW_HORIZONTAL:
			base_pos.position.x -= count * base_pos.size.x / 2.0 + (count - 1) * spacing / 2.0
			if i != 0:
				base_pos.position.x += i * (base_pos.size.x + spacing)
			return base_pos
		CardDesign.GrowMode.GROW_VERTICAL:
			base_pos.position.y -= count * base_pos.size.y / 2.0 + (count - 1) * spacing / 2.0
			if i != 0:
				base_pos.position.y += i * (base_pos.size.y + spacing)
			return base_pos
		CardDesign.GrowMode.SHRINK_HORIZONTAL:
			if not wide and count == 1:
				base_pos.size.x /= 2.0
				base_pos.position.x += base_pos.size.x / 2.0
			base_pos.size.x -= (count - 1) * spacing
			base_pos.size.x /= count
			if i != 0:
				base_pos.position.x += i * (base_pos.size.x + spacing)
			return base_pos
		CardDesign.GrowMode.SHRINK_VERTICAL:
			if not wide and count == 1:
				base_pos.size.y /= 2.0
				base_pos.position.y += base_pos.size.y / 2.0
			base_pos.size.y -= (count - 1) * spacing
			base_pos.size.y /= count
			if i != 0:
				base_pos.position.y += i * (base_pos.size.y + spacing)
			return base_pos
		_:
			push_error("unhandled grow mode %d %s" % [grow_mode, WhyIsntThisInGodot.find_builtin_enum_key_name(&"CardDesign", &"GrowMode", grow_mode)])
			return Rect2()
