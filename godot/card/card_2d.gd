class_name Card2D
extends ColorRect

const SHADER := preload("res://card/card_2d.gdshader")

var global: JigsawGlobal
var card_index: int = -1:
	set(new_value):
		if card_index != new_value:
			card_index = new_value
			_card_changed()

var _shader: ShaderMaterial
var _face: CardFace

func _ready() -> void:
	if material is ShaderMaterial:
		_shader = material
	else:
		_shader = ShaderMaterial.new()
	_shader.shader = SHADER
	material = _shader

	_card_changed()

func _card_changed() -> void:
	if not _shader:
		return
	if card_index == -1:
		return

	var card := global.state.cards[card_index]

	var design := card.get_design(global)
	var card_size := design.base_transform.basis * Vector3(1, 1, 0)
	custom_minimum_size = Vector2(card_size.x, card_size.y) * design.pixel_size
	size = custom_minimum_size

	if not _face:
		_face = CardFace.new()
		add_child(_face, false, Node.INTERNAL_MODE_BACK)
		_shader.set_shader_parameter(&"card_viewport", _face.get_texture())

	_face.global = global
	_face.card_index = card_index

	Card3D.setup_shader(_shader, global, card)
