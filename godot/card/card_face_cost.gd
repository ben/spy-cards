class_name CardFaceCost
extends TextureRect

var global: JigsawGlobal
var card_index: int = -1:
	set(new_value):
		if card_index != new_value:
			card_index = new_value
			_update()
var stat: StatDef.Stat = StatDef.NONE:
	set(new_value):
		if stat != new_value:
			stat = new_value
			_update()
var amount: int:
	set(new_value):
		if amount != new_value:
			amount = new_value
			_update()
var amount_inf: int:
	set(new_value):
		if amount_inf != new_value:
			amount_inf = new_value
			_update()
var nan: bool:
	set(new_value):
		if nan != new_value:
			nan = new_value
			_update()

var _number_label: SquishLabel

func _ready() -> void:
	expand_mode = ExpandMode.EXPAND_IGNORE_SIZE
	_number_label = SquishLabel.new()
	_number_label.theme_type_variation = &"CardCostLabel"
	_number_label.centered = true
	add_child(_number_label, false, Node.INTERNAL_MODE_BACK)
	resized.connect(_update)

func _update() -> void:
	if not _number_label:
		return
	if card_index != -1 and global and global.mode:
		var card := global.state.cards[card_index]
		var stat_def := global.mode.get_stat(stat)
		if not stat_def:
			return
		texture = global.mode.get_icon_texture(stat_def.icon)
		var amount_param := JigsawParameterAmount.make_nan() if nan else JigsawParameterAmount.make(amount, amount_inf)
		var formatted_text := FormattedText.make_plain(str(amount_param))
		if stat_def.format_cost:
			var context := JigsawContext.make(global)
			var err := context.evaluate(stat_def.format_cost, [JigsawParameterCardInstance.make(card_index), amount_param], [JigsawParameterFormattedText.make(formatted_text), JigsawParameterIcon.make(stat_def.icon)])
			if err:
				push_warning("computing card cost display: ", err) # TODO: error handling
			else:
				var formatted_text_param: JigsawParameterFormattedText = context.results[0]
				formatted_text = formatted_text_param.text
				var icon_param: JigsawParameterIcon = context.results[1]
				texture = global.mode.get_icon_texture(icon_param.icon)
		_number_label.text_scale = card.get_design(global).card_costs_text_scale
		_number_label.max_size = Vector2(size.x, 0.0)
		_number_label.set_formatted_text(formatted_text, global, card_index, Color(), 0)
		_number_label.size = Vector2.ZERO
