class_name CardFaceTribe
extends TextureRect

var global: JigsawGlobal
var card_index: int = -1:
	set(new_value):
		card_index = new_value
		_update()
var tribe_name: Array[FormattedText]:
	set(new_value):
		if tribe_name != new_value:
			tribe_name = new_value
			_update()
var color: Color:
	set(new_value):
		if color != new_value:
			color = new_value
			_update()
var wide: bool:
	set(new_value):
		if wide != new_value:
			wide = new_value
			_update()
var wider: bool:
	set(new_value):
		if wider != new_value:
			wider = new_value
			_update()

var _name_label: SquishLabel

func _ready() -> void:
	expand_mode = ExpandMode.EXPAND_IGNORE_SIZE
	_name_label = SquishLabel.new()
	_name_label.theme_type_variation = &"CardTribeLabel"
	_name_label.centered = true
	add_child(_name_label, false, Node.INTERNAL_MODE_BACK)
	resized.connect(_update)

func _update() -> void:
	if not _name_label:
		return
	if card_index != -1:
		var card := global.state.cards[card_index]
		var design := card.get_design(global)
		if design:
			texture = global.mode.get_icon_texture(design.tribe_bubble_wide if wide else design.tribe_bubble)
			_name_label.text_scale = design.card_tribes_text_scale
			if wider:
				_name_label.text_scale.x *= 2.0
	self_modulate = color
	_name_label.max_size = Vector2(size.x, 0.0)
	_name_label.set_formatted_text(tribe_name, global, card_index, Color(), 0)
	_name_label.size = Vector2.ZERO
