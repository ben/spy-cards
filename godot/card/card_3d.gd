class_name Card3D
extends MeshInstance3D

const SHADER := preload("res://card/card_3d.gdshader")

var global: JigsawGlobal
var card_index: int = -1:
	set(new_value):
		if card_index != new_value:
			card_index = new_value
			_card_changed()

var _shader: ShaderMaterial
var _quad: QuadMesh
var _body: AnimatableBody3D
var _shape: CollisionShape3D
var _box: BoxShape3D
var _face: CardFace

func _init() -> void:
	if mesh is QuadMesh:
		_quad = mesh
	else:
		_quad = QuadMesh.new()
	if _quad.material is ShaderMaterial:
		_shader = _quad.material
	else:
		_shader = ShaderMaterial.new()
	_shader.shader = SHADER
	_quad.material = _shader
	mesh = _quad

	_box = BoxShape3D.new()
	_shape = CollisionShape3D.new()
	_shape.shape = _box

	_body = AnimatableBody3D.new()
	_body.sync_to_physics = false
	_body.add_child(_shape)
	add_child(_body)

	gi_mode = GI_MODE_DYNAMIC

func _card_changed() -> void:
	if card_index == -1:
		if _face:
			_face.card_index = -1
		return

	var card := global.state.cards[card_index]

	var design := card.get_design(global)
	var card_size := design.base_transform.basis * Vector3(1, 1, 0)
	_quad.size = Vector2(card_size.x, card_size.y)
	_quad.center_offset = design.base_transform.origin
	_box.size = card_size + Vector3(0, 0, 1.0 / design.pixel_size)
	_shape.position = design.base_transform.origin + Vector3(-1, 1, 0) / design.pixel_size

	if not _face:
		_face = CardFace.new()
		add_child(_face, false, Node.INTERNAL_MODE_BACK)
		_shader.set_shader_parameter(&"card_viewport", _face.get_texture())

	_face.global = global
	_face.card_index = card_index

	Card3D.setup_shader(_shader, global, card)

static func setup_shader(shader: ShaderMaterial, _global: JigsawGlobal, card_instance: CardInstance) -> void:
	if not card_instance:
		return

	var mode := _global.mode
	var design := card_instance.get_design(_global)
	var rank := mode.get_rank(card_instance.rank)
	shader.set_shader_parameter(&"card_front", mode.get_icon_texture(design.card_front))
	shader.set_shader_parameter(&"card_front_window", mode.get_icon_texture(design.card_front_window))
	shader.set_shader_parameter(&"card_front_decoration", mode.get_icon_texture(rank.front) if rank else null)
	shader.set_shader_parameter(&"card_decoration_pos", sprite_transform_to_uv(design.rank_decoration_transform, design.base_transform))
	shader.set_shader_parameter(&"portrait", mode.get_icon_texture(card_instance.portrait))
	shader.set_shader_parameter(&"portrait_pos", sprite_transform_to_uv(design.portrait_transform, design.base_transform))
	shader.set_shader_parameter(&"card_rank_color", rank.get_color() if rank else Color.WHITE)
	shader.set_shader_parameter(&"back_only", _global.current_side in card_instance.face_down_for_side)

	shader.set_shader_parameter(&"card_back", mode.get_icon_texture(card_instance.back))
	shader.set_shader_parameter(&"fancy_smooth_corners", design.smooth_corners)

static func compute_sprite_transform(x: float, y: float, z: float, width: float, height: float, dpi: float, pivot_x: float, pivot_y: float) -> Transform3D:
	var pivot := Transform3D(Basis.IDENTITY, Vector3(0.5 - pivot_x, 0.5 - pivot_y, 0.0))
	var scaled := pivot.scaled(Vector3(width, height, dpi) / dpi)
	var move := scaled.translated(Vector3(x, y, z))
	return move

static func sprite_transform_to_uv(sprite: Transform3D, base: Transform3D) -> Transform3D:
	# hopefully someone smarter than me can optimize this, but it doesn't get called every frame
	const QUAD_UV_TO_WORLD := Transform3D(Basis(Vector3.RIGHT, Vector3.DOWN, Vector3.BACK), Vector3(-0.5, 0.5, 0.0))
	var uv_to_world := base * QUAD_UV_TO_WORLD
	var world_to_uv := uv_to_world.affine_inverse()
	return world_to_uv * base * sprite.affine_inverse() * uv_to_world
