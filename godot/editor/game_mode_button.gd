class_name GameModeButton
extends Button

var card: Card2D
var _slug: String

func _init(slug: String, revision: int, revisions: int, mode: GameModeSummary, title: String, rank: RankDef.Rank, color: Color) -> void:
	assert(not slug.trim_prefix("local/").contains("/"))
	_slug = slug
	if revision > 0:
		_slug = "%s.%d" % [slug, revision]
	elif revisions > 0:
		_slug = "%s.%d" % [slug, revisions]

	var fake_mode := GameMode.new()
	fake_mode.default_card_design = preload("res://predefined/mode/design_vanilla.tres").duplicate()

	# TODO: make this a config?
	if slug in ["hunters_journal", "pvz", "star-crossed"]:
		fake_mode.default_card_design.card_front_window = IconDef.NONE

	fake_mode.effects = [EffectDef.FLAVOR_TEXT]
	fake_mode.stats = [StatDef.TP]
	fake_mode.ranks = [RankDef.FIRST_CUSTOM]
	fake_mode.custom_ranks = [Predefined.RANK[rank].duplicate()]
	fake_mode.custom_ranks[0].front = IconDef.NONE
	fake_mode.tribes = [TribeDef.FIRST_CUSTOM]
	fake_mode.custom_tribes = [TribeDef.new()]
	fake_mode.custom_tribes[0].name = mode.author
	fake_mode.custom_tribes[0].color = color
	fake_mode.custom_tribes[0].display = TribeDef.WIDE
	fake_mode.custom_icons = mode.custom_icons

	var def := CardDef.new()
	def.name = title
	def.portrait = mode.mode_thumbnail
	if revision > 0 or revisions != 0:
		def.costs = [StatValue.new()]
		def.costs[0].stat = StatDef.TP
		def.costs[0].amount = revision if revision > 0 else revisions
	def.rank = RankDef.FIRST_CUSTOM
	if mode.author:
		def.tribes = [TribeDef.FIRST_CUSTOM]
	def.effects = [EffectInstance.new()]
	def.effects[0].priority = 0
	def.effects[0].effect = EffectDef.FLAVOR_TEXT
	def.effects[0].params = [
		JigsawParameterString.make(mode.description),
		JigsawParameterColor.make(Color.BLACK),
		JigsawParameterBoolean.make(false),
	]
	fake_mode.card_defs = [def]

	var global := JigsawGlobal.new()
	global.mode = fake_mode
	global.state = JigsawState.new()
	add_child(global)

	card = Card2D.new()
	card.global = global
	global.state.cards.append(CardInstance.make(global, def))
	card.card_index = len(global.state.cards) - 1
	card.mouse_filter = Control.MOUSE_FILTER_IGNORE
	add_child(card)

enum Command {
	OPEN = 0,
}

var _menu: PopupMenu

func _ready() -> void:
	custom_minimum_size = card.size
	_menu = PopupMenu.new()
	_menu.id_pressed.connect(_on_menu_id_pressed)
	_menu.add_item(tr(&"Open", &"open game mode in editor"), Command.OPEN)
	add_child(_menu)

func _pressed() -> void:
	_menu.position = get_global_mouse_position()
	_menu.popup()

func _on_menu_id_pressed(command: Command) -> void:
	match command:
		Command.OPEN:
			EditorMain.instance.navigate_to("/game/editor/mode/" + _slug)
		_:
			assert(false, "unhandled menu option")
