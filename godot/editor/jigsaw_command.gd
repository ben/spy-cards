class_name JigsawCommandEditor
extends Control

@export var command: JigsawCommand:
	set(new_value):
		if command != new_value:
			command = new_value
			_reset()

func _reset() -> void:
	pass # TODO
