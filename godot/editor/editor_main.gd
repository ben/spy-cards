class_name EditorMain
extends Control

const EDITOR_THEME: Dictionary[String, Theme] = {
	"light": preload("res://editor/editor_theme_light.tres"),
	"dark": preload("res://editor/editor_theme_dark.tres"),
}

static var instance: EditorMain
static var navigate_to_on_ready: String = "/game/editor"

@onready var tabs: TabContainer = %TabContainer

func _ready() -> void:
	instance = self

	assert(PlayerPreferences.theme in EDITOR_THEME)
	theme = EDITOR_THEME[PlayerPreferences.theme]

	navigate_to(navigate_to_on_ready)

func _on_tab_container_tab_selected(idx: int) -> void:
	if tabs == null:
		# being called in init
		return

	for i in tabs.get_tab_count():
		tabs.set_tab_button_icon(i, null)

	var tab: EditorTab = tabs.get_tab_control(idx)
	Router.record_in_history_directly(tab.get_url_path())
	if not tab.button.is_empty():
		tabs.set_tab_button_icon(idx, tabs.get_theme_icon(tab.button))

func _on_tab_container_tab_button_pressed(idx: int) -> void:
	if tabs == null:
		# being called in init
		return

	var tab: EditorTab = tabs.get_tab_control(idx)
	tab._on_button_pressed()

func find_tab_for_path(path: String) -> int:
	for i in tabs.get_tab_count():
		var tab: EditorTab = tabs.get_tab_control(i)
		if tab.get_url_path() == path:
			return i

	return -1

func navigate_to(path: String) -> void:
	var idx := find_tab_for_path(path)
	if idx == -1:
		idx = create_tab_for_path(path)
	if idx == -1:
		Router._not_found("")
		return

	tabs.current_tab = idx
	tabs.get_tab_bar().grab_focus.call_deferred()

func create_tab_for_path(path: String) -> int:
	if not path.begins_with("/game/editor/mode/"):
		push_warning("Cannot create editor tab for path " + path)
		return -1

	var p := path.trim_prefix("/game/editor/mode/")

	var is_local := p.begins_with("local/")
	p = p.trim_prefix("local/")

	if not p.contains("/"):
		if is_local:
			assert(false, "TODO")
			return create_tab_for_mode(p, null, true)
		else:
			if p in Predefined.BUILT_IN_MODES:
				return create_tab_for_mode(p, Predefined.BUILT_IN_MODES[p], false)

			assert(false, "TODO")
			return create_tab_for_mode(p, null, false)

	push_warning("Cannot create editor tab for path " + path)
	return -1

func create_tab_for_mode(slug: String, container: DataContainer, is_local: bool) -> int:
	var mode_tab := EditorTabMode.new(slug, container, is_local)
	tabs.add_child(mode_tab, true)
	var idx := tabs.get_tab_idx_from_control(mode_tab)
	tabs.set_tab_title(idx, mode_tab.title)
	return idx
