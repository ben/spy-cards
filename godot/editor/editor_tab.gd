class_name EditorTab
extends MarginContainer

@export var title: String
@export var button: StringName

func get_url_path() -> String:
	assert(false, "method should have been overridden: EditorTab.get_url_path")
	return "/game/editor/ERROR"

func _on_button_pressed() -> void:
	assert(false, "method should have been overridden: EditorTab._on_button_pressed")
