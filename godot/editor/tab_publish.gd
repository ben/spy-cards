class_name EditorTabPublish
extends EditorTab

@onready var _unavailable: Control = %UnavailableContainer
@onready var _available: Control = %AvailableContainer
@onready var _status_request: HTTPRequest = %StatusHTTPRequest
@onready var _checking: Control = %CheckingContainer
@onready var _error: Label = %ErrorLabel
@onready var _logged_in: Control = %LoggedInContainer
@onready var _logged_in_name: Label = %LoggedInLabel
@onready var _logged_out: Control = %LoggedOutContainer

func get_url_path() -> String:
	return "/game/editor/publish"

func _ready() -> void:
	if not OS.has_feature("web"):
		_unavailable.visible = true
		_available.visible = false
		return

	if JavaScriptBridge.get_interface("location").get("origin") != APIEndpoints.ORIGIN:
		_unavailable.visible = true
		_available.visible = false
		return

	var version_label: Label = %VersionLabel
	version_label.text = tr(&"%s v%s") % [ProjectSettings["application/config/name"], ProjectSettings["application/config/version"]]

	reset_status()

func reset_status() -> void:
	_checking.visible = true
	_error.visible = false
	_logged_out.visible = false
	_logged_in.visible = false

	var err := _status_request.request(APIEndpoints.STATUS)
	if err != OK and err != ERR_BUSY:
		show_error(tr(&"Failed to send a request for your account data: error %d: %s") % [err, error_string(err)])

func show_error(message: String) -> void:
	_checking.visible = false
	_error.visible = true
	_error.text = message
	_logged_out.visible = false
	_logged_in.visible = false

func _on_unavailable_desc_label_meta_clicked(meta: Variant) -> void:
	OS.shell_open(meta as String)

func _on_log_in_button_pressed() -> void:
	JavaScriptBridge.get_interface("location").set("href", APIEndpoints.LOG_IN)

func _on_log_out_button_pressed() -> void:
	JavaScriptBridge.get_interface("location").set("href", APIEndpoints.LOG_OUT)

func _on_status_http_request_request_completed(result: int, response_code: int, _headers: PackedStringArray, body: PackedByteArray) -> void:
	if result != HTTPRequest.RESULT_SUCCESS:
		show_error(tr(&"Unexpected result when retrieving account data: %s") % [WhyIsntThisInGodot.find_builtin_enum_key_name(&"HTTPRequest", &"Result", result)])
		return
	if response_code != HTTPClient.RESPONSE_OK:
		show_error(tr(&"Unexpected response code when retrieving account data: %s") % [WhyIsntThisInGodot.find_builtin_enum_key_name(&"HTTPClient", &"ResponseCode", response_code)])
		return

	var data: Dictionary = JSON.parse_string(body.get_string_from_utf8())
	var logged_in: bool = data["logged-in"]
	var user_name: String = data["user-name"]

	_logged_out.visible = not logged_in
	_logged_in.visible = logged_in
	_logged_in_name.text = tr(&"Currently logged in as %s.") % [user_name.to_upper()]
