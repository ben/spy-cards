# singleton EditorHooks
extends Node

signal editor_action

static var is_editor: bool 

func connect_action(callable: Callable) -> void:
	editor_action.connect(callable)

func disconnect_action(callable: Callable) -> void:
	editor_action.disconnect(callable)

func emit_action() -> void:
	editor_action.emit()
