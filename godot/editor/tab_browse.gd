class_name EditorTabBrowse
extends EditorTab

var _loading_screen: LoadingScreen

func get_url_path() -> String:
	return "/game/editor"

func _ready() -> void:
	_loading_screen = LoadingScreen.create(tr(&"Setting up..."))
	_loading_screen.starting_request_count = FileRequester.total_requests
	# TabContainer -> EditorMain
	get_parent_control().get_parent_control().add_child.call_deferred(_loading_screen)

	init_local_modes()
	init_vanilla_modes()
	init_termacade_modes()
	fetch_online_modes()

func init_local_modes() -> void:
	pass # TODO

func init_vanilla_modes() -> void:
	var container: HFlowContainer = %VanillaContainer
	@warning_ignore("assert_always_true")
	assert(Predefined.VANILLA_1_2_1 == Predefined.VANILLA_LATEST)
	container.add_child(GameModeButton.new("vanilla", 4, 4, Predefined.VANILLA_1_2_1.mode_summary, tr(&"Bug Fables 1.2.1"), RankDef.ATTACKER, Color8(105, 150, 46)))
	container.add_child(GameModeButton.new("vanilla", 3, 4, Predefined.VANILLA_1_1_1.mode_summary, tr(&"Bug Fables 1.1.1"), RankDef.ATTACKER, Color8(105, 150, 46)))
	container.add_child(GameModeButton.new("vanilla", 2, 4, Predefined.VANILLA_1_1.mode_summary, tr(&"Bug Fables 1.1"), RankDef.ATTACKER, Color8(105, 150, 46)))
	container.add_child(GameModeButton.new("vanilla", 1, 4, Predefined.VANILLA_1_0_5.mode_summary, tr(&"Bug Fables 1.0.5"), RankDef.ATTACKER, Color8(105, 150, 46)))

func init_termacade_modes() -> void:
	var container: HFlowContainer = %TermacadeContainer
	container.add_child(GameModeButton.new("miteknight", 1, 1, Predefined.MITE_KNIGHT.mode_summary, tr(&"Mite Knight"), RankDef.MINI_BOSS, Color8(255, 179, 96)))
	container.add_child(GameModeButton.new("flowerjourney", 1, 1, Predefined.FLOWER_JOURNEY.mode_summary, tr(&"Flower Journey"), RankDef.MINI_BOSS, Color8(255, 179, 96)))
	container.add_child(GameModeButton.new("aphidfestival", 1, 1, Predefined.APHID_FESTIVAL.mode_summary, tr(&"Aphid Festival"), RankDef.MINI_BOSS, Color8(255, 179, 96)))
	container.add_child(GameModeButton.new("whackaworm", 1, 1, Predefined.WHACK_A_WORM.mode_summary, tr(&"Whack-a-Worm"), RankDef.MINI_BOSS, Color8(255, 179, 96)))
	container.add_child(GameModeButton.new("catchworms", 1, 1, Predefined.CATCH_WORMS.mode_summary, tr(&"Catch Worms"), RankDef.MINI_BOSS, Color8(255, 179, 96)))

func fetch_online_modes() -> void:
	var request: HTTPRequest = %OnlineModesHTTPRequest
	request.request_completed.connect(init_online_modes)
	var err := request.request(APIEndpoints.GAME_MODES)
	if err:
		var progress_bar: ProgressBar = %OnlinePlaceholderProgressBar
		progress_bar.visible = false
		var desc: Label = %OnlinePlaceholderDescLabel
		desc.text = tr(&"Failed to request online game modes: encountered error %d (%s) when sending request.") % [err, error_string(err)]

func init_online_modes(result: int, response_code: int, _headers: PackedStringArray, body: PackedByteArray) -> void:
	var progress_bar: ProgressBar = %OnlinePlaceholderProgressBar
	progress_bar.visible = false

	var placeholder_desc: Label = %OnlinePlaceholderDescLabel
	if result != HTTPRequest.RESULT_SUCCESS:
		placeholder_desc.text = tr(&"Request for online game modes failed: unexpected result %s") % [WhyIsntThisInGodot.find_builtin_enum_key_name(&"HTTPRequest", &"Result", result)]
		return
	if response_code != HTTPClient.RESPONSE_OK:
		placeholder_desc.text = tr(&"Request for online game modes failed: unexpected response code %s") % [WhyIsntThisInGodot.find_builtin_enum_key_name(&"HTTPClient", &"ResponseCode", response_code)]
		return

	var placeholder_title: Label = %OnlinePlaceholderLabel
	placeholder_title.visible = false
	placeholder_desc.visible = false

	var modes_container: VBoxContainer = %OnlineModesContainer
	var categories: Dictionary[int, HFlowContainer] = {}

	var mode_data_dict: Dictionary = JSON.parse_string(body.get_string_from_utf8())

	var categories_dict: Array = mode_data_dict["c"]
	for category_dict: Dictionary in categories_dict:
		var id: int = category_dict["i"]
		var title_str: String = category_dict["t"]
		var desc: String = category_dict["d"]
		var _color_int: int = category_dict["c"]

		var category_title := Label.new()
		category_title.theme_type_variation = placeholder_title.theme_type_variation
		category_title.text = title_str
		var category_desc := Label.new()
		category_desc.theme_type_variation = placeholder_desc.theme_type_variation
		category_desc.text = desc
		category_desc.autowrap_mode = TextServer.AUTOWRAP_WORD_SMART
		var flow := HFlowContainer.new()

		modes_container.add_child(category_title)
		modes_container.add_child(category_desc)
		modes_container.add_child(flow)
		categories[id] = flow

	var modes_dict: Array = mode_data_dict["m"]
	for mode_dict: Dictionary in modes_dict:
		var slug: String = mode_dict["s"]
		var title_str: String = mode_dict["t"]
		var revisions: int = mode_dict["r"]
		var mode_data_raw: String = mode_dict["m"]
		var first_card_raw: String = "" if mode_dict["fc"] == null else mode_dict["fc"]
		var color_int: int = mode_dict["c"]
		var color := Color8((color_int >> 16) & 255, (color_int >> 8) & 255, color_int & 255)
		var category_id: int = mode_dict["ci"]

		var mode := LegacyParse.card_set([mode_data_raw] if first_card_raw.is_empty() else [mode_data_raw, first_card_raw], Predefined.VANILLA_1_2_1.mode)
		if slug not in ["shapes", "jimmy"]:
			mode.mode.description = mode.mode.description.strip_edges().get_slice("\n\n", 0)

		FileRequester.fetch_container(mode)
		categories[category_id].add_child(GameModeButton.new(slug, 0, revisions, mode.mode, title_str, RankDef.BOSS, color))

	_loading_screen.free_on_completed = true
	# force a check for closing the loading screen
	_loading_screen.progress_current = _loading_screen.progress_current

func _on_close_editor_button_pressed() -> void:
	Router.goto("/game/modes")
