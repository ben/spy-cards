class_name EditorIconPicker
extends Window

const SUGGESTED_BUILTIN_PORTRAIT: Array[IconDef.Icon] = [
	IconDef.Icon.PORTRAIT_ZOMBIANT,
	IconDef.Icon.PORTRAIT_JELLYSHROOM,
	IconDef.Icon.PORTRAIT_SPIDER,
	IconDef.Icon.PORTRAIT_ZASP,
	IconDef.Icon.PORTRAIT_CACTILING,
	IconDef.Icon.PORTRAIT_PSICORP,
	IconDef.Icon.PORTRAIT_THIEF,
	IconDef.Icon.PORTRAIT_BANDIT,
	IconDef.Icon.PORTRAIT_INICHAS,
	IconDef.Icon.PORTRAIT_SEEDLING,
	IconDef.Icon.PORTRAIT_BLANK,
	IconDef.Icon.PORTRAIT_ACORNLING,
	IconDef.Icon.PORTRAIT_WEEVIL,
	IconDef.Icon.PORTRAIT_VENUS_BUD,
	IconDef.Icon.PORTRAIT_NUMBNAIL,
	IconDef.Icon.PORTRAIT_MOTHIVA,
	IconDef.Icon.PORTRAIT_CHOMPER,
	IconDef.Icon.PORTRAIT_KABBU,
	IconDef.Icon.PORTRAIT_ACOLYTE_ARIA,
	IconDef.Icon.PORTRAIT_WASP_TROOPER,
	IconDef.Icon.PORTRAIT_MIDGE,
	IconDef.Icon.PORTRAIT_VENUS_GUARDIAN,
	IconDef.Icon.PORTRAIT_UNDERLING,
	IconDef.Icon.PORTRAIT_MONSIEUR_SCARLET,
	IconDef.Icon.PORTRAIT_GOLDEN_SEEDLING,
	IconDef.Icon.PORTRAIT_MOTHER_CHOMPER,
	IconDef.Icon.PORTRAIT_BURGLAR,
	IconDef.Icon.PORTRAIT_BEE_BOOP,
	IconDef.Icon.PORTRAIT_ABOMIHONEY,
	IconDef.Icon.PORTRAIT_CARMINA,
	IconDef.Icon.PORTRAIT_SECURITY_TURRET,
	IconDef.Icon.PORTRAIT_DENMUKI,
	IconDef.Icon.PORTRAIT_HEAVY_DRONE_B33,
	IconDef.Icon.PORTRAIT_WASP_SCOUT,
	IconDef.Icon.PORTRAIT_ARROW_WORM,
	IconDef.Icon.PORTRAIT_AHONEYNATION,
	IconDef.Icon.PORTRAIT_MENDER,
	IconDef.Icon.PORTRAIT_DUNE_SCORPION,
	IconDef.Icon.PORTRAIT_ASTOTHELES,
	IconDef.Icon.PORTRAIT_KALI,
	IconDef.Icon.PORTRAIT_KRAWLER,
	IconDef.Icon.PORTRAIT_HAUNTED_CLOTH,
	IconDef.Icon.PORTRAIT_WARDEN,
	IconDef.Icon.PORTRAIT_THE_WATCHER,
	IconDef.Icon.PORTRAIT_SEEDLING_KING,
	IconDef.Icon.PORTRAIT_FLOWERLING,
	IconDef.Icon.PORTRAIT_PLUMPLING,
	IconDef.Icon.PORTRAIT_JUMPING_SPIDER,
	IconDef.Icon.PORTRAIT_LEAFBUG_NINJA,
	IconDef.Icon.PORTRAIT_LEAFBUG_ARCHER,
	IconDef.Icon.PORTRAIT_LEAFBUG_CLUBBER,
	IconDef.Icon.PORTRAIT_MADESPHY,
	IconDef.Icon.PORTRAIT_MIMIC_SPIDER,
	IconDef.Icon.PORTRAIT_THE_BEAST,
	IconDef.Icon.PORTRAIT_CHOMPER_BRUTE,
	IconDef.Icon.PORTRAIT_WASP_DRILLER,
	IconDef.Icon.PORTRAIT_GENERAL_ULTIMAX,
	IconDef.Icon.PORTRAIT_WASP_BOMBER,
	IconDef.Icon.PORTRAIT_WILD_CHOMPER,
	IconDef.Icon.PORTRAIT_ZOMBEE,
	IconDef.Icon.PORTRAIT_ZOMBEETLE,
	IconDef.Icon.PORTRAIT_BLOATSHROOM,
	IconDef.Icon.PORTRAIT_PRIMAL_WEEVIL,
	IconDef.Icon.PORTRAIT_CROSS,
	IconDef.Icon.PORTRAIT_POI,
	IconDef.Icon.PORTRAIT_MANTIDFLY,
	IconDef.Icon.PORTRAIT_FALSE_MONARCH,
	IconDef.Icon.PORTRAIT_MOTHFLY,
	IconDef.Icon.PORTRAIT_MOTHFLY_CLUSTER,
	IconDef.Icon.PORTRAIT_BROODMOTHER,
	IconDef.Icon.PORTRAIT_MAKI,
	IconDef.Icon.PORTRAIT_KINA,
	IconDef.Icon.PORTRAIT_WASP_KING,
	IconDef.Icon.PORTRAIT_IRONNAIL,
	IconDef.Icon.PORTRAIT_BELOSTOSS,
	IconDef.Icon.PORTRAIT_WATER_STRIDER,
	IconDef.Icon.PORTRAIT_DIVING_SPIDER,
	IconDef.Icon.PORTRAIT_CENN,
	IconDef.Icon.PORTRAIT_PISCI,
	IconDef.Icon.PORTRAIT_RUFFIAN,
	IconDef.Icon.PORTRAIT_INNKEEPER,
	IconDef.Icon.PORTRAIT_TOD,
	IconDef.Icon.PORTRAIT_LEBY,
	IconDef.Icon.PORTRAIT_YATANTA,
	IconDef.Icon.PORTRAIT_CHUCK,
	IconDef.Icon.PORTRAIT_LAYNA,
	IconDef.Icon.PORTRAIT_MM,
	IconDef.Icon.PORTRAIT_LEVI,
	IconDef.Icon.PORTRAIT_TANJERIN,
	IconDef.Icon.PORTRAIT_PROFESSOR_NEOLITH,
	IconDef.Icon.PORTRAIT_ISAU,
	IconDef.Icon.PORTRAIT_GS_TECHNICIAN,
	IconDef.Icon.PORTRAIT_CRISBEE,
	IconDef.Icon.PORTRAIT_KUT,
	IconDef.Icon.PORTRAIT_FRY,
	IconDef.Icon.PORTRAIT_LEIF,
	IconDef.Icon.PORTRAIT_BOOK,
	IconDef.Icon.PORTRAIT_PROFESSOR_HONEYCOMB,
	IconDef.Icon.PORTRAIT_DOCTOR_HB,
	IconDef.Icon.PORTRAIT_ZASP_DISGUISED,
	IconDef.Icon.PORTRAIT_MADELEINE,
	IconDef.Icon.PORTRAIT_VI,
	IconDef.Icon.PORTRAIT_ALI,
	IconDef.Icon.PORTRAIT_VIVI,
	IconDef.Icon.PORTRAIT_MALBEE,
	IconDef.Icon.PORTRAIT_TAKKUN,
	IconDef.Icon.PORTRAIT_BOMBY,
	IconDef.Icon.PORTRAIT_BUTOMU,
	IconDef.Icon.PORTRAIT_MAYOR,
	IconDef.Icon.PORTRAIT_EETL,
	IconDef.Icon.PORTRAIT_VEN,
	IconDef.Icon.PORTRAIT_GEN_AND_ERI,
	IconDef.Icon.PORTRAIT_ARTIA,
	IconDef.Icon.PORTRAIT_MUN,
	IconDef.Icon.PORTRAIT_ALEX,
	IconDef.Icon.PORTRAIT_MAKI_AND_KINA,
	IconDef.Icon.PORTRAIT_FABRI,
	IconDef.Icon.PORTRAIT_TIDAL_WYRM,
	IconDef.Icon.PORTRAIT_RIZ,
	IconDef.Icon.PORTRAIT_RIZZA,
	IconDef.Icon.PORTRAIT_ZOMMOTH,
	IconDef.Icon.PORTRAIT_PEACOCK_SPIDER,
	IconDef.Icon.PORTRAIT_UNKNOWN,
	IconDef.Icon.PORTRAIT_THE_WIZARD,
	IconDef.Icon.PORTRAIT_ULTIMAX_TANK,
	IconDef.Icon.PORTRAIT_ELOM,
	IconDef.Icon.PORTRAIT_EREMI,
	IconDef.Icon.PORTRAIT_VI_DEED,
	IconDef.Icon.PORTRAIT_LEIF_DEED,
	IconDef.Icon.PORTRAIT_MASK,
	IconDef.Icon.PORTRAIT_TABLET,
	IconDef.Icon.PORTRAIT_KEY_1,
	IconDef.Icon.PORTRAIT_KEY_2,
	IconDef.Icon.PORTRAIT_QUEEN_ELIZANT_II,
	IconDef.Icon.PORTRAIT_BROACH,
	IconDef.Icon.PORTRAIT_WASP_KING_CROWN,
	IconDef.Icon.PORTRAIT_GREEN_BUG_RANGER,
	IconDef.Icon.PORTRAIT_DEAD_LANDER_ALPHA,
	IconDef.Icon.PORTRAIT_DEAD_LANDER_BETA,
	IconDef.Icon.PORTRAIT_DEAD_LANDER_GAMMA,
	IconDef.Icon.PORTRAIT_THE_EVERLASTING_KING,
	IconDef.Icon.PORTRAIT_KABBU_DEED,
	IconDef.Icon.PORTRAIT_YIN,
	IconDef.Icon.PORTRAIT_DEVOURER,
	IconDef.Icon.PORTRAIT_TARAR,
	IconDef.Icon.PORTRAIT_BUMBLE,
	IconDef.Icon.PORTRAIT_ENGIRA,
	IconDef.Icon.PORTRAIT_KABBU_QUEST,
	IconDef.Icon.PORTRAIT_REEVES,
	IconDef.Icon.PORTRAIT_HAUNTED_CLOTH_FIRE,
	IconDef.Icon.PORTRAIT_KRAWLER_FIRE,
	IconDef.Icon.PORTRAIT_WARDEN_FIRE,
	IconDef.Icon.PORTRAIT_REBECCA,
	IconDef.Icon.PORTRAIT_ROY,
	IconDef.Icon.PORTRAIT_STRATOS_AND_DELILAH,
	IconDef.Icon.PORTRAIT_STRATOS,
	IconDef.Icon.PORTRAIT_DELILAH,
	IconDef.Icon.PORTRAIT_DIGIT_1,
	IconDef.Icon.PORTRAIT_DIGIT_2,
	IconDef.Icon.PORTRAIT_DIGIT_3,
	IconDef.Icon.PORTRAIT_DIGIT_4,
	IconDef.Icon.PORTRAIT_DIGIT_5,
	IconDef.Icon.PORTRAIT_DIGIT_6,
	IconDef.Icon.PORTRAIT_DIGIT_7,
]

const SUGGESTED_STAT: Array[IconDef.Icon] = [
	IconDef.Icon.STAT_ATK,
	IconDef.Icon.STAT_DEF,
	IconDef.Icon.STAT_HP,
	IconDef.Icon.STAT_TP,
	IconDef.Icon.STAT_TP_INF,
	IconDef.Icon.STAT_BERRIES,
	IconDef.Icon.STAT_CRYSTAL,
	IconDef.Icon.STAT_EXP,
	IconDef.Icon.STAT_MP,
]

const SUGGESTED_STATUS: Array[IconDef.Icon] = [
	IconDef.Icon.STATUS_FREEZE,
	IconDef.Icon.STATUS_POISON,
	IconDef.Icon.STATUS_NUMB,
	IconDef.Icon.STATUS_SLEEP,
	IconDef.Icon.STATUS_UNSTEADY,
	IconDef.Icon.STATUS_FLIPPED,
	IconDef.Icon.STATUS_DEF_UP,
	IconDef.Icon.STATUS_DEF_DOWN,
	IconDef.Icon.STATUS_ATK_UP,
	IconDef.Icon.STATUS_ATK_DOWN,
	IconDef.Icon.STATUS_TAUNT,
	IconDef.Icon.STATUS_DANGER,
	IconDef.Icon.STATUS_TP_REGEN,
	IconDef.Icon.STATUS_HP_REGEN,
	IconDef.Icon.STATUS_MOTHFLY,
	IconDef.Icon.STATUS_CHARGE,
	IconDef.Icon.STATUS_BURN,
	IconDef.Icon.STATUS_INK,
	IconDef.Icon.STATUS_STICKY,
	IconDef.Icon.STATUS_TALK,
	IconDef.Icon.STATUS_THINK,
	IconDef.Icon.STATUS_JANET,
	IconDef.Icon.ARTIFACT_MASK,
	IconDef.Icon.ARTIFACT_TABLET,
	IconDef.Icon.ARTIFACT_TABLET_CRACK_1,
	IconDef.Icon.ARTIFACT_TABLET_CRACK_2,
	IconDef.Icon.ARTIFACT_KEY_HANDLE,
	IconDef.Icon.ARTIFACT_KEY_TEETH,
	IconDef.Icon.ARTIFACT_KEY_FULL,
	IconDef.Icon.ARTIFACT_ELIZANT,
	IconDef.Icon.ARTIFACT_BROACH,
	IconDef.Icon.ARTIFACT_CROWN,
]

const SUGGESTED_CARD_BACK: Array[IconDef.Icon] = [
	IconDef.Icon.BACK_ATTACKER,
	IconDef.Icon.BACK_EFFECT,
	IconDef.Icon.BACK_MINI_BOSS,
	IconDef.Icon.BACK_BOSS,
	IconDef.Icon.BACK_TOKEN,
]

const SUGGESTED_CARD_FRONT: Array[IconDef.Icon] = [
	IconDef.Icon.FRONT_ATTACKER,
	IconDef.Icon.FRONT_EFFECT,
	IconDef.Icon.FRONT_MINI_BOSS,
	IconDef.Icon.FRONT_BOSS,
	IconDef.Icon.FRONT_TOKEN,
]
