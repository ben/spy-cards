class_name JigsawCommandListEditor
extends Control

@export var commands: JigsawCommandList:
	set(new_value):
		if commands != new_value:
			commands = new_value
			_reset()

var command_nodes: Array[JigsawCommandEditor]

func _reset() -> void:
	pass # TODO
