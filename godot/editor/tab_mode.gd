class_name EditorTabMode
extends EditorTab

var _slug: String
var _container: DataContainer
var _is_local: bool

func _init(slug: String, container: DataContainer, is_local: bool) -> void:
	if is_local:
		title = tr(&"%s (local)", &"editor tab name for game mode") % [slug]
	else:
		title = tr(&"%s (read-only)", &"editor tab name for game mode") % [slug]

	button = &"close_tab"

	_slug = slug
	_container = container
	_is_local = is_local

func get_url_path() -> String:
	if _is_local:
		return "/game/editor/mode/local/" + _slug

	return "/game/editor/mode/" + _slug

func _on_button_pressed() -> void:
	queue_free()
