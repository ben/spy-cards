class_name FileRequester
extends Node

static var _instance := FileRequester.new()
static var total_requests: int
static var _was_queue_empty := true

const MAX_CONCURRENCY := 8
const LRU_SIZE := 256
const _READ_BUILTIN_CACHE := true
const _READ_CACHE := true
const _WRITE_CACHE := true
const _DEBUG_TASKS := false

var _recently_used: Array[FileRequest]
var _pre_queued_requests: Array[FileRequest]
var _queued_requests: Array[FileRequest]
var _live_requests: Array[FileRequest]
var _post_live_requests: Array[FileRequest]
var _requesters: Array[HTTPRequest]

signal request_queue_empty
signal cache_written

func _init() -> void:
	name = &"FileRequester"
	DirAccess.make_dir_recursive_absolute("user://fileid_cache")
	_live_requests.resize(MAX_CONCURRENCY)
	_requesters.resize(MAX_CONCURRENCY)
	for i in MAX_CONCURRENCY:
		_requesters[i] = HTTPRequest.new()
		_requesters[i].request_completed.connect(_on_request_completed.bind(i))
		add_child(_requesters[i], false, Node.INTERNAL_MODE_BACK)

	(Engine.get_main_loop() as SceneTree).root.add_child(self)

func _process(_delta: float) -> void:
	_check_async_tasks(0)

func _check_async_tasks(force_wait: int) -> void:
	for i in range(len(_post_live_requests) - 1, -1, -1):
		var req := _post_live_requests[i]
		if WorkerThreadPool.is_task_completed(req.task) or force_wait > 0:
			force_wait -= 1
			WorkerThreadPool.wait_for_task_completion(req.task)
			if _DEBUG_TASKS:
				print("download postprocess async task %d %s finished" % [req.task, Base32.encode_crockford(req.file_id)])
			_post_live_requests.remove_at(i)
			req.completed = true
			req._on_completed()
			cache_written.emit()

			_add_to_lru(req)

	for i in range(len(_pre_queued_requests) - 1, -1, -1):
		var req := _pre_queued_requests[i]
		if WorkerThreadPool.is_task_completed(req.task) or force_wait > 0:
			WorkerThreadPool.wait_for_task_completion(req.task)
			_pre_queued_requests.remove_at(i)
			force_wait -= 1

			if _DEBUG_TASKS:
				print("cache query async task %d %s finished with %s" % [req.task, Base32.encode_crockford(req.file_id), "HIT" if req.was_cached else "MISS"])
			if req.was_cached:
				req.completed = true
				req._on_completed()
				_add_to_lru(req)
			else:
				_queued_requests.append(req)
				_check_queue()

	if _was_queue_empty and count_pending_requests() == 0:
		_was_queue_empty = true
		request_queue_empty.emit()

static func add_request_for_icon(icon: IconDef) -> void:
	if icon and icon.file_id:
		_find_or_create_file_request(icon.file_id, null).add_icon(icon)

static func add_request_for_effect_parameter_cid_opus(param: JigsawParameterCIDOpus) -> void:
	if param and param.cid:
		_find_or_create_cid_request(param.cid, Vector2(param.loop_start, param.loop_end)).add_audio_legacy(param)

static func add_request_for_effect_parameter_file_id_opus(param: JigsawParameterFileIDOpus) -> void:
	if param and param.file_id:
		_find_or_create_file_request(param.file_id, Vector2(param.loop_start, param.loop_end)).add_audio(param)

static func add_request_for_effect_parameter_file_id_gltf(param: JigsawParameterFileIDGLTF) -> void:
	if param and param.file_id:
		_find_or_create_file_request(param.file_id, param.json_data.get("blender_lights", "")).add_scene(param)

static func _create_cid_request(cid: PackedByteArray, extra_data: Variant) -> FileRequest:
	total_requests += 1

	var file_id_enc := Base32.encode_cid(cid)

	var fr := FileRequest.new()
	fr.file_id = cid
	fr.extra_data = extra_data
	fr.legacy_cid = true
	_instance.add_child(fr)

	if _READ_CACHE:
		fr.task = WorkerThreadPool.add_task(fr._check_for_cached_file.bind(file_id_enc), false, "check file cache for %s" % [Base32.encode_cid(cid)])
		_instance._pre_queued_requests.append(fr)
		return fr

	_instance._queued_requests.append(fr)
	_was_queue_empty = false
	_check_queue()
	return fr

static func _find_or_create_cid_request(cid: PackedByteArray, extra_data: Variant) -> FileRequest:
	for fr in _instance._recently_used:
		if fr.file_id == cid and fr.extra_data == extra_data and fr.legacy_cid:
			return fr
	for fr in _instance._post_live_requests:
		if fr.file_id == cid and fr.extra_data == extra_data and fr.legacy_cid:
			return fr
	for fr in _instance._live_requests:
		if fr and fr.file_id == cid and fr.extra_data == extra_data and fr.legacy_cid:
			return fr
	for fr in _instance._queued_requests:
		if fr.file_id == cid and fr.extra_data == extra_data and fr.legacy_cid:
			return fr
	for fr in _instance._pre_queued_requests:
		if fr.file_id == cid and fr.extra_data == extra_data and fr.legacy_cid:
			return fr

	return _create_cid_request(cid, extra_data)

static func _create_file_request(file_id: PackedByteArray, extra_data: Variant) -> FileRequest:
	total_requests += 1

	var file_id_enc := Base32.encode_crockford(file_id)

	var fr := FileRequest.new()
	fr.file_id = file_id
	fr.extra_data = extra_data
	_instance.add_child(fr)

	if _READ_BUILTIN_CACHE or _READ_CACHE:
		if _DEBUG_TASKS:
			print("starting async cache query for %s" % [Base32.encode_crockford(fr.file_id)])
		fr.task = WorkerThreadPool.add_task(fr._check_for_cached_file.bind(file_id_enc), false, "check file cache for %s" % [file_id_enc])
		_instance._pre_queued_requests.append(fr)
		return fr

	_instance._queued_requests.append(fr)
	_was_queue_empty = false
	_check_queue()
	return fr

static func _find_or_create_file_request(file_id: PackedByteArray, extra_data: Variant) -> FileRequest:
	for fr in _instance._recently_used:
		if fr.file_id == file_id and fr.extra_data == extra_data and not fr.legacy_cid:
			return fr
	for fr in _instance._post_live_requests:
		if fr.file_id == file_id and fr.extra_data == extra_data and not fr.legacy_cid:
			return fr
	for fr in _instance._live_requests:
		if fr and fr.file_id == file_id and fr.extra_data == extra_data and not fr.legacy_cid:
			return fr
	for fr in _instance._queued_requests:
		if fr.file_id == file_id and fr.extra_data == extra_data and not fr.legacy_cid:
			return fr
	for fr in _instance._pre_queued_requests:
		if fr.file_id == file_id and fr.extra_data == extra_data and not fr.legacy_cid:
			return fr

	return _create_file_request(file_id, extra_data)

static func _check_queue() -> void:
	for i in MAX_CONCURRENCY:
		if _instance._live_requests[i]:
			continue

		var retry := true
		while retry:
			retry = false

			if _instance._queued_requests.is_empty():
				return

			var req := _instance._queued_requests[0]
			_instance._queued_requests.remove_at(0)

			_instance._live_requests[i] = req
			var err := _instance._requesters[i].request(APIEndpoints.legacy_cid(req.file_id) if req.legacy_cid else APIEndpoints.file_id(req.file_id))
			if err != OK:
				push_error("failed to send file request (fileid %s; errcode %d; %s)" % [Base32.encode_cid(req.file_id) if req.legacy_cid else Base32.encode_crockford(req.file_id), err, error_string(err)])
				_instance._live_requests[i] = null
				retry = true

static func count_pending_requests() -> int:
	var total := len(_instance._pre_queued_requests) + len(_instance._queued_requests) + len(_instance._post_live_requests)
	for live in _instance._live_requests:
		if live:
			total += 1
	return total

static func _on_request_completed(result: int, response_code: int, headers: PackedStringArray, body: PackedByteArray, parallel_request_id: int) -> void:
	var req := _instance._live_requests[parallel_request_id]
	_instance._live_requests[parallel_request_id] = null
	_instance._post_live_requests.append(req)

	if _DEBUG_TASKS:
		print("starting async download postprocess for %s" % [Base32.encode_crockford(req.file_id)])
	var file_id_enc := Base32.encode_cid(req.file_id) if req.legacy_cid else Base32.encode_crockford(req.file_id)
	req.task = WorkerThreadPool.add_task(req._on_file_downloaded.bind(file_id_enc, result, response_code, headers, body), true, "decode file id %s" % [file_id_enc])

	_check_queue()

static func _add_to_lru(fr: FileRequest) -> void:
	if LRU_SIZE == 0:
		fr.queue_free()
		return

	if len(_instance._recently_used) < LRU_SIZE:
		_instance._recently_used.append(fr)
	else:
		assert(len(_instance._recently_used) == LRU_SIZE, "LRU overfull")

		var old_index := _instance._recently_used.find(fr)
		if old_index != -1:
			_instance._recently_used.remove_at(old_index)
			_instance._recently_used.append(fr)
		else:
			_instance._recently_used[0].queue_free()
			_instance._recently_used.pop_front()
			_instance._recently_used.append(fr)

static func _cancel(fr: FileRequest) -> void:
	assert(fr in _instance._pre_queued_requests or fr in _instance._queued_requests or fr in _instance._live_requests or fr in _instance._post_live_requests, "non-queued/live request cancelled")

	var i := _instance._pre_queued_requests.find(fr)
	if i != -1:
		WorkerThreadPool.wait_for_task_completion(fr.task)
		_instance._pre_queued_requests.remove_at(i)
		fr.queue_free()
		return

	i = _instance._queued_requests.find(fr)
	if i != -1:
		_instance._queued_requests.remove_at(i)
		fr.queue_free()
		return

	i = _instance._live_requests.find(fr)
	if i != -1:
		_instance._requesters[i].cancel_request()
		_instance._live_requests[i] = null
		fr.queue_free()
		_check_queue()
		return

	i = _instance._post_live_requests.find(fr)
	if i != -1:
		WorkerThreadPool.wait_for_task_completion(fr.task)
		if _DEBUG_TASKS:
			print("cancelling request for %s" % [Base32.encode_crockford(fr.file_id)])
		_instance._post_live_requests.remove_at(i)
		fr.queue_free()
		return

static func fetch_container(container: DataContainer) -> void:
	if not container:
		return

	# TODO: embedded files
	if container.container_type == DataContainer.GAME_MODE_SUMMARY:
		fetch_game_mode_summary(container.mode_summary)
	else:
		fetch_game_mode(container.mode)

static func fetch_game_mode(mode: GameMode) -> void:
	if not mode:
		return
	fetch_game_mode_summary(mode)
	for card in mode.card_defs:
		fetch_card(card)
	for modifier in mode.custom_modifiers:
		fetch_modifier(modifier)
	for effect in mode.custom_effects:
		fetch_effect(effect)

	# TODO: game mode fields

static func fetch_game_mode_summary(summary: GameModeSummary) -> void:
	if not summary:
		return

	for icon in summary.custom_icons:
		fetch_icon(icon)

static func fetch_icon(icon: IconDef) -> void:
	if icon:
		add_request_for_icon(icon)

static func fetch_card(card: CardDef) -> void:
	if not card:
		return

	for effect in card.effects:
		fetch_effect_instance(effect)

static func fetch_modifier(modifier: ModifierDef) -> void:
	if not modifier:
		return

	# TODO: modifier effects

static func fetch_effect(effect: EffectDef) -> void:
	if not effect:
		return

	# TODO: nodes

	for param in effect.default_parameters:
		fetch_param(param)

static func fetch_effect_instance(instance: EffectInstance) -> void:
	if not instance:
		return

	for param in instance.params:
		fetch_param(param)

static func fetch_param(param: JigsawParameter) -> void:
	if not param:
		return

	match param:
		JigsawParameterEffectInstance:
			fetch_effect_instance((param as JigsawParameterEffectInstance).instance)
		JigsawParameterCIDOpus:
			add_request_for_effect_parameter_cid_opus(param as JigsawParameterCIDOpus)
		JigsawParameterFileIDOpus:
			add_request_for_effect_parameter_file_id_opus(param as JigsawParameterFileIDOpus)
		JigsawParameterFileIDGLTF:
			add_request_for_effect_parameter_file_id_gltf(param as JigsawParameterFileIDGLTF)

static func fetch_predefined() -> void:
	assert(_instance._pre_queued_requests.is_empty(), "should not have queued any requests prior to fetch_predefined")
	assert(_instance._queued_requests.is_empty(), "should not have queued any requests prior to fetch_predefined")

	for mode: DataContainer in Predefined.BUILT_IN_MODES.values():
		fetch_container(mode)

	for effect in Predefined.EFFECT:
		if effect:
			fetch_effect(effect)

	for modifier in Predefined.MODIFIER:
		if modifier:
			fetch_modifier(modifier)

	for stage in Predefined.STAGES:
		add_request_for_effect_parameter_file_id_gltf(stage)

	for sound in Predefined.SOUNDS:
		add_request_for_effect_parameter_file_id_opus(sound)

	for music in Predefined.MUSIC:
		add_request_for_effect_parameter_file_id_opus(music)

	for req in _instance._pre_queued_requests:
		WorkerThreadPool.wait_for_task_completion(req.task)
		if _DEBUG_TASKS:
			print("finished cache query for built-in file %s with %s" % [Base32.encode_crockford(req.file_id), "HIT" if req.was_cached else "MISS"])
		assert(req.was_cached, "built-in file %s not cached" % [Base32.encode_crockford(req.file_id)])
		req.completed = true
		req._on_completed()
		_add_to_lru(req)

	_instance._pre_queued_requests.clear()
