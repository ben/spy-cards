class_name APIEndpoints

const ORIGIN := "https://spy-cards.lubar.me"
const GAME_MODES := "https://spy-cards.lubar.me/spy-cards/custom/api/modes"
const FEATURED_COMMUNITY_PORTRAITS := "https://spy-cards.lubar.me/spy-cards/user-img/community.json"

const LOG_IN := "https://spy-cards.lubar.me/spy-cards/log-in"
const LOG_OUT := "https://spy-cards.lubar.me/spy-cards/log-out"
const STATUS := "https://spy-cards.lubar.me/spy-cards/godot-api/status"

const MATCHMAKING_CREATE_SESSION := "https://spy-cards.lubar.me/spy-cards/matchmaking/create-session"
const MATCHMAKING_JOIN := "https://spy-cards.lubar.me/spy-cards/matchmaking/join"
const MATCHMAKING_POLL := "https://spy-cards.lubar.me/spy-cards/matchmaking/poll"
const MATCHMAKING_SEND := "https://spy-cards.lubar.me/spy-cards/matchmaking/send"

static func file_id(id: PackedByteArray) -> String:
	return "https://spy-cards.lubar.me/spy-cards/godot-fileapi/" + Base32.encode_crockford(id)

static func legacy_cid(id: PackedByteArray) -> String:
	return "https://spy-cards.lubar.me/ipfs/" + Base32.encode_cid(id)

static func random_match_recording(mode: String = "", not_ids: PackedStringArray = []) -> String:
	var url := "https://spy-cards.lubar.me/spy-cards/recording/random"

	var first := true
	if mode:
		url += "?mode="
		first = false

		if mode != "vanilla":
			url += mode.uri_encode()

	for id in not_ids:
		if first:
			url += "?not="
			first = false
		else:
			url += "&not="

		url += id.uri_encode()

	return url

static func get_match_recording(id: String) -> String:
	return "https://spy-cards.lubar.me/spy-cards/recording/get/" + id.uri_encode()
