class_name FileRequest
extends Node

const ERROR_TEXTURE := preload("res://custom_textures/error.png")
const ERROR_AUDIO := preload("res://custom_textures/error.wav")
const ERROR_SCENE := preload("res://custom_textures/error.res")

var file_id: PackedByteArray
## for textures, null.
## for audio, Vector2(loop_start, loop_end).
## for scenes, json_data["blender_lights"] as String.
var extra_data: Variant
var legacy_cid: bool

var content: PackedByteArray
var completed: bool
var was_cached: bool
var task: int

var icons: Array[IconDef]
var _image: Image

var audio_legacy: Array[JigsawParameterCIDOpus]
var audio: Array[JigsawParameterFileIDOpus]
var scene: Array[JigsawParameterFileIDGLTF]
var _audio: AudioStreamWAV
var _scene: GLTFState

static var _gltf_document := GLTFDocument.new()

func _check_for_cached_file(file_id_enc: String) -> void:
	if legacy_cid:
		if FileRequester._READ_CACHE:
			var cache_path := "user://legacy_cid_cache/" + file_id_enc
			if FileAccess.file_exists(cache_path):
				content = FileAccess.get_file_as_bytes(cache_path)
				was_cached = true
				_on_content_ready()
	else:
		if FileRequester._READ_BUILTIN_CACHE:
			var builtin_cache_path := "res://fileid_builtin/" + file_id_enc
			if FileAccess.file_exists(builtin_cache_path):
				var cached_contents := FileAccess.get_file_as_bytes(builtin_cache_path)
				var checksummer := HashingContext.new()
				checksummer.start(HashingContext.HASH_SHA256)
				checksummer.update(cached_contents)
				var checksum := checksummer.finish().slice(0, 8)
				assert(file_id.slice(0, 8) == checksum, "bad contents for %s" % [builtin_cache_path])
				content = cached_contents
				was_cached = true
				_on_content_ready()
				return

		if FileRequester._READ_CACHE:
			var cache_path := "user://fileid_cache/" + file_id_enc
			if FileAccess.file_exists(cache_path):
				var cached_contents := FileAccess.get_file_as_bytes(cache_path)
				var checksummer := HashingContext.new()
				checksummer.start(HashingContext.HASH_SHA256)
				checksummer.update(cached_contents)
				var checksum := checksummer.finish().slice(0, 8)
				if file_id.slice(0, 8) != checksum:
					push_warning("bad contents for cache file %s" % [cache_path])
					DirAccess.remove_absolute(cache_path)
				else:
					content = cached_contents
					was_cached = true
					_on_content_ready()

func _on_file_downloaded(file_id_enc: String, result: int, response_code: int, _headers: PackedStringArray, body: PackedByteArray) -> void:
	if result != HTTPRequest.RESULT_SUCCESS:
		push_warning("request for fileid %s failed: result %d" % [file_id_enc, result])
	elif response_code != HTTPClient.RESPONSE_OK:
		push_warning("request for fileid %s failed: status %d" % [file_id_enc, response_code])
	elif legacy_cid:
		# it's too complicated to generate IPFS hashes here just for some legacy audio; just store it without checking
		if FileRequester._WRITE_CACHE:
			DirAccess.make_dir_recursive_absolute("user://legacy_cid_cache")
			var cache_path := "user://legacy_cid_cache/" + file_id_enc
			var cache_file := FileAccess.open(cache_path, FileAccess.WRITE)
			cache_file.store_buffer(body)
		content = body
	else:
		var checksummer := HashingContext.new()
		checksummer.start(HashingContext.HASH_SHA256)
		checksummer.update(body)
		var checksum := checksummer.finish().slice(0, 8)
		if file_id.slice(0, 8) == checksum:
			if FileRequester._WRITE_CACHE:
				var cache_path := "user://fileid_cache/" + file_id_enc
				var cache_file := FileAccess.open(cache_path, FileAccess.WRITE)
				cache_file.store_buffer(body)
		else:
			push_warning("request for fileid %s returned bad checksum" % [file_id_enc])
		content = body

	_on_content_ready()

func _on_content_ready() -> void:
	var file_id_type := 1
	if not legacy_cid:
		var fh := FormatHelper.read("file id", file_id)
		fh.read_bytes(8) # checksum
		fh.read_uvarint() # opaque ID
		if fh.is_eof():
			file_id_type = 0
		else:
			file_id_type = fh.read_uvarint()
			assert(file_id_type != 0)
		assert(fh.is_valid_eof())

	assert(not _image)
	assert(not _audio)
	assert(not _scene)
	match file_id_type:
		0:
			_image = Image.new()
			var err := _image.load_png_from_buffer(content)
			if err != OK:
				push_error("failed to load png texture (fileid %s; errcode %d; %s)" % [Base32.encode_crockford(file_id), err, error_string(err)])
				_image.copy_from(ERROR_TEXTURE)
		1:
			_audio = AudioStreamWAV.new()
			var err := Opus.decode(content, _audio)
			assert(err == OK, "failed to parse audio")
			if err != OK:
				push_warning("failed to parse audio %s: %d %s" % [Base32.encode_crockford(file_id), err, error_string(err)])
				_audio = ERROR_AUDIO
			else:
				var loop_style: Vector2 = extra_data
				if loop_style != Vector2.ZERO:
					_audio.loop_mode = AudioStreamWAV.LOOP_FORWARD
					_audio.loop_begin = roundi(loop_style.x * _audio.mix_rate)
					_audio.loop_end = roundi(loop_style.y * _audio.mix_rate)

					var total_samples := len(_audio.data)
					if _audio.format == AudioStreamWAV.FORMAT_16_BITS:
						total_samples /= 2
					if _audio.stereo:
						total_samples /= 2

					if _audio.loop_begin >= total_samples:
						_audio.loop_begin = 0
					if _audio.loop_end >= total_samples:
						_audio.loop_end = total_samples - 1
		2:
			# when we have a working multithreaded renderer in web exports, we can enable this
			if false:
				_scene = GLTFState.new()
				var err := _gltf_document.append_from_buffer(content, "", _scene)
				assert(err == OK, "failed to parse scene")
				if err != OK:
					push_warning("failed to parse scene %s: %d %s" % [Base32.encode_crockford(file_id), err, error_string(err)])
					_scene = ERROR_SCENE
				else:
					var blender_lights: String = extra_data
					FileRequest.clean_scene(_scene, blender_lights)

func _on_completed() -> void:
	for icon in icons:
		_handle_icon(icon)
	icons.clear()

	for param in audio_legacy:
		_handle_audio_legacy(param)
	audio_legacy.clear()

	for param in audio:
		_handle_audio(param)
	audio.clear()

	for param in scene:
		_handle_scene(param)
	scene.clear()

func add_icon(icon: IconDef) -> void:
	if completed:
		_handle_icon(icon)
	elif icon not in icons:
		icons.append(icon)

func remove_icon(icon: IconDef) -> void:
	var i := icons.find(icon)
	if i != -1:
		icons.remove_at(i)
		_check_cancel()

func add_audio_legacy(param: JigsawParameterCIDOpus) -> void:
	if completed:
		_handle_audio_legacy(param)
	elif param not in audio_legacy:
		audio_legacy.append(param)

func add_audio(param: JigsawParameterFileIDOpus) -> void:
	if completed:
		_handle_audio(param)
	elif param not in audio:
		audio.append(param)

func add_scene(param: JigsawParameterFileIDGLTF) -> void:
	if completed:
		_handle_scene(param)
	elif param not in scene:
		scene.append(param)

func remove_audio_legacy(param: JigsawParameterCIDOpus) -> void:
	var i := audio_legacy.find(param)
	if i != -1:
		audio_legacy.remove_at(i)
		_check_cancel()

func remove_audio(param: JigsawParameterFileIDOpus) -> void:
	var i := audio.find(param)
	if i != -1:
		audio.remove_at(i)
		_check_cancel()

func remove_scene(param: JigsawParameterFileIDGLTF) -> void:
	var i := scene.find(param)
	if i != -1:
		scene.remove_at(i)
		_check_cancel()

func _handle_icon(icon: IconDef) -> void:
	assert(completed, "request was not completed when _handle_icon was called")
	assert(icon.file_id == file_id, "icon file id mismatch")
	assert(not icon.image, "icon already loaded")
	assert(not icon.texture.get_image(), "icon already loaded")

	if not _image:
		_image = Image.new()
		var err := _image.load_png_from_buffer(content)
		if err != OK:
			push_error("failed to load png texture (fileid %s; errcode %d; %s)" % [Base32.encode_crockford(file_id), err, error_string(err)])
			_image.copy_from(ERROR_TEXTURE)

	icon.texture.set_image(_image)
	icon.emit_changed()

func _handle_audio_legacy(param: JigsawParameterCIDOpus) -> void:
	assert(completed, "request was not completed when _handle_audio_legacy was called")
	assert(param.cid == file_id, "param file id mismatch")
	assert(not param.audio, "audio already loaded")

	if not _audio:
		_audio = AudioStreamWAV.new()
		var err := Opus.decode(content, _audio)
		assert(err == OK, "failed to parse audio")
		if err != OK:
			push_warning("failed to parse audio %s: %d %s" % [Base32.encode_crockford(file_id), err, error_string(err)])
			_audio = ERROR_AUDIO
		else:
			var loop_style: Vector2 = extra_data
			if loop_style != Vector2.ZERO:
				_audio.loop_mode = AudioStreamWAV.LOOP_FORWARD
				_audio.loop_begin = roundi(loop_style.x * _audio.mix_rate)
				_audio.loop_end = roundi(loop_style.y * _audio.mix_rate)

	param.audio = _audio
	param.emit_changed()

func _handle_audio(param: JigsawParameterFileIDOpus) -> void:
	assert(completed, "request was not completed when _handle_audio_legacy was called")
	assert(param.file_id == file_id, "param file id mismatch")
	assert(not param.audio, "audio already loaded")

	if not _audio:
		_audio = AudioStreamWAV.new()
		var err := Opus.decode(content, _audio)
		assert(err == OK, "failed to parse audio")
		if err != OK:
			push_warning("failed to parse audio %s: %d %s" % [Base32.encode_crockford(file_id), err, error_string(err)])
			_audio = ERROR_AUDIO
		else:
			var loop_style: Vector2 = extra_data
			if loop_style != Vector2.ZERO:
				_audio.loop_mode = AudioStreamWAV.LOOP_FORWARD
				_audio.loop_begin = roundi(loop_style.x * _audio.mix_rate)
				_audio.loop_end = roundi(loop_style.y * _audio.mix_rate)

	param.audio = _audio
	param.emit_changed()

func _handle_scene(param: JigsawParameterFileIDGLTF) -> void:
	assert(completed, "request was not completed when _handle_scene was called")
	assert(param.file_id == file_id, "param file id mismatch")
	assert(not param.scene, "scene already loaded")

	if not _scene:
		_scene = GLTFState.new()
		var err := _gltf_document.append_from_buffer(content, "", _scene)
		assert(err == OK, "failed to parse scene")
		if err != OK:
			push_warning("failed to parse scene %s: %d %s" % [Base32.encode_crockford(file_id), err, error_string(err)])
			_scene = ERROR_SCENE
		else:
			var blender_lights: String = extra_data
			FileRequest.clean_scene(_scene, blender_lights)

	param.scene = _scene
	param.emit_changed()

func _check_cancel() -> void:
	if completed:
		# completed requests are freed by the LRU cache handler, not by the requests themselves
		return

	if not icons.is_empty():
		return

	FileRequester._cancel(self)

static func _check_suffix(node_name: String, suffix: String) -> bool:
	return node_name.containsn("-" + suffix) or node_name.containsn("_" + suffix) or node_name.containsn("$" + suffix)

const BLENDER_WATTS_TO_DIRECTIONAL := 1.0
const BLENDER_WATTS_TO_POINT := 0.01

static func clean_scene(state: GLTFState, blender_lights: String) -> void:
	# animations with names starting or ending with "loop" or "cycle" are looped (Godot handles this rule)

	for mat in state.get_materials():
		if not (mat is StandardMaterial3D):
			continue
		var mat3d: StandardMaterial3D = mat

		# use anisotropic filtering where needed
		if mat3d.resource_name == "3DMain" or _check_suffix(mat3d.resource_name, "aniso"):
			mat3d.texture_filter = BaseMaterial3D.TEXTURE_FILTER_LINEAR_WITH_MIPMAPS_ANISOTROPIC

		# use alpha test instead of blending for sprites
		if mat3d.resource_name == "MainPlane" or _check_suffix(mat3d.resource_name, "cutout"):
			mat3d.transparency = BaseMaterial3D.TRANSPARENCY_ALPHA_SCISSOR
			mat3d.alpha_antialiasing_mode = BaseMaterial3D.ALPHA_ANTIALIASING_ALPHA_TO_COVERAGE

		# toon shading
		if _check_suffix(mat3d.resource_name, "toon"):
			mat3d.specular_mode = BaseMaterial3D.SPECULAR_TOON

		# we want mipmaps, but the importer doesn't make them
		if mat3d.albedo_texture:
			var img := mat3d.albedo_texture.get_image()
			if not img.has_mipmaps():
				img.generate_mipmaps()
				mat3d.albedo_texture = ImageTexture.create_from_image(img)

	# Importing lights from Blender is a mess.
	# https://github.com/godotengine/godot/issues/73624
	match blender_lights:
		"standard":
			for light in state.get_lights():
				if light.light_type == "directional":
					light.intensity *= BLENDER_WATTS_TO_DIRECTIONAL * 1.0 / 683.0
				else:
					light.intensity *= BLENDER_WATTS_TO_POINT * 4.0 * PI / 683.0
					light.range = 50.0
		"unitless":
			for light in state.get_lights():
				if light.light_type == "directional":
					light.intensity *= BLENDER_WATTS_TO_DIRECTIONAL
				else:
					light.intensity *= BLENDER_WATTS_TO_POINT * 4.0 * PI
					light.range = 50.0
		"raw":
			for light in state.get_lights():
				if light.light_type == "directional":
					light.intensity *= BLENDER_WATTS_TO_DIRECTIONAL
				else:
					light.intensity *= BLENDER_WATTS_TO_POINT
					light.range = 50.0
