class_name SimpleHTTPRequest
extends HTTPRequest

class Response extends RefCounted:
	var result: HTTPRequest.Result
	var response_code: HTTPClient.ResponseCode = HTTPClient.RESPONSE_OK
	var headers: PackedStringArray
	var body: PackedByteArray

signal response(resp: Response)

func _init() -> void:
	request_completed.connect(_on_request_completed)

func _on_request_completed(result: int, response_code: int, headers: PackedStringArray, body: PackedByteArray) -> void:
	var resp := Response.new()
	resp.result = result as HTTPRequest.Result
	resp.response_code = response_code as HTTPClient.ResponseCode
	resp.headers = headers
	resp.body = body
	response.emit(resp)
