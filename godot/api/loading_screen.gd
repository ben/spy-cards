class_name LoadingScreen
extends Control

const SCENE := preload("res://api/loading_screen.tscn")

const TIPS: PackedStringArray = [
	"It would be cool if you could play Spy Cards Online with other people.", # unknown
	"There are more than eleven cards in existence.", # from Ben Lubar
	"People can play games using cards.", # from Kit Redgrave
	"Most cards have some sort of border around the edge of the card.", # from Goaty Goats
	"Oh wow. Oh wow. Oh wow.", # from Steve Jobs
	"While cards are typically made from physical materials, recent advancements in technology have enabled cards to exist in a completely digital format.", # from Goaty Goats
	"If you rip a card in half, you can't play with it anymore.", # from eviloatmeal
	"Tanjerin was not harmed in the making of this card game. Probably.", # from Steffo
]

signal done

static func create(title: String) -> LoadingScreen:
	var screen: LoadingScreen = SCENE.instantiate()
	screen.title_text = title
	screen.tip_text = TIPS[randi_range(0, len(TIPS) - 1)]
	return screen

var starting_request_count: int = -1
var free_on_completed: bool

var title_text: String:
	get:
		var loading_label: Label = %LoadingLabel
		return loading_label.text
	set(new_value):
		var loading_label: Label = %LoadingLabel
		loading_label.text = new_value

var progress_text: String:
	set(new_value):
		if progress_text == new_value:
			return
		progress_text = new_value
		_update_progress_text()

var tip_text: String:
	get:
		var tip_label: Label = %TipLabel
		return tip_label.text
	set(new_value):
		var tip_label: Label = %TipLabel
		tip_label.text = new_value

var progress_current: int:
	get:
		var progress_bar: ProgressBar = %ProgressBar
		return int(progress_bar.value)
	set(new_value):
		if free_on_completed and new_value == progress_total:
			done.emit()
			queue_free()
		if progress_current == new_value:
			return
		var progress_bar: ProgressBar = %ProgressBar
		progress_bar.value = new_value
		_update_progress_text()

var progress_total: int:
	get:
		var progress_bar: ProgressBar = %ProgressBar
		if progress_bar.indeterminate:
			return 0
		return int(progress_bar.max_value)
	set(new_value):
		if progress_total == new_value:
			return
		var progress_bar: ProgressBar = %ProgressBar
		if new_value <= 0:
			progress_bar.indeterminate = true
		else:
			if progress_bar.indeterminate:
				progress_bar.value = 0
			progress_bar.indeterminate = false
			progress_bar.max_value = new_value
		_update_progress_text()

func _update_progress_text() -> void:
	var progress_label: Label = %ProgressLabel
	if progress_total == 0:
		progress_label.text = progress_text
	else:
		var remaining := progress_total - progress_current
		progress_label.text = tr_n(&"%s (%d remaining)", &"%s (%d remaining)", remaining) % [progress_text, remaining]

func _process(_delta: float) -> void:
	FileRequester._instance._check_async_tasks(1)
	if starting_request_count != -1:
		progress_total = FileRequester.total_requests - starting_request_count
		progress_current = progress_total - FileRequester.count_pending_requests()
