class_name PlayerPreferences

static func _static_init() -> void:
	if OS.has_feature("web"):
		load_web_settings()

static func load_web_settings() -> void:
	var settings := {}
	var window := JavaScriptBridge.get_interface("window")
	var settings_string: Variant = JavaScriptBridge.get_interface("localStorage").get("spy-cards-settings-v0")
	if settings_string is String:
		var json := JSON.new()
		if json.parse(settings_string as String) == OK and json.data is Dictionary:
			settings = json.data
	alternate_colors = settings.get("alternateColors", (window.call("matchMedia", "(prefers-contrast: more)") as JavaScriptObject).get("matches") as bool)
	prefers_reduced_motion = settings.get("prefersReducedMotion", (window.call("matchMedia", "(prefers-reduced-motion)") as JavaScriptObject).get("matches") as bool)
	disable_3d = settings.get("disable3D", disable_3d)
	shadows = settings.get("shadows", shadows)
	card_face_resolution = settings.get("cardFaceResolution", card_face_resolution)
	theme = settings.get("theme", "light" if (window.call("matchMedia", "(prefers-color-scheme: light)") as JavaScriptObject).get("matches") else "dark")
	preferences_label = settings.get("preferencesLabel", preferences_label)
	hide_lobby_code = settings.get("hideLobbyCode", hide_lobby_code)
	display_name = settings.get("lastCardName", display_name)
	display_name_arcade = settings.get("lastTermacadeName", display_name_arcade)

static func save() -> void:
	pass # TODO

static var alternate_colors := false
static var prefers_reduced_motion := false
static var disable_3d := false
static var shadows := true
static var card_face_resolution := 2.0
static var theme := "dark"
static var preferences_label := 0
static var hide_lobby_code := false
static var display_name := ""
static var display_name_arcade := ""
