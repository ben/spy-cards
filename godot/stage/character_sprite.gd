class_name CharacterSprite
extends MeshInstance3D

const SHADER := preload("res://stage/character_sprite.gdshader")
var _shader: ShaderMaterial

@export var mode: GameMode:
	set(new_value):
		if mode != new_value:
			mode = new_value
			_init_mesh()
@export var def: CharacterDef:
	set(new_value):
		if def != new_value:
			def = new_value
			_init_mesh()
@export var player_number: int:
	set(new_value):
		if player_number != new_value:
			player_number = new_value
			_init_mesh()
@export var flip: bool:
	set(new_value):
		if flip != new_value:
			flip = new_value
			_init_mesh()
@export var debug_center: bool:
	set(new_value):
		if debug_center != new_value:
			debug_center = new_value
			_init_mesh()

@export var angry_frames: int:
	set(new_value):
		if angry_frames != new_value:
			angry_frames = new_value
			if _shader:
				_shader.set_shader_parameter(&"angry", angry_frames != 0)

func _ready() -> void:
	_init_mesh()

func _init_mesh() -> void:
	if not mode or not def:
		return

	if not _shader:
		_shader = ShaderMaterial.new()
	_shader.shader = SHADER

	var textures: Array[Texture2D] = [
		mode.get_icon_texture(def.idle0),
		mode.get_icon_texture(def.idle1),
		mode.get_icon_texture(def.angry),
	]
	var idle0_size := textures[0].get_size()
	var idle0_rect := Rect2(Vector2(-def.idle0_center_px, 0.0), idle0_size)
	var idle1_size := textures[1].get_size()
	var idle1_rect := Rect2(Vector2(-def.idle1_center_px, 0.0), idle1_size)
	var angry_size := textures[2].get_size()
	var angry_rect := Rect2(Vector2(-def.angry_center_px, 0.0), angry_size)
	var full_rect := idle0_rect.merge(idle1_rect).merge(angry_rect)

	var quad := QuadMesh.new()
	quad.size = full_rect.size * def.pixel_size
	quad.center_offset = Vector3(-full_rect.get_center().x * def.pixel_size, full_rect.size.y * def.pixel_size / 2.0 - def.pixel_size, 0.0)
	quad.material = _shader
	mesh = quad

	var idle0_scale := full_rect.size / idle0_rect.size
	var idle1_scale := full_rect.size / idle1_rect.size
	var angry_scale := full_rect.size / angry_rect.size
	var transforms: PackedVector4Array = [
		Vector4(idle0_scale.x, idle0_scale.y, (full_rect.position.x - idle0_rect.position.x) / full_rect.size.x, 1.0 - idle0_scale.y),
		Vector4(idle1_scale.x, idle1_scale.y, (full_rect.position.x - idle1_rect.position.x) / full_rect.size.x, 1.0 - idle1_scale.y),
		Vector4(angry_scale.x, angry_scale.y, (full_rect.position.x - angry_rect.position.x) / full_rect.size.x, 1.0 - angry_scale.y),
	]

	if flip:
		quad.center_offset.x = -quad.center_offset.x
		for i in len(transforms):
			transforms[i].x = -transforms[i].x
			transforms[i].z = -transforms[i].x + transforms[i].z

	_shader.set_shader_parameter(&"sprite", textures)
	_shader.set_shader_parameter(&"transform", transforms)
	_shader.set_shader_parameter(&"player_number", player_number)
	_shader.set_shader_parameter(&"angry", angry_frames != 0)
	_shader.set_shader_parameter(&"reduce_motion", PlayerPreferences.prefers_reduced_motion)
	if not debug_center:
		_shader.set_shader_parameter(&"debug_center", -1.0)
	elif flip:
		_shader.set_shader_parameter(&"debug_center", 1.0 + full_rect.position.x / full_rect.size.x)
	else:
		_shader.set_shader_parameter(&"debug_center", -full_rect.position.x / full_rect.size.x)

func _physics_process(_delta: float) -> void:
	if not mode or not def:
		return

	if angry_frames > 0:
		angry_frames -= 1
