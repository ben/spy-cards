class_name AudienceMesh
extends QuadMesh

const SHADER := preload("res://stage/audience_mesh.gdshader")

@export var mode: GameMode:
	set(new_value):
		if mode != new_value:
			mode = new_value
			emit_changed()
@export var pixel_size: float:
	set(new_value):
		if pixel_size != new_value:
			pixel_size = new_value
			emit_changed()
@export var normal_icon: IconDef.Icon:
	set(new_value):
		if normal_icon != new_value:
			normal_icon = new_value
			emit_changed()
@export var cheer_icon: IconDef.Icon:
	set(new_value):
		if cheer_icon != new_value:
			cheer_icon = new_value
			emit_changed()

@export_storage var _shader: ShaderMaterial
var _normal_texture: Texture2D:
	set(new_value):
		if _normal_texture == new_value:
			return
		if _normal_texture and _normal_texture.changed.is_connected(_on_texture_changed):
			_normal_texture.changed.disconnect(_on_texture_changed)
		_normal_texture = new_value
		if _normal_texture:
			_normal_texture.changed.connect(_on_texture_changed)
		_on_texture_changed()
var _cheer_texture: Texture2D:
	set(new_value):
		if _cheer_texture == new_value:
			return
		if _cheer_texture and _cheer_texture != _normal_texture and _cheer_texture.changed.is_connected(_on_texture_changed):
			_cheer_texture.changed.disconnect(_on_texture_changed)
		_cheer_texture = new_value
		if _cheer_texture and _cheer_texture != _normal_texture:
			_cheer_texture.changed.connect(_on_texture_changed)
		_on_texture_changed()

func _init() -> void:
	changed.connect(_on_changed)

func _on_changed() -> void:
	if not mode:
		return

	if not _shader:
		_shader = ShaderMaterial.new()
		_shader.shader = SHADER

		material = _shader

	_normal_texture = mode.get_icon_texture(normal_icon)
	_cheer_texture = mode.get_icon_texture(cheer_icon)

	_shader.set_shader_parameter(&"audience_texture", [_normal_texture, _cheer_texture])

func _on_texture_changed() -> void:
	assert(_shader, "shader should have been set before textures")

	var normal_texture_size := Vector2.ONE
	var cheer_texture_size := Vector2.ONE
	if _normal_texture:
		normal_texture_size = _normal_texture.get_size()
	if _cheer_texture:
		cheer_texture_size = _cheer_texture.get_size()

	var max_size := normal_texture_size.max(cheer_texture_size)

	size = max_size * pixel_size
	center_offset = Vector3(0.0, size.y / 2.0 - pixel_size, 0.0)

	var normal_scale := max_size / normal_texture_size
	var cheer_scale := max_size / cheer_texture_size
	var normal_transform := Vector4(normal_scale.x, normal_scale.y, 0.5 - normal_scale.x / 2.0, 1.0 - normal_scale.y)
	var cheer_transform := Vector4(cheer_scale.x, cheer_scale.y, 0.5 - cheer_scale.x / 2.0, 1.0 - cheer_scale.y)

	_shader.set_shader_parameter(&"audience_transform", [normal_transform, cheer_transform])

static func create(game_mode: GameMode, units_per_pixel: float, normal: IconDef.Icon, cheer: IconDef.Icon) -> AudienceMesh:
	var mesh := AudienceMesh.new()

	mesh.mode = game_mode
	mesh.pixel_size = units_per_pixel
	mesh.normal_icon = normal
	mesh.cheer_icon = cheer

	return mesh

static func create_multi_meshes(game_mode: GameMode) -> Array[MultiMesh]:
	var meshes: Array[MultiMesh] = []

	for member in game_mode.audience:
		var mesh := MultiMesh.new()
		mesh.mesh = create(game_mode, member.pixel_size, member.front, member.front_cheer)
		mesh.use_colors = true
		mesh.use_custom_data = true
		mesh.transform_format = MultiMesh.TRANSFORM_3D
		mesh.visible_instance_count = 0
		meshes.append(mesh)

		mesh = MultiMesh.new()
		mesh.mesh = create(game_mode, member.pixel_size, member.back, member.back_cheer)
		mesh.use_colors = true
		mesh.use_custom_data = true
		mesh.transform_format = MultiMesh.TRANSFORM_3D
		mesh.visible_instance_count = 0
		meshes.append(mesh)

	return meshes
