//go:build (android || (darwin && ios)) && !headless
// +build android darwin,ios
// +build !headless

package main

import (
	"context"
	"log"
	"os"
	"runtime"
	"sync"
	"time"

	"git.lubar.me/ben/spy-cards/arcade/touchcontroller"
	"git.lubar.me/ben/spy-cards/gfx"
	"git.lubar.me/ben/spy-cards/gfx/sprites"
	"git.lubar.me/ben/spy-cards/input"
	"git.lubar.me/ben/spy-cards/internal"
	"git.lubar.me/ben/spy-cards/spoilerguard"
	"golang.org/x/mobile/app"
	"golang.org/x/mobile/event/key"
	"golang.org/x/mobile/event/lifecycle"
	"golang.org/x/mobile/event/paint"
	"golang.org/x/mobile/event/size"
	"golang.org/x/mobile/event/touch"
	"golang.org/x/mobile/exp/audio/al"
	"golang.org/x/mobile/gl"
)

const startPath = "/game/home/vanilla"

var (
	heldInputs []input.Button
	inputTime  time.Time
	inputLock  sync.Mutex
)

func appMain(ictx *input.Context) {
	log.SetFlags(log.LstdFlags | log.Lshortfile)

	var (
		isVisible bool

		frameSyncFinished = make(chan struct{})
	)

	app.Main(func(a app.App) {
		defer al.CloseDevice()

		for e := range a.Events() {
			switch e := a.Filter(e).(type) {
			case lifecycle.Event:
				switch e.Crosses(lifecycle.StageVisible) {
				case lifecycle.CrossOn:
					gfx.Init(e.DrawContext.(gl.Context))

					isVisible = true

					a.Send(paint.Event{})
				case lifecycle.CrossOff:
					isVisible = false

					gfx.Release()

					if runtime.GOOS == "windows" {
						os.Exit(0) // hard-exit or we'll never exit on Windows
					}
				}

			case size.Event:
				gfx.SetSize(e)
				touchcontroller.Size(e)
				ictx.OnSize(e)

			case touch.Event:
				touchcontroller.Touch(e)
				ictx.OnTouch(e)

			case key.Event:
				if e.Modifiers&(key.ModControl|key.ModAlt|key.ModMeta) != 0 {
					continue
				}

				ictx.OnKey(e)
				spoilerguard.OnKey(e)

				var button input.Button

				switch e.Code {
				case key.CodeUpArrow:
					button = input.BtnUp
				case key.CodeDownArrow:
					button = input.BtnDown
				case key.CodeLeftArrow:
					button = input.BtnLeft
				case key.CodeRightArrow:
					button = input.BtnRight
				case key.CodeC:
					button = input.BtnConfirm
				case key.CodeX:
					button = input.BtnCancel
				case key.CodeZ:
					button = input.BtnSwitch
				case key.CodeV:
					button = input.BtnToggle
				case key.CodeEscape:
					button = input.BtnPause
				case key.CodeReturnEnter:
					button = input.BtnHelp
				default:
					// unhandled key

					continue
				}

				inputLock.Lock()

				switch e.Direction {
				case key.DirPress:
					heldInputs = append(heldInputs, button)
				case key.DirRelease:
					for i := 0; i < len(heldInputs); i++ {
						if heldInputs[i] == button {
							heldInputs = append(heldInputs[:i], heldInputs[i+1:]...)
							i--
						}
					}
				}

				inputTime = time.Now()

				inputLock.Unlock()

			case paint.Event:
				if e.External || !isVisible {
					continue
				}

				gfx.FrameSync <- frameSyncFinished
				<-frameSyncFinished
				touchcontroller.Render()
				a.Publish()
				a.Send(paint.Event{})
			}
		}
	})
}

func getInputs() []input.Button {
	inputLock.Lock()

	i := append([]input.Button(nil), heldInputs...)

	touchHeld, touchTime := touchcontroller.Held()
	i = append(i, touchHeld...)

	if touchTime.After(inputTime) {
		sprites.ButtonStyle = internal.StyleGenericGamepad
	} else {
		sprites.ButtonStyle = internal.StyleKeyboard
	}

	inputLock.Unlock()

	return i
}

func doPreload(ctx context.Context, s preloadSet) <-chan struct{} {
	return s.do(ctx, false)
}

func doRedirect(u string) {
	panic("TODO: doRedirect")
}

func pushURL(u string)        {}
func onPopURL(f func(string)) {}
