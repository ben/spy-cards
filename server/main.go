// Package server implements the web service for Spy Cards Online.
package server

import (
	"flag"
	"log"
	"net/http"
	"strconv"
	"strings"
	"time"

	"git.lubar.me/ben/spy-cards/internal"
	"github.com/coder/websocket"
	"github.com/jackc/pgtype"
)

// Main is the main method for the Spy Cards Online web service.
func Main() {
	flagHTTP := flag.String("http", ":8335", "[address]:port to listen on")
	flagDB := flag.String("db", "", "data source name for PostgreSQL")
	flag.Parse()

	initDB(*flagDB)
	initGodotRPC()

	http.HandleFunc("/spy-cards/ws", websocketServer)

	http.HandleFunc("/spy-cards/matchmaking/create-session", handleMatchmakingCreateSession)
	http.HandleFunc("/spy-cards/matchmaking/join", handleMatchmakingJoin)
	http.HandleFunc("/spy-cards/matchmaking/poll", handleMatchmakingPoll)
	http.HandleFunc("/spy-cards/matchmaking/send", handleMatchmakingSend)

	http.HandleFunc("/arcade/", handleLanding)
	http.HandleFunc("/game/", handleLanding)

	http.HandleFunc("/spy-cards/", spyCardsLanding)
	http.HandleFunc("/spy-cards/log-in", loginPage)
	http.HandleFunc("/spy-cards/log-out", logoutPage)
	http.HandleFunc("/spy-cards/change-password", changePasswordPage)

	http.HandleFunc("/spy-cards/report-issue", handleIssueReportPost)
	http.HandleFunc("/spy-cards/report-issue/auto", handleAutomatedReportPost)

	http.HandleFunc("/spy-cards/user-img/upload", uploadUserImg)
	http.HandleFunc("/spy-cards/user-img/prt.json", getUserImgTanjerinList)
	http.HandleFunc("/spy-cards/user-img/community.json", getUserImgCommunityList)
	http.HandleFunc("/spy-cards/user-img/", serveUserImg)
	http.HandleFunc("/spy-cards/godot-fileapi/", serveUserImgGodot)
	http.HandleFunc("/spy-cards/user-img/mode/", serveModePortraitSheet)
	http.HandleFunc("/spy-cards/oh_yeah_name_every_spy_card.zip", serveLegacyTestData)

	http.HandleFunc("/spy-cards/admin/", adminMiddleware)
	adminRouter.HandleFunc("/spy-cards/admin/", adminLanding)
	adminRouter.HandleFunc("/spy-cards/admin/portraits", adminPortraits)
	adminRouter.HandleFunc("/spy-cards/admin/reports", adminReports)
	adminRouter.HandleFunc("/spy-cards/admin/recordings", adminRecordings)
	adminRouter.HandleFunc("/spy-cards/admin/arcade-recordings", adminArcadeRecordings)
	adminRouter.HandleFunc("/spy-cards/admin/modes", adminModes)
	adminRouter.HandleFunc("/spy-cards/admin/users", adminUsers)
	adminRouter.HandleFunc("/spy-cards/admin/new-user", adminNewUser)
	adminRouter.HandleFunc("/spy-cards/admin/user", adminUser)
	adminRouter.HandleFunc("/spy-cards/admin/extensions", adminExtensions)

	http.HandleFunc("/spy-cards/changelog/", handleChangelog)
	http.HandleFunc("/spy-cards/custom/edit-mode/", editCustomMode)
	http.HandleFunc("/spy-cards/custom/api/", handleCustomCardAPI)
	http.HandleFunc("/spy-cards/custom/api/modes", handleCustomGameModes)
	http.HandleFunc("/spy-cards/custom/api/extensions.json", getExtensionLibrary)

	http.HandleFunc("/spy-cards/godot-api/status", handleGodotStatus)

	http.HandleFunc("/spy-cards/recording/upload", uploadMatchRecording)
	http.HandleFunc("/spy-cards/recording/get/", getMatchRecording)
	http.HandleFunc("/spy-cards/recording/random", randomMatchRecording)

	http.HandleFunc("/spy-cards/arcade/api/upload", handleArcadeAPIUpload)
	http.HandleFunc("/spy-cards/arcade/api/get/", handleArcadeAPIGetRecording)
	http.HandleFunc("/spy-cards/arcade/api/scores", handleArcadeAPIHighScores)

	http.HandleFunc("/spy-cards/high-scores", handleHighScoresMain)
	http.HandleFunc("/spy-cards/high-scores/", handleHighScoresTable)

	panic(http.ListenAndServe(*flagHTTP, nil))
}

func validateOrigin(w http.ResponseWriter, r *http.Request) bool {
	w.Header().Add("Vary", "Origin")

	origin := r.Header.Get("Origin")
	if origin == "" {
		return true
	}

	var originOK bool
	if err := checkAllowedOrigin.QueryRow(origin).Scan(&originOK); err != nil {
		log.Println("checking origin", origin, err)
		http.Error(w, "error checking origin", http.StatusInternalServerError)

		return false
	}

	if !originOK {
		http.Error(w, "origin not whitelisted", http.StatusForbidden)

		return false
	}

	w.Header().Add("Access-Control-Allow-Origin", origin)

	return true
}

func websocketServer(w http.ResponseWriter, r *http.Request) {
	var origins pgtype.TextArray
	if err := getAllowedOrigins.QueryRow().Scan(&origins); err != nil {
		// keep going, we may be fine
		log.Println("failed to get allowed origins:", err)
	}

	opts := &websocket.AcceptOptions{
		Subprotocols: []string{"match", "lobby"},
	}
	_ = origins.AssignTo(&opts.OriginPatterns)

	for i, o := range opts.OriginPatterns {
		opts.OriginPatterns[i] = o[strings.LastIndexByte(o, '/')+1:]
	}

	conn, err := websocket.Accept(w, r, opts)
	if err != nil {
		log.Println("websocket accept err:", err)

		return
	}

	// bump packet size limit from 32k to 1M (some custom card sets are over 32k)
	conn.SetReadLimit(1 << 20)

	switch conn.Subprotocol() {
	case "", "match":
		matchmakingHandler(conn, r)
	default:
		_ = conn.Close(internal.MMClientError, "failed to negotiate subprotocol")
	}
}

func timing(w http.ResponseWriter, key string) func() {
	start := time.Now()

	return func() {
		dur := time.Since(start)

		w.Header().Add("Server-Timing", key+";dur="+strconv.FormatFloat(dur.Seconds()*1000, 'f', 2, 64))
	}
}
