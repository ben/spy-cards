package server

import (
	"bytes"
	"context"
	"crypto/sha256"
	"database/sql"
	"encoding/base64"
	"errors"
	"fmt"
	"html/template"
	"io"
	"log"
	"math/rand"
	"net/http"
	"runtime"
	"strconv"
	"strings"
	"time"

	"git.lubar.me/ben/spy-cards/card"
	"git.lubar.me/ben/spy-cards/format"
	"git.lubar.me/ben/spy-cards/match"
	"github.com/jackc/pgtype"
)

func uploadMatchRecording(w http.ResponseWriter, r *http.Request) {
	w.Header().Add("Cache-Control", "no-store")

	if !validateOrigin(w, r) {
		return
	}

	w.Header().Add("Access-Control-Allow-Methods", http.MethodPut)
	w.Header().Add("Access-Control-Allow-Headers", "content-type")

	if r.Method == http.MethodOptions {
		if r.Header.Get("Access-Control-Request-Method") != http.MethodPut {
			w.WriteHeader(http.StatusBadRequest)

			return
		}

		w.WriteHeader(http.StatusNoContent)

		return
	}

	if r.Method != http.MethodPut {
		w.Header().Add("Allow", "PUT, OPTIONS")
		http.Error(w, "only PUT is allowed", http.StatusMethodNotAllowed)

		return
	}

	readDone := timing(w, "read")

	defer r.Body.Close()

	data, err := io.ReadAll(r.Body)
	if err != nil {
		log.Println("match recording upload error (read)", err)
		http.Error(w, "failed to read uploaded match", http.StatusInternalServerError)

		return
	}

	var mr card.Recording

	err = mr.UnmarshalBinary(data)
	if err != nil {
		log.Println("failed recording:", base64.StdEncoding.EncodeToString(data))
		log.Println("match recording upload error (decode)", err)
		http.Error(w, "failed to decode uploaded match", http.StatusBadRequest)

		return
	}

	hash := sha256.Sum256(data)

	readDone()

	modePrefix := mr.ModeName

	var modeSuffix sql.NullString

	if i := strings.IndexByte(modePrefix, '.'); i != -1 {
		modeSuffix.String = modePrefix[i+1:]
		modeSuffix.Valid = true
		modePrefix = modePrefix[:i]
	}

	switch {
	case mr.ModeName == "custom":
		for _, c := range mr.CustomCards.Cards {
			if c.Portrait == card.PortraitCustomEmbedded {
				http.Error(w, "cannot upload custom cards with embedded PNG files to this endpoint (try reuploading the card image on the card with the longest code)", http.StatusBadRequest)

				return
			}
		}
	case mr.ModeName == "":
		if len(mr.CustomCards.Cards) != 0 {
			http.Error(w, "vanilla recording contained custom cards", http.StatusBadRequest)

			return
		}
	case !modeSuffix.Valid || modeSuffix.String == "":
		log.Println("failed recording:", base64.StdEncoding.EncodeToString(data))
		log.Println("unexpected recording mode", mr.ModeName, "(no suffix)")
		http.Error(w, "unexpected recording mode "+mr.ModeName, http.StatusBadRequest)

		return
	default:
		var (
			modeName      string
			expectedCards string
			modeRevision  int
		)

		// TODO: fix this code for variants
		if false {
			if err := getCustomCardSet.QueryRow(modePrefix, modeSuffix).Scan(&modeName, &expectedCards, &modeRevision); errors.Is(err, sql.ErrNoRows) {
				log.Println("failed recording:", base64.StdEncoding.EncodeToString(data))
				log.Println("unexpected recording mode", mr.ModeName, "(not in DB)")
				http.Error(w, "unexpected recording mode "+mr.ModeName, http.StatusBadRequest)

				return
			} else if err != nil {
				log.Println("failed recording:", base64.StdEncoding.EncodeToString(data))
				log.Println("recording mode lookup failed", err)
				http.Error(w, "database error looking up mode", http.StatusInternalServerError)

				return
			}

			expectedCodes := strings.Split(expectedCards, ",")

			if mr.CustomCards.Mode != nil {
				b, err := mr.CustomCards.Mode.MarshalBinary()
				if err != nil {
					log.Println("failed recording:", base64.StdEncoding.EncodeToString(data))
					log.Println("card code enc err", 0, expectedCodes[0], err)
					http.Error(w, "unexpected content for game mode", http.StatusBadRequest)

					return
				}

				if enc := base64.StdEncoding.EncodeToString(b); enc != expectedCodes[0] {
					log.Println("failed recording:", base64.StdEncoding.EncodeToString(data))
					log.Println("card code didn't match ", 0, expectedCodes[0], enc)
					http.Error(w, "unexpected content for game mode", http.StatusBadRequest)

					return
				}

				expectedCodes = expectedCodes[1:]
			}

			if len(expectedCodes) != len(mr.CustomCards.Cards) {
				log.Println("failed recording:", base64.StdEncoding.EncodeToString(data))
				log.Println("card code didn't match (length)", len(expectedCodes), len(mr.CustomCards.Cards))
				http.Error(w, "unexpected content for custom card list", http.StatusBadRequest)

				return
			}

			for i, cc := range mr.CustomCards.Cards {
				b, err := cc.MarshalBinary()
				if err != nil {
					log.Println("failed recording:", base64.StdEncoding.EncodeToString(data))
					log.Println("card code enc err", i, expectedCodes[i], err)
					http.Error(w, "unexpected content for custom card list", http.StatusBadRequest)

					return
				}

				if enc := base64.StdEncoding.EncodeToString(b); enc != expectedCodes[i] {
					log.Println("failed recording:", base64.StdEncoding.EncodeToString(data))
					log.Println("card code didn't match ", i, expectedCodes[i], enc)
					http.Error(w, "unexpected content for custom card list", http.StatusBadRequest)

					return
				}
			}
		}
	}

	dbDone := timing(w, "db")

	var id format.FileID

	copy(id.Hash[:], hash[:])

	if err = insertMatchRecording.QueryRow(
		r.Header.Get("X-Forwarded-For"),
		hash[:],
		data,
		mr.FormatVersion,
		mr.Version[0],
		mr.Version[1],
		mr.Version[2],
		modePrefix,
		modeSuffix,
	).Scan(&id.ID); err != nil {
		log.Println("match recording upload error (store)", err)
		http.Error(w, "failed to store uploaded match", http.StatusInternalServerError)

		return
	}

	dbDone()

	go decodeSpyCardsRecording(int64(id.ID))

	w.Header().Add("Content-Type", "text/plain")
	w.WriteHeader(http.StatusCreated)

	b, _ := id.MarshalText()
	_, _ = w.Write(b)
}

func getMatchRecording(w http.ResponseWriter, r *http.Request) {
	w.Header().Add("Access-Control-Allow-Origin", "*")

	if r.Method != http.MethodGet && r.Method != http.MethodHead {
		w.Header().Add("Allow", "GET, HEAD")
		http.Error(w, "only GET is allowed", http.StatusMethodNotAllowed)

		return
	}

	w.Header().Add("Cache-Control", "no-store")

	var id format.FileID

	err := id.UnmarshalText([]byte(r.URL.Path[len("/spy-cards/recording/get/"):]))
	if err != nil {
		w.Header().Set("Cache-Control", "public, max-age=86400")
		http.NotFound(w, r)

		return
	}

	dbDone := timing(w, "db")

	var (
		matchHash, matchData   []byte
		modePrefix, modeSuffix sql.NullString
	)

	err = getMatchRecordingData.QueryRow(id.ID).Scan(&matchHash, &matchData, &modePrefix, &modeSuffix)
	if errors.Is(err, sql.ErrNoRows) {
		w.Header().Set("Cache-Control", "public, max-age=600")
		http.NotFound(w, r)

		return
	}

	if !bytes.Equal(id.Hash[:], matchHash[:8]) {
		w.Header().Set("Cache-Control", "public, max-age=86400")
		http.NotFound(w, r)

		return
	}

	dbDone()

	revision, _ := strconv.Atoi(modeSuffix.String)

	if modePrefix.Valid && modeSuffix.Valid && modePrefix.String != "" && modeSuffix.String != "" && modeHasCustomPortraits(modePrefix.String, revision) {
		w.Header().Add("Link", "</spy-cards/user-img/mode/"+modePrefix.String+"."+modeSuffix.String+".webp>;rel=preload;as=image;crossorigin=anonymous")
	}

	w.Header().Set("Cache-Control", "public, max-age=31536000, immutable")
	w.Header().Set("Content-Type", "application/vnd.spycards.matchdata")
	w.Header().Set("Content-Length", strconv.Itoa(len(matchData)))
	w.WriteHeader(http.StatusOK)

	if r.Method == http.MethodGet {
		_, _ = w.Write(matchData)
	}
}

func randomMatchRecording(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodGet && r.Method != http.MethodHead {
		w.Header().Add("Allow", "GET, HEAD")
		http.Error(w, "only GET is allowed", http.StatusMethodNotAllowed)

		return
	}

	w.Header().Add("Cache-Control", "no-store")

	if !validateOrigin(w, r) {
		return
	}

	query := r.URL.Query()

	searchMode := sql.NullString{String: query.Get("mode")}
	_, searchMode.Valid = query["mode"]

	notCodes := query["not"]

	notIDs := make([]int64, len(notCodes))

	for i, c := range notCodes {
		var id format.FileID

		err := id.UnmarshalText([]byte(c))
		if err != nil {
			http.Error(w, "invalid request", http.StatusBadRequest)

			return
		}

		notIDs[i] = int64(id.ID)
	}

	var notIDsArray pgtype.Int8Array
	if err := notIDsArray.Set(notIDs); err != nil {
		log.Println("get random recording failed to assign array", err)
		http.Error(w, "database error", http.StatusInternalServerError)

		return
	}

	var (
		id   format.FileID
		hash []byte
	)

	dbDone := timing(w, "db")

	recent := rand.Intn(2) == 0 /*#nosec*/

	err := getRandomMatchRecording.QueryRow(notIDsArray, &searchMode, &recent).Scan(&id.ID, &hash)

	if recent && errors.Is(err, sql.ErrNoRows) {
		recent = false

		err = getRandomMatchRecording.QueryRow(notIDsArray, &searchMode, &recent).Scan(&id.ID, &hash)
	}

	dbDone()

	switch {
	case errors.Is(err, sql.ErrNoRows):
		http.Error(w, "no more matches available", http.StatusServiceUnavailable)
	case err != nil:
		log.Println("get random recording failed", err)
		http.Error(w, "database error", http.StatusInternalServerError)
	default:
		copy(id.Hash[:], hash)
		b, _ := id.MarshalText()

		w.Header().Add("Link", "</spy-cards/recording/get/"+string(b)+">;rel=preload;as=fetch;crossorigin=anonymous")
		w.WriteHeader(http.StatusOK)

		_, _ = w.Write(b)
	}
}

func decodeSpyCardsRecording(id int64) {
	var (
		rec card.Recording
		r   *match.Recording

		hash, data     []byte
		prefix, suffix sql.NullString
	)

	err := getMatchRecordingData.QueryRow(id).Scan(&hash, &data, &prefix, &suffix)
	if err != nil {
		goto handleError
	}

	err = rec.UnmarshalBinary(data)
	if err != nil {
		goto handleError
	}

	{
		ctx, cancel := context.WithTimeout(context.Background(), 1*time.Minute)
		defer cancel()

		r, err = match.NewRecording(ctx, &rec)
		if err != nil {
			goto handleError
		}

		m := r.CreateMatch()

		max := uint64(r.NumRounds() - 1)
		for !r.RoundProcessed(max) {
			runtime.Gosched()
		}

		for i := uint64(0); i < max; i++ {
			state, turn, err1 := r.Round(i)
			if err1 != nil {
				err = fmt.Errorf("round %d error %w", state.Round, err)

				goto handleError
			}

			m.State = *state
			m.State.TurnData = turn

			for player := range state.Sides {
				tp := state.Sides[player].TP

				if turn.Ready[player]&^((1<<len(state.Sides[player].Hand))-1) != 0 {
					err = fmt.Errorf("verification: P%d played cards %b from %d card hand on round %d", player+1, turn.Ready[player], len(state.Sides[player].Hand), state.Round)

					goto handleError
				}

				for i, c := range state.Sides[player].Hand {
					if turn.Ready[player]&(1<<i) == 0 {
						continue
					}

					cost := card.Num(c.Def.TP)
					cost.Add(state.Sides[player].ModTP[c.Def.ID], m.SpecialFlags[card.SpecialSubtractInf])
					cost.Negate()

					tp.Add(cost, m.SpecialFlags[card.SpecialSubtractInf])
				}

				if tp.Less(card.Num(0)) {
					err = fmt.Errorf("verification: P%d has %v remaining TP on round %d", player+1, tp, state.Round)

					goto handleError
				}
			}

			if w := m.Winner(); w != 0 {
				err = fmt.Errorf("verification: P%d wins on round %d of %d", w, i, max)

				goto handleError
			}
		}

		state, _, err1 := r.Round(max)
		if err1 != nil {
			err = fmt.Errorf("end of match error %w", err)

			goto handleError
		}

		m.State = *state

		if w := m.Winner(); w == 0 {
			err = fmt.Errorf("verification: No match winner: P1 Health %v, P2 Health %v, Round Winner %d", m.State.Sides[0].HP, m.State.Sides[1].HP, m.State.RoundWinner)

			goto handleError
		}
	}

	goto updateDB

handleError:
	if err == nil {
		err = errors.New("unknown error")
	}

updateDB:
	var errString sql.NullString

	if err != nil {
		errString.String = fmt.Sprintf("%+v", err)
		errString.Valid = true
	}

	_, err = decodedSpyCardsRecording.Exec(id, &errString)
	if err != nil {
		log.Println("failed to update decoded recording:", err)
	}

	if r != nil {
		updateCardHighScores(id, r)
	}
}

func updateCardHighScores(id int64, rec *match.Recording) {
	type availableCard struct {
		tableID int64
		cardID  int64
		code    []byte
		defID   card.ID
	}

	var availableCards []availableCard

	rows, err := getHighScoreCards.Query()
	if err != nil {
		log.Println("failed to get list of cards elligeable for high scores:", err)

		return
	}
	defer rows.Close()

	for rows.Next() {
		var a availableCard

		err = rows.Scan(&a.tableID, &a.cardID, &a.code)
		if err != nil {
			log.Println("failed to read high score cards:", err)

			return
		}

		if a.code[0] != 5 { // latest card format revision
			var def card.Def

			err = def.UnmarshalBinary(a.code)
			if err == nil {
				b, err := def.MarshalBinary()
				if err == nil {
					a.code = b
				}
			}
		}

		for _, def := range rec.Recording.CustomCards.Cards {
			if b, err := def.MarshalBinary(); err == nil && bytes.Equal(b, a.code) {
				a.defID = def.ID
				availableCards = append(availableCards, a)

				break
			}
		}
	}

	if err := rows.Err(); err != nil {
		log.Println("failed to read high score cards:", err)

		return
	}

	_, err = deleteRecordingHighScores.Exec(id)
	if err != nil {
		log.Println("failed to remove old recording high scores:", err)

		return
	}

	for round := 0; round < rec.NumRounds(); round++ {
		_, t, err := rec.Round(uint64(round))
		if err != nil {
			continue
		}

		state, err := rec.RoundEnd(uint64(round))
		if err != nil {
			continue
		}

		if t == nil {
			continue
		}

		for player := range t.Played {
			seen := make(map[card.ID]bool)
			for _, cardID := range t.Played[player] {
				if seen[cardID] {
					continue
				}

				seen[cardID] = true

				for _, a := range availableCards {
					if a.defID == cardID {
						_, err = addRecordingHighScore.Exec(a.tableID, a.cardID, id, round, player == 0, state.Sides[player].FinalATK.Amount)
						if err != nil {
							log.Println("recording high score:", err)
						}
					}
				}
			}
		}
	}
}

var (
	tmplHighScoresMain = template.Must(template.New("high-scores-main").Parse(`<!DOCTYPE html>
<html lang="en" id="top-of-page">
<head>
<meta charset="utf-8">
<title>Custom Card High Scores - Spy Cards Online</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="/style/layout.css">
<meta name="color-scheme" content="dark light">
</head>
<body>
<article class="readme">
<a href="/game.html" rel="home" class="back-link">&larr; Back</a>
<h1>Custom Card High Scores</h1>
<table>
<thead>
<th>Title</th>
<th>Entries</th>
</thead>
<tbody>
{{range .Tables -}}
<tr>
<td><a href="/spy-cards/high-scores/{{.ID}}">{{.Title}}</a></td>
<td>{{.Entries}}</td>
</tr>
{{end -}}
</tbody>
</table>
</article>
</body>
</html>
`))
	tmplHighScoresTable = template.Must(template.New("high-scores-table").Parse(`<!DOCTYPE html>
<html lang="en" id="top-of-page">
<head>
<meta charset="utf-8">
<title>{{.Table.Title}} - Custom Card High Scores - Spy Cards Online</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="/style/layout.css">
<meta name="color-scheme" content="dark light">
</head>
<body>
<article class="readme">
<a href="/spy-cards/high-scores" rel="up" class="back-link">&larr; Back</a>
<h1>High Scores: {{.Table.Title}}</h1>
<p><em>Showing {{len .Entries}} of {{.Table.Entries}} entries.</em></p>
<table>
<thead>
<tr>
<th>#</th>
<th>Recording</th>
<th>Date</th>
<th>Version</th>
<th>Player</th>
<th>Round</th>
<th>Score</th>
</tr>
</thead>
<tbody>
{{range .Entries -}}
<tr>
<td>{{.Num}}</td>
<th><a href="/game.html?recording={{.RecordingID}}">{{.RecordingID}}</a></th>
<td><time datetime="{{.UploadedAt.Format "2006-01-02T15:04:05Z07:00"}}">{{.UploadedAt.Format "2 Jan 2006"}}</time></td>
<td>v{{.VersionMajor}}.{{.VersionMinor}}.{{.VersionPatch}}{{with .ModePrefix}}-{{.}}{{end}}{{with .ModeSuffix}}.{{.}}{{end}}</td>
<td>{{if .ByHost}}P1{{else}}P2{{end}}</td>
<td>{{.Round}}</td>
<td>{{.Score}}</td>
</tr>
{{end -}}
</tbody>
<table>
</article>
</body>
</html>
`))
)

type highScoresTable struct {
	ID      int64
	Title   string
	Entries int64
}

type highScoresEntry struct {
	Num           int64
	RecordingID   format.FileID
	RecordingHash []byte
	Round         int64
	ByHost        bool
	Score         int64
	UploadedAt    time.Time
	VersionMajor  int64
	VersionMinor  int64
	VersionPatch  int64
	ModePrefix    string
	ModeSuffix    string
}

func handleHighScoresMain(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Cache-Control", "no-store")

	dbDone := timing(w, "db")

	rows, err := getHighScoreTables.QueryContext(r.Context(), nil)
	if err != nil {
		log.Println("getting high scores table list:", err)
		http.Error(w, "database error", http.StatusInternalServerError)

		return
	}
	defer rows.Close()

	var data struct {
		Tables []highScoresTable
	}

	for rows.Next() {
		var table highScoresTable

		err = rows.Scan(&table.ID, &table.Title, &table.Entries)
		if err != nil {
			log.Println("reading high scores table list:", err)
			http.Error(w, "database error", http.StatusInternalServerError)

			return
		}

		data.Tables = append(data.Tables, table)
	}

	if err = rows.Err(); err != nil {
		log.Println("reading high scores table list:", err)
		http.Error(w, "database error", http.StatusInternalServerError)

		return
	}

	dbDone()

	w.Header().Set("Cache-Control", "public, max-age=3600, s-maxage=30, stale-while-revalidate=86400")
	w.Header().Set("Content-Type", "text/html; charset=utf-8")
	_ = tmplHighScoresMain.Execute(w, &data)
}

func handleHighScoresTable(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Cache-Control", "no-store")

	if r.URL.Path == "/spy-cards/high-scores/" {
		w.Header().Set("Cache-Control", "public, max-age=3600, stale-while-revalidate=86400")
		http.Redirect(w, r, "/spy-cards/high-scores", http.StatusMovedPermanently)

		return
	}

	tableID, err := strconv.ParseInt(r.URL.Path[len("/spy-cards/high-scores/"):], 10, 64)
	if err != nil {
		w.Header().Set("Cache-Control", "public, max-age=3600, stale-while-revalidate=86400")
		http.NotFound(w, r)

		return
	}

	dbDone := timing(w, "db")

	var data struct {
		Table   highScoresTable
		Entries []highScoresEntry
	}

	if err := getHighScoreTables.QueryRowContext(r.Context(), tableID).Scan(&data.Table.ID, &data.Table.Title, &data.Table.Entries); errors.Is(err, sql.ErrNoRows) {
		w.Header().Set("Cache-Control", "public, max-age=3600, s-maxage=30, stale-while-revalidate=86400")
		http.NotFound(w, r)

		return
	} else if err != nil {
		log.Println("getting high scores table:", err)
		http.Error(w, "database error", http.StatusInternalServerError)

		return
	}

	if data.Table.Entries > 1000 {
		data.Entries = make([]highScoresEntry, 0, 1000)
	} else {
		data.Entries = make([]highScoresEntry, 0, data.Table.Entries)
	}

	rows, err := getHighScoreEntries.QueryContext(r.Context(), tableID)
	if err != nil {
		log.Println("getting high scores entries:", err)
		http.Error(w, "database error", http.StatusInternalServerError)

		return
	}
	defer rows.Close()

	for rows.Next() {
		var entry highScoresEntry
		if err = rows.Scan(&entry.RecordingID.ID, &entry.RecordingHash, &entry.Round, &entry.ByHost, &entry.Score, &entry.UploadedAt, &entry.VersionMajor, &entry.VersionMinor, &entry.VersionPatch, &entry.ModePrefix, &entry.ModeSuffix); err != nil {
			log.Println("reading high scores entries:", err)
			http.Error(w, "database error", http.StatusInternalServerError)

			return
		}

		copy(entry.RecordingID.Hash[:], entry.RecordingHash)

		entry.Num = int64(len(data.Entries)) + 1
		entry.Round++
		data.Entries = append(data.Entries, entry)
	}

	if err = rows.Err(); err != nil {
		log.Println("getting high scores entries:", err)
		http.Error(w, "database error", http.StatusInternalServerError)

		return
	}

	dbDone()

	w.Header().Set("Cache-Control", "public, max-age=30, stale-while-revalidate=86400")
	w.Header().Set("Content-Type", "text/html; charset=utf-8")
	_ = tmplHighScoresTable.Execute(w, &data)
}
