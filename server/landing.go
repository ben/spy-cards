package server

import (
	"bytes"
	"database/sql"
	"errors"
	"log"
	"net/http"
	"strconv"
	"strings"

	"git.lubar.me/ben/spy-cards/arcade"
	"git.lubar.me/ben/spy-cards/card"
	"git.lubar.me/ben/spy-cards/format"
	"git.lubar.me/ben/spy-cards/internal"
	"git.lubar.me/ben/spy-cards/internal/router"
	"git.lubar.me/ben/spy-cards/match"
)

func handleLanding(w http.ResponseWriter, r *http.Request) {
	var info router.PageInfo

	w.Header().Add("Cache-Control", "public, max-age=30")

	err := info.UnmarshalText([]byte(r.RequestURI))
	if err != nil {
		log.Printf("landing err %+v for %q", err, r.RequestURI)

		http.Error(w, "error parsing request", http.StatusInternalServerError)

		return
	}

	if info.ShouldRedirect() {
		http.Error(w, "bad request filename", http.StatusBadRequest)

		return
	}

	var name string

	data := templateData{WASM: true}

	b, err := info.MarshalText()
	if err != nil {
		panic(err)
	}

	data.Canon = string(b)

	switch info.Page {
	case router.PageNotFound:
		http.NotFound(w, r)

		return
	case router.PageArcadeGame:
		switch info.Game {
		case arcade.FlowerJourney:
			name = "page-arcade-game-flower-journey.tmpl"
			data.Title = "Flower Journey - Termacade"

			// TODO
		case arcade.MiteKnight:
			name = "page-arcade-game-mite-knight.tmpl"
			data.Title = "Mite Knight - Termacade"

			// TODO
		default:
			panic("server: unhandled arcade game: " + info.Game.String())
		}
	case router.PageArcadeHighScores:
		if info.HSMode != arcade.AllTime {
			w.Header().Add("X-Robots-Tag", "noindex")
		}

		name = "page-arcade-high-scores.tmpl"
		data.Title = "Termacade"

		// TODO
	case router.PageArcadeRecording:
		name = "page-arcade-recording.tmpl"

		var fileID format.FileID

		err = fileID.UnmarshalText([]byte(info.Code))
		if err != nil {
			http.NotFound(w, r)

			return
		}

		var hash, recData []byte

		err := getArcadeRecording.QueryRowContext(r.Context(), fileID.ID).Scan(&hash, &recData)
		if errors.Is(err, sql.ErrNoRows) {
			http.NotFound(w, r)

			return
		}

		if err != nil {
			log.Printf("landing arcade recording err %+v for %q", err, r.RequestURI)
			http.Error(w, "database error", http.StatusInternalServerError)

			return
		}

		if !bytes.Equal(hash[:8], fileID.Hash[:]) {
			http.NotFound(w, r)

			return
		}

		var rec arcade.Recording

		err = rec.UnmarshalBinary(recData)
		if err != nil {
			log.Printf("landing arcade recording err %+v for %q", err, r.RequestURI)
			http.Error(w, "database error", http.StatusInternalServerError)

			return
		}

		switch rec.Game {
		case arcade.MiteKnight:
			data.Title = "Mite Knight"
			data.PreviewImage = internal.GetConfig(r.Context()).BaseURL + "img/arcade/mkpreview.jpeg"
		case arcade.FlowerJourney:
			data.Title = "Flower Journey"
			data.PreviewImage = internal.GetConfig(r.Context()).BaseURL + "img/arcade/fjpreview.jpeg"
		default:
			data.Title = rec.Game.String()
		}

		data.Description = "A recording of the " + data.Title + " arcade game being played by " + rec.PlayerName + " on " + rec.Start.Format("Jan 2, 2006") + "."
		data.Title += " Recording (" + rec.PlayerName + ", " + rec.Start.Format("Jan 2, 2006") + ") - Termacade"
	case router.PageCardsHome:
		if info.Revision != 0 {
			w.Header().Add("X-Robots-Tag", "noindex")
		}

		name = "page-cards-home.tmpl"

		var (
			modeName  = "Play Online"
			cardCodes []byte
			revision  int
		)

		if info.Mode == match.ModeCustom {
			w.Header().Add("X-Robots-Tag", "noindex")

			data.IsCustom = true
		} else if info.Mode != "" && info.Mode != "vanilla" {
			err = getCustomCardSet.QueryRowContext(r.Context(), info.Mode, &sql.NullInt64{
				Int64: int64(info.Revision),
				Valid: info.Revision != 0,
			}).Scan(&modeName, &cardCodes, &revision)

			if errors.Is(err, sql.ErrNoRows) {
				http.NotFound(w, r)

				return
			}

			if err != nil {
				log.Printf("landing home err %+v for %q", err, r.RequestURI)
				http.Error(w, "database error", http.StatusInternalServerError)

				return
			}
		}

		data.Title = modeName + " - Spy Cards Online"

		data.Set = &card.Set{}

		if err = data.Set.UnmarshalText(cardCodes); err != nil {
			log.Printf("landing home err %+v for %q", err, r.RequestURI)
			http.Error(w, "database error", http.StatusInternalServerError)

			return
		}

		data.Set.Variant = int(info.Variant)

		variants := data.Set.Mode.GetAll(card.FieldVariant)
		if len(variants) != 0 {
			data.Variants = make([]*card.Variant, len(variants))

			for i, f := range variants {
				data.Variants[i] = f.(*card.Variant)
			}
		}

		data.Set.Mode, data.Variant = data.Set.Mode.Variant(int(info.Variant))

		data.PreviewImage = internal.GetConfig(r.Context()).BaseURL + "img/logo512.png"
		if md, ok := data.Set.Mode.Get(card.FieldMetadata).(*card.Metadata); ok && md.Portrait == card.PortraitCustomExternal {
			data.PreviewImage = internal.GetConfig(r.Context()).UserImageBaseURL + format.Encode32(md.CustomPortrait) + ".png"
		}

		if b, err := (&router.PageInfo{
			Page: router.PageCardsDeckEditor,
			Mode: info.Mode,
		}).MarshalText(); err == nil {
			data.DeckEditorURL = string(b)
		}

		data.Cards = match.CardsForHome(data.Set)

		first := true

		for _, f := range data.Set.Mode.GetAll(card.FieldMetadata) {
			md := f.(*card.Metadata)

			title := md.Title

			if first {
				first = false

				if title == "" && modeName != "" {
					title = modeName + " (v" + strconv.Itoa(revision) + ")"
				}

				data.Description = md.Description
			}

			if title != "" {
				data.Text = append(data.Text, pageText{
					Type:           "title",
					Text:           title,
					Portrait:       md.Portrait,
					CustomPortrait: md.CustomPortrait,
				})
			}

			if md.Author != "" {
				data.Text = append(data.Text, pageText{
					Type: "author",
					Text: md.Author,
				})
			}

			if md.Description != "" {
				for _, para := range strings.Split(md.Description, "\n\n") {
					data.Text = append(data.Text, pageText{
						Type: "paragraph",
						Text: para,
					})
				}
			}

			if md.LatestChanges != "" {
				data.Text = append(data.Text, pageText{
					Type: "latest-changes",
					Text: internal.GetConfig(r.Context()).ModeChangelogBaseURL + info.Mode + "#v" + strconv.Itoa(revision),
				})

				for _, para := range strings.Split(md.LatestChanges, "\n\n") {
					data.Text = append(data.Text, pageText{
						Type: "paragraph",
						Text: para,
					})
				}
			}
		}
	case router.PageCardsJoin:
		w.Header().Add("X-Robots-Tag", "noindex")

		name = "page-cards-join.tmpl"
		data.Title = "Joining Match - Spy Cards Online"
	case router.PageCardsModes:
		name = "page-cards-modes.tmpl"
		data.Title = "Community Game Modes - Spy Cards Online"

		data.Set = &card.Set{}
		data.CardLink = make(map[card.ID]string)

		serverAvailable := getPublicGameModes(r.Context(), w, false)
		if serverAvailable == nil {
			return
		}

		available := &match.AvailableModes{
			Categories: serverAvailable.Categories,
			Modes:      make([]match.AvailableMode, len(serverAvailable.Modes)),
		}

		for i := range serverAvailable.Modes {
			available.Modes[i] = serverAvailable.Modes[i].AvailableMode
		}

		match.MakeModesPseudoSet(data.Set, available, func(c *card.Def, _ *card.Metadata, am *match.AvailableMode) {
			data.Cards = append(data.Cards, c.ID)

			var page *router.PageInfo

			if am != nil {
				page = &router.PageInfo{
					Page: router.PageCardsHome,
					Mode: am.Client,
				}
			} else {
				switch c.ID {
				case 0:
					page = &router.PageInfo{
						Page: router.PageCardsHome,
						Mode: match.ModeCustom,
					}
				case 1:
					page = &router.PageInfo{
						Page: router.PageStatic,
						Raw:  "/settings.html",
					}
				case 2:
					page = &router.PageInfo{
						Page: router.PageArcadeMain,
					}
				case 3:
					page = &router.PageInfo{
						Page: router.PageAphidFestival,
					}
				default:
					version := card.BugFablesVersion(c.ID - 4)

					page = &router.PageInfo{
						Page: router.PageCardsHome,
						Mode: "vanilla",
					}

					if version != card.LatestVersion {
						page.Revision = int(version + 1)
					}
				}
			}

			b, err := page.MarshalText()
			if err == nil {
				data.CardLink[c.ID] = string(b)
			}
		})
	case router.PageCardsDeck:
		name = "page-cards-deck.tmpl"

		var deck card.Deck

		err = deck.UnmarshalText([]byte(info.Code))
		if err != nil {
			http.NotFound(w, r)

			return
		}

		data.CardClass = " small"
		data.Cards = deck

		var (
			modeName  string
			cardCodes []byte
			revision  int
		)

		if info.Mode == match.ModeCustom {
			w.Header().Add("X-Robots-Tag", "noindex")

			data.IsCustom = true
		} else if info.Mode != "" && info.Mode != "vanilla" {
			err = getCustomCardSet.QueryRowContext(r.Context(), info.Mode, &sql.NullInt64{}).Scan(&modeName, &cardCodes, &revision)

			if errors.Is(err, sql.ErrNoRows) {
				http.NotFound(w, r)

				return
			}

			if err != nil {
				log.Printf("landing deck err %+v for %q", err, r.RequestURI)
				http.Error(w, "database error", http.StatusInternalServerError)

				return
			}
		}

		data.Title = modeName + " Shared Deck - Spy Cards Online"
		if modeName == "" {
			data.Title = data.Title[1:]
		}

		data.Set = &card.Set{}

		if err = data.Set.UnmarshalText(cardCodes); err != nil {
			log.Printf("landing deck err %+v for %q", err, r.RequestURI)
			http.Error(w, "database error", http.StatusInternalServerError)

			return
		}

		data.PreviewImage = internal.GetConfig(r.Context()).BaseURL + "img/logo512.png"
		if md, ok := data.Set.Mode.Get(card.FieldMetadata).(*card.Metadata); ok && md.Portrait == card.PortraitCustomExternal {
			data.PreviewImage = internal.GetConfig(r.Context()).UserImageBaseURL + format.Encode32(md.CustomPortrait) + ".png"
		}

		b = []byte("A shared Spy Cards Online deck containing ")

		for i, id := range deck {
			c := data.Set.Card(id)
			if c == nil {
				http.NotFound(w, r)

				return
			}

			if i != 0 && i == len(deck)-1 {
				if i == 1 {
					b = append(b, " and "...)
				} else {
					b = append(b, ", and "...)
				}
			} else if i != 0 {
				b = append(b, ", "...)
			}

			b = append(b, c.DisplayName()...)
		}

		data.Description = string(b)
	case router.PageCardsDeckEditor:
		name = "page-cards-deck-editor.tmpl"
		data.Title = "Spy Cards Online"

		// TODO
	case router.PageCardsRecording:
		name = "page-cards-recording.tmpl"
		data.Title = "Spy Cards Online"

		// TODO
	case router.PageArcadeMain, router.PageCardsEditor:
		http.Error(w, "bad request filename", http.StatusBadRequest)
	default:
		panic("server: unhandled landing page: " + info.Page.String())
	}

	err = tmpl.ExecuteTemplate(w, name, &data)
	if err != nil {
		log.Printf("landing tmpl error %+v on %q", err, r.RequestURI)
	}
}
