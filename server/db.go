package server

import (
	"database/sql"
	"log"
	"time"

	// PostgreSQL database driver.
	_ "github.com/jackc/pgx/v4/stdlib"
)

var (
	begin func() (*sql.Tx, error)

	checkAllowedOrigin *sql.Stmt
	getAllowedOrigins  *sql.Stmt

	insertUserImage       *sql.Stmt
	getUserImage          *sql.Stmt
	getUserImages         *sql.Stmt
	getUserImageByHash    *sql.Stmt
	getCommunityPortraits *sql.Stmt
	getTanjerinUserImages *sql.Stmt

	insertIssueReport      *sql.Stmt
	insertAutomatedReport  *sql.Stmt
	getPortraitCount       *sql.Stmt
	getPortraitList        *sql.Stmt
	getReportCount         *sql.Stmt
	getReportList          *sql.Stmt
	updateReportDevComment *sql.Stmt

	getMatchCodePrefix          *sql.Stmt
	getCustomCardSet            *sql.Stmt
	getCustomGameModeCount      *sql.Stmt
	getCustomGameModeCategories *sql.Stmt
	getCustomGameModeList       *sql.Stmt
	refreshCustomGameModeList   *sql.Stmt
	getCustomGameModesForOwner  *sql.Stmt
	getCustomGameMode           *sql.Stmt
	getCustomGameModeSets       *sql.Stmt
	getCustomGameModeIDBySlug   *sql.Stmt
	insertCustomCard            *sql.Stmt
	createCustomCardSet         *sql.Stmt
	addCardToCustomSet          *sql.Stmt
	setLatestGameModeRevision   *sql.Stmt

	insertMatchRecording     *sql.Stmt
	getRecordingCount        *sql.Stmt
	getRecordingList         *sql.Stmt
	getMatchRecordingData    *sql.Stmt
	getRandomMatchRecording  *sql.Stmt
	decodedSpyCardsRecording *sql.Stmt

	getUserByID            *sql.Stmt
	getUserByLogin         *sql.Stmt
	getUserCurrentPassword *sql.Stmt
	updateUserPassword     *sql.Stmt
	getUserCount           *sql.Stmt
	getUserList            *sql.Stmt
	createUser             *sql.Stmt

	auditCompleteMatchmaking  *sql.Stmt
	auditMatchmakingPing      *sql.Stmt
	auditMatchmakingUsedRelay *sql.Stmt
	auditMatchmakingBegin     *sql.Stmt

	insertArcadeRecording        *sql.Stmt
	getArcadeRecordingCount      *sql.Stmt
	getArcadeRecordingList       *sql.Stmt
	getArcadeRecording           *sql.Stmt
	decodedArcadeRecording       *sql.Stmt
	getArcadeHighScoresRecent    *sql.Stmt
	getArcadeHighScoresWeekly    *sql.Stmt
	getArcadeHighScoresQuarterly *sql.Stmt
	getArcadeHighScoresAllTime   *sql.Stmt

	getCustomStages *sql.Stmt
	getCustomMusic  *sql.Stmt

	getHighScoreCards         *sql.Stmt
	deleteRecordingHighScores *sql.Stmt
	addRecordingHighScore     *sql.Stmt
	getHighScoreTables        *sql.Stmt
	getHighScoreEntries       *sql.Stmt

	getTestDataLastModified           *sql.Stmt
	getTestDataLegacyCustomCardSets   *sql.Stmt
	getTestDataLegacyMatchRecordings  *sql.Stmt
	getTestDataLegacyArcadeRecordings *sql.Stmt
)

func initDB(dsn string) {
	db, err := sql.Open("pgx", dsn)
	if err != nil {
		panic(err)
	}

	do := func(_ sql.Result, err error) {
		if err != nil {
			panic(err)
		}
	}

	do(db.Exec(`CREATE TABLE IF NOT EXISTS "user_accounts" (
	"id" BIGSERIAL NOT NULL PRIMARY KEY,
	"login_name" TEXT NOT NULL UNIQUE,
	"password" BYTEA NOT NULL,
	"session_reset" INT NOT NULL DEFAULT 0,
	"is_admin" BOOL NOT NULL DEFAULT FALSE
);`))
	do(db.Exec(`CREATE TABLE IF NOT EXISTS "allowed_origins" (
	"origin" TEXT NOT NULL PRIMARY KEY
);`))
	do(db.Exec(`CREATE TABLE IF NOT EXISTS "user_images" (
	"id" BIGSERIAL PRIMARY KEY,
	"hash" BYTEA NOT NULL,
	"data" BYTEA NOT NULL,
	"uploaded_by" INET[] NOT NULL,
	"uploaded_at" TIMESTAMPTZ NOT NULL DEFAULT NOW(),
	"whitelisted" BOOL NOT NULL DEFAULT FALSE,
	"tanjerin" BOOL DEFAULT NULL,
	"removed_reason" TEXT DEFAULT NULL,
	"data_type" BIGINT NOT NULL DEFAULT 0
);`))
	do(db.Exec(`CREATE TABLE IF NOT EXISTS "issue_reports" (
	"id" BIGSERIAL PRIMARY KEY,
	"uploaded_by" INET[] NOT NULL,
	"uploaded_at" TIMESTAMPTZ NOT NULL DEFAULT NOW(),
	"user_agent" TEXT NOT NULL,
	"version" TEXT NOT NULL,
	"message" TEXT NOT NULL,
	"stack" TEXT NOT NULL,
	"state" JSONB NOT NULL,
	"custom" TEXT NOT NULL,
	"user_comment" TEXT NOT NULL,
	"dev_comment" TEXT DEFAULT NULL,
	"cache_version" TEXT NOT NULL DEFAULT '',
	"log_tail" TEXT NOT NULL DEFAULT ''
);`))
	do(db.Exec(`CREATE TABLE IF NOT EXISTS "automated_reports" (
	"id" BIGSERIAL PRIMARY KEY,
	"uploaded_by" INET[] NOT NULL,
	"uploaded_at" TIMESTAMPTZ NOT NULL DEFAULT NOW(),
	"user_agent" TEXT NOT NULL,
	"content" BYTEA NOT NULL,
	"content_type" TEXT NOT NULL
);`))
	do(db.Exec(`CREATE TABLE IF NOT EXISTS "custom_game_mode_categories" (
	"id" BIGSERIAL PRIMARY KEY,
	"name" TEXT NOT NULL,
	"description" TEXT NOT NULL,
	"color_rgb" INTEGER NOT NULL,
	"order" BIGINT NOT NULL
);`))
	do(db.Exec(`CREATE TABLE IF NOT EXISTS "custom_game_modes" (
	"id" BIGSERIAL PRIMARY KEY,
	"name" TEXT NOT NULL,
	"client_slug" TEXT NOT NULL UNIQUE,
	"latest_revision" BIGINT NOT NULL DEFAULT 0,
	"owner" BIGINT NOT NULL REFERENCES "user_accounts"("id"),
	"category_id" BIGINT DEFAULT NULL REFERENCES "custom_game_mode_categories"("id"),
	"prevent" TEXT
);`))
	do(db.Exec(`CREATE TABLE IF NOT EXISTS "custom_card_sets" (
	"id" BIGSERIAL PRIMARY KEY,
	"mode_id" BIGINT NOT NULL REFERENCES "custom_game_modes" ("id"),
	"uploaded_by" INET[] NOT NULL,
	"uploaded_at" TIMESTAMPTZ NOT NULL DEFAULT NOW(),
	"revision" BIGINT NOT NULL,
	UNIQUE ("mode_id", "revision")
);`))
	do(db.Exec(`CREATE TABLE IF NOT EXISTS "match_code_prefixes" (
	"client" TEXT PRIMARY KEY,
	"prefix" CHAR(4) NOT NULL UNIQUE CHECK ("prefix" NOT LIKE '%[^0-9A-HJKMNP-TV-Z]%')
);`))
	do(db.Exec(`CREATE TABLE IF NOT EXISTS "match_recordings" (
	"id" BIGSERIAL PRIMARY KEY,
	"uploaded_by" INET[] NOT NULL,
	"uploaded_at" TIMESTAMPTZ NOT NULL DEFAULT NOW(),
	"hash" BYTEA NOT NULL,
	"data" BYTEA NOT NULL,
	"format_version" BIGINT NOT NULL,
	"version_major" BIGINT NOT NULL,
	"version_minor" BIGINT NOT NULL,
	"version_patch" BIGINT NOT NULL,
	"mode_prefix" TEXT NOT NULL,
	"mode_suffix" TEXT,
	"decode_failed_reason" TEXT DEFAULT 'not attempted'
);`))
	do(db.Exec(`CREATE TABLE IF NOT EXISTS "custom_cards" (
	"id" BIGSERIAL PRIMARY KEY,
	"code" BYTEA NOT NULL,
	"format_version" INT NOT NULL
		GENERATED ALWAYS AS (get_byte("code", 0)) STORED,
	CONSTRAINT "custom_cards_code_unique" EXCLUDE USING HASH ("code" WITH =)
);`))
	do(db.Exec(`CREATE TABLE IF NOT EXISTS "custom_card_set_cards" (
	"set_id" BIGINT NOT NULL REFERENCES "custom_card_sets"("id"),
	"card_id" BIGINT NOT NULL REFERENCES "custom_cards"("id"), 
	"order" INT NOT NULL,
	UNIQUE("set_id", "order")
);`))
	do(db.Exec(`CREATE TABLE IF NOT EXISTS "matchmaking_audit_log" (
	"id" BIGSERIAL PRIMARY KEY,
	"code" CHAR(24) NOT NULL,
	"client" TEXT NOT NULL,
	"created_at" TIMESTAMPTZ NOT NULL DEFAULT NOW(),
	"last_ping" TIMESTAMPTZ NOT NULL DEFAULT NOW(),
	"used_relay" BOOL NOT NULL DEFAULT FALSE,
	"ended" TIMESTAMPTZ DEFAULT NULL,
	"end_reason" INT DEFAULT NULL,
	"p1_addr" INET[] NOT NULL,
	"p1_ua" TEXT NOT NULL,
	"p2_addr" INET[] DEFAULT NULL,
	"p2_ua" TEXT DEFAULT NULL
);`))
	do(db.Exec(`DO $$
BEGIN
	BEGIN
		CREATE TYPE "arcade_game" AS ENUM ('MiteKnight', 'FlowerJourney');
	EXCEPTION WHEN duplicate_object THEN
	END;
END
$$;`))
	do(db.Exec(`CREATE TABLE IF NOT EXISTS "arcade_recordings" (
	"id" BIGSERIAL PRIMARY KEY,
	"uploaded_at" TIMESTAMPTZ NOT NULL DEFAULT NOW(),
	"uploaded_by" INET[] NOT NULL,
	"user_agent" TEXT NOT NULL,
	"data" BYTEA NOT NULL,
	"hash" BYTEA NOT NULL,
	"decode_failed_reason" TEXT DEFAULT 'not attempted',
	"game" "arcade_game",
	"player_name" CHAR(3),
	"score" BIGINT,
	"vanilla" BOOL
);`))
	do(db.Exec(`CREATE TABLE IF NOT EXISTS "custom_stages" (
	"id" BIGSERIAL PRIMARY KEY,
	"name" TEXT NOT NULL,
	"cid" BYTEA NOT NULL UNIQUE
);`))
	do(db.Exec(`CREATE TABLE IF NOT EXISTS "custom_music" (
	"id" BIGSERIAL PRIMARY KEY,
	"name" TEXT NOT NULL,
	"cid" BYTEA NOT NULL UNIQUE,
	"loop_start" REAL NOT NULL,
	"loop_end" REAL NOT NULL
);`))
	do(db.Exec(`CREATE TABLE IF NOT EXISTS "high_score_tables" (
	"id" BIGSERIAL PRIMARY KEY,
	"title" TEXT NOT NULL
);`))
	do(db.Exec(`CREATE TABLE IF NOT EXISTS "high_score_cards" (
	"table_id" BIGINT NOT NULL REFERENCES "high_score_tables"("id"),
	"card_id" BIGINT NOT NULL REFERENCES "custom_cards"("id"),
	PRIMARY KEY ("table_id", "card_id")
);`))
	do(db.Exec(`CREATE TABLE IF NOT EXISTS "high_score_card_instances" (
	"table_id" BIGINT NOT NULL,
	"card_id" BIGINT NOT NULL,
	"recording_id" BIGINT NOT NULL REFERENCES "match_recordings"("id"),
	"round_num" BIGINT NOT NULL,
	"by_host" BOOLEAN NOT NULL,
	"score" BIGINT NOT NULL,
	FOREIGN KEY ("table_id", "card_id") REFERENCES "high_score_cards"("table_id", "card_id"),
	PRIMARY KEY ("table_id", "card_id", "recording_id", "round_num", "by_host")
);`))
	do(db.Exec(`CREATE TABLE IF NOT EXISTS "lobby_users" (
	"id" BIGSERIAL PRIMARY KEY,
	"ip_addr" INET[] NOT NULL,
	"user_agent" TEXT NOT NULL,
	"display_name" TEXT NOT NULL, -- mutable
	"display_name_precis" TEXT NOT NULL, -- mutable
	"character" TEXT NOT NULL, -- mutable
	"color_rgb" INTEGER NOT NULL CHECK("color_rgb" >= 0 AND "color_rgb" < 16777216), -- mutable
	"session_id" CHAR(32) NOT NULL UNIQUE,
	"created_at" TIMESTAMPTZ NOT NULL DEFAULT NOW(),
	"last_seen" TIMESTAMPTZ NOT NULL DEFAULT NOW() -- mutable
);`))
	do(db.Exec(`CREATE TABLE IF NOT EXISTS "lobby_rooms" (
	"id" BIGSERIAL PRIMARY KEY,
	"name" TEXT NOT NULL UNIQUE,
	"created_by" BIGINT NOT NULL REFERENCES "lobby_users"("id"),
	"created_at" TIMESTAMPTZ NOT NULL DEFAULT NOW(),
	"last_used" TIMESTAMPTZ NOT NULL DEFAULT NOW(), -- mutable
	"join_password" BYTEA, -- bcrypt
	"member_limit" INTEGER NOT NULL DEFAULT 100,
	"last_revision" BIGINT NOT NULL DEFAULT 0 -- mutable
);`))
	do(db.Exec(`CREATE TABLE IF NOT EXISTS "lobby_room_users" (
	"room_id" BIGINT NOT NULL REFERENCES "lobby_rooms"("id"),
	"user_id" BIGINT NOT NULL REFERENCES "lobby_users"("id"),
	"first_joined" TIMESTAMPTZ NOT NULL DEFAULT NOW(),
	"last_joined" TIMESTAMPTZ NOT NULL DEFAULT NOW(), -- mutable
	"left_room" TIMESTAMPTZ DEFAULT NULL, -- mutable
	PRIMARY KEY ("room_id", "user_id")
);`))
	do(db.Exec(`CREATE TABLE IF NOT EXISTS "lobby_messages" (
	"id" BIGSERIAL PRIMARY KEY,
	"room_id" BIGINT NOT NULL REFERENCES "lobby_rooms"("id"),
	"user_id" BIGINT NOT NULL REFERENCES "lobby_users"("id"),
	"special_type" TEXT DEFAULT NULL,
	"message" TEXT NOT NULL,
	"posted_at" TIMESTAMPTZ NOT NULL DEFAULT NOW(),
	"withdrawn_at" TIMESTAMPTZ DEFAULT NULL, -- mutable
	"last_revision" BIGINT NOT NULL -- mutable
);`))
	do(db.Exec(`CREATE TABLE IF NOT EXISTS "lobby_challenges" (
	"id" BIGSERIAL PRIMARY KEY,
	"room_id" BIGINT NOT NULL REFERENCES "lobby_rooms"("id"),
	"user_id" BIGINT NOT NULL REFERENCES "lobby_users"("id"),
	"target_id" BIGINT REFERENCES "lobby_users"("id"), -- mutable
	"set_id" BIGINT REFERENCES "custom_card_sets"("id"),
	"custom_cards" BYTEA,
	"variant" INTEGER NOT NULL,
	"posted_at" TIMESTAMPTZ NOT NULL DEFAULT NOW(),
	"accepted_at" TIMESTAMPTZ DEFAULT NULL, -- mutable
	"withdrawn_at" TIMESTAMPTZ DEFAULT NULL, -- mutable
	"recording_id" BIGINT DEFAULT NULL REFERENCES "match_recordings"("id"), -- mutable
	"last_revision" BIGINT NOT NULL, -- mutable
	CHECK ("set_id" IS NULL OR "custom_cards" IS NULL)
);`))
	do(db.Exec(`CREATE MATERIALIZED VIEW IF NOT EXISTS "custom_game_modes_latest" ("id", "client_slug", "prefix", "name0401", "name", "revision_count", "mode_data", "first_card", "category_color", "category_id") AS
SELECT m."id", m."client_slug", "prefix", COALESCE(m."prevent", m."name") || ' and Prevent ' || COALESCE(m."prevent", m."name") || ' Expansion', m."name", m."latest_revision",
	(SELECT "code" FROM "custom_cards" cc INNER JOIN "custom_card_set_cards" ccsc ON cc."id" = ccsc."card_id" INNER JOIN "custom_card_sets" s ON ccsc."set_id" = s."id" WHERE s."mode_id" = m."id" ORDER BY s."id" DESC, ccsc."order" ASC LIMIT 1),
	(SELECT "code" FROM "custom_cards" cc INNER JOIN "custom_card_set_cards" ccsc ON cc."id" = ccsc."card_id" INNER JOIN "custom_card_sets" s ON ccsc."set_id" = s."id" WHERE s."mode_id" = m."id" ORDER BY s."id" DESC, ccsc."order" ASC LIMIT 1 OFFSET 1),
	mc."color_rgb", mc."id"
  FROM "custom_game_modes" m
  LEFT JOIN "match_code_prefixes" p ON p."client" = '-' || m."client_slug"
  LEFT JOIN "custom_game_mode_categories" mc ON mc."id" = m."category_id"
  WITH NO DATA;`))
	do(db.Exec(`REFRESH MATERIALIZED VIEW "custom_game_modes_latest";`))

	prepare := func(query string) *sql.Stmt {
		stmt, err := db.Prepare(query)
		if err != nil {
			panic(err)
		}

		return stmt
	}

	begin = db.Begin
	checkAllowedOrigin = prepare(`SELECT EXISTS(SELECT 1 FROM "allowed_origins" WHERE "origin" = $1::TEXT);`)
	getAllowedOrigins = prepare(`SELECT array_agg("origin") FROM "allowed_origins";`)
	insertUserImage = prepare(`INSERT INTO "user_images" ("uploaded_by", "hash", "data", "data_type") VALUES (('{' || $1::TEXT || '}')::INET[], $2::BYTEA, $3::BYTEA, $4::BIGINT) RETURNING "id";`)
	getUserImage = prepare(`SELECT "hash", "data" FROM "user_images" WHERE "id" = $1::BIGINT AND "data_type" = $2::BIGINT AND "removed_reason" IS NULL;`)
	getUserImages = prepare(`SELECT "id", "hash", "data" FROM "user_images" WHERE "id" = ANY($1::BIGINT[]) AND "data_type" = $2::BIGINT AND "removed_reason" IS NULL;`)
	getUserImageByHash = prepare(`SELECT "id" FROM "user_images" WHERE "hash" = $1::BYTEA AND "data_type" = $2::BIGINT;`)
	getCommunityPortraits = prepare(`SELECT "hash", "id" FROM "user_images" WHERE "whitelisted" AND "data_type" = 0 AND "removed_reason" IS NULL ORDER BY "id" ASC;`)
	getTanjerinUserImages = prepare(`SELECT "hash", "id", "tanjerin" FROM "user_images" WHERE "tanjerin" IS NOT NULL AND "data_type" = 0 AND "removed_reason" IS NULL ORDER BY "id" ASC;`)
	insertIssueReport = prepare(`INSERT INTO "issue_reports" ("uploaded_by", "user_agent", "version", "cache_version", "message", "stack", "state", "custom", "user_comment", "log_tail") VALUES (('{' || $1::TEXT || '}')::INET[], $2::TEXT, $3::TEXT, $4::TEXT, $5::TEXT, $6::TEXT, $7::TEXT::JSONB, $8::TEXT, $9::TEXT, $10::TEXT);`)
	insertAutomatedReport = prepare(`INSERT INTO "automated_reports" ("uploaded_by", "user_agent", "content", "content_type") VALUES (('{' || $1::TEXT || '}')::INET[], $2::TEXT, $3::BYTEA, $4::TEXT);`)
	getPortraitCount = prepare(`SELECT COUNT(*) FROM "user_images";`)
	getPortraitList = prepare(`SELECT "id", "hash", "whitelisted", "tanjerin" FROM "user_images" WHERE "removed_reason" IS NULL ORDER BY "id" DESC LIMIT $2::INT OFFSET $1::INT;`)
	getReportCount = prepare(`SELECT COUNT(*) FROM "issue_reports";`)
	getReportList = prepare(`SELECT "id", "version", "cache_version", "user_agent", "message", "stack", "state", "custom", "user_comment", "dev_comment", "log_tail" FROM "issue_reports" ORDER BY "id" DESC LIMIT $2::INT OFFSET $1::INT;`)
	updateReportDevComment = prepare(`UPDATE "issue_reports" SET "dev_comment" = $2::TEXT WHERE "id" = $1::BIGINT;`)
	getMatchCodePrefix = prepare(`SELECT "prefix" FROM "match_code_prefixes" WHERE "client" = $1::TEXT`)
	getCustomCardSet = prepare(`SELECT m."name", array_to_string(ARRAY(SELECT replace(encode("code", 'base64'), E'\n', '') FROM "custom_cards" cc INNER JOIN "custom_card_set_cards" ccsc ON cc."id" = ccsc."card_id" WHERE ccsc."set_id" = s."id" ORDER BY ccsc."order" ASC), ',') "cards", s."revision" FROM "custom_card_sets" s
 INNER JOIN "custom_game_modes" m ON s."mode_id" = m."id"
 WHERE m."client_slug" = $1::TEXT
   AND (s."revision" = $2::INT OR $2::INT IS NULL)
 ORDER BY s."id" DESC LIMIT 1;`)
	getCustomGameModeCount = prepare(`SELECT COUNT(*) FROM "custom_game_modes";`)
	getCustomGameModeCategories = prepare(`SELECT mc."id", mc."name", mc."description", mc."color_rgb" FROM "custom_game_mode_categories" mc ORDER BY mc."order" ASC;`)
	getCustomGameModeList = prepare(`SELECT "id", "client_slug", "prefix", CASE WHEN EXTRACT(MONTH FROM NOW()) = 4 AND EXTRACT(DAY FROM NOW()) = 1 THEN "name0401" ELSE "name" END "name", "revision_count", "mode_data", "first_card", "category_color", "category_id" FROM "custom_game_modes_latest"
 ORDER BY "id" ASC LIMIT $2::INT OFFSET $1::INT;`)
	refreshCustomGameModeList = prepare(`REFRESH MATERIALIZED VIEW CONCURRENTLY "custom_game_modes_latest";`)
	getCustomGameModesForOwner = prepare(`SELECT "id", "client_slug", "name" FROM "custom_game_modes" WHERE "owner" = $1::BIGINT ORDER BY "id" ASC;`)
	getCustomGameMode = prepare(`SELECT "client_slug", "name", "latest_revision", "owner", "prefix" FROM "custom_game_modes"
  LEFT JOIN "match_code_prefixes" ON "client" = '-' || "client_slug"
 WHERE "id" = $1::BIGINT;`)
	getCustomGameModeSets = prepare(`SELECT "revision", array_to_string(ARRAY(SELECT replace(encode("code", 'base64'), E'\n', '') FROM "custom_cards" cc INNER JOIN "custom_card_set_cards" ccsc ON cc."id" = ccsc."card_id" WHERE ccsc."set_id" = s."id" ORDER BY ccsc."order" ASC), ',') "cards" FROM "custom_card_sets" s
 WHERE "mode_id" = $1::BIGINT
 ORDER BY "revision" ASC;`)
	getCustomGameModeIDBySlug = prepare(`SELECT "id", "name" FROM "custom_game_modes" WHERE "client_slug" = $1::TEXT;`)
	insertCustomCard = prepare(`WITH "existing" AS (SELECT "id" FROM "custom_cards" WHERE "code" = $1::BYTEA), "inserted" AS (INSERT INTO "custom_cards" ("code") SELECT $1::BYTEA WHERE NOT EXISTS (SELECT * FROM "existing") ON CONFLICT DO NOTHING RETURNING "id") SELECT COALESCE((SELECT "id" FROM "existing"), (SELECT "id" FROM "inserted"), (SELECT "id" FROM "custom_cards" WHERE "code" = $1::BYTEA));`)
	createCustomCardSet = prepare(`INSERT INTO "custom_card_sets" ("mode_id", "uploaded_by", "revision") VALUES ($1::BIGINT, ('{' || $2::TEXT || '}')::INET[], $3::BIGINT) RETURNING "id";`)
	addCardToCustomSet = prepare(`INSERT INTO "custom_card_set_cards" ("set_id", "card_id", "order") VALUES ($1::BIGINT, $2::BIGINT, $3::INT);`)
	setLatestGameModeRevision = prepare(`UPDATE "custom_game_modes" SET "latest_revision" = $2::BIGINT WHERE "id" = $1::BIGINT;`)
	insertMatchRecording = prepare(`INSERT INTO "match_recordings" ("uploaded_by", "hash", "data", "format_version", "version_major", "version_minor", "version_patch", "mode_prefix", "mode_suffix") VALUES (('{' || $1::TEXT || '}')::INET[], $2::BYTEA, $3::BYTEA, $4::BIGINT, $5::BIGINT, $6::BIGINT, $7::BIGINT, $8::TEXT, $9::TEXT) RETURNING "id";`)
	getRecordingCount = prepare(`SELECT COUNT(*) FROM "match_recordings";`)
	getRecordingList = prepare(`SELECT "id", "uploaded_at", "hash", "version_major", "version_minor", "version_patch", "mode_prefix", "mode_suffix", "decode_failed_reason" FROM "match_recordings" ORDER BY "id" DESC LIMIT $2::INT OFFSET $1::INT;`)
	getMatchRecordingData = prepare(`SELECT "hash", "data", "mode_prefix", "mode_suffix" FROM "match_recordings" WHERE "id" = $1::BIGINT;`)
	getRandomMatchRecording = prepare(`SELECT "id", "hash" FROM "match_recordings" WHERE "mode_prefix" <> 'custom' AND "id" <> ALL ($1::BIGINT[]) AND ("mode_prefix" = $2::TEXT OR $2::TEXT IS NULL) AND (NOT $3::BOOLEAN OR "uploaded_at" > NOW() - INTERVAL '1 month') AND "decode_failed_reason" IS NULL ORDER BY RANDOM() LIMIT 1;`)
	decodedSpyCardsRecording = prepare(`UPDATE "match_recordings" SET "decode_failed_reason" = $2::TEXT WHERE "id" = $1::BIGINT;`)
	getUserByID = prepare(`SELECT "login_name", "session_reset", "is_admin" FROM "user_accounts" WHERE "id" = $1::BIGINT;`)
	getUserByLogin = prepare(`SELECT "id", "password", "session_reset", "is_admin" FROM "user_accounts" WHERE "login_name" = $1::TEXT;`)
	getUserCurrentPassword = prepare(`SELECT "password" FROM "user_accounts" WHERE "id" = $1::BIGINT;`)
	updateUserPassword = prepare(`UPDATE "user_accounts" SET "password" = $3::BYTEA, "session_reset" = "session_reset" + 1 WHERE "id" = $1::BIGINT AND "password" = $2::BYTEA;`)
	getUserCount = prepare(`SELECT COUNT(*) FROM "user_accounts";`)
	getUserList = prepare(`SELECT "id", "login_name", "session_reset", "is_admin" FROM "user_accounts" ORDER BY "id" ASC LIMIT $2::INT OFFSET $1::INT;`)
	createUser = prepare(`INSERT INTO "user_accounts" ("login_name", "password") VALUES ($1::TEXT, $2::BYTEA) RETURNING "id";`)
	auditCompleteMatchmaking = prepare(`UPDATE "matchmaking_audit_log" SET "end_reason" = $2::INT, "ended" = NOW(), "p2_addr" = ('{' || $3::TEXT || '}')::INET[], "p2_ua" = $4::TEXT WHERE "id" = $1::BIGINT;`)
	auditMatchmakingPing = prepare(`UPDATE "matchmaking_audit_log" SET "last_ping" = NOW() WHERE "id" = $1::BIGINT;`)
	auditMatchmakingUsedRelay = prepare(`UPDATE "matchmaking_audit_log" SET "used_relay" = TRUE WHERE "id" = $1::BIGINT;`)
	auditMatchmakingBegin = prepare(`INSERT INTO "matchmaking_audit_log" ("code", "client", "p1_addr", "p1_ua") VALUES ($1::TEXT, $2::TEXT, ('{' || $3::TEXT || '}')::INET[], $4::TEXT) RETURNING "id";`)
	insertArcadeRecording = prepare(`INSERT INTO "arcade_recordings" ("uploaded_by", "user_agent", "data", "hash") VALUES (('{' || $1::TEXT || '}')::INET[], $2::TEXT, $3::BYTEA, $4::BYTEA) RETURNING "id";`)
	getArcadeRecordingCount = prepare(`SELECT COUNT(*) FROM "arcade_recordings";`)
	getArcadeRecordingList = prepare(`SELECT "id", "uploaded_at", "hash", "game", "player_name", "score", "vanilla", "decode_failed_reason" FROM "arcade_recordings" ORDER BY "id" DESC LIMIT $2::INT OFFSET $1::INT;`)
	getArcadeRecording = prepare(`SELECT "hash", "data" FROM "arcade_recordings" WHERE "id" = $1::BIGINT;`)
	decodedArcadeRecording = prepare(`UPDATE "arcade_recordings" SET "decode_failed_reason" = $2::TEXT, "game" = $3::arcade_game, "player_name" = $4::CHAR(3), "score" = $5::BIGINT, "vanilla" = $6::BOOL WHERE "id" = $1::BIGINT;`)

	const arcadeHighScoresFields = `"id", "hash", "score", "player_name"`

	getArcadeHighScoresRecent = prepare(`SELECT ` + arcadeHighScoresFields + ` FROM "arcade_recordings" WHERE "uploaded_at" > NOW() - INTERVAL '14 days' AND "decode_failed_reason" IS NULL AND "vanilla" AND "score" IS NOT NULL AND "game" = $1::arcade_game ORDER BY "id" DESC LIMIT $3::INT OFFSET $2::INT;`)
	getArcadeHighScoresWeekly = prepare(`SELECT ` + arcadeHighScoresFields + ` FROM "arcade_recordings" WHERE "uploaded_at" > DATE_TRUNC('day', NOW()) - INTERVAL '7 days' AND "decode_failed_reason" IS NULL AND "vanilla" AND "score" IS NOT NULL AND "game" = $1::arcade_game ORDER BY "score" desc, "id" ASC LIMIT $3::INT OFFSET $2::INT;`)
	getArcadeHighScoresQuarterly = prepare(`SELECT ` + arcadeHighScoresFields + ` FROM "arcade_recordings" WHERE "uploaded_at" > DATE_TRUNC('month', NOW()) - INTERVAL '3 months' AND "decode_failed_reason" IS NULL AND "vanilla" AND "score" IS NOT NULL AND "game" = $1::arcade_game ORDER BY "score" desc, "id" ASC LIMIT $3::INT OFFSET $2::INT;`)
	getArcadeHighScoresAllTime = prepare(`SELECT ` + arcadeHighScoresFields + ` FROM "arcade_recordings" WHERE "decode_failed_reason" IS NULL AND "vanilla" AND "score" IS NOT NULL AND "game" = $1::arcade_game ORDER BY "score" desc, "id" ASC LIMIT $3::INT OFFSET $2::INT;`)

	getCustomStages = prepare(`SELECT "id", "name", "cid" FROM "custom_stages" ORDER BY "id" ASC;`)
	getCustomMusic = prepare(`SELECT "id", "name", "cid", "loop_start", "loop_end" FROM "custom_music" ORDER BY "id" ASC;`)

	getHighScoreCards = prepare(`SELECT hsc."table_id", hsc."card_id", cc."code" FROM "high_score_cards" hsc INNER JOIN "custom_cards" cc ON cc."id" = hsc."card_id";`)
	deleteRecordingHighScores = prepare(`DELETE FROM "high_score_card_instances" WHERE "recording_id" = $1::BIGINT;`)
	addRecordingHighScore = prepare(`INSERT INTO "high_score_card_instances" ("table_id", "card_id", "recording_id", "round_num", "by_host", "score") VALUES ($1::BIGINT, $2::BIGINT, $3::BIGINT, $4::BIGINT, $5::BOOLEAN, $6::BIGINT);`)
	getHighScoreTables = prepare(`SELECT hs."id", hs."title", (SELECT COUNT(*) FROM (SELECT DISTINCT hsci."recording_id", hsci."round_num", hsci."by_host" FROM "high_score_card_instances" hsci INNER JOIN "match_recordings" mr ON mr."id" = hsci."recording_id" WHERE hsci."table_id" = hs."id" AND mr."decode_failed_reason" IS NULL) hsciu) "entries" FROM "high_score_tables" hs WHERE $1::BIGINT IS NULL OR $1::BIGINT = hs."id" ORDER BY hs."id" ASC;`)
	getHighScoreEntries = prepare(`SELECT DISTINCT hsci."recording_id", mr."hash", hsci."round_num", hsci."by_host", hsci."score", mr."uploaded_at", mr."version_major", mr."version_minor", mr."version_patch", mr."mode_prefix", mr."mode_suffix" FROM "high_score_card_instances" hsci INNER JOIN "match_recordings" mr ON hsci."recording_id" = mr."id" WHERE hsci."table_id" = $1::BIGINT AND mr."decode_failed_reason" IS NULL ORDER BY hsci."score" DESC, hsci."recording_id" ASC, hsci."round_num" ASC, hsci."by_host" DESC LIMIT 1000;`)

	getTestDataLastModified = prepare(`SELECT (SELECT MAX("uploaded_at") FROM "custom_card_sets") "last_set", (SELECT MAX("uploaded_at") FROM "match_recordings" WHERE "decode_failed_reason" IS NULL) "last_match", (SELECT MAX("uploaded_at") FROM "arcade_recordings" WHERE "decode_failed_reason" IS NULL) "last_arcade";`)
	getTestDataLegacyCustomCardSets = prepare(`SELECT cgm."client_slug", ccs."revision", ccs."uploaded_at", (SELECT STRING_AGG(ENCODE(cc."code", 'base64'), ',' ORDER BY ccsc."order" ASC) "data" FROM "custom_card_set_cards" ccsc INNER JOIN "custom_cards" cc ON cc."id" = ccsc."card_id" WHERE ccsc."set_id" = ccs."id") FROM "custom_card_sets" ccs INNER JOIN "custom_game_modes" cgm ON ccs."mode_id" = cgm."id" ORDER BY ccs."id" ASC;`)
	getTestDataLegacyMatchRecordings = prepare(`SELECT "id", "hash", "uploaded_at", "data" FROM "match_recordings" WHERE "decode_failed_reason" IS NULL ORDER BY "id" ASC;`)
	getTestDataLegacyArcadeRecordings = prepare(`SELECT "id", "hash", "uploaded_at", "data" FROM "arcade_recordings" WHERE "decode_failed_reason" IS NULL ORDER BY "id" ASC;`)

	if false {
		// revalidate all recordings
		go func() {
			var maxRecordingID int64
			if err := db.QueryRow(`SELECT MAX("id") FROM "match_recordings";`).Scan(&maxRecordingID); err != nil {
				panic(err)
			}

			for id := int64(1); id <= maxRecordingID; id++ {
				log.Println("handling recording", id)

				ch := make(chan struct{}, 1)

				go func(id int64) {
					decodeSpyCardsRecording(id)
					ch <- struct{}{}
				}(id)

				select {
				case <-ch:
				case <-time.After(time.Minute):
					log.Println("timed out")
				}
			}
			log.Println("done handling recordings")
		}()
	}
}
