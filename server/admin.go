package server

import (
	"crypto/rand"
	"database/sql"
	"errors"
	"io"
	"log"
	"math/big"
	"net/http"
	"net/url"
	"sort"
	"strconv"
	"strings"
	"time"

	"git.lubar.me/ben/spy-cards/format"
	"github.com/davecgh/go-spew/spew"
	"golang.org/x/crypto/bcrypt"
)

var adminRouter http.ServeMux

func adminMiddleware(w http.ResponseWriter, r *http.Request) {
	w.Header().Add("Cache-Control", "no-store")
	w.Header().Set("X-Robots-Tag", "noindex")

	u, err := getCurrentUser(&r)
	if err != nil {
		log.Println("gcu admin", err)
		http.Error(w, "database error", http.StatusInternalServerError)

		return
	}

	if u == nil {
		http.SetCookie(w, &http.Cookie{
			Name:     "spy_cards_login_redirect",
			Value:    r.URL.RequestURI(),
			Path:     "/spy-cards/",
			SameSite: http.SameSiteStrictMode,
			HttpOnly: true,
			Secure:   true,
		})
		http.Redirect(w, r, "/spy-cards/log-in", http.StatusFound)

		return
	}

	if !u.IsAdmin {
		http.NotFound(w, r)

		return
	}

	adminRouter.ServeHTTP(w, r)
}

func adminLanding(w http.ResponseWriter, r *http.Request) {
	if r.URL.Path != "/spy-cards/admin/" {
		http.NotFound(w, r)

		return
	}

	var data struct {
		MatchCodes []string
	}

	sessions.Lock()

	for k := range sessions.m {
		data.MatchCodes = append(data.MatchCodes, format.Encode32Space(k[:]))
	}

	sessions.Unlock()

	sort.Strings(data.MatchCodes)

	w.Header().Add("Content-Type", "text/html")
	_ = tmpl.ExecuteTemplate(w, "admin-landing.tmpl", &data)
}

func adminPortraits(w http.ResponseWriter, r *http.Request) {
	type PortraitData struct {
		ID        format.FileID
		ImageURL  string
		Community bool
		Tanjerin  sql.NullBool
	}

	var data struct {
		Portraits   []PortraitData
		CurrentPage int
		MaxPage     int
		TotalCount  int
	}

	dbDone := timing(w, "db")

	err := getPortraitCount.QueryRow().Scan(&data.TotalCount)
	if err != nil {
		http.Error(w, "getting portrait count: "+err.Error(), http.StatusInternalServerError)

		return
	}

	data.MaxPage = (data.TotalCount + 249) / 250

	data.CurrentPage, err = strconv.Atoi(r.FormValue("page"))
	if err != nil || data.CurrentPage <= 0 {
		data.CurrentPage = 1
	}

	rows, err := getPortraitList.Query((data.CurrentPage-1)*250, 250)
	if err != nil {
		http.Error(w, "getting portraits: "+err.Error(), http.StatusInternalServerError)

		return
	}
	defer rows.Close()

	for rows.Next() {
		var (
			portrait PortraitData
			hash     []byte
		)

		if err = rows.Scan(&portrait.ID.ID, &hash, &portrait.Community, &portrait.Tanjerin); err != nil {
			http.Error(w, "getting portraits: "+err.Error(), http.StatusInternalServerError)

			return
		}

		copy(portrait.ID.Hash[:], hash)

		b, _ := portrait.ID.MarshalText()
		portrait.ImageURL = "../user-img/" + string(b) + ".webp"
		data.Portraits = append(data.Portraits, portrait)
	}

	if err = rows.Err(); err != nil {
		http.Error(w, "getting portraits: "+err.Error(), http.StatusInternalServerError)

		return
	}

	dbDone()

	w.Header().Add("Content-Type", "text/html")
	_ = tmpl.ExecuteTemplate(w, "admin-portraits.tmpl", &data)
}

func adminReports(w http.ResponseWriter, r *http.Request) {
	if r.Method == http.MethodPost {
		csrfKey, err := getCSRFKey(w, r, false)
		if err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)

			return
		}

		if !verifyCSRFToken(csrfKey, r.FormValue("csrf-token")) {
			http.Error(w, "request validation failed (try again)", http.StatusBadRequest)

			return
		}

		_, err = updateReportDevComment.Exec(r.FormValue("id"), r.FormValue("comment"))
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)

			return
		}

		http.Redirect(w, r, r.URL.String(), http.StatusFound)

		return
	}

	type ReportData struct {
		ID           int64
		Version      string
		CacheVersion string
		UserAgent    string
		Message      string
		Stack        string
		State        string
		Custom       string
		UserComment  string
		DevComment   sql.NullString
		LogTail      string
	}

	var data struct {
		Reports     []ReportData
		CurrentPage int
		MaxPage     int
		TotalCount  int
		CSRFToken   string
	}

	csrfKey, err := getCSRFKey(w, r, true)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)

		return
	}

	data.CSRFToken, err = createCSRFToken(csrfKey)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)

		return
	}

	dbDone := timing(w, "db")

	err = getReportCount.QueryRow().Scan(&data.TotalCount)
	if err != nil {
		http.Error(w, "getting report count: "+err.Error(), http.StatusInternalServerError)

		return
	}

	data.MaxPage = (data.TotalCount + 24) / 25

	data.CurrentPage, err = strconv.Atoi(r.FormValue("page"))
	if err != nil || data.CurrentPage <= 0 {
		data.CurrentPage = 1
	}

	rows, err := getReportList.Query((data.CurrentPage-1)*25, 25)
	if err != nil {
		http.Error(w, "getting reports: "+err.Error(), http.StatusInternalServerError)

		return
	}
	defer rows.Close()

	for rows.Next() {
		var report ReportData
		if err = rows.Scan(&report.ID, &report.Version, &report.CacheVersion, &report.UserAgent, &report.Message, &report.Stack, &report.State, &report.Custom, &report.UserComment, &report.DevComment, &report.LogTail); err != nil {
			http.Error(w, "getting reports: "+err.Error(), http.StatusInternalServerError)

			return
		}

		data.Reports = append(data.Reports, report)
	}

	if err = rows.Err(); err != nil {
		http.Error(w, "getting reports: "+err.Error(), http.StatusInternalServerError)

		return
	}

	dbDone()

	w.Header().Add("Content-Type", "text/html")
	_ = tmpl.ExecuteTemplate(w, "admin-reports.tmpl", &data)
}

func adminRecordings(w http.ResponseWriter, r *http.Request) {
	if r.Method == http.MethodPost {
		csrfKey, err := getCSRFKey(w, r, false)
		if err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)

			return
		}

		if !verifyCSRFToken(csrfKey, r.FormValue("csrf-token")) {
			http.Error(w, "request validation failed (try again)", http.StatusBadRequest)

			return
		}

		id, err := strconv.ParseInt(r.PostFormValue("retry"), 10, 64)
		if err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)

			return
		}

		decodeSpyCardsRecording(id)

		http.Redirect(w, r, r.RequestURI, http.StatusFound)

		return
	}

	type RecordingData struct {
		ID           format.FileID
		UploadedAt   time.Time
		VersionMajor int64
		VersionMinor int64
		VersionPatch int64
		ModePrefix   string
		ModeSuffix   sql.NullString
		Failed       sql.NullString
		Code         string
	}

	var data struct {
		Recordings  []RecordingData
		CurrentPage int
		MaxPage     int
		TotalCount  int
		CSRFToken   string
	}

	dbDone := timing(w, "db")

	err := getRecordingCount.QueryRow().Scan(&data.TotalCount)
	if err != nil {
		http.Error(w, "getting recording count: "+err.Error(), http.StatusInternalServerError)

		return
	}

	data.MaxPage = (data.TotalCount + 249) / 250

	data.CurrentPage, err = strconv.Atoi(r.FormValue("page"))
	if err != nil || data.CurrentPage <= 0 {
		data.CurrentPage = 1
	}

	rows, err := getRecordingList.Query((data.CurrentPage-1)*250, 250)
	if err != nil {
		http.Error(w, "getting recordings: "+err.Error(), http.StatusInternalServerError)

		return
	}
	defer rows.Close()

	for rows.Next() {
		var (
			recording RecordingData
			hash      []byte
		)

		if err = rows.Scan(&recording.ID.ID, &recording.UploadedAt, &hash, &recording.VersionMajor, &recording.VersionMinor, &recording.VersionPatch, &recording.ModePrefix, &recording.ModeSuffix, &recording.Failed); err != nil {
			http.Error(w, "getting recordings: "+err.Error(), http.StatusInternalServerError)

			return
		}

		copy(recording.ID.Hash[:], hash)

		code, err := recording.ID.MarshalText()
		if err != nil {
			http.Error(w, "marshaling recording ID: "+err.Error(), http.StatusInternalServerError)

			return
		}

		recording.Code = string(code)

		data.Recordings = append(data.Recordings, recording)
	}

	if err = rows.Err(); err != nil {
		http.Error(w, "getting recordings: "+err.Error(), http.StatusInternalServerError)

		return
	}

	dbDone()

	csrfKey, err := getCSRFKey(w, r, true)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)

		return
	}

	data.CSRFToken, err = createCSRFToken(csrfKey)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)

		return
	}

	w.Header().Add("Content-Type", "text/html")
	_ = tmpl.ExecuteTemplate(w, "admin-recordings.tmpl", &data)
}

func adminModes(w http.ResponseWriter, r *http.Request) {
	var data struct {
		Modes       []GameMode
		CurrentPage int
		MaxPage     int
		TotalCount  int
	}

	dbDone := timing(w, "db")

	err := getCustomGameModeCount.QueryRow().Scan(&data.TotalCount)
	if err != nil {
		http.Error(w, "getting game mode count: "+err.Error(), http.StatusInternalServerError)

		return
	}

	data.MaxPage = (data.TotalCount + 49) / 50

	data.CurrentPage, err = strconv.Atoi(r.FormValue("page"))
	if err != nil || data.CurrentPage <= 0 {
		data.CurrentPage = 1
	}

	rows, err := getCustomGameModeList.Query((data.CurrentPage-1)*50, 50)
	if err != nil {
		http.Error(w, "getting game modes: "+err.Error(), http.StatusInternalServerError)

		return
	}
	defer rows.Close()

	for rows.Next() {
		var (
			mode          GameMode
			categoryID    sql.NullInt64
			categoryColor sql.NullInt32
		)

		if err = rows.Scan(&mode.ID, &mode.Client, &mode.Prefix, &mode.Name, &mode.Revisions, &mode.ModeData, &mode.FirstCard, &categoryColor, &categoryID); err != nil {
			http.Error(w, "getting game modes: "+err.Error(), http.StatusInternalServerError)

			return
		}

		data.Modes = append(data.Modes, mode)
	}

	if err = rows.Err(); err != nil {
		http.Error(w, "getting game modes: "+err.Error(), http.StatusInternalServerError)

		return
	}

	dbDone()

	w.Header().Add("Content-Type", "text/html")
	_ = tmpl.ExecuteTemplate(w, "admin-modes.tmpl", &data)
}

func adminUsers(w http.ResponseWriter, r *http.Request) {
	var data struct {
		Users       []User
		CurrentPage int
		MaxPage     int
		TotalCount  int
	}

	dbDone := timing(w, "db")

	err := getUserCount.QueryRow().Scan(&data.TotalCount)
	if err != nil {
		http.Error(w, "getting user count: "+err.Error(), http.StatusInternalServerError)

		return
	}

	data.MaxPage = (data.TotalCount + 49) / 50

	data.CurrentPage, err = strconv.Atoi(r.FormValue("page"))
	if err != nil || data.CurrentPage <= 0 {
		data.CurrentPage = 1
	}

	rows, err := getUserList.Query((data.CurrentPage-1)*50, 50)
	if err != nil {
		http.Error(w, "getting users: "+err.Error(), http.StatusInternalServerError)

		return
	}
	defer rows.Close()

	for rows.Next() {
		var user User
		if err = rows.Scan(&user.ID, &user.LoginName, &user.SessionReset, &user.IsAdmin); err != nil {
			http.Error(w, "getting users: "+err.Error(), http.StatusInternalServerError)

			return
		}

		data.Users = append(data.Users, user)
	}

	if err = rows.Err(); err != nil {
		http.Error(w, "getting users: "+err.Error(), http.StatusInternalServerError)

		return
	}

	dbDone()

	w.Header().Add("Content-Type", "text/html")
	_ = tmpl.ExecuteTemplate(w, "admin-users.tmpl", &data)
}

func adminNewUser(w http.ResponseWriter, r *http.Request) {
	var data struct {
		Error     string
		Username  string
		Password  string
		CSRFToken string
	}

	data.Username = r.PostFormValue("username")
	data.Password = r.PostFormValue("password")

	if r.Method == http.MethodPost {
		csrfKey, err := getCSRFKey(w, r, false)
		if err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)

			return
		}

		if !verifyCSRFToken(csrfKey, r.FormValue("csrf-token")) {
			http.Error(w, "request validation failed (try again)", http.StatusBadRequest)

			return
		}

		switch {
		case data.Username == "":
			data.Error = "Missing username."
		case data.Password == "":
			data.Error = "Missing password."
		case url.QueryEscape(data.Username) != data.Username, strings.ToLower(data.Username) != data.Username:
			data.Error = "Invalid username."
		}

		if data.Error == "" {
			var userID int64

			cryptPass, err := bcrypt.GenerateFromPassword([]byte(data.Password), bcrypt.DefaultCost)
			if err == nil {
				err = createUser.QueryRow(data.Username, cryptPass).Scan(&userID)
			}

			if err != nil {
				data.Error = err.Error()
			} else {
				http.Redirect(w, r, "user?id="+strconv.FormatInt(userID, 10), http.StatusFound)
			}
		}
	}

	csrfKey, err := getCSRFKey(w, r, true)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)

		return
	}

	data.CSRFToken, err = createCSRFToken(csrfKey)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)

		return
	}

	if data.Password == "" {
		max := big.NewInt(10000)

		n, err := rand.Int(rand.Reader, max)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)

			return
		}

		n.Add(n, max)
		data.Password = "changeme" + n.String()[1:]
	}

	w.Header().Add("Content-Type", "text/html")
	_ = tmpl.ExecuteTemplate(w, "admin-new-user.tmpl", &data)
}

func adminUser(w http.ResponseWriter, r *http.Request) {
	var data struct {
		User  User
		Modes []GameMode
	}

	var err error

	data.User.ID, err = strconv.ParseInt(r.FormValue("id"), 10, 64)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)

		return
	}

	dbDone := timing(w, "db")

	err = getUserByID.QueryRow(data.User.ID).Scan(&data.User.LoginName, &data.User.SessionReset, &data.User.IsAdmin)
	if errors.Is(err, sql.ErrNoRows) {
		http.NotFound(w, r)

		return
	}

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)

		return
	}

	modes, err := getCustomGameModesForOwner.Query(data.User.ID)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)

		return
	}
	defer modes.Close()

	for modes.Next() {
		var mode GameMode
		if err = modes.Scan(&mode.ID, &mode.Client, &mode.Name); err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)

			return
		}

		data.Modes = append(data.Modes, mode)
	}

	if err = modes.Err(); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)

		return
	}

	dbDone()

	w.Header().Add("Content-Type", "text/html")
	_ = tmpl.ExecuteTemplate(w, "admin-user.tmpl", &data)
}

func adminArcadeRecordings(w http.ResponseWriter, r *http.Request) {
	if r.Method == http.MethodPost {
		csrfKey, err := getCSRFKey(w, r, false)
		if err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)

			return
		}

		if !verifyCSRFToken(csrfKey, r.FormValue("csrf-token")) {
			http.Error(w, "request validation failed (try again)", http.StatusBadRequest)

			return
		}

		id, err := strconv.ParseInt(r.PostFormValue("retry"), 10, 64)
		if err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)

			return
		}

		decodeArcadeRecording(id)

		http.Redirect(w, r, r.RequestURI, http.StatusFound)

		return
	}

	type RecordingData struct {
		ID         format.FileID
		UploadedAt time.Time
		Game       sql.NullString
		Player     sql.NullString
		Score      sql.NullInt64
		Vanilla    sql.NullBool
		Failed     sql.NullString
		Code       string
	}

	var data struct {
		Recordings  []RecordingData
		CurrentPage int
		MaxPage     int
		TotalCount  int
		CSRFToken   string
	}

	dbDone := timing(w, "db")

	err := getArcadeRecordingCount.QueryRow().Scan(&data.TotalCount)
	if err != nil {
		http.Error(w, "getting arcade recording count: "+err.Error(), http.StatusInternalServerError)

		return
	}

	data.MaxPage = (data.TotalCount + 249) / 250

	data.CurrentPage, err = strconv.Atoi(r.FormValue("page"))
	if err != nil || data.CurrentPage <= 0 {
		data.CurrentPage = 1
	}

	rows, err := getArcadeRecordingList.Query((data.CurrentPage-1)*250, 250)
	if err != nil {
		http.Error(w, "getting arcade recordings: "+err.Error(), http.StatusInternalServerError)

		return
	}
	defer rows.Close()

	for rows.Next() {
		var (
			recording RecordingData
			hash      []byte
		)

		if err = rows.Scan(&recording.ID.ID, &recording.UploadedAt, &hash, &recording.Game, &recording.Player, &recording.Score, &recording.Vanilla, &recording.Failed); err != nil {
			http.Error(w, "getting arcade recordings: "+err.Error(), http.StatusInternalServerError)

			return
		}

		copy(recording.ID.Hash[:], hash)

		code, err := recording.ID.MarshalText()
		if err != nil {
			http.Error(w, "marshaling recording ID: "+err.Error(), http.StatusInternalServerError)

			return
		}

		recording.Code = string(code)

		data.Recordings = append(data.Recordings, recording)
	}

	if err = rows.Err(); err != nil {
		http.Error(w, "getting arcade recordings: "+err.Error(), http.StatusInternalServerError)

		return
	}

	dbDone()

	csrfKey, err := getCSRFKey(w, r, true)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)

		return
	}

	data.CSRFToken, err = createCSRFToken(csrfKey)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)

		return
	}

	w.Header().Add("Content-Type", "text/html")
	_ = tmpl.ExecuteTemplate(w, "admin-arcade-recordings.tmpl", &data)
}

func adminExtensions(w http.ResponseWriter, r *http.Request) {
	var data struct {
		Custom    *CustomExtensions
		CSRFToken string
	}

	csrfKey, err := getCSRFKey(w, r, true)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)

		return
	}

	data.CSRFToken, err = createCSRFToken(csrfKey)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)

		return
	}

	dbDone := timing(w, "db")

	data.Custom, err = getCustomExtensions(r.Context())
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)

		return
	}

	dbDone()

	w.Header().Add("Content-Type", "text/html")
	_ = tmpl.ExecuteTemplate(w, "admin-extensions.tmpl", &data)
}

func handleIssueReportPost(w http.ResponseWriter, r *http.Request) {
	w.Header().Add("Cache-Control", "no-store")

	if !validateOrigin(w, r) {
		return
	}

	w.Header().Add("Access-Control-Allow-Methods", http.MethodPost)
	w.Header().Add("Access-Control-Allow-Headers", "content-type")

	if r.Method == http.MethodOptions {
		if r.Header.Get("Access-Control-Request-Method") != http.MethodPost {
			w.WriteHeader(http.StatusBadRequest)

			return
		}

		w.WriteHeader(http.StatusNoContent)

		return
	}

	if r.Method != http.MethodPost {
		w.Header().Add("Allow", "POST, OPTIONS")
		http.Error(w, "only POST is allowed", http.StatusMethodNotAllowed)

		return
	}

	err := r.ParseMultipartForm(1 << 20)
	if err != nil {
		http.Error(w, "bad request", http.StatusBadRequest)

		return
	}

	_, err = insertIssueReport.Exec(
		r.Header.Get("X-Forwarded-For"),
		r.UserAgent(),
		r.Form.Get("v"),
		r.Form.Get("v2"),
		r.Form.Get("m"),
		r.Form.Get("t"),
		r.Form.Get("s"),
		r.Form.Get("c"),
		r.Form.Get("u"),
		r.Form.Get("l"),
	)
	if err != nil {
		log.Println("error submitting report", err)
		spew.Dump(r.Form)
		http.Error(w, "database error", http.StatusInternalServerError)

		return
	}

	w.WriteHeader(http.StatusAccepted)
}

func handleAutomatedReportPost(w http.ResponseWriter, r *http.Request) {
	if r.Method == http.MethodOptions {
		if r.Header.Get("Access-Control-Request-Method") != http.MethodPost {
			w.WriteHeader(http.StatusBadRequest)

			return
		}

		w.WriteHeader(http.StatusNoContent)

		return
	}

	if r.Method != http.MethodPost {
		w.Header().Add("Allow", "POST, OPTIONS")
		http.Error(w, "only POST is allowed", http.StatusMethodNotAllowed)

		return
	}

	b, err := io.ReadAll(r.Body)
	if err != nil {
		http.Error(w, "bad request", http.StatusBadRequest)

		return
	}

	_, err = insertAutomatedReport.Exec(
		r.Header.Get("X-Forwarded-For"),
		r.UserAgent(),
		b,
		r.Header.Get("Content-Type"),
	)
	if err != nil {
		log.Println("error submitting report", err)
		spew.Dump(b)
		http.Error(w, "database error", http.StatusInternalServerError)

		return
	}

	w.WriteHeader(http.StatusNoContent)
}
