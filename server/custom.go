package server

import (
	"context"
	"database/sql"
	"encoding/base64"
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"net/http"
	"regexp"
	"strconv"
	"strings"
	"sync"

	"git.lubar.me/ben/spy-cards/card"
	"git.lubar.me/ben/spy-cards/match"
)

var twoNewlines = regexp.MustCompile(`\n\n+`)

func handleCustomCardAPI(w http.ResponseWriter, r *http.Request) {
	w.Header().Add("Access-Control-Allow-Origin", "*")
	w.Header().Set("Cache-Control", "no-store")

	path := strings.Split(strings.TrimPrefix(r.URL.Path, "/spy-cards/custom/api/"), "/")
	if (len(path) == 2 && path[0] == "latest") || (len(path) == 3 && path[0] == "get-revision") {
		var response struct {
			Name         string
			Cards        string
			Revision     int
			QuickJoin    bool `json:",omitempty"`
			AnyRecording bool `json:",omitempty"`
		}

		mode := path[1]

		var rev sql.NullInt64

		if len(path) == 3 {
			var err error

			rev.Int64, err = strconv.ParseInt(path[2], 10, 64)
			rev.Valid = true

			if err != nil {
				http.NotFound(w, r)

				return
			}
		}

		dbDone := timing(w, "db")

		if err := getCustomCardSet.QueryRow(mode, rev).Scan(&response.Name, &response.Cards, &response.Revision); errors.Is(err, sql.ErrNoRows) {
			http.NotFound(w, r)

			return
		} else if err != nil {
			log.Println("custom card API error", err)
			http.Error(w, "database error", http.StatusInternalServerError)

			return
		}

		var (
			randomID   int64
			randomHash []byte
		)

		if err := getRandomMatchRecording.QueryRow("{}", mode, false).Scan(&randomID, &randomHash); err == nil {
			response.AnyRecording = true
		} else if !errors.Is(err, sql.ErrNoRows) {
			log.Println("custom card API error", err)
			http.Error(w, "database error", http.StatusInternalServerError)

			return
		}

		if matchCodePrefix("-"+mode) != nil {
			response.QuickJoin = true
		}

		dbDone()

		// No longer used for Godot version.
		//if modeHasCustomPortraits(mode, response.Revision) {
		//	w.Header().Add("Link", "</spy-cards/user-img/mode/"+mode+"."+strconv.Itoa(response.Revision)+".webp>;rel=preload;as=image;crossorigin=anonymous")
		//}

		if rev.Valid {
			w.Header().Set("Cache-Control", "public, max-age=86400")
		} else {
			w.Header().Set("Cache-Control", "public, max-age=30, s-maxage=5")
		}

		w.Header().Set("Content-Type", "application/json")
		_ = json.NewEncoder(w).Encode(&response)

		return
	}

	if len(path) == 2 && path[0] == "changelog" {
		http.Redirect(w, r, "/spy-cards/changelog/"+path[1], http.StatusMovedPermanently)

		return
	}

	http.NotFound(w, r)
}

func handleChangelog(w http.ResponseWriter, r *http.Request) {
	slug := strings.TrimPrefix(r.URL.Path, "/spy-cards/changelog/")

	var (
		modeID   int64
		modeName string
	)

	dbDone := timing(w, "db")

	if err := getCustomGameModeIDBySlug.QueryRow(slug).Scan(&modeID, &modeName); errors.Is(err, sql.ErrNoRows) {
		http.NotFound(w, r)

		return
	} else if err != nil {
		log.Println("custom card API error", err)
		http.Error(w, "database error", http.StatusInternalServerError)

		return
	}

	data := templateData{
		Title:    "Changelog for " + modeName + " - Spy Cards Online",
		Canon:    "/spy-cards/changelog/" + slug,
		ModeName: modeName,
		ModeSlug: slug,
	}

	rows, err := getCustomGameModeSets.Query(modeID)
	if err != nil {
		log.Println("custom card API error", err)
		http.Error(w, "database error", http.StatusInternalServerError)

		return
	}
	defer rows.Close()

	var prevRev string

	for rows.Next() {
		var (
			rev       revisionData
			cardCodes string
		)

		if err = rows.Scan(&rev.Num, &cardCodes); err != nil {
			log.Println("custom card API error", err)
			http.Error(w, "database error", http.StatusInternalServerError)

			return
		}

		var s card.Set
		if err := s.UnmarshalText([]byte(cardCodes)); err != nil {
			continue
		}

		if md, ok := s.Mode.Get(card.FieldMetadata).(*card.Metadata); ok {
			if md.LatestChanges != prevRev {
				paragraphs := twoNewlines.Split(md.LatestChanges, -1)
				for _, p := range paragraphs {
					rev.Paragraphs = append(rev.Paragraphs, strings.Split(p, "\n"))
				}
			}

			prevRev = md.LatestChanges
		} else {
			prevRev = ""
		}

		data.Revisions = append(data.Revisions, rev)
	}

	if err = rows.Err(); err != nil {
		log.Println("custom card API error", err)
		http.Error(w, "database error", http.StatusInternalServerError)

		return
	}

	dbDone()

	for i, j := 0, len(data.Revisions)-1; i < j; i, j = i+1, j-1 {
		data.Revisions[i], data.Revisions[j] = data.Revisions[j], data.Revisions[i]
	}

	w.Header().Set("Cache-Control", "public, max-age=30, s-maxage=5, stale-while-revalidate=3600")
	w.Header().Set("Content-Type", "text/html; charset=utf-8")
	_ = tmpl.ExecuteTemplate(w, "game-mode-changelog.tmpl", &data)
}

type tinyModeID struct {
	mode     string
	revision int
}

var (
	hasCustomPortraitsCache     = make(map[tinyModeID]bool)
	hasCustomPortraitsCacheLock sync.Mutex
)

func modeHasCustomPortraits(mode string, revision int) bool {
	hasCustomPortraitsCacheLock.Lock()
	has, ok := hasCustomPortraitsCache[tinyModeID{mode, revision}]
	hasCustomPortraitsCacheLock.Unlock()

	if ok {
		return has
	}

	var (
		name           string
		cards          string
		actualRevision int
	)

	if err := getCustomCardSet.QueryRow(mode, revision).Scan(&name, &cards, &actualRevision); err != nil {
		return false
	}

	if actualRevision != revision {
		return false
	}

	var set card.Set

	if err := set.UnmarshalText([]byte(cards)); err != nil {
		return false
	}

	for _, c := range set.Cards {
		if c.Portrait == card.PortraitCustomExternal {
			has = true

			break
		}
	}

	if !has && set.Mode != nil {
		for _, f := range set.Mode.Fields {
			if md, ok := f.(*card.Metadata); ok && md.Portrait == card.PortraitCustomExternal {
				has = true

				break
			}

			if v, ok := f.(*card.Variant); ok {
				for _, r := range v.Rules {
					if md, ok := r.(*card.Metadata); ok && md.Portrait == card.PortraitCustomExternal {
						has = true

						break
					}
				}

				if has {
					break
				}
			}
		}
	}

	hasCustomPortraitsCacheLock.Lock()
	hasCustomPortraitsCache[tinyModeID{mode, revision}] = has
	hasCustomPortraitsCacheLock.Unlock()

	return has
}

func editCustomMode(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Cache-Control", "no-store")

	cu, _ := getCurrentUser(&r)
	if cu == nil {
		http.SetCookie(w, &http.Cookie{
			Name:     "spy_cards_login_redirect",
			Value:    r.URL.RequestURI(),
			Path:     "/spy-cards/",
			SameSite: http.SameSiteStrictMode,
			HttpOnly: true,
			Secure:   true,
		})
		http.Redirect(w, r, "/spy-cards/log-in", http.StatusFound)

		return
	}

	id, err := strconv.ParseInt(r.URL.Path[len("/spy-cards/custom/edit-mode/"):], 10, 64)
	if err != nil {
		http.NotFound(w, r)

		return
	}

	var data templateData

	data.Mode.ID = id

	var ownerID int64

	err = getCustomGameMode.QueryRow(id).Scan(&data.Mode.Client, &data.Mode.Name, &data.Mode.Revisions, &ownerID, &data.Mode.Prefix)
	if err != nil && errors.Is(err, sql.ErrNoRows) {
		log.Println("get custom game mode", err)
		http.Error(w, "database error", http.StatusInternalServerError)

		return
	}

	if errors.Is(err, sql.ErrNoRows) || ownerID != cu.ID {
		http.Error(w, "this is not your game mode", http.StatusForbidden)

		return
	}

	if r.Method == http.MethodPost {
		csrfKey, err := getCSRFKey(w, r, false)
		if err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)

			return
		}

		if !verifyCSRFToken(csrfKey, r.FormValue("csrf-token")) {
			http.Error(w, "request validation failed (try again)", http.StatusBadRequest)

			return
		}

		if r.FormValue("cards") == "" {
			http.Error(w, "missing cards", http.StatusBadRequest)

			return
		}

		ids, err := validateCustomCardSet(r.FormValue("cards"))
		if err != nil {
			http.Error(w, "cards failed validation: "+err.Error(), http.StatusBadRequest)

			return
		}

		tx, err := begin()
		if err != nil {
			log.Println("create custom card set begin", err)
			http.Error(w, "database error", http.StatusInternalServerError)

			return
		}

		defer tx.Rollback()

		var setID int64

		rev := data.Mode.Revisions + 1

		createSet := tx.Stmt(createCustomCardSet)
		defer createSet.Close()

		if err = createSet.QueryRow(id, r.Header.Get("X-Forwarded-For"), rev).Scan(&setID); err != nil {
			log.Println("create custom card set create set", err)
			http.Error(w, "database error", http.StatusInternalServerError)

			return
		}

		addCard := tx.Stmt(addCardToCustomSet)
		defer addCard.Close()

		for i, cardID := range ids {
			if _, err = addCard.Exec(setID, cardID, i+1); err != nil {
				log.Println("create custom card set add card", err)
				http.Error(w, "database error", http.StatusInternalServerError)

				return
			}
		}

		setLatest := tx.Stmt(setLatestGameModeRevision)
		defer setLatest.Close()

		if _, err = setLatest.Exec(id, rev); err != nil {
			log.Println("create custom card set update revision", err)
			http.Error(w, "database error", http.StatusInternalServerError)

			return
		}

		if err = tx.Commit(); err != nil {
			log.Println("create custom card set commit", err)
			http.Error(w, "database error", http.StatusInternalServerError)

			return
		}

		if _, err = refreshCustomGameModeList.Exec(); err != nil {
			log.Println("refresh custom game mode list", err)
			http.Error(w, "database error", http.StatusInternalServerError)

			return
		}

		http.Redirect(w, r, r.URL.Path, http.StatusFound)

		return
	}

	rows, err := getCustomGameModeSets.Query(id)
	if err != nil {
		log.Println("get custom game mode revisions", err)
		http.Error(w, "database error", http.StatusInternalServerError)

		return
	}
	defer rows.Close()

	for rows.Next() {
		var rev modeRevision

		err = rows.Scan(&rev.Revision, &rev.Cards)
		if err != nil {
			log.Println("get custom game mode revisions", err)
			http.Error(w, "database error", http.StatusInternalServerError)

			return
		}

		data.Sets = append(data.Sets, rev)
	}

	if err := rows.Err(); err != nil {
		log.Println("get custom game mode revisions", err)
		http.Error(w, "database error", http.StatusInternalServerError)

		return
	}

	csrfKey, err := getCSRFKey(w, r, true)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)

		return
	}

	data.CSRFToken, err = createCSRFToken(csrfKey)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)

		return
	}

	w.Header().Set("Content-Type", "text/html; charset=utf-8")
	_ = tmpl.ExecuteTemplate(w, "user-game-mode.tmpl", &data)
}

func validateCustomCardSet(cardCodes string) ([]int64, error) {
	overridden := make(map[card.ID]bool)
	codes := strings.Split(cardCodes, ",")
	ids := make([]int64, len(codes))

	for i, code := range codes {
		b, err := base64.StdEncoding.DecodeString(code)
		if err != nil {
			return nil, fmt.Errorf("for card code %q: %w", code, err)
		}

		if i == 0 {
			var gm card.GameMode

			err = gm.UnmarshalBinary(b)
			if err != nil && b[0] != 3 { // current game mode format version
				return nil, errors.New("custom game modes must contain a Game Mode Metadata entry. add one and leave the title blank")
			}

			if err != nil {
				return nil, fmt.Errorf("expected game mode definition at start: %w", err)
			}

			haveMetadata := false

			for _, gmf := range gm.Fields {
				if md, ok := gmf.(*card.Metadata); ok {
					if !haveMetadata && md.Title != "" {
						return nil, errors.New("game mode title field should be left blank for official custom game modes")
					}

					if !haveMetadata && md.Author == "" {
						return nil, errors.New("game mode is missing author")
					}

					if !haveMetadata && md.Description == "" {
						return nil, errors.New("game mode is missing short description")
					}

					if md.Portrait == card.PortraitCustomEmbedded {
						if len(md.CustomPortrait) != 0 {
							return nil, errors.New("portrait is embedded in game mode; reupload portrait for a shorter game mode code")
						}

						if !haveMetadata {
							return nil, errors.New("game mode is missing portrait")
						}
					}

					haveMetadata = true
				}
			}

			if !haveMetadata {
				return nil, errors.New("missing game mode metadata")
			}
		} else {
			var c card.Def
			err = c.UnmarshalBinary(b)
			if err != nil {
				return nil, fmt.Errorf("for card code %q: %w", code, err)
			}

			if c.Portrait == card.PortraitCustomEmbedded {
				return nil, fmt.Errorf("for card code %q: portrait is embedded in card; reupload card portrait for a shorter card code", code)
			}

			if overridden[c.ID] {
				return nil, fmt.Errorf("card %d is overridden multiple times by this set", uint64(c.ID))
			}
			overridden[c.ID] = true
		}

		if err = insertCustomCard.QueryRow(b).Scan(&ids[i]); err != nil {
			return nil, fmt.Errorf("for card code %q: %w", code, err)
		}
	}

	return ids, nil
}

type availableModes struct {
	Categories []match.ModeCategory `json:"c"`
	Modes      []GameMode           `json:"m"`
}

// GameMode is a community-owned Spy Cards Online custom game mode.
type GameMode struct {
	ID     int64          `json:"-"`
	Prefix sql.NullString `json:"-"`
	match.AvailableMode
}

func getPublicGameModes(ctx context.Context, w http.ResponseWriter, preloadPortraits bool) *availableModes {
	var data availableModes

	rows, err := getCustomGameModeCategories.QueryContext(ctx)
	if err != nil {
		log.Println("custom game mode API error", err)
		http.Error(w, "database error", http.StatusInternalServerError)

		return nil
	}
	defer rows.Close()

	for rows.Next() {
		var cat match.ModeCategory
		if err = rows.Scan(&cat.ID, &cat.Name, &cat.Description, &cat.Color); err != nil {
			log.Println("custom game mode API error", err)
			http.Error(w, "database error", http.StatusInternalServerError)

			return nil
		}

		data.Categories = append(data.Categories, cat)
	}

	if err := rows.Err(); err != nil {
		log.Println("custom game mode API error", err)
		http.Error(w, "database error", http.StatusInternalServerError)

		return nil
	}

	rows, err = getCustomGameModeList.QueryContext(ctx, 0, 1000)
	if err != nil {
		log.Println("custom game mode API error", err)
		http.Error(w, "database error", http.StatusInternalServerError)

		return nil
	}
	defer rows.Close()

	for rows.Next() {
		var (
			mode          GameMode
			categoryID    sql.NullInt64
			categoryColor sql.NullInt32
		)

		if err = rows.Scan(&mode.ID, &mode.Client, &mode.Prefix, &mode.Name, &mode.Revisions, &mode.ModeData, &mode.FirstCard, &categoryColor, &categoryID); err != nil {
			log.Println("custom game mode API error", err)
			http.Error(w, "database error", http.StatusInternalServerError)

			return nil
		}

		if categoryColor.Valid {
			mode.CategoryColor = categoryColor.Int32
		} else {
			mode.CategoryColor = 0x808080
		}

		mode.CategoryID = categoryID.Int64

		if len(mode.FirstCard) != 0 && mode.FirstCard[0] == 3 {
			// a game mode with no custom cards was causing issues
			mode.FirstCard = nil
		}

		// 3 = game mode format revision
		if len(mode.ModeData) != 0 && mode.ModeData[0] == 3 {
			var decodedMode card.GameMode
			if err := decodedMode.UnmarshalBinary(mode.ModeData); err == nil {
				md, ok := decodedMode.Get(card.FieldMetadata).(*card.Metadata)
				if ok && preloadPortraits && md.Portrait == card.PortraitCustomExternal {
					// This created too long of an HTTP header
					//w.Header().Add("Link", "</spy-cards/user-img/"+format.Encode32(md.CustomPortrait)+".webp>;rel=preload;as=image;crossorigin=anonymous")
				}
			}

			data.Modes = append(data.Modes, mode)
		}
	}

	if err := rows.Err(); err != nil {
		log.Println("custom game mode API error", err)
		http.Error(w, "database error", http.StatusInternalServerError)

		return nil
	}

	if len(data.Modes) >= 900 {
		log.Println("WARNING: custom mode count approaching query limit")
	}

	return &data
}

func handleCustomGameModes(w http.ResponseWriter, r *http.Request) {
	w.Header().Add("Access-Control-Allow-Origin", "*")

	dbDone := timing(w, "db")

	data := getPublicGameModes(r.Context(), w, true)
	if data == nil {
		return
	}

	dbDone()

	w.Header().Set("Cache-Control", "public, max-age=30, stale-while-revalidate=3600")
	w.Header().Set("Content-Type", "application/json")
	_ = json.NewEncoder(w).Encode(data)
}

type CustomStage struct {
	ID   int64                  `json:"-"`
	Name string                 `json:"n"`
	CID  card.ContentIdentifier `json:"c"`
}

type CustomMusic struct {
	ID    int64                  `json:"-"`
	Name  string                 `json:"n"`
	CID   card.ContentIdentifier `json:"c"`
	Start float32                `json:"s"`
	End   float32                `json:"e"`
}

type CustomExtensions struct {
	Stages []CustomStage `json:"s"`
	Music  []CustomMusic `json:"m"`
}

func getCustomExtensions(ctx context.Context) (*CustomExtensions, error) {
	var data CustomExtensions

	data.Music = make([]CustomMusic, 0)
	data.Stages = make([]CustomStage, 0)

	stages, err := getCustomStages.QueryContext(ctx)
	if err != nil {
		return nil, fmt.Errorf("server: querying stage extensions: %w", err)
	}
	defer stages.Close()

	for stages.Next() {
		var stage CustomStage
		if err = stages.Scan(&stage.ID, &stage.Name, &stage.CID); err != nil {
			return nil, fmt.Errorf("server: scanning stage extensions: %w", err)
		}

		data.Stages = append(data.Stages, stage)
	}

	if err := stages.Err(); err != nil {
		return nil, fmt.Errorf("server: getting stage extensions: %w", err)
	}

	musics, err := getCustomMusic.QueryContext(ctx)
	if err != nil {
		return nil, fmt.Errorf("server: querying music extensions: %w", err)
	}
	defer musics.Close()

	for musics.Next() {
		var music CustomMusic
		if err = musics.Scan(&music.ID, &music.Name, &music.CID, &music.Start, &music.End); err != nil {
			return nil, fmt.Errorf("server: scanning music extensions: %w", err)
		}

		data.Music = append(data.Music, music)
	}

	if err := musics.Err(); err != nil {
		return nil, fmt.Errorf("server: getting music extensions: %w", err)
	}

	return &data, nil
}

func getExtensionLibrary(w http.ResponseWriter, r *http.Request) {
	w.Header().Add("Access-Control-Allow-Origin", "*")

	dbDone := timing(w, "db")

	data, err := getCustomExtensions(r.Context())
	if err != nil {
		log.Println("extensions API error", err)
		http.Error(w, "database error", http.StatusInternalServerError)

		return
	}

	dbDone()

	w.Header().Set("Cache-Control", "public, max-age=300")
	w.Header().Set("Content-Type", "application/json")
	_ = json.NewEncoder(w).Encode(data)
}
