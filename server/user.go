package server

import (
	"bytes"
	"context"
	"crypto/hmac"
	"crypto/rand"
	"crypto/sha256"
	"crypto/subtle"
	"database/sql"
	"encoding/base64"
	"encoding/binary"
	"errors"
	"fmt"
	"log"
	"net/http"
	"os"
	"strings"
	"time"

	"golang.org/x/crypto/bcrypt"
)

var sessionKey = func() []byte {
	b, err := os.ReadFile("/spy-cards-session-key")
	if err != nil {
		panic(err)
	}

	return b
}()

// User is a Spy Cards Online user account.
type User struct {
	ID           int64
	LoginName    string
	password     []byte
	SessionReset int
	IsAdmin      bool
}

type currentUserKey struct{}

func spyCardsLanding(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Cache-Control", "no-store")
	w.Header().Set("X-Robots-Tag", "noindex")

	if r.URL.Path != "/spy-cards/" {
		http.NotFound(w, r)

		return
	}

	var data templateData

	data.User, _ = getCurrentUser(&r)
	if data.User != nil {
		rows, err := getCustomGameModesForOwner.Query(data.User.ID)
		if err != nil {
			log.Println("get custom game modes for user", err)
			http.Error(w, "database error", http.StatusInternalServerError)

			return
		}
		defer rows.Close()

		for rows.Next() {
			var mode GameMode
			if err := rows.Scan(&mode.ID, &mode.Client, &mode.Name); err != nil {
				log.Println("get custom game modes for user", err)
				http.Error(w, "database error", http.StatusInternalServerError)

				return
			}

			data.Modes = append(data.Modes, mode)
		}

		if err := rows.Err(); err != nil {
			log.Println("get custom game modes for user", err)
			http.Error(w, "database error", http.StatusInternalServerError)

			return
		}
	}

	w.Header().Set("Content-Type", "text/html; charset=utf-8")
	_ = tmpl.ExecuteTemplate(w, "user-landing.tmpl", &data)
}

func setLoginCookie(w http.ResponseWriter, u *User) {
	// login cookie format (Base64-encoded (no padding)):
	// - byte[32]: HMAC-SHA256 checksum of remaining fields with global session key as the key
	// - byte: 0 (version number)
	// - uvarint: user ID
	// - uvarint: user session reset (password change) count
	buf := make([]byte, sha256.Size+1+binary.MaxVarintLen64*2)
	n1 := binary.PutUvarint(buf[sha256.Size+1:], uint64(u.ID))
	n2 := binary.PutUvarint(buf[sha256.Size+1+n1:], uint64(u.SessionReset))
	buf = buf[:sha256.Size+1+n1+n2]
	hash := hmac.New(sha256.New, sessionKey)
	_, _ = hash.Write(buf[sha256.Size:])
	copy(buf, hash.Sum(nil))

	http.SetCookie(w, &http.Cookie{
		Name:     "spy_cards_login",
		Value:    base64.RawStdEncoding.EncodeToString(buf),
		Path:     "/spy-cards/",
		MaxAge:   86400 * 30,
		SameSite: http.SameSiteStrictMode,
		HttpOnly: true,
		Secure:   true,
	})
}

func getCurrentUser(r **http.Request) (*User, error) {
	u, ok := (*r).Context().Value(currentUserKey{}).(*User)
	if ok {
		return u, nil
	}

	c, err := (*r).Cookie("spy_cards_login")
	if err != nil {
		// no cookie
		return nil, nil //nolint:nilerr
	}

	b, err := base64.RawStdEncoding.DecodeString(c.Value)
	if err != nil {
		// only propagate database errors
		return nil, nil //nolint:nilerr
	}

	if len(b) <= sha256.Size {
		// too short to be valid
		return nil, nil
	}

	hash := hmac.New(sha256.New, sessionKey)
	_, _ = hash.Write(b[sha256.Size:])

	if subtle.ConstantTimeCompare(hash.Sum(nil), b[:sha256.Size]) != 1 {
		return nil, nil
	}

	b = b[sha256.Size:]
	if b[0] != 0 {
		return nil, nil
	}

	b = b[1:]
	userID, n := binary.Uvarint(b)

	if n <= 0 {
		return nil, nil
	}

	b = b[n:]
	sessionReset, n := binary.Uvarint(b)

	if n != len(b) {
		return nil, nil
	}

	u = &User{
		ID: int64(userID),
	}

	switch err := getUserByID.QueryRow(userID).Scan(&u.LoginName, &u.SessionReset, &u.IsAdmin); {
	case errors.Is(err, sql.ErrNoRows):
		u = nil
	case err != nil:
		return nil, fmt.Errorf("server: getting current user: %w", err)
	case u.SessionReset != int(sessionReset):
		u = nil
	}

	*r = (*r).WithContext(context.WithValue((*r).Context(), currentUserKey{}, u))

	return u, nil
}

func loginPage(w http.ResponseWriter, r *http.Request) {
	w.Header().Add("Cache-Control", "no-store")
	w.Header().Set("X-Robots-Tag", "noindex")

	cu, _ := getCurrentUser(&r)
	if cu != nil {
		onLoggedIn(w, r)

		return
	}

	if r.Method == http.MethodPost {
		csrfKey, err := getCSRFKey(w, r, false)
		if err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)

			return
		}

		if !verifyCSRFToken(csrfKey, r.FormValue("csrf-token")) {
			http.Error(w, "request validation failed (try again)", http.StatusBadRequest)

			return
		}

		loginName := r.FormValue("login-name")
		password := []byte(r.FormValue("password"))

		u := &User{
			LoginName: loginName,
		}

		err = getUserByLogin.QueryRow(loginName).Scan(&u.ID, &u.password, &u.SessionReset, &u.IsAdmin)
		if err == nil {
			err = bcrypt.CompareHashAndPassword(u.password, password)
		}

		if err != nil {
			http.Error(w, "login failed", http.StatusBadRequest)

			return
		}

		setLoginCookie(w, u)

		onLoggedIn(w, r)

		return
	}

	csrfKey, err := getCSRFKey(w, r, true)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)

		return
	}

	csrfToken, err := createCSRFToken(csrfKey)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)

		return
	}

	w.Header().Set("Content-Type", "text/html; charset=utf-8")
	_ = tmpl.ExecuteTemplate(w, "user-log-in.tmpl", &templateData{
		CSRFToken: csrfToken,
	})
}

func logoutPage(w http.ResponseWriter, r *http.Request) {
	w.Header().Add("Cache-Control", "no-store")
	w.Header().Set("X-Robots-Tag", "noindex")

	cu, _ := getCurrentUser(&r)
	if cu == nil {
		http.Redirect(w, r, "/spy-cards/", http.StatusFound)

		return
	}

	if r.Method == http.MethodPost {
		csrfKey, err := getCSRFKey(w, r, false)
		if err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)

			return
		}

		if !verifyCSRFToken(csrfKey, r.FormValue("csrf-token")) {
			http.Error(w, "request validation failed (try again)", http.StatusBadRequest)

			return
		}

		http.SetCookie(w, &http.Cookie{
			Name:     "spy_cards_login",
			Value:    "",
			Path:     "/spy-cards/",
			Expires:  time.Unix(1, 0),
			SameSite: http.SameSiteStrictMode,
			HttpOnly: true,
			Secure:   true,
		})

		w.Header().Add("Clear-Site-Data", "\"cookies\"")

		http.Redirect(w, r, "/spy-cards/", http.StatusFound)

		return
	}

	csrfKey, err := getCSRFKey(w, r, true)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)

		return
	}

	csrfToken, err := createCSRFToken(csrfKey)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)

		return
	}

	w.Header().Set("Content-Type", "text/html; charset=utf-8")
	_ = tmpl.ExecuteTemplate(w, "user-log-out.tmpl", &templateData{
		CSRFToken: csrfToken,
	})
}

func changePasswordPage(w http.ResponseWriter, r *http.Request) {
	w.Header().Add("Cache-Control", "no-store")
	w.Header().Set("X-Robots-Tag", "noindex")

	cu, _ := getCurrentUser(&r)
	if cu == nil {
		http.SetCookie(w, &http.Cookie{
			Name:     "spy_cards_login_redirect",
			Value:    r.URL.RequestURI(),
			Path:     "/spy-cards/",
			SameSite: http.SameSiteStrictMode,
			HttpOnly: true,
			Secure:   true,
		})
		http.Redirect(w, r, "/spy-cards/log-in", http.StatusFound)

		return
	}

	if r.Method == http.MethodPost {
		csrfKey, err := getCSRFKey(w, r, false)
		if err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)

			return
		}

		if !verifyCSRFToken(csrfKey, r.FormValue("csrf-token")) {
			http.Error(w, "request validation failed (try again)", http.StatusBadRequest)

			return
		}

		currentPassword := []byte(r.FormValue("current-password"))
		newPassword := []byte(r.FormValue("new-password"))
		newPassword2 := []byte(r.FormValue("new-password2"))

		if !bytes.Equal(newPassword, newPassword2) {
			http.Error(w, "new password did not match confirmation", http.StatusBadRequest)

			return
		}

		var currentHash []byte
		if err := getUserCurrentPassword.QueryRow(cu.ID).Scan(&currentHash); err != nil {
			log.Println("change password get current", err)
			http.Error(w, "database error", http.StatusInternalServerError)

			return
		}

		if err = bcrypt.CompareHashAndPassword(currentHash, currentPassword); err != nil {
			http.Error(w, "current password did not match", http.StatusForbidden)

			return
		}

		newHash, err := bcrypt.GenerateFromPassword(newPassword, bcrypt.DefaultCost)
		if err != nil {
			http.Error(w, "password crypt error", http.StatusInternalServerError)

			return
		}

		_, err = updateUserPassword.Exec(cu.ID, currentHash, newHash)
		if err != nil {
			log.Println("change password update", err)
			http.Error(w, "database error", http.StatusInternalServerError)

			return
		}

		cu.SessionReset++

		setLoginCookie(w, cu)
		http.Redirect(w, r, "/spy-cards/", http.StatusFound)

		return
	}

	csrfKey, err := getCSRFKey(w, r, true)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)

		return
	}

	csrfToken, err := createCSRFToken(csrfKey)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)

		return
	}

	w.Header().Set("Content-Type", "text/html; charset=utf-8")
	_ = tmpl.ExecuteTemplate(w, "user-change-password.tmpl", &templateData{
		CSRFToken: csrfToken,
	})
}

func getCSRFKey(w http.ResponseWriter, r *http.Request, create bool) ([]byte, error) {
	csrfToken, err := r.Cookie("spy_cards_csrf_token")
	if err != nil && !create {
		return nil, errors.New("missing CSRF token")
	}

	if csrfToken != nil {
		b, err := base64.RawStdEncoding.DecodeString(csrfToken.Value)
		if len(b) == 32 && err == nil {
			return b, nil
		}

		if !create {
			return nil, errors.New("invalid CSRF token")
		}
	}

	b := make([]byte, 32)

	_, err = rand.Read(b)
	if err != nil {
		return nil, fmt.Errorf("server: getting random data for CSRF token: %w", err)
	}

	http.SetCookie(w, &http.Cookie{
		Name:     "spy_cards_csrf_token",
		Value:    base64.RawStdEncoding.EncodeToString(b),
		Path:     "/spy-cards/",
		SameSite: http.SameSiteStrictMode,
		Secure:   true,
		HttpOnly: true,
	})

	return b, nil
}

func createCSRFToken(b []byte) (string, error) {
	buf := make([]byte, 64)

	_, err := rand.Read(buf[:32])
	if err != nil {
		return "", fmt.Errorf("server: getting random data for CSRF token: %w", err)
	}

	for i := 0; i < 32; i++ {
		buf[i+32] = buf[i] ^ b[i]
	}

	return base64.RawStdEncoding.EncodeToString(buf), nil
}

func verifyCSRFToken(b []byte, token string) bool {
	buf, err := base64.RawStdEncoding.DecodeString(token)
	if err != nil || len(buf) != 64 {
		return false
	}

	for i := 0; i < 32; i++ {
		buf[i+32] ^= buf[i]
	}

	return bytes.Equal(buf[32:], b)
}

func onLoggedIn(w http.ResponseWriter, r *http.Request) {
	redirectTo, err := r.Cookie("spy_cards_login_redirect")
	if err == nil {
		http.SetCookie(w, &http.Cookie{
			Name:     "spy_cards_login_redirect",
			Value:    "",
			Expires:  time.Unix(1, 0),
			SameSite: http.SameSiteStrictMode,
			HttpOnly: true,
			Secure:   true,
		})

		if strings.HasPrefix(redirectTo.Value, "/spy-cards/") {
			http.Redirect(w, r, redirectTo.Value, http.StatusFound)

			return
		}
	}

	http.Redirect(w, r, "/spy-cards/", http.StatusFound)
}
