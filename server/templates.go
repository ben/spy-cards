package server

import (
	"context"
	"embed"
	"html"
	"html/template"
	"strconv"
	"strings"

	"git.lubar.me/ben/spy-cards/card"
	"git.lubar.me/ben/spy-cards/format"
	"git.lubar.me/ben/spy-cards/internal"
)

//go:embed templates
var templates embed.FS

var tmpl = template.Must(template.New("templates").Funcs(template.FuncMap{
	"card": func(set *card.Set, def *card.Def) template.HTML {
		/*#nosec*/
		return template.HTML(def.ToHTML(context.Background(), set))
	},
	"portrait": func(portrait uint8, custom []byte) template.HTML {
		if portrait == card.PortraitCustomEmbedded {
			return ""
		}

		if portrait == card.PortraitCustomExternal {
			/*#nosec*/
			return `<div class="portrait external" data-x="15" data-y="15"><img src="` + template.HTML(html.EscapeString(internal.GetConfig(context.Background()).UserImageBaseURL+format.Encode32(custom)+".webp")) + `" alt="" loading="lazy"></div>`
		}

		/*#nosec*/
		return `<div class="portrait builtin" data-x="` + template.HTML(strconv.Itoa(int(portrait&15))) + `" data-y="` + template.HTML(strconv.Itoa(int(portrait>>4))) + `"></div>`
	},
	"splitnl": func(s string) []string {
		return strings.Split(s, "\n")
	},
}).ParseFS(templates, "templates/*.tmpl"))

type pageText struct {
	Type           string
	Text           string
	Portrait       uint8
	CustomPortrait []byte
}

type revisionData struct {
	Num        int
	Paragraphs [][]string
}

type modeRevision struct {
	Revision int
	Cards    string
}

type templateData struct {
	Title         string
	Canon         string
	Set           *card.Set
	Cards         []card.ID
	CardLink      map[card.ID]string
	DeckEditorURL string
	Description   string
	PreviewImage  string
	CardClass     string
	Variants      []*card.Variant
	Variant       *card.Variant
	Text          []pageText
	Revisions     []revisionData
	ModeName      string
	ModeSlug      string
	CSRFToken     string
	User          *User
	Modes         []GameMode
	Mode          GameMode
	Sets          []modeRevision
	IsCustom      bool
	WASM          bool
}
