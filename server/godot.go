package server

import (
	"archive/zip"
	"bytes"
	"database/sql"
	"encoding/base64"
	"encoding/binary"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"io/fs"
	"log"
	"net"
	"net/http"
	"os"
	"os/exec"
	"strconv"
	"syscall"
	"time"

	"git.lubar.me/ben/spy-cards/format"
)

func initGodotRPC() {
	// delete any socket we had from a previous container run
	_ = os.Remove("/go/src/spy-cards/godot.sock")

	cmd := exec.Command("./spy_cards_online_dedicated_server.x86_64")
	cmd.Dir = "/go/src/spy-cards/server/dedi/"
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr

	err := cmd.Start()
	if err != nil {
		panic(err)
	}

	// this is inefficient, but it should happen very infrequently (only during startup)
	for {
		_, err := os.Stat("/go/src/spy-cards/godot.sock")
		if err == nil {
			break
		}

		if cmd.Process.Signal(syscall.Signal(0)) != nil {
			// process crashed before making socket file
			panic(cmd.Wait())
		}

		if errors.Is(err, fs.ErrNotExist) {
			time.Sleep(100 * time.Millisecond)
		} else {
			panic(err)
		}
	}

	go func() {
		// take down the Go process if the Godot process dies. the opposite is already true because we're the pid 1 of the Docker container.
		panic(cmd.Wait())
	}()
}

func godotRPCInternal(request []byte) (response []byte, err error) {
	conn, err := net.Dial("unixpacket", "/go/src/spy-cards/godot.sock")
	if err != nil {
		err = fmt.Errorf("dial godot rpc: %w", err)
		return
	}
	defer conn.Close()

	_, err = conn.Write(request)
	if err != nil {
		err = fmt.Errorf("write godot rpc: %w", err)
		return
	}

	// Because Go makes this slightly harder than raw C Linux API calls do, we don't send an arbitrary-sized packet.
	// Instead, we first send a length and then the actual payload.
	// For some extra error checking, allocate an extra byte and make sure we didn't read it.
	var length [8 + 1]byte
	n, err := conn.Read(length[:])
	if err != nil {
		err = fmt.Errorf("read godot rpc (size): %w", err)
		return
	}

	if n != 8 {
		err = fmt.Errorf("tried to read godot rpc size packet (8 bytes), but received packet was (at least) %d bytes", n)
		return
	}

	expectedLen := int(binary.LittleEndian.Uint64(length[:]))
	// special case: no second packet if it would be empty (can't send an empty packet)
	if expectedLen == 0 {
		response = make([]byte, 0)
	} else {
		response = make([]byte, expectedLen+1)

		n, err = conn.Read(response)
		response = response[:n]
		if err != nil {
			err = fmt.Errorf("read godot rpc: %w", err)
			return
		}

		if n != expectedLen {
			err = fmt.Errorf("tried to read godot rpc payload packet (%d bytes), but received packet was (at least) %d bytes", expectedLen, n)
			return
		}
	}

	err = conn.Close()
	if err != nil {
		err = fmt.Errorf("close godot rpc: %w", err)
		return
	}

	return
}

type godotStatus struct {
	LoggedIn bool   `json:"logged-in"`
	UserName string `json:"user-name"`
}

func handleGodotStatus(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Cache-Control", "no-store")
	w.Header().Add("Access-Control-Allow-Origin", "https://spy-cards.lubar.me")

	user, err := getCurrentUser(&r)
	if err != nil {
		log.Println("failed to get current user in godot api status", err)
		http.Error(w, "database error", http.StatusInternalServerError)

		return
	}

	var status godotStatus
	if user != nil {
		status.LoggedIn = true
		status.UserName = user.LoginName
	}

	encoded, err := json.Marshal(&status)
	if err != nil {
		log.Println("failed to encode response in godot api status", err)
		http.Error(w, "database error", http.StatusInternalServerError)

		return
	}

	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	w.Header().Set("Content-Length", strconv.Itoa(len(encoded)))
	w.WriteHeader(http.StatusOK)
	_, _ = w.Write(encoded)
}

func serveLegacyTestData(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Access-Control-Allow-Origin", "*")

	if r.Method != http.MethodGet && r.Method != http.MethodHead {
		w.Header().Add("Allow", "GET, HEAD")
		http.Error(w, "only GET is allowed", http.StatusMethodNotAllowed)

		return
	}

	dbDone := timing(w, "db")
	var lastSet, lastMatch, lastArcade time.Time
	err := getTestDataLastModified.QueryRow().Scan(&lastSet, &lastMatch, &lastArcade)
	dbDone()
	if err != nil {
		log.Println("failed to get last modified time in godot legacy test data", err)
		http.Error(w, "database error", http.StatusInternalServerError)

		return
	}

	lastSet = lastSet.UTC().Truncate(time.Second)
	lastMatch = lastMatch.UTC().Truncate(time.Second)
	lastArcade = lastArcade.UTC().Truncate(time.Second)

	lastModified := lastSet
	if lastMatch.After(lastModified) {
		lastModified = lastMatch
	}
	if lastArcade.After(lastModified) {
		lastModified = lastArcade
	}

	lastModifiedText := lastModified.UTC().Format(http.TimeFormat)

	w.Header().Set("Cache-Control", "public, max-age=3600, stale-while-revalidate=7200")
	w.Header().Set("Last-Modified", lastModifiedText)
	if ifModifiedSince, err := time.Parse(http.TimeFormat, r.Header.Get("If-Modified-Since")); err == nil && !ifModifiedSince.Before(lastModified) {
		w.WriteHeader(http.StatusNotModified)
		return
	}

	zipDone := timing(w, "zip")
	var buf bytes.Buffer
	z := zip.NewWriter(&buf)

	_, err = z.CreateHeader(&zip.FileHeader{
		Name:     "game_mode/",
		Modified: lastSet,
	})
	if err != nil {
		log.Println("failed to encode zip file in godot legacy test data", err)
		http.Error(w, "database error", http.StatusInternalServerError)

		return
	}

	dbDone = timing(w, "db")
	rows, err := getTestDataLegacyCustomCardSets.Query()
	if err != nil {
		log.Println("failed to get card sets in godot legacy test data", err)
		http.Error(w, "database error", http.StatusInternalServerError)

		return
	}
	defer rows.Close()
	for rows.Next() {
		var (
			clientSlug string
			revision   int
			uploadedAt time.Time
			data       string
		)

		err = rows.Scan(&clientSlug, &revision, &uploadedAt, &data)
		if err != nil {
			log.Println("failed to get card sets in godot legacy test data", err)
			http.Error(w, "database error", http.StatusInternalServerError)

			return
		}

		zw, err := z.CreateHeader(&zip.FileHeader{
			Name:     fmt.Sprintf("game_mode/%s.%d", clientSlug, revision),
			Modified: uploadedAt.UTC().Truncate(time.Second),
			Method:   zip.Deflate,
		})
		if err != nil {
			log.Println("failed to encode zip file in godot legacy test data", err)
			http.Error(w, "database error", http.StatusInternalServerError)

			return
		}
		_, err = io.WriteString(zw, data)
		if err != nil {
			log.Println("failed to encode zip file in godot legacy test data", err)
			http.Error(w, "database error", http.StatusInternalServerError)

			return
		}
	}
	err = rows.Err()
	if err != nil {
		log.Println("failed to get card sets in godot legacy test data", err)
		http.Error(w, "database error", http.StatusInternalServerError)

		return
	}
	dbDone()

	_, err = z.CreateHeader(&zip.FileHeader{
		Name:     "card_recording/",
		Modified: lastMatch,
	})
	if err != nil {
		log.Println("failed to encode zip file in godot legacy test data", err)
		http.Error(w, "database error", http.StatusInternalServerError)

		return
	}

	dbDone = timing(w, "db")
	rows, err = getTestDataLegacyMatchRecordings.Query()
	if err != nil {
		log.Println("failed to get match recordings in godot legacy test data", err)
		http.Error(w, "database error", http.StatusInternalServerError)

		return
	}
	defer rows.Close()
	for rows.Next() {
		var (
			id         uint64
			hash       []byte
			uploadedAt time.Time
			data       []byte
		)

		err = rows.Scan(&id, &hash, &uploadedAt, &data)
		if err != nil {
			log.Println("failed to get match recordings in godot legacy test data", err)
			http.Error(w, "database error", http.StatusInternalServerError)

			return
		}

		var fileID format.FileID
		fileID.ID = id
		copy(fileID.Hash[:], hash)

		zw, err := z.CreateHeader(&zip.FileHeader{
			Name:     "card_recording/" + fileID.String(),
			Modified: uploadedAt.UTC().Truncate(time.Second),
			Method:   zip.Deflate,
		})
		if err != nil {
			log.Println("failed to encode zip file in godot legacy test data", err)
			http.Error(w, "database error", http.StatusInternalServerError)

			return
		}
		_, err = zw.Write(data)
		if err != nil {
			log.Println("failed to encode zip file in godot legacy test data", err)
			http.Error(w, "database error", http.StatusInternalServerError)

			return
		}
	}
	err = rows.Err()
	if err != nil {
		log.Println("failed to get match recordings in godot legacy test data", err)
		http.Error(w, "database error", http.StatusInternalServerError)

		return
	}
	dbDone()

	_, err = z.CreateHeader(&zip.FileHeader{
		Name:     "arcade_recording/",
		Modified: lastArcade,
	})
	if err != nil {
		log.Println("failed to encode zip file in godot legacy test data", err)
		http.Error(w, "database error", http.StatusInternalServerError)

		return
	}

	dbDone = timing(w, "db")
	rows, err = getTestDataLegacyArcadeRecordings.Query()
	if err != nil {
		log.Println("failed to get arcade recordings in godot legacy test data", err)
		http.Error(w, "database error", http.StatusInternalServerError)

		return
	}
	defer rows.Close()
	for rows.Next() {
		var (
			id         uint64
			hash       []byte
			uploadedAt time.Time
			data       []byte
		)

		err = rows.Scan(&id, &hash, &uploadedAt, &data)
		if err != nil {
			log.Println("failed to get arcade recordings in godot legacy test data", err)
			http.Error(w, "database error", http.StatusInternalServerError)

			return
		}

		var fileID format.FileID
		fileID.ID = id
		copy(fileID.Hash[:], hash)

		zw, err := z.CreateHeader(&zip.FileHeader{
			Name:     "arcade_recording/" + fileID.String(),
			Modified: uploadedAt.UTC().Truncate(time.Second),
			Method:   zip.Deflate,
		})
		if err != nil {
			log.Println("failed to encode zip file in godot legacy test data", err)
			http.Error(w, "database error", http.StatusInternalServerError)

			return
		}
		_, err = zw.Write(data)
		if err != nil {
			log.Println("failed to encode zip file in godot legacy test data", err)
			http.Error(w, "database error", http.StatusInternalServerError)

			return
		}
	}
	err = rows.Err()
	if err != nil {
		log.Println("failed to get arcade recordings in godot legacy test data", err)
		http.Error(w, "database error", http.StatusInternalServerError)

		return
	}
	dbDone()

	err = z.Close()
	if err != nil {
		log.Println("failed to encode zip file in godot legacy test data", err)
		http.Error(w, "database error", http.StatusInternalServerError)

		return
	}
	zipDone()

	w.Header().Set("Content-Type", "application/zip")
	w.Header().Set("Content-Length", strconv.Itoa(buf.Len()))
	_, _ = w.Write(buf.Bytes())
}

var userImageInternalContentTypes = [...]string{
	"image/png",
	"audio/ogg; codecs=opus",
	"model/gltf-binary",
	"application/vnd.spycards.datacontainer",
	"", // local PNG
	"", // local Opus
	"", // local GLB
	"", // local DataContainer
}

func serveUserImgGodot(w http.ResponseWriter, r *http.Request) {
	w.Header().Add("Access-Control-Allow-Origin", "*")

	if r.Method != http.MethodGet && r.Method != http.MethodHead {
		w.Header().Add("Allow", "GET, HEAD")
		http.Error(w, "only GET is allowed", http.StatusMethodNotAllowed)

		return
	}

	fileIDEnc := r.URL.Path[len("/spy-cards/godot-fileapi/"):]

	var fileID format.FileID

	err := fileID.UnmarshalText([]byte(fileIDEnc))
	if err != nil {
		w.Header().Set("Cache-Control", "public, max-age=86400")
		http.NotFound(w, r)

		return
	}

	if fileID.Type >= uint64(len(userImageInternalContentTypes)) {
		w.Header().Set("Cache-Control", "public, max-age=86400")
		http.NotFound(w, r)

		return
	}

	if userImageInternalContentTypes[fileID.Type] == "" {
		w.Header().Set("Cache-Control", "public, max-age=86400")
		http.NotFound(w, r)

		return
	}

	var fileHash, fileData []byte

	dbDone := timing(w, "db")
	err = getUserImage.QueryRow(fileID.ID, fileID.Type).Scan(&fileHash, &fileData)
	dbDone()
	if errors.Is(err, sql.ErrNoRows) {
		w.Header().Set("Cache-Control", "public, max-age=600")
		http.NotFound(w, r)

		return
	}

	if err != nil {
		log.Println("error getting", r.URL.Path, err)
		w.Header().Set("Cache-Control", "no-store")
		http.Error(w, "database error", http.StatusInternalServerError)

		return
	}

	if !bytes.Equal(fileID.Hash[:], fileHash[:8]) {
		w.Header().Set("Cache-Control", "public, max-age=86400")
		http.NotFound(w, r)

		return
	}

	w.Header().Set("Cache-Control", "public, max-age=31536000, immutable")
	w.Header().Set("Content-Type", userImageInternalContentTypes[fileID.Type])
	w.Header().Set("Content-Length", strconv.Itoa(len(fileData)))
	w.Header().Set("Content-Digest", "sha-256=:"+base64.StdEncoding.EncodeToString(fileHash)+":")
	w.WriteHeader(http.StatusOK)

	if r.Method == http.MethodGet {
		_, _ = w.Write(fileData)
	}
}
