package server

import (
	"bytes"
	"crypto/sha256"
	"database/sql"
	"encoding/json"
	"errors"
	"image"
	"image/png"
	"io"
	"log"
	"math/bits"
	"net/http"
	"strconv"
	"strings"

	"git.lubar.me/ben/spy-cards/card"
	"git.lubar.me/ben/spy-cards/format"
	"git.lubar.me/ben/spy-cards/internal/webp"
	"github.com/jackc/pgtype"
	"golang.org/x/image/draw"
)

const (
	formatPNG  = "png"
	formatWebP = "webp"
)

func uploadUserImg(w http.ResponseWriter, r *http.Request) {
	w.Header().Add("Cache-Control", "no-store")

	if !validateOrigin(w, r) {
		return
	}

	w.Header().Add("Access-Control-Allow-Methods", http.MethodPut)
	w.Header().Add("Access-Control-Allow-Headers", "content-type")

	if r.Method == http.MethodOptions {
		if r.Header.Get("Access-Control-Request-Method") != http.MethodPut {
			w.WriteHeader(http.StatusBadRequest)

			return
		}

		w.WriteHeader(http.StatusNoContent)

		return
	}

	if r.Method != http.MethodPut {
		w.Header().Add("Allow", "PUT, OPTIONS")
		http.Error(w, "only PUT is allowed", http.StatusMethodNotAllowed)

		return
	}

	defer r.Body.Close()

	readDone := timing(w, "read")

	// 128 KiB max file size
	// (128x128 32-bit pixels without metadata is half of that)
	const maxSize = 128 << 10

	var buf bytes.Buffer

	_, err := io.CopyN(&buf, r.Body, maxSize+1)
	if err == nil {
		log.Println("user png too big", r.Header.Get("Content-Type"), r.Header.Get("Content-Length"))
		http.Error(w, "file too large", http.StatusRequestEntityTooLarge)

		return
	}

	if !errors.Is(err, io.EOF) {
		log.Println("upload err", err)
		http.Error(w, err.Error(), http.StatusInternalServerError)

		return
	}

	config, imgFmt, err := image.DecodeConfig(bytes.NewReader(buf.Bytes()))
	if err != nil {
		log.Println("failed to decode user image", strconv.Quote(r.Header.Get("Content-Type")), err)
		http.Error(w, "invalid image format", http.StatusBadRequest)

		return
	}

	readDone()

	if imgFmt != formatPNG {
		log.Println("bad user image type", strconv.Quote(r.Header.Get("Content-Type")), imgFmt)
		http.Error(w, "invalid image format", http.StatusBadRequest)

		return
	}

	if config.Height > 512 || config.Width > 512 {
		log.Println("user png dims", config.Width, "x", config.Height)
		// for safety (file size is already checked)
		http.Error(w, "image dimensions too large", http.StatusBadRequest)

		return
	}

	hash := sha256.Sum256(buf.Bytes())

	dbDone := timing(w, "db")

	var fileID format.FileID

	copy(fileID.Hash[:], hash[:])

	if err = getUserImageByHash.QueryRow(hash[:], 0).Scan(&fileID.ID); err != nil {
		err = insertUserImage.QueryRow(r.Header.Get("X-Forwarded-For"), hash[:], buf.Bytes(), 0).Scan(&fileID.ID)
		if err != nil {
			log.Println("upload store err", err)
			http.Error(w, err.Error(), http.StatusInternalServerError)

			return
		}
	}

	dbDone()

	w.Header().Add("Content-Type", "text/plain")
	w.WriteHeader(http.StatusCreated)

	b, _ := fileID.MarshalText()
	_, _ = w.Write(b)
}

func serveUserImg(w http.ResponseWriter, r *http.Request) {
	w.Header().Add("Access-Control-Allow-Origin", "*")

	if r.Method != http.MethodGet && r.Method != http.MethodHead {
		w.Header().Add("Allow", "GET, HEAD")
		http.Error(w, "only GET is allowed", http.StatusMethodNotAllowed)

		return
	}

	wantFmt := formatPNG
	if strings.HasSuffix(r.URL.Path, ".webp") {
		wantFmt = formatWebP
	}

	if !strings.HasSuffix(r.URL.Path, "."+wantFmt) {
		w.Header().Set("Cache-Control", "public, max-age=86400")
		http.NotFound(w, r)

		return
	}

	fileIDEnc := r.URL.Path[len("/spy-cards/user-img/"):]
	fileIDEnc = fileIDEnc[:len(fileIDEnc)-len(wantFmt)-1]

	var fileID format.FileID

	err := fileID.UnmarshalText([]byte(fileIDEnc))
	if err != nil || fileID.Type != 0 {
		w.Header().Set("Cache-Control", "public, max-age=86400")
		http.NotFound(w, r)

		return
	}

	dbDone := timing(w, "db")

	var fileHash, fileData []byte

	err = getUserImage.QueryRow(fileID.ID, fileID.Type).Scan(&fileHash, &fileData)
	if errors.Is(err, sql.ErrNoRows) {
		w.Header().Set("Cache-Control", "public, max-age=600")
		http.NotFound(w, r)

		return
	}

	if err != nil {
		log.Println("error getting", r.URL.Path, err)
		w.Header().Set("Cache-Control", "no-store")
		http.Error(w, "database error", http.StatusInternalServerError)

		return
	}

	if !bytes.Equal(fileID.Hash[:], fileHash[:8]) {
		w.Header().Set("Cache-Control", "public, max-age=86400")
		http.NotFound(w, r)

		return
	}

	dbDone()

	_, actualFmt, err := image.DecodeConfig(bytes.NewReader(fileData))
	if err != nil {
		log.Println("error decoding", r.URL.Path, err)
		w.Header().Set("Cache-Control", "no-store")
		http.Error(w, "database error", http.StatusInternalServerError)

		return
	}

	if actualFmt != wantFmt {
		recodeDone := timing(w, "recode")

		w.Header().Add("X-Original-Content-Type", "image/"+actualFmt)

		img, _, err := image.Decode(bytes.NewReader(fileData))
		if err == nil {
			switch wantFmt {
			case formatPNG:
				var buf bytes.Buffer
				err = png.Encode(&buf, img)
				fileData = buf.Bytes()
			case formatWebP:
				fileData, err = webp.EncodeRGBA(img, 75)
			default:
				panic("unhandled wantFmt: " + wantFmt)
			}
		}

		if err != nil {
			log.Println("error decoding", r.URL.Path, err)
			w.Header().Set("Cache-Control", "no-store")
			http.Error(w, "database error", http.StatusInternalServerError)

			return
		}

		recodeDone()
	}

	w.Header().Set("Cache-Control", "public, max-age=31536000, immutable")
	w.Header().Set("Content-Type", "image/"+wantFmt)
	w.Header().Set("Content-Length", strconv.Itoa(len(fileData)))
	w.WriteHeader(http.StatusOK)

	if r.Method == http.MethodGet {
		_, _ = w.Write(fileData)
	}
}

func getUserImgCommunityList(w http.ResponseWriter, r *http.Request) {
	w.Header().Add("Access-Control-Allow-Origin", "*")

	if r.Method != http.MethodGet && r.Method != http.MethodHead {
		w.Header().Add("Allow", "GET, HEAD")
		http.Error(w, "only GET is allowed", http.StatusMethodNotAllowed)

		return
	}

	dbDone := timing(w, "db")

	rows, err := getCommunityPortraits.Query()
	if err != nil {
		log.Println("community portrait list get", err)
		http.Error(w, "database error", http.StatusInternalServerError)

		return
	}
	defer rows.Close()

	var script bytes.Buffer

	_, _ = script.WriteString("[")

	first := true

	for rows.Next() {
		var (
			id   format.FileID
			hash []byte
		)

		err = rows.Scan(&hash, &id.ID)
		if err != nil {
			log.Println("community portrait list get", err)
			http.Error(w, "database error", http.StatusInternalServerError)

			return
		}

		copy(id.Hash[:], hash)

		if first {
			first = false
		} else {
			_, _ = script.WriteString(", ")
		}

		_ = script.WriteByte('"')
		b, _ := id.MarshalText()
		_, _ = script.Write(b)
		_ = script.WriteByte('"')
	}

	_, _ = script.WriteString("]\n")

	if err := rows.Err(); err != nil {
		log.Println("community portrait list get", err)
		http.Error(w, "database error", http.StatusInternalServerError)

		return
	}

	dbDone()

	w.Header().Set("Cache-Control", "public, max-age=3600, stale-while-revalidate=86400")
	w.Header().Set("Content-Type", "application/json")
	w.Header().Set("Content-Length", strconv.Itoa(script.Len()))
	w.WriteHeader(http.StatusOK)

	if r.Method == http.MethodGet {
		_, _ = script.WriteTo(w)
	}
}

func getUserImgTanjerinList(w http.ResponseWriter, r *http.Request) {
	w.Header().Add("Access-Control-Allow-Origin", "*")

	if r.Method != http.MethodGet && r.Method != http.MethodHead {
		w.Header().Add("Allow", "GET, HEAD")
		http.Error(w, "only GET is allowed", http.StatusMethodNotAllowed)

		return
	}

	dbDone := timing(w, "db")

	rows, err := getTanjerinUserImages.Query()
	if err != nil {
		log.Println("tanjerin get", err)
		http.Error(w, "database error", http.StatusInternalServerError)

		return
	}
	defer rows.Close()

	var ids []string

	for rows.Next() {
		var (
			id       format.FileID
			hash     []byte
			tanjerin bool
		)

		err = rows.Scan(&hash, &id.ID, &tanjerin)
		if err != nil {
			log.Println("tanjerin get", err)
			http.Error(w, "database error", http.StatusInternalServerError)

			return
		}

		copy(id.Hash[:], hash)

		b, _ := id.MarshalText()
		formatted := string(b)

		if !tanjerin {
			formatted = "!" + formatted
		}

		ids = append(ids, formatted)
	}

	if err := rows.Err(); err != nil {
		log.Println("tanjerin get", err)
		http.Error(w, "database error", http.StatusInternalServerError)

		return
	}

	dbDone()

	b, err := json.Marshal(ids)
	if err != nil {
		panic(err)
	}

	w.Header().Set("Cache-Control", "public, max-age=3600, stale-while-revalidate=86400")
	w.Header().Set("Content-Type", "application/json")
	w.Header().Set("Content-Length", strconv.Itoa(len(b)))
	w.WriteHeader(http.StatusOK)

	if r.Method == http.MethodGet {
		_, _ = w.Write(b)
	}
}

func serveModePortraitSheet(w http.ResponseWriter, r *http.Request) {
	mode := strings.TrimPrefix(r.URL.Path, "/spy-cards/user-img/mode/")

	if !strings.HasSuffix(mode, ".webp") {
		http.NotFound(w, r)

		return
	}

	mode = strings.TrimSuffix(mode, ".webp")

	dot := strings.Index(mode, ".")
	if dot == -1 {
		http.NotFound(w, r)

		return
	}

	rev, err := strconv.ParseInt(mode[dot+1:], 10, 64)
	if err != nil || rev <= 0 {
		http.NotFound(w, r)

		return
	}

	w.Header().Add("Access-Control-Allow-Origin", "*")
	w.Header().Add("Cache-Control", "no-store")

	dbDone := timing(w, "db")

	var (
		modeName     string
		modeCards    string
		modeRevision int64
	)

	if err := getCustomCardSet.QueryRow(mode[:dot], rev).Scan(&modeName, &modeCards, &modeRevision); err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			http.NotFound(w, r)

			return
		}

		log.Println("mode portrait sheet db err:", err)
		http.Error(w, "database error", http.StatusInternalServerError)

		return
	}

	var set card.Set

	if err := set.UnmarshalText([]byte(modeCards)); err != nil {
		log.Println("mode portrait sheet card err:", err)
		http.Error(w, "could not parse game mode", http.StatusInternalServerError)

		return
	}

	set.BuildPortraitIndex()

	if len(set.PortraitIndex) == 0 {
		w.Header().Set("Cache-Control", "public, max-age=31536000, immutable")

		http.Error(w, "there are no custom portraits for this game mode", http.StatusNotFound)

		return
	}

	var (
		img    *image.RGBA
		perRow int
	)

	switch bits.Len(uint(len(set.PortraitIndex) - 1)) {
	case 0:
		img = image.NewRGBA(image.Rect(0, 0, 128, 128))
		perRow = 1
	case 1:
		img = image.NewRGBA(image.Rect(0, 0, 256, 128))
		perRow = 2
	case 2:
		img = image.NewRGBA(image.Rect(0, 0, 256, 256))
		perRow = 2
	case 3:
		img = image.NewRGBA(image.Rect(0, 0, 512, 256))
		perRow = 4
	case 4:
		img = image.NewRGBA(image.Rect(0, 0, 512, 512))
		perRow = 4
	case 5:
		img = image.NewRGBA(image.Rect(0, 0, 1024, 512))
		perRow = 8
	case 6:
		img = image.NewRGBA(image.Rect(0, 0, 1024, 1024))
		perRow = 8
	case 7:
		img = image.NewRGBA(image.Rect(0, 0, 2048, 1024))
		perRow = 16
	case 8:
		img = image.NewRGBA(image.Rect(0, 0, 2048, 2048))
		perRow = 16
	case 9:
		img = image.NewRGBA(image.Rect(0, 0, 4096, 2048))
		perRow = 32
	case 10:
		img = image.NewRGBA(image.Rect(0, 0, 4096, 4096))
		perRow = 32
	default:
		http.Error(w, "number of custom portraits ("+strconv.Itoa(len(set.PortraitIndex))+") exceeds maximum for this endpoint", http.StatusInternalServerError)

		return
	}

	portraitIDs := make([]int64, 0, len(set.PortraitIndex))

	for id := range set.PortraitIndex {
		var fid format.FileID

		if err := fid.UnmarshalBinary([]byte(id)); err != nil {
			log.Println("mode portrait sheet dec id err:", err)
			http.Error(w, "failed to encode portrait sheet", http.StatusInternalServerError)

			return
		}

		portraitIDs = append(portraitIDs, int64(fid.ID))
	}

	var portraitIDsPG pgtype.Int8Array

	if err := portraitIDsPG.Set(portraitIDs); err != nil {
		panic(err)
	}

	rows, err := getUserImages.Query(&portraitIDsPG, 0)
	if err != nil {
		log.Println("mode portrait sheet db err:", err)
		http.Error(w, "database error", http.StatusInternalServerError)

		return
	}
	defer rows.Close()

	for rows.Next() {
		var (
			fid     format.FileID
			hash    []byte
			imgData []byte
		)

		if err := rows.Scan(&fid.ID, &hash, &imgData); err != nil {
			log.Println("mode portrait sheet db err:", err)
			http.Error(w, "database error", http.StatusInternalServerError)

			return
		}

		copy(fid.Hash[:], hash)

		bfid, err := fid.MarshalBinary()
		if err != nil {
			panic(err)
		}

		i, ok := set.PortraitIndex[string(bfid)]
		if !ok {
			log.Println("mode portrait sheet db returned portrait", fid.ID, "which is not part of", mode)
			http.Error(w, "database error", http.StatusInternalServerError)

			return
		}

		x, y := i%perRow, i/perRow
		x, y = x*128, y*128

		p, _, err := image.Decode(bytes.NewReader(imgData))
		if err != nil {
			log.Println("mode portrait sheet decoding", fid.ID, "err:", err)
			http.Error(w, "database error", http.StatusInternalServerError)

			return
		}

		draw.CatmullRom.Scale(img, image.Rect(x, y, x+128, y+128), p, p.Bounds(), draw.Src, nil)
	}

	if err := rows.Err(); err != nil {
		log.Println("mode portrait sheet db err:", err)
		http.Error(w, "database error", http.StatusInternalServerError)

		return
	}

	dbDone()

	recodeDone := timing(w, "recode")

	b, err := webp.EncodeRGBA(img, 75)
	if err != nil {
		log.Println("mode portrait sheet enc err:", err)
		http.Error(w, "failed to encode portrait sheet", http.StatusInternalServerError)

		return
	}

	recodeDone()

	w.Header().Set("Cache-Control", "public, max-age=31536000, immutable")
	w.Header().Set("Content-Type", "image/webp")
	w.Header().Set("Content-Length", strconv.Itoa(len(b)))

	w.WriteHeader(http.StatusOK)

	_, _ = w.Write(b)
}
