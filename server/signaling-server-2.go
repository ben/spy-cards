package server

import (
	"crypto/rand"
	"io"
	"net/http"
	"strconv"
	"sync"
	"time"

	"git.lubar.me/ben/spy-cards/format"
)

var matchmakingSessions = make(map[string]*MatchmakingSession)
var matchmakingLock sync.Mutex

const MatchmakingCodeLength = 6
const MatchmakingCreateAttempts = 100
const MatchmakingCodeCharacters = format.Crockford32Encoding
const MatchmakingPollInterval = 15 * time.Second
const MatchmakingSessionExpiry = 5 * time.Minute
const MatchmakingSessionMinPlayers = 2
const MatchmakingSessionMaxPlayers = 255
const MatchmakingVerificationCodeLength = 20

type MatchmakingSession struct {
	LastSeen    time.Time
	PlayerLimit int
	Players     []*[MatchmakingVerificationCodeLength]byte

	// the order of this slice is:
	// 0: p1 + p2
	// 1: p1 + p3
	// 2: p2 + p3
	// 3: p1 + p4
	// 4: p2 + p4
	// 5: p3 + p4
	// etc.
	//
	// That is, for two players numbered H and J, H < J,
	// their index in the slice is (J-2) * (J-2) + (H-1).
	PlayerPairs []MatchmakingPlayerPair
}

type MatchmakingPlayerPair struct {
	Host   string
	HostCh chan struct{}
	Join   string
	JoinCh chan struct{}
}

func randomMatchmakingCode() string {
	var buf [MatchmakingCodeLength]byte
	_, err := rand.Read(buf[:])
	if err != nil {
		panic(err)
	}

	for i := range buf {
		// this probably isn't uniformly random but whatever
		buf[i] = MatchmakingCodeCharacters[int(buf[i])%len(MatchmakingCodeCharacters)]
	}

	return string(buf[:])
}

func handleMatchmakingCreateSession(w http.ResponseWriter, r *http.Request) {
	w.Header().Add("Cache-Control", "no-store")

	if r.Method != http.MethodPost {
		w.Header().Add("Allow", http.MethodPost)
		http.Error(w, "method not allowed", http.StatusMethodNotAllowed)

		return
	}

	cmd := r.PostFormValue("cmd")
	if cmd == "" {
		http.Error(w, "missing required parameter", http.StatusBadRequest)

		return
	}

	maxPlayers, err := strconv.Atoi(r.PostFormValue("maxplayers"))
	if err != nil || maxPlayers < MatchmakingSessionMinPlayers || maxPlayers > MatchmakingSessionMaxPlayers {
		http.Error(w, "missing required parameter", http.StatusBadRequest)

		return
	}

	if len(cmd) >= 1e9 {
		http.Error(w, "command too large for buffer", http.StatusBadRequest)

		return
	}

	matchmakingLock.Lock()
	defer matchmakingLock.Unlock()

	cleanExpiredMatchmakingSessions()

	for attempt := 0; attempt < MatchmakingCreateAttempts; attempt++ {
		code := randomMatchmakingCode()
		_, ok := matchmakingSessions[code]
		if !ok {
			session := &MatchmakingSession{
				LastSeen:    time.Now(),
				PlayerLimit: maxPlayers,
				Players:     make([]*[MatchmakingVerificationCodeLength]byte, maxPlayers),
				PlayerPairs: make([]MatchmakingPlayerPair, (maxPlayers-3)*(maxPlayers-3)+(maxPlayers-2+1)),
			}

			for i := range session.PlayerPairs {
				session.PlayerPairs[i].HostCh = make(chan struct{}, 1)
				session.PlayerPairs[i].JoinCh = make(chan struct{}, 1)
			}

			var verification [MatchmakingVerificationCodeLength]byte
			_, err := rand.Read(verification[:])
			if err != nil {
				panic(err)
			}

			session.Players[0] = &verification
			session.PlayerPairs[0].Host = cmd + "\n"
			session.PlayerPairs[0].HostCh <- struct{}{}

			matchmakingSessions[code] = session

			codeAndVerification := code + "\n" + format.Encode32(verification[:])

			w.Header().Set("Content-Type", "text/plain; charset=utf-8")
			w.Header().Set("Content-Length", strconv.Itoa(len(codeAndVerification)))
			w.WriteHeader(http.StatusCreated)
			_, _ = io.WriteString(w, codeAndVerification)

			return
		}
	}

	http.Error(w, "could not allocate a matchmaking session ID", http.StatusInternalServerError)
}

// lock must already be held when this is called
func cleanExpiredMatchmakingSessions() {
	for code, session := range matchmakingSessions {
		if time.Since(session.LastSeen) > MatchmakingSessionExpiry {
			for _, pair := range session.PlayerPairs {
				close(pair.HostCh)
				close(pair.JoinCh)
			}
			delete(matchmakingSessions, code)
		}
	}
}

func handleMatchmakingJoin(w http.ResponseWriter, r *http.Request) {
	w.Header().Add("Cache-Control", "no-store")

	if r.Method != http.MethodPost {
		w.Header().Add("Allow", http.MethodPost)
		http.Error(w, "method not allowed", http.StatusMethodNotAllowed)

		return
	}

	code := format.CleanCrockford32(r.PostFormValue("code"))
	if len(code) != MatchmakingCodeLength {
		http.Error(w, "missing required parameter", http.StatusBadRequest)

		return
	}

	matchmakingLock.Lock()
	defer matchmakingLock.Unlock()

	cleanExpiredMatchmakingSessions()

	session := matchmakingSessions[code]
	if session == nil {
		http.Error(w, "match not found", http.StatusNotFound)

		return
	}

	session.LastSeen = time.Now()
	for i := range session.Players {
		if session.Players[i] != nil {
			continue
		}

		var verification [MatchmakingVerificationCodeLength]byte
		_, err := rand.Read(verification[:])
		if err != nil {
			panic(err)
		}

		session.Players[i] = &verification

		playerNumberAndVerification := strconv.Itoa(session.PlayerLimit) + "\n" + strconv.Itoa(i+1) + "\n" + format.Encode32(verification[:])

		w.Header().Set("Content-Type", "text/plain; charset=utf-8")
		w.Header().Set("Content-Length", strconv.Itoa(len(playerNumberAndVerification)))
		w.WriteHeader(http.StatusOK)
		_, _ = io.WriteString(w, playerNumberAndVerification)

		return
	}

	http.Error(w, "match already joined by the maximum number of players", http.StatusNotFound)
}

func handleMatchmakingPoll(w http.ResponseWriter, r *http.Request) {
	w.Header().Add("Cache-Control", "no-store")

	if r.Method != http.MethodPost {
		w.Header().Add("Allow", http.MethodPost)
		http.Error(w, "method not allowed", http.StatusMethodNotAllowed)

		return
	}

	code := format.CleanCrockford32(r.PostFormValue("code"))
	if len(code) != MatchmakingCodeLength {
		http.Error(w, "missing required parameter", http.StatusBadRequest)

		return
	}

	from, err := strconv.Atoi(r.PostFormValue("from"))
	if err != nil || from <= 0 || from > MatchmakingSessionMaxPlayers {
		http.Error(w, "missing required parameter", http.StatusBadRequest)

		return
	}

	to, err := strconv.Atoi(r.PostFormValue("to"))
	if err != nil || to == from || to <= 0 || to > MatchmakingSessionMaxPlayers {
		http.Error(w, "missing required parameter", http.StatusBadRequest)

		return
	}

	host, join := from, to
	if host > join {
		host, join = join, host
	}

	pair := (join-2)*(join-2) + (host - 1)

	matchmakingLock.Lock()

	cleanExpiredMatchmakingSessions()

	session := matchmakingSessions[code]
	if session == nil {
		matchmakingLock.Unlock()

		http.Error(w, "match not found", http.StatusNotFound)

		return
	}

	if from > session.PlayerLimit || to > session.PlayerLimit {
		matchmakingLock.Unlock()

		http.Error(w, "match not found", http.StatusNotFound)

		return
	}

	// there's a timing attack here but the $5 Wrench Method is much easier
	verify := r.PostFormValue("verify")
	if session.Players[from-1] == nil || verify != format.Encode32(session.Players[from-1][:]) {
		matchmakingLock.Unlock()

		http.Error(w, "match not found", http.StatusNotFound)

		return
	}

	session.LastSeen = time.Now()

	matchmakingLock.Unlock()

	ch := session.PlayerPairs[pair].HostCh
	if host == from {
		ch = session.PlayerPairs[pair].JoinCh
	}

	w.Header().Set("Content-Type", "text/plain; charset=utf-8")
	w.WriteHeader(http.StatusOK)

	f, ok := w.(http.Flusher)
	if ok {
		f.Flush()
	}

	timeout := time.NewTimer(MatchmakingPollInterval)
	defer timeout.Stop()

	select {
	case <-ch:
	case <-timeout.C:
	case <-r.Context().Done():
	}

	matchmakingLock.Lock()

	message := ""
	if host == from {
		message = session.PlayerPairs[pair].Join
		session.PlayerPairs[pair].Join = ""
	} else {
		message = session.PlayerPairs[pair].Host
		session.PlayerPairs[pair].Host = ""
	}

	matchmakingLock.Unlock()

	_, _ = io.WriteString(w, message)
}

func handleMatchmakingSend(w http.ResponseWriter, r *http.Request) {
	w.Header().Add("Cache-Control", "no-store")

	if r.Method != http.MethodPost {
		w.Header().Add("Allow", http.MethodPost)
		http.Error(w, "method not allowed", http.StatusMethodNotAllowed)

		return
	}

	code := format.CleanCrockford32(r.PostFormValue("code"))
	if len(code) != MatchmakingCodeLength {
		http.Error(w, "missing required parameter", http.StatusBadRequest)

		return
	}

	from, err := strconv.Atoi(r.PostFormValue("from"))
	if err != nil || from <= 0 || from > MatchmakingSessionMaxPlayers {
		http.Error(w, "missing required parameter", http.StatusBadRequest)

		return
	}

	to, err := strconv.Atoi(r.PostFormValue("to"))
	if err != nil || to == from || to <= 0 || to > MatchmakingSessionMaxPlayers {
		http.Error(w, "missing required parameter", http.StatusBadRequest)

		return
	}

	host, join := from, to
	if host > join {
		host, join = join, host
	}

	pair := (join-2)*(join-2) + (host - 1)

	cmd := r.PostFormValue("cmd")
	if cmd == "" {
		http.Error(w, "missing required parameter", http.StatusBadRequest)

		return
	}

	if len(cmd) >= 1e9 {
		http.Error(w, "command too large for buffer", http.StatusBadRequest)

		return
	}

	matchmakingLock.Lock()
	defer matchmakingLock.Unlock()

	cleanExpiredMatchmakingSessions()

	session := matchmakingSessions[code]
	if session == nil {
		http.Error(w, "match not found", http.StatusNotFound)

		return
	}

	if from > session.PlayerLimit || to > session.PlayerLimit {
		http.Error(w, "match not found", http.StatusNotFound)

		return
	}

	// there's a timing attack here but the $5 Wrench Method is much easier
	verify := r.PostFormValue("verify")
	if session.Players[from-1] == nil || verify != format.Encode32(session.Players[from-1][:]) {
		http.Error(w, "match not found", http.StatusNotFound)

		return
	}

	session.LastSeen = time.Now()

	if host == from {
		if len(session.PlayerPairs[pair].Host)+len(cmd) >= 1e9 {
			http.Error(w, "command too large for buffer", http.StatusBadRequest)

			return
		}

		session.PlayerPairs[pair].Host += cmd + "\n"
		select {
		case session.PlayerPairs[pair].HostCh <- struct{}{}:
		default:
		}
	} else {
		if len(session.PlayerPairs[pair].Join)+len(cmd) >= 1e9 {
			http.Error(w, "command too large for buffer", http.StatusBadRequest)

			return
		}

		session.PlayerPairs[pair].Join += cmd + "\n"
		select {
		case session.PlayerPairs[pair].JoinCh <- struct{}{}:
		default:
		}
	}

	w.Header().Set("Content-Type", "text/plain; charset=utf-8")
	w.Header().Set("Content-Length", "0")
	w.WriteHeader(http.StatusAccepted)
}
