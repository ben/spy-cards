package festival

import (
	"fmt"

	"git.lubar.me/ben/spy-cards/gfx/sprites"
	"git.lubar.me/ben/spy-cards/screenreader"
)

func (g *Game) MakeScreenReaderUI() *screenreader.UI {
	return &screenreader.UI{
		Elements: []screenreader.Element{
			&screenreader.Wrapper{
				ElementBase: screenreader.ElementBase{
					ComputeVisible: func() bool {
						return g.CurrentLoc == LocationFarm || g.CurrentLoc == LocationArena || g.CurrentLoc == LocationKut
					},
				},
				Elements: []screenreader.Element{
					&screenreader.Label{
						ElementPosition: screenreader.ElementPosition{
							Camera: &g.textCam,
							X:      -315.5,
							Y:      179.9,
							Z:      0,
							Sx:     40,
							Sy:     40,
						},
						Font:   sprites.FontD3Streetism,
						Center: true,
						Text: func() string {
							return fmt.Sprintf("Your Aphid is Level %d", g.Aphid.Level)
						},
						VisText: func() string {
							return fmt.Sprintf("Level %d", g.Aphid.Level)
						},
					},
					&screenreader.Label{
						ElementPosition: screenreader.ElementPosition{
							Camera: &g.textCam,
							X:      -335.5,
							Y:      139.9,
							Z:      0,
							Sx:     40,
							Sy:     40,
						},
						Font: sprites.FontD3Streetism,
						Text: func() string {
							return fmt.Sprintf("%d%% Health", g.Aphid.Health)
						},
						VisText: func() string {
							return fmt.Sprintf("%d%%", g.Aphid.Health)
						},
					},
					&screenreader.Label{
						ElementPosition: screenreader.ElementPosition{
							Camera: &g.textCam,
							X:      -335.5,
							Y:      89.9,
							Z:      0,
							Sx:     40,
							Sy:     40,
						},
						Font: sprites.FontD3Streetism,
						Text: func() string {
							return fmt.Sprintf("%d Berries", g.Aphid.Berries)
						},
						VisText: func() string {
							return fmt.Sprintf("%d", g.Aphid.Berries)
						},
					},
					&screenreader.Button{
						ElementPosition: screenreader.ElementPosition{
							Camera: &g.textCam,
							X:      -201,
							Y:      151,
							Z:      0,
							Sx:     38.4,
							Sy:     38.4,
						},
						Sprite: func() *sprites.Sprite {
							return sprites.BtnHistory
						},
						Text: func() string {
							return "History"
						},
						Click: func() {
							g.CurrentLoc = LocationHistory
						},
					},
					&screenreader.Button{
						ElementPosition: screenreader.ElementPosition{
							Camera: &g.textCam,
							X:      -325,
							Y:      32,
							Z:      0,
							Sx:     38.4,
							Sy:     38.4,
						},
						Sprite: func() *sprites.Sprite {
							return sprites.BtnPibu
						},
						Text: func() string {
							return "Go to Farm"
						},
						Click: func() {
							if g.CurrentLoc != LocationFarm {
								g.CurrentLoc = LocationFarm
								g.Transition = 255
								g.LocAnim = 0
							}
						},
					},
					&screenreader.Button{
						ElementPosition: screenreader.ElementPosition{
							Camera: &g.textCam,
							X:      -325,
							Y:      -34,
							Z:      0,
							Sx:     38.4,
							Sy:     38.4,
						},
						Sprite: func() *sprites.Sprite {
							return sprites.BtnKut
						},
						Text: func() string {
							return "Go to Kitchen"
						},
						Click: func() {
							if g.CurrentLoc != LocationKut {
								g.CurrentLoc = LocationKut
								g.Transition = 255
								g.LocAnim = 0
							}
						},
					},
					&screenreader.Button{
						ElementPosition: screenreader.ElementPosition{
							Camera: &g.textCam,
							X:      -325,
							Y:      -110,
							Z:      0,
							Sx:     29.3,
							Sy:     29.3,
						},
						Sprite: func() *sprites.Sprite {
							return sprites.BtnTanjerin
						},
						Text: func() string {
							return "Go to Arena"
						},
						Click: func() {
							if g.CurrentLoc != LocationArena {
								g.CurrentLoc = LocationArena
								g.Transition = 255
								g.LocAnim = 0
							}
						},
					},
				},
			},
			&screenreader.Button{
				ElementBase: screenreader.ElementBase{
					ComputeVisible: func() bool {
						return g.CurrentLoc == LocationFarm
					},
				},
				ElementPosition: screenreader.ElementPosition{
					Camera: &g.cam,
					X:      -35.11,
					Y:      0.49,
					Z:      1.62,
					Sx:     1,
					Sy:     1,
				},
				Sprite: func() *sprites.Sprite {
					return sprites.AphidBody[g.Aphid.ID-1]
				},
				Text: func() string {
					return "Pet Your Aphid"
				},
				Click: g.aphidClicked,
			},
			&screenreader.Button{
				ElementBase: screenreader.ElementBase{
					ComputeVisible: func() bool {
						return g.CurrentLoc == LocationFarm && g.Aphid.Level >= 10
					},
				},
				ElementPosition: screenreader.ElementPosition{
					Camera: &g.cam,
					X:      -35.11,
					Y:      2,
					Z:      1.62,
					Sx:     1,
					Sy:     1,
				},
				Sprite: func() *sprites.Sprite {
					return sprites.BtnWifeMode
				},
				Text: func() string {
					return "What's This?!"
				},
				Click: g.turnOnWifeMode,
			},
			&screenreader.Wrapper{
				ElementBase: screenreader.ElementBase{
					ComputeVisible: func() bool {
						return g.CurrentLoc == LocationArena
					},
					AriaRole: "alert",
				},
				Elements: []screenreader.Element{
					&screenreader.Paragraph{
						ElementPosition: screenreader.ElementPosition{
							Camera: &g.textCam,
							X:      0,
							Y:      -170,
							Z:      0,
							Sx:     800,
							Sy:     125,
						},
						Sprite: sprites.Blank,
						Text: func() string {
							if g.Aphid.Health <= 1 {
								return tanjerinResource1 + "\n" + tanjerinResource2
							}

							return tanjerinDefault1 + "\n" + tanjerinDefault2
						},
					},
					&screenreader.Button{
						ElementPosition: screenreader.ElementPosition{
							Camera: &g.cam,
							X:      59.6,
							Y:      2,
							Z:      9.49,
							Sx:     -1,
							Sy:     1,
						},
						Sprite: func() *sprites.Sprite {
							if g.LocAnim == 1 {
								return sprites.TanjerinCheer
							}

							return sprites.TanjerinIdle
						},
						Text: func() string {
							return "Tanjerin"
						},
						Click: func() {
							g.LocAnim = 12
						},
					},
				},
			},
			&screenreader.Wrapper{
				ElementBase: screenreader.ElementBase{
					ComputeVisible: func() bool {
						return g.CurrentLoc == LocationKut
					},
					AriaRole: "alert",
				},
				Elements: []screenreader.Element{
					&screenreader.Paragraph{
						ElementPosition: screenreader.ElementPosition{
							Camera: &g.textCam,
							X:      0,
							Y:      -170,
							Z:      0,
							Sx:     800,
							Sy:     125,
						},
						Sprite: sprites.Blank,
						Text: func() string {
							text1, text2 := g.kutText()

							return text1 + "\n" + text2
						},
					},
					&screenreader.Wrapper{
						ElementBase: screenreader.ElementBase{
							ComputeVisible: func() bool {
								return g.LocAnim == 0 || g.LocAnim >= 400
							},
						},
						Elements: []screenreader.Element{
							&screenreader.Button{
								ElementPosition: screenreader.ElementPosition{
									Camera: &g.cam,
									X:      89,
									Y:      2.01,
									Z:      15.92,
									Sx:     1,
									Sy:     1,
								},
								Sprite: func() *sprites.Sprite {
									return sprites.FoodShroomRaw
								},
								Text: func() string {
									return "Cook a Mushroom"
								},
								Click: func() {
									g.cook(FoodShroom)
								},
							},
							&screenreader.Button{
								ElementPosition: screenreader.ElementPosition{
									Camera: &g.cam,
									X:      90.37,
									Y:      2.01,
									Z:      15.92,
									Sx:     1,
									Sy:     1,
								},
								Sprite: func() *sprites.Sprite {
									return sprites.FoodLeafRaw
								},
								Text: func() string {
									return "Cook a Crunchy Leaf"
								},
								Click: func() {
									g.cook(FoodLeaf)
								},
							},
							&screenreader.Button{
								ElementPosition: screenreader.ElementPosition{
									Camera: &g.cam,
									X:      91.61,
									Y:      2.01,
									Z:      15.92,
									Sx:     1,
									Sy:     1,
								},
								Sprite: func() *sprites.Sprite {
									return sprites.FoodHoneyRaw
								},
								Text: func() string {
									return "Cook a Honey Drop"
								},
								Click: func() {
									g.cook(FoodHoney)
								},
							},
						},
					},
				},
			},
			&screenreader.Wrapper{
				ElementBase: screenreader.ElementBase{
					ComputeVisible: func() bool {
						return g.CurrentLoc == LocationHistory
					},
					AriaRole: "alert",
				},
				Elements: []screenreader.Element{
					&screenreader.Paragraph{
						ElementPosition: screenreader.ElementPosition{
							Camera: &g.textCam,
							X:      0,
							Y:      160,
							Z:      0,
							Sx:     800,
							Sy:     125,
						},
						Sprite: sprites.Blank,
						Text: func() string {
							return joyHistory1 + "\n" + joyHistory2
						},
					},
					&screenreader.TextButton{
						ElementPosition: screenreader.ElementPosition{
							Camera: &g.textCam,
							X:      120,
							Y:      -180,
							Z:      0,
							Sx:     72,
							Sy:     72,
						},
						Font:   sprites.FontD3Streetism,
						Center: true,
						Text: func() string {
							return "Go Back"
						},
						VisText: func() string {
							return "Go Back"
						},
						Click: func() {
							g.CurrentLoc = g.PrevLoc
						},
					},
				},
			},
			&screenreader.Wrapper{
				ElementBase: screenreader.ElementBase{
					ComputeVisible: func() bool {
						return g.CurrentLoc == LocationWifeMode
					},
					AriaRole: "alert",
				},
				Elements: []screenreader.Element{
					&screenreader.Paragraph{
						ElementPosition: screenreader.ElementPosition{
							Camera: &g.textCam,
							X:      0,
							Y:      80,
							Z:      0,
							Sx:     800,
							Sy:     175,
						},
						Sprite: sprites.Blank,
						Text: func() string {
							return joySuitor1 + "\n" + joySuitor2 + "\n" + joySuitor3
						},
					},
					&screenreader.Button{
						ElementPosition: screenreader.ElementPosition{
							Camera: &g.textCam,
							X:      -168,
							Y:      -40,
							Z:      0,
							Sx:     -100,
							Sy:     100,
						},
						Sprite: func() *sprites.Sprite {
							return sprites.AphidBody[g.Suitors[0].ID-1]
						},
						Text: func() string {
							return "Select First Suitor"
						},
						Click: func() {
							g.chooseSuitor(0)
						},
					},
					&screenreader.Button{
						ElementPosition: screenreader.ElementPosition{
							Camera: &g.textCam,
							X:      18,
							Y:      -40,
							Z:      0,
							Sx:     -100,
							Sy:     100,
						},
						Sprite: func() *sprites.Sprite {
							return sprites.AphidBody[g.Suitors[1].ID-1]
						},
						Text: func() string {
							return "Select Second Suitor"
						},
						Click: func() {
							g.chooseSuitor(1)
						},
					},
					&screenreader.Button{
						ElementPosition: screenreader.ElementPosition{
							Camera: &g.textCam,
							X:      212,
							Y:      -40,
							Z:      0,
							Sx:     -100,
							Sy:     100,
						},
						Sprite: func() *sprites.Sprite {
							return sprites.AphidBody[g.Suitors[2].ID-1]
						},
						Text: func() string {
							return "Select Third Suitor"
						},
						Click: func() {
							g.chooseSuitor(2)
						},
					},
					&screenreader.TextButton{
						ElementPosition: screenreader.ElementPosition{
							Camera: &g.textCam,
							X:      0,
							Y:      -200,
							Z:      0,
							Sx:     72,
							Sy:     72,
						},
						Font:   sprites.FontD3Streetism,
						Center: true,
						Text: func() string {
							return "Maybe Later"
						},
						VisText: func() string {
							return "Maybe Later"
						},
					},
				},
			},
			&screenreader.Wrapper{
				ElementBase: screenreader.ElementBase{
					ComputeVisible: func() bool {
						return g.CurrentLoc == LocationTutorial && g.LocAnim < 4
					},
					AriaRole: "alert",
				},
				Elements: []screenreader.Element{
					&screenreader.Paragraph{
						ElementPosition: screenreader.ElementPosition{
							Camera: &g.textCam,
							X:      0,
							Y:      -120,
							Z:      0,
							Sx:     800,
							Sy:     125,
						},
						Sprite: sprites.Blank,
						Text: func() string {
							switch g.LocAnim {
							case 0:
								return joyTutorialA1 + "\n" + joyTutorialA2
							case 1:
								return joyTutorialB1 + "\n" + joyTutorialB2
							case 2:
								return joyTutorialC1 + "\n" + joyTutorialC2
							default:
								return joyTutorialD1 + "\n" + joyTutorialD2
							}
						},
					},
					&screenreader.Button{
						ElementPosition: screenreader.ElementPosition{
							Camera: &g.textCam,
							X:      350,
							Y:      -220,
							Z:      0,
							Sx:     50,
							Sy:     50,
						},
						Sprite: func() *sprites.Sprite {
							return &sprites.Sprite{
								X0: sprites.Pointer.Y0,
								X1: sprites.Pointer.Y1,
								Y0: -sprites.Pointer.X1,
								Y1: -sprites.Pointer.X0,
							}
						},
						Text: func() string {
							return "Continue"
						},
						Click: func() {
							g.LocAnim++
						},
					},
				},
			},
		},
	}
}
