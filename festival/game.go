//go:generate stringer -type Location -trimprefix Location

package festival

import (
	"context"
	"math/rand"
	"time"

	"git.lubar.me/ben/spy-cards/arcade/touchcontroller"
	"git.lubar.me/ben/spy-cards/audio"
	"git.lubar.me/ben/spy-cards/gfx"
	"git.lubar.me/ben/spy-cards/gfx/sprites"
	"git.lubar.me/ben/spy-cards/input"
	"git.lubar.me/ben/spy-cards/internal/router"
	"git.lubar.me/ben/spy-cards/room"
)

type Location uint8

// Constants for Location.
const (
	LocationFarm Location = iota
	LocationArena
	LocationKut
	LocationHistory
	LocationWifeMode
	LocationInn
	LocationTutorial
	numLocations
)

var locationCamera [numLocations]gfx.Vector = [...]gfx.Vector{
	LocationFarm:     {-35.5, 2.3, -6, 1},
	LocationArena:    {61.8, 2.3, 3.2, 1},
	LocationKut:      {90.5, 2.3, 11.7, 1},
	LocationHistory:  {-35.5, 2.3, -6, 1},
	LocationWifeMode: {-35.5, 2.3, -6, 1},
	LocationInn:      {-35.5, 2.3, -6, 1},
	LocationTutorial: {-35.5, 2.3, -6, 1},
}

type Game struct {
	CurrentLoc  Location
	PrevLoc     Location
	Transition  uint8
	LocState    uint8
	LocAnim     uint16
	RNG         *rand.Rand
	Aphid       ActivePibu
	History     Pibus
	Suitors     [3]Pibu
	SaveRequest uint64
	cam         gfx.Camera
	textCam     gfx.Camera
	stage       *room.Stage
	sb          *sprites.Batch
	tb          *sprites.Batch
	ictx        *input.Context
}

func New(ctx context.Context) (*Game, error) {
	g := &Game{}

	g.RNG = rand.New(rand.NewSource(time.Now().UnixNano())) /*#nosec*/

	g.cam.SetDefaults()
	g.cam.Rotation.Identity()
	g.cam.Offset.Identity()

	g.textCam.SetDefaults()
	g.textCam.Position.Identity()
	g.textCam.Rotation.Identity()
	g.textCam.Offset.Identity()

	g.stage = room.NewStage(ctx, "img/room/festival.stage", nil)
	g.stage.Paper = false
	g.stage.UpdateCamera = func(_ *room.CustomStageProperties, _ uint64, _ int, cam *gfx.Camera) bool {
		*cam = g.cam

		return false
	}

	g.sb = sprites.NewBatch(&g.cam)
	g.tb = sprites.NewBatch(&g.textCam)

	g.ictx = input.GetContext(ctx)

	if err := g.Resize(ctx); err != nil {
		return nil, err
	}

	g.cam.Position.Translation(locationCamera[LocationFarm][0], locationCamera[LocationFarm][1], locationCamera[LocationFarm][2])
	g.cam.MarkDirty()

	if err := g.LoadData(); err != nil {
		return nil, err
	}

	return g, nil
}

func (g *Game) Resize(ctx context.Context) error {
	touchcontroller.SkipFrame = true

	w, h := gfx.PixelSize()
	aspect := float32(w) / float32(h)

	g.cam.Perspective.Perspective(gfx.HorizontalFOV(76), aspect, 0.3, 150)

	if aspect > 16.0/9.0 {
		g.textCam.Perspective.Scale(-1.0/aspect/225.0, -1.0/225.0, 0.01)
	} else {
		g.textCam.Perspective.Scale(-1.0/400.0, -aspect/400.0, 0.01)
	}

	return nil
}

func (g *Game) Update(ctx context.Context, frameAdvance bool) (*router.PageInfo, error) {
	touchcontroller.SkipFrame = true

	if !frameAdvance {
		return nil, nil
	}

	g.stage.Update()

	if g.SaveRequest != 0 {
		g.SaveRequest--

		if g.SaveRequest == 0 {
			g.AutoSave()
		}
	}

	if g.CurrentLoc != LocationTutorial {
		prevBerries := g.Aphid.Berries

		g.Aphid.Update()

		if g.Aphid.Berries != prevBerries {
			g.QueueAutoSave()
		}
	}

	switch g.CurrentLoc {
	case LocationInn:
		audio.StopMusic()
	case LocationFarm, LocationTutorial:
		audio.Field1.PlayMusic(0, false)
	case LocationArena:
		audio.Battle0.PlayMusic(0, false)
	case LocationKut:
		audio.Chef1.PlayMusic(0, false)
	}

	if router.FlagSlow.IsSet() && g.Transition > 1 {
		g.Transition--

		return nil, nil
	}

	if g.Transition > 10 {
		g.Transition -= 10

		return nil, nil
	}

	if g.Transition != 0 {
		g.Transition = 0

		g.PrevLoc = g.CurrentLoc

		g.cam.Position.Translation(locationCamera[g.CurrentLoc][0], locationCamera[g.CurrentLoc][1], locationCamera[g.CurrentLoc][2])
		g.cam.MarkDirty()
	}

	mx, my, click := g.ictx.Mouse()
	_, _, lastClick := g.ictx.LastMouse()

	if g.CurrentLoc == LocationFarm || (g.CurrentLoc == LocationArena && g.LocAnim <= 1) || (g.CurrentLoc == LocationKut && (g.LocAnim < 90 || g.LocAnim >= 400)) {
		if lastClick && !click && !g.ictx.IsMouseDrag() && sprites.SpriteHitTest(&g.textCam, sprites.BtnHistory, -201, 151, 0, 38.4, 38.4, mx, my) {
			g.ictx.ConsumeClick()

			g.CurrentLoc = LocationHistory
		}

		if lastClick && !click && !g.ictx.IsMouseDrag() && sprites.SpriteHitTest(&g.textCam, sprites.BtnPibu, -325, 32, 0, 38.4, 38.4, mx, my) {
			g.ictx.ConsumeClick()

			if g.CurrentLoc != LocationFarm {
				g.CurrentLoc = LocationFarm
				g.Transition = 255
				g.LocAnim = 0
			}
		}

		if lastClick && !click && !g.ictx.IsMouseDrag() && sprites.SpriteHitTest(&g.textCam, sprites.BtnKut, -325, -34, 0, 38.4, 38.4, mx, my) {
			g.ictx.ConsumeClick()

			if g.CurrentLoc != LocationKut {
				g.CurrentLoc = LocationKut
				g.Transition = 255
				g.LocAnim = 0
				g.LocState = 0
			}
		}

		if lastClick && !click && !g.ictx.IsMouseDrag() && sprites.SpriteHitTest(&g.textCam, sprites.BtnTanjerin, -325, -110, 0, 29.3, 29.3, mx, my) {
			g.ictx.ConsumeClick()

			if g.CurrentLoc != LocationArena {
				g.CurrentLoc = LocationArena
				g.Transition = 255
				g.LocAnim = 0
			}
		}
	}

	switch g.CurrentLoc {
	case LocationFarm:
		if lastClick && !click && !g.ictx.IsMouseDrag() && sprites.SpriteHitTest(&g.cam, sprites.AphidBody[g.Aphid.ID-1], -35.11, 0.49, 1.62, 1, 1, mx, my) {
			g.ictx.ConsumeClick()

			g.aphidClicked()
		}

		if g.Aphid.Level >= 10 && lastClick && !click && !g.ictx.IsMouseDrag() && sprites.SpriteHitTest(&g.cam, sprites.BtnWifeMode, -35.11, 2, 1.62, 1, 1, mx, my) {
			g.ictx.ConsumeClick()

			g.turnOnWifeMode()
		}
	case LocationArena:
		tanj := sprites.TanjerinIdle
		if g.LocAnim == 1 {
			tanj = sprites.TanjerinCheer
		}

		if g.LocAnim <= 1 && g.Aphid.Health > 1 && lastClick && !click && !g.ictx.IsMouseDrag() && sprites.SpriteHitTest(&g.cam, tanj, 59.6, 2, 9.49, 1, 1, mx, my) {
			g.ictx.ConsumeClick()

			g.LocAnim = 12
		}

		if g.LocAnim > 1 {
			g.LocAnim--

			if g.LocAnim == 1 && g.Aphid.Health <= 1 {
				g.LocAnim = 0
			}

			if g.LocAnim == 6 {
				g.Aphid.Tackle()

				if g.Aphid.Health > 1 {
					audio.Damage1.PlaySound(0, 0, 0, 59.6, 2, 9.49)
				} else {
					audio.Damage2.PlaySound(0, 0, 0, 59.6, 2, 9.49)
				}
			}
		}
	case LocationKut:
		if g.LocAnim == 0 || g.LocAnim >= 400 {
			if lastClick && !click && !g.ictx.IsMouseDrag() && sprites.SpriteHitTest(&g.cam, sprites.FoodShroomRaw, 89, 2.01, 15.92, 1, 1, mx, my) {
				g.ictx.ConsumeClick()

				g.cook(FoodShroom)
			}

			if lastClick && !click && !g.ictx.IsMouseDrag() && sprites.SpriteHitTest(&g.cam, sprites.FoodLeafRaw, 90.37, 2.01, 15.92, 1, 1, mx, my) {
				g.ictx.ConsumeClick()

				g.cook(FoodLeaf)
			}

			if lastClick && !click && !g.ictx.IsMouseDrag() && sprites.SpriteHitTest(&g.cam, sprites.FoodHoneyRaw, 91.61, 2.01, 15.92, 1, 1, mx, my) {
				g.ictx.ConsumeClick()

				g.cook(FoodHoney)
			}
		} else {
			g.LocAnim--

			switch g.LocAnim {
			case 261:
				audio.Kut1.PlaySound(0, 0, 0, 90.37, 2.01, 15.92)
			case 216:
				audio.Kut2.PlaySound(0, 0, 0, 90.37, 2.01, 15.92)
			case 90:
				audio.AtkSuccess.PlaySound(0, 0, 0, 90.37, 2.01, 15.92)

				if FoodCategory(g.LocState>>4) == g.Aphid.FavoriteFoods {
					g.Aphid.Health += 50
				} else {
					g.Aphid.Health += 25
				}

				if g.Aphid.Health > 100 {
					g.Aphid.Health = 100
				}

				g.AutoSave()
			}
		}
	case LocationHistory:
		if lastClick && !click && !g.ictx.IsMouseDrag() && sprites.TextHitTest(&g.textCam, sprites.FontD3Streetism, "Go Back", 120, -180, 0, 72, 72, mx, my, true) {
			g.ictx.ConsumeClick()

			g.CurrentLoc = g.PrevLoc
		}
	case LocationWifeMode:
		if lastClick && !click && !g.ictx.IsMouseDrag() && sprites.SpriteHitTest(&g.textCam, sprites.AphidBody[g.Suitors[0].ID-1], -168, -40, 0, -100, 100, mx, my) {
			g.ictx.ConsumeClick()

			g.chooseSuitor(0)
		}

		if lastClick && !click && !g.ictx.IsMouseDrag() && sprites.SpriteHitTest(&g.textCam, sprites.AphidBody[g.Suitors[1].ID-1], 18, -40, 0, -100, 100, mx, my) {
			g.ictx.ConsumeClick()

			g.chooseSuitor(1)
		}

		if lastClick && !click && !g.ictx.IsMouseDrag() && sprites.SpriteHitTest(&g.textCam, sprites.AphidBody[g.Suitors[2].ID-1], 212, -40, 0, -100, 100, mx, my) {
			g.ictx.ConsumeClick()

			g.chooseSuitor(2)
		}

		if lastClick && !click && !g.ictx.IsMouseDrag() && sprites.TextHitTest(&g.textCam, sprites.FontD3Streetism, "Maybe Later", 0, -200, 0, 72, 72, mx, my, true) {
			g.ictx.ConsumeClick()

			g.CurrentLoc = LocationFarm
		}
	case LocationInn:
		g.LocAnim--

		if g.LocAnim == 255 {
			g.History = append(g.History, g.Aphid.Pibu)
			g.Aphid.Blend(&g.Suitors[g.LocState], g.RNG)
			g.AutoSave()
		}

		if g.LocAnim == 0 {
			g.CurrentLoc = LocationFarm
			g.LocState = 0
		}
	case LocationTutorial:
		if g.LocAnim < 4 {
			if (lastClick && !click && !g.ictx.IsMouseDrag()) || g.ictx.Consume(input.BtnConfirm) {
				g.ictx.ConsumeClick()

				g.LocAnim++
			}
		} else {
			g.LocAnim += 3

			if g.LocAnim >= 250 {
				g.CurrentLoc = LocationFarm
				g.PrevLoc = LocationFarm
				g.Transition = 0
				g.LocAnim = 0

				g.AutoSave()
			}
		}
	}

	return nil, nil
}

func (g *Game) aphidClicked() {
	audio.FunnyStep.PlaySound(0, 0, 0, -35.11, 0.49, 1.62)

	g.Aphid.Squish = 60
	g.Aphid.Berries++
	g.QueueAutoSave()
}

func (g *Game) cook(f FoodCategory) {
	if g.Aphid.Berries < 20 {
		if g.LocAnim < 400 {
			g.LocAnim = 400
		} else if g.LocAnim < 411 {
			g.LocAnim++
		}

		return
	}

	g.Aphid.Berries -= 20
	g.LocState = uint8(f<<4) + 1 + uint8(g.RNG.Intn(3))
	g.LocAnim = 306
}

func (g *Game) turnOnWifeMode() {
	g.CurrentLoc = LocationWifeMode

	perm := g.RNG.Perm(len(suitors))[:3]

	g.Suitors[0] = suitors[perm[0]]
	g.Suitors[1] = suitors[perm[1]]
	g.Suitors[2] = suitors[perm[2]]

	randomize := func(i int) {
		g.Suitors[i].ID = uint8(g.RNG.Intn(3) + 1)
		g.Suitors[i].Body.R = uint8(g.RNG.Intn(256))
		g.Suitors[i].Body.G = uint8(g.RNG.Intn(256))
		g.Suitors[i].Body.B = uint8(g.RNG.Intn(256))
		g.Suitors[i].Leg.R = uint8(g.RNG.Intn(256))
		g.Suitors[i].Leg.G = uint8(g.RNG.Intn(256))
		g.Suitors[i].Leg.B = uint8(g.RNG.Intn(256))
	}

	switch g.RNG.Intn(100) {
	case 0:
		randomize(0)
		randomize(1)
		randomize(2)
	case 10:
		randomize(0)
	case 20:
		randomize(1)
	case 30:
		randomize(2)
	}
}

func (g *Game) chooseSuitor(i int) {
	g.CurrentLoc = LocationInn
	g.LocAnim = 511
	g.LocState = uint8(i)

	audio.Inn.PlaySoundGlobal(0, 0, 0)
}
