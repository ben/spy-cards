package festival

import (
	"context"
	"math"
	"math/rand"
	"strconv"
	"time"

	"git.lubar.me/ben/spy-cards/audio"
	"git.lubar.me/ben/spy-cards/gfx"
	"git.lubar.me/ben/spy-cards/gfx/sprites"
	"git.lubar.me/ben/spy-cards/internal"
	"git.lubar.me/ben/spy-cards/internal/router"
	"golang.org/x/mobile/exp/f32"
	"golang.org/x/mobile/gl"
)

var arbitrary = time.Now()

type smokeParticle struct {
	offX, offY, offZ float32
	velX, velY, velZ float32
	rotZ, size       float32
	part             uint8
}

var smokeParticles = func() []smokeParticle {
	r := rand.New(rand.NewSource(0)) /*#nosec*/

	particles := make([]smokeParticle, 97)

	for i := range particles {
		particles[i].offX = r.Float32()*0.2 - 0.1
		particles[i].offY = r.Float32()*0.2 - 0.1
		particles[i].offZ = r.Float32()*0.1 - 0.1
		particles[i].velX = r.Float32()*2 - 1
		particles[i].velY = r.Float32()*3 - 0.5
		particles[i].velZ = r.Float32()*0.2 - 0.2
		particles[i].rotZ = r.Float32() * 2 * math.Pi
		particles[i].size = r.Float32()/3 + 0.25
		particles[i].part = uint8(r.Intn(2))
	}

	return particles
}()

func (g *Game) Render(ctx context.Context) error {
	if g.Transition != 0 {
		prev := locationCamera[g.PrevLoc]
		cur := locationCamera[g.CurrentLoc]
		lerp := func(a, b float32) float32 {
			c := float32(g.Transition) / 255

			return a*c + b*(1-c)
		}
		g.cam.Position.Translation(lerp(prev[0], cur[0]), lerp(prev[1], cur[1]), lerp(prev[2], cur[2]))
		audio.SetListenerPosition(-lerp(prev[0], cur[0]), -lerp(prev[1], cur[1]), -lerp(prev[2], cur[2]), 0, 0, 0)
	} else {
		prev := locationCamera[g.PrevLoc]
		g.cam.Position.Translation(prev[0], prev[1], prev[2])
		audio.SetListenerPosition(-prev[0], -prev[1], -prev[2], 0, 0, 0)
	}

	g.cam.MarkDirty()

	g.stage.Render()
	g.sb.Reset(&g.cam)
	g.tb.Reset(&g.textCam)

	if g.Transition != 0 {
		g.renderTrans()
	} else {
		switch g.CurrentLoc {
		case LocationFarm:
			g.renderFarm(true)
		case LocationArena:
			g.renderArena(true)
		case LocationKut:
			g.renderKut(true)
		case LocationHistory:
			g.renderHistory()
		case LocationWifeMode:
			g.renderWifeMode()
		case LocationInn:
			g.renderInn()
		case LocationTutorial:
			g.renderTutorial()
		default:
			panic("missing render for location " + g.CurrentLoc.String())
		}
	}

	gfx.GL.Enable(gl.DEPTH_TEST)
	g.sb.Render()
	gfx.GL.Disable(gl.DEPTH_TEST)
	g.tb.Render()

	return nil
}

func (g *Game) renderText(offX, offY float32, lines ...string) {
	g.tb.Append(sprites.Blank, 0, -120+offY, 0, 800, 50*float32(len(lines))+25, sprites.Color{R: 127, G: 127, B: 127, A: 127})

	for i, l := range lines {
		sprites.DrawText(g.tb, sprites.FontBubblegumSans, l, -270+offX, -160+offY+25*float32(len(lines))-50*float32(i), 0, 64, 64, sprites.Black)
	}
}

func (g *Game) renderTrans() {
	g.renderFarm(false)
	g.renderArena(false)
	g.renderKut(false)
	g.renderHUD()
}

func (g *Game) renderFarm(hud bool) {
	g.Aphid.Render(g.sb, -35.11, 0.49, 1.62, 1, 1)

	if _, m, d := time.Now().Date(); router.FlagMMORPG.IsSet() || (m == time.April && d == 1) {
		g.sb.AppendEx(sprites.FestivalWordArt, -35.11, 4, 1.62, 1, 1, sprites.White, 0, 0, 0, -15*math.Pi/180)
	}

	if hud {
		if g.Aphid.Level >= 10 && g.CurrentLoc != LocationInn {
			g.sb.Append(sprites.BtnWifeMode, -35.11, 2, 1.62, 1, 1, sprites.White)
		}

		g.renderHUD()
	}
}

func (g *Game) renderArena(hud bool) {
	tanj := sprites.TanjerinIdle
	blend := float32(0)

	if g.LocAnim > 1 {
		tanj = sprites.TanjerinHide

		if g.LocAnim > 6 {
			blend = float32(11-g.LocAnim) / 5
		} else {
			blend = float32(g.LocAnim-1) / 5
		}
	} else if g.LocAnim == 1 {
		tanj = sprites.TanjerinCheer
	}

	g.sb.Append(tanj, 59.6, 2, 9.49, -1, 1, sprites.White)
	g.Aphid.Render(g.sb, blend*59.6+(1-blend)*64.1, blend*2+(1-blend)*1.54, blend*9.49+(1-blend)*9.4, 1, 1)

	if hud {
		if g.Aphid.Health <= 1 {
			g.renderText(0, -50, tanjerinResource1, tanjerinResource2)
		} else {
			g.renderText(0, -50, tanjerinDefault1, tanjerinDefault2)
		}

		g.renderHUD()
	}
}

func (g *Game) renderKut(hud bool) {
	kutSprite := sprites.KutIdle
	rotY := float32(0)

	switch {
	case g.LocAnim == 0 || g.LocAnim >= 400:
		break // use idle pose
	case g.LocAnim > 296:
		kutSprite = sprites.KutSlice[0]
	case g.LocAnim > 286:
		kutSprite = sprites.KutSlice[1]
	case g.LocAnim > 261:
		kutSprite = sprites.KutSlice[2]
	case g.LocAnim > 256:
		kutSprite = sprites.KutSlice[3]
	case g.LocAnim > 251:
		kutSprite = sprites.KutSlice[5]
	case g.LocAnim > 245:
		kutSprite = sprites.KutSlice[4]
	case g.LocAnim > 242:
		kutSprite = sprites.KutSlice[3]
	case g.LocAnim > 235:
		kutSprite = sprites.KutSlice[5]
	case g.LocAnim > 216:
		kutSprite = sprites.KutSlice[4]
	case g.LocAnim > 201:
		rotY = float32(216-g.LocAnim) * math.Pi / 10
		kutSprite = sprites.KutSlice[7]
	case g.LocAnim > 198:
		rotY = math.Pi
		kutSprite = sprites.KutSlice[8]
	case g.LocAnim > 195:
		rotY = math.Pi
		kutSprite = sprites.KutSlice[9]
	case g.LocAnim > 180:
		rotY = float32(g.LocAnim-181) * math.Pi / 10
		kutSprite = sprites.KutSlice[7]
	case g.LocAnim > 177:
		kutSprite = sprites.KutSlice[8]
	case g.LocAnim > 174:
		kutSprite = sprites.KutSlice[9]
	case g.LocAnim > 159:
		rotY = float32(174-g.LocAnim) * math.Pi / 10
		kutSprite = sprites.KutSlice[7]
	case g.LocAnim > 156:
		rotY = math.Pi
		kutSprite = sprites.KutSlice[8]
	case g.LocAnim > 153:
		rotY = math.Pi
		kutSprite = sprites.KutSlice[9]
	case g.LocAnim > 138:
		rotY = float32(g.LocAnim-139) * math.Pi / 10
		kutSprite = sprites.KutSlice[7]
	case g.LocAnim > 135:
		kutSprite = sprites.KutSlice[8]
	case g.LocAnim > 132:
		kutSprite = sprites.KutSlice[9]
	case g.LocAnim > 117:
		rotY = float32(132-g.LocAnim) * math.Pi / 10
		kutSprite = sprites.KutSlice[7]
	case g.LocAnim > 114:
		rotY = math.Pi
		kutSprite = sprites.KutSlice[8]
	case g.LocAnim > 111:
		rotY = math.Pi
		kutSprite = sprites.KutSlice[9]
	case g.LocAnim > 96:
		rotY = float32(g.LocAnim-97) * math.Pi / 10
		kutSprite = sprites.KutSlice[7]
	case g.LocAnim > 93:
		kutSprite = sprites.KutSlice[8]
	case g.LocAnim > 90:
		kutSprite = sprites.KutSlice[9]
	default:
		kutSprite = sprites.KutSlice[6]
	}

	if g.LocAnim <= 216 && g.LocAnim > 90 {
		for i := g.LocAnim + 30; i >= g.LocAnim && i > 120; i-- {
			if i > 216 {
				continue
			}

			p := smokeParticles[i-120]
			life := float32(i-g.LocAnim) / 60

			g.sb.AppendEx(sprites.SmokeParticle[p.part], 90.32+p.offX+p.velX*life, 1.87+p.offY+p.velY*life, 15.125+p.offZ+p.velZ*life, p.size, p.size, sprites.White, 0, 0, 0, p.rotZ)
		}
	}

	sx := float32(1.5)

	if rotY > math.Pi/2 {
		rotY -= math.Pi
		sx = -sx
	}

	g.sb.AppendEx(kutSprite, 90.32, 1.87, 19.14, sx, 1.5, sprites.White, 0, 0, rotY, 0)

	switch {
	case g.LocAnim == 0 || g.LocAnim >= 400:
		g.sb.Append(sprites.FoodShroomRaw, 89, 2.01, 15.92, 1, 1, sprites.White)
		g.sb.Append(sprites.FoodLeafRaw, 90.37, 2.01, 15.92, 1, 1, sprites.White)
		g.sb.Append(sprites.FoodHoneyRaw, 91.61, 2.01, 15.92, 1, 1, sprites.White)
	case g.LocAnim > 117:
		switch FoodCategory(g.LocState >> 4) {
		case FoodShroom:
			g.sb.Append(sprites.FoodShroomRaw, 90.37, 2.01, 15.92, 1, 1, sprites.White)
		case FoodLeaf:
			g.sb.Append(sprites.FoodLeafRaw, 90.37, 2.01, 15.92, 1, 1, sprites.White)
		case FoodHoney:
			g.sb.Append(sprites.FoodHoneyRaw, 90.37, 2.01, 15.92, 1, 1, sprites.White)
		}
	default:
		switch FoodCategory(g.LocState >> 4) {
		case FoodShroom:
			g.sb.Append(sprites.FoodShroomCooked[g.LocState&0xf-1], 90.37, 2.01, 15.92, 1, 1, sprites.White)
		case FoodLeaf:
			g.sb.Append(sprites.FoodLeafCooked[g.LocState&0xf-1], 90.37, 2.01, 15.92, 1, 1, sprites.White)
		case FoodHoney:
			g.sb.Append(sprites.FoodHoneyCooked[g.LocState&0xf-1], 90.37, 2.01, 15.92, 1, 1, sprites.White)
		}
	}

	if hud {
		text1, text2 := g.kutText()
		g.renderText(0, -50, text1, text2)

		g.renderHUD()
	}
}

func (g *Game) kutText() (string, string) {
	switch {
	case g.LocAnim == 0:
		return kutDefault1, kutDefault2
	case g.LocAnim == 410:
		return kutDefault1[:21], kutDefault2[:0]
	case g.LocAnim >= 400:
		return kutResource1, kutResource2
	case g.LocAnim > 90:
		return kutCook1, kutCook2
	case FoodCategory(g.LocState>>4) == g.Aphid.FavoriteFoods:
		return kutSuccess1, kutSuccess2
	default:
		return kutFail1, kutFail2
	}
}

func (g *Game) renderHistory() {
	switch g.PrevLoc {
	case LocationFarm:
		g.renderFarm(false)
	case LocationArena:
		g.renderArena(false)
	case LocationKut:
		g.renderKut(false)
	}

	g.tb.Append(sprites.Blank, 0, 0, 0, 800, 600, sprites.Color{R: 63, G: 63, B: 63, A: 63})
	g.renderText(0, 280, joyHistory1, joyHistory2)
	g.tb.Append(sprites.Joy, -337, -0.2043304, 0, -100, 100, sprites.White)
	g.Aphid.Render(g.tb, -203, -149, 0, -100, 100)

	var history [20]Pibu

	for i := range history {
		history[i].ID = g.Aphid.ID
		history[i].Body.A = 255
		history[i].Leg.A = 255
	}

	suffix := g.History
	if len(suffix) > 20 {
		suffix = suffix[len(suffix)-20:]
	}

	copy(history[:], suffix)

	for i, p := range history {
		x, y := i%7, i/7
		if y == 2 {
			x++
		}

		p.Render(g.tb, -199+88*float32(x), 65-72*float32(y), 0, 50, 50)
	}

	mx, my, _ := g.ictx.Mouse()

	btnColor := sprites.White
	if sprites.TextHitTest(&g.textCam, sprites.FontD3Streetism, "Go Back", 120, -180, 0, 72, 72, mx, my, true) {
		btnColor = sprites.Yellow
	}

	sprites.DrawTextBorder(g.tb, sprites.FontD3Streetism, "Go Back", 120, -180, 0, 72, 72, btnColor, sprites.Black, true)
}

func (g *Game) renderWifeMode() {
	g.renderFarm(false)

	g.tb.Append(sprites.Blank, 0, 0, 0, 800, 600, sprites.Color{R: 63, G: 63, B: 63, A: 63})
	g.renderText(0, 200, joySuitor1, joySuitor2, joySuitor3)
	g.tb.Append(sprites.Joy, -337, -0.2043304, 0, -100, 100, sprites.White)
	g.Suitors[0].Render(g.tb, -168, -40, 0, -100, 100)
	g.Suitors[1].Render(g.tb, 18, -40, 0, -100, 100)
	g.Suitors[2].Render(g.tb, 212, -40, 0, -100, 100)

	mx, my, _ := g.ictx.Mouse()

	btnColor := sprites.White
	if sprites.TextHitTest(&g.textCam, sprites.FontD3Streetism, "Maybe Later", 0, -200, 0, 72, 72, mx, my, true) {
		btnColor = sprites.Yellow
	}

	sprites.DrawTextBorder(g.tb, sprites.FontD3Streetism, "Maybe Later", 0, -200, 0, 72, 72, btnColor, sprites.Black, true)
}

func (g *Game) renderInn() {
	g.renderFarm(true)

	a := uint8(g.LocAnim)
	if g.LocAnim >= 256 {
		a = uint8(511 - g.LocAnim)
	}

	g.tb.Append(sprites.Blank, 0, 0, 0, 2000, 2000, sprites.Color{R: 0, G: 0, B: 0, A: a})
}

func (g *Game) renderTutorial() {
	g.renderFarm(false)

	a := 255 - uint8(g.LocAnim)

	g.tb.AppendEx(sprites.Joy, -337, -0.2043304, 0, -100, 100, sprites.Color{R: a, G: a, B: a, A: a}, sprites.FlagNoDiscard, 0, 0, 0)

	switch g.LocAnim {
	case 0:
		g.renderText(-30, 0, joyTutorialA1, joyTutorialA2)
	case 1:
		g.renderText(-30, 0, joyTutorialB1, joyTutorialB2)
	case 2:
		g.renderText(-30, 0, joyTutorialC1, joyTutorialC2)
	case 3:
		g.renderText(-30, 0, joyTutorialD1, joyTutorialD2)
	}

	if g.LocAnim < 4 {
		const bounceAmount = 0.5

		bounceTime := float32(time.Since(arbitrary).Seconds()) * 7 * bounceAmount

		if internal.PrefersReducedMotion() {
			bounceTime = 0
		}

		sx := 1 + f32.Sin(bounceTime)*0.1*bounceAmount*0.75
		sy := 1 + f32.Cos(bounceTime+1)*0.1*bounceAmount*0.75

		g.tb.AppendEx(sprites.Pointer, 350, -220, 0, sx*50, sy*50, sprites.White, sprites.FlagNoDiscard, 0, 0, math.Pi/2)
	}
}

func (g *Game) renderHUD() {
	g.tb.Append(sprites.Description, -315.5, 144.9, 0, 57.47525, 84.63462, sprites.White)
	g.tb.Append(sprites.HP, -361.5, 150.9, 0, 25, 25, sprites.White)
	g.tb.Append(sprites.Berries, -361.5, 99.9, 0, 37.5, 37.5, sprites.White)

	b := make([]byte, 0, 64)

	b = append(b[:0], "Level "...)
	b = strconv.AppendUint(b, uint64(g.Aphid.Level), 10)
	sprites.DrawTextCentered(g.tb, sprites.FontD3Streetism, string(b), -315.5, 179.9, 0, 40, 40, sprites.Black, false)

	b = strconv.AppendUint(b[:0], uint64(g.Aphid.Health), 10)
	b = append(b, '%')
	sprites.DrawText(g.tb, sprites.FontD3Streetism, string(b), -335.5, 139.9, 0, 40, 40, sprites.Black)

	b = strconv.AppendUint(b[:0], g.Aphid.Berries, 10)
	sprites.DrawText(g.tb, sprites.FontD3Streetism, string(b), -335.5, 89.9, 0, 40, 40, sprites.Black)

	g.tb.Append(sprites.BtnHistory, -201, 151, 0, 38.4, 38.4, sprites.White)
	g.tb.Append(sprites.BtnPibu, -325, 32, 0, 38.4, 38.4, sprites.White)
	g.tb.Append(sprites.BtnKut, -325, -34, 0, 38.4, 38.4, sprites.White)
	g.tb.Append(sprites.BtnTanjerin, -325, -110, 0, 29.3, 29.3, sprites.White)

	if false {
		g.tb.Append(sprites.BtnSave, -325, -183, 0, 38.4, 38.4, sprites.White)
		g.tb.Append(sprites.BtnQuit, 341.8, 179.3, 0, 29.3, 29.3, sprites.White)
	}
}
