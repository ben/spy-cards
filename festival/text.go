package festival

const (
	kutDefault1  = "I'll cook your friend a 5 star dish"
	kutDefault2  = "for 20 berries. Click your pick."
	kutResource1 = "...You intend to make a"
	kutResource2 = "mockery of me? It's 20 berries."
	kutCook1     = "Witness it! A life of skill and"
	kutCook2     = "mastery put to use!"
	kutSuccess1  = "My, it really seems to enjoy this"
	kutSuccess2  = "type of dish. (+50 Health!)"
	kutFail1     = "Hmph, it doesn't seem a fan of"
	kutFail2     = "this cuisine... (+25 Health)"

	tanjerinDefault1  = "It's training time! Click on"
	tanjerinDefault2  = "Tanjerin to train!"
	tanjerinResource1 = "...Your Aphid's too tired! Take it"
	tanjerinResource2 = "out for some lunch!"

	joyTutorialA1 = "Hey! You're the new farmer, right?"
	joyTutorialA2 = "The aphid's been waiting for you!"
	joyTutorialB1 = "You can pet it, feed it or even train"
	joyTutorialB2 = "it!"
	joyTutorialC1 = "If it gets to level 10... Maybe"
	joyTutorialC2 = "something special will happen!"
	joyTutorialD1 = "Well, I'm off to care for mine. Have"
	joyTutorialD2 = "fun!"
	joySuitor1    = "Oh, your Aphid's so popular! Are"
	joySuitor2    = "you okay with it getting married?"
	joySuitor3    = "(Click on the suitor)"
	joyHistory1   = "Here's your Aphid's last 20"
	joyHistory2   = "generations!"
)
