package festival

import (
	"fmt"

	"git.lubar.me/ben/spy-cards/internal"
)

const (
	dataKey    = "spy-cards-aphid-festival-data-v0"
	historyKey = "spy-cards-aphid-festival-history-v0"
)

func (g *Game) LoadData() error {
	b, err := internal.LoadData(dataKey)
	if err != nil {
		return fmt.Errorf("festival: loading data: %w", err)
	}

	if b == nil {
		g.Aphid.Pibu = initial
		g.Aphid.ID = 1
		g.Aphid.Health = 100
		g.Aphid.Generation = 1
		g.Aphid.Level = 1
		g.Aphid.FavoriteFoods = FoodCategory(g.RNG.Intn(int(numFoods)))
		g.CurrentLoc = LocationTutorial
		g.PrevLoc = LocationTutorial
	} else if err := g.Aphid.UnmarshalText(b); err != nil {
		return fmt.Errorf("festival: decoding saved aphid: %w", err)
	} else {
		g.CurrentLoc = LocationFarm
		g.PrevLoc = LocationFarm
	}

	g.Transition = 0
	g.LocAnim = 0

	b, err = internal.LoadData(historyKey)
	if err != nil {
		return fmt.Errorf("festival: loading history: %w", err)
	}

	if b == nil {
		g.History = nil
	} else if err = g.History.UnmarshalText(b); err != nil {
		return fmt.Errorf("festival: decoding aphid history: %w", err)
	}

	return nil
}

func (g *Game) AutoSave() {
	g.SaveRequest = 0

	b, err := g.Aphid.MarshalText()
	if err != nil {
		panic(fmt.Errorf("festival: autosave failed to encode data: %w", err))
	}

	err = internal.StoreData(dataKey, b)
	if err != nil {
		panic(fmt.Errorf("festival: autosave failed to save data: %w", err))
	}

	if len(g.History) != 0 {
		b, err = g.History.MarshalText()
		if err != nil {
			panic(fmt.Errorf("festival: autosave failed to encode history: %w", err))
		}

		err = internal.StoreData(historyKey, b)
		if err != nil {
			panic(fmt.Errorf("festival: autosave failed to save history: %w", err))
		}
	}
}

func (g *Game) QueueAutoSave() {
	if g.SaveRequest == 0 {
		g.SaveRequest = 300
	}
}
