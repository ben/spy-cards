//go:generate stringer -type FoodCategory -trimprefix Food

package festival

import (
	"bytes"
	"errors"
	"fmt"
	"math"
	"math/rand"
	"strconv"

	"git.lubar.me/ben/spy-cards/gfx/sprites"
)

var errInvalidData = errors.New("festival: invalid data")

type FoodCategory uint8

// Constants for FoodCategory.
const (
	FoodShroom FoodCategory = iota
	FoodLeaf
	FoodHoney
	numFoods
)

var (
	initial = Pibu{
		ID:   1,
		Body: sprites.Color{R: 157, G: 173, B: 9, A: 255},
		Leg:  sprites.Color{R: 142, G: 96, B: 47, A: 255},
	}

	suitors [10]Pibu = [...]Pibu{
		{
			ID:   1,
			Body: sprites.Color{R: 157, G: 173, B: 9, A: 255},
			Leg:  sprites.Color{R: 142, G: 96, B: 47, A: 255},
		},
		{
			ID:   1,
			Body: sprites.Color{R: 221, G: 170, B: 57, A: 255},
			Leg:  sprites.Color{R: 142, G: 96, B: 47, A: 255},
		},
		{
			ID:   1,
			Body: sprites.Color{R: 11, G: 164, B: 152, A: 255},
			Leg:  sprites.Color{R: 145, G: 135, B: 125, A: 255},
		},
		{
			ID:   1,
			Body: sprites.Color{R: 168, G: 138, B: 168, A: 255},
			Leg:  sprites.Color{R: 197, G: 95, B: 55, A: 255},
		},
		{
			ID:   1,
			Body: sprites.Color{R: 206, G: 219, B: 203, A: 255},
			Leg:  sprites.Color{R: 60, G: 222, B: 207, A: 255},
		},
		{
			ID:   1,
			Body: sprites.Color{R: 68, G: 68, B: 68, A: 255},
			Leg:  sprites.Color{R: 227, G: 227, B: 227, A: 255},
		},
		{
			ID:   1,
			Body: sprites.Color{R: 154, G: 13, B: 94, A: 255},
			Leg:  sprites.Color{R: 255, G: 217, B: 9, A: 255},
		},
		{
			ID:   1,
			Body: sprites.Color{R: 249, G: 255, B: 109, A: 255},
			Leg:  sprites.Color{R: 126, G: 126, B: 126, A: 255},
		},
		{
			ID:   1,
			Body: sprites.Color{R: 95, G: 183, B: 155, A: 255},
			Leg:  sprites.Color{R: 30, G: 193, B: 33, A: 255},
		},
		{
			ID:   1,
			Body: sprites.Color{R: 164, G: 36, B: 36, A: 255},
			Leg:  sprites.Color{R: 144, G: 77, B: 144, A: 255},
		},
	}
)

type ActivePibu struct {
	Berries       uint64
	Generation    uint64
	Level         uint8
	Health        uint8
	FavoriteFoods FoodCategory
	Pibu

	// Temporary (not saved)
	BerryTimer uint8
	Squish     uint8
}

func (p *ActivePibu) Update() {
	p.BerryTimer++

	if p.BerryTimer >= 180 {
		p.Berries++
		p.BerryTimer -= 180
	}

	if p.Squish > 5 {
		p.Squish -= 5
	} else {
		p.Squish = 0
	}
}

func (p *ActivePibu) Render(sb *sprites.Batch, x, y, z, scaleX, scaleY float32) {
	scaleX *= 1 - float32(p.Squish)/255
	p.Pibu.Render(sb, x, y, z, scaleX, scaleY)
}

func (p *ActivePibu) Tackle() {
	if p.Health <= 1 {
		return
	}

	p.Level++
	if p.Level > 10 {
		p.Level = 10
	}

	damage := 31 - uint8(p.Generation)
	if p.Generation > 26 {
		damage = 5
	}

	if p.Health <= damage {
		p.Health = 1
	} else {
		p.Health -= damage
	}
}

func (p *ActivePibu) Blend(suitor *Pibu, r *rand.Rand) {
	p.Level = 1
	p.Health = 100
	p.FavoriteFoods = FoodCategory(r.Intn(int(numFoods)))
	p.Generation++
	p.Pibu.Blend(suitor, r)
}

func (p *ActivePibu) MarshalText() ([]byte, error) {
	b := []byte(nil)
	b = strconv.AppendUint(b, uint64(p.ID), 10)
	b = append(b, '@')
	b = strconv.AppendUint(b, uint64(p.Level), 10)
	b = append(b, "@10@"...)
	b = strconv.AppendUint(b, p.Generation, 10)
	b = append(b, '@')
	b = strconv.AppendUint(b, uint64(p.Health), 10)
	b = append(b, '@')
	b = strconv.AppendUint(b, uint64(p.FavoriteFoods), 10)
	b = append(b, '@')
	b = append(b, p.Pibu.marshal(false)...)
	b = append(b, '@')
	b = strconv.AppendUint(b, p.Berries, 10)

	return b, nil
}

func (p *ActivePibu) UnmarshalText(b []byte) error {
	parts := bytes.Split(b, []byte{'@'})
	if len(parts) != 13 {
		return errInvalidData
	}

	id, err := strconv.ParseUint(string(parts[0]), 10, 8)
	if err != nil || id < 1 || id > 3 {
		return errInvalidData
	}

	level, err := strconv.ParseUint(string(parts[1]), 10, 8)
	if err != nil || level < 1 || level > 10 {
		return errInvalidData
	}

	if !bytes.Equal(parts[2], []byte("10")) {
		return errInvalidData
	}

	generation, err := strconv.ParseUint(string(parts[3]), 10, 64)
	if err != nil || generation < 1 {
		return errInvalidData
	}

	health, err := strconv.ParseUint(string(parts[4]), 10, 8)
	if err != nil || health < 1 || health > 100 {
		return errInvalidData
	}

	foods, err := strconv.ParseUint(string(parts[5]), 10, 8)
	if err != nil || FoodCategory(foods) >= numFoods {
		return errInvalidData
	}

	berries, err := strconv.ParseUint(string(parts[12]), 10, 64)
	if err != nil {
		return errInvalidData
	}

	err = p.Pibu.UnmarshalText(bytes.Join(parts[6:12], []byte{'@'}))
	if err != nil {
		return err
	}

	p.ID = uint8(id)
	p.Level = uint8(level)
	p.Health = uint8(health)
	p.FavoriteFoods = FoodCategory(foods)
	p.Generation = generation
	p.Berries = berries

	return nil
}

type Pibus []Pibu

func (ps *Pibus) MarshalText() ([]byte, error) {
	b := make([][]byte, len(*ps))

	for i, p := range *ps {
		var err error

		b[i], err = p.MarshalText()
		if err != nil {
			return nil, err
		}
	}

	return bytes.Join(b, []byte{'\n'}), nil
}

func (ps *Pibus) UnmarshalText(b []byte) error {
	parts := bytes.Split(b, []byte{'\n'})
	*ps = make([]Pibu, len(parts))

	for i, p := range parts {
		err := (*ps)[i].UnmarshalText(p)
		if err != nil {
			return err
		}
	}

	return nil
}

type Pibu struct {
	ID   uint8
	Body sprites.Color
	Leg  sprites.Color
}

func (p *Pibu) Render(sb *sprites.Batch, x, y, z, scaleX, scaleY float32) {
	sb.Append(sprites.AphidBody[p.ID-1], x, y, z, scaleX, scaleY, p.Body)
	sb.Append(sprites.AphidLeg[p.ID-1], x, y, z-0.001, scaleX, scaleY, p.Leg)
}

func (p *Pibu) Blend(suitor *Pibu, r *rand.Rand) {
	switch r.Intn(3) {
	case 0:
		if r.Intn(2) == 1 {
			*p = *suitor
		}
	case 1:
		if r.Intn(2) == 1 {
			p.ID = suitor.ID
		}

		if r.Intn(2) == 1 {
			p.Body = suitor.Body
		} else {
			p.Leg = suitor.Leg
		}
	case 2:
		if r.Intn(2) == 1 {
			p.ID = suitor.ID
		}

		blend := func(a *uint8, b uint8) {
			af := float64(*a)
			bf := float64(b)

			*a = uint8(math.Sqrt((af*af + bf*bf) / 2))
		}

		blend(&p.Body.R, suitor.Body.R)
		blend(&p.Body.G, suitor.Body.G)
		blend(&p.Body.B, suitor.Body.B)
		blend(&p.Leg.R, suitor.Leg.R)
		blend(&p.Leg.G, suitor.Leg.G)
		blend(&p.Leg.B, suitor.Leg.B)
	}
}

func (p *Pibu) MarshalText() ([]byte, error) {
	return p.marshal(true), nil
}

func (p *Pibu) marshal(includeID bool) []byte {
	b := []byte(fmt.Sprintf(
		"%d@%d@%d@%d@%d@%d",
		p.Body.R, p.Body.G, p.Body.B,
		p.Leg.R, p.Leg.G, p.Leg.B,
	))

	if includeID {
		b = append(b, '@')
		b = strconv.AppendUint(b, uint64(p.ID), 10)
	}

	return b
}

func (p *Pibu) UnmarshalText(b []byte) error {
	parts := bytes.Split(b, []byte{'@'})
	if len(parts) != 6 && len(parts) != 7 {
		return errInvalidData
	}

	r1, err := strconv.ParseUint(string(parts[0]), 10, 8)
	if err != nil {
		return errInvalidData
	}

	g1, err := strconv.ParseUint(string(parts[1]), 10, 8)
	if err != nil {
		return errInvalidData
	}

	b1, err := strconv.ParseUint(string(parts[2]), 10, 8)
	if err != nil {
		return errInvalidData
	}

	r2, err := strconv.ParseUint(string(parts[3]), 10, 8)
	if err != nil {
		return errInvalidData
	}

	g2, err := strconv.ParseUint(string(parts[4]), 10, 8)
	if err != nil {
		return errInvalidData
	}

	b2, err := strconv.ParseUint(string(parts[5]), 10, 8)
	if err != nil {
		return errInvalidData
	}

	if len(parts) == 7 {
		id, err := strconv.ParseUint(string(parts[6]), 10, 8)
		if err != nil || id > 3 {
			return errInvalidData
		}

		if id < 1 {
			id = 1
		}

		p.ID = uint8(id)
	} else {
		p.ID = 1
	}

	p.Body.R = uint8(r1)
	p.Body.G = uint8(g1)
	p.Body.B = uint8(b1)
	p.Body.A = 255

	p.Leg.R = uint8(r2)
	p.Leg.G = uint8(g2)
	p.Leg.B = uint8(b2)
	p.Leg.A = 255

	return nil
}
