//go:build !js || !wasm
// +build !js !wasm

package screenreader

type updateElementReturnType = struct{}
type elementData struct{}

func (e *elementData) release() {}

func (ui *UI) Init()    {}
func (ui *UI) Update()  {}
func (ui *UI) Release() {}

func (e *Wrapper) initElement()    {}
func (e *Paragraph) initElement()  {}
func (e *Label) initElement()      {}
func (e *Button) initElement()     {}
func (e *TextButton) initElement() {}

func (e *Wrapper) updateElement() updateElementReturnType    { return struct{}{} }
func (e *Paragraph) updateElement() updateElementReturnType  { return struct{}{} }
func (e *Label) updateElement() updateElementReturnType      { return struct{}{} }
func (e *Button) updateElement() updateElementReturnType     { return struct{}{} }
func (e *TextButton) updateElement() updateElementReturnType { return struct{}{} }
