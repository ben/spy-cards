package screenreader

import (
	"git.lubar.me/ben/spy-cards/gfx"
	"git.lubar.me/ben/spy-cards/gfx/sprites"
	"git.lubar.me/ben/spy-cards/internal"
)

var InactiveUI = &UI{
	Elements: []Element{
		&TextButton{
			ElementPosition: ElementPosition{
				Camera: func() *gfx.Camera {
					var cam gfx.Camera

					cam.SetDefaults()
					cam.Position.Identity()
					cam.MarkDirty()

					return &cam
				}(),
				Sx: 1,
				Sy: 1,
			},
			Center:  true,
			Font:    sprites.FontD3Streetism,
			Text:    func() string { return "Enable Screen Reader Mode" },
			VisText: func() string { return "Enable Screen Reader Mode (requires refresh)" },
			Click: func() {
				s := internal.LoadSettings()

				s.ScreenReader = true

				// Ease up on rendering to reserve more CPU time for screen reader code.
				// This is only done by the quick button; it can still be modified in settings.
				s.LimitGPULevel = 1
				s.DPIScale = 0.5

				internal.SaveSettings(s)
			},
		},
	},
}
