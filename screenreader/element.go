package screenreader

import (
	"git.lubar.me/ben/spy-cards/gfx"
	"git.lubar.me/ben/spy-cards/gfx/sprites"
)

type UI struct {
	Elements []Element
	elementData
}

type Element interface {
	isVisible() bool
	initElement()
	updateElement() updateElementReturnType
	release()
}

type LeafElement interface {
	Element

	isLeafElement()
}

type ElementBase struct {
	ComputeVisible func() bool
	AriaRole       string
	elementData
}

func (e *ElementBase) isVisible() bool {
	return e.ComputeVisible == nil || e.ComputeVisible()
}

type ElementPosition struct {
	Camera  *gfx.Camera
	X, Y, Z float32
	Sx, Sy  float32
	EdgeX   int8
	EdgeY   int8

	UpdatePosition func(*ElementPosition)
}

func (e *ElementPosition) isLeafElement() {}

type Wrapper struct {
	ElementBase
	Elements []Element
}

var _ Element = (*Wrapper)(nil)

type Paragraph struct {
	ElementBase
	ElementPosition
	Sprite *sprites.Sprite
	Text   func() string
}

var _ LeafElement = (*Paragraph)(nil)

type Label struct {
	ElementBase
	ElementPosition
	Font    sprites.FontID
	Center  bool
	Text    func() string
	VisText func() string
}

var _ LeafElement = (*Label)(nil)

type Button struct {
	ElementBase
	ElementPosition
	Sprite func() *sprites.Sprite
	Text   func() string
	Click  func()
}

var _ LeafElement = (*Button)(nil)

type TextButton struct {
	ElementBase
	ElementPosition
	Font    sprites.FontID
	Center  bool
	Text    func() string
	VisText func() string
	Click   func()
}

var _ LeafElement = (*TextButton)(nil)
