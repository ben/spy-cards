//go:build js && wasm
// +build js,wasm

package screenreader

import (
	"git.lubar.me/ben/spy-cards/gfx"
	"git.lubar.me/ben/spy-cards/gfx/sprites"
	"git.lubar.me/ben/spy-cards/internal"
)

var updatePosition = internal.Function.New("", `return function updatePosition(el, x0, y0, x1, y1) {
	if (el.style.position !== "fixed") {
		el.style.position = "fixed";
	}

	function setDimension(name, value) {
		value = value.toFixed(4) + "px";

		if (el.style[name] !== value) {
			el.style[name] = value;
		}
	}

	setDimension("left", (x0 + 1) / 2 * innerWidth);
	setDimension("top", (1 - y0) / 2 * innerHeight);
	setDimension("width", (x1 - x0) / 2 * innerWidth);
	setDimension("height", (y0 - y1) / 2 * innerHeight);
}`).Invoke()

func (e *elementData) textPosition(font sprites.FontID, text string, center bool, pos *ElementPosition) {
	width := float32(0)

	for _, letter := range text {
		width += sprites.TextAdvance(font, letter, 1)
	}

	x0, x1 := float32(0), width

	if center {
		x0 = -width / 2
		x1 = width / 2
	}

	e.position(&sprites.Sprite{
		X0: x0,
		X1: x1,
		Y0: -0.1,
		Y1: 0.6,
	}, pos)
}

func (e *elementData) position(sprite *sprites.Sprite, pos *ElementPosition) {
	var (
		combined gfx.Matrix
		corners  [2]gfx.Vector
		v        gfx.Vector
	)

	if pos.UpdatePosition != nil {
		pos.UpdatePosition(pos)
	}

	w, h := gfx.Size()

	x, y := pos.X, pos.Y

	if pos.EdgeX != 0 {
		x += float32(w) / float32(pos.EdgeX)
	}

	if pos.EdgeY != 0 {
		y += float32(h) / float32(pos.EdgeY)
	}

	pos.Camera.Combined(&combined)

	v.Multiply(&combined, &gfx.Vector{x + sprite.X0*pos.Sx, y + sprite.Y0*pos.Sy, pos.Z, 1})
	corners[0].Multiply(&pos.Camera.Perspective, &v)
	v.Multiply(&combined, &gfx.Vector{x + sprite.X1*pos.Sx, y + sprite.Y1*pos.Sy, pos.Z, 1})
	corners[1].Multiply(&pos.Camera.Perspective, &v)

	corners[0][0] /= corners[0][3]
	corners[0][1] /= corners[0][3]
	corners[1][0] /= corners[1][3]
	corners[1][1] /= corners[1][3]

	if corners[0][0] > corners[1][0] {
		corners[0][0], corners[1][0] = corners[1][0], corners[0][0]
	}

	if corners[0][1] < corners[1][1] {
		corners[0][1], corners[1][1] = corners[1][1], corners[0][1]
	}

	updatePosition.Invoke(e.el, corners[0][0], corners[0][1], corners[1][0], corners[1][1])
}
