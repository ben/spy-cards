//go:build js && wasm
// +build js,wasm

package screenreader

import (
	"runtime"
	"syscall/js"

	"git.lubar.me/ben/spy-cards/internal"
)

type updateElementReturnType = js.Value

var lastButton js.Value

type elementData struct {
	el      js.Value
	btnFunc js.Func
}

var (
	updateText = internal.Function.New("", `return function updateText(el, text) {
	if (el.textContent !== text) {
		el.textContent = text;
	}
}`).Invoke()

	updateVisibility = internal.Function.New("", `return function updateElement(el, before, cel, visible) {
	if (visible) {
		if (before !== cel) {
			el.insertBefore(cel, before);
		}

		return cel.nextSibling;
	}

	const parent = cel.parentNode;
	if (parent) {
		parent.removeChild(cel);
	}

	return before === cel ? cel.nextSibling : before;
}`).Invoke()

	updateUI = internal.Function.New("", `return function updateUI(after, el, cel, visible) {
	if (visible) {
		if (after === cel || (after && after.nextSibling === cel)) {
			return cel;
		}

		if (after) {
			el.insertBefore(cel, after.nextSibling);
		} else {
			el.appendChild(cel);
		}

		return cel;
	}

	if (after === cel) {
		after = cel.nextSibling;
	}

	const parent = cel.parentNode;
	if (parent) {
		parent.removeChild(cel);
	}

	return after;
}`).Invoke()
)

func (ui *UI) Init() {
	ui.el = js.Global().Get("document").Get("body")

	for _, c := range ui.Elements {
		c.initElement()
	}
}

func (ui *UI) Update() {
	after := ui.el.Call("querySelector", ".sr-only")

	if after.Truthy() {
		after = after.Get("previousSibling")
	}

	for _, c := range ui.Elements {
		cel := c.updateElement()

		updateUI.Invoke(after, ui.el, cel, c.isVisible())
	}

	if lastButton.Truthy() {
		if lastButton.Get("parentNode").Truthy() {
			lastButton.Call("focus")
		}

		lastButton = js.Undefined()
	}
}

func (ui *UI) Release() {
	for _, c := range ui.Elements {
		c.release()
	}
}

func (e *elementData) release() {
	if parent := e.el.Get("parentNode"); parent.Truthy() {
		parent.Call("removeChild", e.el)
	}

	e.btnFunc.Release()
}

func (e *Wrapper) initElement() {
	runtime.SetFinalizer(e, func(e *Wrapper) {
		e.release()
	})

	e.el = js.Global().Get("document").Call("createElement", "div")
	e.el.Get("classList").Call("add", "sr-only")

	if e.AriaRole != "" {
		e.el.Call("setAttribute", "role", e.AriaRole)
	}

	for _, c := range e.Elements {
		c.initElement()
	}
}

func (e *Wrapper) updateElement() updateElementReturnType {
	if e.isVisible() {
		before := e.el.Get("firstChild")

		for _, c := range e.Elements {
			cel := c.updateElement()

			before = updateVisibility.Invoke(e.el, before, cel, c.isVisible())
		}
	}

	return e.el
}

func (e *Wrapper) release() {
	e.elementData.release()

	for _, c := range e.Elements {
		c.release()
	}
}

func (e *Paragraph) initElement() {
	runtime.SetFinalizer(e, func(e *Paragraph) {
		e.release()
	})

	e.el = js.Global().Get("document").Call("createElement", "p")
	e.el.Get("classList").Call("add", "sr-only")

	if e.AriaRole != "" {
		e.el.Call("setAttribute", "role", e.AriaRole)
	}
}

func (e *Paragraph) updateElement() updateElementReturnType {
	if e.isVisible() {
		updateText.Invoke(e.el, e.Text())

		e.position(e.Sprite, &e.ElementPosition)
	}

	return e.el
}

func (e *Label) initElement() {
	runtime.SetFinalizer(e, func(e *Label) {
		e.release()
	})

	e.el = js.Global().Get("document").Call("createElement", "div")
	e.el.Get("classList").Call("add", "sr-only")

	if e.AriaRole != "" {
		e.el.Call("setAttribute", "role", e.AriaRole)
	}
}

func (e *Label) updateElement() updateElementReturnType {
	if e.isVisible() {
		updateText.Invoke(e.el, e.Text())

		e.textPosition(e.Font, e.VisText(), e.Center, &e.ElementPosition)
	}

	return e.el
}

func (e *Button) initElement() {
	runtime.SetFinalizer(e, func(e *Button) {
		e.release()
	})

	e.el = js.Global().Get("document").Call("createElement", "button")
	e.el.Get("classList").Call("add", "sr-only")

	if e.AriaRole != "" {
		e.el.Call("setAttribute", "role", e.AriaRole)
	}

	// sever garbage collector link
	clickFunc := e.Click

	e.btnFunc = js.FuncOf(func(this js.Value, args []js.Value) interface{} {
		args[0].Call("preventDefault")

		lastButton = this

		clickFunc()

		return js.Undefined()
	})
	e.el.Call("addEventListener", "click", e.btnFunc)
}

func (e *Button) updateElement() updateElementReturnType {
	if e.isVisible() {
		updateText.Invoke(e.el, e.Text())

		e.position(e.Sprite(), &e.ElementPosition)
	}

	return e.el
}

func (e *TextButton) initElement() {
	runtime.SetFinalizer(e, func(e *TextButton) {
		e.release()
	})

	e.el = js.Global().Get("document").Call("createElement", "button")
	e.el.Get("classList").Call("add", "sr-only")

	// sever garbage collector link
	clickFunc := e.Click

	e.btnFunc = js.FuncOf(func(this js.Value, args []js.Value) interface{} {
		args[0].Call("preventDefault")

		lastButton = this

		clickFunc()

		return js.Undefined()
	})
	e.el.Call("addEventListener", "click", e.btnFunc)
}

func (e *TextButton) updateElement() updateElementReturnType {
	if e.isVisible() {
		updateText.Invoke(e.el, e.Text())

		e.textPosition(e.Font, e.VisText(), e.Center, &e.ElementPosition)
	}

	return e.el
}
