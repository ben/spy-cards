//go:build !headless
// +build !headless

package room

import (
	"context"
	"errors"
	"fmt"
	"log"
	"math"
	"sort"
	"sync"

	"git.lubar.me/ben/spy-cards/audio"
	"git.lubar.me/ben/spy-cards/card"
	"git.lubar.me/ben/spy-cards/gfx"
	"git.lubar.me/ben/spy-cards/gfx/sprites"
	"git.lubar.me/ben/spy-cards/internal"
	"git.lubar.me/ben/spy-cards/internal/router"
	"golang.org/x/mobile/gl"
)

type Stage struct {
	tick   uint64
	cam    gfx.Camera
	batch  *sprites.Batch
	batch2 *sprites.Batch
	stage  *customStage
	lock   sync.Mutex

	audience     []*Audience
	players      [2]*Player
	player       uint8
	disable3D    bool
	Paper        bool
	IsFlipped    bool
	Before       func(props *CustomStageProperties, tick uint64, player int, cam *gfx.Camera)
	UpdateCamera func(props *CustomStageProperties, tick uint64, player int, cam *gfx.Camera) bool

	fftAvg uint64
}

var stageCache = internal.Cache{MaxEntries: 32}

func loadStageFromCache(ctx context.Context, path string) (*customStage, error) {
	v, err := stageCache.Do(path, func() (interface{}, error) {
		sprites.CardBattle.StartPreload()
		sprites.Audience[0][AudienceFront].StartPreload()
		sprites.Audience[0][AudienceBackBlur1].StartPreload()

		return loadStage(ctx, path)
	})
	if err != nil {
		return nil, fmt.Errorf("room: loading stage: %w", err)
	}

	cs, _ := v.(*customStage)

	return cs, nil
}

func cidToPath(ctx context.Context, path string, cid card.ContentIdentifier) string {
	if cid != nil {
		return internal.GetConfig(ctx).IPFSBaseURL + cid.String()
	}

	return path
}

// PreloadStage loads a stage into the cache.
func PreloadStage(ctx context.Context, defaultPath string, cid card.ContentIdentifier) error {
	if internal.LoadSettings().Disable3D || router.FlagNo3d.IsSet() {
		// don't preload stages if we aren't going to render them
		return nil
	}

	s, err := loadStageFromCache(ctx, cidToPath(ctx, defaultPath, cid))
	if err != nil {
		return err
	}

	if err := s.lm.Preload(); err != nil {
		return fmt.Errorf("loading stage lightmap: %w", err)
	}

	if err := s.tex.Preload(); err != nil {
		return fmt.Errorf("loading stage texture: %w", err)
	}

	return nil
}

// NewStage creates a Spy Cards Online tournament stage.
func NewStage(ctx context.Context, defaultPath string, cid card.ContentIdentifier) *Stage {
	r := &Stage{
		UpdateCamera: DefaultUpdateCamera,
		Paper:        true,
	}
	r.cam.SetDefaults()

	r.disable3D = internal.LoadSettings().Disable3D || router.FlagNo3d.IsSet()

	if !r.disable3D {
		s, err := loadStageFromCache(ctx, cidToPath(ctx, defaultPath, cid))
		if err != nil {
			log.Printf("ERROR: failed to load stage: %+v", err)

			r.disable3D = true
		}

		r.stage = s
	}

	return r
}

func (r *Stage) Update() {
	if r.disable3D {
		return
	}

	r.tick++

	if len(r.audience) != 0 {
		audienceDir := 0

		if fft := audio.MusicFFT(); fft != nil {
			total := uint64(0)

			for i := len(fft) * 16 / 48000; i < len(fft)*8000/48000; i++ {
				total += uint64(fft[i])
			}

			switch {
			case total == 0:
				audienceDir = 0
			case total < r.fftAvg:
				audienceDir = -1
			default:
				audienceDir = 1
			}

			if r.fftAvg == 0 {
				r.fftAvg = total
			} else {
				r.fftAvg = (r.fftAvg + total) / 2
			}
		}

		for _, a := range r.audience {
			a.Update(audienceDir)
		}
	}

	for _, p := range r.players {
		if p != nil {
			p.Update()
		}
	}

	r.lock.Lock()

	props := defaultCustomStageProperties
	if r.stage != nil {
		props = r.stage.prop
	}

	r.lock.Unlock()

	r.IsFlipped = r.UpdateCamera(&props, r.tick, int(r.player), &r.cam)
}

func DefaultUpdateCamera(props *CustomStageProperties, tick uint64, player int, cam *gfx.Camera) bool {
	w, h := gfx.Size()

	s, c := math.Sincos(float64(tick) * math.Pi / 30 / 140)
	aspect := float32(w) / float32(h)
	sx, sy := float32(1.0), float32(1.0)

	if aspect > 16.0/9.0 {
		sy = aspect * 9.0 / 16.0
		aspect = 16.0 / 9.0
	} else if aspect < 3.0/4.0 {
		sx, sy = aspect*4.0/3.0, aspect*4.0/3.0
	}

	if player == 2 {
		sx = -sx
	}

	if internal.PrefersReducedMotion() {
		s, c = 0, 0
	}

	var m0, m1 gfx.Matrix

	m0.Perspective(-45*math.Pi/180, aspect, 0.3, 50)
	m1.Scale(sx, sy, 1)
	cam.Perspective.Multiply(&m0, &m1)
	cam.Position.Translation(props.CamX, props.CamY, props.CamZ)
	cam.Offset.Translation(0, 2.5, -7.5)
	m0.RotationX(float32((12 + 4*c) * math.Pi / 180))
	m1.RotationY(float32((5 * s) * math.Pi / 180))
	cam.Rotation.Multiply(&m0, &m1)
	cam.MarkDirty()

	return player == 2
}

func ZoomCamera(target gfx.Matrix, nearZ float32) func(props *CustomStageProperties, tick uint64, player int, cam *gfx.Camera) bool {
	return func(_ *CustomStageProperties, _ uint64, player int, cam *gfx.Camera) bool {
		w, h := gfx.Size()

		aspect := float32(w) / float32(h)
		sx, sy := float32(1.0), float32(1.0)

		if aspect > 16.0/9.0 {
			sy = aspect * 9.0 / 16.0
			aspect = 16.0 / 9.0
		} else if aspect < 3.0/4.0 {
			sx, sy = aspect*4.0/3.0, aspect*4.0/3.0
		}

		if player == 2 {
			sx = -sx
		}

		var m0, m1 gfx.Matrix

		m0.Perspective(-45*math.Pi/180, aspect, nearZ, 50)
		m1.Scale(sx, sy, 1)
		cam.Perspective.Multiply(&m0, &m1)
		m0.Multiply(&cam.Position, &cam.Rotation)
		cam.Position.Multiply(&m0, &cam.Offset)
		cam.Rotation.Identity()
		cam.Offset.Identity()

		for i := range cam.Position {
			cam.Position[i] = cam.Position[i]*0.9 + target[i]*0.1
		}

		cam.MarkDirty()

		return player == 2
	}
}

func (r *Stage) Render() {
	if r.disable3D {
		gfx.GL.ClearColor(0.25, 0.25, 0.25, 1)

		return
	}

	if r.IsFlipped {
		gfx.GL.FrontFace(gl.CW)
		defer gfx.GL.FrontFace(gl.CCW)
	}

	r.lock.Lock()
	stage := r.stage
	r.lock.Unlock()

	props := defaultCustomStageProperties
	if stage != nil {
		props = stage.prop
	}

	gfx.GL.ClearColor(props.ClearR, props.ClearG, props.ClearB, 1)

	if r.Before != nil {
		r.Before(&props, r.tick, int(r.player), &r.cam)
	}

	if stage != nil {
		stage.Render(&r.cam, r.IsFlipped)
	}

	if r.batch == nil {
		r.batch = sprites.NewBatch(&r.cam)
		r.batch2 = sprites.NewBatch(&r.cam)
	} else {
		r.batch.Reset(&r.cam)
		r.batch2.Reset(&r.cam)
	}

	for _, a := range r.audience {
		if a.Z < 0 {
			break
		}

		a.Render(r.batch, props.SpriteScale)
	}

	for _, p := range r.players {
		if p != nil {
			p.Render(r.batch, props.SpriteScale, sprites.FlagNoDiscard, 0, 0, 0)
		}
	}

	for _, a := range r.audience {
		if a.Z >= 0 {
			continue
		}

		a.Render(r.batch2, props.SpriteScale)
	}

	if r.Paper {
		r.batch2.Append(sprites.CardBattle, props.CamX, props.CamY+3.5, props.CamZ-2.5, 0.85, 0.85, sprites.Color{R: 68, G: 68, B: 68, A: 51})
	}

	gfx.GL.DepthMask(false)
	gfx.GL.Enable(gl.DEPTH_TEST)
	r.batch.Render()
	gfx.GL.Disable(gl.DEPTH_TEST)
	r.batch2.Render()
	gfx.GL.DepthMask(true)
}

func (r *Stage) SetPlayer(i int) {
	if i < 0 || i > 2 {
		i = 0
	}

	r.player = uint8(i)
}

var errStagePlayerIndex = errors.New("room: unexpected index for stage player (expected 1 or 2)")

func (r *Stage) AddPlayer(p *Player, i int) {
	if i != 1 && i != 2 {
		panic(errStagePlayerIndex)
	}

	if i == 1 {
		p.X = 3
		p.Flip = true
	} else {
		p.X = 7
		p.Flip = false
	}

	p.Y = 0.5
	p.Z = 1

	r.players[i-1] = p
}

func (r *Stage) ClearAudience() {
	r.audience = r.audience[:0]
}

func (r *Stage) AddAudience(a ...*Audience) {
	r.lock.Lock()

	props := defaultCustomStageProperties
	if r.stage != nil {
		props = r.stage.prop
	}

	r.lock.Unlock()

	for _, member := range a {
		if member.Z < 0 {
			member.Y = 0.5 + props.FrontY
			member.Blur = member.Z*-0.025 + 0.025
		} else {
			member.Y = 0.5
			member.Blur = 0
		}

		r.audience = append(r.audience, member)
	}

	sort.Slice(r.audience, func(i, j int) bool {
		return r.audience[i].Z > r.audience[j].Z
	})
}

func (r *Stage) RemoveAudience(a *Audience) {
	for i, member := range r.audience {
		if member == a {
			r.audience = append(r.audience[:i], r.audience[i+1:]...)

			return
		}
	}
}

func (r *Stage) SetStage(ctx context.Context, defaultPath string, cid card.ContentIdentifier) {
	if r.disable3D {
		return
	}

	path := cidToPath(ctx, defaultPath, cid)

	r.lock.Lock()
	if r.stage != nil && r.stage.name == path {
		r.lock.Unlock()
		// already current stage
		return
	}

	placeholder := &customStage{prop: defaultCustomStageProperties}
	r.stage = placeholder
	r.lock.Unlock()

	go func() {
		s, err := loadStageFromCache(ctx, path)
		if err != nil {
			log.Println("ERROR: loading stage:", err)

			return
		}

		r.lock.Lock()
		if r.stage == placeholder {
			r.stage = s

			for _, a := range r.audience {
				if a.Z < 0 {
					a.Y = 0.5 + s.prop.FrontY
				}
			}
		} else {
			log.Println("DEBUG: discarding stage: stage was changed while loading")
		}
		r.lock.Unlock()
	}()
}
