package main

import (
	"fmt"
	"math"
	"os"
)

func compile(name string) {
	props, err := os.ReadFile(name + ".json")
	if err != nil && !os.IsNotExist(err) {
		fmt.Fprintf(os.Stderr, "mkstage: reading stage properties: %+v\n", err)
		os.Exit(1)

		return
	}

	if props != nil {
		compileV2(name, props)

		return
	}

	diffuse, err := readLegacyImage(name+"_diffuse", "png")
	if err == nil {
		compileV1(name, diffuse)

		return
	}

	if os.IsNotExist(err) {
		compileV0(name)

		return
	}

	fmt.Fprintf(os.Stderr, "mkstage: reading diffuse texture: %+v\n", err)
	os.Exit(1)
}

func compileV2(name string, props []byte) {
	diffuse, err := os.ReadFile(name + "_diffuse.webp")
	if err != nil {
		fmt.Fprintf(os.Stderr, "mkstage: reading diffuse texture: %+v\n", err)
		os.Exit(1)

		return
	}

	lighting, err := os.ReadFile(name + "_lighting.webp")
	if err != nil {
		fmt.Fprintf(os.Stderr, "mkstage: reading lighting texture: %+v\n", err)
		os.Exit(1)

		return
	}

	pos1, tex1, face1, _, err := readObj(name + "_diffuse.obj")
	if err != nil {
		fmt.Fprintf(os.Stderr, "mkstage: reading diffuse model: %+v\n", err)
		os.Exit(1)

		return
	}

	pos2, tex2, face2, _, err := readObj(name + "_lighting.obj")
	if err != nil {
		fmt.Fprintf(os.Stderr, "mkstage: reading lighting model: %+v\n", err)
		os.Exit(1)

		return
	}

	if len(pos1) != len(pos2) || len(face1) != len(face2) {
		fmt.Fprintln(os.Stderr, "mkstage: obj length mismatch")
		os.Exit(1)

		return
	}

	for i := range face1 {
		for j := range face1[i] {
			if pos1[face1[i][j][0]] != pos2[face2[i][j][0]] {
				fmt.Fprintln(os.Stderr, "mkstage: obj coord mismatch")
				os.Exit(1)

				return
			}
		}
	}

	w, err := os.Create(name + ".stage")
	if err != nil {
		fmt.Fprintf(os.Stderr, "mkstage: creating stage file: %+v\n", err)
		os.Exit(1)

		return
	}
	defer w.Close()

	data, elem, maxElem := convertObj1(pos1, tex1, tex2, face1, face2)

	writeV1or2(w, data, elem, maxElem, diffuse, lighting, props)
}

func compileV1(name string, diffuse []byte) {
	lighting, err := readLegacyImage(name+"_lighting", "jpeg")
	if err != nil {
		fmt.Fprintf(os.Stderr, "mkstage: reading lighting texture: %+v\n", err)
		os.Exit(1)

		return
	}

	pos1, tex1, face1, _, err := readObj(name + "_diffuse.obj")
	if err != nil {
		fmt.Fprintf(os.Stderr, "mkstage: reading diffuse model: %+v\n", err)
		os.Exit(1)

		return
	}

	pos2, tex2, face2, _, err := readObj(name + "_lighting.obj")
	if err != nil {
		fmt.Fprintf(os.Stderr, "mkstage: reading lighting model: %+v\n", err)
		os.Exit(1)

		return
	}

	if len(pos1) != len(pos2) || len(face1) != len(face2) {
		fmt.Fprintln(os.Stderr, "mkstage: obj length mismatch")
		os.Exit(1)

		return
	}

	for i := range face1 {
		for j := range face1[i] {
			if pos1[face1[i][j][0]] != pos2[face2[i][j][0]] {
				fmt.Fprintln(os.Stderr, "mkstage: obj coord mismatch")
				os.Exit(1)

				return
			}
		}
	}

	w, err := os.Create(name + ".stage")
	if err != nil {
		fmt.Fprintf(os.Stderr, "mkstage: creating stage file: %+v\n", err)
		os.Exit(1)

		return
	}
	defer w.Close()

	data, elem, maxElem := convertObj1(pos1, tex1, tex2, face1, face2)

	writeV1or2(w, data, elem, maxElem, diffuse, lighting, nil)
}

func compileV0(name string) {
	img, err := readLegacyImage(name, "jpeg")
	if err != nil {
		fmt.Fprintf(os.Stderr, "mkstage: reading texture: %+v\n", err)
		os.Exit(1)

		return
	}

	pos, tex, face, maxFace, err := readObj(name + ".obj")
	if err != nil {
		fmt.Fprintf(os.Stderr, "mkstage: reading model: %+v\n", err)
		os.Exit(1)

		return
	}

	if maxFace > math.MaxUint16 {
		fmt.Fprintln(os.Stderr, "mkstage: stage has too many polygons for v0")
		os.Exit(1)

		return
	}

	face16 := toFace16(face)

	data, elem := convertObj0(pos, tex, face16)

	w, err := os.Create(name + ".stage")
	if err != nil {
		fmt.Fprintf(os.Stderr, "mkstage: creating stage file: %+v\n", err)
		os.Exit(1)

		return
	}
	defer w.Close()

	writeV0(w, data, elem, img)
}

func readLegacyImage(name, legacyExt string) ([]byte, error) {
	webp, err := os.ReadFile(name + ".webp")
	if err == nil {
		return webp, nil
	}

	if !os.IsNotExist(err) {
		return nil, err //nolint:wrapcheck
	}

	return os.ReadFile(name + "." + legacyExt) //nolint:wrapcheck
}
