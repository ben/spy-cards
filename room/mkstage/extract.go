package main

import (
	"bytes"
	"encoding/binary"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"os"
	"reflect"
	"unsafe"
)

func extract(name string) {
	b, err := os.ReadFile(name + ".stage")
	if err != nil {
		fmt.Fprintf(os.Stderr, "mkstage: opening compiled stage: %+v\n", err)
		os.Exit(1)

		return
	}

	r := bytes.NewReader(b)

	version, err := binary.ReadUvarint(r)
	if err != nil {
		fmt.Fprintf(os.Stderr, "mkstage: reading stage version: %+v\n", err)
		os.Exit(1)

		return
	}

	if version > 2 {
		fmt.Fprintf(os.Stderr, "mkstage: unhandled stage version: %d\n", version)
		os.Exit(1)

		return
	}

	elements := extractBuffer(r, "element buffer")

	if len(elements)&1 != 0 {
		fmt.Fprintf(os.Stderr, "mkstage: element buffer contains non-integer number of elements: %d.5\n", len(elements)>>1)
		os.Exit(1)

		return
	}

	data := extractBuffer(r, "vertex buffer")

	switch version {
	case 0:
		extractV0(name, r, elements, data)
	case 1:
		extractV1orV2(name, r, elements, data, false)
	case 2:
		extractV1orV2(name, r, elements, data, true)
	}

	if _, err = r.ReadByte(); errors.Is(err, io.EOF) {
		return
	}

	if err == nil {
		fmt.Fprintln(os.Stderr, "mkstage: unexpected data after end of stage")
		os.Exit(1)

		return
	}

	fmt.Fprintf(os.Stderr, "mkstage: error reading stage: %+v\n", err)
	os.Exit(1)
}

func extractBuffer(r *bytes.Reader, name string) []byte {
	length, err := binary.ReadUvarint(r)
	if err != nil {
		fmt.Fprintf(os.Stderr, "mkstage: reading %s length: %+v\n", name, err)
		os.Exit(1)

		return nil
	}

	b := make([]byte, length)
	if _, err := io.ReadFull(r, b); err != nil {
		fmt.Fprintf(os.Stderr, "mkstage: reading %s: %+v\n", name, err)
		os.Exit(1)

		return nil
	}

	return b
}

func extractV0(name string, r *bytes.Reader, elementBytes, dataBytes []byte) {
	var (
		elements []uint16
		data     []struct {
			X, Y, Z float32
			S, T    float32
		}
	)

	elementsHeader := (*reflect.SliceHeader)(unsafe.Pointer(&elements))
	elementsHeader.Data = uintptr(unsafe.Pointer(&elementBytes[0]))
	elementsHeader.Len = len(elementBytes) / int(unsafe.Sizeof(elements[0]))
	elementsHeader.Cap = len(elementBytes) / int(unsafe.Sizeof(elements[0]))

	if len(dataBytes)%int(unsafe.Sizeof(data[0])) != 0 {
		fmt.Fprintf(os.Stderr, "mkstage: vertex buffer contains non-integer number of elements: %.2f\n", float64(len(dataBytes))/float64(unsafe.Sizeof(data[0])))
		os.Exit(1)

		return
	}

	dataHeader := (*reflect.SliceHeader)(unsafe.Pointer(&data))
	dataHeader.Data = uintptr(unsafe.Pointer(&dataBytes[0]))
	dataHeader.Len = len(dataBytes) / int(unsafe.Sizeof(data[0]))
	dataHeader.Cap = len(dataBytes) / int(unsafe.Sizeof(data[0]))

	extractObj(name+".obj", extractElement16(elements), func() (x, y, z, s, t float32, ok bool) {
		if len(data) == 0 {
			return
		}

		x = data[0].X
		y = data[0].Y
		z = data[0].Z
		s = data[0].S
		t = data[0].T
		ok = true

		data = data[1:]

		return
	})

	var err error

	lighting := extractBuffer(r, "lighting texture")
	if lighting[0] == 0x52 {
		err = os.WriteFile(name+".webp", lighting, 0644)
	} else {
		err = os.WriteFile(name+".jpeg", lighting, 0644)
	}

	if err != nil {
		fmt.Fprintf(os.Stderr, "mkstage: writing lighting texture: %+v\n", err)
		os.Exit(1)

		return
	}
}

func extractV1orV2(name string, r *bytes.Reader, elementBytes, dataBytes []byte, isV2 bool) {
	diffuse := extractBuffer(r, "diffuse texture")
	lighting := extractBuffer(r, "lighting texture")

	var err error

	if isV2 || diffuse[0] == 0x52 {
		err = os.WriteFile(name+"_diffuse.webp", diffuse, 0644)
	} else {
		err = os.WriteFile(name+"_diffuse.png", diffuse, 0644)
	}

	if err != nil {
		fmt.Fprintf(os.Stderr, "mkstage: writing diffuse texture: %+v\n", err)
		os.Exit(1)

		return
	}

	if isV2 || diffuse[0] == 0x52 {
		err = os.WriteFile(name+"_lighting.webp", lighting, 0644)
	} else {
		err = os.WriteFile(name+"_lighting.jpeg", lighting, 0644)
	}

	if err != nil {
		fmt.Fprintf(os.Stderr, "mkstage: writing lighting texture: %+v\n", err)
		os.Exit(1)

		return
	}

	var props struct {
		Element32 bool
	}

	if isV2 {
		propsBytes := extractBuffer(r, "stage properties")
		if err = json.Unmarshal(propsBytes, &props); err != nil {
			fmt.Fprintf(os.Stderr, "mkstage: decoding stage properties: %+v\n", err)
			os.Exit(1)

			return
		}

		if err = os.WriteFile(name+".json", propsBytes, 0644); err != nil {
			fmt.Fprintf(os.Stderr, "mkstage: writing stage properties: %+v\n", err)
			os.Exit(1)

			return
		}
	}

	var data []struct {
		X, Y, Z float32
		SD, TD  float32
		SL, TL  float32
	}

	makeDataReader := func(tex func(i int) (s, t float32)) func() (x, y, z, s, t float32, ok bool) {
		i := 0

		return func() (x, y, z, s, t float32, ok bool) {
			if i >= len(data) {
				return
			}

			x = data[i].X
			y = data[i].Y
			z = data[i].Z
			s, t = tex(i)
			ok = true

			i++

			return
		}
	}

	if len(dataBytes)%int(unsafe.Sizeof(data[0])) != 0 {
		fmt.Fprintf(os.Stderr, "mkstage: vertex buffer contains non-integer number of elements: %.2f\n", float64(len(dataBytes))/float64(unsafe.Sizeof(data[0])))
		os.Exit(1)

		return
	}

	dataHeader := (*reflect.SliceHeader)(unsafe.Pointer(&data))
	dataHeader.Data = uintptr(unsafe.Pointer(&dataBytes[0]))
	dataHeader.Len = len(dataBytes) / int(unsafe.Sizeof(data[0]))
	dataHeader.Cap = len(dataBytes) / int(unsafe.Sizeof(data[0]))

	if props.Element32 {
		var elements []uint32

		if len(elementBytes)&2 != 0 {
			fmt.Fprintf(os.Stderr, "mkstage: element buffer contains non-integer number of elements: %d.%d\n", len(elementBytes)>>2, (len(elementBytes)&3)*25)
			os.Exit(1)

			return
		}

		elementsHeader := (*reflect.SliceHeader)(unsafe.Pointer(&elements))
		elementsHeader.Data = uintptr(unsafe.Pointer(&elementBytes[0]))
		elementsHeader.Len = len(elementBytes) / int(unsafe.Sizeof(elements[0]))
		elementsHeader.Cap = len(elementBytes) / int(unsafe.Sizeof(elements[0]))

		extractObj(name+"_diffuse.obj", extractElement32(elements), makeDataReader(func(i int) (s, t float32) {
			return data[i].SD, data[i].TD
		}))

		extractObj(name+"_lighting.obj", extractElement32(elements), makeDataReader(func(i int) (s, t float32) {
			return data[i].SL, data[i].TL
		}))
	} else {
		var elements []uint16

		elementsHeader := (*reflect.SliceHeader)(unsafe.Pointer(&elements))
		elementsHeader.Data = uintptr(unsafe.Pointer(&elementBytes[0]))
		elementsHeader.Len = len(elementBytes) / int(unsafe.Sizeof(elements[0]))
		elementsHeader.Cap = len(elementBytes) / int(unsafe.Sizeof(elements[0]))

		extractObj(name+"_diffuse.obj", extractElement16(elements), makeDataReader(func(i int) (s, t float32) {
			return data[i].SD, data[i].TD
		}))

		extractObj(name+"_lighting.obj", extractElement16(elements), makeDataReader(func(i int) (s, t float32) {
			return data[i].SL, data[i].TL
		}))
	}
}

func extractElement16(elements []uint16) func() (a, b, c int, ok bool) {
	return func() (a, b, c int, ok bool) {
		if len(elements) < 3 {
			return
		}

		a = int(elements[0])
		b = int(elements[1])
		c = int(elements[2])
		ok = true

		elements = elements[3:]

		return
	}
}

func extractElement32(elements []uint32) func() (a, b, c int, ok bool) {
	return func() (a, b, c int, ok bool) {
		if len(elements) < 3 {
			return
		}

		a = int(elements[0])
		b = int(elements[1])
		c = int(elements[2])
		ok = true

		elements = elements[3:]

		return
	}
}

func extractObj(name string, element func() (a, b, c int, ok bool), vertex func() (x, y, z, s, t float32, ok bool)) {
	f, err := os.Create(name)
	if err != nil {
		fmt.Fprintf(os.Stderr, "mkstage: creating %q: %+v\n", name, err)
		os.Exit(1)

		return
	}

	check := func(_ int, err error) {
		if err != nil {
			fmt.Fprintf(os.Stderr, "mkstage: writing %q: %+v\n", name, err)
			os.Exit(1)
		}
	}

	defer func() {
		check(0, f.Close())
	}()

	check(fmt.Fprintln(f, "# Extracted by mkstage"))

	for {
		x, y, z, s, t, ok := vertex()
		if !ok {
			break
		}

		check(fmt.Fprintf(f, "v %.6f %.6f %.6f\n", x, y, z))
		check(fmt.Fprintf(f, "vt %.6f %.6f\n", s, t))
	}

	for {
		a, b, c, ok := element()
		if !ok {
			break
		}

		a++
		b++
		c++

		check(fmt.Fprintf(f, "f %d/%d %d/%d %d/%d\n", a, a, b, b, c, c))
	}
}
