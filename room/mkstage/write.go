package main

import (
	"encoding/binary"
	"encoding/json"
	"io"
	"math"
	"strconv"
)

func writeV1or2(w io.Writer, data []float32, elem []uint32, maxElem uint32, img1, img2, props []byte) {
	var varint [binary.MaxVarintLen64]byte

	var someProps struct {
		Element32 bool
	}

	if len(props) != 0 {
		if err := json.Unmarshal(props, &someProps); err != nil {
			panic(err)
		}
	}

	if maxElem > math.MaxUint16 && !someProps.Element32 {
		panic("element count (" + strconv.FormatUint(uint64(maxElem)+1, 10) + ") is too high to store without Element32 flag set")
	}

	version := uint64(1)
	if len(props) != 0 {
		version = 2
	}

	_, err := w.Write(varint[:binary.PutUvarint(varint[:], version)])
	if err != nil {
		panic(err)
	}

	if someProps.Element32 {
		_, err = w.Write(varint[:binary.PutUvarint(varint[:], uint64(len(elem)*4))])
		if err != nil {
			panic(err)
		}

		err = binary.Write(w, binary.LittleEndian, elem)
		if err != nil {
			panic(err)
		}
	} else {
		_, err = w.Write(varint[:binary.PutUvarint(varint[:], uint64(len(elem)*2))])
		if err != nil {
			panic(err)
		}

		elem16 := make([]uint16, len(elem))
		for i, x := range elem {
			elem16[i] = uint16(x)
		}

		err = binary.Write(w, binary.LittleEndian, elem16)
		if err != nil {
			panic(err)
		}
	}

	_, err = w.Write(varint[:binary.PutUvarint(varint[:], uint64(len(data)*4))])
	if err != nil {
		panic(err)
	}

	err = binary.Write(w, binary.LittleEndian, data)
	if err != nil {
		panic(err)
	}

	_, err = w.Write(varint[:binary.PutUvarint(varint[:], uint64(len(img1)))])
	if err != nil {
		panic(err)
	}

	_, err = w.Write(img1)
	if err != nil {
		panic(err)
	}

	_, err = w.Write(varint[:binary.PutUvarint(varint[:], uint64(len(img2)))])
	if err != nil {
		panic(err)
	}

	_, err = w.Write(img2)
	if err != nil {
		panic(err)
	}

	if len(props) != 0 {
		_, err = w.Write(varint[:binary.PutUvarint(varint[:], uint64(len(props)))])
		if err != nil {
			panic(err)
		}

		_, err = w.Write(props)
		if err != nil {
			panic(err)
		}
	}
}

func writeV0(w io.Writer, data []float32, elem []uint16, img []byte) {
	var varint [binary.MaxVarintLen64]byte

	_, err := w.Write(varint[:binary.PutUvarint(varint[:], 0)])
	if err != nil {
		panic(err)
	}

	_, err = w.Write(varint[:binary.PutUvarint(varint[:], uint64(len(elem)*2))])
	if err != nil {
		panic(err)
	}

	err = binary.Write(w, binary.LittleEndian, elem)
	if err != nil {
		panic(err)
	}

	_, err = w.Write(varint[:binary.PutUvarint(varint[:], uint64(len(data)*4))])
	if err != nil {
		panic(err)
	}

	err = binary.Write(w, binary.LittleEndian, data)
	if err != nil {
		panic(err)
	}

	_, err = w.Write(varint[:binary.PutUvarint(varint[:], uint64(len(img)))])
	if err != nil {
		panic(err)
	}

	_, err = w.Write(img)
	if err != nil {
		panic(err)
	}
}
