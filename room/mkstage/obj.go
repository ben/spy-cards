package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"strconv"
	"strings"
)

func readObj(name string) (pos [][3]float32, tex [][2]float32, face [][3][2]uint32, maxFace uint32, err error) {
	f, err := os.Open(name)
	if err != nil {
		return
	}
	defer f.Close()

	return parseObj(f)
}

func parseObj(r io.Reader) (pos [][3]float32, tex [][2]float32, face [][3][2]uint32, maxFace uint32, err error) {
	s := bufio.NewScanner(r)

	for s.Scan() {
		parts := strings.Split(s.Text(), " ")

		switch parts[0] {
		case "#":
			// comment
		case "v":
			x, err := strconv.ParseFloat(parts[1], 32)
			if err != nil {
				return nil, nil, nil, 0, fmt.Errorf("mkstage: parsing vertex x coordinate: %w", err)
			}

			y, err := strconv.ParseFloat(parts[2], 32)
			if err != nil {
				return nil, nil, nil, 0, fmt.Errorf("mkstage: parsing vertex y coordinate: %w", err)
			}

			z, err := strconv.ParseFloat(parts[3], 32)
			if err != nil {
				return nil, nil, nil, 0, fmt.Errorf("mkstage: parsing vertex z coordinate: %w", err)
			}

			pos = append(pos, [3]float32{float32(x), float32(y), float32(z)})
		case "vt":
			s, err := strconv.ParseFloat(parts[1], 32)
			if err != nil {
				return nil, nil, nil, 0, fmt.Errorf("mkstage: parsing vertex s coordinate: %w", err)
			}

			t, err := strconv.ParseFloat(parts[2], 32)
			if err != nil {
				return nil, nil, nil, 0, fmt.Errorf("mkstage: parsing vertex t coordinate: %w", err)
			}

			tex = append(tex, [2]float32{float32(s), float32(t)})
		case "l":
			// polyline (ignore)
		case "s":
			// smooth/sharp (ignore)
		case "f":
			var f [3][2]uint32

			for i := 1; i <= 3; i++ {
				if _, err := fmt.Sscanf(parts[i], "%d/%d", &f[i-1][0], &f[i-1][1]); err != nil {
					return nil, nil, nil, 0, fmt.Errorf("mkstage: parsing face vertex indices: %w", err)
				}

				f[i-1][0]--
				f[i-1][1]--

				if maxFace < f[i-1][0] {
					maxFace = f[i-1][0]
				}

				if maxFace < f[i-1][1] {
					maxFace = f[i-1][1]
				}
			}

			face = append(face, f)
		default:
			return nil, nil, nil, 0, fmt.Errorf("mkstage: unexpected Wavefront OBJ command %q", parts[0])
		}
	}

	return pos, tex, face, maxFace, s.Err() //nolint:wrapcheck
}
