package main

import "math"

func convertObj0(pos [][3]float32, tex [][2]float32, face [][3][2]uint16) (data []float32, elem []uint16) {
	m := make(map[[2]uint16]uint16)

	for _, f := range face {
		for _, v := range f {
			vi, ok := m[v]
			if !ok {
				vi = uint16(len(m))
				m[v] = vi

				data = append(data, pos[v[0]][:]...)
				data = append(data, tex[v[1]][:]...)
			}

			elem = append(elem, vi)
		}
	}

	return data, elem
}

func convertObj1(pos [][3]float32, tex1, tex2 [][2]float32, face1, face2 [][3][2]uint32) (data []float32, elem []uint32, maxElem uint32) {
	m := make(map[[3]uint32]uint32)

	for i := range face1 {
		for j := range face1[i] {
			v := [3]uint32{
				face1[i][j][0],
				face1[i][j][1],
				face2[i][j][1],
			}

			vi, ok := m[v]
			if !ok {
				if len(m) > math.MaxUint32 {
					panic("too many faces")
				}

				vi = uint32(len(m))
				m[v] = vi

				data = append(data, pos[v[0]][:]...)
				data = append(data, tex1[v[1]][:]...)
				data = append(data, tex2[v[2]][:]...)
			}

			elem = append(elem, vi)
		}
	}

	return data, elem, uint32(len(m) - 1)
}

func toFace16(face32 [][3][2]uint32) [][3][2]uint16 {
	face16 := make([][3][2]uint16, len(face32))

	for i := range face32 {
		face16[i][0][0] = uint16(face32[i][0][0])
		face16[i][0][1] = uint16(face32[i][0][1])
		face16[i][1][0] = uint16(face32[i][1][0])
		face16[i][1][1] = uint16(face32[i][1][1])
		face16[i][2][0] = uint16(face32[i][2][0])
		face16[i][2][1] = uint16(face32[i][2][1])
	}

	return face16
}
