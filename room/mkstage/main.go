package main

import (
	"flag"
	"fmt"
	"os"
)

var flagExtract = flag.Bool("x", false, "extract rather than creating a stage file")

func main() {
	flag.Usage = func() {
		fmt.Fprintf(os.Stderr, "Usage: %s [options] <name>\n", os.Args[0])
		fmt.Fprintln(os.Stderr)
		fmt.Fprintln(os.Stderr, "Required Files:")
		fmt.Fprintln(os.Stderr, "  v0: <name>.obj, <name>.(webp|jpeg)")
		fmt.Fprintln(os.Stderr, "  v1: <name>_diffuse.obj, <name>_lighting.obj, <name>_diffuse.(webp|png), <name>_lighting.(webp|jpeg)")
		fmt.Fprintln(os.Stderr, "  v2: <name>.json, <name>_diffuse.obj, <name>_lighting.obj <name>_diffuse.webp, <name>_lighting.webp")
		fmt.Fprintln(os.Stderr, "  extract: <name>.stage")
		fmt.Fprintln(os.Stderr)
		fmt.Fprintln(os.Stderr, "Options:")
		flag.PrintDefaults()

		os.Exit(2)
	}

	flag.Parse()

	if flag.NArg() != 1 {
		flag.Usage()
	}

	name := flag.Arg(0)

	if *flagExtract {
		extract(name)

		return
	}

	compile(name)
}
