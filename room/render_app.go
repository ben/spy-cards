//go:build (!js || !wasm) && !headless
// +build !js !wasm
// +build !headless

package room

import (
	"git.lubar.me/ben/spy-cards/gfx"
	"git.lubar.me/ben/spy-cards/gfx/sprites"
	"golang.org/x/mobile/gl"
)

func (s *customStage) Render(cam *gfx.Camera, flipped bool) {
	if s.buf == nil {
		return
	}

	gfx.Lock.Lock()

	gfx.GL.UseProgram(roomProgram.Program)

	gfx.StatRecordTextureUse(s.tex)
	gfx.StatRecordTextureUse(s.lm)
	gfx.StatRecordTriangleBatch(s.buf.Count)

	gfx.GL.Disable(gl.BLEND)
	gfx.GL.Enable(gl.SAMPLE_ALPHA_TO_COVERAGE)

	if s.noCull {
		gfx.GL.Disable(gl.CULL_FACE)
	}

	if !s.noDepth {
		gfx.GL.Enable(gl.DEPTH_TEST)
	}

	var combined gfx.Matrix

	gfx.GL.UniformMatrix4fv(roomPerspective.Uniform, cam.Perspective[:])
	cam.Combined(&combined)
	gfx.GL.UniformMatrix4fv(roomCamera.Uniform, combined[:])

	var flip [16]float32

	if flipped {
		copy(flip[:], s.prop.FlipUV)
	}

	gfx.GL.Uniform4fv(roomFlipUV.Uniform, flip[:])

	gfx.GL.Uniform1i(roomDiffuse.Uniform, 0)
	gfx.GL.Uniform1i(roomLightmap.Uniform, 1)

	gfx.GL.ActiveTexture(gl.TEXTURE1)
	gfx.GL.BindTexture(gl.TEXTURE_2D, s.lm.LazyTexture(sprites.Blank.Texture()))
	gfx.GL.ActiveTexture(gl.TEXTURE0)
	gfx.GL.BindTexture(gl.TEXTURE_2D, s.tex.LazyTexture(sprites.Blank.Texture()))

	s.buf.Bind()

	if s.prop.Element32 {
		gfx.GL.DrawElements(gl.TRIANGLES, s.buf.Count, gl.UNSIGNED_INT, 0)
	} else {
		gfx.GL.DrawElements(gl.TRIANGLES, s.buf.Count, gl.UNSIGNED_SHORT, 0)
	}

	s.buf.Unbind()

	if !s.noDepth {
		gfx.GL.Disable(gl.DEPTH_TEST)
	}

	if s.noCull {
		gfx.GL.Enable(gl.CULL_FACE)
	}

	gfx.GL.Disable(gl.SAMPLE_ALPHA_TO_COVERAGE)
	gfx.GL.Enable(gl.BLEND)

	gfx.Lock.Unlock()
}
