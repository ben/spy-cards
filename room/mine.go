//go:build !headless
// +build !headless

package room

import (
	"context"
	"fmt"
	"log"
	"math"
	"time"

	"git.lubar.me/ben/spy-cards/audio"
	"git.lubar.me/ben/spy-cards/gfx"
	"git.lubar.me/ben/spy-cards/gfx/sprites"
	"git.lubar.me/ben/spy-cards/input"
	"git.lubar.me/ben/spy-cards/internal"
	"git.lubar.me/ben/spy-cards/internal/router"
	"golang.org/x/mobile/gl"
)

const mineSlots = 15

type mine struct {
	ictx      *input.Context
	cam       gfx.Camera
	textCam   gfx.Camera
	stageLoad chan *customStage
	stage     *customStage
	batch     *sprites.Batch
	tb        *sprites.Batch

	players   []*Player
	player    int
	selected  int
	frac      float32
	rot       float32
	blur      float32
	changed   bool
	confirmed bool
	disable3D bool
	onConfirm func(*Character)
}

func loadMine() (interface{}, error) {
	sprites.Cerise1.StartPreload()
	sprites.Tanjerin1.StartPreload()

	return loadStage(context.Background(), "img/room/mine.stage")
}

// PreloadMine loads the 3D model for the mine into cache.
func PreloadMine() error {
	if internal.LoadSettings().Disable3D || router.FlagNo3d.IsSet() {
		// don't preload stages if we aren't going to render them
		return nil
	}

	_, err := stageCache.Do("img/room/mine.stage", loadMine)
	if err != nil {
		return fmt.Errorf("room: loading mine: %w", err)
	}

	return nil
}

// NewMine returns a new character selection room.
func NewMine(ctx context.Context, onConfirm func(*Character)) Room {
	r := &mine{
		onConfirm: onConfirm,
	}
	r.cam.SetDefaults()
	r.cam.Position.Identity()

	var m0, m1, m2 gfx.Matrix

	m0.Translation(6.75, 0.25, -5.5)
	m2.RotationY(-0.125 * math.Pi)
	m1.Multiply(&m0, &m2)
	m0.RotationX(0.0625 * math.Pi)
	r.cam.Offset.Multiply(&m0, &m1)
	r.cam.Rotation.Identity()

	r.textCam.SetDefaults()
	r.textCam.Rotation.Identity()
	r.textCam.Position.Identity()
	r.textCam.Offset.Scale(-0.125, -0.125, -0.125)

	i := 0

	settings := internal.LoadSettings()
	initialCharacter := settings.Character

	found := false

	for _, c := range PlayerCharacters {
		if c.Hidden && initialCharacter != c.Name {
			continue
		}

		if c.Name == initialCharacter {
			r.player = i
			found = true
		}

		r.AddPlayer(NewPlayer(c), i)
		i++
	}

	r.ictx = input.GetContext(ctx)

	if !found {
		r.player = int(time.Now().UnixNano()) % len(r.players)
	}

	r.rot = -float32((r.player-1)%mineSlots) * math.Pi * 2 / (mineSlots + 1)

	r.disable3D = settings.Disable3D || router.FlagNo3d.IsSet()

	if !r.disable3D {
		for _, sound := range []*audio.Sound{audio.PageFlip, audio.CrowdGasp, audio.Buzzer} {
			sound.StartPreload()
		}

		r.stageLoad = make(chan *customStage, 1)

		go func() {
			m, err := stageCache.Do("img/room/mine.stage", loadMine)
			if err != nil {
				log.Printf("ERROR: failed to load mine: %+v", err)
			}

			r.stageLoad <- m.(*customStage)
		}()
	}

	return r
}

func (r *mine) AddPlayer(p *Player, i int) {
	if len(r.players) <= i {
		r.players = append(r.players, make([]*Player, i-len(r.players)+1)...)
	}

	r.players[i] = p
}

func (r *mine) SetPlayer(i int) {
	r.player = i
}

func (r *mine) Update() {
	playerMod := float32(math.Mod(float64(r.player)-float64(r.frac), mineSlots))
	if playerMod < 0 {
		playerMod += mineSlots
	}

	targetRot := -playerMod * math.Pi * 2 / (mineSlots + 1)
	moveRot := targetRot - r.rot

	for moveRot > math.Pi {
		moveRot -= math.Pi * 2
	}

	for moveRot < -math.Pi {
		moveRot += math.Pi * 2
	}

	r.blur = moveRot / (math.Pi * 2 / (mineSlots + 1)) * 0.125

	r.rot += moveRot * 0.1

	r.cam.Rotation.RotationY(r.rot)
	r.cam.MarkDirty()

	r.selected = r.player % len(r.players)
	if r.selected < 0 {
		r.selected += len(r.players)
	}

	if r.players[r.selected] != nil {
		if r.cam.Position[13] == 0 {
			r.cam.Position[13] = r.players[r.selected].Character.Sprites[0].Y1 * 0.75
		} else {
			r.cam.Position[13] = r.cam.Position[13]*0.95 + r.players[r.selected].Character.Sprites[0].Y1*0.0375
		}
	}

	for i := range r.players {
		if r.players[i] != nil {
			r.players[i].Update()
		}
	}

	w, h := gfx.PixelSize()

	gfx.GL.Viewport(0, 0, w, h)

	aspect := float32(w) / float32(h)
	sx, sy := float32(1.0), float32(1.0)

	if aspect > 16.0/9.0 {
		sy = aspect * 9.0 / 16.0
		aspect = 16.0 / 9.0
	}

	var m0, m1 gfx.Matrix

	m0.Perspective(-60*math.Pi/180, aspect, 0.3, 150)
	m1.Scale(sx, sy, 1)
	r.cam.Perspective.Multiply(&m0, &m1)

	r.textCam.Perspective.Scale(1, float32(w)/float32(h), 0.01)

	if !r.confirmed {
		if wheel := r.ictx.Wheel(); wheel != 0 {
			audio.PageFlip.PlaySoundGlobal(0, 0, 0)

			r.player -= int(math.Copysign(1, float64(wheel)))
			r.changed = true
		}

		mx, my, click := r.ictx.Mouse()
		lastX, _, lastClick := r.ictx.LastMouse()

		mouseConfirmed := false

		switch {
		case lastClick && !click && !r.ictx.IsMouseDrag() && sprites.TextHitTest(&r.textCam, sprites.FontBubblegumSans, r.players[r.selected].Character.DisplayName, 3.5, 0, 0, 1.5, 1.5, mx, my, true):
			mouseConfirmed = true
		case lastClick && click && mx != lastX:
			r.frac += (mx - lastX) * 3
		case !click:
			r.frac *= 0.9
		}

		switch {
		case mouseConfirmed || r.ictx.Consume(input.BtnConfirm):
			if r.players[r.selected] != nil {
				r.confirmed = true
				for i, p := range r.players {
					if i != r.selected && p != nil {
						p.BecomeAngry(-1)
					}
				}

				if r.changed {
					s := internal.LoadSettings()
					s.Character = r.players[r.selected].Character.Name
					internal.SaveSettings(s)
				}

				audio.CrowdGasp.PlaySoundGlobal(0, 0, 0)

				if r.onConfirm != nil {
					r.onConfirm(r.players[r.selected].Character)
				}
			} else {
				audio.Buzzer.PlaySoundGlobal(0, 0, 0)
			}
		case r.frac >= 1:
			r.frac--

			fallthrough
		case r.ictx.ConsumeAllowRepeat(input.BtnLeft, 30, 30):
			audio.PageFlip.PlaySoundGlobal(0, 0, 0)
			r.player--
			r.changed = true
		case r.frac <= -1:
			r.frac++

			fallthrough
		case r.ictx.ConsumeAllowRepeat(input.BtnRight, 30, 30):
			audio.PageFlip.PlaySoundGlobal(0, 0, 0)
			r.player++
			r.changed = true
		}
	}
}

func (r *mine) Render() {
	if r.disable3D {
		gfx.GL.ClearColor(0.25, 0.25, 0.25, 1)
		gfx.GL.Clear(gl.COLOR_BUFFER_BIT)
	} else {
		gfx.GL.ClearColor(0, 0, 0, 1)
		gfx.GL.Clear(gl.COLOR_BUFFER_BIT)

		if r.stage != nil {
			r.stage.Render(&r.cam, false)
		} else {
			select {
			case r.stage = <-r.stageLoad:
			default:
			}
		}

		gfx.GL.Enable(gl.DEPTH_TEST)

		if r.batch == nil {
			r.batch = sprites.NewBatch(&r.cam)
		} else {
			r.batch.Reset(&r.cam)
		}

		for i := mineSlots / 2; i >= -2; i-- {
			unclampedIndex := r.player + i

			index := unclampedIndex % len(r.players)
			if index < 0 {
				index += len(r.players)
			}

			if r.players[index] == nil {
				continue
			}

			slot := (unclampedIndex%mineSlots + mineSlots) % mineSlots
			spoke := float64(slot) * math.Pi * 2 / (mineSlots + 1)

			switch {
			case i < 0:
				r.players[index].Flip = true
				r.players[index].Color = sprites.Color{R: 191, G: 191, B: 191, A: 191}
			case i == 0:
				r.players[index].Flip = false
				r.players[index].Color = sprites.Color{R: 255, G: 255, B: 255, A: 255}
			case i > 0:
				r.players[index].Flip = false
				r.players[index].Color = sprites.Color{R: 127, G: 127, B: 127, A: 255}
			}

			sin, cos := math.Sincos(spoke)
			r.players[index].X = 6.75 * float32(cos)
			r.players[index].Z = 6.75 * float32(sin)

			scale := float32(1)
			if r.stage != nil {
				scale = r.stage.prop.SpriteScale
			}

			r.players[index].Render(r.batch, scale, sprites.FlagNoDiscard, 0, 0.125*math.Pi-r.rot, 0)
		}

		r.batch.Render()

		gfx.GL.Disable(gl.DEPTH_TEST)
	}

	if r.tb == nil {
		r.tb = sprites.NewBatch(&r.textCam)
	} else {
		r.tb.Reset(&r.textCam)
	}

	if r.players[r.selected] != nil {
		mx, my, _ := r.ictx.Mouse()
		hover := sprites.TextHitTest(&r.textCam, sprites.FontBubblegumSans, r.players[r.selected].Character.DisplayName, 3.5, 0, 0, 1.5, 1.5, mx, my, true)

		borderColor := sprites.Black

		if r.confirmed || hover {
			borderColor = sprites.Rainbow
		}

		sprites.DrawTextBorder(r.tb, sprites.FontBubblegumSans, r.players[r.selected].Character.DisplayName, 3.5, 0, 0, 1.5, 1.5, sprites.White, borderColor, true)

		if !r.confirmed {
			sprites.DrawTextBorder(r.tb, sprites.FontD3Streetism, "Select with "+sprites.Button(input.BtnConfirm), 3.5, -2.25, 0, 0.65, 0.65, sprites.White, sprites.Black, true)
		}
	}

	if !r.confirmed {
		sprites.DrawTextBorder(r.tb, sprites.FontD3Streetism, "Rotate with "+sprites.Button(input.BtnLeft)+" "+sprites.Button(input.BtnRight), 3.5, -2.75, 0, 0.65, 0.65, sprites.White, sprites.Black, true)
	}

	r.tb.Render()
}
