// Package room implements the 3D scenes that appear behind Spy Cards Online matches.
package room

import (
	"bufio"
	"context"
	_ "embed" // embedded shaders
	"encoding/binary"
	"encoding/json"
	"errors"
	"fmt"
	"io"

	"git.lubar.me/ben/spy-cards/gfx"
	"git.lubar.me/ben/spy-cards/internal"
	"golang.org/x/mobile/gl"
)

// Room is a Spy Cards Online background.
type Room interface {
	Update()
	Render()
	SetPlayer(int)
	AddPlayer(p *Player, i int)
}

var (
	//go:embed room2.vs
	roomVertex2 string
	//go:embed room2.fs
	roomFragment2 string
	//go:embed room3.vs
	roomVertex3 string
	//go:embed room3.fs
	roomFragment3 string
	roomProgram   = gfx.Shader("room", roomVertex2, roomFragment2, roomVertex3, roomFragment3)

	roomPerspective = roomProgram.Uniform("perspective")
	roomCamera      = roomProgram.Uniform("camera")
	roomFlipUV      = roomProgram.Uniform("flipUV")
	roomDiffuse     = roomProgram.Uniform("diffuse")
	roomLightmap    = roomProgram.Uniform("lightmap")

	posAttrib = roomProgram.Attrib("in_pos")
	texAttrib = roomProgram.Attrib("in_tex")
)

type customStage struct {
	name string
	buf  *gfx.StaticBuffer
	tex  *gfx.AssetTexture
	lm   *gfx.AssetTexture
	prop CustomStageProperties

	noCull  bool
	noDepth bool
}

var defaultCustomStageProperties = CustomStageProperties{
	SpriteScale: 1,
	CamX:        5,
	FrontY:      -0.5,
}

type CustomStageProperties struct {
	// Element32 determines whether the element buffer uses 32- or 16-bit
	// (the default) integers.
	//
	// Default: false
	Element32 bool

	// SpriteScale is the factor by which the size of player and audience
	// sprites are scaled.
	//
	// Default: 1
	SpriteScale float32

	// CamX is the camera X coordinate before offsetting.
	//
	// Default: 5
	CamX float32

	// CamY is the camera Y coordinate before offsetting.
	//
	// Default: 0
	CamY float32

	// CamZ is the camera Z coordinate before offsetting.
	//
	// Default: 0
	CamZ float32

	// FrontY is the Y coordinate offset of the audience near the camera.
	//
	// Default: -0.5
	FrontY float32

	// ClearR is the red component of the sky color.
	//
	// Default: 0
	ClearR float32

	// ClearG is the green component of the sky color.
	//
	// Default: 0
	ClearG float32

	// ClearB is the blue component of the sky color.
	//
	// Default: 0
	ClearB float32

	// FlipUV is an array of [s0, t0, s1, t1] rectangles in the diffuse
	// texture that should have their S coordinate reversed if the stage
	// is being rendered backwards.
	FlipUV []float32
}

var (
	errBadSize   = errors.New("room: bad size")
	errExtraData = errors.New("room: extra data after EOF")
)

type errStageVersion uint64

func (err errStageVersion) Error() string {
	return fmt.Sprintf("room: unhandled stage format version: %d", uint64(err))
}

func loadStage(_ context.Context, name string) (*customStage, error) {
	f, err := internal.OpenAsset(name)
	if err != nil {
		return nil, fmt.Errorf("room: loading stage %q: %w", name, err)
	}
	defer f.Close()

	r := bufio.NewReader(f)

	version, err := binary.ReadUvarint(r)
	if err != nil {
		return nil, fmt.Errorf("room: loading stage %q: %w", name, err)
	}

	if version > 2 {
		return nil, errStageVersion(version)
	}

	l, err := binary.ReadUvarint(r)
	if err != nil {
		return nil, fmt.Errorf("room: loading stage %q: %w", name, err)
	}

	if l&1 != 0 {
		return nil, fmt.Errorf("room: non-integer number of elements: %d.5: %w", l>>1, errBadSize)
	}

	elements := make([]uint8, l)
	if _, err := io.ReadFull(r, elements); err != nil {
		return nil, fmt.Errorf("room: loading stage %q: %w", name, err)
	}

	l, err = binary.ReadUvarint(r)
	if err != nil {
		return nil, fmt.Errorf("room: loading stage %q: %w", name, err)
	}

	data := make([]uint8, l)
	if _, err := io.ReadFull(r, data); err != nil {
		return nil, fmt.Errorf("room: loading stage %q: %w", name, err)
	}

	if version == 0 {
		data2 := make([]uint8, len(data)/20*28)

		for i, j := 0, 0; i < len(data); i, j = i+20, j+28 {
			copy(data2[j:j+12], data[i:i+12])
			copy(data2[j+20:j+28], data[i+12:i+20])
		}

		data = data2
	}

	baseTex := gfx.BlankAssetTexture()

	if version >= 1 {
		l, err = binary.ReadUvarint(r)
		if err != nil {
			return nil, fmt.Errorf("room: loading stage %q: %w", name, err)
		}

		baseTexData := make([]byte, l)
		if _, err := io.ReadFull(r, baseTexData); err != nil {
			return nil, fmt.Errorf("room: loading stage %q: %w", name, err)
		}

		mimeType := "image/png"
		if baseTexData[0] == 0x52 {
			mimeType = "image/webp"
		}

		baseTex = gfx.NewEmbeddedTexture(name+"_diffuse", false, false, baseTexData, mimeType, true)
	}

	l, err = binary.ReadUvarint(r)
	if err != nil {
		return nil, fmt.Errorf("room: loading stage %q: %w", name, err)
	}

	lightmapData := make([]byte, l)
	if _, err := io.ReadFull(r, lightmapData); err != nil {
		return nil, fmt.Errorf("room: loading stage %q: %w", name, err)
	}

	props := defaultCustomStageProperties

	if version >= 2 {
		l, err = binary.ReadUvarint(r)
		if err != nil {
			return nil, fmt.Errorf("room: loading stage %q: %w", name, err)
		}

		propertyData := make([]byte, l)
		if _, err := io.ReadFull(r, propertyData); err != nil {
			return nil, fmt.Errorf("room: loading stage %q: %w", name, err)
		}

		err := json.Unmarshal(propertyData, &props)
		if err != nil {
			return nil, fmt.Errorf("room: loading stage %q: %w", name, err)
		}
	}

	if _, err = r.ReadByte(); !errors.Is(err, io.EOF) {
		if err != nil {
			return nil, fmt.Errorf("room: loading stage %q: %w", name, err)
		}

		return nil, errExtraData
	}

	mimeType := "image/jpeg"
	if lightmapData[0] == 0x52 {
		mimeType = "image/webp"
	}

	lightmap := gfx.NewEmbeddedTexture(name+"_lightmap", false, false, lightmapData, mimeType, true)

	lightmap.StartPreload()

	if err := baseTex.Preload(); err != nil {
		return nil, fmt.Errorf("room: loading stage %q base texture: %w", name, err)
	}

	if err := lightmap.Preload(); err != nil {
		return nil, fmt.Errorf("room: loading stage %q lightmap: %w", name, err)
	}

	elementSize := 2
	if props.Element32 {
		elementSize = 4
	}

	buf := gfx.NewStaticBuffer(name, data, elements, elementSize, func() {
		gfx.GL.EnableVertexAttribArray(posAttrib.Attrib)
		gfx.GL.EnableVertexAttribArray(texAttrib.Attrib)

		gfx.GL.VertexAttribPointer(posAttrib.Attrib, 3, gl.FLOAT, false, 28, 0)
		gfx.GL.VertexAttribPointer(texAttrib.Attrib, 4, gl.FLOAT, false, 28, 12)
	}, func() {
		// leave attribs 0 and 1 enabled
	})

	return &customStage{
		name: name,
		buf:  buf,
		tex:  baseTex,
		lm:   lightmap,
		prop: props,

		noCull: version < 2,
	}, nil
}
