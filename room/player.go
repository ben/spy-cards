package room

import (
	"math"
	"time"

	"git.lubar.me/ben/spy-cards/gfx/sprites"
	"git.lubar.me/ben/spy-cards/internal"
	"git.lubar.me/ben/spy-cards/spoilerguard"
)

// Player is a Spy Cards Online player avatar.
type Player struct {
	Character *Character

	X     float32
	Y     float32
	Z     float32
	Scale float32
	Flip  bool
	Color sprites.Color
	Angry uint64
	Anim  uint64
}

var playerAnimationOffset uint64

// NewPlayer creates a Player with a given Character.
func NewPlayer(c *Character) *Player {
	playerAnimationOffset += 23

	return &Player{
		Character: mapCharacter(c),
		Scale:     1,
		Color:     sprites.White,
		Anim:      playerAnimationOffset % 40,
	}
}

func mapCharacter(c *Character) *Character {
	if sg := spoilerguard.LoadData(); sg != nil && sg.Menu&8 != 0 {
		return CharacterByName["tanjerin"]
	}

	return c
}

// Update is called once per 60th of a second to update animations.
func (p *Player) Update() {
	p.Anim++

	if p.Anim/20 >= 2 || internal.PrefersReducedMotion() {
		p.Anim = 0
	}

	if p.Angry != 0 && p.Angry != ^uint64(0) {
		p.Angry--
	}
}

// BecomeAngry makes the player use their "angry" sprite for the given amount of time.
//
// A negative duration causes the player to become angry indefinitely.
func (p *Player) BecomeAngry(d time.Duration) {
	if d < 0 {
		p.Angry = ^uint64(0)
	} else {
		p.Angry = uint64(d * 60 / time.Second)
	}
}

// Render draws the player to the screen.
func (p *Player) Render(sb *sprites.Batch, scale float32, flags sprites.RenderFlag, rx, ry, rz float32) {
	if p.Character == nil {
		return
	}

	sx, sy := p.Scale*scale, p.Scale*scale
	if p.Flip {
		sx = -sx
	}

	sprite := p.Character.Sprites[2]
	if p.Angry == 0 {
		sprite = p.Character.Sprites[p.Anim/20]
	}

	sb.AppendEx(sprites.AudienceShadow, p.X, p.Y+0.01, p.Z, sx, sy, sprites.Color{A: 127}, sprites.FlagNoDiscard, -math.Pi/2, 0, 0)
	sb.AppendEx(sprite, p.X, p.Y, p.Z, sx, sy, p.Color, flags, rx, ry, rz)
}
