//go:build js && wasm && !headless
// +build js,wasm,!headless

package room

import (
	"syscall/js"
	"unsafe"

	"git.lubar.me/ben/spy-cards/gfx"
	"git.lubar.me/ben/spy-cards/gfx/sprites"
	"git.lubar.me/ben/spy-cards/internal"
	"golang.org/x/mobile/gl"
)

var (
	jsUniformBuf = internal.Uint8Array.New(4 * 4 * 4 * 3)

	jsRender js.Value

	_ = gfx.Custom("renderStage", func() {
		jsRender = internal.Function.New("gl,prog,puni,cuni,funi,t0uni,t1uni,pbuf,cbuf,fbuf,pattr,tattr", `

return function renderStage(tex0, tex1, dbuf, ebuf, count, noCull, noDepth, element32, vao) {
	gl.useProgram(prog);

	gl.disable(gl.BLEND);
	gl.enable(gl.SAMPLE_ALPHA_TO_COVERAGE);

	if (noCull) {
		gl.disable(gl.CULL_FACE);
	}

	if (!noDepth) {
		gl.enable(gl.DEPTH_TEST);
	}

	gl.uniformMatrix4fv(puni, false, pbuf);
	gl.uniformMatrix4fv(cuni, false, cbuf);
	gl.uniform4fv(funi, fbuf);
	gl.uniform1i(t0uni, 0);
	gl.uniform1i(t1uni, 1);

	gl.activeTexture(gl.TEXTURE1);
	gl.bindTexture(gl.TEXTURE_2D, tex1);
	gl.activeTexture(gl.TEXTURE0);
	gl.bindTexture(gl.TEXTURE_2D, tex0);

	if (vao) {
		gl.bindVertexArray(vao);
	} else {
		gl.bindBuffer(gl.ARRAY_BUFFER, dbuf);

		gl.enableVertexAttribArray(pattr);
		gl.enableVertexAttribArray(tattr);

		gl.vertexAttribPointer(pattr, 3, gl.FLOAT, false, 28, 0);
		gl.vertexAttribPointer(tattr, 4, gl.FLOAT, false, 28, 12);

		gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, ebuf);
	}

	gl.drawElements(gl.TRIANGLES, count, element32 ? gl.UNSIGNED_INT : gl.UNSIGNED_SHORT, 0);

	if (vao) {
		gl.bindVertexArray(null);
	} else {
		// leave attribs 0 and 1 enabled
	}

	if (!noDepth) {
		gl.disable(gl.DEPTH_TEST);
	}

	if (noCull) {
		gl.enable(gl.CULL_FACE);
	}

	gl.disable(gl.SAMPLE_ALPHA_TO_COVERAGE);
	gl.enable(gl.BLEND);
}`).Invoke(
			gfx.GL.(gl.JSWrapper).JSValue(),
			*roomProgram.Program.Value,
			*roomPerspective.Uniform.Value,
			*roomCamera.Uniform.Value,
			*roomFlipUV.Uniform.Value,
			*roomDiffuse.Uniform.Value,
			*roomLightmap.Uniform.Value,
			internal.Float32Array.New(jsUniformBuf.Get("buffer"), 4*4*4*0, 4*4),
			internal.Float32Array.New(jsUniformBuf.Get("buffer"), 4*4*4*1, 4*4),
			internal.Float32Array.New(jsUniformBuf.Get("buffer"), 4*4*4*2, 4*4),
			*posAttrib.Attrib.Value,
			*texAttrib.Attrib.Value,
		)
	}, func() {
		jsRender = js.Null()
	})
)

func (s *customStage) Render(cam *gfx.Camera, flipped bool) {
	if s.buf == nil {
		return
	}

	gfx.Lock.Lock()

	var camBuf [3]gfx.Matrix

	camBuf[0] = cam.Perspective
	cam.Combined(&camBuf[1])

	if flipped {
		copy(camBuf[2][:], s.prop.FlipUV)
	}

	/*#nosec*/
	js.CopyBytesToJS(jsUniformBuf, (*[unsafe.Sizeof(camBuf)]byte)(unsafe.Pointer(&camBuf))[:])

	gfx.StatRecordTextureUse(s.tex)
	gfx.StatRecordTextureUse(s.lm)
	gfx.StatRecordTriangleBatch(s.buf.Count)

	vao := js.Undefined()
	if s.buf.VAO != (gl.VertexArray{}) {
		vao = *s.buf.VAO.Value
	}

	jsRender.Invoke(
		*s.tex.LazyTexture(sprites.Blank.Texture()).Value,
		*s.lm.LazyTexture(sprites.Blank.Texture()).Value,
		*s.buf.Data.Value,
		*s.buf.Element.Value,
		s.buf.Count,
		s.noCull,
		s.noDepth,
		s.prop.Element32,
		vao,
	)

	gfx.Lock.Unlock()
}
