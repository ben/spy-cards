uniform mat4 perspective;
uniform mat4 camera;
attribute vec3 in_pos;
attribute vec4 in_tex;
varying vec4 tex_coord;

void main() {
	gl_Position = perspective * camera * vec4(in_pos.x, in_pos.y, -in_pos.z, 1.0);
	tex_coord = vec4(in_tex.x, 1.0 - in_tex.y, in_tex.z, 1.0 - in_tex.w);
}
