package room

type DefaultStageInfo struct {
	Version [3]uint64
	ByMonth [12][16]string
}

const (
	// Generic Spy Cards tournament stage.
	stage0001 = "img/room/stage0001.stage"
	// Golden Hills outdoor area.
	stage0002 = "img/room/stage0002.stage"
	// Forsaken Lands outdoor area.
	stage0003 = "img/room/stage0003.stage"
	// Doppel's Underground Tavern.
	stage0004 = "img/room/stage0004.stage"
)

var onlyStage0001 = [16]string{
	stage0001, stage0001, stage0001, stage0001,
	stage0001, stage0001, stage0001, stage0001,
	stage0001, stage0001, stage0001, stage0001,
	stage0001, stage0001, stage0001, stage0001,
}

var mostStage0001 = [16]string{
	stage0002, stage0003, stage0001, stage0001,
	stage0001, stage0001, stage0001, stage0001,
	stage0001, stage0001, stage0001, stage0001,
	stage0001, stage0001, stage0001, stage0001,
}

var someStage0002 = [16]string{
	stage0002, stage0002, stage0002, stage0002,
	stage0003, stage0001, stage0001, stage0001,
	stage0001, stage0001, stage0001, stage0001,
	stage0001, stage0001, stage0001, stage0001,
}

var mostStage0002 = [16]string{
	stage0002, stage0002, stage0002, stage0002,
	stage0002, stage0002, stage0002, stage0002,
	stage0002, stage0002, stage0002, stage0002,
	stage0003, stage0001, stage0001, stage0001,
}

var someStage0003 = [16]string{
	stage0003, stage0003, stage0003, stage0003,
	stage0002, stage0001, stage0001, stage0001,
	stage0001, stage0001, stage0001, stage0001,
	stage0001, stage0001, stage0001, stage0001,
}

var mostStage0003 = [16]string{
	stage0003, stage0003, stage0003, stage0003,
	stage0003, stage0003, stage0003, stage0003,
	stage0003, stage0003, stage0003, stage0003,
	stage0002, stage0001, stage0001, stage0001,
}

var mostStage0001b = [16]string{
	stage0002, stage0003, stage0004, stage0004,
	stage0004, stage0004, stage0004, stage0001,
	stage0001, stage0001, stage0001, stage0001,
	stage0001, stage0001, stage0001, stage0001,
}

var someStage0002b = [16]string{
	stage0002, stage0002, stage0002, stage0002,
	stage0003, stage0004, stage0004, stage0004,
	stage0001, stage0001, stage0001, stage0001,
	stage0001, stage0001, stage0001, stage0001,
}

var mostStage0002b = [16]string{
	stage0002, stage0002, stage0002, stage0002,
	stage0002, stage0002, stage0002, stage0002,
	stage0002, stage0002, stage0002, stage0002,
	stage0003, stage0004, stage0001, stage0001,
}

var someStage0003b = [16]string{
	stage0003, stage0003, stage0003, stage0003,
	stage0002, stage0004, stage0004, stage0004,
	stage0001, stage0001, stage0001, stage0001,
	stage0001, stage0001, stage0001, stage0001,
}

var mostStage0003b = [16]string{
	stage0003, stage0003, stage0003, stage0003,
	stage0003, stage0003, stage0003, stage0003,
	stage0003, stage0003, stage0003, stage0003,
	stage0002, stage0004, stage0001, stage0001,
}

// DefaultStages per version and month, split into 16 buckets.
var DefaultStages = [...]DefaultStageInfo{
	{
		Version: [3]uint64{0, 0, 0},
		ByMonth: [12][16]string{
			onlyStage0001, onlyStage0001, onlyStage0001,
			onlyStage0001, onlyStage0001, onlyStage0001,
			onlyStage0001, onlyStage0001, onlyStage0001,
			onlyStage0001, onlyStage0001, onlyStage0001,
		},
	},
	{
		Version: [3]uint64{0, 3, 16},
		ByMonth: [12][16]string{
			mostStage0003, someStage0003, mostStage0001,
			mostStage0001, mostStage0001, someStage0002,
			someStage0002, someStage0002, mostStage0002,
			mostStage0002, someStage0003, mostStage0003,
		},
	},
	{
		Version: [3]uint64{0, 3, 17},
		ByMonth: [12][16]string{
			mostStage0003b, someStage0003b, mostStage0001b,
			mostStage0001b, mostStage0001b, someStage0002b,
			someStage0002b, someStage0002b, mostStage0002b,
			mostStage0002b, someStage0003b, mostStage0003b,
		},
	},
}
