precision mediump float;

uniform vec4 flipUV[4];

uniform sampler2D diffuse;
uniform sampler2D lightmap;
varying vec4 tex_coord;

vec2 checkFlip(vec4 flip, vec2 coord) {
	if (all(lessThan(vec4(flip.xy, coord), vec4(coord, flip.zw)))) {
		coord.x = flip.z - (coord.x - flip.x);
	}

	return coord;
}

void main() {
	vec2 diffuse_coord = tex_coord.xy;
	diffuse_coord = checkFlip(flipUV[0], diffuse_coord);
	diffuse_coord = checkFlip(flipUV[1], diffuse_coord);
	diffuse_coord = checkFlip(flipUV[2], diffuse_coord);
	diffuse_coord = checkFlip(flipUV[3], diffuse_coord);

	vec4 color = texture2D(diffuse, diffuse_coord);
	vec4 light = texture2D(lightmap, tex_coord.zw);
	gl_FragColor = color * light;

	if (gl_FragColor.a < 0.25) {
		discard;
	}

	gl_FragColor /= gl_FragColor.a;
}
