//go:build headless
// +build headless

package room

import (
	"context"

	"git.lubar.me/ben/spy-cards/card"
)

type Stage struct{}

func PreloadMine() error {
	return nil
}

func PreloadStage(ctx context.Context, defaultPath string, cid card.ContentIdentifier) error {
	return nil
}

func NewStage(ctx context.Context, defaultPath string, cid card.ContentIdentifier) *Stage {
	return &Stage{}
}

func (r *Stage) Update() {
}

func (r *Stage) Render() {
}

func (r *Stage) SetPlayer(i int) {
}

func (r *Stage) AddPlayer(p *Player, i int) {
}

func (r *Stage) ClearAudience() {
}

func (r *Stage) AddAudience(a ...*Audience) {
}

func (r *Stage) RemoveAudience(a *Audience) {
}

func (r *Stage) SetStage(ctx context.Context, defaultPath string, cid card.ContentIdentifier) {
}
