#version 300 es

uniform mat4 perspective;
uniform mat4 camera;
in vec3 in_pos;
in vec4 in_tex;
out vec4 tex_coord;

void main() {
	gl_Position = perspective * camera * vec4(in_pos.x, in_pos.y, -in_pos.z, 1.0);
	tex_coord = vec4(in_tex.x, 1.0 - in_tex.y, in_tex.z, 1.0 - in_tex.w);
}
