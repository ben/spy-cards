//go:build !headless
// +build !headless

package main

import (
	"time"

	"git.lubar.me/ben/gamepad"
	"git.lubar.me/ben/spy-cards/arcade/touchcontroller"
	"git.lubar.me/ben/spy-cards/gfx/sprites"
	"git.lubar.me/ben/spy-cards/input"
	"git.lubar.me/ben/spy-cards/internal"
)

var (
	keyHeld  = make(map[string]bool)
	controls = internal.LoadSettings().Controls

	lastKeyboard     time.Time
	lastGamepad      time.Time
	lastGamepadStyle = internal.StyleGenericGamepad
)

func init() {
	internal.OnSettingsChanged = append(internal.OnSettingsChanged, func(s *internal.Settings) {
		controls = s.Controls
	})
}

func getInputs() []input.Button {
	keyboardMap := defaultKeyButton
	if controls.Keyboard > 0 && controls.Keyboard <= len(controls.CustomKB) {
		keyboardMap = controls.CustomKB[controls.Keyboard-1].Code
	}

	gamepads := gamepad.List()

	var buttonHeld []input.Button

	for button := input.BtnUp; button <= input.BtnHelp; button++ {
		if keyHeld[keyboardMap[button]] {
			buttonHeld = append(buttonHeld, button)
			lastKeyboard = time.Now()
		}

		for _, gp := range gamepads {
			mapping := [input.NumButtons]internal.GamepadButton{
				input.BtnUp:      {Button: gp.ButtonToIndex(gamepad.BtnDUp)},
				input.BtnDown:    {Button: gp.ButtonToIndex(gamepad.BtnDDown)},
				input.BtnLeft:    {Button: gp.ButtonToIndex(gamepad.BtnDLeft)},
				input.BtnRight:   {Button: gp.ButtonToIndex(gamepad.BtnDRight)},
				input.BtnConfirm: {Button: gp.ButtonToIndex(gamepad.BtnA)},
				input.BtnCancel:  {Button: gp.ButtonToIndex(gamepad.BtnB)},
				input.BtnSwitch:  {Button: gp.ButtonToIndex(gamepad.BtnX)},
				input.BtnToggle:  {Button: gp.ButtonToIndex(gamepad.BtnY)},
				input.BtnPause:   {Button: gp.ButtonToIndex(gamepad.BtnStart)},
				input.BtnHelp:    {Button: gp.ButtonToIndex(gamepad.BtnSelect)},
			}
			style := internal.StyleGenericGamepad

			index := controls.Gamepad[gp.Name()]
			if index != 0 && index <= len(controls.Gamepad) {
				custom := controls.CustomGP[index-1]
				mapping = custom.Button

				style = custom.Style
				if style == internal.StyleKeyboard {
					style = internal.StyleGenericGamepad
				}
			} else if mapping[input.BtnConfirm].Button == -1 {
				continue
			}

			var pressed bool

			if mapped := mapping[button]; mapped.IsAxis {
				axis := gp.Axis(mapped.Button)

				if mapped.IsPositive {
					pressed = axis > 0.5
				} else {
					pressed = axis < -0.5
				}
			} else {
				_, pressed = gp.Button(mapped.Button)
			}

			if pressed {
				buttonHeld = append(buttonHeld, button)
				lastGamepad = time.Now()
				lastGamepadStyle = style
			}
		}
	}

	touchHeld, lastTouch := touchcontroller.Held()
	buttonHeld = append(buttonHeld, touchHeld...)

	switch {
	case lastTouch.After(lastGamepad) && lastTouch.After(lastKeyboard):
		sprites.ButtonStyle = internal.StyleGenericGamepad
	case lastGamepad.After(lastKeyboard):
		sprites.ButtonStyle = lastGamepadStyle
	default:
		sprites.ButtonStyle = internal.StyleKeyboard
	}

	return buttonHeld
}
