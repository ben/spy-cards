module git.lubar.me/ben/spy-cards

go 1.22.0

toolchain go1.23.3

require (
	git.lubar.me/ben/gamepad v0.0.6-0.20210729211940-b836abfca1ab
	github.com/BenLubar/convnet v1.1.3-0.20210701184423-7198c7938c25
	github.com/BenLubar/opus v0.0.0-20210627075520-3ea609ce74a4
	github.com/davecgh/go-spew v1.1.1
	github.com/golang/freetype v0.0.0-20170609003504-e2365dfdc4a0
	github.com/jackc/pgtype v1.14.4
	github.com/jackc/pgx/v4 v4.18.3
	github.com/mjibson/go-dsp v0.0.0-20180508042940-11479a337f12
	github.com/pion/webrtc/v3 v3.3.4
	golang.org/x/crypto v0.29.0
	golang.org/x/exp/shiny v0.0.0-20241108190413-2d47ceb2692f
	golang.org/x/image v0.22.0
	golang.org/x/mobile v0.0.0-20241108191957-fa514ef75a0f
	golang.org/x/sync v0.9.0
	gopkg.in/yaml.v2 v2.2.2
)

require (
	github.com/coder/websocket v1.8.12
	github.com/google/uuid v1.6.0 // indirect
	github.com/jackc/chunkreader/v2 v2.0.1 // indirect
	github.com/jackc/pgconn v1.14.3 // indirect
	github.com/jackc/pgio v1.0.0 // indirect
	github.com/jackc/pgpassfile v1.0.0 // indirect
	github.com/jackc/pgproto3/v2 v2.3.3 // indirect
	github.com/jackc/pgservicefile v0.0.0-20240606120523-5a60cdf6a761 // indirect
	github.com/pion/datachannel v1.5.9 // indirect
	github.com/pion/dtls/v2 v2.2.12 // indirect
	github.com/pion/ice/v2 v2.3.37 // indirect
	github.com/pion/interceptor v0.1.37 // indirect
	github.com/pion/logging v0.2.2 // indirect
	github.com/pion/mdns v0.0.12 // indirect
	github.com/pion/randutil v0.1.0 // indirect
	github.com/pion/rtcp v1.2.14 // indirect
	github.com/pion/rtp v1.8.9 // indirect
	github.com/pion/sctp v1.8.34 // indirect
	github.com/pion/sdp/v3 v3.0.9 // indirect
	github.com/pion/srtp/v2 v2.0.20 // indirect
	github.com/pion/stun v0.6.1 // indirect
	github.com/pion/transport/v2 v2.2.10 // indirect
	github.com/pion/turn/v2 v2.1.6 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	github.com/stretchr/testify v1.9.0 // indirect
	github.com/wlynxg/anet v0.0.5 // indirect
	golang.org/x/mod v0.22.0 // indirect
	golang.org/x/net v0.31.0 // indirect
	golang.org/x/sys v0.27.0 // indirect
	golang.org/x/text v0.20.0 // indirect
	golang.org/x/tools v0.27.0 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)

replace golang.org/x/mobile => git.lubar.me/ben/mobile v0.0.0-20241121212522-7698afc82093
