#!/bin/bash -e

GO=go

if true; then

go generate ./tables/tables3d/model

echo Building WebASM
# jsnowrapper is a local modification using the patch from https://golang.org/issue/44006
GOOS=js GOARCH=wasm go build -o www/spy-cards.wasm
brotli -f5 www/spy-cards.wasm
echo Building Linux
GOOS=linux GOARCH=amd64 $GO build -o /dev/null -tags headless

fi

if true; then

pushd godot-native
./build.sh
popd

rm -f www/godot/*
pushd godot
godot-latest --headless --import
godot-latest --headless --export-debug 'Web'
godot-latest --headless --export-release 'Web'
#godot-latest --headless --export-release 'Linux'
#godot-latest --headless --export-release 'Windows Desktop'
godot-latest --headless --export-release 'Dedicated Server'
popd
brotli -f5 www/godot/*.wasm www/godot/*.pck

fi

echo Building TypeScript
tsc --build tsconfig.json
terser -f semicolons=false -o www/script/spy-cards.js www/script/sc-*.js

cd www

mv script/service-worker.js .
cp -R ../assets/* .

echo Building Docs
for md in docs/*.md; do
	pandoc --template=docs/_template.html --toc --toc-depth=2 --lua-filter=docs/filter.lua --from=markdown-smart "$md" -o "${md//.md}.html"
done

echo Compressing
wait

echo Building SRI Hashes
sed -e 's/ integrity="[^"]*"//' -i *.html godot/*.html docs/*.html ../server/templates/*.tmpl
sha256sum script/*.js style/*.css | while read -r "hash" "file"; do
	b64hash="`xxd -r -p <<<"$hash" | base64`"
	sed -e 's#\("\|/\)'"$file"'?[0-9a-f]*"#\1'"$file"'?'"${hash:0:8}"'" integrity="sha256-'"$b64hash"'"#' -i *.html godot/*.html docs/*.html ../server/templates/*.tmpl
done

rm -f cache-data.txt
find * .well-known -type f -exec sha256sum {} + > cache-data.txt
config_hash=`sha256sum script/config.js | cut -d ' ' -f 1`
base32_hash=`sha256sum script/sc-base32.js | cut -d ' ' -f 1`
cache_hash=`sha256sum cache-data.txt | cut -d ' ' -f 1`
sed -e 's/%CONFIG_HASH%/'"${config_hash:0:8}"'/' \
    -e 's/%BASE32_HASH%/'"${base32_hash:0:8}"'/' \
    -e 's/%CACHE_HASH%/'"$cache_hash"'/' -i service-worker.js

echo Build Complete
