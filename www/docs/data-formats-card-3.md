---
title: Data Formats (Card v3 - Game Mode)
...

Since game modes and custom cards are shared using the same mechanism,
game mode format versions are disjoint with custom card versions.

(base64-encoded)

- uvarint FormatVersion (3)
- uvarint FieldCount
- GameModeField\[FieldCount\] Fields

# GameModeField

- uvarint Type
- uvarint DataLength
- byte\[DataLength\] Data

## 0: Metadata

- string Title
- string Author
- string Description
- string LatestChanges
- optional:
    - uint8 PortraitID
    - If PortraitID is 254 or 255:
        - uvarint CustomPortraitLength
        - byte\[CustomPortraitLength\] CustomPortrait

## 1: BannedCards

If BannedCards is present and BannedCardCount is 0, all vanilla cards are banned.

- uint8 Padding = 0
    - If this is not the first byte, or if the encoded data is exactly 1 byte in length, ignore this field and the Flags field.
- uvarint Flags
    - 0..1:
        - 0: cards cannot be picked for a deck or random-summoned
        - 1: cards are unpickable but can still be random-summoned
        - 2: cards do not appear on home screen card list
        - 3: (reserved)
- uvarint BannedCardCount
- uvarint\[BannedCardCount\] BannedCardIDs

## 2: GameRules

Repeated:

- uvarint Rule
- varint Value

If Rule is None, there is no value and this field ends.

Rules that have their default value are not encoded.

Rules and their default values are as follows:

- 0: None (no value)
- 1: MaxHP (5)
- 2: HandMinSize (3)
- 3: HandMaxSize (5) (max value 50)
- 4: DrawPerRound (2)
- 5: CardsPerDeck (15)
- 6: MinTP (2)
- 7: MaxTP (10)
- 8: TPPerRound (1)
- 9: BossCards (1)
- 10: MiniBossCards (2)
- 11: DuplicateBoss (1)
- 12: DuplicateMiniBoss (1)
- 13: DuplicateEffect (0)
- 14: DuplicateAttacker (0)
- 15: CardsPerRound (0)

Rules use unsigned varints unless otherwise specified.

## 3: SummonCard

- uvarint Flags
    - bit 0: both players (if not set, summon for player 1 only)
- uvarint CardID

## 4: Variant

- string Title
- string NPC
- uvarint FieldCount
- Field[FieldCount] Fields

## 5: UnfilterCard

- uvarint Flags
- uvarint CardID

## 6: DeckLimitFilter

- uvarint Count
- uint8 RankTribe (high 4 bits rank, low 4 bits tribe)
- If RankTribe is 128:
    - [CardFilter (v5)](data-formats-card-5.html#cardfilter) Filter
- If RankTribe is 255:
    - uvarint CardID
- Otherwise:
    - (only RankTribe is 14 mod 16) string CustomTribe
- If RankTribe is 128, optional:
    - uvarint CondCount (if not present, 0)
    - [CardFilter (v5)](data-formats-card-5.html#cardfilter) Condition

## 7: Timer

- uvarint StartTime
- uvarint MaxTime
- uvarint PerTurn
- uvarint MaxPerTurn

## 8: Turn0Effect

- uvarint Flags
- uvarint CardFormatVersion
- EffectDef Effect

## 9: VanillaVersion

- uvarint Version
    - 0: 1.05
    - 1: 1.1

## 10: CardGroup

- uvarint Flags
    - (reserved)
- string Title
- uvarint Count
- uvarint\[Count\] CardIDs

## 11: SpecialFlags

- uvarint FlagsFlags
    - (reserved)
- uvarint Count
- uvarint\[Count\] SetFlags
    - 0: (reserved)
    - 1: Allow Subtracting Infinities
    - 2: Use Effect Card Back
    - 3: No Damage on Win
    - 4: Reveal Card Selection
    - 5: Both Players are Player 1
    - 6: Auto-Ready if Negative TP
    - 7: Hide Game Log
