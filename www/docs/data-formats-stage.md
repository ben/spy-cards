---
title: Data Formats (Stage)
...

- uvarint Version = 0, 1, or 2
- uvarint ElementBufSize
- uint8\[ElementBufSize\] ElementBuf
- uvarint DataBufSize
- uint8\[DataBufSize\] DataBuf
- if version 1 or greater:
    - uvarint Tex1Length
    - uint8\[Tex1Length\] TextureSheet1 (PNG or WebP)
- uvarint Tex2Length
- uint8\[Tex2Length\] TextureSheet2 (JPEG or WebP)
- if version 2 or greater:
    - uvarint PropsLength
    - uint8\[PropsLength\] Props (JSON)

# Buffers

## Element Buffer

- repeated:
    - uint16\[3\] Triangle

## Data Buffer

- repeated:
    - float32\[3\] VertexPos
    - if version 1 or greater:
        - float32\[2\] TexCoord1
    - float32\[2\] TexCoord2

# Properties

## SpriteScale

(number; default 1)

The factor by which the size of player and audience sprites are scaled.

## CamX

(number; default 5)

The camera X coordinate before offsetting.

## CamY

(number; default 0)

The camera Y coordinate before offsetting.

## CamZ

(number; default 0)

The camera Z coordinate before offsetting.

## FrontY

(number; default -0.5)

The Y coordinate offset of the audience near the camera.

## ClearR

(number; default 0)

The red component of the sky color.

## ClearG

(number; default 0)

The green component of the sky color.

## ClearB

(number; default 0)

The blue component of the sky color.

## FlipUV

(array of 0, 4, 8, 12, or 16 numbers; default empty)

Regions of the diffuse texture to reverse along the S axis when rendering the stage backwards.
