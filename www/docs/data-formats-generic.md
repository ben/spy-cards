---
title: Data Formats (Generic)
...

# Binary-to-Text

- **Base64:** <https://tools.ietf.org/html/rfc4648#section-4>
- **Crockford Base32:** <https://crockford.com/base32.html>

# Varint

Defined the same way as it is defined in Protocol Buffers, that is:

- Byte values between 0 and 127 represent their own value and stop.
- Other byte values represent their 7 least significant bits and continue.
- Byte order is little-endian.

# Match Code

## Legacy Match Code

(Crockford Base32 encoded)

15 random bytes

The first 20 bits are overridden based on the client type:

- Vanilla: 11001101 10111100 1100
- Custom: 01100110 01110101 0100

Some custom game modes have their own prefixes.

The prefix followed by all zeroes is used for the quick join match code.

## Godot Match Code

Match codes in the Godot version of Spy Cards Online are simply 6 random Crockford Base32 characters.

# File ID

(Crockford Base32 encoded)

- 8 bytes: first 8 bytes of SHA-256 hash of file
- uvarint: opaque ID
- uvarint: file type ID
  - if this is 0, it is omitted from the encoding and the file is a PNG image (that is, the file ID of a PNG will end after the previous uvarint and an explicit 0 for this field is invalid)
  - if this is 1, the file is an OGG Opus file
  - if this is 2, the file is a glb (glTF binary) file
  - if this is 3, the file is a Spy Cards Online data container file
  - if this is 4, the file is a PNG image that is NOT uploaded to the server (it is sent along with the mode in ad-hoc custom play)
  - if this is 5, the file is an OGG Opus file that is NOT uploaded to the server (it is sent along with the mode in ad-hoc custom play)
  - if this is 6, the file is a glb (glTF binary) file that is NOT uploaded to the server (it is sent along with the mode in ad-hoc custom play)
  - if this is 7, the file is a Spy Cards Online data container file that is NOT uploaded to the server
