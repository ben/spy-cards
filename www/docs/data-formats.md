---
title: Data Formats
...

This page has been split into sub-pages as it has become very large.

- [Generic](data-formats-generic.html)
- [Legacy Card (v0 and v1)](data-formats-card-0.html)
- [Legacy Card (v2 and v4)](data-formats-card-2.html)
- [Legacy Game Mode (v3)](data-formats-card-3.html)
- [Legacy Card (v5)](data-formats-card-5.html)
- [Data Container (v0)](#TODO)
- [Deck](data-formats-deck.html)
- [Legacy Recording](data-formats-recording.html)
- [Legacy Termacade](data-formats-termacade.html)
- [Legacy Stage](data-formats-stage.html)
- [Appendices](data-formats-appendix.html)
