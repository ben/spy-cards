---
title: Discord Landing Page
...

Hi! You're probably thinking something like "WHAT? SPY CARDS ONLINE?!" right now. That's normal.

Here are some links to get you started:

## Wait, I'm not here from Discord!

If you want to talk to us, we're in the [#spy-cards-spoilers](https://discord.gg/bsyVDUE) channel. As the name implies, Spy Cards is very spoiler-heavy, so bail out now if you haven't played Bug Fables yet and are avoiding spoilers. If you're part of the way through the game, [Spoiler Guard](../spoiler-guard.html) can help, but tread lightly in the chat.

## I want to make a game mode

First things first, make the game mode in the [card editor](../game.html?custom=editor). If you need help understanding anything in the card editor, ask on Discord. We're more than happy to help you out.

When you're ready to publish the first version of your game mode, ask BenLubar to add it for you. You'll need to provide:

- The name of your game mode (eg. Midge Hell, this can be changed later).
- The URL identifier you want your game mode to have (eg. midge, this *cannot* be changed later).
- (optional) A match code prefix for your game mode (eg. M1G3). This enables the Auto Join button on the home screen. Match code prefixes are 4 characters and can contain numbers and letters except for I, L, O, and U.

Once the game mode is created and assigned to you, publishing revisions is self-service.

## NPCs

Nobody online to play at 3 AM? No problem! Just add `?vs-npc` to the end of the URL to have a simulated Bug Fables NPC player. These use the same AI as in the game. You can also specify `?vs-npc=tp2-generic` to get an opponent who's slightly better at deck building, or `?vs-npc=mender-spam` if you want to have a bad time. A full list of NPCs is available in the Variant field of the [card editor](../game.html?custom=editor&edit=mode).

## Decks

Decks you create, whether in the [deck editor](../game.html?deck) or during a match, get saved in your browser. There is no hard limit on the number of decks you can store.

Clicking on a deck generates a shareable link to it. For example, here are the decks used by [Chuck](../game.html?deck=4HH0000007VXYZFG84210GG8), [Arie](../game.html?deck=310J10G84212NANCPAD6K7VW), [Crow](../game.html?deck=101AXEPH8MCJ8G845G), [Shay](../game.html?deck=511KHRWE631GRD6K9MMA5840) and [Carmina](../game.html?deck=3P7T52H8MA5273HG842YF7KR). Carmina uses a [different deck](../game.html?deck=01H00000000013HSMR) in the tutorial, and all players on Metal Island create new decks for each tournament.

As of Bug Fables 1.1, Metal Island players can also choose one of several premade decks: <span class="space-links">[[1]](../game.html?deck=511KHRRE631GRD6K1M) [[2]](../game.html?deck=101AXEGH8MCJ8G845G) [[3]](../game.html?deck=01H00000000013HS4R) [[4]](../game.html?deck=3J7T52H8MA5273HG04) [[5]](../game.html?deck=4HH0000007VHYZFG04) [[6]](../game.html?deck=310J10G84212NANC68) [[7]](../game.html?deck=0K0WP8H9MT90J8NA4R) [[8]](../game.html?deck=74S6K98NJS8036K340) [[9]](../game.html?deck=5QBNJS93BNTJ73C608) [[10]](../game.html?deck=406H0G852H8GC63H0W) [[11]](../game.html?deck=6KM6VDGVE73HRWKS4W) [[12]](../game.html?deck=2GSQKSRXEQBHVGRC64) [[13]](../game.html?deck=7PAQKSRXEQBHVGRC64) [[14]](../game.html?deck=22RBDPSDQBNJXFQV5W)</span>

There is a [fifteenth Metal Island deck](../game.html?deck=81H0000007VHYZFG04) which is supposed to be available to Janet after completing the game, but due to a bug it is never chosen.

## Match Viewer

At the end of a match, you can upload a recording of the match to the server, which will give you a link to the match viewer. You can also specify [?recording=auto](../game.html?recording=auto) to have it auto-play random recordings.

## Termacade?!

[Yes](../arcade.html).

## APHID FESTIVAL???

[Yes](../festival.html).

## BUG FABLES: THE EVERLASTING FIGHTERS?!?!?!

[Kinda](../elf.html).
