---
title: Data Formats (Deck)
...

(Crockford Base32 encoded)

# Short Form

Byte 0:

- 1 bit (most significant): 0
- 5 bits: boss card index (see table below)
- 2 bits: top 2 bits of first mini-boss card index

Byte 1:

- 3 bits: bottom 3 bits of first mini-boss card index
- 5 bits: second mini-boss card index

Bytes 2, 5, 8, ...:

- 6 bits: enemy card index
- 2 bits: top 2 bits of enemy card index

Bytes 3, 6, 9, ...:

- 4 bits: bottom 4 bits of enemy card index
- 4 bits: top 4 bits of enemy card index

Bytes 4, 7, 10, ...:

- 2 bits: bottom 2 bits of enemy card index
- 6 bits: enemy card index

# Long Form (variant 0)

Byte 0: 10000000

Remaining bytes:

- repeated uint8: global card ID

# Long Form (variant 1)

Byte 0: 10000001

Remaining bytes:

- repeated varint: global card ID

# Long Form (variant 2)

Byte 0: 10000010

Remaining bytes:

- repeated varint: global card ID minus 128
