---
title: Data Formats (Tables Model)
...

Bug Tables uses Wavefront OBJ files to represent 3D models, with a simple extension that allows for objects to be reused:

```
i [name] [x] [y] [z] [rx] [ry] [rz] [sx] [sy] [sz]
```

There is also no `mtllib` statement as all materials are hard-coded. `s` and `l` commands are currently ignored.

The problem is: parsing text-based formats is slow. Using this format directly adds several seconds to the startup time of Spy Cards Online.

To work around this, we parse and preprocess the OBJ file and then write the processed data to a binary format. The binary files are embedded in the game executable, so only the latest version of the format needs to be supported.

# Data Format

- uvarint NameLength
- byte\[NameLength\] Name
- uvarint MaterialLength
- byte\[MaterialLength\] Material
- uvarint InstanceCount
- struct\[InstanceCount\] Instances
	- uvarint InstanceNameLength
	- byte\[InstanceNameLength\] InstanceName
	- float32\[3\] Position
	- float32\[3\] Rotation
	- float32\[3\] Scale
- struct\[...\] Buffer (remaining contents of file, count must be a multiple of 3)
	- float32\[3\] Position
	- uint16\[2\] TexCoord
	- float32\[3\] Normal
