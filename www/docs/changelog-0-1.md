---
title: Changelog (v0.1.x)
toc: false
...

*For newer changelog entries, see the [v0.2.x changelog](changelog-0-2.html).*

## v0.1.43

- Added a [simulator](../simulator.html) page
- Please forgive me for I have added `?npc=mender-spam`

## v0.1.42

- The `?npc` parameter has been expanded to support the following AI presets:
    - Any tournament player (eg. `?npc=bu-gi`, `?npc=kage`, `?npc=ritchee`)
    - Any card master (not listed here; only the fourth currently works as the rest have illegal decks)
    - Carmina's first two decks (`?npc=tutorial` and `?npc=carmina2` (currently does not work as she has an illegal deck))
    - Saved Decks mode (`?npc=saved-decks`)
    - Generic mode (`?npc`, or eg. `?npc=invalid-name`)

## v0.1.41

- Added VS HP condition.

## v0.1.40

- Added the ability to reorder cards and effects in the custom card editor.

## v0.1.39

- Added `?npc=saved-decks`, which uses a random saved deck if there is a saved deck available for picking rather than generate a deck from random cards.

## v0.1.38

- Added [Spoiler Guard](../spoiler-guard.html), which allows players to avoid seeing cards they would not see in Bug Fables during vanilla matches.

## v0.1.37

- Fixed multi-turn setup effects being saved as "override card text" for all turns after the first.

## v0.1.36

- Fixed desync caused by cards not being properly removed from opponent's hand when played.

## v0.1.35

- Fixed Unity and ATK (once) only applying once per card even if there were multiple effects of that type.
- Added an option to Setup to use the original card description.
- Audience size will slowly grow every rematch.
- Modified the game protocol to afford more secrecy to readied cards and remove the theoretical ability to predict coin flips.

## v0.1.34

- Setup cards now show the effect that was setup rather than the full description of the card.

## v0.1.33

- Fixed a crash when trying to summon a random card with a rank that does not exist in a specific tribe.

## v0.1.32

- Empower effects that apply to one card are now written as "Empower Card" rather than "Empower" to reduce confusion.

## v0.1.31

- Custom clients have moved in preparation for future infrastructure changes. Old addresses will redirect.

## v0.1.30

- Added an "invert" option to the coin flip condition, which visually swaps the heads and tails sides of the coin.

## v0.1.29

- Fixed a crash when a card with flavor text or a TP effect was summoned.

## v0.1.28

- Summon Random can now have a card type specified.

## v0.1.27

- Added Flavor Text effect type.

## v0.1.26

- Added Quick Join button.

## v0.1.25

- Fixed match verification failing on rematches.
- Opponent Left now overrides offered rematches in both directions on the end-of-match screen.
- Fixed HP indicators being missing on rematches.

## v0.1.24

- Fixed match recording corruption.
- Fixed a crash when a card had multi-turn setup.

## v0.1.23

- Fixed Multiply Healing not allowing 0 in the card editor.
- Added an Auto-Play checkbox to the match viewer.

## v0.1.22

- Card definitions are now local to each match rather than being global per browser tab. This only affects the random match viewer for now.
- Custom card editor now removes already-replaced cards from the base card selector.

## v0.1.21

- Added new effect types for custom cards:
    - TP (only allowed unconditionally or inside Setup)
    - Stat (after modifiers)
    - VS Stat
    - VS Stat (after modifiers)
    - If HP
    - Per Tribe
    - Per Rank
    - Multiply Healing
    - Summon as Opponent
- Improved consistency of tribe names in effect descriptions.
- Effects with negative amounts are now displayed with easier-to-understand phrasing.
- Setup cards now remain visible on the field between turns.

## v0.1.20

- Added a blue highlight on the current effect being processed.

## v0.1.19

- Match viewer:
    - Added HP indicators.
    - Fixed a race condition when changing the currently viewed round number very quickly.
    - Clicking the "next round" button now plays the current round's animations.
    - Added `?autoplay` mode.

## v0.1.18

- Split the Audio toggle into separate Music and Sound toggles. When both are disabled, the full-sized "Enable Audio" button will be used.

## v0.1.17

- Added support for `?no3d`, which disables all 3D backgrounds.

## v0.1.16

- Added audio.

## v0.1.15

- Fixed a crash on match end if using custom cards that replace vanilla cards.
- A preliminary version of the match viewer is now available.

## v0.1.14

- Fixed a crash on match end.

## v0.1.13

- Re-added match recording code at end of match.

## v0.1.12

- Fixed match not ending if one player dropped below 1 HP on a turn that resulted in a tie.

## v0.1.11

- Player characters:
    - Added Arie.
    - Added Bu-gi.
    - Added Jayde.
    - Added Johnny.
    - Added Kage.
    - Added Kenny.
    - Added Malbee.
    - Added Nero.
    - Added Pibu.
    - Added Richee.
    - Added Serene.
    - Added Shay.
    - Added Vanessa.

## v0.1.10

- Fixed If Card only being able to see 1 copy of the card.

## v0.1.9

- Fixed not being able to select custom decks.
- Midge now has 1 ATK rather than a very complicated effect equivalent to midges having 1 ATK.

## v0.1.8

- Fixed the Limit condition checking the limit for the current player rather than the player that played the card.

## v0.1.7

- Added coin flips and condition results to the game log.

## v0.1.6

- Fixed On Win, On Loss, On Tie, and Unless Tie activating on loss, on win, unless tie, and on tie, respectively.
- Added card pack client support to the deck editor.

## v0.1.5

- Modified the character selector to work better on Mobile Safari.
- Coin flip animation for heads-or-tails now only uses the ATK/DEF icon if the heads effect is an effect-atk and the tails effect is an effect-def.
- Fixed custom card lists not being properly removed in clients other than custom.html.

## v0.1.4

- Mobile devices will now display a reduced-detail version of the character selector.
- Added the ability to drag the character selector.
- Improved layout of pre-match menus.

## v0.1.3

- Modified character select room geometry to improve performance.
- Altered audience distribution to improve player visibility.
- Match codes for different client types are now visibly different.

## v0.1.2

- Fixed custom client not being able to play matches since the 3D update.

## v0.1.1

- Inverting the On Win condition now results in On Opponent Win rather than On Loss Or Tie.

## v0.1.0

- Spy Cards is now 3D.
- You can play as Tanjerin.
- No, it's July, not April.

## Older Entries

*For older changelog entries, see the [v0.0.x changelog](changelog-0-0.html).*
