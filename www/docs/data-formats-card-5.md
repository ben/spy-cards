---
title: Data Formats (Card v5)
...

(base64-encoded)

- uvarint FormatVersion = 5
- uvarint GlobalCardID
- string DisplayName
- svarint TPCost
- uint8 Portrait
    - Values 0..232 are built-in portraits.
    - Values 233..253 are reserved.
    - Values 247..253 are built-in portraits.
    - Value 254 is an embedded PNG.
    - Value 255 is a File ID.
- uint8 RankTribe
    - Most significant 4 bits are the rank of this card.
        - Value 0 is Attacker.
        - Value 1 is Effect.
        - Value 2 is Mini-Boss.
        - Value 3 is Boss.
        - Value 4 is Token.
        - Values 5..15 are reserved.
    - Least significant 4 bits are the card's first tribe.
        - Values 0..13 are a built-in tribe (see Appendix A).
        - Value 14 denotes a custom tribe.
        - Value 15 is reserved.
- [CustomTribe](#customtribe) CustomTribe0
    - This field is omitted if the first tribe is not custom.
- uvarint AdditionalTribeCount
- repeated ceil(AdditionalTribeCount/2) times:
    - uint8 PackedTribes
        - Both sets of 4 bits have the same meaning as the tribe half of RankTribe.
        - If AdditionalTribeCount is odd, the last entry's least significant 4 bits are set to 15.
    - [CustomTribe](#customtribe) CustomTribe1
        - This field is omitted if the first (most significant bits) tribe is not custom.
    - [CustomTribe](#customtribe) CustomTribe2
        - This field is omitted if the second (least significant bits) tribe is not custom.
- uvarint EffectCount
- repeated EffectCount times:
    - [EffectDef](#effectdef) Effect
- if Portrait is 254 or 255:
    - uvarint CustomPortraitLength
    - byte\[CustomPortraitLength\] CustomPortrait
- uvarint ExtensionFieldCount
- repeated ExtensionFieldCount times:
    - uvarint FieldType
    - uvarint FieldLength
    - [ExtensionField](#extensionfield) Field (FieldLength bytes)

# CustomTribe

- uint8\[3\] ColorRGB
- string Name

# CardFilter

*All Tag values not specified here are reserved.*

Specific card:

- uint8 Tag = 1
- uvarint CardID

Targeted card:

*Targeted card is the card this effect appears on, unless a parent effect specifies otherwise.*

- uint8 Tag = 65

Generic filter:

*All tribe filter fields must match to satisfy the card filter. Any rank and any TP filter field must match.*

- repeated arbitrary number of times:
    - uint8 Tag
        - 0001xxxx: built-in tribe (see Appendix A)
        - 00011110: custom tribe
        - 00011111: reserved
        - 0010xxxx: not built-in tribe (see Appendix A)
        - 00101110: not custom tribe
        - 00101111: reserved
        - 00110000: attacker
        - 00110001: effect
        - 00110010: mini-boss
        - 00110011: boss
        - 00110100: token
        - 01000000: TP
        - 01000001: (used for Target)
    - string CustomTribe
        - This field is omitted if Tag is not one of the two custom tribe tags.
    - svarint TP
        - This field is omitted if Tag is not TP.
- uint8 EndTag = 0

# EffectDef

*Root effects cannot be results, and must have priority 0. Passive effects must results in locations where it is specified that they are allowed, and must have priority 0.*

- uint8 EffectID
- uint8 Priority
    - Value 0 is immediate - no sound or visual effects are played, and the effect resolves as soon as possible.
    - Values 1..63 occur before stats are displayed on the screen during a round.
    - Values 64..223 occur during the main part of the round.
    - Values 224..254 occur after the winner of the round has been determined.
    - Value 255 is reserved.
- uvarint Flags (defined per effect)
- ... Data (defined per effect)

*All effect IDs not described here are reserved.*

## 0: Flavor Text

*Has no effect during a match.*

Root.

Flags:

- 0: hide remaining effects on this card's description
- 1: use a custom color

Data:

- if flag 1 is set:
    - uint8 Red
    - uint8 Green
    - uint8 Blue
- string Text

## 1: Card Stat

*Modify this card's stats by the given amount. Cards start with 0 ATK and 0 DEF.*

Flags:

- 0: modify DEF. if this flag is not set, modify ATK.
- 1: multiply Amount by infinity. Amount must be 1 or -1.

Data:

- svarint Amount

## 2: Empower

*Modify target cards' stats by the given amount.*

Flags:

- 0: modify DEF. if this flag is not set, modify ATK.
- 1: multiply Amount by infinity. Amount must be 1 or -1.
- 2: affect opponent. if this flag is not set, affect the current player.

Data:

- svarint Amount
- [CardFilter](#cardfilter) Filter

## 3: Summon

*Add a card to the field. Any effects that should have already been resolved in priority order are resolved at this time.*

Flags:

- 0: summon card for opponent. if this flag is not set, summon card for current player.
- 1: hide summoning card. this flag being set causes the replacement summon animation to play.
- 2: hide summoned card. this flag being set causes the summoned card to be invisible.

Data:

- uvarint Count
- [CardFilter](#cardfilter) Filter

## 4: Heal

*Modify target player's health by the given amount. Health may go below 0, but cannot go above the maximum health for the mode.*

Flags:

- 0: multiply Amount by infinity. Amount must be 1 or -1.
- 1: heal the opponent. if this flag is not set, heal the current player.
- 2: ignore healing multipliers.

Data:

- svarint Amount

## 5: TP

*Modify available TP for this turn.*

Passive.

Flags:

- 0: multiply Amount by infinity. Amount must be 1 or -1.

Data:

- svarint Amount

## 6: Numb

*Flip a target card face-down and remove its stat contribution. This also prevents all future effects from running on the target card except for Numb and On Numb.*

*Cards are targeted as follows: The higher of the card's ATK or DEF stat is selected. The card with the overall lowest stat is numbed. ATK is considered to be higher than DEF if they are numerically equal.*

Flags:

- 0: multiply Count by infinity. Count must be 1.
- 1: target current player's cards. if this flag is not set, target opponent's cards.
- 2: select the card with the highest stats when deciding which to numb.
- 3: ignore ATK when selecting a card to numb.
- 4: ignore DEF when selecting a card to numb.
- 5: target cards with 0 for both stats last.
- 6: add an effect for each card numbed.
- 7: add an effect for each numb instance that could not find a target. for infinite numbs, the effect runs if no targets were numbed.
- 8: summon numbed card to opposite side of field.
- 9: ignore prevent numb when targeting cards.

Data:

- uvarint Count
- [CardFilter](#cardfilter) Filter
- [EffectDef](#effectdef) SuccessEffect
    - If flag 6 is not set, this field is omitted.
- [EffectDef](#effectdef) FailureEffect
    - If flag 7 is not set, this field is omitted.

## 7: Raw Stat

*Modify the player's stats by the given amount. Numbing cards will not reverse these changes.*

Flags:

- 0: modify DEF. if this flag is not set, modify ATK.
- 1: multiply Amount by infinity. Amount must be 1 or -1.
- 2: affect opponent. if this flag is not set, affect the current player.

Data:

- svarint Amount

## 8: Multiply Healing

Flags:

- 0: affect healing for the opponent. if this flag is not set, affect healing for the current player.
- 1: only affect future healing. if this flag is not set, heals are multiplied retroactively.
- 2..3:
    - 0: modify all heal effects.
    - 1: only modify heals that start as negative.
    - 2: only modify heals that start as positive.
    - 3: (reserved)

Data:

- svarint Multiplier

## 9: Prevent Numb

*Targeted cards are given immunity to the next numb that could target them. If a card has this immunity, it is skipped when selecting a card to numb, and the immunity is removed.*

Flags:

- 0: prevent numbs for the opponent's cards. if this flag is not set, prevent numbs for the current player's cards.

Data:

- uvarint Count
- [CardFilter](#cardfilter) Filter

## 10: Modify Available Cards

Flags:

- 0: modify opponent's cards. if this flag is not set, modify current player's cards.
- 1..2:
    - 0: add card.
    - 1: remove card.
    - 2: move card to field.
    - 3: replace matching cards.
- 3..4:
    - 0: target any location. if adding a card, add to the hand but do not shuffle back into the deck.
    - 1: target hand only.
    - 2: target deck only.
    - 3: target field only. invalid with move card to field.
- 5..6:
    - 0: don't require an existing card
    - 1: require existing card on own field
    - 2: require existing card on opponent's field
    - 3: (reserved)
- 7: on success effect
- 8: on failure effect
- 9: has group name

Data:

- uvarint Count
- if flag 1..2 is 3:
    - uvarint ReplacementCardID
    - (additionally, flag 5..6 must be 0)
- [CardFilter](#cardfilter) Filter
- if flag 9 is set:
    - string Group
- if flag 7 is set:
    - [EffectDef](#effectdef) SuccessResult
- if flag 8 is set:
    - [EffectDef](#effectdef) FailureResult

## 11: Modify Card Cost

Passive.

Flags:

- 0: multiply Amount by infinity. Amount must be 1 or -1.
- 1: set the card's cost. If this flag is not set, the change in cost is relative.

Data:

- svarint Amount
- [CardFilter](#cardfilter) Filter

## 12: Draw Card

Flags:

- 0: opponent draws cards. if this flag is not set, the current player draws cards.
- 1: ignore maximum hand size
- 2: use filter
- 3: add an effect for each card drawn or discarded
- 4: add an effect for each card NOT drawn or discarded

Data:

- svarint Count
    - If Count is negative, move cards from the hand to the deck instead.
- if flag 2 is set:
    - [CardFilter](#cardfilter) Filter
- if flag 3 is set:
    - [EffectDef](#effectdef) SuccessEffect (targeted card is the card drawn or discarded)
- if flag 4 is set:
    - [EffectDef](#effectdef) FailEffect (targeted card is inherited from this effect)

## 13: Modify Game Rule

Flags:

- 0: relative change. if this flag is not set, the change is absolute.
- 1..2:
    - 0: affect current player's game rules.
    - 1: affect opponent's game rules.
    - 2: affect game rules for both players.
    - 3: (reserved)

Data:

- uvarint RuleID
    - this cannot be 0 (None)
    - changing rules that affect deck creation has no effect as the deck is already built
    - see [GameRules field documentation](data-formats-card-3.html#gamerules) for a list of rule IDs
- svarint Value

## 14: Delay Setup

Flags:

- 0: modify setup effects for the opponent. if this flag is not set, modify the current player's setup effects.
- 1: delete setup effects that reach 0 rounds. if this flag is not set, apply setup effects that reach 0 rounds.

Data:

- svarint Amount
- [CardFilter](#cardfilter) Filter

## 128: Card (Condition)

Flags:

- 0: check the opponent's cards. if this flag is not set, check the current player's cards.
- 1..2:
    - 0: apply the effect if at least Count cards match.
    - 1: apply the effect if fewer than Count cards match.
    - 2: apply the effect multiple times if a multiple of Count cards match.
    - 3: (reserved)
- 3: allow numbed cards to be counted for this condition
- 4..5:
    - 0: search for cards on field
    - 1: search for cards in hand
    - 2: search for cards in deck
    - 3: (reserved)

Data:

- uvarint Count
- [CardFilter](#cardfilter) Filter
- [EffectDef](#effectdef) Result (targeted card is an arbitrary matching card, unless flag 1..2 is 1, in which case the targeted card is unchanged)

## 129: Limit (Condition)

*As a special case, a single layer of Limit maintains the Passives Allowed status of an effect result slot.*

Flags:

- 0: run the effect if this condition has been checked more than Count times. if this flag is not set, run the effect if this condition has been checked less than or equal to Count times.

Data:

- uvarint Count
- [EffectDef](#effectdef) Result

## 130: Winner (Condition)

Priority must be at least 224.

Flags:

- 0..2:
    - 0: run the effect if the current player wins the round.
    - 1: run the effect if the opponent wins the round.
    - 2: run the effect if the round is a tie.
    - 3: run the effect if any player wins the round.
    - 4: run the effect if the current player does not win the round.
    - 5: run the effect if the opponent does not win the round.
    - 6: (reserved)
    - 7: (reserved)

Data:

- [EffectDef](#effectdef) Result

## 131: Apply (Condition)

Flags:

- 0: run the effect in the following round. if this flag is not set, run the effect immediately.
- 1: run the effect for the opponent. if this flag is not set, run the effect for the current player. effects that act on the current card may ignore this flag.
- 2..3: 
    - 0: display only the effect text on the ghost card.
    - 1: display the full text of the current card on the ghost card.
    - 2: do not create a ghost card.
    - 3: (reserved)

Data:

- [EffectDef](#effectdef) Result (passives allowed)

## 132: Coin (Condition)

Flags:

- 0: this coin has an effect on tails.
- 1: visually swap the sides of the coin.
- 2: display ATK/DEF rather than happy/sad sides on the coin.

Data:

- uvarint Count
- if flag 2 is set, chance of heads is (WeightHeads + 1) / (WeightHeads + WeightTails + 1)
    - uvarint WeightHeads
    - uvarint WeightTails
- [EffectDef](#effectdef) HeadsResult
- [EffectDef](#effectdef) TailsResult
    - If flag 0 is not set, this field is omitted.

## 133: Stat (Condition)

Flags:

- 0: check the stat for the opponent. if this flag is not set, check the stat for the current player.
- 1: run the effect if the stat is less than the Amount. if this flag is not set, run the effect if the stat is greater than or equal to the Amount.
- 2..3:
    - 0: check ATK.
    - 1: check DEF.
    - 2: check HP.
    - 3: check TP.
- 4: run this effect multiple times if the stat satisfies a multiple of Amount.
- 5..6:
    - 0: no math
    - 1: add another stat
    - 2: subtract another stat
    - 3: (reserved)
- 7: add or subtract a stat from the opponent. if this flag is not set, add or subtract a stat from the current player.
- 8..9: 
    - 0: add or subtract ATK.
    - 1: add or subtract DEF.
    - 2: add or subtract HP.
    - 3: add or subtract TP.

Data:

- svarint Amount
- [EffectDef](#effectdef) Result

## 134: In Hand (Condition)

Root.

Flags:

- 0: do not reveal the card to the opponent. the card is still revealed to the opponent's computer.
- 1..2:
    - 0: the effect only applies if the card is not played
    - 1: the effect applies regardless of whether the card is played
    - 2: the effect only applies if the card is played
    - 3: (reserved)

Data:

- uvarint MinRounds
- uvarint MaxRounds
    - If MaxRounds is 0, it is treated as infinity.
- [EffectDef](#effectdef) Result (passives allowed)

## 135: Last Effect (Condition)

*All remaining effects on this card are removed after the result effect is processed. Conditions cannot be the result of this effect.*

Flags:

- *(no flags are currently defined)*

Data:

- [EffectDef](#effectdef) Result
    - The priority of this EffectDef must be 0 or 1.

## 136: On Numb (Condition)

Root.

Flags:

- *(no flags are currently defined)*

Data:

- [EffectDef](#effectdef) Result (targeted card is the numbing card)
    - The priority of this EffectDef must be 0 or 1.

## 137: Multiple Effects (Condition)

Flags:

- *(no flags are currently defined)*

Data:

- [EffectDef](#effectdef) Result
- [EffectDef](#effectdef) Result2

## 138: On Discard (Condition)

Root.

Flags:

- *(no flags are currently defined)*

Data:

- [EffectDef](#effectdef) Result (targeted card is the card with the Draw Card effect)

## 139: Compare Target Card (Condition)

Flags:

- 0: run the result if the filter does not match. if this flag is not set, run the result if the filter matches.

Data:

- [CardFilter](#cardfilter) Filter
- [EffectDef](#effectdef) Result

## 140: On Exile (Condition)

Root.

Flags:

- 0: filter by exile group.

Data:

- if flag 0 is set:
    - string Group
- [EffectDef](#effectdef) Result

# ExtensionField

*All type IDs not described here are reserved.*

## 0: SetStageIPFS (deprecated)

- uvarint Flags
    - bit 0: override stage even if a custom stage is already active
- uvarint CIDLength
- byte\[CIDLength\] FileCID
    - See IPFS for a description of CIDs.
    - File format: [Spy Cards Online Stage](data-formats-stage.html)

## 1: SetMusic

- uvarint Flags
    - bit 0: override music even if custom music is already playing
- uvarint CIDLength
- byte\[CIDLength\] FileCID
    - See IPFS for a description of CIDs.
    - File format: ogg/opus
- float32 LoopStart
- float32 LoopEnd

## 2: PlaySound

*Sound will be played when the card's first queued effect runs with nonzero priority.*

- uvarint Flags
    - (no flags defined)
- uvarint CIDLength
- byte\[CIDLength\] FileCID
    - See IPFS for a description of CIDs.
    - File format: ogg/opus

## 3: SetStage

- uvarint Flags
    - bit 0: override stage even if a custom stage is already active
- uvarint NameLength
- byte\[NameLength\] Name
    - Name must not contain any characters that are URL-encoded.
    - Stage is loaded from game assets: `img/room/[Name].stage`
    - File format: [Spy Cards Online Stage](data-formats-stage.html)
