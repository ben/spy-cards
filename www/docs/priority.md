---
title: Explanation of Effect Priority
...

There are 256 effect priorities. They are evaluated from lowest to highest (that is, an effect with priority 1 will generally run before an effect with priority 2).

Some actions take place automatically when certain effect priorities are reached. Any effect with a priority that is at least one of these values will run after the specified action has taken place:

- 1: animations and sounds are enabled. that is, effects with priority 0 will not have an animation or sound effect.
- 64: the ATK and DEF icons are displayed.
- 224: the winner of the round is determined.
- 255: end of round. no effects can run after this, so no effects can have priority 255.

## Default Priorities

The default priorities for effects are:

- 1: Prevent Numb
- 5: Modify Game Rule
- 5: Apply (Setup)
- 10: Limit (Only Once, Except The First)
- 25: Summon (Summon, Turns Into)
- 35: If Card (If, VS, Per)
- 40: Heal (Heal, Take Damage)
- 40: Multiply Healing
- 45: Draw (Draw, Discard)
- 50: Modify Available Cards (Add, Clone, Exile, Play)
- 55: Delay Setup
- 65: Stat (ATK, DEF)
- 80: Empower (Empower, Fortify, Enfeeble, Break)
- 85: If Stat (If ATK, If DEF, If HP, If TP)
- 90: Numb (Numb, Steal)
- 95: Raw Stat (Pierce)
- 225: Winner (On Win, On Lose, On Tie, Lifesteal)
- 254: Passive

## Effect Queue

During the effect processing phase of a round, effects run in an order determined partially by the priority of each effect and partially by other factors based on how and when the effect was queued.

The initial effect queue is sorted by the following factors, with the first factor sorting the entire queue, and each factor beyond that sorting the groups into smaller groups:

1. Effect priority
2. Player number
3. Card position on field (for the left side of the field, this is left-to-right, top-to-bottom, and for the right side of the field, this is right-to-left, top-to-bottom.)
4. Position on the card (from top to bottom of the effect list)

During processing, some effects add their results to the queue. Assuming the group we just sorted is group 0, and group -1 happens before group 0, their position is determined by the following steps:

1. If the effect is On Numb, the result is queued at the end of group -2.
2. If the effect is not a summon and the result is at a priority that has already been reached (including the current priority), the result is queued at the end of group -1.
3. The effect is queued after all effects for its priority and player in group 0.
