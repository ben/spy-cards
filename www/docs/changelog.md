---
title: Changelog (v0.3.x)
toc: false
...

*For future development goals, see the [roadmap](roadmap.html) and [known issue list](known-issues.html).*

## v0.3.27

- **Card Editor:** Added On Fail to Draw Card.
- **Card Editor:** Fixed banned card checkboxes sometimes staying unchecked when they should be checked.
- **Spy Cards:** Added 1.2.1 vanilla (fixed mender!)

## v0.3.26

- **Card Editor:** Added a relative/absolute toggle to Modify Card Cost.
- **Card Editor:** Added On Exile condition.

## v0.3.25

- **Card Editor:** Added a special flag to hide the game log.
- **Card Editor:** Setup now keeps the target card.

## v0.3.24

- **Card Editor:** Checking or unchecking the "override music" box now correctly enables the Save button.
- **Card Editor:** Added a special flag to auto-ready if a player starts a round with less than 0 TP available.
- **Spy Cards:** Fixed In Hand -> Limit not correctly applying.

## v0.3.23

- **Aphid Festival:** Fixed a crash related to screen reader handling of rare aphids.
- **Spy Cards:** Fixed a crash in match verification.
- **Spy Cards:** Fixed a crash related to clicking on a certain part of the home screen on game modes with no variants.
- **Spy Cards:** Fixed the card viewer being misaligned for one frame when loading the home screen.
- **Spy Cards:** Fixed screen flickering when cheap card rendering updates its texture atlas.
- **Spy Cards:** Fixed a case where cheap card rendering would not render anything.
- **Spy Cards:** Fixed graphical oddities in several stages on low graphics quality.
- **Match Viewer:** Added a loading screen for when a recording takes more than 1 frame to load.
- **Performance:** Added a graphics quality selector to the settings menu.

## v0.3.22

- **Card Editor:** Added the Compare Target Card condition.
- **Card Editor:** In Hand now defaults to keeping the card face-down to avoid confusion.
- **Card Editor:** Added Max Cards Per Round game rule.
- **Performance:** Several behind-the-scenes optimizations.

## v0.3.21

- **Card Editor:** Added the Target Card filter target.
- **Card Editor:** Added True Numb.
- **Spy Cards:** Numbing an invisible card no longer makes a sound.
- **Spy Cards:** Fixed not being able to end turn if TP was finite but less than 0.
- **Spy Cards:** Greatly improved the keyboard accessibility of the game mode home screen.
- **Spy Cards:** Fixed the Card condition's game summary log message showing card IDs rather than a number.
- **Performance:** Spy Cards Online can now continue processing while the tab is hidden.

## v0.3.20

- **Card Editor:** Added the current card code to the card editor for convenience.
- **Card Editor:** Added an On Discard effect.
- **Card Editor:** The Draw Card effect can now have a result for each card drawn or discarded.
- **Card Editor:** In Hand &rsquo; Limit &rsquo; TP, In Hand &rsquo; Limit &rsquo; Modify Card Cost, Setup &rsquo; Limit &rsquo; TP, and Setup &rsquo; Limit &rsquo; Modify Card Cost are now permitted.
- **Card Editor:** Added a "target card" filter, which allows effects to use the matched card from conditions like If Card or the numbing card from On Numb.
- **Spy Cards:** You can now undo ending your turn.

## v0.3.19

- **Card Editor:** Checkboxes in the Restrictions section of the card editor have more reasonable hitboxes.
- **Spy Cards:** Added an alternate card color scheme, available in settings.
- **Spy Cards:** Greatly improved the clarity of text rendering.
- **Spy Cards:** The "Speed Up" text can now be clicked for the same effect as pressing the indicated button.
- **Aphid Festival:** Added experimental screen reader support.
- **Spoiler Guard:** Updated deck importer to handle Team Slacker.
- **Termacade:** Made some optimizations to the CRT effect. Disabling the CRT effect now only disables the sub-pixel coloring, not the screen shaping.

## v0.3.18

- **Card Editor:** Added No Damage on Win special flag.
- **Card Editor:** Added Reveal Card Selection special flag.
- **Card Editor:** Added Both Players are Player 1 special flag. (This affects HP and fields, but not TP or hands.)
- **Spy Cards:** Removed the animation for numbs that hit 0 targets.
- **Spy Cards:** Multi-round Setup (Original Text) effects no longer have an animation after the first round.
- **Spy Cards:** Added a workaround for looking at another tab causing the game to freeze.
- **Spy Cards:** Replaced the Join button on the home screen with a new button that allows you to watch random recordings of a game mode.
- **Spy Cards:** Added Doppel's Underground Tavern stage.

## v0.3.17

- **Fighters:** Added health indicators.
- **Card Editor:** Added the ability to add or subtract stats in the Stat condition.
- **Card Editor:** Hidden tribes are now displayed verbatim in effect descriptions.
- **Spy Cards:** Fixed modified game rules not being reset before verifying a match.
- **Spy Cards:** Fixed On Numb effects having no animation.
- **Spy Cards:** Added a Golden Hills stage.
- **Spy Cards:** Added a Forsaken Lands stage.
- **Spy Cards:** Singleplayer no longer establishes a network connection to the NPC running in the same tab; instead, the data is directly exchanged between the two game instances.

## v0.3.16

- **Card Editor:** Tweaked the way that infinity symbols, Greek letters, and simple card descriptions render to be a closer match to the game.
- **Card Editor:** Added `=` to the set of characters that can be rendered in a card description.
- **Card Editor:** Added Pay effect (If Card A (X): Numb Own Card A (X) and Y). The Numb must be an earlier priority than the If Card.
- **Card Editor:** Added Freeze effect (Numb Card A (X) / If Numb: Summon Card B (1) as Opponent). Card B's name must be the same as card A's name with the prefix "Frozen" added. This also works for numbing your own cards with a non-opponent summon.
- **Card Editor:** Added Replace as a possible action for Modify Available Cards.
- **Spy Cards:** Unpickable cards no longer show the blocked TP icon. The icon is now only displayed on cards that cost infinity TP (through a Modify Card Cost effect).
- **Spy Cards:** Spy Cards will now try to work around Safari's lack of support for Opus and WebP.
- **Spy Cards:** Fixed cards beyond the mode-defined max hand size appearing offscreen.
- **Spy Cards:** Stackable tokens on the home screen now display with x1.
- **Performance:** Hopefully reduced overall memory usage.

## v0.3.15

- **Card Editor:** Added Unless Win and Unless Lose conditions.
- **Card Editor:** Added weighted coin flips. Uh oh!
- **Card Editor:** Added custom flavor text colors. UH OH!
- **Spy Cards:** Loading a custom mode (that is, a mode that is not uploaded to the server) will now attempt to build a sprite sheet for portraits client-side.
- **Spy Cards:** Fixed some card corruption bugs that would happen when merging multiple custom card sets.
- **Spy Cards:** Card rendering has been rewritten to be significantly more efficient in most cases.

## v0.3.14

- **Card Editor:** The card editor now displays a warning if you use a letter not supported by the Spy Cards font rendering system in a text field.
- **Spy Cards:** Fixed flickering or black screens when waiting for networked data from the opponent.
- **Spy Cards:** Connection establishment is now more robust and will succeed in more situations.
- **Spy Cards:** The game client will now automatically re-establish its match code if the connection is lost, for example if the person joining the match quickly closes the tab.
- **Spy Cards:** The round trip time to your opponent is now displayed in the upper right.
- **Spy Cards:** Fixed the game log viewer being much more computationally expensive than necessary.
- **Spy Cards:** Fixed hovering over cards on the field sometimes not causing them to zoom in.
- **Spy Cards:** Fixed coins not appearing on stacked tokens.
- **Spy Cards:** Added a confirmation if you try to close the tab during a match.

## v0.3.13

- **Card Editor:** The card editor now shows a preview of the card being edited.
- **Card Editor:** Portraits in BMP format will now be automatically converted to PNG rather than rejected.
- **Card Editor:** Added game rules for maximum number of duplicate cards of each rank in a deck.
- **Card Editor:** Delay Setup now has an option to destroy setups that reach 0 rounds rather than applying them.
- **Card Editor:** Added the ability to filter by the base TP of a card.
- **Spy Cards:** Fixed cards hidden on the home screen also being considered banned.
- **Spy Cards:** Fixed a desync in the Draw Card effect.

## v0.3.12

- **Spy Cards:** Fixed variants being reapplied on rematch.
- **Spy Cards:** Fixed Prevent Numb always applying 1 instance of the effect regardless of the stated amount.
- **Card Editor:** In Hand effects that are displayed to the opponent but do nothing now show "Reveal" in the card description.
- **Card Editor:** "Attacker or Effect or Mini-Boss or Boss" is now shortened to "Non-Token".
- **Card Editor:** "Exile From Field (1): (this card)" is now shortened to "Exile Self"
- **Card Editor:** Added "On Play" as an option for In Hand effects.
- **Card Editor:** Nested If Card effects now use conjunctions ("If Zasp: If Mothiva: foo" now displays as "If Zasp and Mothiva: foo")
- **Card Editor:** Negative lifesteal now displays as "On Win: Take X Damage"
- **Performance:** Text rendering now uses signed distance fields rather than alpha masking. This saves a significant amount of video memory as the font sprite sheet is now much smaller.
- **Performance:** Textures are now resized based on your GPU's reported maximum texture size. 16k and above (high end) has no change. 8k (medium end) will cut the dimensions of 8 megapixel and 16 megapixel textures in half. Low-end graphics cards limit each texture to 1 megapixel.

## v0.3.11

- **Card Editor:** Added a new "Special Flags" field. Two flags defined in this version are Allow Subtracting Infinities and Use Effect Card Back.
- **Spy Cards:** Added a sound effect for the Draw Card effect.
- **Spy Cards:** Added a "complete my deck" button to the deck builder, which fills the empty slots in your deck with cards which hopefully synergize at least a little.
- **Spy Cards:** Added more complex border designs to cards to differentiate the different ranks in a way other than by color.
- **Spy Cards:** Fixed several crashes, especially in Safari.

## v0.3.10

- **Spy Cards:** Fixed setup cards from the previous match showing up before the first round of a rematch.
- **Spy Cards:** Added a game log viewer ingame, which can be found by hovering in the top left corner of the screen. Viewing the game log will pause the game.
- **Spy Cards:** Added an icon for Prevent Numb.
- **Performance:** All portraits for a game mode are now loaded as a single sprite sheet.
- **Performance:** Frame rendering no longer waits for audio to load.
- **Performance:** Modified sprite batch system to support more than 8k sprites per batch. Stages can now contain more than 2^16 vertices.
- **UI:** Clicking on things on touchscreens should be more consistent now.
- **UI:** Implemented new logic for screen size handling. To go back to the old version, uncheck "Use Standard Size" in settings.

## v0.3.9

- **Spy Cards:** Updated for Bug Fables 1.1.1.

## v0.3.8

- **Spy Cards:** Tokens that are summoned directly, have 0 TP cost, and are not banned now visually stack.
- **Spy Cards:** Game modes with no cards that have stats (card stat, empower, etc.) no longer show the stat counter during a turn.
- **Spy Cards:** Fixed a crash in the Draw Cards effect.
- **Spy Cards:** Fixed In Hand effects other than TP and Modify Card Cost failing to apply. There is now an animation for In Hand Summon.
- **Card Editor:** Fixed a crash when dragging a group onto another group.
- **Card Editor:** Added a "hidden" mode to Setup.
- **Card Editor:** Added a "hide on home screen" ban state for cards.
- **Card Editor:** Modify Card Cost can now specify infinity.

## v0.3.7

- **Spy Cards:** Fixed missing HP animations for damage.
- **Spy Cards:** Fixed the HP indicator updating too soon if the other player lifesteals.
- **Spy Cards:** Fixed the sound effect for losing a turn being too quiet.
- **Spy Cards:** Multiple consecutive copies of the same coin or summon effect are now combined for faster animations.
- **Spy Cards:** Stolen cards are now immune to numbs to avoid a soft lock when a card steals a copy of itself.
- **Spy Cards:** Setup cards now remain visible on the field during round transitions.
- **Spy Cards:** Various performance optimizations, especially at game startup.
- **Card Editor:** Prevented conditions from being used as the result of On Numb or Last Effect.
- **Card Editor:** Added an "Exile Group" option to Modify Available Cards. A matching group name must be given to Return From Exile to match those cards.
- **Card Editor:** Added Delay Setup, which allows active Setup effects to have their turn count modified. Setups that reach 0 turns are applied and no longer considered to be setups.
- **Deck Editor:** Order of cards now matches the order shown on the home screen and in the card editor.

## v0.3.6

- **Spy Cards:** Fixed text and cards appearing very small on screens with high pixel densities.
- **Spy Cards:** Fixed extension fields (music and custom stages) not being applied for turn 0 effects.
- **Spy Cards:** Added a deck editor link to the home screen.
- **Spy Cards:** Various performance optimizations.
- **Card Editor:** Added a "card group" field for easier card management within the editor.
- **Card Editor:** Replaced the Modify Available Cards flag fields with a dropdown menu, which should be easier to find what you want in.
- **Card Editor:** Duplicate Card now places the new card directly after the existing card, and also copies vanilla card names.
- **Deck Editor:** Added a delete button for saved decks.
- **Settings:** Added an option to disable passive animations like the camera panning and audience hopping. This option is forced if your browser sends the `prefers-reduced-motion` signal.
- **Settings:** Added options for changing render sampling, trading quality for speed or vice versa.
- **Settings:** Added an option to override the operating system setting for color scheme preference.

## v0.3.5

- **Spy Cards:** Fixed custom music and stages not being applied for cards summoned via an effect.
- **Spy Cards:** Fixed game rules not being reset to the defaults on rematch.
- **Spy Cards:** Added a Speed Up button in the effect resolution stage of a round.
- **Spy Cards:** Fixed a crash caused by using a certain menu code on 1.05 vanilla.
- **Deck Builder:** Fixed being able to select a saved deck that would be disallowed by deck limit filters.
- **Room:** Made some subtle changes to audience behavior.
- **Spoiler Guard:** Menu codes can now be disabled by typing them backwards. Good luck!
- **Spoiler Guard:** Added indicators for active menu codes to the home screen.
- **Networking:** Fixed some race conditions in the matchmaking handshake algorithm.
- **Performance:** Introduced a cheaper rendering path for large quantities of the same card.
- **Performance:** Reduced the initial download size of Spy Cards Online.
- **Performance:** Fixed a case where the card editor would take an extremely long time to load.

## v0.3.4

- **Card Editor:** Deck limits can now be conditional. For example, "if the deck contains Devourer, a maximum of 5 bugs can be in the deck".
- **Card Editor:** The Draw Card effect can have a filter to force a player to draw or discard specific cards.
- **Card Editor:** The If Card condition can look for cards in a player's hand or deck instead of cards on the field.
- **Card Editor:** Added On Success and On Failure results to Modify Available Cards.
- **Card Editor:** Added an option for Numb to summon a copy of the numbed card to the opposite side of the field.
- **Card Editor:** Added Modify Game Rule effect.
- **Spy Cards:** Fixed a false positive error in match validation caused by improper RNG seeding.
- **Spy Cards:** Fixed a bug where the previous audience would not leave during a rematch.
- **Spy Cards:** Greatly improved first-run performance.

## v0.3.3

- **Spy Cards:** Fixed In Hand condition not functioning for TP and Modify Card Cost effects.
- **Spy Cards:** If Stat now correctly determines the remaining TP for a player.
- **Card Editor:** Completely rebuilt the card editor.
- **Card Editor:** Moved "upload custom portrait" to the start of the portrait list.
- **Deck Editor:** It is now possible to scroll when not hovering over cards.
- **Settings:** The settings page can now load more reliably on low-powered devices.

## v0.3.2

- **Card Editor:** Fixed crashes when trying to autosave several incomplete effects.
- **Card Editor:** Fixed vanilla cards losing their edits when re-entering the editor.
- **Card Editor:** Added missing Filter field to Empower effect.
- **Card Editor:** Added missing fields to Stat (Condition) effect.
- **Card Editor:** Fixed variant editor using the base game mode's rule list.
- **Card Editor:** Fixed cancelling the custom tribe editor deleting a tribe.
- **Spy Cards:** Fixed NPC opponents failing to send required data for the Modify Available Cards effect.
- **Spy Cards:** Fixed some edge conditions for In Hand (Condition) effect.
- **Spy Cards:** Fixed being able to select saved decks with unused custom card IDs for the current mode.
- **Spy Cards:** Fixed a crash when accepting a rematch.
- **Spy Cards:** Fixed a crash related to exiling multiple cards from the hand.
- **Settings:** Fixed percentages (used for audio levels) dividing by 10 when increased or decreased.

## v0.3.1

- **Mite Knight:** Moved fireball smoke to a visible location to match changes made in Bug Fables 1.1.
- **Spy Cards:** Added changes from Bug Fables 1.1:
    - Wild Chomper TP cost from 4 to 3.
    - Plumpling TP cost from 6 to 4.
    - Midge is now a 1-attacker.
    - Added Stratos.
    - Added Delilah.
    - Added prebuilt decks to applicable NPCs.
    - Card order has been updated to match the ingame order.
- **Spy Cards:** Cards with ATK, DEF, or ATK and DEF are displayed as with simple descriptions even if they are not of the Attacker rank.
- **Spy Cards:** Updated Skirby to match his new appearence in Bug Fables 1.1.
- **Spy Cards:** Added a game mode selector.
- **Card Editor:** Card format is now v5. The editor has been updated, and existing custom cards will be automatically converted. There are many changes in this version. Most notably:
    - It is now possible to change the rank of a card.
    - The priority of each effect can now be modified directly.
    - Several new effects have been added:
        - Prevent Numb shields against a Numb effect.
        - Modify Available Cards allows cards to be added or removed from either player's hand, deck, or field.
        - Modify Card Cost changes the TP cost of cards passively.
        - Draw Card forces a player to draw or discard cards.
        - In Hand allows effects to be run for a card that is in a player's hand.
        - Last Effect prevents a card from running any additional effects.
        - Multiple Effects allows multiple effects to be run as the result of a single condition.
    - Several existing effects have received new options:
        - Numb's targeting now has several methods of tweaking it.
        - Numb can now run an effect if it succeeds or fails.
        - If Stat can now check HP or TP in addition to ATK or DEF.
    - Filters are now much more powerful.
    - Cards can now change the music or stage when they are played.

## Older Entries

*For older changelog entries, see the [v0.2.x changelog](changelog-0-2.html).*
