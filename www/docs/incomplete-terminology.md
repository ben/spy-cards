---
title: Terminology
...

- **Vanilla:** Something that matches the behavior of the Spy Cards minigame in Bug Fables: The Everlasting Sapling. There are a few non-Vanilla behaviors in Spy Cards Online. Not including custom cards or non-Vanilla effects (only used on custom cards), the differences are:
    - Numb targets cards based on stats rather than the order they were played. This was done to remove unnecessary complexity when choosing cards. Rather than force the player to remember to always play the lowest ATK cards first, the Numb effect simply targets them before higher-stat cards. This also means that summoning a low-ATK card can protect a higher-ATK card that was manually played from Numb.
    - The stats from Empower are added to the targeted cards rather than to the player. This was done both to allow counter-play against empower and because it just kinda made sense that numbing an empowered creature should get rid of the empowerment.
    - Tribes are visible on cards. This takes out a lot of the guesswork caused by the manually-written card descriptions in Bug Fables. No more "why does B-33 empower wardens" or "is the wasp king a wasp". Now you just know because it's written right there.
- **Round** or **Turn:** Used interchangeably. The base unit of play-time in Spy Cards Online. Rounds are sequentially numbered, starting with 1. During each round, both players select cards from their hands to play, which are placed on the corresponding sides of the field. The effects of each card are then processed.
- **Hand:** The cards a player has available to play. Each turn starts with drawing a number of cards, so that a player has between a minimum and maximum number of cards to choose from.
- **Deck:** The cards owned by a player that are not in their hand or discard pile. (The cards that appear on the field can be thought of as copies of the cards in the discard pile.) Shuffled every turn before drawing cards.
- **Discard:** Played cards go here after the turn they are played. An additional turn later, they are returned to the deck and can be drawn.
- **Field:** 
- **Setup:** An effect that will occur in a later turn.

# Cards

- **Card:** 
- **Rank:** 
- **Face:** The side of the card with the writing. You probably already figured this out.
- **Back:** The non-face side of the card. Cards have a back design based on their rank, with Attacker and Effect cards sharing a green smiley face design, Mini-Boss cards having a silver angry face design, Boss cards having a more complex purple grinning face design, and Token cards having a blue hexagon pattern. You can see the backs of the cards in your opponent's hand.
- **Portrait:** The icon on the upper middle section of the face of a card. These do not have any effect on gameplay and serve to help you quickly identify cards.
- **Tribe:** 
- **Cost:** 
- **Effect:** 

# Stats

- **ATK:** Attack stat. The player with a higher ATK stat at the end of the round wins the round. ATK below 0 counts as 0.
- **DEF:** Defense stat. The opponent's DEF is subtracted from your ATK at the end of the round. DEF below 0 counts as 0.
- **HP:** Health stat. If this is zero or negative at the end of a turn for one player, they lose. If both players have zero or negative health at the end of a turn, the player that wins the turn wins the match.
- **TP:** Teamwork Points. Playing cards costs teamwork points. Teamwork points are refilled before every round. The number of teamwork points available to each player generally increases as the match goes on.

## Modifying Stats

- **Pierce:** Subtract from the opponent's DEF.
- **Empower:** Increase the ATK of each specified card.
- **Enfeeble:** Decrease the ATK of each specified card.
- **Fortify:** Increase the DEF of each specified card.
- **Break:** Decrease the DEF of each specified card.
- **Unity:** An Empower that can only happen once per round.
- **Heal:** 
- **Damage:** 
- **Lifesteal:** 

# Moving Cards

## Numb

- **Numb:** Flip specified card(s) face-down. Their ATK and DEF stats are nullified. Effects other than Numb that have not yet been applied are not applied.
- **Prevent Numb:** Applies an invisible counter to all specified cards which is consumed when the card would have been targetable by a Numb.
- **On Numb:** Effect is applied the moment a card is affected by Numb.

# Conditions

- **Only Once**, **Max. X Times**, **Except First Time**, **Except First X Times:** This effect on this card can only be processed a limited number of times. The same effect can appear on copies of the same card. Different cards or different effects on the same card are not considered to be the same for this effect.
- **On Numb:** (See [Numb](#numb))
