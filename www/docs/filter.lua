function Header(el)
	if el.level ~= 1 then
		return el
	end

	return {
		pandoc.RawBlock("html", "\n</article>\n<article class=\"readme\">\n<a href=\"#top-of-page\" class=\"back-link\">&uarr; Back to Top</a>\n"),
		el,
	}
end
