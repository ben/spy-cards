---
title: Known Issues
...

# Can't Fix

*Note: On iOS, all web browsers are Safari. This is due to the [App Store Policy](https://developer.apple.com/app-store/review/guidelines/#third-party-software) not allowing other web rendering or JavaScript engines.*

- iOS 11+: `Error: Out of executable memory in function at index ####`: This is caused by Spectre mitigations and can't be fixed until Safari implements an interpreter. Play on another device if possible. <https://bugs.webkit.org/show_bug.cgi?id=181723>

# Bugs

<!--*All known issues in this category have been fixed at the time of writing.*-->

- Moving card groups can crash the card editor with a "card order invariant violation" error.
- Possible race condition in interactive connection establishment (ICE).

# Requests

<!--*All known issues in this category have been fixed at the time of writing.*-->

- View modified cards/game rules for custom modes.
- View deck during a match.
- Set NPC to battle on home screen.
