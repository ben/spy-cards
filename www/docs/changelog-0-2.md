---
title: Changelog (v0.2.x)
toc: false
...

*For newer changelog entries, see the [v0.3.x changelog](changelog.html).*

## v0.2.82

- **Spy Cards:** Fixed a problem with the encoding of recordings in singleplayer on the custom client.

## v0.2.81

- **Spy Cards:** Changed a mini-boss from Unity to Empower to match actual Bug Fables behavior. This only affects an extremely rare edge case.

## v0.2.80

- **Spy Cards:** Fixed numb not being able to target cards with 0 ATK and 0 DEF.

## v0.2.79

- **Spy Cards:** Max-size audience is now much rarer (1 in 200,000,000 rather than 1 in 20,000 for 100 members).

## v0.2.78

- **Spy Cards:** Optimized mine lightmap.

## v0.2.77

- **Spy Cards:** Numb now also considers the lower stat of cards with both ATK and DEF, so a card with 3 ATK 0 DEF will be numbed before a card with 3 ATK 1 DEF.

## v0.2.76

- **Spy Cards:** Greatly improved stage rendering performance for audience members standing near the camera.

## v0.2.75

- **Deck Editor:** Fixed deck corruption when entering something that is not a deck code into the importer.
- **Spy Cards:** Improved rendering performance.
- **Spy Cards:** Fixed inconsistencies in the random number generator.
- **Spy Cards:** Fixed several desyncs in singleplayer mode.

## v0.2.74

- **Spy Cards:** The "mine" background is now rendered in WebGL.

## v0.2.73

- **Card Editor:** Fixed Deck Limit Filter not properly displaying assigned tribes.
- **Deck Editor:** Fixed an issue with encoding decks that contained only custom cards and at least one card's number was above 32.
- **Spy Cards:** Fixed all but one card being dropped for player 2 if a variant is active in the custom client.
- **Spy Cards:** Improved performance when the Disable 3D Scenes option is turned on.

## v0.2.72

- **Card Editor:** Added the ability to force a round timer.

## v0.2.71

- **Spy Cards:** The "stage" background is now rendered in WebGL.

## v0.2.70

- **Termacade:** Added an emulated controller for touch screens.
- **Termacade:** Fixed a race condition if textured loaded while a frame was being rendered that could cause the wrong texture to be uploaded to the GPU.

## v0.2.69

- **Spy Cards:** Added an option to automatically upload recordings at the end of matches. This does not apply to matches where an NPC was playing.
- **Spy Cards:** Effects hidden by Flavor Text now run as a group rather than playing a sound and pausing for each effect.
- **Settings:** Clarified that "Clear Game Cache" does not affect saved data.
- **Settings:** Changed "Copy Control Set" to "New Control Set" based on feedback.
- **Settings:** Added High Scores Name.
- **Settings:** Added Disable CRT Effect.
- **Settings:** Added Always Show Buttons.
- **Settings:** Added Auto-Upload Recordings.
- Nice

## v0.2.68

- **Card Editor:** Tribes with names that begin with an underscore are hidden.
- **Card Editor:** Added Deck Limit Filter game rule type.
- **Spy Cards:** Cards with nested limit conditions now display the limit as a prefix to avoid confusion about where the limit is.
- **Termacade:** Improved priority order of preloading assets.

## v0.2.67

- **Spy Cards:** Improvements to variant handling that should fix some crashes.
- **Termacade:** The Termacade now runs using WebAssembly.

## v0.2.66

- **Spy Cards:** Stat changes within On Numb effects are applied directly to the player.

## v0.2.65

- **Spy Cards:** Fixed failure to connect on custom variants.
- **Card Editor:** Added "On Numb" condition.

## v0.2.64

- **Card Editor:** Page URL only updates after no edits have been made for 1 second. (This should fix browser lag issues when typing a long description.)
- **Spy Cards:** Singleplayer now uses a fake matchmaking server to avoid singleplayer-specific connection edge cases.

## v0.2.63

- **Card Editor:** Added "Unfilter Card" rule, which prevents a card from being targeted by filters.

## v0.2.62

- **Spy Cards:** Fixed NPC players processing cards as if they were numbed permanently rather than just for the turn.

## v0.2.61

- **Spy Cards:** Fixed Empower Opponent inconsistently targeting players.

## v0.2.60

- **Card Editor:** Fixed Setup effects on the opponent with even numbers of turns applying to the current player instead.
- **Match Viewer:** Added game log.

## v0.2.59

- **Spy Cards:** Cards that do replacement summons are no longer considered to exist after they perform the summon. (This matches Bug Fables.)
- **Spy Cards:** "If Card" effect now allows numbed cards to satisfy the condition.
- **Card Editor:** Individual card editors no longer load immediately. Instead, a button to edit each card is available. This cuts load times in half for large card sets.

## v0.2.58

- **Spy Cards:** Removed STUN servers from singleplayer in an attempt to make local connection attempts more reliable.
- **Spy Cards:** (hopefully) fixed a race condition when a connection was established too quickly.

## v0.2.57

- **Card Editor:** Added a new condition type: "Priority Override". This forces an effect to run at a different stage of processing than it normally would.

## v0.2.56

- **Flower Journey:** Added the random seed to the game over screen.
- ~~**Mite Knight:** Experimentally reduced speed of certain timers by one half in an attempt to better match Bug Fables.~~

## v0.2.55

- **Spy Cards:** Fixed custom client always using variant 0 from the joining player's perspective.
- **Spy Cards:** Fixed setup field not showing up until the first turn was confirmed.
- **Match Viewer:** Fixed a crash when a desync caused no player to win by the end of the match.

## v0.2.54

- **Spy Cards:** Custom cards that cost infinity TP and are also banned no longer display the TP icon and are no longer listed in the banned cards list.

## v0.2.53

- **Card Editor:** Improved performance when loading a large card set.
- **Card Editor:** Improved performance when editing a card in a large card set.
- **Card Editor:** Improved performance when opening the card selector.

## v0.2.52

- **Spy Cards:** Added some internal logging to help diagnose singleplayer desyncs.

## v0.2.51

- **Spy Cards:** Made the effect processor more resilient against timing-related desynchronization in cases where an effect would cause multiple other effects to occur.

## v0.2.50

- **Spy Cards:** Fixed HP display being clamped to 5 if it changed during a turn.
- **Spy Cards:** Fixed an encoding error in the match recording for game modes with variants.

## v0.2.49

- **Spy Cards:** Added code to help detect and diagnose desyncs at the networking layer.

## v0.2.48

- **Spy Cards:** Fixed a crash when an invisible card summons a visible card.
- **Spy Cards:** Fixed singleplayer NPC opponent closing the connection too quickly.
- **Spoiler Guard:** Fixed a crash related to a `keydown` event with no `key`.

## v0.2.47

- **Card Editor:** Added "variant" game mode field.

## v0.2.46

- **Mite Knight:** Fixed game end sound effects playing 1 second too soon.

## v0.2.45

- **Mite Knight:** Fixed end-of-game hearts display missing the first heart.
- **Termacade:** Fixed a crash related to custom controller bindings.

## v0.2.44

- **Mite Knight:** Fixed a desynchronization introduced in v0.2.41 related to being hit by an enemy shortly before moving. Recordings of versions 0.2.41, 0.2.42, and 0.2.43 will emulate this bug to maintain compatibility.

## v0.2.43

- **Card Editor:** Fixed a crash when attempting to copy an unselected effect.
- **Card Editor:** Added the ability to make a card summon as invisible. Invisible cards apply their effects with no sounds or delays.

## v0.2.42

- **Card Editor:** Added "if HP at end of round" condition.

## v0.2.41

- **Termacade:** Recordings can now be fast forwarded and rewound. Press left to go back by 10 seconds, and press right to go forward by 5 seconds.

## v0.2.40

- **Termacade:** Added support for binding actions to analog controller inputs.
- **Mite Knight:** Reduced the size of the smoke particle effect to match Bug Fables more closely.
- **Mite Knight:** Reduced duration of fade-out when starting the game.

## v0.2.39

- **Mite Knight:** Corrected the width of the hearts on the end screen.
- **Termacade:** Added support for viewing recordings to the high scores screen. Playback controls will be added later.

## v0.2.38

- **Mite Knight:** Altered particle effects to be more accurate.
    - Slightly decreased particle size.
    - Increased initial particle gravity.
    - Moved spawn point up slightly.
    - Smoke now always starts moving up.
    - Increased size and decreased intensity of grass particles.
    - Darkened smoke slightly.
- **Mite Knight:** Mite Knight now appears in front of other objects.

## v0.2.37

- **Termacade:** High Scores can now be viewed from the Termacade menu. The recording viewer is coming soon.

## v0.2.36

- **Spy Cards:** Fixed HP being limited to 5 instead of Max HP when changed.

## v0.2.35

- **Termacade:** Fixed an edge case where the frame counter could increase without actually processing a frame.
- **Card Editor:** Added a "Copy" button to effects.

## v0.2.34

- **Spy Cards:** Fixed a race condition that would cause the wrong number to be displayed when updating the stats of multiple cards via a single effect (eg. Empower).

## v0.2.33

- **Termacade:** High scores can now be recorded. They are not yet displayed.

## v0.2.32

- **Settings:** Added the ability to modify what buttons the controls are mapped onto. This currently only affects the Termacade.
- **Spy Cards:** Fixed a crash at the start of the match in Vanilla.

## v0.2.31

- **Card Editor:** Added the ability to summon a card at the start of the match, which can be used to implement more complex game rules.
- **Spy Cards:** Added the button handling logic from the Termacade. Does not yet accept gamepad buttons. Let me know if anything unexpected happens.
- **Game Log:** Added messages about random cards being selected for summon effects.

## v0.2.30

- **Card Editor:** Fixed a crash when setting a card with no effects to 0 or 10 TP.
- **Card Editor:** Fixed a crash when setting a card image to a very large file.
- **Play Online:** Fixed a crash when entering a match code before the connection to the matchmaking server is ready.

## v0.2.29

- Improved rendering performance in Mite Knight.
- Added particle effects to Mite Knight.
- Fixed dead enemies visually disappearing before having a chance to be knocked back in Mite Knight.
- Added a version number to the Termacade.
- Added instructions on how to use the Termacade.
- If you press a button on a supported game controller, key prompts will now show controller buttons in the Termacade.
- Added a placeholder "controllers" section to the settings page. In the future, it will be possible to change button mapping here.

## v0.2.28

- Fixed conditional effects not being waited for in some cases.

## v0.2.27

- The Numb effect now ignores whether the card holding it is Numbed.
- Added a settings option to the Termacade menu.
- Constructed some snazzy new neon signs in the Termacade.

## v0.2.26

- Fixed negative stat effects inside limit conditions not displaying their sign.

## v0.2.25

- Fixed a race condition between NPC movement ticks in Mite Knight.
- Added the random seed to the end screen in Mite Knight.
- Reduced delay before inputs are allowed during floor changes in Mite Knight.
- Added `?drawButtons` to display buttons being pressed in the arcade.

## v0.2.24

- Fixed fog distance in Mite Knight.
- Fixed initial camera position in Mite Knight.
- Fixed clouds being invisible 25% of the time in Flower Jouney.
- Added the ability to override game rules in Mite Knight and Flower Journey.  
  For example, `?overrideFlowerJourneyGameRule_countdownLength=5` makes Flower Journey count down from 5 rather than 3.  
  More game rules will be documented later.

## v0.2.23

- Fixed Mite Knight camera.
- Fixed Mite Knight sprite angles.

## v0.2.22

- Added compass to Mite Knight.
- Fixed fog shading on door in Mite Knight.
- Fireball now properly spins in Mite Knight.
- Fixed Termacade crash in Google Chrome.

## v0.2.21

- Fixed corridor generation in Mite Knight.
- Fixed pause screen layout in Mite Knight.

## v0.2.20

- Fixed terrain texture distortion in Mite Knight.
- Reduced GPU texture memory usage.

## v0.2.19

- Added controller support to Termacade.
- Reduced lag in Mite Knight.

## v0.2.18

- Fixed random summons summoning banned cards in some cases.

## v0.2.17

- Fixed the numb animation flipping the card that performed the numb rather than the card that was numbed.

## v0.2.16

- Changed the wording of the "If Card" condition to ensure it's always clear whether it requires a specific card or a tribe.
- Fixed a crash in the deck editor when using the combining operator (`;`) with a custom game mode.
- Reduced the amount of data downloaded when updating to a new version.
- Fixed face-down cards sometimes appearing face-up in reverse.

## v0.2.15

- Added a [settings page](/settings.html).

## v0.2.14

- Modes (eg. `midge.0`) can now be entered into the custom card editor and other places that allow custom card codes to be joined with `;`.

## v0.2.13

- Added a message when an update is available and being downloaded.
- Fixed a crash when playing a card above ID 255.

## v0.2.12

- Fixed clipping issues on Google Chrome with 3D backgrounds.
- Added a workaround for issues caused by having more than 30 cards in the player's hand.

## v0.2.11

- Fixed a crash when selecting the last mini-boss in the card list as the first mini-boss in the deck.
- Fixed a crash when creating a deck in the deck editor with a custom card set that has no game rules.
- Card TP can now be set to any value in \[-250, 250\] rather than the old limits of \[0, 10\]
- Added support for backwards-compatibly changing vanilla cards in a future release.
- Added a page at [service-worker-status](/service-worker-status) to show the exact version of Spy Cards Online's ServiceWorker that is running.

## v0.2.10

- Added "Game Rules" game mode field.
- The following properties of Spy Cards can now be modified using the custom card editor:
    - Max HP
    - Hand
        - Minimum Size
        - Maximum Size
        - Cards Drawn Per Turn
    - Teamwork Points (TP)
        - Minimum
        - Maximum
        - Gained Per Turn
    - Cards Per Deck
        - Total
        - Boss
        - Mini-Boss

## v0.2.9

- Added a notification when an update is available.
- Spy Cards Online should now load slightly faster when not updating.
- Your browser may now allow you to install Spy Cards Online as an app.

## v0.2.8

- Added support for keyboard control of the game during a match.
- Added search support to the character selector.
- Major work towards improving interface accessibility for those using assistive technologies.

## v0.2.7

- Fixed animation for setup cards being replaced via the summon effect.
- Fixed match winner calculation when both players have negative HP.

## v0.2.6

- Fixed game mode description appearing over the match.
- Renamed Empower variants:
    - +ATK remains Empower
    - -ATK is now Enfeeble
    - +DEF is now Fortify
    - -DEF is now Break
- Fixed empower opponent once being displayed as Unity.
- Numbing cards now recalculates stats, so numbing infinity ATK or DEF now works as if the card was not played in the first place.
- Numb can now target specific cards and filters other than any-tribe Attacker.

## v0.2.5

- Cards that require infinite TP (i.e. cards that cannot be added to a deck) now show a "no" symbol rather than an infinity TP symbol.
- Added the ability to have more than 2 tribes assigned to a card.
- Added some internal infrastructure for custom game mode metadata.
    - **Update:** This is now modifiable in the card editor.

## v0.2.4

- Fixed numb subtracting base attacker stats twice.

## v0.2.3

- Allowed Multiply Healing to specifically affect positive or negative heal effects.
- Setup cards that show the original card description will share a ghost card if the same card instance creates multiple setup effects.
- Added "Clone Card" button to card editor.

## v0.2.2

- Fixed a crash related to the end-of-match deck display.
- Fixed the background color of form inputs on light mode.
- Fixed negating infinity not updating the card editor preview.
- Fixed modifying the tribe of a card filter not updating the card editor preview.
- Adjusted spacing between available cards on the home page.
- Added "Number of Turns" to setup effects in the card editor.

## v0.2.1

- Fixed replace-with-random saying "cards cards".
- Fixed attacker cards with negative stats not showing the minus sign.
- Fixed giant coins in the card editor on some devices.
- Fixed logs not containing card names.
- Fixed deck encoding causing match validation errors.
- Fixed If Tribe checking the tribe of the acting card rather than the tribe of other cards.
- Improved compatibility with iOS devices.

## v0.2.0

- The custom card system has been completely rewritten.
- All existing card codes will be converted to the new system automatically.
- Most effects can now be applied directly to the opponent.
- Custom card sets can be combined using `;`. The card loader will automatically change IDs so they don't overlap.

## Older Entries

*For older changelog entries, see the [v0.1.x changelog](changelog-0-1.html).*
