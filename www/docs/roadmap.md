---
title: Spy Cards Online Version Roadmap
...

*(~~No~~ major features are currently planned.)*

## Spy Cards Online v0.4

A full rewrite of Spy Cards Online to Godot is in progress.

Planned features overview:

- No longer runs like garbage.
- Integrated game mode publishing (no more messing around with the weird website or asking me for a slot once you have an account).
- Shorter lobby join codes.
- No more websockets.
- No more copy-pasting long Base64 strings. (Download game modes as compressed files, copy cards between modes in the editor directly.)
- Locally saved game modes in the editor. (With import/export.)
- Locally saved match recordings. (Including partial recordings of matches that were aborted.)
- New "Jigsaw" language, similar to MIT Scratch but specialized for Spy Cards Online.
- Portrait images will be uploaded with the game mode rather than directly from the editor as you select the files.
- In addition to card portraits, use custom music, sound effects, sprites, and 3D models.
- Custom ranks, including fully editable card layouts/designs.
- Custom stats, usable whereever any stat is used (make a card cost ATK and HP, I'm not judging).
- Modify card data (rank, tribes, costs, name, effects, etc.) during gameplay.
- Up to 255 players and unlimited bots, adjustible per game mode variant.
- Custom effects - no more flavor text masking paragraphs of card text, just make your own effect that does what you want.
- Custom NPCs.
- Complete control over the stage, HUD, audience, and player characters.
- "Modifier" effects. (Empower and Numb are no longer special!)
- Custom locations (no longer just "exile groups").
- "Retroactive debugger" - inspect the state the game was in when *someone else* hit a "crash" command in your game mode.
- All existing content (should) still work as it currently does.
- Mite Knight and Flower Journey will be implemented in the card game mode editor.
- Rollback netcode. In a card game.
- Resume a match recording and play from that point with your friends or an NPC.
- "Termacade Back Catalog" category for game modes.
- Worm related game modes?
- No assumptions. No regerts.
