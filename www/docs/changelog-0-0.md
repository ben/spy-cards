---
title: Changelog (v0.0.x)
toc: false
...

*For newer changelog entries, see the [v0.1.x changelog](changelog-0-1.html).*

## v0.0.45

- Added a Game Log button to allow players to review what happened so far in the match.

## v0.0.44

- ATK and DEF can now become negative. Negative ATK and DEF are still shown as 0 and processed as 0 when the turn ends.

## v0.0.43

- Added "On Win" and "On Tie" conditions for custom cards.
- In cases where both players have zero or negative HP at the end of a single turn, the winner is whichever player won the turn. This is only possible via negative healing on custom cards.
- Fixed a card editor crash when a card had a custom second tribe but not a custom first tribe.

## v0.0.42

- Added link to changelog.
- Added link to card editor.

## v0.0.41

- Fixed a crash related to conditional effects.

## v0.0.40

- Fixed a crash that would occur when a card had "replace with a random card" as well as another effect.

## v0.0.39

- Added a button to submit an error report to the crash screen.
- Nested setup is now replaced by "Setup (X turns)" in the UI.

## v0.0.38

- Possible fix for old Attacker card links not working.

## v0.0.37

- Fixed Broodmother's Empower effect in the custom client.
- Added an indicator for when your connection to your opponent is lost.

## v0.0.36

- Fixed several condition types having their "invert" flag inverted on save. (Ironic!)

## v0.0.35

- "Summon Random Mini-Boss" has been expanded to all four card types.
- Fixed deck editor not allowing multiple custom cards at once.
- Added condition types:
    - VS Card
    - If Card Type
    - VS Card Type
- Most conditions (with the exception of conditions that occur multiple times and Setup) can be inverted.
- Added count fields to VS Tribe and If Card.

## v0.0.34

- Fixed the tribe list becoming longer every time it was clicked.
- Custom tribes are now available in the card editor.
- Custom Attacker cards can now have DEF, and their ATK can be modified.

## v0.0.33

- Added support for whitelisted community-created portrait icons in the custom card editor.

## v0.0.32

- Fixed Numb not counting base ATK.

## v0.0.31

- Removed "Only Once" condition for custom cards.
- Added "Limit" condition for custom cards, which is equivalent to "Only Once" if the count is set to 1.
- Fixed attacker card descriptions being squished.

## v0.0.30

- Added support for custom cards in the deck editor.
- Added a deck editor link to the custom client landing page.
- Decks containing placeholder cards can now be imported and exported.
- The card editor now allows negative values for the Amount field of effects.

## v0.0.29

- Card effects added by the summoning of a card are now processed after all summons of that type, with the correct card highlighted.
- The Per-Card condition now ignores any cards summoned during its processing.

## v0.0.28

- Added "Only Once" condition type for custom cards. This will apply its result the first time it is encountered per turn (per effect instance, per player).
- Custom card descriptions that are too long are now squished vertically.
- Card images are now uploaded to a server, greatly reducing the size of custom card codes for custom portraits.
- Custom cards can now cost 0 or infinite TP. Cards that cost infinite TP cannot be chosen when making a deck.

## v0.0.27

- Fixed "If Tribe" not encoding the count in custom cards.
- Fixed "Unity" being missing from the effect list in the card editor.
- Added an editor button to the custom cards landing page if custom cards are present.
- Fixed Setup counting as a card being played for various effects.

## v0.0.26

- Matches with custom cards are now available. Only the host needs the URL of the custom cards. Player 2 will receive the card list and have a chance to review it before accepting the match.

## v0.0.25

- A preview of the card editor is now available. It does not yet have all features implemented, and custom cards cannot yet be played, but the features that are currently in place will continue working, and card codes created now will continue working when the custom card feature is fully released.

## v0.0.24

- Fixed cards being permanently flipped on several screens related to deck selection on iOS browsers.

## v0.0.23

- Fixed card names being missing in the deck editor.

## v0.0.22

- Added fancier tribe labels to cards.
- Reduced the height of the card nameplate to better match the game.
- Added modification data to the version number to avoid playing against someone on a different client.

## v0.0.21

- Fixed extreme blurriness on Firefox when hovering over cards in some places where cards are displayed very small.
- Removed the tribe names from cards in places that display entire decks as they were unreadable anyway at such a small font size.
- Fixed a situation where the "opponent has disconnected" error handler would activate due to the matchmaking server disconnecting when it was no longer needed.

## v0.0.20

- Improved coin layout when a card has more than 3 coins.
- Fixed card summoning animation.
- Reduced the size of cards on the field slightly.

## v0.0.19

- Fix infinitely spinning coins in Google Chrome.

## v0.0.18

- Fixed summoned cards not applying effects that had already been processed.
- Fixed turn counter disappearing when playing a rematch.

## v0.0.17

- Swapped the order of the coin effects on Wasp Bomber for clarity as numb coin effects happen later in the turn.
- Added buttons to offer a rematch and to exit on the end-of-match screen.
- Added a minimum amount of time (1 second) per turn before both players being ready can end the turn.
- Fixed the icon on a certain card that has Unity.

## v0.0.16

- Fixed a crash when Carmina.

## v0.0.15

- Added an animation for coin flips.
- Added a delay after a numb effect is applied to make sure the card has time to flip.
- The card that is currently having its effect acted upon is now raised above other cards so it is always visible.

## v0.0.14

- Changed Mender from Bots (4) to Bots (5) to match ingame behavior.
- Added an experimental relayed connection method that sends all data through the matchmaking server. (opt-in)

## v0.0.13

- Added a list of tribes the card is part of to each card. This design is temporary until a better visual design is created.
- Both players now have the ATK indicator above the DEF indicator.
- Added some logging to help diagnose situations where the fatal error handler would partially activate.
- You can now click on a card while creating a deck to remove that card.
- Added a big link to the deck editor on the front page.
- Deck editor now has an "Edit" button rather than a "Save" button for decks you already have saved. The deck that was selected to be edited is not removed automatically, but can be deleted later.
- Deck imports now process in reverse order so the top code will be the top deck after importing multiple decks.

## v0.0.12

- Decks larger than 15 cards can be added to the saved deck list. Only 15-card decks can be played, for now.

## v0.0.11

- Reordered effects to make sure Scarlet's heal happens before numb.

## v0.0.10

- Fixed a possible race condition if cards or the ready button were clicked before the players' hands were fully set up.

## v0.0.9

- Fixed Unity. Again.
- Midges now have the special "1 attack per turn" effect rather than the "1 attack per card per turn" effect most cards that say "ATK: 1" have.
- Your and your opponent's decks are displayed at the end of the match.
- Added version number to the deck editor.

## v0.0.8

- Fixed saved deck corruption bug.

## v0.0.7

- Fixed "init was already called" or a similarly bizarre error message if the connection to your opponent dropped but was reestablished.
- Added webrtc-adapter.js, which should smooth out some browser implementation differences.
- Converted Spy Cards to TypeScript.

## v0.0.6

- Fixed added effects only being applied once per match.

## v0.0.5

- Fixed Setup.
- Reduced the size of cards in the deck builder.

## v0.0.4

- Replaced Venus' Guardian's Unity with Per-Card to fix the difference caused by Unity fixes.
- Fixed Unity activating for cards that already contributed to Unity.
- All tribes have been updated to match their ingame names.
- Card IDs have been updated to match their ingame values.
- Corrected spelling of "Belostoss".

## v0.0.3

- Reduced the size of the random state per turn from 2x32 bytes to 2x4 bytes. Reduced the initial random state from 2x32 bytes to 2x16 bytes. (This will greatly reduce the size of match recordings.)
- Added SRI (subresource integrity).

## v0.0.2

- Fixed stat-based conditions ignoring boosts from empower/VS.

## v0.0.1

- Added version numbers.
- Fixed summon abilities summoning a card with the summoner's description rather than the summoned card's description.
- Reverted Unity changes. Unity now matches what exists within the game.
