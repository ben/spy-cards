"use strict";
var SpyCards;
(function (SpyCards) {
    var Native;
    (function (Native) {
        function onGLContextFail(message) {
            var container = document.createElement("div");
            container.classList.add("error-report");
            var h1 = document.createElement("h1");
            h1.textContent = "Uh oh!";
            container.appendChild(h1);
            var flavor = document.createElement("p");
            flavor.classList.add("flavor");
            flavor.textContent = "Spy Cards Online was unable to start up. The error it reported was:";
            container.appendChild(flavor);
            var msg = document.createElement("p");
            msg.classList.add("message");
            msg.textContent = message;
            container.appendChild(msg);
            var h2 = document.createElement("h2");
            h2.textContent = "Next Steps";
            container.appendChild(h2);
            var ul = document.createElement("ul");
            function addHint(text) {
                var li = document.createElement("li");
                li.textContent = text;
                ul.appendChild(li);
            }
            addHint("This error most commonly occurs right after installing new graphics card drivers.");
            addHint("If you haven't restarted your web browser recently, doing so might fix this.");
            addHint("Your browser might have more information about what went wrong in the developer console (ctrl+shift+i).");
            addHint("If all else fails, ask for help on the Discord linked to on the home page.");
            container.appendChild(ul);
            document.body.appendChild(container);
            debugger;
        }
        Native.onGLContextFail = onGLContextFail;
        Native.go = new window.Go();
        var inst;
        var mod;
        var startFetch;
        var ready = new Promise(function (resolve) { return startFetch = resolve; }).then(function () {
            performance.mark("fetchWASMStart");
            var loading = document.getElementById("loading");
            var loadingSub = document.createElement("div");
            loadingSub.id = "loading-sub";
            if (loading) {
                loading.appendChild(loadingSub);
            }
            loadingSub.innerHTML = "Fetching WebAssembly&hellip;";
            return WebAssembly.instantiateStreaming(fetch("/spy-cards.wasm"), Native.go.importObject).then(function (result) {
                performance.mark("fetchWASMFinish");
                performance.measure("fetchWASM", "fetchWASMStart", "fetchWASMFinish");
                inst = result.instance;
                mod = result.module;
                loadingSub.innerHTML = "Starting up&hellip;";
            });
        });
        var failedUpdate = false;
        function checkUpdate(force) {
            if (failedUpdate) {
                return Promise.resolve();
            }
            if (!navigator.serviceWorker) {
                return Promise.resolve();
            }
            var loading = document.getElementById("loading");
            return navigator.serviceWorker.ready.then(function (sw) {
                if (sw && force) {
                    performance.mark("checkSWStart");
                    sw.update().then(function () {
                        performance.mark("checkSWFinish");
                        performance.measure("checkSW", "checkSWStart", "checkSWFinish");
                    });
                }
                return sw;
            }).then(function (sw) {
                if (!sw || !sw.installing) {
                    return Promise.resolve();
                }
                loading.innerHTML = "Updating&hellip;";
                return new Promise(function (resolve, reject) {
                    navigator.serviceWorker.addEventListener("controllerchange", function () {
                        resolve();
                    });
                    navigator.serviceWorker.addEventListener("message", function (e) {
                        if (e.data.type === "update-error") {
                            reject();
                        }
                    });
                }).then(function () {
                    setTimeout(function () { location.reload(); }, 1000);
                    return new Promise(function () { });
                });
            })["catch"](function () {
                failedUpdate = true;
                loading.innerHTML = "Loading&hellip;";
            });
        }
        Native.preloadedGeneric = {};
        Native.preloadedAudio = {};
        Native.preloadedTextures = {};
        function preloadHint() {
            var urls = [];
            for (var _i = 0; _i < arguments.length; _i++) {
                urls[_i] = arguments[_i];
            }
            var _loop_1 = function (i) {
                var url = urls[i];
                if (url.endsWith(".webp")) {
                    Native.preloadedTextures[url] = new Promise(function (resolve, reject) {
                        var img = new Image();
                        img.crossOrigin = "anonymous";
                        img.src = url;
                        img.close = function () { };
                        if (img.decode) {
                            img.decode().then(function () { return resolve(img); }, reject);
                        }
                        else {
                            img.onload = function () { return resolve(img); };
                            img.onerror = reject;
                        }
                    });
                }
                else {
                    // TODO: if opus decoding is supported, do that here
                    Native.preloadedGeneric[url] = fetch(url, {
                        mode: "cors",
                        credentials: "omit"
                    });
                }
            };
            for (var i = 0; i < urls.length; i++) {
                _loop_1(i);
            }
        }
        Native.preloadHint = preloadHint;
        function run() {
            var argv = [];
            for (var _i = 0; _i < arguments.length; _i++) {
                argv[_i] = arguments[_i];
            }
            var argvLength = 0;
            for (var i = 0; i < argv.length; i++) {
                argvLength += argv[i].length;
            }
            if (argvLength > (1 << 16)) {
                return Promise.reject(new Error("URL too long (approximately " + Math.round(argvLength / 1000) + "k characters)"));
            }
            startFetch();
            var checkingReady = checkUpdate(true);
            (function () {
                if (localStorage["spy-cards-settings-v0"]) {
                    return Promise.resolve();
                }
                var loading = document.getElementById("loading");
                ready = (function (orig) {
                    document.querySelectorAll(".hide-on-wasm-ready").forEach(function (el) { return el.parentNode.removeChild(el); });
                    SpyCards.Audio.Sounds.Confirm.preload();
                    var form = document.createElement("form");
                    form.classList.add("first-run", "readme");
                    var h1 = document.createElement("h1");
                    h1.textContent = "It looks like it's your first time here.";
                    form.appendChild(h1);
                    var p = document.createElement("p");
                    p.textContent = "These choices can be changed at any time on ";
                    var settingsLink = document.createElement("a");
                    settingsLink.href = "/settings.html";
                    settingsLink.textContent = "the Settings page";
                    p.appendChild(settingsLink);
                    p.appendChild(document.createTextNode("."));
                    form.appendChild(p);
                    var l1 = document.createElement("label");
                    l1.textContent = "Music and Sound Effects";
                    var c1 = document.createElement("input");
                    c1.type = "checkbox";
                    c1.addEventListener("input", function () {
                        if (c1.checked) {
                            SpyCards.Audio.setVolume(0.6, 0.6);
                            SpyCards.Audio.Sounds.Confirm.play();
                        }
                        else {
                            SpyCards.Audio.setVolume(0, 0);
                        }
                    });
                    if (!SpyCards.Audio.actx) {
                        c1.disabled = true;
                        l1.innerHTML = "<del>" + l1.innerHTML + "</del><br><small>(does not work in this browser)</small>";
                    }
                    l1.insertBefore(c1, l1.firstChild);
                    form.appendChild(l1);
                    var l2 = document.createElement("label");
                    l2.textContent = "3D Backgrounds";
                    var c2 = document.createElement("input");
                    c2.type = "checkbox";
                    c2.addEventListener("input", function () {
                        var settings = SpyCards.loadSettings();
                        settings.disable3D = !c2.checked;
                        SpyCards.saveSettings(settings);
                        SpyCards.Audio.Sounds.Confirm.play();
                    });
                    l2.insertBefore(c2, l2.firstChild);
                    form.appendChild(l2);
                    var sg = document.createElement("label");
                    sg.className = "spoiler-guard-link";
                    var sga = document.createElement("a");
                    sga.href = "/spoiler-guard.html";
                    sga.target = "_blank";
                    sga.textContent = "Spoiler Guard";
                    sg.appendChild(sga);
                    form.appendChild(sg);
                    var btn = document.createElement("button");
                    btn.disabled = true;
                    checkingReady.then(function () { return btn.disabled = false; });
                    btn.textContent = "Continue";
                    var formDone = new Promise(function (resolve) {
                        btn.addEventListener("click", function (e) {
                            e.preventDefault();
                            var isMobile;
                            if (navigator.userAgentData) {
                                isMobile = navigator.userAgentData.mobile;
                            }
                            else {
                                isMobile = navigator.userAgent.indexOf(" Mobile ") !== -1;
                            }
                            // make sure there is a settings object for next time
                            var settings = SpyCards.loadSettings();
                            if (isMobile) {
                                if (window.devicePixelRatio > 1) {
                                    // set sampling to subsample by default on phones
                                    settings.dpiScale = 0.5;
                                }
                                // CRT looks terrible at non-native sampling so get rid of that too
                                settings.disableCRT = true;
                                // reduce GPU power usage to extend battery life
                                // (you can't see the graphical differences at that size anyway)
                                settings.limitGPULevel = 1;
                            }
                            SpyCards.saveSettings(settings);
                            resolve();
                        });
                    });
                    form.appendChild(btn);
                    document.body.appendChild(form);
                    if (loading) {
                        loading.style.display = "none";
                    }
                    return formDone.then(function () {
                        document.body.removeChild(form);
                        if (loading) {
                            loading.style.display = "";
                        }
                        return orig;
                    });
                })(ready);
                return ready;
            })().then(function () { return checkingReady; }).then(function () {
                ready = ready.then(function () {
                    return checkUpdate(false);
                }).then(function () {
                    var params = new URLSearchParams(location.search);
                    if (params.has("GOGC")) {
                        Native.go.env.GOGC = params.get("GOGC");
                    }
                    if (params.has("GODEBUG")) {
                        Native.go.env.GODEBUG = params.get("GODEBUG");
                    }
                    if (params.has("GOMAXPROCS")) {
                        Native.go.env.GOMAXPROCS = params.get("GOMAXPROCS");
                    }
                    if (params.has("GOTRACEBACK")) {
                        Native.go.env.GOTRACEBACK = params.get("GOTRACEBACK");
                    }
                    Native.go.argv = ["js"].concat(argv);
                    window.SIGQUIT = function () {
                        Native.go._pendingEvent = { id: 0 };
                        Native.go._resume();
                    };
                    performance.mark("goInitStart");
                    return Native.go.run(inst);
                }).then(function () {
                    return WebAssembly.instantiate(mod, Native.go.importObject);
                }).then(function (i) {
                    inst = i;
                })["catch"](errorHandler);
                return ready;
            });
            return ready;
        }
        Native.run = run;
    })(Native = SpyCards.Native || (SpyCards.Native = {}));
})(SpyCards || (SpyCards = {}));
