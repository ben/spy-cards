"use strict";
var disableDoSquish = false;
function doSquish(el) {
    if (disableDoSquish) {
        return;
    }
    el.querySelectorAll(".squish").forEach(function (s) {
        var max = parseInt(getComputedStyle(s.parentElement).width.replace("px", ""), 10);
        s.style.transform = "";
        var current = s.clientWidth;
        if (current > max) {
            s.style.transform = "scaleX(calc(" + max + " / " + current + "))";
        }
    });
    el.querySelectorAll(".squishy").forEach(function (s) {
        var max = parseInt(getComputedStyle(s.parentElement).height.replace("px", ""), 10);
        s.style.transform = "";
        var current = s.clientHeight;
        if (current > max) {
            s.style.transform = "scaleY(calc(" + max + " / " + current + "))";
        }
    });
}
addEventListener("load", function () {
    doSquish(document);
});
function sleep(ms) {
    return new Promise(function (resolve) {
        setTimeout(function () {
            resolve();
        }, ms);
    });
}
var SpyCards;
(function (SpyCards) {
    function loadSettings() {
        if (localStorage["spy-cards-settings-v0"]) {
            return JSON.parse(localStorage["spy-cards-settings-v0"]);
        }
        var settings = {
            audio: {
                music: 0,
                sounds: 0
            },
            disable3D: true
        };
        var anyLegacy = false;
        if (localStorage["spy-cards-audio-settings-v0"]) {
            var legacyAudio = JSON.parse(localStorage["spy-cards-audio-settings-v0"]);
            settings.audio.music = legacyAudio.enabled || legacyAudio.musicEnabled ? 0.6 : 0;
            settings.audio.sounds = legacyAudio.enabled || legacyAudio.sfxEnabled ? 0.6 : 0;
            anyLegacy = true;
        }
        if (localStorage["spy-cards-player-sprite-v0"]) {
            settings.character = localStorage["spy-cards-player-sprite-v0"];
            anyLegacy = true;
        }
        if (anyLegacy) {
            saveSettings(settings);
            delete localStorage["spy-cards-audio-settings-v0"];
            delete localStorage["spy-cards-player-sprite-v0"];
        }
        return settings;
    }
    SpyCards.loadSettings = loadSettings;
    function saveSettings(settings) {
        localStorage["spy-cards-settings-v0"] = JSON.stringify(settings);
        if (navigator.serviceWorker && navigator.serviceWorker.controller) {
            navigator.serviceWorker.controller.postMessage({ type: "settings-changed" });
        }
        window.dispatchEvent(new Event("spy-cards-settings-changed"));
    }
    SpyCards.saveSettings = saveSettings;
})(SpyCards || (SpyCards = {}));
(function () {
    function setColorScheme() {
        var setting = SpyCards.loadSettings().theme;
        var light = setting === "light";
        if (!setting && window.matchMedia) {
            light = window.matchMedia("(prefers-color-scheme: light)").matches;
        }
        document.documentElement.classList.toggle("use-light-theme", light);
        var themeColor = document.querySelector("meta[name=\"theme-color\"]");
        if (themeColor) {
            themeColor.setAttribute("content", light ? "#acc" : "#023");
        }
    }
    window.addEventListener("spy-cards-settings-changed", setColorScheme);
    var prefersLight = window.matchMedia("(prefers-color-scheme: light)");
    if (prefersLight && prefersLight.addEventListener) {
        prefersLight.addEventListener("change", setColorScheme);
    }
    setColorScheme();
})();
