"use strict";
if (!NodeList.prototype.forEach) {
    NodeList.prototype.forEach = Array.prototype.forEach;
}
if (!window.Blob.prototype.arrayBuffer) {
    window.Blob.prototype.arrayBuffer = function () {
        var blob = this;
        return new Promise(function (resolve, reject) {
            var reader = new FileReader();
            reader.onload = function () {
                resolve(reader.result);
            };
            reader.onerror = reject;
            reader.readAsArrayBuffer(blob);
        });
    };
}
if (!window.TextDecoder) {
    window.TextDecoder = function () {
        // from https://stackoverflow.com/a/59339612/2664560
        this.decode = function (array) {
            var out = "";
            var len = array.length;
            var i = 0;
            while (i < len) {
                var c = array[i++];
                switch (c >> 4) {
                    case 0:
                    case 1:
                    case 2:
                    case 3:
                    case 4:
                    case 5:
                    case 6:
                    case 7:
                        // 0xxxxxxx
                        out += String.fromCharCode(c);
                        break;
                    case 12:
                    case 13:
                        // 110x xxxx   10xx xxxx
                        var char1 = array[i++];
                        out += String.fromCharCode(((c & 0x1F) << 6) | (char1 & 0x3F));
                        break;
                    case 14:
                        // 1110 xxxx  10xx xxxx  10xx xxxx
                        var char2 = array[i++];
                        var char3 = array[i++];
                        out += String.fromCharCode(((c & 0x0F) << 12) |
                            ((char2 & 0x3F) << 6) |
                            ((char3 & 0x3F) << 0));
                        break;
                }
            }
            return out;
        };
    };
}
if (!WebAssembly.instantiateStreaming) { // polyfill
    WebAssembly.instantiateStreaming = function (resp, importObject) {
        return Promise.resolve(resp).then(function (r) {
            return r.arrayBuffer();
        }).then(function (source) {
            return WebAssembly.instantiate(source, importObject);
        });
    };
}
