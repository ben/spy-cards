"use strict";
function feature(name, supported) {
    var el = document.querySelector("[data-feature=\"" + name + "\"]");
    if (!el) {
        throw new Error("feature list missing feature: " + name);
    }
    if (supported) {
        el.classList.add("supported");
    }
    else {
        el.classList.add("unsupported");
    }
    return supported;
}
function information(name, description) {
    var el = document.querySelector("[data-feature=\"" + name + "\"]");
    if (!el) {
        throw new Error("feature list missing feature: " + name);
    }
    el.classList.add("information");
    el.insertBefore(document.createTextNode(": " + description), el.firstElementChild);
}
// ########## basic runtime requirements ##########
feature("JavaScript", true);
feature("high resolution time", !!window.performance && !!performance.now);
feature("secure random number generator", !!window.crypto && !!crypto.getRandomValues);
feature("Background Task API", !!window.requestIdleCallback);
feature("requestAnimationFrame", !!window.requestAnimationFrame);
feature("requestPostAnimationFrame", !!window.requestPostAnimationFrame);
feature("fetch", !!window.fetch);
feature("Promise", !!window.Promise);
feature("Worker", !!window.Worker);
feature("TextEncoder", !!window.TextEncoder);
feature("TextDecoder", !!window.TextDecoder);
feature("Typed Arrays", !!window.Uint8Array && !!window.Float32Array);
// ########## communication ##########
feature("WebSocket", !!window.WebSocket);
feature("WebRTC", !!window.RTCPeerConnection);
feature("ServiceWorker", !!navigator.serviceWorker && !!navigator.serviceWorker.register);
// ########## WebGL ##########
if (feature("HTML5 Canvas", !!window.HTMLCanvasElement)) {
    var gl2 = document.createElement("canvas").getContext("webgl");
    if (feature("WebGL", !!gl2)) {
        feature("OES_element_index_uint", !!gl2.getExtension("OES_element_index_uint"));
        feature("OES_standard_derivatives", !!gl2.getExtension("OES_standard_derivatives"));
        feature("EXT_texture_filter_anisotropic", !!gl2.getExtension("EXT_texture_filter_anisotropic"));
        feature("WEBGL_debug_renderer_info", !!gl2.getExtension("WEBGL_debug_renderer_info"));
        feature("KHR_parallel_shader_compile", !!gl2.getExtension("KHR_parallel_shader_compile"));
        feature("GPULevel Medium", gl2.getParameter(gl2.MAX_TEXTURE_SIZE) >= 8192);
        feature("GPULevel High", gl2.getParameter(gl2.MAX_TEXTURE_SIZE) >= 16384 && gl2.getParameter(gl2.RENDERER).indexOf("Intel") === -1 && (!gl2.getExtension("WEBGL_debug_renderer_info") || gl2.getParameter(gl2.getExtension("WEBGL_debug_renderer_info").UNMASKED_RENDERER_WEBGL).indexOf("Intel") === -1));
        var ext = gl2.getExtension("WEBGL_lose_context");
        if (feature("WEBGL_lose_context", !!ext)) {
            ext.loseContext();
        }
    }
    var gl3 = document.createElement("canvas").getContext("webgl2");
    if (feature("WebGL 2", !!gl3)) {
        feature("EXT2_texture_filter_anisotropic", !!gl3.getExtension("EXT_texture_filter_anisotropic"));
        feature("WEBGL2_debug_renderer_info", !!gl3.getExtension("WEBGL_debug_renderer_info"));
        feature("KHR2_parallel_shader_compile", !!gl3.getExtension("KHR_parallel_shader_compile"));
        feature("GPULevel2 Medium", gl3.getParameter(gl3.MAX_TEXTURE_SIZE) >= 8192);
        feature("GPULevel2 High", gl3.getParameter(gl3.MAX_TEXTURE_SIZE) >= 16384 && gl3.getParameter(gl3.RENDERER).indexOf("Intel") === -1 && (!gl3.getExtension("WEBGL_debug_renderer_info") || gl3.getParameter(gl3.getExtension("WEBGL_debug_renderer_info").UNMASKED_RENDERER_WEBGL).indexOf("Intel") === -1));
        var ext = gl3.getExtension("WEBGL_lose_context");
        if (feature("WEBGL2_lose_context", !!ext)) {
            ext.loseContext();
        }
    }
}
if (feature("createImageBitmap available", !!window.createImageBitmap)) {
    fetch("/img/logo512.png", {
        mode: "cors",
        credentials: "omit"
    }).then(function (resp) {
        return resp.blob();
    }).then(function (blob) {
        return createImageBitmap(blob, { premultiplyAlpha: "premultiply" });
    }).then(function (ib) {
        ib.close();
        feature("createImageBitmap works", true);
    }, function () {
        feature("createImageBitmap works", false);
    });
}
(function (img) {
    img.onload = function () {
        feature("WebP Decoding", true);
    };
    img.onerror = function () {
        feature("WebP Decoding", false);
    };
    img.src = "data:image/webp;base64,UklGRi4AAABXRUJQVlA4ICIAAABwAQCdASoBAAEAD8D+JaACdAFAAAD+y7XJ+l7SXS4tAAAA";
})(new Image());
// ########## WebAssembly ##########
function wasmFeature(name, module) {
    feature("WebAssembly Extension: " + name, WebAssembly.validate(module));
}
if (feature("WebAssembly", !!window.WebAssembly)) {
    // we use a polyfill for instantiateStreaming, but
    // not for compileStreaming, so check the one that
    // won't be pollyfilled.
    feature("WebAssembly.instantiateStreaming", !!window.WebAssembly.compileStreaming);
    // (module
    //   (func (export "f") (result v128)
    //     (v128.const i32x4 1 2 3 4)))
    wasmFeature("SIMD", new Uint8Array([
        0x00, 0x61, 0x73, 0x6d, 0x01, 0x00, 0x00, 0x00,
        0x01, 0x05, 0x01, 0x60, 0x00, 0x01, 0x7b, 0x03,
        0x02, 0x01, 0x00, 0x07, 0x05, 0x01, 0x01, 0x66,
        0x00, 0x00, 0x0a, 0x16, 0x01, 0x14, 0x00, 0xfd,
        0x0c, 0x01, 0x00, 0x00, 0x00, 0x02, 0x00, 0x00,
        0x00, 0x03, 0x00, 0x00, 0x00, 0x04, 0x00, 0x00,
        0x00, 0x0b,
    ]));
    // (module
    //   (memory (1) (1) (shared))
    //   (func (export "f")
    //     (atomic.fence)))
    wasmFeature("Threads", new Uint8Array([
        0x00, 0x61, 0x73, 0x6d, 0x01, 0x00, 0x00, 0x00,
        0x01, 0x04, 0x01, 0x60, 0x00, 0x00, 0x03, 0x02,
        0x01, 0x00, 0x05, 0x06, 0x01, 0x01, 0x00, 0x80,
        0x80, 0x04, 0x07, 0x05, 0x01, 0x01, 0x66, 0x00,
        0x00, 0x0a, 0x07, 0x01, 0x05, 0x00, 0xfe, 0x03,
        0x00, 0x0b, 0x0b, 0x06, 0x01, 0x00, 0x41, 0x00,
        0x0b, 0x00,
    ]));
    // (module
    //   (func (export "f") (param $x i32) (result i32)
    //     (i32.extend16_s (local.get $x))))
    wasmFeature("SignExt", new Uint8Array([
        0x00, 0x61, 0x73, 0x6d, 0x01, 0x00, 0x00, 0x00,
        0x01, 0x06, 0x01, 0x60, 0x01, 0x7f, 0x01, 0x7f,
        0x03, 0x02, 0x01, 0x00, 0x07, 0x05, 0x01, 0x01,
        0x66, 0x00, 0x00, 0x0a, 0x07, 0x01, 0x05, 0x00,
        0x20, 0x00, 0xc1, 0x0b,
    ]));
    // (module
    //   (func (export "f") (param $x f64) (result i32)
    //     (i32.trunc_sat_f64_s (local.get $x))))
    wasmFeature("SatConv", new Uint8Array([
        0x00, 0x61, 0x73, 0x6d, 0x01, 0x00, 0x00, 0x00,
        0x01, 0x06, 0x01, 0x60, 0x01, 0x7c, 0x01, 0x7f,
        0x03, 0x02, 0x01, 0x00, 0x07, 0x05, 0x01, 0x01,
        0x66, 0x00, 0x00, 0x0a, 0x08, 0x01, 0x06, 0x00,
        0x20, 0x00, 0xfc, 0x02, 0x0b,
    ]));
}
// ########## Audio ##########
feature("AudioContext", (!!window.AudioContext || !!window.webkitAudioContext) &&
    isFinite(new (window.AudioContext || window.webkitAudioContext)().currentTime));
if (feature("Media Capabilities API", !!navigator.mediaCapabilities)) {
    navigator.mediaCapabilities.decodingInfo({
        "type": "file",
        "audio": {
            "contentType": "audio/ogg;codecs=opus"
        }
    }).then(function (info) {
        feature("Opus Decoding", info.supported);
    });
}
// ########## Informational ##########
if (window.requestAnimationFrame) {
    var actx_1 = (window.AudioContext || window.webkitAudioContext) ? new (window.AudioContext || window.webkitAudioContext)() : null;
    // record 60 frame timings; drop 5 samples from each side; round average
    var frameTimings_1 = [];
    var audioLatency_1 = [];
    requestAnimationFrame(function measure(t) {
        frameTimings_1.push(t);
        audioLatency_1.push(actx_1 ? actx_1.outputLatency : 0);
        if (frameTimings_1.length <= 60) {
            requestAnimationFrame(measure);
        }
        else {
            for (var i = frameTimings_1.length - 1; i > 0; i--) {
                frameTimings_1[i] -= frameTimings_1[i - 1];
            }
            frameTimings_1.shift(); // drop base time
            frameTimings_1.sort(function (a, b) {
                return b - a;
            });
            audioLatency_1.sort(function (a, b) {
                return b - a;
            });
            var frameTimingsFiltered = frameTimings_1.slice(5, -5);
            information("target frame rate", Math.round(1000 * frameTimingsFiltered.length / frameTimingsFiltered.reduce(function (a, b) {
                return a + b;
            }, 0)) + " FPS");
            var audioLatencyFiltered = audioLatency_1.slice(5, -5);
            if (actx_1) {
                information("audio output latency", Math.round(audioLatencyFiltered.reduce(function (a, b) {
                    return a + b;
                }, 0) / audioLatencyFiltered.length * 100000) / 100 + "ms");
            }
        }
    });
}
information("device pixel ratio", devicePixelRatio + "\u00d7");
