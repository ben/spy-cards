"use strict";
document.querySelectorAll("#screenshot-gallery a").forEach(function (el) {
    el.addEventListener("click", function (e) {
        e.preventDefault();
        var big = document.querySelector("#big-screenshot");
        big.innerHTML = "";
        var img = new Image();
        img.width = 1920;
        img.height = 1080;
        img.crossOrigin = "anonymous";
        img.src = el.href;
        img.alt = el.firstChild.alt;
        img.title = el.firstChild.alt;
        big.appendChild(img);
    });
});
addEventListener("load", function () {
    var po = document.getElementById("play-online");
    if (po) {
        var versionLink = document.createElement("a");
        versionLink.className = "version-number";
        versionLink.textContent = "v" + spyCardsVersionPrefix;
        versionLink.href = "/docs/changelog.html#v" + spyCardsVersionPrefix;
        po.appendChild(versionLink);
    }
});
