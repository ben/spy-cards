"use strict";
var SpyCards;
(function (SpyCards) {
    var UI;
    (function (UI) {
        function remove(el) {
            if (el && el.parentNode) {
                el.parentNode.removeChild(el);
            }
        }
        UI.remove = remove;
        function clear(el) {
            if (!el) {
                return;
            }
            el.textContent = "";
        }
        UI.clear = clear;
        function button(label, classes, click) {
            var btn = document.createElement("button");
            btn.classList.add.apply(btn.classList, classes);
            btn.textContent = label;
            btn.addEventListener("click", function (e) {
                e.preventDefault();
                click();
            });
            return btn;
        }
        UI.button = button;
        function option(label, value) {
            var opt = document.createElement("option");
            opt.textContent = label;
            opt.value = value;
            return opt;
        }
        UI.option = option;
    })(UI = SpyCards.UI || (SpyCards.UI = {}));
})(SpyCards || (SpyCards = {}));
