"use strict";
var SpyCards;
(function (SpyCards) {
    var TheRoom;
    (function (TheRoom) {
        function playerDisplayName(character) {
            return character.displayName || (character.name.substr(0, 1).toUpperCase() + character.name.substr(1));
        }
        TheRoom.playerDisplayName = playerDisplayName;
        var Player;
        (function (Player) {
            Player.characters = [
                {
                    name: "amber"
                },
                {
                    name: "aria",
                    displayName: "Acolyte Aria"
                },
                {
                    name: "arie"
                },
                {
                    name: "astotheles"
                },
                {
                    name: "bomby"
                },
                {
                    name: "bu-gi"
                },
                {
                    name: "carmina"
                },
                {
                    name: "celia"
                },
                {
                    name: "cenn"
                },
                {
                    name: "cerise"
                },
                {
                    name: "chompy"
                },
                {
                    name: "chubee"
                },
                {
                    name: "chuck"
                },
                {
                    name: "crisbee"
                },
                {
                    name: "crow"
                },
                {
                    name: "diana"
                },
                {
                    name: "eremi"
                },
                {
                    name: "futes"
                },
                {
                    name: "genow"
                },
                {
                    name: "honeycomb",
                    displayName: "Professor Honeycomb"
                },
                {
                    name: "janet"
                },
                {
                    name: "jaune"
                },
                {
                    name: "jayde"
                },
                {
                    name: "johnny"
                },
                {
                    name: "kabbu"
                },
                {
                    name: "kage"
                },
                {
                    name: "kali"
                },
                {
                    name: "kenny"
                },
                {
                    name: "kina"
                },
                {
                    name: "lanya"
                },
                {
                    name: "leif"
                },
                {
                    name: "levi"
                },
                {
                    name: "maki"
                },
                {
                    name: "malbee"
                },
                {
                    name: "mar"
                },
                {
                    name: "mothiva"
                },
                {
                    name: "neolith",
                    displayName: "Professor Neolith"
                },
                {
                    name: "nero"
                },
                {
                    name: "pibu"
                },
                {
                    name: "pisci"
                },
                {
                    name: "ritchee"
                },
                {
                    name: "riz"
                },
                {
                    name: "samira"
                },
                {
                    name: "scarlet",
                    displayName: "Monsieur Scarlet"
                },
                {
                    name: "serene"
                },
                {
                    name: "shay"
                },
                {
                    name: "tanjerin"
                },
                {
                    name: "ultimax",
                    displayName: "General Ultimax"
                },
                {
                    name: "vanessa"
                },
                {
                    name: "vi"
                },
                {
                    name: "yin"
                },
                {
                    name: "zaryant"
                },
                {
                    name: "zasp"
                }
            ];
        })(Player = TheRoom.Player || (TheRoom.Player = {}));
    })(TheRoom = SpyCards.TheRoom || (SpyCards.TheRoom = {}));
})(SpyCards || (SpyCards = {}));
