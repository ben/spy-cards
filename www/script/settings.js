"use strict";
var form = document.querySelector("form");
while (form.firstChild) {
    form.removeChild(form.firstChild);
}
if (new URLSearchParams(location.search).get("back") === "arcade") {
    document.querySelector(".back-link").href = "arcade.html";
}
var fs;
function fieldset(name) {
    fs = document.createElement("fieldset");
    var legend = document.createElement("legend");
    legend.textContent = name;
    fs.appendChild(legend);
    form.appendChild(fs);
}
function range(name, initialValue, update, max) {
    if (max === void 0) { max = "1"; }
    var input = document.createElement("input");
    input.type = "range";
    input.min = "0";
    input.max = max;
    input.step = "0.05";
    input.valueAsNumber = initialValue;
    var caption = document.createElement("span");
    caption.textContent = input.valueAsNumber ? (input.valueAsNumber * 100).toFixed(0) + "%" : "off";
    input.addEventListener("input", function () {
        caption.textContent = input.valueAsNumber ? (input.valueAsNumber * 100).toFixed(0) + "%" : "off";
        update(input.valueAsNumber);
    });
    var label = document.createElement("label");
    label.textContent = name + ": ";
    label.appendChild(caption);
    label.appendChild(document.createElement("br"));
    label.appendChild(input);
    fs.appendChild(label);
}
function checkbox(name, initialValue, update) {
    var input = document.createElement("input");
    input.type = "checkbox";
    input.checked = initialValue;
    input.addEventListener("input", function () {
        update(input.checked);
    });
    var label = document.createElement("label");
    label.appendChild(input);
    label.appendChild(document.createTextNode(" " + name));
    fs.appendChild(label);
    return input;
}
function select(name, values, initialValue, update) {
    var select = document.createElement("select");
    for (var i = 0; i < values.length; i++) {
        var opt = SpyCards.UI.option(values[i].name, values[i].value);
        if (values[i].disabled) {
            opt.disabled = true;
        }
        select.appendChild(opt);
    }
    select.value = initialValue;
    select.addEventListener("input", function () {
        update(select.value);
    });
    var label = document.createElement("label");
    label.textContent = name + ": ";
    label.appendChild(document.createElement("br"));
    label.appendChild(select);
    fs.appendChild(label);
    return label;
}
var settings = SpyCards.loadSettings();
fieldset("Audio");
range("Music", settings.audio.music, function (music) {
    settings.audio.music = music;
    SpyCards.saveSettings(settings);
});
range("Sounds", settings.audio.sounds, function (sounds) {
    settings.audio.sounds = sounds;
    SpyCards.saveSettings(settings);
});
fieldset("Flags");
checkbox("Disable 3D Scenes", settings.disable3D, function (disabled) {
    settings.disable3D = disabled;
    SpyCards.saveSettings(settings);
});
checkbox("Disable CRT Effect", settings.disableCRT, function (force) {
    settings.disableCRT = force;
    SpyCards.saveSettings(settings);
});
checkbox("Show Buttons In Termacade", settings.displayTermacadeButtons, function (force) {
    settings.displayTermacadeButtons = force;
    SpyCards.saveSettings(settings);
});
checkbox("Auto-Upload Recordings", settings.autoUploadRecording, function (force) {
    settings.autoUploadRecording = force;
    SpyCards.saveSettings(settings);
});
var prm = checkbox("Disable Passive Animations", settings.prefersReducedMotion, function (force) {
    settings.prefersReducedMotion = force;
    SpyCards.saveSettings(settings);
});
if (window.matchMedia && window.matchMedia("(prefers-reduced-motion: reduce)").matches) {
    prm.checked = true;
    prm.disabled = true;
}
checkbox("Use Standard Size", !settings.noStandardSize, function (force) {
    settings.noStandardSize = !force;
    SpyCards.saveSettings(settings);
});
checkbox("Alternative Card Colors", settings.alternateColors, function (force) {
    settings.alternateColors = force;
    SpyCards.saveSettings(settings);
});
checkbox("Screen Reader Support (Partial)", settings.screenReader, function (force) {
    settings.screenReader = force;
    SpyCards.saveSettings(settings);
});
select("Render Sampling", [{
        name: "Subsample",
        value: "0.5"
    }, {
        name: "Normal",
        value: "1"
    }, {
        name: "Supersample",
        value: "2"
    }].concat((settings.dpiScale && settings.dpiScale !== 0.5 && settings.dpiScale !== 1 && settings.dpiScale !== 2) ? [{
        name: "Custom (" + settings.dpiScale + ")",
        value: String(settings.dpiScale)
    }] : []), String(settings.dpiScale || 1), function (scale) {
    settings.dpiScale = parseFloat(scale);
    SpyCards.saveSettings(settings);
});
var testGL = document.createElement("canvas").getContext("webgl");
var testDebugRendererInfo = testGL && testGL.getExtension("WEBGL_debug_renderer_info");
select("Graphics Quality", [{
        name: "High",
        value: "3",
        disabled: !testGL || testGL.getParameter(testGL.MAX_TEXTURE_SIZE) < 16384 || (testDebugRendererInfo && testGL.getParameter(testDebugRendererInfo.UNMASKED_RENDERER_WEBGL).indexOf("Intel") !== -1)
    }, {
        name: "Medium",
        value: "2",
        disabled: !testGL || testGL.getParameter(testGL.MAX_TEXTURE_SIZE) < 8192
    }, {
        name: "Low",
        value: "1"
    }], String(settings.limitGPULevel || "3"), function (level) {
    settings.limitGPULevel = parseInt(level, 10);
    SpyCards.saveSettings(settings);
});
var loseTestGL = testGL && testGL.getExtension("WEBGL_lose_context");
if (loseTestGL) {
    loseTestGL.loseContext();
}
select("Color Scheme", [{
        name: "Use System Setting (" + ((window.matchMedia && window.matchMedia("(prefers-color-scheme: light)").matches) ? "Light" : "Dark") + ")",
        value: ""
    }, {
        name: "Light",
        value: "light"
    }, {
        name: "Dark",
        value: "dark"
    }], String(settings.theme || ""), function (value) {
    if (value) {
        settings.theme = value;
    }
    else {
        delete settings.theme;
    }
    SpyCards.saveSettings(settings);
});
if (navigator.serviceWorker && navigator.serviceWorker.controller) {
    fs.appendChild(document.createElement("br"));
    fs.appendChild(SpyCards.UI.button("Clear Game Cache", [], function () {
        if (!confirm("Really clear game cache? This will cause Spy Cards Online to redownload all assets, which will take a while, but can also fix problems caused by cache corruption. This will not affect any saved settings or data.")) {
            return;
        }
        caches.keys().then(function (keys) {
            return Promise.all(keys.map(function (key) {
                return caches["delete"](key);
            }));
        }).then(function () {
            return navigator.serviceWorker.getRegistration();
        }).then(function (reg) {
            return reg.unregister();
        }).then(function () {
            location.reload();
        });
    }));
}
fieldset("Future Settings");
checkbox("Shadows", typeof settings.shadows == "undefined" ? true : settings.shadows, function (shadows) {
    settings.shadows = shadows;
    SpyCards.saveSettings(settings);
});
range("Card Face Resolution", settings.cardFaceResolution || 2.0, function (resolution) {
    settings.cardFaceResolution = resolution;
    SpyCards.saveSettings(settings);
}, "2.5");
fieldset("Last Selected");
select("Player Character", [{
        name: "(none)",
        value: ""
    }].concat(SpyCards.TheRoom.Player.characters.map(function (c) { return ({
    name: SpyCards.TheRoom.playerDisplayName(c),
    value: c.name
}); })), settings.character || "", function (value) {
    settings.character = value || null;
    SpyCards.saveSettings(settings);
});
select("Termacade Menu Option", [{
        name: "(none)",
        value: ""
    }, {
        name: "Flower Journey",
        value: "0"
    }, {
        name: "Mite Knight",
        value: "1"
    }, {
        name: "High Scores",
        value: "2"
    }, {
        name: "Settings",
        value: "3"
    }], String(settings.lastTermacadeOption || ""), function (value) {
    settings.lastTermacadeOption = parseInt(value, 10) || null;
    SpyCards.saveSettings(settings);
});
var termacadeNameInput = document.createElement("input");
termacadeNameInput.type = "text";
termacadeNameInput.value = settings.lastTermacadeName || "";
termacadeNameInput.required = false;
termacadeNameInput.minLength = 3;
termacadeNameInput.maxLength = 3;
termacadeNameInput.pattern = "^[0-9A-Z]*$";
termacadeNameInput.addEventListener("input", function () {
    if (termacadeNameInput.reportValidity()) {
        settings.lastTermacadeName = termacadeNameInput.value;
        SpyCards.saveSettings(settings);
    }
});
var termacadeNameLabel = document.createElement("label");
termacadeNameLabel.appendChild(document.createTextNode("High Scores Name:"));
termacadeNameLabel.appendChild(document.createElement("br"));
termacadeNameLabel.appendChild(termacadeNameInput);
fs.appendChild(termacadeNameLabel);
fieldset("Spoiler Guard");
var spoilerGuardStatus = document.createElement("p");
spoilerGuardStatus.textContent = SpyCards.SpoilerGuard.getSpoilerGuardData() ? "Active" : "Inactive";
fs.appendChild(spoilerGuardStatus);
fs.appendChild(SpyCards.UI.button("Spoiler Guard Settings", [], function () { return location.href = "spoiler-guard.html"; }));
fieldset("Controllers");
var controllerFieldset = fs;
var controllerLegend = fs.querySelector("legend");
function refreshControllers() {
    SpyCards.UI.clear(controllerFieldset);
    controllerFieldset.appendChild(controllerLegend);
    if (!settings.controls) {
        settings.controls = {};
    }
    if (!settings.controls.gamepad) {
        settings.controls.gamepad = {};
    }
    if (!settings.controls.customKB) {
        settings.controls.customKB = [];
    }
    if (!settings.controls.customGP) {
        settings.controls.customGP = [];
    }
    var keyboardOptions = [
        {
            name: "Bug Fables Default",
            value: "0"
        }
    ].concat(settings.controls.customKB.map(function (_a, i) {
        var name = _a.name;
        return ({ name: name, value: String(i + 1) });
    }));
    fs = controllerFieldset;
    select("Keyboard", keyboardOptions, String(settings.controls.keyboard || 0), function (selected) {
        settings.controls.keyboard = parseInt(selected, 10);
        SpyCards.saveSettings(settings);
    });
    var gamepads = navigator.getGamepads ? navigator.getGamepads() || [] : [];
    var any = false;
    var _loop_1 = function (i) {
        var gp = gamepads[i];
        if (!gp) {
            return "continue";
        }
        any = true;
        var gamepadOptions = [
            {
                name: "Standard Mapping",
                value: "0"
            }
        ].concat(settings.controls.customGP.map(function (_a, i) {
            var name = _a.name;
            return ({ name: name, value: String(i + 1) });
        }));
        var label = select("", gamepadOptions, String(settings.controls.gamepad[gp.id] || 0), function (selected) {
            settings.controls.gamepad[gp.id] = parseInt(selected, 10);
            SpyCards.saveSettings(settings);
        });
        if (gp.mapping !== "standard") {
            label.querySelector("option").disabled = true;
            if (label.querySelector("select").value === "0") {
                label.querySelector("select").value = "";
            }
        }
        var controllerID = document.createElement("code");
        controllerID.textContent = gp.id;
        label.insertBefore(controllerID, label.firstChild);
    };
    for (var i = 0; i < gamepads.length; i++) {
        _loop_1(i);
    }
    if (!any) {
        var note = document.createElement("p");
        note.style.fontStyle = "italic";
        note.textContent = "No gamepads detected. If one is plugged in, try pressing a button on it while this page is open.";
        controllerFieldset.appendChild(note);
    }
}
refreshControllers();
addEventListener("gamepadconnected", function () { return refreshControllers(); });
addEventListener("gamepaddisconnected", function () { return refreshControllers(); });
fieldset("Controls");
var controlsFieldset = fs;
var controlsLegend = fs.querySelector("legend");
function buildControlsEditor(selected) {
    if (selected === void 0) { selected = "kb0"; }
    SpyCards.UI.clear(controlsFieldset);
    controlsFieldset.appendChild(controlsLegend);
    var controlSetLabel = document.createElement("label");
    controlSetLabel.textContent = "Edit Control Set: ";
    controlSetLabel.appendChild(document.createElement("br"));
    var controlSetSelect = document.createElement("select");
    controlSetLabel.appendChild(controlSetSelect);
    var controlSetGroupKB = document.createElement("optgroup");
    controlSetGroupKB.label = "Keyboard";
    var controlSetGroupGP = document.createElement("optgroup");
    controlSetGroupGP.label = "Gamepad";
    controlSetSelect.appendChild(controlSetGroupKB);
    controlSetSelect.appendChild(controlSetGroupGP);
    var defaultControlSetOptionKB = SpyCards.UI.option("Bug Fables Default", "kb0");
    var defaultControlSetOptionGP = SpyCards.UI.option("Standard Mapping", "gp0");
    controlSetGroupKB.appendChild(defaultControlSetOptionKB);
    controlSetGroupGP.appendChild(defaultControlSetOptionGP);
    if (!settings.controls) {
        settings.controls = {};
    }
    if (!settings.controls.customKB) {
        settings.controls.customKB = [];
    }
    if (!settings.controls.customGP) {
        settings.controls.customGP = [];
    }
    settings.controls.customKB.forEach(function (_a, i) {
        var name = _a.name;
        controlSetGroupKB.appendChild(SpyCards.UI.option(name, "kb" + (i + 1)));
    });
    settings.controls.customGP.forEach(function (_a, i) {
        var name = _a.name;
        controlSetGroupGP.appendChild(SpyCards.UI.option(name, "gp" + (i + 1)));
    });
    controlSetSelect.value = selected;
    controlsFieldset.appendChild(controlSetLabel);
    controlSetSelect.addEventListener("input", function () {
        fs = controlsFieldset;
        SpyCards.UI.clear(controlsFieldset);
        controlsFieldset.appendChild(controlsLegend);
        controlsFieldset.appendChild(controlSetLabel);
        var index = parseInt(controlSetSelect.value.substr(2), 10);
        var baseData = {
            name: controlSetSelect.selectedOptions[0].textContent
        };
        if (controlSetSelect.value.startsWith("kb")) {
            var keys_1 = index === 0 ? SpyCards.defaultKeyButton : settings.controls.customKB[index - 1].code;
            baseData.code = keys_1;
            for (var btn = SpyCards.Button.Up; btn <= SpyCards.Button.Help; btn++) {
                addButton(btn, keys_1[btn], index === 0 ? null : function (input, btn) {
                    input.value = "(press key)";
                    SpyCards.nextPressedKey().then(function (code) {
                        input.value = code;
                        keys_1[btn] = code;
                        SpyCards.saveSettings(settings);
                    });
                }, SpyCards.ButtonStyle.Keyboard);
            }
        }
        else if (controlSetSelect.value.startsWith("gp")) {
            var note = document.createElement("p");
            note.style.fontStyle = "italic";
            note.textContent = "Gamepads that support the Standard Mapping control set use a shared, ";
            var noteLink = document.createElement("a");
            noteLink.href = "https://w3c.github.io/gamepad/standard_gamepad.svg";
            noteLink.target = "_blank";
            noteLink.rel = "noopener";
            noteLink.textContent = "standardized set of button positions";
            note.appendChild(noteLink);
            note.appendChild(document.createTextNode("."));
            controlsFieldset.appendChild(note);
            var buttons_1 = index === 0 ? SpyCards.standardGamepadButton : settings.controls.customGP[index - 1].button;
            baseData.button = buttons_1;
            var style = index !== 0 && settings.controls.customGP[index - 1].style || SpyCards.ButtonStyle.GenericGamepad;
            baseData.style = style;
            for (var btn = SpyCards.Button.Up; btn <= SpyCards.Button.Help; btn++) {
                addButton(btn, formatButton(buttons_1[btn]), index === 0 ? null : function (input, btn) {
                    debugger;
                    input.value = "(press button)";
                    SpyCards.nextPressedButton().then(function (buttonID) {
                        input.value = formatButton(buttonID);
                        buttons_1[btn] = buttonID;
                        SpyCards.saveSettings(settings);
                    });
                }, style);
            }
        }
        else {
            debugger;
        }
        controlsFieldset.appendChild(document.createElement("br"));
        controlsFieldset.appendChild(SpyCards.UI.button("New Control Set", [], function () {
            var data = JSON.parse(JSON.stringify(baseData));
            data.name = prompt("What should the new control set be named?", data.name);
            if (controlSetSelect.value.startsWith("kb")) {
                buildControlsEditor("kb" + settings.controls.customKB.push(data));
            }
            else if (controlSetSelect.value.startsWith("gp")) {
                buildControlsEditor("gp" + settings.controls.customGP.push(data));
            }
            else {
                debugger;
            }
            SpyCards.saveSettings(settings);
            refreshControllers();
        }));
        if (index === 0) {
            var note = document.createElement("p");
            note.style.fontStyle = "italic";
            note.textContent = "This is a default control set. It cannot be modified or deleted.";
            controlsFieldset.appendChild(note);
        }
        else {
            controlsFieldset.appendChild(document.createElement("br"));
            controlsFieldset.appendChild(SpyCards.UI.button("Delete Control Set", [], function () {
                if (!confirm("Are you sure you want to delete the control set '" + baseData.name + "'? This cannot be undone.")) {
                    return;
                }
                if (controlSetSelect.value.startsWith("kb")) {
                    settings.controls.customKB.splice(index - 1, 1);
                    buildControlsEditor("kb0");
                }
                else if (controlSetSelect.value.startsWith("gp")) {
                    settings.controls.customGP.splice(index - 1, 1);
                    buildControlsEditor("gp0");
                }
                else {
                    debugger;
                }
                SpyCards.saveSettings(settings);
                refreshControllers();
            }));
        }
        function formatButton(buttonID) {
            if (Array.isArray(buttonID)) {
                return "Axis " + buttonID[0] + (buttonID[1] ? " (+)" : " (-)");
            }
            return "Button " + buttonID;
        }
        function addButton(button, current, edit, style) {
            var label = document.createElement("label");
            label.textContent = SpyCards.Button[button] + ": ";
            label.appendChild(document.createElement("br"));
            var input = document.createElement("input");
            input.type = "text";
            input.readOnly = true;
            if (edit) {
                input.addEventListener("focus", function () {
                    edit(input, button);
                    requestAnimationFrame(function () {
                        input.blur();
                    });
                });
            }
            else {
                input.disabled = true;
            }
            input.value = current;
            label.appendChild(input);
            controlsFieldset.appendChild(label);
        }
    });
    controlSetSelect.dispatchEvent(new InputEvent("input"));
}
buildControlsEditor();
