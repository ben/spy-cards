"use strict";
var __values = (this && this.__values) || function(o) {
    var s = typeof Symbol === "function" && Symbol.iterator, m = s && o[s], i = 0;
    if (m) return m.call(o);
    if (o && typeof o.length === "number") return {
        next: function () {
            if (o && i >= o.length) o = void 0;
            return { value: o && o[i++], done: !o };
        }
    };
    throw new TypeError(s ? "Object is not iterable." : "Symbol.iterator is not defined.");
};
function removeCardNulls(obj, isCard) {
    var e_1, _a, e_2, _b;
    if (Array.isArray(obj)) {
        try {
            for (var obj_1 = __values(obj), obj_1_1 = obj_1.next(); !obj_1_1.done; obj_1_1 = obj_1.next()) {
                var c = obj_1_1.value;
                removeCardNulls(c, isCard);
            }
        }
        catch (e_1_1) { e_1 = { error: e_1_1 }; }
        finally {
            try {
                if (obj_1_1 && !obj_1_1.done && (_a = obj_1["return"])) _a.call(obj_1);
            }
            finally { if (e_1) throw e_1.error; }
        }
        return;
    }
    if (typeof obj !== "object" || !obj) {
        return;
    }
    try {
        for (var _c = __values(Object.keys(obj)), _d = _c.next(); !_d.done; _d = _c.next()) {
            var key = _d.value;
            if (isCard && obj[key] === null) {
                delete obj[key];
                continue;
            }
            removeCardNulls(obj[key], isCard || key === "card");
        }
    }
    catch (e_2_1) { e_2 = { error: e_2_1 }; }
    finally {
        try {
            if (_d && !_d.done && (_b = _c["return"])) _b.call(_c);
        }
        finally { if (e_2) throw e_2.error; }
    }
    return obj;
}
document.querySelectorAll(".state-data").forEach(function (el) {
    var obj = removeCardNulls(JSON.parse(el.textContent), false);
    var btn = document.createElement("button");
    btn.textContent = "Log to console";
    btn.addEventListener("click", function (e) {
        e.preventDefault();
        console.log(obj);
    });
    el.parentNode.insertBefore(btn, el);
    el.textContent = JSON.stringify(obj, null, "\t");
});
document.querySelectorAll(".dev-comment").forEach(function (el) {
    el.addEventListener("input", function () {
        el.parentElement.style.backgroundColor = "#ff0";
    });
});
