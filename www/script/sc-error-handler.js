"use strict";
addEventListener("error", function (e) {
    if (SpyCards.disableErrorHandler) {
        return;
    }
    if (e.error && e.error.message === "Go program has already exited") {
        // don't mask real error
        return;
    }
    if (!e.error && e.target instanceof HTMLScriptElement) {
        var scriptName = e.target.src.replace(/^[^\?]*\/|\?.*$/g, "");
        console.debug("TODO: error screen for script that failed to load:", scriptName);
    }
    else if (e.error) {
        errorHandler(e.error);
    }
    else if (e.message && e.lineno) {
        errorHandler(new Error(e.message + " at " + e.filename + ":" + e.lineno + ":" + e.colno));
    }
    else {
        console.error("unhandled type of ErrorEvent", e);
        debugger;
    }
}, true);
var SpyCards;
(function (SpyCards) {
    // prevent keyboard inputs from being eaten
    SpyCards.disableKeyboard = false;
    SpyCards.disableErrorHandler = false;
    SpyCards.serviceWorkerVersion = "";
    fetch("/service-worker-status").then(function (r) { return r.text(); }).then(function (v) { return SpyCards.serviceWorkerVersion = v; });
    SpyCards.errLogBuf = [];
    SpyCards.crashCleanup = [];
})(SpyCards || (SpyCards = {}));
function errorHandler(ex) {
    SpyCards.crashCleanup.filter(function (f) {
        try {
            f();
        }
        catch (ex) {
            debugger;
        }
        return false;
    });
    console.error("Error handler triggered:", ex, ex.goStack || ex.stack);
    debugger;
    if (document.documentElement.classList.contains("fatal-error")) {
        return;
    }
    SpyCards.disableKeyboard = true;
    var form = document.createElement("form");
    form.classList.add("error-report");
    var h1 = document.createElement("h1");
    h1.textContent = "Uh oh!";
    form.appendChild(h1);
    var flavor = document.createElement("p");
    flavor.classList.add("flavor");
    flavor.textContent = "It looks like Spy Cards Online crashed due to a bug! But that's impossible because Spy Cards Online contains no... oh.";
    form.appendChild(flavor);
    var message = document.createElement("p");
    message.classList.add("message");
    if (/^PANIC:/.test(ex.message)) {
        message.textContent = ex.message;
    }
    else {
        message.textContent = "Error: " + ex.message;
    }
    form.appendChild(message);
    var h2 = document.createElement("h2");
    h2.textContent = "Report Error";
    form.appendChild(h2);
    var commentField = document.createElement("textarea");
    commentField.name = "u";
    var commentLabel = document.createElement("label");
    commentLabel.textContent = "What happened right before the crash?";
    commentLabel.appendChild(commentField);
    form.appendChild(commentLabel);
    var submit = document.createElement("button");
    submit.type = "submit";
    submit.textContent = "Submit Report";
    form.appendChild(submit);
    var disclaimer = document.createElement("p");
    disclaimer.classList.add("disclaimer");
    disclaimer.textContent = "Submitting this form will send the following data:";
    form.appendChild(disclaimer);
    var dataList = document.createElement("ul");
    dataList.classList.add("disclaimer");
    function addItem(text) {
        var item = document.createElement("li");
        item.textContent = text;
        dataList.appendChild(item);
    }
    addItem("The version of Spy Cards Online (" + spyCardsVersion + ")");
    addItem("The error message (" + ex.message + ")");
    addItem("The location in the code where the error occurred");
    if (location.hash && location.hash.length > 1) {
        addItem("Custom card definitions (from the address bar)");
    }
    if (SpyCards.errLogBuf.length) {
        addItem("The last " + SpyCards.errLogBuf.length + " lines from the game log");
    }
    addItem("Your comment entered above (if any)");
    form.appendChild(dataList);
    var isAlertError = location.search.indexOf("alertError") !== -1;
    var onSubmit = function (e) {
        e.preventDefault();
        submit.disabled = true;
        submit.textContent = "Sending...";
        var data = new FormData(form);
        data.append("v", spyCardsVersion);
        data.append("v2", SpyCards.serviceWorkerVersion);
        data.append("m", ex.message);
        data.append("t", ex.goStack || ex.stack);
        data.append("s", JSON.stringify({
            h: location.host,
            p: location.pathname,
            s: location.search
        }));
        data.append("c", location.hash ? location.hash.substr(1) : "");
        data.append("l", SpyCards.errLogBuf.join("\n"));
        function closeForm(message) {
            form.textContent = "";
            var thanks = document.createElement("p");
            thanks.classList.add("thanks");
            thanks.textContent = message;
            form.appendChild(thanks);
        }
        var xhr = new XMLHttpRequest();
        xhr.open("POST", issue_report_handler, !isAlertError);
        xhr.addEventListener("load", function () {
            if (xhr.status === 202) {
                closeForm("Report submitted. Thanks for helping to make Spy Cards Online a little less buggy. Or more buggy.");
                return;
            }
            closeForm("Form submission failed due to an error. Oh, the irony! (remote code " + xhr.status + ")");
        });
        xhr.addEventListener("error", function () {
            closeForm("Form submission failed due to an error. Oh, the irony! (local code " + xhr.status + ")");
        });
        xhr.send(data);
    };
    form.addEventListener("submit", onSubmit);
    if (isAlertError) {
        onSubmit(new Event("submit"));
        alert(ex.goStack || ex.stack);
    }
    document.documentElement.classList.add("fatal-error");
    document.body.appendChild(form);
    document.scrollingElement.scrollTop = 0;
}
if ("serviceWorker" in navigator) {
    if (navigator.serviceWorker.controller) {
        navigator.serviceWorker.addEventListener("controllerchange", function () {
            var updateDialog = document.createElement("div");
            updateDialog.classList.add("update-available");
            updateDialog.setAttribute("role", "alertdialog");
            var updateTitle = document.createElement("h1");
            updateTitle.id = "service-worker-update-available-title";
            updateDialog.setAttribute("aria-labelledby", updateTitle.id);
            updateTitle.textContent = "Update Available";
            updateDialog.appendChild(updateTitle);
            var updateMessage = document.createElement("p");
            updateMessage.id = "service-worker-update-available-message";
            updateDialog.setAttribute("aria-describedby", updateMessage.id);
            updateMessage.textContent = "An update to Spy Cards Online is available.";
            updateDialog.appendChild(updateMessage);
            var updateAccept = document.createElement("button");
            updateAccept.textContent = "Apply Now";
            updateAccept.addEventListener("click", function (e) {
                e.preventDefault();
                location.reload();
            });
            updateDialog.appendChild(updateAccept);
            var updateDecline = document.createElement("button");
            updateDecline.textContent = "Ignore";
            updateDecline.addEventListener("click", function (e) {
                e.preventDefault();
                if (updateDialog.parentNode) {
                    updateDialog.parentNode.removeChild(updateDialog);
                }
            });
            updateDialog.appendChild(updateDecline);
            document.querySelectorAll(".update-available").forEach(function (el) {
                if (el.parentNode) {
                    el.parentNode.removeChild(el);
                }
            });
            var showUpdateDialog = function () {
                document.body.appendChild(updateDialog);
                updateAccept.focus();
            };
            if (document.documentElement.classList.contains("active")) {
                var matchWaiter_1 = setInterval(function () {
                    if (document.documentElement.classList.contains("active")) {
                        return;
                    }
                    clearInterval(matchWaiter_1);
                    showUpdateDialog();
                }, 1000);
            }
            else {
                showUpdateDialog();
            }
        });
    }
    var hadController_1 = !!navigator.serviceWorker.controller;
    var registerSW = function () {
        navigator.serviceWorker.register("/service-worker.js").then(function (reg) {
            reg.addEventListener("updatefound", function (e) {
                console.log("serviceworker update available");
                if (!hadController_1) {
                    // we didn't load via a serviceworker, so we don't need to tell the user
                    return;
                }
                var updateDialog = document.createElement("div");
                updateDialog.classList.add("update-available");
                updateDialog.setAttribute("role", "alert");
                var updateTitle = document.createElement("h1");
                updateTitle.id = "service-worker-update-found-title";
                updateDialog.setAttribute("aria-labelledby", updateTitle.id);
                updateTitle.textContent = "Update Found";
                updateDialog.appendChild(updateTitle);
                var updateMessage = document.createElement("p");
                updateMessage.id = "service-worker-update-found-message";
                updateDialog.setAttribute("aria-describedby", updateMessage.id);
                updateMessage.textContent = "An update to Spy Cards Online is being downloaded…";
                updateDialog.appendChild(updateMessage);
                var progress = document.createElement("progress");
                progress.value = 0;
                progress.max = 1;
                updateDialog.appendChild(progress);
                document.body.appendChild(updateDialog);
            });
            var sw;
            if (reg.installing) {
                sw = reg.installing;
            }
            else if (reg.waiting) {
                sw = reg.waiting;
            }
            else if (reg.active) {
                sw = reg.active;
            }
            if (sw) {
                console.log("serviceworker initial state", sw.state);
                sw.addEventListener("statechange", function (e) {
                    console.log("serviceworker state change", sw.state);
                });
            }
        })["catch"](function (err) {
            console.info("registering ServiceWorker rejected:", err);
        });
    };
    if (location.pathname.indexOf("/game") === 0 || !window.requestIdleCallback) {
        registerSW();
    }
    else {
        requestIdleCallback(registerSW);
    }
    navigator.serviceWorker.addEventListener("message", function (e) {
        switch (e.data.type) {
            case "settings-changed":
                window.dispatchEvent(new Event("spy-cards-settings-changed"));
                break;
            case "update-progress":
                var progress = document.querySelector(".update-available progress");
                if (progress) {
                    progress.value = e.data.current;
                    progress.max = e.data.total;
                }
                break;
            case "update-error":
                console.error("service worker update failed with error: " + e.data.message);
                document.querySelectorAll(".update-available").forEach(function (el) {
                    if (el.parentNode) {
                        el.parentNode.removeChild(el);
                    }
                });
                break;
            default:
                debugger;
                break;
        }
    });
    if (navigator.serviceWorker.startMessages) {
        navigator.serviceWorker.startMessages();
    }
}
