"use strict";
var SpyCards;
(function (SpyCards) {
    var SpoilerGuard;
    (function (SpoilerGuard) {
        var _a;
        var cardEnemyIDs = [
            0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 14, 15, 16, 17, 19, 20, 21, 23, 24, 25,
            26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43,
            44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 61, 63, 64,
            65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82,
            83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 94, 95, 96, 97, 98, 111, 112,
        ];
        var encCards = {
            "ȮȬ": 0, "ȭȭ": 1, "ȭȬ": 2, "ȯ": 3,
            "ȫȭ": 4, "ȫȬ": 5, "Ȯ": 6, "ȫȩ": 7,
            "Ȯȧ": 8, "Ȫȧ": 9, "Ȯȯ": 10, "Ȭȭ": 11,
            "ȮȮ": 12, "Ȯȭ": 13, "ȭȫ": 14, "ȨȮ": 15,
            "ȭȦ": 16, "ȭȨ": 17, "ȫȨ": 18, "ȫȦ": 19,
            "ȫȧ": 20, "ȪȦ": 21, "ȪȮ": 22, "Ȫȭ": 23,
            "ȨȬ": 24, "Ȩȭ": 25, "Ȩȯ": 26, "ȩȨ": 27,
            "ȩȧ": 28, "ȬȮ": 29, "ȭȯ": 30, "ȮȪ": 31,
            "Ȧ": 32, "ȧȦ": 33, "ȧȧ": 34, "ȧ": 35,
            "Ȭȯ": 36, "Ȫȩ": 37, "Ȯȫ": 38, "ȩȦ": 39,
            "ȭȮ": 40, "ȩȭ": 41, "Ȯȩ": 42, "ȭȪ": 43,
            "Ȭȫ": 44, "ȪȬ": 45, "Ȫȫ": 46, "ȪȪ": 47,
            "ȧȩ": 48, "ȧȨ": 49, "ȮȨ": 50, "ȬȬ": 0,
            "ȩ": 1, "Ȫ": 2, "ȫ": 3, "ȬȨ": 4,
            "Ȭȧ": 5, "ȩȪ": 6, "ȩȬ": 7, "ȩȫ": 8,
            "ȩȯ": 9, "Ȩȫ": 10, "ȨȪ": 11, "Ȩ": 12,
            "ȮȦ": 13, "ȫȮ": 14, "ȭȧ": 15, "ȧȫ": 16,
            "ȧȮ": 17, "ȧȭ": 18, "Ȩȩ": 19, "ȨȨ": 20,
            "Ȩȧ": 21, "ȭ": 0, "Ȭ": 1, "ȬȪ": 2,
            "ȫȫ": 3, "ȪȨ": 4, "ȩȮ": 5, "ȭȩ": 6,
            "Ȭȩ": 7, "ȧȬ": 8, "ȫȯ": 9, "ȬȦ": 10,
            "ȫȪ": 11, "ȧȪ": 12, "ȩȩ": 13, "ȧȯ": 14,
            "Ȫȯ": 15, "ȨȦ": 16, "ȮȮȮ": 22, "ȮȮȭ": 23
        };
        var sep0 = "ȳ";
        var sep1 = "ɟ";
        var sep2 = "ȕ";
        var sep3 = "ɣɌɏɓɖɋɣ";
        var nilval = "ȯ";
        var questID = "ȩ";
        function flagMap(s) {
            return s.split(sep0).map(function (b) { return !(b.length & 1); });
        }
        function getSpoilerGuardData() {
            var val = localStorage["spy-cards-spoiler-guard-v0"];
            return val ? JSON.parse(val) : null;
        }
        SpoilerGuard.getSpoilerGuardData = getSpoilerGuardData;
        var GuardState;
        (function (GuardState) {
            GuardState[GuardState["QuestLocked"] = 0] = "QuestLocked";
            GuardState[GuardState["QuestNotAccepted"] = 1] = "QuestNotAccepted";
            GuardState[GuardState["QuestNotCompleted"] = 2] = "QuestNotCompleted";
            GuardState[GuardState["NotMetCarmina"] = 3] = "NotMetCarmina";
            GuardState[GuardState["CardsNotApproved"] = 4] = "CardsNotApproved";
            GuardState[GuardState["NotAllSeen"] = 5] = "NotAllSeen";
            GuardState[GuardState["NotAllSpied"] = 6] = "NotAllSpied";
            GuardState[GuardState["Disabled"] = 7] = "Disabled";
        })(GuardState = SpoilerGuard.GuardState || (SpoilerGuard.GuardState = {}));
        var cannotPlayState = (_a = {},
            _a[GuardState.QuestLocked] = true,
            _a[GuardState.QuestNotAccepted] = true,
            _a[GuardState.QuestNotCompleted] = true,
            _a[GuardState.NotMetCarmina] = true,
            _a[GuardState.CardsNotApproved] = true,
            _a);
        function getSpoilerGuardState() {
            var save = getSpoilerGuardData();
            if (!save) {
                return GuardState.Disabled;
            }
            if (save.q !== 2) {
                return [
                    GuardState.QuestLocked,
                    GuardState.QuestNotAccepted,
                    GuardState.QuestNotCompleted
                ][save.q + 1];
            }
            if (!save.t) {
                return GuardState.NotMetCarmina;
            }
            if (!save.a) {
                return GuardState.CardsNotApproved;
            }
            var seenAllEnemies = true;
            var spiedAllEnemies = true;
            var enemyBitmap = Base64.decode(save.s);
            for (var i = 0; i < cardEnemyIDs.length; i++) {
                var card = cardEnemyIDs[i];
                var j = card >> 2;
                var k = (card & 3) << 1;
                if (!(enemyBitmap[j] & (1 << k))) {
                    seenAllEnemies = false;
                }
                if (!(enemyBitmap[j] & (2 << k))) {
                    spiedAllEnemies = false;
                }
            }
            if (!seenAllEnemies) {
                return GuardState.NotAllSeen;
            }
            if (!spiedAllEnemies) {
                return GuardState.NotAllSpied;
            }
            return GuardState.Disabled;
        }
        SpoilerGuard.getSpoilerGuardState = getSpoilerGuardState;
        function surveySaveData(form) {
            function ask(question, options) {
                var p = document.createElement("p");
                p.textContent = question;
                form.appendChild(p);
                var buttons = document.createElement("div");
                buttons.classList.add("buttons");
                form.appendChild(buttons);
                if (!options) {
                    options = ["Yes", "No"];
                }
                var optionPromises = options.map(function (opt, i) {
                    return new Promise(function (resolve) {
                        var btn = SpyCards.UI.button(opt, [], function () { return resolve(i); });
                        buttons.appendChild(btn);
                    });
                });
                return Promise.race(optionPromises).then(function (response) {
                    SpyCards.UI.remove(p);
                    SpyCards.UI.remove(buttons);
                    return response;
                });
            }
            return ask("Which do you want to do?", ["Upload Save File", "Answer Some Questions"]).then(function (wantSurvey) {
                if (!wantSurvey) {
                    return null;
                }
                var chapterNumber;
                var questCompleted;
                var enemyData = new Uint8Array(256 / 8);
                function seenSpied(id) {
                    enemyData[id >> 2] |= 3 << ((id & 3) << 1);
                }
                return ask("What is your current chapter number?", ["1", "2", "3", "4", "5", "6", "7"]).then(function (num) {
                    chapterNumber = num + 1;
                    if (chapterNumber <= 2) {
                        return 1;
                    }
                    return ask("Have you completed the sidequest \"Requesting Assistance\"?");
                }).then(function (quest) {
                    questCompleted = !quest;
                    if (questCompleted) {
                        seenSpied(31); // Monsieur Scarlet
                    }
                    if (chapterNumber < 1) {
                        return 1;
                    }
                    // chapter 1
                    seenSpied(0); // Zombiant
                    seenSpied(1); // Jellyshroom
                    seenSpied(2); // Spider
                    seenSpied(8); // Inichas
                    seenSpied(9); // Seedling
                    if (chapterNumber < 2) {
                        return 1;
                    }
                    return chapterNumber > 2 ? 0 : ask("Have you reached Golden Settlement?");
                }).then(function (answer) {
                    if (answer) {
                        return 1;
                    }
                    seenSpied(14); // Numbnail
                    seenSpied(16); // Acornling
                    seenSpied(17); // Weevil
                    seenSpied(20); // Chomper
                    seenSpied(25); // Wasp Trooper
                    seenSpied(29); // Midge
                    seenSpied(30); // Underling
                    seenSpied(32); // Golden Seedling
                    return chapterNumber > 2 ? 0 : ask("Have you gained passage to Golden Hills?");
                }).then(function (answer) {
                    if (answer) {
                        return 1;
                    }
                    seenSpied(19); // Venus' Bud
                    seenSpied(21); // Acolyte Aria
                    if (chapterNumber > 2) {
                        seenSpied(3); // Zasp
                        seenSpied(15); // Mothiva
                        seenSpied(24); // Venus' Guardian
                    }
                    if (chapterNumber < 3) {
                        return 1;
                    }
                    return chapterNumber > 3 ? 0 : ask("Have you reached Defiant Root?");
                }).then(function (answer) {
                    if (answer) {
                        return 1;
                    }
                    seenSpied(4); // Cactiling
                    seenSpied(5); // Psicorp
                    seenSpied(6); // Thief
                    seenSpied(7); // Bandit
                    seenSpied(28); // Wasp Scout
                    seenSpied(33); // Arrow Worm
                    seenSpied(39); // Burglar
                    return chapterNumber > 3 ? 0 : ask("Have you found the Overseer?");
                }).then(function (answer) {
                    if (answer) {
                        return 1;
                    }
                    seenSpied(42); // Ahoneynation
                    seenSpied(43); // Bee-Boop
                    seenSpied(44); // Security Turret
                    seenSpied(45); // Denmuki
                    seenSpied(48); // Abomihoney
                    if (chapterNumber > 3) {
                        seenSpied(46); // Heavy Drone B-33
                        seenSpied(34); // Carmina
                        seenSpied(36); // Broodmother
                        seenSpied(47); // Mender
                    }
                    if (chapterNumber < 4) {
                        return 1;
                    }
                    return chapterNumber > 4 ? 0 : ask("Have you obtained the Earth Key?");
                }).then(function (answer) {
                    if (answer) {
                        return 1;
                    }
                    seenSpied(40); // Astotheles
                    return chapterNumber > 4 ? 0 : ask("Have you reached the sand castle?");
                }).then(function (answer) {
                    if (answer) {
                        return 1;
                    }
                    seenSpied(49); // Dune Scorpion
                    seenSpied(81); // Belostoss
                    seenSpied(83); // Water Strider
                    seenSpied(84); // Diving Spider
                    return chapterNumber > 4 ? 0 : ask("Have you obtained the fourth artifact?");
                }).then(function (answer) {
                    if (answer) {
                        return 1;
                    }
                    seenSpied(54); // The Watcher
                    seenSpied(57); // Krawler
                    seenSpied(58); // Haunted Cloth
                    seenSpied(61); // Warden
                    if (chapterNumber > 4) {
                        seenSpied(23); // Kabbu
                        seenSpied(51); // Kali
                        seenSpied(85); // Cenn
                        seenSpied(86); // Pisci
                    }
                    return ask("Have you discovered a use for the machine in Professor Honeycomb's office?");
                }).then(function (answer) {
                    if (!answer) {
                        seenSpied(41); // Mother Chomper
                        seenSpied(70); // Chomper Brute
                    }
                    if (chapterNumber < 5) {
                        return 1;
                    }
                    return chapterNumber > 5 ? 0 : ask("Have you reached the swamp?");
                }).then(function (answer) {
                    if (answer) {
                        return 1;
                    }
                    seenSpied(38); // Flowerling
                    seenSpied(63); // Jumping Spider
                    seenSpied(71); // Mantidfly
                    seenSpied(73); // Wild Chomper
                    return chapterNumber > 5 ? 0 : ask("Have you reached the Wasp Kingdom?");
                }).then(function (answer) {
                    if (answer) {
                        return;
                    }
                    seenSpied(65); // Leafbug Ninja
                    seenSpied(66); // Leafbug Archer
                    seenSpied(67); // Leafbug Clubber
                    seenSpied(68); // Madesphy
                    seenSpied(69); // The Beast
                    if (chapterNumber > 5) {
                        seenSpied(26); // Wasp Bomber
                        seenSpied(27); // Wasp Driller
                        seenSpied(72); // General Ultimax
                        seenSpied(97); // Riz
                    }
                }).then(function () {
                    return chapterNumber < 5 ? 1 : ask("Have you discovered a use for the gem dropped by The Watcher?");
                }).then(function (answer) {
                    if (!answer) {
                        seenSpied(52); // Zombee
                        seenSpied(53); // Zombeetle
                        seenSpied(56); // Bloatshroom
                        seenSpied(96); // Zommoth
                    }
                    if (chapterNumber < 6) {
                        return 1;
                    }
                    return chapterNumber > 6 ? 0 : ask("Have you crossed the Forsaken Lands?");
                }).then(function (answer) {
                    if (answer) {
                        return 1;
                    }
                    seenSpied(37); // Plumpling
                    seenSpied(64); // Mimic Spider
                    seenSpied(78); // Mothfly
                    seenSpied(79); // Mothfly Cluster
                    seenSpied(80); // Ironnail
                    return chapterNumber > 6 ? 0 : ask("Do you have the boat?");
                }).then(function (answer) {
                    if (answer) {
                        return 1;
                    }
                    seenSpied(74); // Cross
                    seenSpied(75); // Poi
                    seenSpied(76); // Primal Weevil
                    return chapterNumber > 6 ? 0 : ask("Have you reached Rubber Prison?");
                }).then(function (answer) {
                    if (answer) {
                        return -1;
                    }
                    seenSpied(82); // Ruffian
                    if (chapterNumber > 6) {
                        seenSpied(95); // ULTIMAX Tank
                    }
                    if (chapterNumber < 7) {
                        return -1;
                    }
                    seenSpied(87); // Dead Lander α
                    seenSpied(88); // Dead Lander β
                    seenSpied(89); // Dead Lander γ
                    return chapterNumber > 7 ? 0 : ask("Which is the furthest area you have reached?", ["Dead Lands", "The Machine", "Sapling Plains"]);
                }).then(function (answer) {
                    return answer === 2 ? ask("Has the sapling been destroyed?") : 1;
                }).then(function (answer) {
                    if (answer) {
                        return -1;
                    }
                    seenSpied(90); // Wasp King
                    seenSpied(91); // The Everlasting King
                    return ask("Have you fought Team Maki? Answer yes even if you lost.");
                }).then(function (answer) {
                    if (!answer) {
                        seenSpied(92); // Maki
                        seenSpied(93); // Kina
                        seenSpied(94); // Yin
                    }
                    return answer === -1 ? 1 : ask("Have you fought Team Slacker? Answer yes even if you lost.");
                }).then(function (answer) {
                    if (!answer) {
                        seenSpied(111); // Stratos
                        seenSpied(112); // Delilah
                    }
                    return chapterNumber < 3 ? 1 : ask("Have you completed the following bounty: Devourer", ["Yes", "No"]);
                }).then(function (answer) {
                    if (!answer) {
                        seenSpied(98);
                    }
                    return chapterNumber < 4 ? 1 : ask("Have you completed the following bounty: Tidal Wyrm", ["Yes", "No"]);
                }).then(function (answer) {
                    if (!answer) {
                        seenSpied(50);
                    }
                    return chapterNumber < 5 ? 1 : ask("Have you completed the following bounty: Seedling King", ["Yes", "No"]);
                }).then(function (answer) {
                    if (!answer) {
                        seenSpied(35);
                    }
                    return chapterNumber < 6 ? 1 : ask("Have you completed the following bounty: False Monarch", ["Yes", "No"]);
                }).then(function (answer) {
                    if (!answer) {
                        seenSpied(77);
                    }
                    return chapterNumber < 6 ? 1 : !(enemyData[76 >> 2] & (1 << (76 & 3))) ? 1 : ask("Have you completed the following bounty: Peacock Spider", ["Yes", "No"]);
                }).then(function (answer) {
                    if (!answer) {
                        seenSpied(55);
                    }
                    return {
                        q: chapterNumber <= 2 ? -1 : questCompleted ? 2 : 0,
                        t: questCompleted,
                        a: questCompleted,
                        d: null,
                        s: Base64.encode(enemyData),
                        m: null
                    };
                });
            });
        }
        SpoilerGuard.surveySaveData = surveySaveData;
        function parseSaveData(data) {
            // to avoid giving away how saves are encoded,
            // we're not actually decoding the save,
            // just grabbing the specific data we need.
            var sections = data.split(sep2);
            var flags = flagMap(sections[11]);
            var modes0 = [613, 614, 615, 616, 656, 681];
            var modes1 = modes0.map(function (n, i) { return flags[n] ? 1 << i : 0; });
            var modes2 = modes1.reduce(function (x, y) { return x | y; }, 0);
            var questType = sections[5].split(sep1).map(function (q) { return q.split(sep0); }).findIndex(function (q) { return q.indexOf(questID) !== -1; });
            var deck0 = sections[12].split(sep3)[12];
            var deck1 = deck0.split(sep0).map(function (e) { return encCards[e]; });
            var deck2 = (deck1.length !== 15 || deck1.some(function (c) { return typeof c !== "number"; })) ? null : new Uint8Array(11);
            if (deck2) {
                deck2[0] = (deck1[0] << 2) | (deck1[1] >> 3);
                deck2[1] = (deck1[1] << 5) | deck1[2];
                for (var i_1 = 2, j = 3; j < deck1.length; i_1 += 3, j += 4) {
                    deck2[i_1] = (deck1[j] << 2) | (deck1[j + 1] >> 4);
                    deck2[i_1 + 1] = (deck1[j + 1] << 4) | (deck1[j + 2] >> 2);
                    deck2[i_1 + 2] = (deck1[j + 2] << 6) | deck1[j + 3];
                }
            }
            var spyData = flagMap(sections[10].split(sep1)[1]);
            var enemyData0 = sections[17];
            var enemyData1 = enemyData0.split(sep1);
            var enemyData2 = enemyData1.map(function (e) { return e.split(sep0); });
            var enemyData3 = enemyData2.map(function (e) { return e.map(function (c) { return c !== nilval; }); });
            var enemyData4 = new Uint8Array(256 / 8);
            var i = 0, b = 0;
            for (var j = 0; j < 128; j++) {
                if (enemyData3[j][0]) {
                    enemyData4[i] |= 1 << b;
                }
                b++;
                if (spyData[j]) {
                    enemyData4[i] |= 1 << b;
                }
                b++;
                if (b >= 8) {
                    i++;
                    b = 0;
                }
            }
            if (b || i !== enemyData4.length) {
                throw new Error("invalid data");
            }
            return {
                q: questType,
                t: flags[236],
                a: flags[237],
                d: deck2 ? Base32.Crockford.encode(deck2) : null,
                s: Base64.encode(enemyData4),
                m: modes2
            };
        }
        SpoilerGuard.parseSaveData = parseSaveData;
        function requestDataFile() {
            var upload = document.createElement("input");
            upload.type = "file";
            upload.accept = ".dat";
            var filePromise = new Promise(function (resolve, reject) {
                upload.addEventListener("input", function (e) {
                    if (upload.files.length) {
                        resolve(upload.files[0]);
                    }
                    else {
                        reject(new Error("no file selected"));
                    }
                });
            });
            upload.click();
            return filePromise;
        }
        function onFile(f) {
            if (["save0.dat", "save1.dat", "save2.dat"].indexOf(f.name) === -1) {
                // TODO: confirm user intended this file
                debugger;
            }
            f.text().then(function (data) {
                var parsed = parseSaveData(data);
                localStorage["spy-cards-spoiler-guard-v0"] = JSON.stringify(parsed);
                updateSpoilerGuardState();
            });
        }
        var enableButtons = document.querySelectorAll(".enable-spoiler-guard");
        if (enableButtons.length) {
            document.addEventListener("dragover", function (e) {
                e.preventDefault();
                e.dataTransfer.dropEffect = "copy";
            });
            document.addEventListener("drop", function (e) {
                e.preventDefault();
                onFile(e.dataTransfer.files[0]);
            });
            enableButtons.forEach(function (btn) {
                btn.addEventListener("click", function (e) {
                    e.preventDefault();
                    var form = document.createElement("div");
                    form.classList.add("readme", "spoiler-guard-form");
                    form.setAttribute("role", "form");
                    form.setAttribute("aria-live", "assertive");
                    document.body.appendChild(form);
                    surveySaveData(form).then(function (surveyData) {
                        SpyCards.UI.remove(form);
                        if (surveyData === null) {
                            requestDataFile().then(function (f) { return onFile(f); });
                        }
                        else {
                            localStorage["spy-cards-spoiler-guard-v0"] = JSON.stringify(surveyData);
                            updateSpoilerGuardState();
                        }
                    });
                });
            });
        }
        var disableButton = document.querySelector(".disable-spoiler-guard");
        if (disableButton) {
            disableButton.addEventListener("click", function (e) {
                e.preventDefault();
                delete localStorage["spy-cards-spoiler-guard-v0"];
                updateSpoilerGuardState();
            });
        }
        else if (cannotPlayState[getSpoilerGuardState()]) {
            location.href = "spoiler-guard.html";
        }
        function updateSpoilerGuardState() {
            var data = getSpoilerGuardData();
            var isEnabled = data !== null;
            var state = getSpoilerGuardState();
            document.documentElement.classList.toggle("room-pr", data && data.m && (data.m & 8) !== 0);
            document.querySelectorAll(".spoiler-guard-disabled, .spoiler-guard-enabled, [data-spoiler-guard-state]").forEach(function (el) {
                el.hidden = false;
                if (el.classList.contains("spoiler-guard-disabled")) {
                    el.hidden = isEnabled;
                }
                if (el.classList.contains("spoiler-guard-enabled")) {
                    el.hidden = !isEnabled;
                }
                if (el.hidden) {
                    return;
                }
                var stateRaw = el.getAttribute("data-spoiler-guard-state");
                if (!stateRaw) {
                    return;
                }
                var expectedState = parseInt(stateRaw, 10);
                el.hidden = expectedState !== state;
            });
            document.querySelectorAll(".spoiler-guard-deck").forEach(function (el) {
                if (!data || !data.d) {
                    el.hidden = true;
                    return;
                }
                el.hidden = false;
                el.querySelectorAll("a").forEach(function (a) {
                    a.href = "/game/deck/vanilla/" + data.d;
                });
            });
        }
        setTimeout(updateSpoilerGuardState, 1);
    })(SpoilerGuard = SpyCards.SpoilerGuard || (SpyCards.SpoilerGuard = {}));
})(SpyCards || (SpyCards = {}));
