"use strict";
var Base32;
(function (Base32) {
    function encode32(alphabet, src) {
        var dst = [];
        var bits = 0, n = 0;
        for (var i = 0; i < src.length; i++) {
            n <<= 8;
            n |= src[i];
            bits += 8;
            while (bits >= 5) {
                bits -= 5;
                dst.push(alphabet.charAt(n >> bits));
                n &= (1 << bits) - 1;
            }
        }
        if (bits) {
            while (bits < 5) {
                n <<= 1;
                bits++;
            }
            dst.push(alphabet.charAt(n));
        }
        return dst.join("");
    }
    function decode32(alphabet, src) {
        var dst = new Uint8Array(Math.floor(src.length / 8 * 5));
        var bits = 0, n = 0;
        for (var i = 0, j = 0; i < src.length; i++) {
            var k = alphabet.indexOf(src.charAt(i));
            if (k === -1) {
                throw new Error("invalid character in string");
            }
            n <<= 5;
            n |= k;
            bits += 5;
            while (bits >= 8) {
                bits -= 8;
                dst[j++] = n >> bits;
                n &= (1 << bits) - 1;
            }
        }
        return dst;
    }
    /**
     * Implementation of Crockford Base 32
     * https://www.crockford.com/base32.html
     */
    var Crockford;
    (function (Crockford) {
        var alphabet = "0123456789ABCDEFGHJKMNPQRSTVWXYZ";
        function normalize(s) {
            s = s.toUpperCase();
            s = s.replace(/O/g, "0");
            s = s.replace(/[IL]/g, "1");
            s = s.replace(/[ \-_]/g, "");
            return s;
        }
        function encode(src) {
            return encode32(alphabet, src);
        }
        Crockford.encode = encode;
        function decode(src) {
            src = normalize(src);
            return decode32(alphabet, src);
        }
        Crockford.decode = decode;
    })(Crockford = Base32.Crockford || (Base32.Crockford = {}));
    var IPFS;
    (function (IPFS) {
        var alphabet = "abcdefghijklmnopqrstuvwxyz234567";
        function encode(src) {
            return encode32(alphabet, src);
        }
        IPFS.encode = encode;
        function decode(src) {
            return decode32(alphabet, src);
        }
        IPFS.decode = decode;
    })(IPFS = Base32.IPFS || (Base32.IPFS = {}));
})(Base32 || (Base32 = {}));
// Because atob and btoa are weird, fiddly, and don't properly support nul bytes.
var Base64;
(function (Base64) {
    var alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
    function encode(src) {
        var dst = [];
        var bits = 0, n = 0;
        for (var i = 0; i < src.length; i++) {
            n <<= 8;
            n |= src[i];
            bits += 8;
            while (bits >= 6) {
                bits -= 6;
                dst.push(alphabet.charAt(n >> bits));
                n &= (1 << bits) - 1;
            }
        }
        if (bits) {
            while (bits < 6) {
                n <<= 1;
                bits++;
            }
            dst.push(alphabet.charAt(n));
        }
        while (dst.length % 4) {
            dst.push("=");
        }
        return dst.join("");
    }
    Base64.encode = encode;
    function decode(src) {
        src = src.replace(/[=]+$/, "");
        var dst = new Uint8Array(Math.floor(src.length / 8 * 6));
        var bits = 0, n = 0;
        for (var i = 0, j = 0; i < src.length; i++) {
            var k = alphabet.indexOf(src.charAt(i));
            if (k === -1) {
                throw new Error("invalid character in string");
            }
            n <<= 6;
            n |= k;
            bits += 6;
            while (bits >= 8) {
                bits -= 8;
                dst[j++] = n >> bits;
                n &= (1 << bits) - 1;
            }
        }
        return dst;
    }
    Base64.decode = decode;
})(Base64 || (Base64 = {}));
