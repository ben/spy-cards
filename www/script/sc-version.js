"use strict";
var spyCardsVersionPrefix = "0.3.27";
var spyCardsClientMode = location.pathname === "/game.html" ? new URLSearchParams(location.search).get("mode") : "";
var spyCardsVersionSuffix = (function () {
    var path = location.pathname.substr(1);
    if (spyCardsClientMode) {
        return "-" + spyCardsClientMode;
    }
    if (path.substr(path.length - ".html".length) === ".html") {
        return "-" + path.substr(0, path.length - 5);
    }
    return "";
})();
var spyCardsVersion = spyCardsVersionPrefix + spyCardsVersionSuffix;
var spyCardsVersionVariableSuffix = "";
