"use strict"
var SpyCards;(function(SpyCards){var Audio;(function(Audio){if(window.AudioContext){Audio.actx=new AudioContext}else if(window.webkitAudioContext){Audio.actx=new window.webkitAudioContext}var soundsGain
var t0=Audio.actx&&Audio.actx.currentTime
if(t0!==t0){Audio.actx=null}Audio.soundsVolume=0
Audio.musicVolume=0
var fetchAudioPromise=new Promise((function(resolve){return Audio.startFetchAudio=Audio.actx?resolve:function(){}})).then((function(){var settings=SpyCards.loadSettings()
Audio.soundsVolume=settings.audio.sounds
Audio.musicVolume=settings.audio.music
soundsGain=Audio.actx.createGain()
soundsGain.connect(Audio.actx.destination)
soundsGain.gain.setValueAtTime(Audio.soundsVolume,Audio.actx.currentTime)
addEventListener("click",(function(){if(Audio.actx.state==="suspended"){Audio.actx.resume()}}),true)}))
function isEnabled(){Audio.startFetchAudio()
return fetchAudioPromise.then((function(){return Audio.soundsVolume>0||Audio.musicVolume>0}))}Audio.isEnabled=isEnabled
function setVolume(music,sounds){Audio.soundsVolume=sounds
Audio.musicVolume=music
var settings=SpyCards.loadSettings()
settings.audio.music=music
settings.audio.sounds=sounds
SpyCards.saveSettings(settings)
if(soundsGain){soundsGain.gain.setValueAtTime(sounds,Audio.actx.currentTime)}}Audio.setVolume=setVolume
addEventListener("spy-cards-settings-changed",(function(){var s=SpyCards.loadSettings().audio
Audio.soundsVolume=s.sounds
Audio.musicVolume=s.music
if(soundsGain){soundsGain.gain.setValueAtTime(Audio.soundsVolume,Audio.actx.currentTime)}}))
function fetchAudio(name){return fetchAudioPromise.then((function(){return fetch(name.indexOf("/")===-1?"/audio/"+name+".opus":name)})).then((function(response){return response.arrayBuffer()})).then((function(body){return(Audio.actx.state==="suspended"?Audio.actx.resume():Promise.resolve()).then((function(){return body}))})).then((function(body){if(Audio.actx.decodeAudioData.length>1){return new Promise((function(resolve,reject){var w=new Worker("/script/opus-decoder-worker.js",{name:"opus-decoder"})
w.onmessage=function(e){w.terminate()
if(e.data.config){var abuf=Audio.actx.createBuffer(e.data.config.numberOfChannels,e.data.config.length,e.data.config.sampleRate)
for(var i=0;i<e.data.channels.length;i++){abuf.getChannelData(i).set(new Float32Array(e.data.channels[i],0),0)}return resolve(abuf)}if(e.data.error){return reject(new Error("audio: opusfile decoding error "+-e.data.error))}reject(new Error("audio: unknown error"))}
w.postMessage({id:0,name:name,buffer:body},[body])}))}return Audio.actx.decodeAudioData(body)}))}var Sound=function(){function Sound(name,pitch,volume){if(pitch===void 0){pitch=1}if(volume===void 0){volume=1}this.name=name
this.buffer=fetchAudio(name)
this.pitch=pitch
this.volume=volume}Sound.prototype.createSource=function(){Audio.startFetchAudio()
var sound=this
return this.buffer.then((function(buffer){var source=Audio.actx.createBufferSource()
source.buffer=buffer
source.playbackRate.setValueAtTime(sound.pitch,Audio.actx.currentTime)
return source}))}
Sound.prototype.preload=function(){Audio.startFetchAudio()
return this.buffer.then((function(){}))}
Sound.prototype.play=function(delay,overridePitch,overrideVolume){if(delay===void 0){delay=0}if(!Audio.soundsVolume){return Promise.resolve()}var sound=this
return this.createSource().then((function(src){var dest=soundsGain
if(sound.volume!==1){if(!sound.gain){sound.gain=Audio.actx.createGain()
sound.gain.connect(dest)
sound.gain.gain.setValueAtTime(sound.volume,Audio.actx.currentTime)}dest=sound.gain}if(overrideVolume){var gain=Audio.actx.createGain()
gain.connect(dest)
gain.gain.setValueAtTime(overrideVolume,Audio.actx.currentTime)
dest=gain}if(overridePitch){src.playbackRate.setValueAtTime(overridePitch*sound.pitch,Audio.actx.currentTime)}src.connect(dest)
src.start(Audio.actx.currentTime+delay)}))}
return Sound}()
Audio.Sound=Sound
Audio.Sounds={Confirm:new Sound("Confirm")}})(Audio=SpyCards.Audio||(SpyCards.Audio={}))})(SpyCards||(SpyCards={}))
"use strict"
var Base32;(function(Base32){function encode32(alphabet,src){var dst=[]
var bits=0,n=0
for(var i=0;i<src.length;i++){n<<=8
n|=src[i]
bits+=8
while(bits>=5){bits-=5
dst.push(alphabet.charAt(n>>bits))
n&=(1<<bits)-1}}if(bits){while(bits<5){n<<=1
bits++}dst.push(alphabet.charAt(n))}return dst.join("")}function decode32(alphabet,src){var dst=new Uint8Array(Math.floor(src.length/8*5))
var bits=0,n=0
for(var i=0,j=0;i<src.length;i++){var k=alphabet.indexOf(src.charAt(i))
if(k===-1){throw new Error("invalid character in string")}n<<=5
n|=k
bits+=5
while(bits>=8){bits-=8
dst[j++]=n>>bits
n&=(1<<bits)-1}}return dst}var Crockford;(function(Crockford){var alphabet="0123456789ABCDEFGHJKMNPQRSTVWXYZ"
function normalize(s){s=s.toUpperCase()
s=s.replace(/O/g,"0")
s=s.replace(/[IL]/g,"1")
s=s.replace(/[ \-_]/g,"")
return s}function encode(src){return encode32(alphabet,src)}Crockford.encode=encode
function decode(src){src=normalize(src)
return decode32(alphabet,src)}Crockford.decode=decode})(Crockford=Base32.Crockford||(Base32.Crockford={}))
var IPFS;(function(IPFS){var alphabet="abcdefghijklmnopqrstuvwxyz234567"
function encode(src){return encode32(alphabet,src)}IPFS.encode=encode
function decode(src){return decode32(alphabet,src)}IPFS.decode=decode})(IPFS=Base32.IPFS||(Base32.IPFS={}))})(Base32||(Base32={}))
var Base64;(function(Base64){var alphabet="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/"
function encode(src){var dst=[]
var bits=0,n=0
for(var i=0;i<src.length;i++){n<<=8
n|=src[i]
bits+=8
while(bits>=6){bits-=6
dst.push(alphabet.charAt(n>>bits))
n&=(1<<bits)-1}}if(bits){while(bits<6){n<<=1
bits++}dst.push(alphabet.charAt(n))}while(dst.length%4){dst.push("=")}return dst.join("")}Base64.encode=encode
function decode(src){src=src.replace(/[=]+$/,"")
var dst=new Uint8Array(Math.floor(src.length/8*6))
var bits=0,n=0
for(var i=0,j=0;i<src.length;i++){var k=alphabet.indexOf(src.charAt(i))
if(k===-1){throw new Error("invalid character in string")}n<<=6
n|=k
bits+=6
while(bits>=8){bits-=8
dst[j++]=n>>bits
n&=(1<<bits)-1}}return dst}Base64.decode=decode})(Base64||(Base64={}))
"use strict"
addEventListener("error",(function(e){if(SpyCards.disableErrorHandler){return}if(e.error&&e.error.message==="Go program has already exited"){return}if(!e.error&&e.target instanceof HTMLScriptElement){var scriptName=e.target.src.replace(/^[^\?]*\/|\?.*$/g,"")
console.debug("TODO: error screen for script that failed to load:",scriptName)}else if(e.error){errorHandler(e.error)}else if(e.message&&e.lineno){errorHandler(new Error(e.message+" at "+e.filename+":"+e.lineno+":"+e.colno))}else{console.error("unhandled type of ErrorEvent",e)
debugger}}),true)
var SpyCards;(function(SpyCards){SpyCards.disableKeyboard=false
SpyCards.disableErrorHandler=false
SpyCards.serviceWorkerVersion=""
fetch("/service-worker-status").then((function(r){return r.text()})).then((function(v){return SpyCards.serviceWorkerVersion=v}))
SpyCards.errLogBuf=[]
SpyCards.crashCleanup=[]})(SpyCards||(SpyCards={}))
function errorHandler(ex){SpyCards.crashCleanup.filter((function(f){try{f()}catch(ex){debugger}return false}))
console.error("Error handler triggered:",ex,ex.goStack||ex.stack)
debugger
if(document.documentElement.classList.contains("fatal-error")){return}SpyCards.disableKeyboard=true
var form=document.createElement("form")
form.classList.add("error-report")
var h1=document.createElement("h1")
h1.textContent="Uh oh!"
form.appendChild(h1)
var flavor=document.createElement("p")
flavor.classList.add("flavor")
flavor.textContent="It looks like Spy Cards Online crashed due to a bug! But that's impossible because Spy Cards Online contains no... oh."
form.appendChild(flavor)
var message=document.createElement("p")
message.classList.add("message")
if(/^PANIC:/.test(ex.message)){message.textContent=ex.message}else{message.textContent="Error: "+ex.message}form.appendChild(message)
var h2=document.createElement("h2")
h2.textContent="Report Error"
form.appendChild(h2)
var commentField=document.createElement("textarea")
commentField.name="u"
var commentLabel=document.createElement("label")
commentLabel.textContent="What happened right before the crash?"
commentLabel.appendChild(commentField)
form.appendChild(commentLabel)
var submit=document.createElement("button")
submit.type="submit"
submit.textContent="Submit Report"
form.appendChild(submit)
var disclaimer=document.createElement("p")
disclaimer.classList.add("disclaimer")
disclaimer.textContent="Submitting this form will send the following data:"
form.appendChild(disclaimer)
var dataList=document.createElement("ul")
dataList.classList.add("disclaimer")
function addItem(text){var item=document.createElement("li")
item.textContent=text
dataList.appendChild(item)}addItem("The version of Spy Cards Online ("+spyCardsVersion+")")
addItem("The error message ("+ex.message+")")
addItem("The location in the code where the error occurred")
if(location.hash&&location.hash.length>1){addItem("Custom card definitions (from the address bar)")}if(SpyCards.errLogBuf.length){addItem("The last "+SpyCards.errLogBuf.length+" lines from the game log")}addItem("Your comment entered above (if any)")
form.appendChild(dataList)
var isAlertError=location.search.indexOf("alertError")!==-1
var onSubmit=function(e){e.preventDefault()
submit.disabled=true
submit.textContent="Sending..."
var data=new FormData(form)
data.append("v",spyCardsVersion)
data.append("v2",SpyCards.serviceWorkerVersion)
data.append("m",ex.message)
data.append("t",ex.goStack||ex.stack)
data.append("s",JSON.stringify({h:location.host,p:location.pathname,s:location.search}))
data.append("c",location.hash?location.hash.substr(1):"")
data.append("l",SpyCards.errLogBuf.join("\n"))
function closeForm(message){form.textContent=""
var thanks=document.createElement("p")
thanks.classList.add("thanks")
thanks.textContent=message
form.appendChild(thanks)}var xhr=new XMLHttpRequest
xhr.open("POST",issue_report_handler,!isAlertError)
xhr.addEventListener("load",(function(){if(xhr.status===202){closeForm("Report submitted. Thanks for helping to make Spy Cards Online a little less buggy. Or more buggy.")
return}closeForm("Form submission failed due to an error. Oh, the irony! (remote code "+xhr.status+")")}))
xhr.addEventListener("error",(function(){closeForm("Form submission failed due to an error. Oh, the irony! (local code "+xhr.status+")")}))
xhr.send(data)}
form.addEventListener("submit",onSubmit)
if(isAlertError){onSubmit(new Event("submit"))
alert(ex.goStack||ex.stack)}document.documentElement.classList.add("fatal-error")
document.body.appendChild(form)
document.scrollingElement.scrollTop=0}if("serviceWorker"in navigator){if(navigator.serviceWorker.controller){navigator.serviceWorker.addEventListener("controllerchange",(function(){var updateDialog=document.createElement("div")
updateDialog.classList.add("update-available")
updateDialog.setAttribute("role","alertdialog")
var updateTitle=document.createElement("h1")
updateTitle.id="service-worker-update-available-title"
updateDialog.setAttribute("aria-labelledby",updateTitle.id)
updateTitle.textContent="Update Available"
updateDialog.appendChild(updateTitle)
var updateMessage=document.createElement("p")
updateMessage.id="service-worker-update-available-message"
updateDialog.setAttribute("aria-describedby",updateMessage.id)
updateMessage.textContent="An update to Spy Cards Online is available."
updateDialog.appendChild(updateMessage)
var updateAccept=document.createElement("button")
updateAccept.textContent="Apply Now"
updateAccept.addEventListener("click",(function(e){e.preventDefault()
location.reload()}))
updateDialog.appendChild(updateAccept)
var updateDecline=document.createElement("button")
updateDecline.textContent="Ignore"
updateDecline.addEventListener("click",(function(e){e.preventDefault()
if(updateDialog.parentNode){updateDialog.parentNode.removeChild(updateDialog)}}))
updateDialog.appendChild(updateDecline)
document.querySelectorAll(".update-available").forEach((function(el){if(el.parentNode){el.parentNode.removeChild(el)}}))
var showUpdateDialog=function(){document.body.appendChild(updateDialog)
updateAccept.focus()}
if(document.documentElement.classList.contains("active")){var matchWaiter_1=setInterval((function(){if(document.documentElement.classList.contains("active")){return}clearInterval(matchWaiter_1)
showUpdateDialog()}),1e3)}else{showUpdateDialog()}}))}var hadController_1=!!navigator.serviceWorker.controller
var registerSW=function(){navigator.serviceWorker.register("/service-worker.js").then((function(reg){reg.addEventListener("updatefound",(function(e){console.log("serviceworker update available")
if(!hadController_1){return}var updateDialog=document.createElement("div")
updateDialog.classList.add("update-available")
updateDialog.setAttribute("role","alert")
var updateTitle=document.createElement("h1")
updateTitle.id="service-worker-update-found-title"
updateDialog.setAttribute("aria-labelledby",updateTitle.id)
updateTitle.textContent="Update Found"
updateDialog.appendChild(updateTitle)
var updateMessage=document.createElement("p")
updateMessage.id="service-worker-update-found-message"
updateDialog.setAttribute("aria-describedby",updateMessage.id)
updateMessage.textContent="An update to Spy Cards Online is being downloaded…"
updateDialog.appendChild(updateMessage)
var progress=document.createElement("progress")
progress.value=0
progress.max=1
updateDialog.appendChild(progress)
document.body.appendChild(updateDialog)}))
var sw
if(reg.installing){sw=reg.installing}else if(reg.waiting){sw=reg.waiting}else if(reg.active){sw=reg.active}if(sw){console.log("serviceworker initial state",sw.state)
sw.addEventListener("statechange",(function(e){console.log("serviceworker state change",sw.state)}))}}))["catch"]((function(err){console.info("registering ServiceWorker rejected:",err)}))}
if(location.pathname.indexOf("/game")===0||!window.requestIdleCallback){registerSW()}else{requestIdleCallback(registerSW)}navigator.serviceWorker.addEventListener("message",(function(e){switch(e.data.type){case"settings-changed":window.dispatchEvent(new Event("spy-cards-settings-changed"))
break
case"update-progress":var progress=document.querySelector(".update-available progress")
if(progress){progress.value=e.data.current
progress.max=e.data.total}break
case"update-error":console.error("service worker update failed with error: "+e.data.message)
document.querySelectorAll(".update-available").forEach((function(el){if(el.parentNode){el.parentNode.removeChild(el)}}))
break
default:debugger
break}}))
if(navigator.serviceWorker.startMessages){navigator.serviceWorker.startMessages()}}"use strict"
document.querySelectorAll("#screenshot-gallery a").forEach((function(el){el.addEventListener("click",(function(e){e.preventDefault()
var big=document.querySelector("#big-screenshot")
big.innerHTML=""
var img=new Image
img.width=1920
img.height=1080
img.crossOrigin="anonymous"
img.src=el.href
img.alt=el.firstChild.alt
img.title=el.firstChild.alt
big.appendChild(img)}))}))
addEventListener("load",(function(){var po=document.getElementById("play-online")
if(po){var versionLink=document.createElement("a")
versionLink.className="version-number"
versionLink.textContent="v"+spyCardsVersionPrefix
versionLink.href="/docs/changelog.html#v"+spyCardsVersionPrefix
po.appendChild(versionLink)}}))
"use strict"
var SpyCards;(function(SpyCards){var _a,_b
var Button;(function(Button){Button[Button["Up"]=0]="Up"
Button[Button["Down"]=1]="Down"
Button[Button["Left"]=2]="Left"
Button[Button["Right"]=3]="Right"
Button[Button["Confirm"]=4]="Confirm"
Button[Button["Cancel"]=5]="Cancel"
Button[Button["Switch"]=6]="Switch"
Button[Button["Toggle"]=7]="Toggle"
Button[Button["Pause"]=8]="Pause"
Button[Button["Help"]=9]="Help"})(Button=SpyCards.Button||(SpyCards.Button={}))
SpyCards.defaultKeyButton=(_a={},_a[Button.Up]="ArrowUp",_a[Button.Down]="ArrowDown",_a[Button.Left]="ArrowLeft",_a[Button.Right]="ArrowRight",_a[Button.Confirm]="KeyC",_a[Button.Cancel]="KeyX",_a[Button.Switch]="KeyZ",_a[Button.Toggle]="KeyV",_a[Button.Pause]="Escape",_a[Button.Help]="Enter",_a)
SpyCards.standardGamepadButton=(_b={},_b[Button.Up]=12,_b[Button.Down]=13,_b[Button.Left]=14,_b[Button.Right]=15,_b[Button.Confirm]=0,_b[Button.Cancel]=1,_b[Button.Switch]=2,_b[Button.Toggle]=3,_b[Button.Pause]=9,_b[Button.Help]=8,_b)
var buttonPressed={}
var buttonWasPressed={}
SpyCards.buttonHeld={}
var keyHeld={}
var lastKeyboard=0
var lastGamepad=0
var inputTicks=0
var ButtonStyle;(function(ButtonStyle){ButtonStyle[ButtonStyle["Keyboard"]=0]="Keyboard"
ButtonStyle[ButtonStyle["GenericGamepad"]=1]="GenericGamepad"})(ButtonStyle=SpyCards.ButtonStyle||(SpyCards.ButtonStyle={}))
var lastGamepadStyle=ButtonStyle.GenericGamepad
function getButtonStyle(){if(!lastGamepad||lastGamepad<lastKeyboard){return ButtonStyle.Keyboard}return lastGamepadStyle}SpyCards.getButtonStyle=getButtonStyle
function saveInputState(){var p={}
var w={}
var h={}
for(var btn=Button.Up;btn<=Button.Help;btn++){p[btn]=buttonPressed[btn]
w[btn]=buttonWasPressed[btn]
h[btn]=SpyCards.buttonHeld[btn]}return{t:inputTicks,p:p,w:w,h:h}}SpyCards.saveInputState=saveInputState
function loadInputState(s){inputTicks=s.t
for(var btn=Button.Up;btn<=Button.Help;btn++){buttonPressed[btn]=s.p[btn]
buttonWasPressed[btn]=s.w[btn]
SpyCards.buttonHeld[btn]=s.h[btn]}}SpyCards.loadInputState=loadInputState
function updateButtons(force){if(force){force(SpyCards.buttonHeld)}else if(SpyCards.updateAIButtons){SpyCards.updateAIButtons(SpyCards.buttonHeld)}else{updateHeldButtons()}inputTicks++
for(var button=Button.Up;button<=Button.Help;button++){if(!SpyCards.buttonHeld[button]){buttonPressed[button]=false
buttonWasPressed[button]=0}else if(buttonWasPressed[button]<inputTicks){buttonWasPressed[button]=Infinity
buttonPressed[button]=true}}}SpyCards.updateButtons=updateButtons
function updateHeldButtons(){var controlsSettings=SpyCards.loadSettings().controls||{}
var gamepadSettings=controlsSettings.gamepad||{}
var keyboardMap=SpyCards.defaultKeyButton
if(controlsSettings.customKB&&controlsSettings.keyboard>0&&controlsSettings.customKB[controlsSettings.keyboard-1]){keyboardMap=controlsSettings.customKB[controlsSettings.keyboard-1].code}var gamepads=navigator.getGamepads()||[]
for(var button=Button.Up;button<=Button.Help;button++){SpyCards.buttonHeld[button]=false
if(keyHeld[keyboardMap[button]]){SpyCards.buttonHeld[button]=true
lastKeyboard=Date.now()}for(var i=0;i<gamepads.length;i++){var gp=gamepads[i]
if(!gp){continue}var mapping=SpyCards.standardGamepadButton
var style=ButtonStyle.GenericGamepad
if(controlsSettings.customGP&&gamepadSettings[gp.id]>0&&controlsSettings.customGP[gamepadSettings[gp.id]-1]){mapping=controlsSettings.customGP[gamepadSettings[gp.id]-1].button
style=controlsSettings.customGP[gamepadSettings[gp.id]-1].style}else if(gp.mapping!=="standard"){continue}var mapped=mapping[button]
var pressed=void 0
if(Array.isArray(mapped)){var axis=gp.axes[mapped[0]]
if(mapped[1]){pressed=axis>.5}else{pressed=axis<-.5}}else{var gpButton=gp.buttons[mapped]
pressed=gpButton&&gpButton.pressed}if(pressed){SpyCards.buttonHeld[button]=true
lastGamepad=Date.now()
lastGamepadStyle=style}}}}function consumeButton(btn){if(buttonPressed[btn]){buttonPressed[btn]=false
return true}return false}SpyCards.consumeButton=consumeButton
function consumeButtonAllowRepeat(btn,delay){if(delay===void 0){delay=.1}if(buttonPressed[btn]){buttonPressed[btn]=false
buttonWasPressed[btn]=inputTicks+delay*60
return true}return false}SpyCards.consumeButtonAllowRepeat=consumeButtonAllowRepeat
function isAllowedKeyCode(code){if(code.startsWith("Alt")||code.startsWith("Control")||code.startsWith("Shift")||code.startsWith("Meta")||code.startsWith("OS")||code.startsWith("Browser")||code.startsWith("Launch")||code.startsWith("Audio")||code.startsWith("Media")||code.startsWith("Volume")||code.startsWith("Lang")||code.startsWith("Page")||code.endsWith("Mode")||code.endsWith("Lock")||/^F[0-9]+$/.test(code)||code===""||code==="Unidentified"||code==="PrintScreen"||code==="Power"||code==="Pause"||code==="ContextMenu"||code==="Help"||code==="Fn"||code==="Tab"||code==="Home"||code==="End"){return false}if(/^Key[A-Z]$|^Digit[0-9]$|^Numpad|^Intl|Up$|Down$|Left$|Right$/.test(code)){return true}switch(code){case"Escape":case"Minus":case"Equal":case"Backspace":case"Enter":case"Semicolon":case"Quote":case"Backquote":case"Backslash":case"Comma":case"Period":case"Slash":case"Space":case"Insert":case"Delete":return true
default:console.warn("Unhandled keycode "+code+"; ignoring for safety.")
debugger
return false}}var awaitingKeyPress=[]
addEventListener("keydown",(function(e){if(SpyCards.disableKeyboard){return}if(e.ctrlKey||e.altKey||e.metaKey){return}if("value"in e.target){return}if(isAllowedKeyCode(e.code)){e.preventDefault()
keyHeld[e.code]=true
for(var i=0;i<awaitingKeyPress.length;i++){awaitingKeyPress[i](e.code)}awaitingKeyPress.length=0}}))
addEventListener("keyup",(function(e){if(SpyCards.disableKeyboard){return}if(e.ctrlKey||e.altKey||e.metaKey){return}if("value"in e.target){return}if(isAllowedKeyCode(e.code)){e.preventDefault()
keyHeld[e.code]=false}}))
function nextPressedKey(){return new Promise((function(resolve){return awaitingKeyPress.push(resolve)}))}SpyCards.nextPressedKey=nextPressedKey
function nextPressedButton(){var alreadyHeld=[]
var gamepads=navigator.getGamepads()
for(var i=0;i<gamepads.length;i++){var gp=gamepads[i]
if(!gp){continue}for(var j=0;j<gp.buttons.length;j++){if(gp.buttons[j].pressed){alreadyHeld.push(gp.id+":"+j)}}for(var j=0;j<gp.axes.length;j++){if(gp.axes[j]>.5){alreadyHeld.push(gp.id+":+"+j)}if(gp.axes[j]<-.5){alreadyHeld.push(gp.id+":-"+j)}}}return new Promise((function(resolve){requestAnimationFrame((function check(){var gamepads=navigator.getGamepads()
for(var i=0;i<gamepads.length;i++){var gp=gamepads[i]
if(!gp){continue}for(var j=0;j<gp.buttons.length;j++){var already=alreadyHeld.indexOf(gp.id+":"+j)
if(already===-1&&gp.buttons[j].pressed){return resolve(j)}if(already!==-1&&!gp.buttons[j].pressed){alreadyHeld.splice(already,1)}}for(var j=0;j<gp.axes.length;j++){var alreadyP=alreadyHeld.indexOf(gp.id+":+"+j)
var alreadyN=alreadyHeld.indexOf(gp.id+":-"+j)
if(alreadyP===-1&&gp.axes[j]>.5){return resolve([j,true])}if(alreadyN===-1&&gp.axes[j]<-.5){return resolve([j,false])}if(alreadyP!==-1&&gp.axes[j]<=.5){alreadyHeld.splice(alreadyP,1)}if(alreadyN!==-1&&gp.axes[j]>=-.5){alreadyHeld.splice(alreadyN,1)}}}requestAnimationFrame(check)}))}))}SpyCards.nextPressedButton=nextPressedButton})(SpyCards||(SpyCards={}))
"use strict"
if(!NodeList.prototype.forEach){NodeList.prototype.forEach=Array.prototype.forEach}if(!window.Blob.prototype.arrayBuffer){window.Blob.prototype.arrayBuffer=function(){var blob=this
return new Promise((function(resolve,reject){var reader=new FileReader
reader.onload=function(){resolve(reader.result)}
reader.onerror=reject
reader.readAsArrayBuffer(blob)}))}}if(!window.TextDecoder){window.TextDecoder=function(){this.decode=function(array){var out=""
var len=array.length
var i=0
while(i<len){var c=array[i++]
switch(c>>4){case 0:case 1:case 2:case 3:case 4:case 5:case 6:case 7:out+=String.fromCharCode(c)
break
case 12:case 13:var char1=array[i++]
out+=String.fromCharCode((c&31)<<6|char1&63)
break
case 14:var char2=array[i++]
var char3=array[i++]
out+=String.fromCharCode((c&15)<<12|(char2&63)<<6|(char3&63)<<0)
break}}return out}}}if(!WebAssembly.instantiateStreaming){WebAssembly.instantiateStreaming=function(resp,importObject){return Promise.resolve(resp).then((function(r){return r.arrayBuffer()})).then((function(source){return WebAssembly.instantiate(source,importObject)}))}}"use strict"
var SpyCards;(function(SpyCards){var TheRoom;(function(TheRoom){function playerDisplayName(character){return character.displayName||character.name.substr(0,1).toUpperCase()+character.name.substr(1)}TheRoom.playerDisplayName=playerDisplayName
var Player;(function(Player){Player.characters=[{name:"amber"},{name:"aria",displayName:"Acolyte Aria"},{name:"arie"},{name:"astotheles"},{name:"bomby"},{name:"bu-gi"},{name:"carmina"},{name:"celia"},{name:"cenn"},{name:"cerise"},{name:"chompy"},{name:"chubee"},{name:"chuck"},{name:"crisbee"},{name:"crow"},{name:"diana"},{name:"eremi"},{name:"futes"},{name:"genow"},{name:"honeycomb",displayName:"Professor Honeycomb"},{name:"janet"},{name:"jaune"},{name:"jayde"},{name:"johnny"},{name:"kabbu"},{name:"kage"},{name:"kali"},{name:"kenny"},{name:"kina"},{name:"lanya"},{name:"leif"},{name:"levi"},{name:"maki"},{name:"malbee"},{name:"mar"},{name:"mothiva"},{name:"neolith",displayName:"Professor Neolith"},{name:"nero"},{name:"pibu"},{name:"pisci"},{name:"ritchee"},{name:"riz"},{name:"samira"},{name:"scarlet",displayName:"Monsieur Scarlet"},{name:"serene"},{name:"shay"},{name:"tanjerin"},{name:"ultimax",displayName:"General Ultimax"},{name:"vanessa"},{name:"vi"},{name:"yin"},{name:"zaryant"},{name:"zasp"}]})(Player=TheRoom.Player||(TheRoom.Player={}))})(TheRoom=SpyCards.TheRoom||(SpyCards.TheRoom={}))})(SpyCards||(SpyCards={}))
"use strict"
var SpyCards;(function(SpyCards){var SpoilerGuard;(function(SpoilerGuard){var _a
var cardEnemyIDs=[0,1,2,3,4,5,6,7,8,9,14,15,16,17,19,20,21,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,61,63,64,65,66,67,68,69,70,71,72,73,74,75,76,77,78,79,80,81,82,83,84,85,86,87,88,89,90,91,92,93,94,95,96,97,98,111,112]
var encCards={"ȮȬ":0,"ȭȭ":1,"ȭȬ":2,"ȯ":3,"ȫȭ":4,"ȫȬ":5,"Ȯ":6,"ȫȩ":7,"Ȯȧ":8,"Ȫȧ":9,"Ȯȯ":10,"Ȭȭ":11,"ȮȮ":12,"Ȯȭ":13,"ȭȫ":14,"ȨȮ":15,"ȭȦ":16,"ȭȨ":17,"ȫȨ":18,"ȫȦ":19,"ȫȧ":20,"ȪȦ":21,"ȪȮ":22,"Ȫȭ":23,"ȨȬ":24,"Ȩȭ":25,"Ȩȯ":26,"ȩȨ":27,"ȩȧ":28,"ȬȮ":29,"ȭȯ":30,"ȮȪ":31,"Ȧ":32,"ȧȦ":33,"ȧȧ":34,"ȧ":35,"Ȭȯ":36,"Ȫȩ":37,"Ȯȫ":38,"ȩȦ":39,"ȭȮ":40,"ȩȭ":41,"Ȯȩ":42,"ȭȪ":43,"Ȭȫ":44,"ȪȬ":45,"Ȫȫ":46,"ȪȪ":47,"ȧȩ":48,"ȧȨ":49,"ȮȨ":50,"ȬȬ":0,"ȩ":1,"Ȫ":2,"ȫ":3,"ȬȨ":4,"Ȭȧ":5,"ȩȪ":6,"ȩȬ":7,"ȩȫ":8,"ȩȯ":9,"Ȩȫ":10,"ȨȪ":11,"Ȩ":12,"ȮȦ":13,"ȫȮ":14,"ȭȧ":15,"ȧȫ":16,"ȧȮ":17,"ȧȭ":18,"Ȩȩ":19,"ȨȨ":20,"Ȩȧ":21,"ȭ":0,"Ȭ":1,"ȬȪ":2,"ȫȫ":3,"ȪȨ":4,"ȩȮ":5,"ȭȩ":6,"Ȭȩ":7,"ȧȬ":8,"ȫȯ":9,"ȬȦ":10,"ȫȪ":11,"ȧȪ":12,"ȩȩ":13,"ȧȯ":14,"Ȫȯ":15,"ȨȦ":16,"ȮȮȮ":22,"ȮȮȭ":23}
var sep0="ȳ"
var sep1="ɟ"
var sep2="ȕ"
var sep3="ɣɌɏɓɖɋɣ"
var nilval="ȯ"
var questID="ȩ"
function flagMap(s){return s.split(sep0).map((function(b){return!(b.length&1)}))}function getSpoilerGuardData(){var val=localStorage["spy-cards-spoiler-guard-v0"]
return val?JSON.parse(val):null}SpoilerGuard.getSpoilerGuardData=getSpoilerGuardData
var GuardState;(function(GuardState){GuardState[GuardState["QuestLocked"]=0]="QuestLocked"
GuardState[GuardState["QuestNotAccepted"]=1]="QuestNotAccepted"
GuardState[GuardState["QuestNotCompleted"]=2]="QuestNotCompleted"
GuardState[GuardState["NotMetCarmina"]=3]="NotMetCarmina"
GuardState[GuardState["CardsNotApproved"]=4]="CardsNotApproved"
GuardState[GuardState["NotAllSeen"]=5]="NotAllSeen"
GuardState[GuardState["NotAllSpied"]=6]="NotAllSpied"
GuardState[GuardState["Disabled"]=7]="Disabled"})(GuardState=SpoilerGuard.GuardState||(SpoilerGuard.GuardState={}))
var cannotPlayState=(_a={},_a[GuardState.QuestLocked]=true,_a[GuardState.QuestNotAccepted]=true,_a[GuardState.QuestNotCompleted]=true,_a[GuardState.NotMetCarmina]=true,_a[GuardState.CardsNotApproved]=true,_a)
function getSpoilerGuardState(){var save=getSpoilerGuardData()
if(!save){return GuardState.Disabled}if(save.q!==2){return[GuardState.QuestLocked,GuardState.QuestNotAccepted,GuardState.QuestNotCompleted][save.q+1]}if(!save.t){return GuardState.NotMetCarmina}if(!save.a){return GuardState.CardsNotApproved}var seenAllEnemies=true
var spiedAllEnemies=true
var enemyBitmap=Base64.decode(save.s)
for(var i=0;i<cardEnemyIDs.length;i++){var card=cardEnemyIDs[i]
var j=card>>2
var k=(card&3)<<1
if(!(enemyBitmap[j]&1<<k)){seenAllEnemies=false}if(!(enemyBitmap[j]&2<<k)){spiedAllEnemies=false}}if(!seenAllEnemies){return GuardState.NotAllSeen}if(!spiedAllEnemies){return GuardState.NotAllSpied}return GuardState.Disabled}SpoilerGuard.getSpoilerGuardState=getSpoilerGuardState
function surveySaveData(form){function ask(question,options){var p=document.createElement("p")
p.textContent=question
form.appendChild(p)
var buttons=document.createElement("div")
buttons.classList.add("buttons")
form.appendChild(buttons)
if(!options){options=["Yes","No"]}var optionPromises=options.map((function(opt,i){return new Promise((function(resolve){var btn=SpyCards.UI.button(opt,[],(function(){return resolve(i)}))
buttons.appendChild(btn)}))}))
return Promise.race(optionPromises).then((function(response){SpyCards.UI.remove(p)
SpyCards.UI.remove(buttons)
return response}))}return ask("Which do you want to do?",["Upload Save File","Answer Some Questions"]).then((function(wantSurvey){if(!wantSurvey){return null}var chapterNumber
var questCompleted
var enemyData=new Uint8Array(256/8)
function seenSpied(id){enemyData[id>>2]|=3<<((id&3)<<1)}return ask("What is your current chapter number?",["1","2","3","4","5","6","7"]).then((function(num){chapterNumber=num+1
if(chapterNumber<=2){return 1}return ask('Have you completed the sidequest "Requesting Assistance"?')})).then((function(quest){questCompleted=!quest
if(questCompleted){seenSpied(31)}if(chapterNumber<1){return 1}seenSpied(0)
seenSpied(1)
seenSpied(2)
seenSpied(8)
seenSpied(9)
if(chapterNumber<2){return 1}return chapterNumber>2?0:ask("Have you reached Golden Settlement?")})).then((function(answer){if(answer){return 1}seenSpied(14)
seenSpied(16)
seenSpied(17)
seenSpied(20)
seenSpied(25)
seenSpied(29)
seenSpied(30)
seenSpied(32)
return chapterNumber>2?0:ask("Have you gained passage to Golden Hills?")})).then((function(answer){if(answer){return 1}seenSpied(19)
seenSpied(21)
if(chapterNumber>2){seenSpied(3)
seenSpied(15)
seenSpied(24)}if(chapterNumber<3){return 1}return chapterNumber>3?0:ask("Have you reached Defiant Root?")})).then((function(answer){if(answer){return 1}seenSpied(4)
seenSpied(5)
seenSpied(6)
seenSpied(7)
seenSpied(28)
seenSpied(33)
seenSpied(39)
return chapterNumber>3?0:ask("Have you found the Overseer?")})).then((function(answer){if(answer){return 1}seenSpied(42)
seenSpied(43)
seenSpied(44)
seenSpied(45)
seenSpied(48)
if(chapterNumber>3){seenSpied(46)
seenSpied(34)
seenSpied(36)
seenSpied(47)}if(chapterNumber<4){return 1}return chapterNumber>4?0:ask("Have you obtained the Earth Key?")})).then((function(answer){if(answer){return 1}seenSpied(40)
return chapterNumber>4?0:ask("Have you reached the sand castle?")})).then((function(answer){if(answer){return 1}seenSpied(49)
seenSpied(81)
seenSpied(83)
seenSpied(84)
return chapterNumber>4?0:ask("Have you obtained the fourth artifact?")})).then((function(answer){if(answer){return 1}seenSpied(54)
seenSpied(57)
seenSpied(58)
seenSpied(61)
if(chapterNumber>4){seenSpied(23)
seenSpied(51)
seenSpied(85)
seenSpied(86)}return ask("Have you discovered a use for the machine in Professor Honeycomb's office?")})).then((function(answer){if(!answer){seenSpied(41)
seenSpied(70)}if(chapterNumber<5){return 1}return chapterNumber>5?0:ask("Have you reached the swamp?")})).then((function(answer){if(answer){return 1}seenSpied(38)
seenSpied(63)
seenSpied(71)
seenSpied(73)
return chapterNumber>5?0:ask("Have you reached the Wasp Kingdom?")})).then((function(answer){if(answer){return}seenSpied(65)
seenSpied(66)
seenSpied(67)
seenSpied(68)
seenSpied(69)
if(chapterNumber>5){seenSpied(26)
seenSpied(27)
seenSpied(72)
seenSpied(97)}})).then((function(){return chapterNumber<5?1:ask("Have you discovered a use for the gem dropped by The Watcher?")})).then((function(answer){if(!answer){seenSpied(52)
seenSpied(53)
seenSpied(56)
seenSpied(96)}if(chapterNumber<6){return 1}return chapterNumber>6?0:ask("Have you crossed the Forsaken Lands?")})).then((function(answer){if(answer){return 1}seenSpied(37)
seenSpied(64)
seenSpied(78)
seenSpied(79)
seenSpied(80)
return chapterNumber>6?0:ask("Do you have the boat?")})).then((function(answer){if(answer){return 1}seenSpied(74)
seenSpied(75)
seenSpied(76)
return chapterNumber>6?0:ask("Have you reached Rubber Prison?")})).then((function(answer){if(answer){return-1}seenSpied(82)
if(chapterNumber>6){seenSpied(95)}if(chapterNumber<7){return-1}seenSpied(87)
seenSpied(88)
seenSpied(89)
return chapterNumber>7?0:ask("Which is the furthest area you have reached?",["Dead Lands","The Machine","Sapling Plains"])})).then((function(answer){return answer===2?ask("Has the sapling been destroyed?"):1})).then((function(answer){if(answer){return-1}seenSpied(90)
seenSpied(91)
return ask("Have you fought Team Maki? Answer yes even if you lost.")})).then((function(answer){if(!answer){seenSpied(92)
seenSpied(93)
seenSpied(94)}return answer===-1?1:ask("Have you fought Team Slacker? Answer yes even if you lost.")})).then((function(answer){if(!answer){seenSpied(111)
seenSpied(112)}return chapterNumber<3?1:ask("Have you completed the following bounty: Devourer",["Yes","No"])})).then((function(answer){if(!answer){seenSpied(98)}return chapterNumber<4?1:ask("Have you completed the following bounty: Tidal Wyrm",["Yes","No"])})).then((function(answer){if(!answer){seenSpied(50)}return chapterNumber<5?1:ask("Have you completed the following bounty: Seedling King",["Yes","No"])})).then((function(answer){if(!answer){seenSpied(35)}return chapterNumber<6?1:ask("Have you completed the following bounty: False Monarch",["Yes","No"])})).then((function(answer){if(!answer){seenSpied(77)}return chapterNumber<6?1:!(enemyData[76>>2]&1<<(76&3))?1:ask("Have you completed the following bounty: Peacock Spider",["Yes","No"])})).then((function(answer){if(!answer){seenSpied(55)}return{q:chapterNumber<=2?-1:questCompleted?2:0,t:questCompleted,a:questCompleted,d:null,s:Base64.encode(enemyData),m:null}}))}))}SpoilerGuard.surveySaveData=surveySaveData
function parseSaveData(data){var sections=data.split(sep2)
var flags=flagMap(sections[11])
var modes0=[613,614,615,616,656,681]
var modes1=modes0.map((function(n,i){return flags[n]?1<<i:0}))
var modes2=modes1.reduce((function(x,y){return x|y}),0)
var questType=sections[5].split(sep1).map((function(q){return q.split(sep0)})).findIndex((function(q){return q.indexOf(questID)!==-1}))
var deck0=sections[12].split(sep3)[12]
var deck1=deck0.split(sep0).map((function(e){return encCards[e]}))
var deck2=deck1.length!==15||deck1.some((function(c){return typeof c!=="number"}))?null:new Uint8Array(11)
if(deck2){deck2[0]=deck1[0]<<2|deck1[1]>>3
deck2[1]=deck1[1]<<5|deck1[2]
for(var i_1=2,j=3;j<deck1.length;i_1+=3,j+=4){deck2[i_1]=deck1[j]<<2|deck1[j+1]>>4
deck2[i_1+1]=deck1[j+1]<<4|deck1[j+2]>>2
deck2[i_1+2]=deck1[j+2]<<6|deck1[j+3]}}var spyData=flagMap(sections[10].split(sep1)[1])
var enemyData0=sections[17]
var enemyData1=enemyData0.split(sep1)
var enemyData2=enemyData1.map((function(e){return e.split(sep0)}))
var enemyData3=enemyData2.map((function(e){return e.map((function(c){return c!==nilval}))}))
var enemyData4=new Uint8Array(256/8)
var i=0,b=0
for(var j=0;j<128;j++){if(enemyData3[j][0]){enemyData4[i]|=1<<b}b++
if(spyData[j]){enemyData4[i]|=1<<b}b++
if(b>=8){i++
b=0}}if(b||i!==enemyData4.length){throw new Error("invalid data")}return{q:questType,t:flags[236],a:flags[237],d:deck2?Base32.Crockford.encode(deck2):null,s:Base64.encode(enemyData4),m:modes2}}SpoilerGuard.parseSaveData=parseSaveData
function requestDataFile(){var upload=document.createElement("input")
upload.type="file"
upload.accept=".dat"
var filePromise=new Promise((function(resolve,reject){upload.addEventListener("input",(function(e){if(upload.files.length){resolve(upload.files[0])}else{reject(new Error("no file selected"))}}))}))
upload.click()
return filePromise}function onFile(f){if(["save0.dat","save1.dat","save2.dat"].indexOf(f.name)===-1){debugger}f.text().then((function(data){var parsed=parseSaveData(data)
localStorage["spy-cards-spoiler-guard-v0"]=JSON.stringify(parsed)
updateSpoilerGuardState()}))}var enableButtons=document.querySelectorAll(".enable-spoiler-guard")
if(enableButtons.length){document.addEventListener("dragover",(function(e){e.preventDefault()
e.dataTransfer.dropEffect="copy"}))
document.addEventListener("drop",(function(e){e.preventDefault()
onFile(e.dataTransfer.files[0])}))
enableButtons.forEach((function(btn){btn.addEventListener("click",(function(e){e.preventDefault()
var form=document.createElement("div")
form.classList.add("readme","spoiler-guard-form")
form.setAttribute("role","form")
form.setAttribute("aria-live","assertive")
document.body.appendChild(form)
surveySaveData(form).then((function(surveyData){SpyCards.UI.remove(form)
if(surveyData===null){requestDataFile().then((function(f){return onFile(f)}))}else{localStorage["spy-cards-spoiler-guard-v0"]=JSON.stringify(surveyData)
updateSpoilerGuardState()}}))}))}))}var disableButton=document.querySelector(".disable-spoiler-guard")
if(disableButton){disableButton.addEventListener("click",(function(e){e.preventDefault()
delete localStorage["spy-cards-spoiler-guard-v0"]
updateSpoilerGuardState()}))}else if(cannotPlayState[getSpoilerGuardState()]){location.href="spoiler-guard.html"}function updateSpoilerGuardState(){var data=getSpoilerGuardData()
var isEnabled=data!==null
var state=getSpoilerGuardState()
document.documentElement.classList.toggle("room-pr",data&&data.m&&(data.m&8)!==0)
document.querySelectorAll(".spoiler-guard-disabled, .spoiler-guard-enabled, [data-spoiler-guard-state]").forEach((function(el){el.hidden=false
if(el.classList.contains("spoiler-guard-disabled")){el.hidden=isEnabled}if(el.classList.contains("spoiler-guard-enabled")){el.hidden=!isEnabled}if(el.hidden){return}var stateRaw=el.getAttribute("data-spoiler-guard-state")
if(!stateRaw){return}var expectedState=parseInt(stateRaw,10)
el.hidden=expectedState!==state}))
document.querySelectorAll(".spoiler-guard-deck").forEach((function(el){if(!data||!data.d){el.hidden=true
return}el.hidden=false
el.querySelectorAll("a").forEach((function(a){a.href="/game/deck/vanilla/"+data.d}))}))}setTimeout(updateSpoilerGuardState,1)})(SpoilerGuard=SpyCards.SpoilerGuard||(SpyCards.SpoilerGuard={}))})(SpyCards||(SpyCards={}))
"use strict"
var SpyCards;(function(SpyCards){var UI;(function(UI){function remove(el){if(el&&el.parentNode){el.parentNode.removeChild(el)}}UI.remove=remove
function clear(el){if(!el){return}el.textContent=""}UI.clear=clear
function button(label,classes,click){var btn=document.createElement("button")
btn.classList.add.apply(btn.classList,classes)
btn.textContent=label
btn.addEventListener("click",(function(e){e.preventDefault()
click()}))
return btn}UI.button=button
function option(label,value){var opt=document.createElement("option")
opt.textContent=label
opt.value=value
return opt}UI.option=option})(UI=SpyCards.UI||(SpyCards.UI={}))})(SpyCards||(SpyCards={}))
"use strict"
var disableDoSquish=false
function doSquish(el){if(disableDoSquish){return}el.querySelectorAll(".squish").forEach((function(s){var max=parseInt(getComputedStyle(s.parentElement).width.replace("px",""),10)
s.style.transform=""
var current=s.clientWidth
if(current>max){s.style.transform="scaleX(calc("+max+" / "+current+"))"}}))
el.querySelectorAll(".squishy").forEach((function(s){var max=parseInt(getComputedStyle(s.parentElement).height.replace("px",""),10)
s.style.transform=""
var current=s.clientHeight
if(current>max){s.style.transform="scaleY(calc("+max+" / "+current+"))"}}))}addEventListener("load",(function(){doSquish(document)}))
function sleep(ms){return new Promise((function(resolve){setTimeout((function(){resolve()}),ms)}))}var SpyCards;(function(SpyCards){function loadSettings(){if(localStorage["spy-cards-settings-v0"]){return JSON.parse(localStorage["spy-cards-settings-v0"])}var settings={audio:{music:0,sounds:0},disable3D:true}
var anyLegacy=false
if(localStorage["spy-cards-audio-settings-v0"]){var legacyAudio=JSON.parse(localStorage["spy-cards-audio-settings-v0"])
settings.audio.music=legacyAudio.enabled||legacyAudio.musicEnabled?.6:0
settings.audio.sounds=legacyAudio.enabled||legacyAudio.sfxEnabled?.6:0
anyLegacy=true}if(localStorage["spy-cards-player-sprite-v0"]){settings.character=localStorage["spy-cards-player-sprite-v0"]
anyLegacy=true}if(anyLegacy){saveSettings(settings)
delete localStorage["spy-cards-audio-settings-v0"]
delete localStorage["spy-cards-player-sprite-v0"]}return settings}SpyCards.loadSettings=loadSettings
function saveSettings(settings){localStorage["spy-cards-settings-v0"]=JSON.stringify(settings)
if(navigator.serviceWorker&&navigator.serviceWorker.controller){navigator.serviceWorker.controller.postMessage({type:"settings-changed"})}window.dispatchEvent(new Event("spy-cards-settings-changed"))}SpyCards.saveSettings=saveSettings})(SpyCards||(SpyCards={}));(function(){function setColorScheme(){var setting=SpyCards.loadSettings().theme
var light=setting==="light"
if(!setting&&window.matchMedia){light=window.matchMedia("(prefers-color-scheme: light)").matches}document.documentElement.classList.toggle("use-light-theme",light)
var themeColor=document.querySelector('meta[name="theme-color"]')
if(themeColor){themeColor.setAttribute("content",light?"#acc":"#023")}}window.addEventListener("spy-cards-settings-changed",setColorScheme)
var prefersLight=window.matchMedia("(prefers-color-scheme: light)")
if(prefersLight&&prefersLight.addEventListener){prefersLight.addEventListener("change",setColorScheme)}setColorScheme()})()
"use strict"
var spyCardsVersionPrefix="0.3.27"
var spyCardsClientMode=location.pathname==="/game.html"?new URLSearchParams(location.search).get("mode"):""
var spyCardsVersionSuffix=function(){var path=location.pathname.substr(1)
if(spyCardsClientMode){return"-"+spyCardsClientMode}if(path.substr(path.length-".html".length)===".html"){return"-"+path.substr(0,path.length-5)}return""}()
var spyCardsVersion=spyCardsVersionPrefix+spyCardsVersionSuffix
var spyCardsVersionVariableSuffix=""
"use strict";(()=>{const enosys=()=>{const err=new Error("not implemented")
err.code="ENOSYS"
return err}
let panicBuf=""
if(!globalThis.fs){let outputBuf=""
globalThis.fs={constants:{O_WRONLY:-1,O_RDWR:-1,O_CREAT:-1,O_TRUNC:-1,O_APPEND:-1,O_EXCL:-1},writeSync(fd,buf){outputBuf+=decoder.decode(buf)
const nl=outputBuf.lastIndexOf("\n")
if(nl!=-1){console.log(outputBuf.substring(0,nl))
outputBuf=outputBuf.substring(nl+1)}return buf.length},write(fd,buf,offset,length,position,callback){if(offset!==0||length!==buf.length||position!==null){callback(enosys())
return}const n=this.writeSync(fd,buf)
callback(null,n)},chmod(path,mode,callback){callback(enosys())},chown(path,uid,gid,callback){callback(enosys())},close(fd,callback){callback(enosys())},fchmod(fd,mode,callback){callback(enosys())},fchown(fd,uid,gid,callback){callback(enosys())},fstat(fd,callback){callback(enosys())},fsync(fd,callback){callback(null)},ftruncate(fd,length,callback){callback(enosys())},lchown(path,uid,gid,callback){callback(enosys())},link(path,link,callback){callback(enosys())},lstat(path,callback){callback(enosys())},mkdir(path,perm,callback){callback(enosys())},open(path,flags,mode,callback){callback(enosys())},read(fd,buffer,offset,length,position,callback){callback(enosys())},readdir(path,callback){callback(enosys())},readlink(path,callback){callback(enosys())},rename(from,to,callback){callback(enosys())},rmdir(path,callback){callback(enosys())},stat(path,callback){callback(enosys())},symlink(path,link,callback){callback(enosys())},truncate(path,length,callback){callback(enosys())},unlink(path,callback){callback(enosys())},utimes(path,atime,mtime,callback){callback(enosys())}}}if(!globalThis.process){globalThis.process={getuid(){return-1},getgid(){return-1},geteuid(){return-1},getegid(){return-1},getgroups(){throw enosys()},pid:-1,ppid:-1,umask(){throw enosys()},cwd(){throw enosys()},chdir(){throw enosys()}}}if(!globalThis.crypto){throw new Error("globalThis.crypto is not available, polyfill required (crypto.getRandomValues only)")}if(!globalThis.performance){throw new Error("globalThis.performance is not available, polyfill required (performance.now only)")}if(!globalThis.TextEncoder){throw new Error("globalThis.TextEncoder is not available, polyfill required")}if(!globalThis.TextDecoder){throw new Error("globalThis.TextDecoder is not available, polyfill required")}const encoder=new TextEncoder("utf-8")
const decoder=new TextDecoder("utf-8")
globalThis.Go=class{constructor(){this.argv=["js"]
this.env={}
this.exit=code=>{if(code!==0){console.warn("exit code:",code)
if(panicBuf){const nl=panicBuf.indexOf("\n\n")
const err=new Error(panicBuf.substr(0,nl).replace(/^panic:/,"PANIC:"))
err.goStack=panicBuf.substr(nl+2)
SpyCards.disableErrorHandler=false
errorHandler(err)}}}
this._exitPromise=new Promise((resolve=>{this._resolveExitPromise=resolve}))
this._pendingEvent=null
this._scheduledTimeouts=new Map
this._nextCallbackTimeoutID=1
const setInt64=(addr,v)=>{this.mem.setUint32(addr+0,v,true)
this.mem.setUint32(addr+4,Math.floor(v/4294967296),true)}
const setInt32=(addr,v)=>{this.mem.setUint32(addr+0,v,true)}
const getInt64=addr=>{const low=this.mem.getUint32(addr+0,true)
const high=this.mem.getInt32(addr+4,true)
return low+high*4294967296}
const loadValue=addr=>{const f=this.mem.getFloat64(addr,true)
if(f===0){return undefined}if(!isNaN(f)){return f}const id=this.mem.getUint32(addr,true)
return this._values[id]}
const storeValue=(addr,v)=>{const nanHead=2146959360
if(typeof v==="number"&&v!==0){if(isNaN(v)){this.mem.setUint32(addr+4,nanHead,true)
this.mem.setUint32(addr,0,true)
return}this.mem.setFloat64(addr,v,true)
return}if(v===undefined){this.mem.setFloat64(addr,0,true)
return}let id=this._ids.get(v)
if(id===undefined){id=this._idPool.pop()
if(id===undefined){id=this._values.length}this._values[id]=v
this._goRefCounts[id]=0
this._ids.set(v,id)}this._goRefCounts[id]++
let typeFlag=0
switch(typeof v){case"object":if(v!==null){typeFlag=1}break
case"string":typeFlag=2
break
case"symbol":typeFlag=3
break
case"function":typeFlag=4
break}this.mem.setUint32(addr+4,nanHead|typeFlag,true)
this.mem.setUint32(addr,id,true)}
const loadSlice=addr=>{const array=getInt64(addr+0)
const len=getInt64(addr+8)
return new Uint8Array(this._inst.exports.mem.buffer,array,len)}
const loadSliceOfValues=addr=>{const array=getInt64(addr+0)
const len=getInt64(addr+8)
const a=new Array(len)
for(let i=0;i<len;i++){a[i]=loadValue(array+i*8)}return a}
const loadString=addr=>{const saddr=getInt64(addr+0)
const len=getInt64(addr+8)
return decoder.decode(new DataView(this._inst.exports.mem.buffer,saddr,len))}
const timeOrigin=Date.now()-performance.now()
this.importObject={_gotest:{add:(a,b)=>a+b},gojs:{"runtime.wasmExit":sp=>{sp>>>=0
const code=this.mem.getInt32(sp+8,true)
this.exited=true
delete this._inst
delete this._values
delete this._goRefCounts
delete this._ids
delete this._idPool
this.exit(code)},"runtime.wasmWrite":sp=>{sp>>>=0
const fd=getInt64(sp+8)
const p=getInt64(sp+16)
const n=this.mem.getInt32(sp+24,true)
fs.writeSync(fd,new Uint8Array(this._inst.exports.mem.buffer,p,n))},"runtime.resetMemoryDataView":sp=>{sp>>>=0
this.mem=new DataView(this._inst.exports.mem.buffer)},"runtime.nanotime1":sp=>{sp>>>=0
setInt64(sp+8,(timeOrigin+performance.now())*1e6)},"runtime.walltime":sp=>{sp>>>=0
const msec=(new Date).getTime()
setInt64(sp+8,msec/1e3)
this.mem.setInt32(sp+16,msec%1e3*1e6,true)},"runtime.scheduleTimeoutEvent":sp=>{sp>>>=0
const id=this._nextCallbackTimeoutID
this._nextCallbackTimeoutID++
this._scheduledTimeouts.set(id,setTimeout((()=>{this._resume()
while(this._scheduledTimeouts.has(id)){console.warn("scheduleTimeoutEvent: missed timeout event")
this._resume()}}),getInt64(sp+8)))
this.mem.setInt32(sp+16,id,true)},"runtime.clearTimeoutEvent":sp=>{sp>>>=0
const id=this.mem.getInt32(sp+8,true)
clearTimeout(this._scheduledTimeouts.get(id))
this._scheduledTimeouts.delete(id)},"runtime.getRandomData":sp=>{sp>>>=0
crypto.getRandomValues(loadSlice(sp+8))},"syscall/js.finalizeRef":sp=>{sp>>>=0
const id=this.mem.getUint32(sp+8,true)
this._goRefCounts[id]--
if(this._goRefCounts[id]===0){const v=this._values[id]
this._values[id]=null
this._ids.delete(v)
this._idPool.push(id)}},"syscall/js.stringVal":sp=>{sp>>>=0
storeValue(sp+24,loadString(sp+8))},"syscall/js.valueGet":sp=>{sp>>>=0
const result=Reflect.get(loadValue(sp+8),loadString(sp+16))
sp=this._inst.exports.getsp()>>>0
storeValue(sp+32,result)},"syscall/js.valueSet":sp=>{sp>>>=0
Reflect.set(loadValue(sp+8),loadString(sp+16),loadValue(sp+32))},"syscall/js.valueDelete":sp=>{sp>>>=0
Reflect.deleteProperty(loadValue(sp+8),loadString(sp+16))},"syscall/js.valueIndex":sp=>{sp>>>=0
storeValue(sp+24,Reflect.get(loadValue(sp+8),getInt64(sp+16)))},"syscall/js.valueSetIndex":sp=>{sp>>>=0
Reflect.set(loadValue(sp+8),getInt64(sp+16),loadValue(sp+24))},"syscall/js.valueCall":sp=>{sp>>>=0
try{const v=loadValue(sp+8)
const m=Reflect.get(v,loadString(sp+16))
const args=loadSliceOfValues(sp+32)
const result=Reflect.apply(m,v,args)
sp=this._inst.exports.getsp()>>>0
storeValue(sp+56,result)
this.mem.setUint8(sp+64,1)}catch(err){sp=this._inst.exports.getsp()>>>0
storeValue(sp+56,err)
this.mem.setUint8(sp+64,0)}},"syscall/js.valueInvoke":sp=>{sp>>>=0
try{const v=loadValue(sp+8)
const args=loadSliceOfValues(sp+16)
const result=Reflect.apply(v,undefined,args)
sp=this._inst.exports.getsp()>>>0
storeValue(sp+40,result)
this.mem.setUint8(sp+48,1)}catch(err){sp=this._inst.exports.getsp()>>>0
storeValue(sp+40,err)
this.mem.setUint8(sp+48,0)}},"syscall/js.valueNew":sp=>{sp>>>=0
try{const v=loadValue(sp+8)
const args=loadSliceOfValues(sp+16)
const result=Reflect.construct(v,args)
sp=this._inst.exports.getsp()>>>0
storeValue(sp+40,result)
this.mem.setUint8(sp+48,1)}catch(err){sp=this._inst.exports.getsp()>>>0
storeValue(sp+40,err)
this.mem.setUint8(sp+48,0)}},"syscall/js.valueLength":sp=>{sp>>>=0
setInt64(sp+16,parseInt(loadValue(sp+8).length))},"syscall/js.valuePrepareString":sp=>{sp>>>=0
const str=encoder.encode(String(loadValue(sp+8)))
storeValue(sp+16,str)
setInt64(sp+24,str.length)},"syscall/js.valueLoadString":sp=>{sp>>>=0
const str=loadValue(sp+8)
loadSlice(sp+16).set(str)},"syscall/js.valueInstanceOf":sp=>{sp>>>=0
this.mem.setUint8(sp+24,loadValue(sp+8)instanceof loadValue(sp+16)?1:0)},"syscall/js.copyBytesToGo":sp=>{sp>>>=0
const dst=loadSlice(sp+8)
const src=loadValue(sp+32)
if(!(src instanceof Uint8Array||src instanceof Uint8ClampedArray)){this.mem.setUint8(sp+48,0)
return}const toCopy=src.subarray(0,dst.length)
dst.set(toCopy)
setInt64(sp+40,toCopy.length)
this.mem.setUint8(sp+48,1)},"syscall/js.copyBytesToJS":sp=>{sp>>>=0
const dst=loadValue(sp+8)
const src=loadSlice(sp+16)
if(!(dst instanceof Uint8Array||dst instanceof Uint8ClampedArray)){this.mem.setUint8(sp+48,0)
return}const toCopy=src.subarray(0,dst.length)
dst.set(toCopy)
setInt64(sp+40,toCopy.length)
this.mem.setUint8(sp+48,1)},debug:value=>{console.log(value)}}}}async run(instance){if(!(instance instanceof WebAssembly.Instance)){throw new Error("Go.run: WebAssembly.Instance expected")}this._inst=instance
this.mem=new DataView(this._inst.exports.mem.buffer)
this._values=[NaN,0,null,true,false,globalThis,this]
this._goRefCounts=new Array(this._values.length).fill(Infinity)
this._ids=new Map([[0,1],[null,2],[true,3],[false,4],[globalThis,5],[this,6]])
this._idPool=[]
this.exited=false
let offset=4096
const strPtr=str=>{const ptr=offset
const bytes=encoder.encode(str+"\0")
new Uint8Array(this.mem.buffer,offset,bytes.length).set(bytes)
offset+=bytes.length
if(offset%8!==0){offset+=8-offset%8}return ptr}
const argc=this.argv.length
const argvPtrs=[]
this.argv.forEach((arg=>{argvPtrs.push(strPtr(arg))}))
argvPtrs.push(0)
const keys=Object.keys(this.env).sort()
keys.forEach((key=>{argvPtrs.push(strPtr(`${key}=${this.env[key]}`))}))
argvPtrs.push(0)
const argv=offset
argvPtrs.forEach((ptr=>{this.mem.setUint32(offset,ptr,true)
this.mem.setUint32(offset+4,0,true)
offset+=8}))
const wasmMinDataAddr=4096+8192
if(offset>=wasmMinDataAddr){throw new Error("total length of command line and environment variables exceeds limit")}this._inst.exports.run(argc,argv)
if(this.exited){this._resolveExitPromise()}await this._exitPromise}_resume(){if(this.exited){throw new Error("Go program has already exited")}this._inst.exports.resume()
if(this.exited){this._resolveExitPromise()}}_makeFuncWrapper(id){const go=this
return function(){if(go.exited){return}const event={id:id,this:this,args:arguments}
go._pendingEvent=event
go._resume()
return event.result}}}})()
"use strict"
var SpyCards;(function(SpyCards){var Native;(function(Native){function onGLContextFail(message){var container=document.createElement("div")
container.classList.add("error-report")
var h1=document.createElement("h1")
h1.textContent="Uh oh!"
container.appendChild(h1)
var flavor=document.createElement("p")
flavor.classList.add("flavor")
flavor.textContent="Spy Cards Online was unable to start up. The error it reported was:"
container.appendChild(flavor)
var msg=document.createElement("p")
msg.classList.add("message")
msg.textContent=message
container.appendChild(msg)
var h2=document.createElement("h2")
h2.textContent="Next Steps"
container.appendChild(h2)
var ul=document.createElement("ul")
function addHint(text){var li=document.createElement("li")
li.textContent=text
ul.appendChild(li)}addHint("This error most commonly occurs right after installing new graphics card drivers.")
addHint("If you haven't restarted your web browser recently, doing so might fix this.")
addHint("Your browser might have more information about what went wrong in the developer console (ctrl+shift+i).")
addHint("If all else fails, ask for help on the Discord linked to on the home page.")
container.appendChild(ul)
document.body.appendChild(container)
debugger}Native.onGLContextFail=onGLContextFail
Native.go=new window.Go
var inst
var mod
var startFetch
var ready=new Promise((function(resolve){return startFetch=resolve})).then((function(){performance.mark("fetchWASMStart")
var loading=document.getElementById("loading")
var loadingSub=document.createElement("div")
loadingSub.id="loading-sub"
if(loading){loading.appendChild(loadingSub)}loadingSub.innerHTML="Fetching WebAssembly&hellip;"
return WebAssembly.instantiateStreaming(fetch("/spy-cards.wasm"),Native.go.importObject).then((function(result){performance.mark("fetchWASMFinish")
performance.measure("fetchWASM","fetchWASMStart","fetchWASMFinish")
inst=result.instance
mod=result.module
loadingSub.innerHTML="Starting up&hellip;"}))}))
var failedUpdate=false
function checkUpdate(force){if(failedUpdate){return Promise.resolve()}if(!navigator.serviceWorker){return Promise.resolve()}var loading=document.getElementById("loading")
return navigator.serviceWorker.ready.then((function(sw){if(sw&&force){performance.mark("checkSWStart")
sw.update().then((function(){performance.mark("checkSWFinish")
performance.measure("checkSW","checkSWStart","checkSWFinish")}))}return sw})).then((function(sw){if(!sw||!sw.installing){return Promise.resolve()}loading.innerHTML="Updating&hellip;"
return new Promise((function(resolve,reject){navigator.serviceWorker.addEventListener("controllerchange",(function(){resolve()}))
navigator.serviceWorker.addEventListener("message",(function(e){if(e.data.type==="update-error"){reject()}}))})).then((function(){setTimeout((function(){location.reload()}),1e3)
return new Promise((function(){}))}))}))["catch"]((function(){failedUpdate=true
loading.innerHTML="Loading&hellip;"}))}Native.preloadedGeneric={}
Native.preloadedAudio={}
Native.preloadedTextures={}
function preloadHint(){var urls=[]
for(var _i=0;_i<arguments.length;_i++){urls[_i]=arguments[_i]}var _loop_1=function(i){var url=urls[i]
if(url.endsWith(".webp")){Native.preloadedTextures[url]=new Promise((function(resolve,reject){var img=new Image
img.crossOrigin="anonymous"
img.src=url
img.close=function(){}
if(img.decode){img.decode().then((function(){return resolve(img)}),reject)}else{img.onload=function(){return resolve(img)}
img.onerror=reject}}))}else{Native.preloadedGeneric[url]=fetch(url,{mode:"cors",credentials:"omit"})}}
for(var i=0;i<urls.length;i++){_loop_1(i)}}Native.preloadHint=preloadHint
function run(){var argv=[]
for(var _i=0;_i<arguments.length;_i++){argv[_i]=arguments[_i]}var argvLength=0
for(var i=0;i<argv.length;i++){argvLength+=argv[i].length}if(argvLength>1<<16){return Promise.reject(new Error("URL too long (approximately "+Math.round(argvLength/1e3)+"k characters)"))}startFetch()
var checkingReady=checkUpdate(true);(function(){if(localStorage["spy-cards-settings-v0"]){return Promise.resolve()}var loading=document.getElementById("loading")
ready=function(orig){document.querySelectorAll(".hide-on-wasm-ready").forEach((function(el){return el.parentNode.removeChild(el)}))
SpyCards.Audio.Sounds.Confirm.preload()
var form=document.createElement("form")
form.classList.add("first-run","readme")
var h1=document.createElement("h1")
h1.textContent="It looks like it's your first time here."
form.appendChild(h1)
var p=document.createElement("p")
p.textContent="These choices can be changed at any time on "
var settingsLink=document.createElement("a")
settingsLink.href="/settings.html"
settingsLink.textContent="the Settings page"
p.appendChild(settingsLink)
p.appendChild(document.createTextNode("."))
form.appendChild(p)
var l1=document.createElement("label")
l1.textContent="Music and Sound Effects"
var c1=document.createElement("input")
c1.type="checkbox"
c1.addEventListener("input",(function(){if(c1.checked){SpyCards.Audio.setVolume(.6,.6)
SpyCards.Audio.Sounds.Confirm.play()}else{SpyCards.Audio.setVolume(0,0)}}))
if(!SpyCards.Audio.actx){c1.disabled=true
l1.innerHTML="<del>"+l1.innerHTML+"</del><br><small>(does not work in this browser)</small>"}l1.insertBefore(c1,l1.firstChild)
form.appendChild(l1)
var l2=document.createElement("label")
l2.textContent="3D Backgrounds"
var c2=document.createElement("input")
c2.type="checkbox"
c2.addEventListener("input",(function(){var settings=SpyCards.loadSettings()
settings.disable3D=!c2.checked
SpyCards.saveSettings(settings)
SpyCards.Audio.Sounds.Confirm.play()}))
l2.insertBefore(c2,l2.firstChild)
form.appendChild(l2)
var sg=document.createElement("label")
sg.className="spoiler-guard-link"
var sga=document.createElement("a")
sga.href="/spoiler-guard.html"
sga.target="_blank"
sga.textContent="Spoiler Guard"
sg.appendChild(sga)
form.appendChild(sg)
var btn=document.createElement("button")
btn.disabled=true
checkingReady.then((function(){return btn.disabled=false}))
btn.textContent="Continue"
var formDone=new Promise((function(resolve){btn.addEventListener("click",(function(e){e.preventDefault()
var isMobile
if(navigator.userAgentData){isMobile=navigator.userAgentData.mobile}else{isMobile=navigator.userAgent.indexOf(" Mobile ")!==-1}var settings=SpyCards.loadSettings()
if(isMobile){if(window.devicePixelRatio>1){settings.dpiScale=.5}settings.disableCRT=true
settings.limitGPULevel=1}SpyCards.saveSettings(settings)
resolve()}))}))
form.appendChild(btn)
document.body.appendChild(form)
if(loading){loading.style.display="none"}return formDone.then((function(){document.body.removeChild(form)
if(loading){loading.style.display=""}return orig}))}(ready)
return ready})().then((function(){return checkingReady})).then((function(){ready=ready.then((function(){return checkUpdate(false)})).then((function(){var params=new URLSearchParams(location.search)
if(params.has("GOGC")){Native.go.env.GOGC=params.get("GOGC")}if(params.has("GODEBUG")){Native.go.env.GODEBUG=params.get("GODEBUG")}if(params.has("GOMAXPROCS")){Native.go.env.GOMAXPROCS=params.get("GOMAXPROCS")}if(params.has("GOTRACEBACK")){Native.go.env.GOTRACEBACK=params.get("GOTRACEBACK")}Native.go.argv=["js"].concat(argv)
window.SIGQUIT=function(){Native.go._pendingEvent={id:0}
Native.go._resume()}
performance.mark("goInitStart")
return Native.go.run(inst)})).then((function(){return WebAssembly.instantiate(mod,Native.go.importObject)})).then((function(i){inst=i}))["catch"](errorHandler)
return ready}))
return ready}Native.run=run})(Native=SpyCards.Native||(SpyCards.Native={}))})(SpyCards||(SpyCards={}))
