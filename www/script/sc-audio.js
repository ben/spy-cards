"use strict";
var SpyCards;
(function (SpyCards) {
    var Audio;
    (function (Audio) {
        if (window.AudioContext) {
            Audio.actx = new AudioContext();
        }
        else if (window.webkitAudioContext) {
            Audio.actx = new window.webkitAudioContext();
        }
        var soundsGain;
        var t0 = Audio.actx && Audio.actx.currentTime;
        if (t0 !== t0) {
            Audio.actx = null;
            // give up. just give up. nothing will work. how is safari like this.
        }
        Audio.soundsVolume = 0;
        Audio.musicVolume = 0;
        var fetchAudioPromise = new Promise(function (resolve) { return Audio.startFetchAudio = Audio.actx ? resolve : function () { }; }).then(function () {
            var settings = SpyCards.loadSettings();
            Audio.soundsVolume = settings.audio.sounds;
            Audio.musicVolume = settings.audio.music;
            soundsGain = Audio.actx.createGain();
            soundsGain.connect(Audio.actx.destination);
            soundsGain.gain.setValueAtTime(Audio.soundsVolume, Audio.actx.currentTime);
            addEventListener("click", function () {
                if (Audio.actx.state === "suspended") {
                    Audio.actx.resume();
                }
            }, true);
        });
        function isEnabled() {
            Audio.startFetchAudio();
            return fetchAudioPromise.then(function () {
                return Audio.soundsVolume > 0 || Audio.musicVolume > 0;
            });
        }
        Audio.isEnabled = isEnabled;
        function setVolume(music, sounds) {
            Audio.soundsVolume = sounds;
            Audio.musicVolume = music;
            var settings = SpyCards.loadSettings();
            settings.audio.music = music;
            settings.audio.sounds = sounds;
            SpyCards.saveSettings(settings);
            if (soundsGain) {
                soundsGain.gain.setValueAtTime(sounds, Audio.actx.currentTime);
            }
        }
        Audio.setVolume = setVolume;
        addEventListener("spy-cards-settings-changed", function () {
            var s = SpyCards.loadSettings().audio;
            Audio.soundsVolume = s.sounds;
            Audio.musicVolume = s.music;
            if (soundsGain) {
                soundsGain.gain.setValueAtTime(Audio.soundsVolume, Audio.actx.currentTime);
            }
        });
        function fetchAudio(name) {
            return fetchAudioPromise.then(function () {
                return fetch(name.indexOf("/") === -1 ? "/audio/" + name + ".opus" : name);
            }).then(function (response) {
                return response.arrayBuffer();
            }).then(function (body) {
                return (Audio.actx.state === "suspended" ? Audio.actx.resume() : Promise.resolve()).then(function () {
                    return body;
                });
            }).then(function (body) {
                if (Audio.actx.decodeAudioData.length > 1) {
                    // Safari compat path
                    return new Promise(function (resolve, reject) {
                        var w = new Worker("/script/opus-decoder-worker.js", {
                            name: "opus-decoder"
                        });
                        w.onmessage = function (e) {
                            w.terminate();
                            if (e.data.config) {
                                var abuf = Audio.actx.createBuffer(e.data.config.numberOfChannels, e.data.config.length, e.data.config.sampleRate);
                                for (var i = 0; i < e.data.channels.length; i++) {
                                    abuf.getChannelData(i).set(new Float32Array(e.data.channels[i], 0), 0);
                                }
                                return resolve(abuf);
                            }
                            if (e.data.error) {
                                return reject(new Error("audio: opusfile decoding error " + (-e.data.error)));
                            }
                            reject(new Error("audio: unknown error"));
                        };
                        w.postMessage({
                            id: 0,
                            name: name,
                            buffer: body
                        }, [body]);
                    });
                }
                return Audio.actx.decodeAudioData(body);
            });
        }
        var Sound = /** @class */ (function () {
            function Sound(name, pitch, volume) {
                if (pitch === void 0) { pitch = 1; }
                if (volume === void 0) { volume = 1; }
                this.name = name;
                this.buffer = fetchAudio(name);
                this.pitch = pitch;
                this.volume = volume;
            }
            Sound.prototype.createSource = function () {
                Audio.startFetchAudio();
                var sound = this;
                return this.buffer.then(function (buffer) {
                    var source = Audio.actx.createBufferSource();
                    source.buffer = buffer;
                    source.playbackRate.setValueAtTime(sound.pitch, Audio.actx.currentTime);
                    return source;
                });
            };
            Sound.prototype.preload = function () {
                Audio.startFetchAudio();
                return this.buffer.then(function () { });
            };
            Sound.prototype.play = function (delay, overridePitch, overrideVolume) {
                if (delay === void 0) { delay = 0; }
                if (!Audio.soundsVolume) {
                    return Promise.resolve();
                }
                var sound = this;
                return this.createSource().then(function (src) {
                    var dest = soundsGain;
                    if (sound.volume !== 1) {
                        if (!sound.gain) {
                            sound.gain = Audio.actx.createGain();
                            sound.gain.connect(dest);
                            sound.gain.gain.setValueAtTime(sound.volume, Audio.actx.currentTime);
                        }
                        dest = sound.gain;
                    }
                    if (overrideVolume) {
                        var gain = Audio.actx.createGain();
                        gain.connect(dest);
                        gain.gain.setValueAtTime(overrideVolume, Audio.actx.currentTime);
                        dest = gain;
                    }
                    if (overridePitch) {
                        src.playbackRate.setValueAtTime(overridePitch * sound.pitch, Audio.actx.currentTime);
                    }
                    src.connect(dest);
                    src.start(Audio.actx.currentTime + delay);
                });
            };
            return Sound;
        }());
        Audio.Sound = Sound;
        Audio.Sounds = {
            Confirm: new Sound("Confirm")
        };
    })(Audio = SpyCards.Audio || (SpyCards.Audio = {}));
})(SpyCards || (SpyCards = {}));
