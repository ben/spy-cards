"use strict";
var OpusDecoderWorker;
(function (OpusDecoderWorker) {
    var module;
    var instance;
    var resetTimeout = null;
    var importObject = {
        env: {
            emscripten_notify_memory_growth: function (index) { }
        }
    };
    if (!WebAssembly.instantiateStreaming) { // polyfill
        WebAssembly.instantiateStreaming = function (resp, importObject) {
            return Promise.resolve(resp).then(function (r) {
                return r.arrayBuffer();
            }).then(function (source) {
                return WebAssembly.instantiate(source, importObject);
            });
        };
    }
    var wasmReady = WebAssembly.instantiateStreaming(fetch("/opus-decoder-worker.wasm"), importObject).then(function (r) {
        module = r.module;
        instance = r.instance;
        instance.exports._initialize();
    });
    function waitReady() {
        return wasmReady.then(function () {
            clearTimeout(resetTimeout);
            resetTimeout = setTimeout(function () {
                instance = null;
            }, 60000);
            if (instance === null) {
                return WebAssembly.instantiate(module, importObject).then(function (i) {
                    i.exports._initialize();
                    instance = i;
                });
            }
            return Promise.resolve();
        });
    }
    self.onmessage = function (e) {
        var request = e.data;
        var rawData = new Uint8Array(request.buffer, 0);
        request.buffer = null;
        waitReady().then(function () {
            try {
                var pcmBufferLength = 5760 * 2;
                var inputBufferPtr = instance.exports.malloc(rawData.length);
                var outputBufferPtr = instance.exports.malloc(4 + pcmBufferLength * 4);
                try {
                    new Uint8Array(instance.exports.memory.buffer, inputBufferPtr, rawData.length).set(rawData, 0);
                    var opusFile = instance.exports.op_open_memory(inputBufferPtr, rawData.length, outputBufferPtr);
                    if (!opusFile) {
                        request.error = new Int32Array(instance.exports.memory.buffer, outputBufferPtr, 1)[0];
                        return;
                    }
                    var pcmBuffers = [];
                    try {
                        var read = instance.exports.op_read_float(opusFile, outputBufferPtr + 4, pcmBufferLength, outputBufferPtr);
                        if (read < 0) {
                            request.error = read;
                            return;
                        }
                        var numChannels = instance.exports.op_channel_count(opusFile, new Int32Array(instance.exports.memory.buffer, outputBufferPtr, 1)[0]);
                        while (read !== 0) {
                            pcmBuffers.push(new Float32Array(new Float32Array(instance.exports.memory.buffer, outputBufferPtr + 4, read * numChannels)));
                            read = instance.exports.op_read_float(opusFile, outputBufferPtr + 4, pcmBufferLength, outputBufferPtr);
                            if (read < 0) {
                                request.error = read;
                                return;
                            }
                        }
                        var combinedPCM_1 = new Float32Array(pcmBuffers.reduce(function (prev, buf) { return prev + buf.length; }, 0));
                        var offset_1 = 0;
                        pcmBuffers.forEach(function (buf) {
                            combinedPCM_1.set(buf, offset_1);
                            offset_1 += buf.length;
                        });
                        request.config = {
                            length: combinedPCM_1.length / numChannels,
                            numberOfChannels: numChannels,
                            sampleRate: 48000
                        };
                        request.channels = [];
                        for (var channel = 0; channel < numChannels; channel++) {
                            var channelBuf = new Float32Array(combinedPCM_1.length / numChannels);
                            for (var i = 0, j = channel; i < channelBuf.length; i++, j += numChannels) {
                                channelBuf[i] = combinedPCM_1[j];
                            }
                            request.channels[channel] = channelBuf.buffer;
                        }
                    }
                    finally {
                        instance.exports.op_free(opusFile);
                    }
                }
                finally {
                    instance.exports.free(inputBufferPtr);
                    instance.exports.free(outputBufferPtr);
                }
            }
            finally {
                self.postMessage(request, request.channels);
            }
        });
    };
})(OpusDecoderWorker || (OpusDecoderWorker = {}));
