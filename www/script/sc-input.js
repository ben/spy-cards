"use strict";
var SpyCards;
(function (SpyCards) {
    var _a, _b;
    var Button;
    (function (Button) {
        Button[Button["Up"] = 0] = "Up";
        Button[Button["Down"] = 1] = "Down";
        Button[Button["Left"] = 2] = "Left";
        Button[Button["Right"] = 3] = "Right";
        Button[Button["Confirm"] = 4] = "Confirm";
        Button[Button["Cancel"] = 5] = "Cancel";
        Button[Button["Switch"] = 6] = "Switch";
        Button[Button["Toggle"] = 7] = "Toggle";
        Button[Button["Pause"] = 8] = "Pause";
        Button[Button["Help"] = 9] = "Help";
    })(Button = SpyCards.Button || (SpyCards.Button = {}));
    SpyCards.defaultKeyButton = (_a = {},
        _a[Button.Up] = "ArrowUp",
        _a[Button.Down] = "ArrowDown",
        _a[Button.Left] = "ArrowLeft",
        _a[Button.Right] = "ArrowRight",
        _a[Button.Confirm] = "KeyC",
        _a[Button.Cancel] = "KeyX",
        _a[Button.Switch] = "KeyZ",
        _a[Button.Toggle] = "KeyV",
        _a[Button.Pause] = "Escape",
        _a[Button.Help] = "Enter",
        _a);
    // see https://w3c.github.io/gamepad/standard_gamepad.svg
    SpyCards.standardGamepadButton = (_b = {},
        _b[Button.Up] = 12,
        _b[Button.Down] = 13,
        _b[Button.Left] = 14,
        _b[Button.Right] = 15,
        _b[Button.Confirm] = 0,
        _b[Button.Cancel] = 1,
        _b[Button.Switch] = 2,
        _b[Button.Toggle] = 3,
        _b[Button.Pause] = 9,
        _b[Button.Help] = 8,
        _b);
    var buttonPressed = {};
    var buttonWasPressed = {};
    SpyCards.buttonHeld = {};
    var keyHeld = {};
    var lastKeyboard = 0;
    var lastGamepad = 0;
    var inputTicks = 0;
    var ButtonStyle;
    (function (ButtonStyle) {
        ButtonStyle[ButtonStyle["Keyboard"] = 0] = "Keyboard";
        ButtonStyle[ButtonStyle["GenericGamepad"] = 1] = "GenericGamepad";
    })(ButtonStyle = SpyCards.ButtonStyle || (SpyCards.ButtonStyle = {}));
    var lastGamepadStyle = ButtonStyle.GenericGamepad;
    function getButtonStyle() {
        if (!lastGamepad || lastGamepad < lastKeyboard) {
            return ButtonStyle.Keyboard;
        }
        return lastGamepadStyle;
    }
    SpyCards.getButtonStyle = getButtonStyle;
    function saveInputState() {
        var p = {};
        var w = {};
        var h = {};
        for (var btn = Button.Up; btn <= Button.Help; btn++) {
            p[btn] = buttonPressed[btn];
            w[btn] = buttonWasPressed[btn];
            h[btn] = SpyCards.buttonHeld[btn];
        }
        return { t: inputTicks, p: p, w: w, h: h };
    }
    SpyCards.saveInputState = saveInputState;
    function loadInputState(s) {
        inputTicks = s.t;
        for (var btn = Button.Up; btn <= Button.Help; btn++) {
            buttonPressed[btn] = s.p[btn];
            buttonWasPressed[btn] = s.w[btn];
            SpyCards.buttonHeld[btn] = s.h[btn];
        }
    }
    SpyCards.loadInputState = loadInputState;
    function updateButtons(force) {
        if (force) {
            force(SpyCards.buttonHeld);
        }
        else if (SpyCards.updateAIButtons) {
            SpyCards.updateAIButtons(SpyCards.buttonHeld);
        }
        else {
            updateHeldButtons();
        }
        inputTicks++;
        for (var button = Button.Up; button <= Button.Help; button++) {
            if (!SpyCards.buttonHeld[button]) {
                buttonPressed[button] = false;
                buttonWasPressed[button] = 0;
            }
            else if (buttonWasPressed[button] < inputTicks) {
                buttonWasPressed[button] = Infinity;
                buttonPressed[button] = true;
            }
        }
    }
    SpyCards.updateButtons = updateButtons;
    function updateHeldButtons() {
        var controlsSettings = SpyCards.loadSettings().controls || {};
        var gamepadSettings = controlsSettings.gamepad || {};
        var keyboardMap = SpyCards.defaultKeyButton;
        if (controlsSettings.customKB && controlsSettings.keyboard > 0 && controlsSettings.customKB[controlsSettings.keyboard - 1]) {
            keyboardMap = controlsSettings.customKB[controlsSettings.keyboard - 1].code;
        }
        var gamepads = navigator.getGamepads() || [];
        for (var button = Button.Up; button <= Button.Help; button++) {
            SpyCards.buttonHeld[button] = false;
            if (keyHeld[keyboardMap[button]]) {
                SpyCards.buttonHeld[button] = true;
                lastKeyboard = Date.now();
            }
            for (var i = 0; i < gamepads.length; i++) {
                var gp = gamepads[i];
                if (!gp) {
                    continue;
                }
                var mapping = SpyCards.standardGamepadButton;
                var style = ButtonStyle.GenericGamepad;
                if (controlsSettings.customGP && gamepadSettings[gp.id] > 0 && controlsSettings.customGP[gamepadSettings[gp.id] - 1]) {
                    mapping = controlsSettings.customGP[gamepadSettings[gp.id] - 1].button;
                    style = controlsSettings.customGP[gamepadSettings[gp.id] - 1].style;
                }
                else if (gp.mapping !== "standard") {
                    continue;
                }
                var mapped = mapping[button];
                var pressed = void 0;
                if (Array.isArray(mapped)) {
                    var axis = gp.axes[mapped[0]];
                    if (mapped[1]) {
                        pressed = axis > 0.5;
                    }
                    else {
                        pressed = axis < -0.5;
                    }
                }
                else {
                    var gpButton = gp.buttons[mapped];
                    pressed = gpButton && gpButton.pressed;
                }
                if (pressed) {
                    SpyCards.buttonHeld[button] = true;
                    lastGamepad = Date.now();
                    lastGamepadStyle = style;
                }
            }
        }
    }
    function consumeButton(btn) {
        if (buttonPressed[btn]) {
            buttonPressed[btn] = false;
            return true;
        }
        return false;
    }
    SpyCards.consumeButton = consumeButton;
    function consumeButtonAllowRepeat(btn, delay) {
        if (delay === void 0) { delay = 0.1; }
        if (buttonPressed[btn]) {
            buttonPressed[btn] = false;
            buttonWasPressed[btn] = inputTicks + delay * 60;
            return true;
        }
        return false;
    }
    SpyCards.consumeButtonAllowRepeat = consumeButtonAllowRepeat;
    function isAllowedKeyCode(code) {
        // don't grab OS/Browser special keys
        if (code.startsWith("Alt") ||
            code.startsWith("Control") ||
            code.startsWith("Shift") ||
            code.startsWith("Meta") ||
            code.startsWith("OS") ||
            code.startsWith("Browser") ||
            code.startsWith("Launch") ||
            code.startsWith("Audio") ||
            code.startsWith("Media") ||
            code.startsWith("Volume") ||
            code.startsWith("Lang") ||
            code.startsWith("Page") ||
            code.endsWith("Mode") ||
            code.endsWith("Lock") ||
            /^F[0-9]+$/.test(code) ||
            code === "" ||
            code === "Unidentified" ||
            code === "PrintScreen" ||
            code === "Power" ||
            code === "Pause" ||
            code === "ContextMenu" ||
            code === "Help" ||
            code === "Fn" ||
            code === "Tab" ||
            code === "Home" ||
            code === "End") {
            return false;
        }
        // allow letters, numbers, and some symbols.
        if (/^Key[A-Z]$|^Digit[0-9]$|^Numpad|^Intl|Up$|Down$|Left$|Right$/.test(code)) {
            return true;
        }
        // additional symbols and special keys listed by name:
        switch (code) {
            case "Escape":
            case "Minus":
            case "Equal":
            case "Backspace":
            case "Enter":
            case "Semicolon":
            case "Quote":
            case "Backquote":
            case "Backslash":
            case "Comma":
            case "Period":
            case "Slash":
            case "Space":
            case "Insert":
            case "Delete":
                return true;
            default:
                console.warn("Unhandled keycode " + code + "; ignoring for safety.");
                debugger;
                return false;
        }
    }
    var awaitingKeyPress = [];
    addEventListener("keydown", function (e) {
        if (SpyCards.disableKeyboard) {
            return;
        }
        if (e.ctrlKey || e.altKey || e.metaKey) {
            return;
        }
        if ("value" in e.target) {
            return;
        }
        if (isAllowedKeyCode(e.code)) {
            e.preventDefault();
            keyHeld[e.code] = true;
            for (var i = 0; i < awaitingKeyPress.length; i++) {
                awaitingKeyPress[i](e.code);
            }
            awaitingKeyPress.length = 0;
        }
    });
    addEventListener("keyup", function (e) {
        if (SpyCards.disableKeyboard) {
            return;
        }
        if (e.ctrlKey || e.altKey || e.metaKey) {
            return;
        }
        if ("value" in e.target) {
            return;
        }
        if (isAllowedKeyCode(e.code)) {
            e.preventDefault();
            keyHeld[e.code] = false;
        }
    });
    function nextPressedKey() {
        return new Promise(function (resolve) { return awaitingKeyPress.push(resolve); });
    }
    SpyCards.nextPressedKey = nextPressedKey;
    function nextPressedButton() {
        var alreadyHeld = [];
        var gamepads = navigator.getGamepads();
        for (var i = 0; i < gamepads.length; i++) {
            var gp = gamepads[i];
            if (!gp) {
                continue;
            }
            for (var j = 0; j < gp.buttons.length; j++) {
                if (gp.buttons[j].pressed) {
                    alreadyHeld.push(gp.id + ":" + j);
                }
            }
            for (var j = 0; j < gp.axes.length; j++) {
                if (gp.axes[j] > 0.5) {
                    alreadyHeld.push(gp.id + ":+" + j);
                }
                if (gp.axes[j] < -0.5) {
                    alreadyHeld.push(gp.id + ":-" + j);
                }
            }
        }
        return new Promise(function (resolve) {
            requestAnimationFrame(function check() {
                var gamepads = navigator.getGamepads();
                for (var i = 0; i < gamepads.length; i++) {
                    var gp = gamepads[i];
                    if (!gp) {
                        continue;
                    }
                    for (var j = 0; j < gp.buttons.length; j++) {
                        var already = alreadyHeld.indexOf(gp.id + ":" + j);
                        if (already === -1 && gp.buttons[j].pressed) {
                            return resolve(j);
                        }
                        if (already !== -1 && !gp.buttons[j].pressed) {
                            alreadyHeld.splice(already, 1);
                        }
                    }
                    for (var j = 0; j < gp.axes.length; j++) {
                        var alreadyP = alreadyHeld.indexOf(gp.id + ":+" + j);
                        var alreadyN = alreadyHeld.indexOf(gp.id + ":-" + j);
                        if (alreadyP === -1 && gp.axes[j] > 0.5) {
                            return resolve([j, true]);
                        }
                        if (alreadyN === -1 && gp.axes[j] < -0.5) {
                            return resolve([j, false]);
                        }
                        if (alreadyP !== -1 && gp.axes[j] <= 0.5) {
                            alreadyHeld.splice(alreadyP, 1);
                        }
                        if (alreadyN !== -1 && gp.axes[j] >= -0.5) {
                            alreadyHeld.splice(alreadyN, 1);
                        }
                    }
                }
                requestAnimationFrame(check);
            });
        });
    }
    SpyCards.nextPressedButton = nextPressedButton;
})(SpyCards || (SpyCards = {}));
