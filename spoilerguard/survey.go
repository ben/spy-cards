package spoilerguard

import (
	"git.lubar.me/ben/spy-cards/card"
	"git.lubar.me/ben/spy-cards/gfx/sprites"
	"git.lubar.me/ben/spy-cards/gui"
)

func SurveyData(s *gui.Scroll, runSync chan<- func()) *Data {
	v := &gui.Vertical{
		Gap:         0.25,
		AlwaysFocus: true,
	}

	runSync <- func() {
		s.Control = v
	}

	ask := func(question string, options ...string) int {
		result := make(chan int, 1)
		focus := new(*gui.FocusHandler)

		runSync <- func() {
			var prev *gui.Button

			v.Controls = v.Controls[:0]
			v.Controls = append(v.Controls, &gui.Button{
				Label:    question,
				Sx:       1.5,
				Sy:       1.5,
				Color:    sprites.Gray,
				Centered: true,
				Style:    gui.BtnNone,
			})

			for i, o := range options {
				i, o := i, o // shadow

				btn := &gui.Button{
					Label: o,
					Sx:    1,
					Sy:    1,
					OnSelect: func() {
						select {
						case result <- i:
						default:
						}
					},
					Color:        sprites.White,
					DefaultFocus: i == 0,
					Centered:     true,
				}

				if prev != nil {
					btn.InputFocusUp = prev.Focus
					prev.InputFocusDown = btn.Focus
				}

				btn.InputFocus = focus
				prev = btn

				v.Controls = append(v.Controls, btn)
			}

			s.SetY(0)
		}

		return <-result
	}

	confirm := func(question string) bool {
		return ask(question, "Yes", "No") == 0
	}

	if ask("Which do you want to do?", "Upload Save File", "Answer Some Questions") == 0 {
		runSync <- func() {
			v.Controls = v.Controls[:0]
		}

		return nil
	}

	chapterNumber := ask("What is your current chapter number?", "1", "2", "3", "4", "5", "6", "7") + 1
	questCompleted := chapterNumber > 2 && ask("Have you completed the request titled\n\"Requesting Assistance\"?", "Yes", "No") == 0

	enemyData := make([]byte, 256/8)
	seenSpied := func(id card.ID) {
		enemyData[id>>2] |= 3 << ((id & 3) << 1)
	}

	if questCompleted {
		seenSpied(card.MonsieurScarlet)
	}

	switch chapterNumber {
	case 7:
		seenSpied(card.DeadLanderα)
		seenSpied(card.DeadLanderβ)
		seenSpied(card.DeadLanderγ)

		if chapterNumber > 7 || ask("Which is the furthest area you have reached?", "Dead Lands", "The Machine", "Sapling Plains") == 2 && confirm("Has the sapling been destroyed?") {
			seenSpied(card.WaspKing)
			seenSpied(card.TheEverlastingKing)

			if chapterNumber > 7 || confirm("Have you fought Team Maki?\nAnswer yes even if you lost.") {
				seenSpied(card.Maki)
				seenSpied(card.Kina)
				seenSpied(card.Yin)
			}

			if chapterNumber > 7 || confirm("Have you fought Team Slacker?\nAnswer yes even if you lost.") {
				seenSpied(card.Stratos)
				seenSpied(card.Delilah)
			}
		}

		fallthrough
	case 6:
		if chapterNumber > 6 || confirm("Have you crossed the Forsaken Lands?") {
			seenSpied(card.Plumpling)
			seenSpied(card.MimicSpider)
			seenSpied(card.Mothfly)
			seenSpied(card.MothflyCluster)
			seenSpied(card.Ironnail)

			if chapterNumber > 6 || confirm("Do you have the boat?") {
				seenSpied(card.Cross)
				seenSpied(card.Poi)
				seenSpied(card.PrimalWeevil)

				if chapterNumber > 6 || confirm("Have you reached Rubber Prison?") {
					seenSpied(card.Ruffian)

					if chapterNumber > 6 {
						seenSpied(card.UltimaxTank)
					}
				}
			}
		}

		fallthrough
	case 5:
		if chapterNumber > 5 || confirm("Have you reached the swamp?") {
			seenSpied(card.Flowerling)
			seenSpied(card.JumpingSpider)
			seenSpied(card.Mantidfly)
			seenSpied(card.WildChomper)

			if chapterNumber > 5 || confirm("Have you reached the Wasp Kingdom?") {
				seenSpied(card.LeafbugNinja)
				seenSpied(card.LeafbugArcher)
				seenSpied(card.LeafbugClubber)
				seenSpied(card.Madesphy)
				seenSpied(card.TheBeast)

				if chapterNumber > 5 {
					seenSpied(card.WaspBomber)
					seenSpied(card.WaspDriller)
					seenSpied(card.GeneralUltimax)
					seenSpied(card.Riz)
				}
			}
		}

		if confirm("Have you discovered a use for\nthe gem dropped by The Watcher?") {
			seenSpied(card.Zombee)
			seenSpied(card.Zombeetle)
			seenSpied(card.Bloatshroom)
			seenSpied(card.Zommoth)
		}

		fallthrough
	case 4:
		if chapterNumber > 4 || confirm("Have you obtained the Earth Key?") {
			seenSpied(card.Astotheles)

			if chapterNumber > 4 || confirm("Have you reached the sand castle?") {
				seenSpied(card.DuneScorpion)
				seenSpied(card.Belostoss)
				seenSpied(card.WaterStrider)
				seenSpied(card.DivingSpider)

				if chapterNumber > 4 || confirm("Have you obtained the fourth artifact?") {
					seenSpied(card.TheWatcher)
					seenSpied(card.Krawler)
					seenSpied(card.HauntedCloth)
					seenSpied(card.Warden)

					if chapterNumber > 4 {
						seenSpied(card.Kabbu)
						seenSpied(card.Kali)
						seenSpied(card.Cenn)
						seenSpied(card.Pisci)
					}
				}

				if confirm("Have you discovered a use for the machine\nin Professor Honeycomb's office?") {
					seenSpied(card.MotherChomper)
					seenSpied(card.ChomperBrute)
				}
			}
		}

		fallthrough
	case 3:
		if chapterNumber > 3 || confirm("Have you reached Defiant Root?") {
			seenSpied(card.Cactiling)
			seenSpied(card.Psicorp)
			seenSpied(card.Thief)
			seenSpied(card.Bandit)
			seenSpied(card.WaspScout)
			seenSpied(card.ArrowWorm)
			seenSpied(card.Burglar)

			if chapterNumber > 3 || confirm("Have you found the Overseer?") {
				seenSpied(card.Ahoneynation)
				seenSpied(card.BeeBoop)
				seenSpied(card.SecurityTurret)
				seenSpied(card.Denmuki)
				seenSpied(card.Abomihoney)

				if chapterNumber > 3 {
					seenSpied(card.HeavyDroneB33)

					seenSpied(card.Carmina)
					seenSpied(card.Broodmother)
					seenSpied(card.Mender)
				}
			}
		}

		fallthrough
	case 2:
		if chapterNumber > 2 || confirm("Have you reached Golden Settlement?") {
			seenSpied(card.Numbnail)
			seenSpied(card.Acornling)
			seenSpied(card.Weevil)
			seenSpied(card.Chomper)
			seenSpied(card.WaspTrooper)
			seenSpied(card.Midge)
			seenSpied(card.Underling)
			seenSpied(card.GoldenSeedling)

			if chapterNumber > 2 || confirm("Have you gained passage to Golden Hills?") {
				seenSpied(card.VenusBud)
				seenSpied(card.AcolyteAria)

				if chapterNumber > 2 {
					seenSpied(card.Zasp)
					seenSpied(card.Mothiva)
					seenSpied(card.VenusGuardian)
				}
			}
		}

		fallthrough
	case 1:
		seenSpied(card.Zombiant)
		seenSpied(card.Jellyshroom)
		seenSpied(card.Spider)
		seenSpied(card.Inichas)
		seenSpied(card.Seedling)
	}

	if chapterNumber >= 3 && confirm("Have you completed the following bounty:\nDevourer") {
		seenSpied(card.Devourer)
	}

	if chapterNumber >= 4 && confirm("Have you completed the following bounty:\nTidal Wyrm") {
		seenSpied(card.TidalWyrm)
	}

	if chapterNumber >= 5 && confirm("Have you completed the following bounty:\nSeedling King") {
		seenSpied(card.SeedlingKing)
	}

	if chapterNumber >= 6 && confirm("Have you completed the following bounty:\nFalse Monarch") {
		seenSpied(card.FalseMonarch)
	}

	if chapterNumber >= 6 && enemyData[76>>2]&(1<<(76&3)) != 0 && confirm("Have you completed the following bounty:\nPeacock Spider") {
		seenSpied(card.PeacockSpider)
	}

	quest := QuestUnavailable

	if chapterNumber > 2 {
		if questCompleted {
			quest = QuestComplete
		} else {
			quest = QuestAvailable
		}
	}

	return &Data{
		Quest:            quest,
		MetCarmina:       questCompleted,
		CarminaApproved:  questCompleted,
		SeenSpiedEnemies: enemyData,
	}
}
