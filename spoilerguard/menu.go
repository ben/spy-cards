package spoilerguard

import (
	"math/bits"

	"git.lubar.me/ben/spy-cards/audio"
	"golang.org/x/mobile/event/key"
)

var (
	menu = []struct {
		high  uint32
		low   uint32
		flag  uint32
		shift uint32
	}{
		{
			high:  1381321031,
			low:   1162149888,
			flag:  1,
			shift: 16,
		},
		{
			high:  1212240452,
			low:   1163088896,
			flag:  2,
			shift: 8,
		},
		{
			high:  1179795789,
			low:   1162825285,
			flag:  4,
			shift: 0,
		},
		{
			high:  1347769160,
			low:   1380926283,
			flag:  8,
			shift: 0,
		},
		{
			high:  1297044037,
			low:   1178686029,
			flag:  16,
			shift: 0,
		},
		{
			high:  1297699668,
			low:   1163024703,
			flag:  32,
			shift: 0,
		},
		{
			high:  1414874694,
			low:   1112885075,
			flag:  23,
			shift: 0,
		},
	}

	menuHist uint64
)

// OnKey handles menu codes for spoiler guard.
func OnKey(e key.Event) {
	if e.Direction == key.DirRelease {
		return
	}

	if e.Rune <= 0 || e.Rune >= 128 {
		return
	}

	if e.Rune >= 97 && e.Rune <= 122 {
		e.Rune &^= 32
	}

	menuHist <<= 8
	menuHist |= uint64(e.Rune)

	for _, m := range menu {
		eff := (uint64(m.high)<<32 | uint64(m.low)) >> m.shift

		if eff == menuHist>>m.shift {
			data := LoadData()
			if data == nil {
				data = &Data{
					Quest:            QuestComplete,
					MetCarmina:       true,
					CarminaApproved:  true,
					Deck:             nil,
					SeenSpiedEnemies: make([]uint8, 32),
				}

				for i := range data.SeenSpiedEnemies {
					data.SeenSpiedEnemies[i] = 255
				}
			}

			if (data.Menu & m.flag) != m.flag {
				data.Menu |= m.flag

				audio.AtkSuccess.PlaySoundGlobal(0, 0, 5)
				SaveData(data)
			}
		} else if bits.ReverseBytes64(eff)>>m.shift == menuHist>>m.shift {
			data := LoadData()
			if data == nil {
				continue
			}

			data.Menu &^= m.flag

			audio.AtkFail.PlaySoundGlobal(0, 0, 5)

			if data.Menu == 0 {
				allSet := true
				for _, b := range data.SeenSpiedEnemies {
					if b != 255 {
						allSet = false

						break
					}
				}

				if allSet {
					data = nil
				}
			}

			SaveData(data)
		}
	}
}
