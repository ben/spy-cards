//go:generate stringer -type QuestProgress -trimprefix Quest
//go:generate stringer -type GuardState -trimprefix State

// Package spoilerguard implements Spoiler Guard, a method of blocking cards
// that would be a spoiler for a Bug Fables player who has not completed
// the game.
package spoilerguard

import (
	"encoding/json"

	"git.lubar.me/ben/spy-cards/card"
	"git.lubar.me/ben/spy-cards/internal"
)

// QuestProgress is a player's progress in the "Requesting Assistance" quest.
type QuestProgress int8

// Constants for QuestProgress.
const (
	QuestUnavailable QuestProgress = -1
	QuestAvailable   QuestProgress = 0
	QuestTaken       QuestProgress = 1
	QuestComplete    QuestProgress = 2
)

// Data is the stored data for Spoiler Guard.
type Data struct {
	Quest            QuestProgress `json:"q"`
	MetCarmina       bool          `json:"t"`
	CarminaApproved  bool          `json:"a"`
	Menu             uint32        `json:"m,omitempty"`
	Deck             card.Deck     `json:"d"`
	SeenSpiedEnemies []uint8       `json:"s"`
}

const localStorageKey = "spy-cards-spoiler-guard-v0"

// LoadData returns the player's stored Spoiler Guard data.
func LoadData() *Data {
	b, err := internal.LoadData(localStorageKey)
	if err != nil {
		panic(err)
	}

	if len(b) == 0 {
		return nil
	}

	var d Data

	if err := json.Unmarshal(b, &d); err != nil {
		panic(err)
	}

	return &d
}

// SaveData stores the player's Spoiler Guard data in persistent storage.
func SaveData(d *Data) {
	if d == nil {
		if err := internal.StoreData(localStorageKey, nil); err != nil {
			panic(err)
		}

		return
	}

	b, err := json.Marshal(d)
	if err != nil {
		panic(err)
	}

	if err := internal.StoreData(localStorageKey, b); err != nil {
		panic(err)
	}
}

// GuardState is a warning level for a player with Spoiler Guard enabled.
// Higher numeric values correspond to less danger of encountering spoilers.
type GuardState uint8

// Constants for GuardState.
const (
	StateQuestLocked GuardState = iota
	StateQuestNotAccepted
	StateQuestNotCompleted
	StateNotMetCarmina
	StateCardsNotApproved
	StateNotAllSeen
	StateNotAllSpied
	StateDisabled
)

// Block returns true if the player should not be allowed to play Spy Cards
// at all in this state.
func (s GuardState) Block() bool {
	switch s {
	case StateQuestLocked,
		StateQuestNotAccepted,
		StateQuestNotCompleted,
		StateNotMetCarmina,
		StateCardsNotApproved:
		return true
	default:
		return false
	}
}

// State returns the GuardState for the player's stored data.
func (d *Data) State() GuardState {
	if d == nil {
		return StateDisabled
	}

	switch d.Quest {
	case QuestUnavailable:
		return StateQuestLocked
	case QuestAvailable:
		return StateQuestNotAccepted
	case QuestTaken:
		return StateQuestNotCompleted
	}

	if !d.MetCarmina {
		return StateNotMetCarmina
	}

	if !d.CarminaApproved {
		return StateCardsNotApproved
	}

	seen, spied := true, true

	for _, id := range cardEnemyIDs {
		i, j := id>>2, (id&3)<<1

		if d.SeenSpiedEnemies[i]&(1<<j) == 0 {
			seen = false
		}

		if d.SeenSpiedEnemies[i]&(2<<j) == 0 {
			spied = false
		}
	}

	if !seen {
		return StateNotAllSeen
	}

	if !spied {
		return StateNotAllSpied
	}

	return StateDisabled
}
