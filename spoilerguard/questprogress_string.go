// Code generated by "stringer -type QuestProgress -trimprefix Quest"; DO NOT EDIT.

package spoilerguard

import "strconv"

func _() {
	// An "invalid array index" compiler error signifies that the constant values have changed.
	// Re-run the stringer command to generate them again.
	var x [1]struct{}
	_ = x[QuestUnavailable - -1]
	_ = x[QuestAvailable-0]
	_ = x[QuestTaken-1]
	_ = x[QuestComplete-2]
}

const _QuestProgress_name = "UnavailableAvailableTakenComplete"

var _QuestProgress_index = [...]uint8{0, 11, 20, 25, 33}

func (i QuestProgress) String() string {
	i -= -1
	if i < 0 || i >= QuestProgress(len(_QuestProgress_index)-1) {
		return "QuestProgress(" + strconv.FormatInt(int64(i+-1), 10) + ")"
	}
	return _QuestProgress_name[_QuestProgress_index[i]:_QuestProgress_index[i+1]]
}
