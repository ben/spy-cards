//go:build !headless && js && wasm
// +build !headless,js,wasm

package main

import (
	"context"
	"fmt"
	"log"
	"runtime"
	"syscall/js"
	"time"

	"git.lubar.me/ben/spy-cards/arcade/touchcontroller"
	"git.lubar.me/ben/spy-cards/audio"
	"git.lubar.me/ben/spy-cards/gfx"
	"git.lubar.me/ben/spy-cards/input"
	"git.lubar.me/ben/spy-cards/internal"
	"git.lubar.me/ben/spy-cards/internal/router"
	"golang.org/x/mobile/gl"
)

var (
	lastDPR float64

	inputContext *input.Context
)

func newGLContext() (gl.Context, gl.Worker) {
	internal.PerformanceMark("webglCreateStart")

	defer func() {
		internal.PerformanceMark("webglCreateFinish")
		internal.PerformanceMeasure("webglCreate", "webglCreateStart", "webglCreateFinish")

		if r := recover(); r != nil {
			js.Global().Get("SpyCards").Get("Native").Call("onGLContextFail", fmt.Sprint(r))

			// hang forever
			select {}
		}
	}()

	return gl.NewContext()
}

var preloadReady = make(chan struct{})

func periodicGC(js.Value, []js.Value) interface{} {
	runtime.GC()

	return js.Undefined()
}

func getStackTrace(js.Value, []js.Value) interface{} {
	buf := make([]byte, 1024)

	for {
		n := runtime.Stack(buf, true)
		if n < len(buf) {
			return string(buf[:n])
		}

		buf = make([]byte, 2*len(buf))
	}
}

func doCrash(js.Value, []js.Value) interface{} {
	go func() {
		panic("crash was requested")
	}()

	return js.Undefined()
}

var ignoreEvent = internal.Function.New("", `return function ignoreEvent(e) { e.preventDefault(); }`).Invoke()

func unsuspendAudio(js.Value, []js.Value) interface{} {
	audio.UnsuspendContext()

	return js.Undefined()
}

func appMain(ictx *input.Context) {
	log.SetFlags(log.Lshortfile)

	if !router.FlagDontForceGC.IsSet() {
		js.Global().Call("setInterval", js.FuncOf(periodicGC), 5000)
	}

	js.Global().Set("goStackTrace", js.FuncOf(getStackTrace))

	js.Global().Set("SIGQUIT", js.FuncOf(doCrash))

	inputContext = ictx

	js.Global().Call("addEventListener", "drop", ignoreEvent)
	js.Global().Call("addEventListener", "click", js.FuncOf(unsuspendAudio), true)

	glctx, worker := newGLContext()
	canvas := glctx.(gl.JSWrapper).JSValue().Get("canvas")
	js.Global().Get("document").Get("body").Call("appendChild", canvas)
	<-preloadReady

	if el := js.Global().Get("document").Call("getElementById", "loading-sub"); el.Truthy() {
		el.Set("innerHTML", "Initializing graphics&hellip;")
		time.Sleep(time.Millisecond)
	}

	gfx.Init(glctx)

	gfx.OnFrameEnd = func() {
		touchcontroller.Render()
	}

	if loading := js.Global().Get("document").Call("getElementById", "loading"); loading.Truthy() {
		loading.Get("parentNode").Call("removeChild", loading)
	}

	registerInputListeners(canvas)

	for range worker.WorkAvailable() {
		worker.DoWork()
	}
}

func doPreload(ctx context.Context, s preloadSet) <-chan struct{} {
	if el := js.Global().Get("document").Call("getElementById", "loading-sub"); el.Truthy() {
		el.Set("innerHTML", "Loading textures and sounds&hellip;")
	}

	ch := s.do(ctx, true)

	<-ch

	close(preloadReady)

	remove := js.Global().Get("document").Call("querySelectorAll", ".hide-on-wasm-ready")
	for i := 0; i < remove.Length(); i++ {
		remove.Index(i).Get("parentNode").Call("removeChild", remove.Index(i))
	}

	return ch
}

func doRedirect(u string) {
	internal.Location.Set("href", u)
}

func pushURL(u string) {
	internal.History.Call("pushState", js.Null(), "", u)
}

var startPath = currentPath()

func currentPath() string {
	pathname := internal.Location.Get("pathname").String()
	search := internal.Location.Get("search").String()
	hash := internal.Location.Get("hash").String()

	return pathname + search + hash
}

func onPopURL(f func(string)) {
	js.Global().Call("addEventListener", "popstate", js.FuncOf(func(js.Value, []js.Value) interface{} {
		audio.StopMusic()
		audio.StopSounds()

		go f(currentPath())

		return js.Undefined()
	}))
}
