package gfx

import (
	"math"
	"unsafe"

	"golang.org/x/mobile/exp/f32"
)

// Vector is a 4-component geometric vector of (x, y, z, w).
//
// w should be 1 for absolute vectors like position and
// 0 for relative vectors like normal.
type Vector [4]float32

// Matrix is a 4x4 transformation matrix.
type Matrix [16]float32

func sincos(r float32) (sin, cos float32) {
	s, c := math.Sincos(float64(r))

	return float32(s), float32(c)
}

// Identity returns the identity matrix.
func (m *Matrix) Identity() {
	*m = Matrix{
		1, 0, 0, 0,
		0, 1, 0, 0,
		0, 0, 1, 0,
		0, 0, 0, 1,
	}
}

// Translation constructs translation matrix.
func (m *Matrix) Translation(x, y, z float32) {
	*m = Matrix{
		1, 0, 0, 0,
		0, 1, 0, 0,
		0, 0, 1, 0,
		x, y, z, 1,
	}
}

// Scale constructs a scaling matrix.
func (m *Matrix) Scale(x, y, z float32) {
	*m = Matrix{
		x, 0, 0, 0,
		0, y, 0, 0,
		0, 0, z, 0,
		0, 0, 0, 1,
	}
}

// RotationX constructs a rotation matrix over the X axis.
func (m *Matrix) RotationX(r float32) {
	s, c := sincos(r)

	*m = Matrix{
		1, 0, 0, 0,
		0, c, -s, 0,
		0, s, c, 0,
		0, 0, 0, 1,
	}
}

// RotationY constructs a rotation matrix over the Y axis.
func (m *Matrix) RotationY(r float32) {
	s, c := sincos(r)

	*m = Matrix{
		c, 0, s, 0,
		0, 1, 0, 0,
		-s, 0, c, 0,
		0, 0, 0, 1,
	}
}

// RotationZ constructs a rotation matrix over the Z axis.
func (m *Matrix) RotationZ(r float32) {
	s, c := sincos(r)

	*m = Matrix{
		c, -s, 0, 0,
		s, c, 0, 0,
		0, 0, 1, 0,
		0, 0, 0, 1,
	}
}

// RotationXYZ constructs a rotation matrix over euler angles.
func (m *Matrix) RotationXYZ(x, y, z float32) {
	sx, cx := sincos(x)
	sy, cy := sincos(y)
	sz, cz := sincos(z)

	*m = Matrix{
		cy*cz - sx*sy*sz,
		-cx * sz,
		cy*sx*sz + cz*sy,
		0,

		cy*sz + cz*sx*sy,
		cx * cz,
		-cy*cz*sx + sy*sz,
		0,

		-cx * sy,
		sx,
		cx * cy,
		0,

		0,
		0,
		0,
		1,
	}
}

// Perspective constructs a perspective matrix.
func (m *Matrix) Perspective(fov, aspect, nearZ, farZ float32) {
	f := 1 / f32.Tan(fov/2)

	*m = Matrix{
		f / aspect, 0, 0, 0,
		0, f, 0, 0,
		0, 0, (farZ + nearZ) / (nearZ - farZ), -1,
		0, 0, (2 * farZ * nearZ) / (nearZ - farZ), 0,
	}
}

// Inverse computes the inverse of the matrix.
//
// This function's implementation is based on the implementation
// from Mesa3D, contributed by Jacques Leroy.
func (m *Matrix) Inverse(m0 *Matrix) bool {
	var (
		wtmp           [4][8]float32
		x, y, z, w, s  float32
		r0, r1, r2, r3 *[8]float32
	)

	r0 = &wtmp[0]
	r1 = &wtmp[1]
	r2 = &wtmp[2]
	r3 = &wtmp[3]

	r0[0] = m0[0]
	r0[1] = m0[4]
	r0[2] = m0[8]
	r0[3] = m0[12]
	r0[4] = 1.0
	r0[5] = 0.0
	r0[6] = 0.0
	r0[7] = 0.0

	r1[0] = m0[1]
	r1[1] = m0[5]
	r1[2] = m0[9]
	r1[3] = m0[13]
	r1[5] = 1.0
	r1[4] = 0.0
	r1[6] = 0.0
	r1[7] = 0.0

	r2[0] = m0[2]
	r2[1] = m0[6]
	r2[2] = m0[10]
	r2[3] = m0[14]
	r2[6] = 1.0
	r2[4] = 0.0
	r2[5] = 0.0
	r2[7] = 0.0

	r3[0] = m0[3]
	r3[1] = m0[7]
	r3[2] = m0[11]
	r3[3] = m0[15]
	r3[7] = 1.0
	r3[4] = 0.0
	r3[5] = 0.0
	r3[6] = 0.0

	// choose pivot
	if math.Abs(float64(r3[0])) > math.Abs(float64(r2[0])) {
		r3, r2 = r2, r3
	}

	if math.Abs(float64(r2[0])) > math.Abs(float64(r1[0])) {
		r2, r1 = r1, r2
	}

	if math.Abs(float64(r1[0])) > math.Abs(float64(r0[0])) {
		r1, r0 = r0, r1
	}

	if r0[0] == 0 {
		// give up
		return false
	}

	// eliminate first variable
	y = r1[0] / r0[0]
	z = r2[0] / r0[0]
	w = r3[0] / r0[0]

	s = r0[1]
	r1[1] -= y * s
	r2[1] -= z * s
	r3[1] -= w * s

	s = r0[2]
	r1[2] -= y * s
	r2[2] -= z * s
	r3[2] -= w * s

	s = r0[3]
	r1[3] -= y * s
	r2[3] -= z * s
	r3[3] -= w * s

	s = r0[4]
	if s != 0 {
		r1[4] -= y * s
		r2[4] -= z * s
		r3[4] -= w * s
	}

	s = r0[5]
	if s != 0 {
		r1[5] -= y * s
		r2[5] -= z * s
		r3[5] -= w * s
	}

	s = r0[6]
	if s != 0 {
		r1[6] -= y * s
		r2[6] -= z * s
		r3[6] -= w * s
	}

	s = r0[7]
	if s != 0 {
		r1[7] -= y * s
		r2[7] -= z * s
		r3[7] -= w * s
	}

	// choose pivot
	if math.Abs(float64(r3[1])) > math.Abs(float64(r2[1])) {
		r3, r2 = r2, r3
	}

	if math.Abs(float64(r2[1])) > math.Abs(float64(r1[1])) {
		r2, r1 = r1, r2
	}

	if r1[1] == 0 {
		// give up
		return false
	}

	// eliminate second variable
	z = r2[1] / r1[1]
	w = r3[1] / r1[1]
	r2[2] -= z * r1[2]
	r3[2] -= w * r1[2]
	r2[3] -= z * r1[3]
	r3[3] -= w * r1[3]

	s = r1[4]
	if s != 0 {
		r2[4] -= z * s
		r3[4] -= w * s
	}

	s = r1[5]
	if s != 0 {
		r2[5] -= z * s
		r3[5] -= w * s
	}

	s = r1[6]
	if s != 0 {
		r2[6] -= z * s
		r3[6] -= w * s
	}

	s = r1[7]
	if s != 0 {
		r2[7] -= z * s
		r3[7] -= w * s
	}

	// choose pivot
	if math.Abs(float64(r3[2])) > math.Abs(float64(r2[2])) {
		r3, r2 = r2, r3
	}

	if r2[2] == 0 {
		// give up
		return false
	}

	// eliminate third variable
	w = r3[2] / r2[2]
	r3[3] -= w * r2[3]
	r3[4] -= w * r2[4]
	r3[5] -= w * r2[5]
	r3[6] -= w * r2[6]
	r3[7] -= w * r2[7]

	// last check
	if r3[3] == 0 {
		return false
	}

	// now back substitute row 3
	s = 1.0 / r3[3]
	r3[4] *= s
	r3[5] *= s
	r3[6] *= s
	r3[7] *= s

	// now back substitute row 2
	z = r2[3]
	s = 1.0 / r2[2]
	r2[4] = s * (r2[4] - r3[4]*z)
	r2[5] = s * (r2[5] - r3[5]*z)
	r2[6] = s * (r2[6] - r3[6]*z)
	r2[7] = s * (r2[7] - r3[7]*z)
	y = r1[3]
	r1[4] -= r3[4] * y
	r1[5] -= r3[5] * y
	r1[6] -= r3[6] * y
	r1[7] -= r3[7] * y
	x = r0[3]
	r0[4] -= r3[4] * x
	r0[5] -= r3[5] * x
	r0[6] -= r3[6] * x
	r0[7] -= r3[7] * x

	// now back substitute row 1
	y = r1[2]
	s = 1.0 / r1[1]
	r1[4] = s * (r1[4] - r2[4]*y)
	r1[5] = s * (r1[5] - r2[5]*y)
	r1[6] = s * (r1[6] - r2[6]*y)
	r1[7] = s * (r1[7] - r2[7]*y)
	x = r0[2]
	r0[4] -= r2[4] * x
	r0[5] -= r2[5] * x
	r0[6] -= r2[6] * x
	r0[7] -= r2[7] * x

	// now back substitute row 0
	x = r0[1]
	s = 1.0 / r0[0]
	r0[4] = s * (r0[4] - r1[4]*x)
	r0[5] = s * (r0[5] - r1[5]*x)
	r0[6] = s * (r0[6] - r1[6]*x)
	r0[7] = s * (r0[7] - r1[7]*x)

	m[0] = r0[4]
	m[4] = r0[5]
	m[8] = r0[6]
	m[12] = r0[7]
	m[1] = r1[4]
	m[5] = r1[5]
	m[9] = r1[6]
	m[13] = r1[7]
	m[2] = r2[4]
	m[6] = r2[5]
	m[10] = r2[6]
	m[14] = r2[7]
	m[3] = r3[4]
	m[7] = r3[5]
	m[11] = r3[6]
	m[15] = r3[7]

	return true
}

func (m *Matrix) Determinant() float32 {
	a := m[0]*m[5] - m[1]*m[4]
	b := m[0]*m[6] - m[2]*m[4]
	c := m[0]*m[7] - m[3]*m[4]
	d := m[1]*m[6] - m[2]*m[5]
	e := m[1]*m[7] - m[3]*m[5]
	f := m[2]*m[7] - m[3]*m[6]
	g := m[8]*m[13] - m[9]*m[12]
	h := m[8]*m[14] - m[10]*m[12]
	i := m[8]*m[15] - m[11]*m[12]
	j := m[9]*m[14] - m[10]*m[13]
	k := m[9]*m[15] - m[11]*m[13]
	l := m[10]*m[15] - m[11]*m[14]

	return a*l - b*k + c*j + d*i - e*h + f*g
}

// Multiply multiplies a matrix by a vector.
func (v *Vector) Multiply(m *Matrix, v0 *Vector) {
	v[0] = m[0]*v0[0] + m[4]*v0[1] + m[8]*v0[2] + m[12]*v0[3]
	v[1] = m[1]*v0[0] + m[5]*v0[1] + m[9]*v0[2] + m[13]*v0[3]
	v[2] = m[2]*v0[0] + m[6]*v0[1] + m[10]*v0[2] + m[14]*v0[3]
	v[3] = m[3]*v0[0] + m[7]*v0[1] + m[11]*v0[2] + m[15]*v0[3]
}

// Multiply multiplies a matrix by a matrix.
func (m *Matrix) Multiply(m0, m1 *Matrix) {
	cols := (*[4]Vector)(unsafe.Pointer(m1))
	rows := (*[4]Vector)(unsafe.Pointer(m))

	rows[0].Multiply(m0, &cols[0])
	rows[1].Multiply(m0, &cols[1])
	rows[2].Multiply(m0, &cols[2])
	rows[3].Multiply(m0, &cols[3])
}

func (m *Matrix) FunHouse(x, y, divisor, falloff, offX, offY, scale, hover float32) {
	rotX := offY * (0.3 + hover*0.6) / falloff
	rotY := -offX * (0.3 + hover*0.6) / falloff
	sx, cx := sincos(rotX)
	sy, cy := sincos(rotY)
	nearZ, farZ := float32(0.3), float32(150.0)
	nzfz := (nearZ + farZ) / (nearZ - farZ)

	*m = Matrix{
		4*cx*scale - 4*cx*sy*x/divisor,
		-4*cx*sy*y/divisor + 4*scale*sx*sy,
		4 * cx * scale * sy * nzfz,
		-4 * cx * sy / divisor,
		4 * sx * x / divisor,
		4*cx*scale + 4*sx*y/divisor,
		-4 * scale * sx * nzfz,
		4 * sx / divisor,
		-4*cx*cy*x/divisor - 4*scale*sy,
		-4*cx*cy*y/divisor + 4*cy*scale*sx,
		4 * cx * cy * scale * nzfz,
		-4 * cx * cy / divisor,
		4 * x,
		4 * y,
		divisor * scale * (2*nearZ*farZ/(nearZ-farZ) - 4*nzfz),
		4,
	}
}
