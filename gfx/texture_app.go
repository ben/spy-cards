//go:build (!js || !wasm) && !headless
// +build !js !wasm
// +build !headless

package gfx

import (
	"bytes"
	"fmt"
	"image"
	"image/draw"
	"image/jpeg"
	"image/png"
	"io"
	"strings"

	"git.lubar.me/ben/spy-cards/internal"
	"git.lubar.me/ben/spy-cards/internal/webp"
	"golang.org/x/mobile/gl"
)

// NewTexture creates a texture.
func NewTexture(name string, img image.Image, nearest, noMip, noClamp, premultiplyAlpha bool) *Texture {
	rgba, ok := img.(*image.RGBA)
	if premultiplyAlpha && !ok {
		bounds := img.Bounds()

		rgba = image.NewRGBA(bounds.Sub(bounds.Min))

		draw.Draw(rgba, rgba.Rect, img, bounds.Min, draw.Src)
	} else if nrgba, ok := img.(*image.NRGBA); ok && !premultiplyAlpha {
		rgba = (*image.RGBA)(nrgba)
	} else if !premultiplyAlpha {
		bounds := img.Bounds()

		nrgba = image.NewNRGBA(bounds.Sub(bounds.Min))

		draw.Draw(nrgba, nrgba.Rect, img, bounds.Min, draw.Src)

		rgba = (*image.RGBA)(nrgba)
	}

	t := &Texture{
		Name:    name,
		pix:     rgba.Pix,
		width:   rgba.Rect.Dx(),
		height:  rgba.Rect.Dy(),
		nearest: nearest,
		noMip:   noMip,
		noClamp: noClamp,
	}
	t.init = t.defaultInit

	onNewTexture(t)

	return t
}

func TexStorage2D(target gl.Enum, levels int, format, ty, sizedformat gl.Enum, width, height int) {
	GL.TexImage2D(target, 0, int(format), width, height, format, ty, nil)
}

func fetchAssetTexture(name string, downScale int, nearest, noMip, noClamp, premultiplyAlpha bool) (*Texture, error) {
	f, err := internal.OpenAsset(name)
	if err != nil {
		return nil, fmt.Errorf("gfx: opening texture asset %q: %w", name, err)
	}
	defer f.Close()

	data, err := io.ReadAll(f)
	if err != nil {
		return nil, fmt.Errorf("gfx: reading texture asset %q: %w", name, err)
	}

	mimeType := "image/png"
	if strings.HasSuffix(name, ".webp") {
		mimeType = "image/webp"
	}

	return newDataTexture(name, data, mimeType, downScale, nearest, noMip, noClamp, premultiplyAlpha)
}

func newDataTexture(name string, b []byte, mime string, downScale int, nearest, noMip, noClamp, premultiplyAlpha bool) (*Texture, error) {
	var (
		img image.Image
		err error
	)

	switch mime {
	case "image/png":
		img, err = png.Decode(bytes.NewReader(b))
	case "image/jpeg":
		img, err = jpeg.Decode(bytes.NewReader(b))
	case "image/webp":
		img, err = webp.DecodeNRGBA(b)
	default:
		err = fmt.Errorf("gfx: unexpected texture MIME type %q", mime)
	}

	if err != nil {
		return nil, fmt.Errorf("gfx: for texture %q: %w", name, err)
	}

	return NewTexture(name, img, nearest, noMip, noClamp, premultiplyAlpha), nil
}
