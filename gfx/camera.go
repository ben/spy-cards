package gfx

import "math"

// Camera tracks the position and orientation of the 3D camera.
type Camera struct {
	Offset      Matrix
	Rotation    Matrix
	Position    Matrix
	Perspective Matrix
	stack       []Matrix

	combined      Matrix
	cacheCombined bool
	Invert        bool
}

// SetDefaults sets the Matrix fields in Camera to default values.
func (c *Camera) SetDefaults() {
	c.Offset.Translation(0, 2.25, -8.25)
	c.Rotation.RotationX(10 * math.Pi / 180)
	c.Position.Translation(0, 70, 0)
	c.Perspective.Perspective(-60*math.Pi/180, 16.0/9.0, 0.3, 150)
	c.cacheCombined = false

	if len(c.stack) == 0 {
		c.Invert = true
		c.stack = make([]Matrix, 1, 16)
		c.stack[0].Identity()
	}
}

// PushTranslation is a helper function to push a translation-based
// transform to the stack.
func (c *Camera) PushTranslation(x, y, z float32) {
	var m Matrix

	m.Translation(x, y, z)

	c.PushTransform(&m)
}

// PushTransform adds a transform to the transform stack.
func (c *Camera) PushTransform(m *Matrix) {
	l := len(c.stack)
	if l == cap(c.stack) {
		c.stack = append(c.stack, Matrix{})
	} else {
		c.stack = c.stack[:l+1]
	}

	c.stack[l].Multiply(&c.stack[l-1], m)

	c.cacheCombined = false
}

// PopTransform removes the last transform added via PushTransform.
func (c *Camera) PopTransform() {
	c.stack = c.stack[:len(c.stack)-1]

	c.cacheCombined = false
}

func (c *Camera) MarkDirty() {
	c.cacheCombined = false
}

// Combined returns the combined transformation matrix.
func (c *Camera) Combined(m *Matrix) {
	if !c.cacheCombined {
		var m0, m1, sn1 Matrix

		m0.Multiply(&c.Offset, &c.Rotation)
		m1.Multiply(&m0, &c.Position)

		if c.Invert {
			sn1.Scale(-1, -1, -1)
			m0.Multiply(&m1, &sn1)
		} else {
			m0 = m1
		}

		c.combined.Multiply(&m0, &c.stack[len(c.stack)-1])

		c.cacheCombined = true
	}

	*m = c.combined
}
