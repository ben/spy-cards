//go:build js && wasm && !headless
// +build js,wasm,!headless

package gfx

import (
	"log"
	"runtime"
	"strings"
	"syscall/js"
	"unsafe"

	"git.lubar.me/ben/spy-cards/internal"
	"golang.org/x/mobile/gl"
)

var (
	requestAnimationFrame = func() js.Value {
		if rpaf := js.Global().Get("requestPostAnimationFrame"); rpaf.Truthy() {
			return rpaf
		}

		return js.Global().Get("requestAnimationFrame")
	}()

	outstandingFrameRequest bool

	frameCh = make(chan bool, 1)
	onFrame = js.FuncOf(func(js.Value, []js.Value) interface{} {
		select {
		case frameCh <- true:
		default:
			log.Println("WARNING: dropping requestAnimationFrame signal")
		}

		return js.Undefined()
	})

	_ = js.Global().Call("setInterval", js.FuncOf(func(js.Value, []js.Value) interface{} {
		if js.Global().Get("document").Get("visibilityState").String() == "hidden" {
			select {
			case frameCh <- false:
			default:
			}
		}

		return js.Undefined()
	}), 50)

	// OnFrameEnd is an internal variable.
	OnFrameEnd = func() {}
)

// NextFrame synchronizes a frame boundary.
func NextFrame() bool {
	OnFrameEnd()

	if !outstandingFrameRequest {
		requestAnimationFrame.Invoke(onFrame)

		outstandingFrameRequest = true
	}

	statFinishFrame()

	shouldRender := <-frameCh

	if shouldRender {
		outstandingFrameRequest = false
	}

	statStartFrame()

	Lock.Lock()
	for GL == nil {
		onInit.Wait()
	}

	if shouldRender {
		GL.Clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT)
	}
	Lock.Unlock()

	return shouldRender
}

var invalidateFramebuffer = func() js.Value {
	var jsGo js.Value

	*(*uint64)(unsafe.Pointer(&jsGo)) = 0x7FF8000100000006

	return internal.Function.New("jsgo", `return function invalidateFramebuffer(gl, fb1, fb2, ptr, len) {
	if (gl.invalidateFramebuffer) {
		gl.bindFramebuffer(gl.FRAMEBUFFER, fb1);
		gl.invalidateFramebuffer(gl.FRAMEBUFFER, new Uint32Array(jsgo.mem.buffer, ptr, len));
	}

	gl.bindFramebuffer(gl.FRAMEBUFFER, fb2);
}`).Invoke(jsGo)
}()

func InvalidateFramebuffer(fb, reset gl.Framebuffer, attachments ...gl.Enum) {
	var fb1, fb2 js.Value

	if fb.Value != nil {
		fb1 = *fb.Value
	}

	if reset.Value != nil {
		fb2 = *reset.Value
	}

	invalidateFramebuffer.Invoke(GL.(gl.JSWrapper).JSValue(), fb1, fb2, uintptr(unsafe.Pointer(&attachments[0])), len(attachments))

	runtime.KeepAlive(attachments)
}

func gatherRendererInfo() string {
	s := "\n\nVendor: " + GL.GetString(gl.VENDOR) + " (masked)\nRenderer: " + GL.GetString(gl.RENDERER) + " (masked)"

	ext := GL.(gl.JSWrapper).JSValue().Call("getExtension", "WEBGL_debug_renderer_info")
	if ext.Truthy() {
		s += "\nVendor: " + GL.GetString(gl.Enum(ext.Get("UNMASKED_VENDOR_WEBGL").Int()))
		s += "\nRenderer: " + GL.GetString(gl.Enum(ext.Get("UNMASKED_RENDERER_WEBGL").Int()))
	} else {
		s += "\nWEBGL_debug_renderer_info extension unavailable"
	}

	return s
}

func jsLoadExtension(name string) js.Value {
	return GL.(gl.JSWrapper).JSValue().Call("getExtension", name)
}

func platformGPULevelDefault() GPUPowerLevel {
	if strings.Contains(GL.GetString(gl.RENDERER), "Intel") {
		return GPUMedium
	}

	if ext := jsLoadExtension("WEBGL_debug_renderer_info"); ext.Truthy() && strings.Contains(GL.GetString(gl.Enum(ext.Get("UNMASKED_RENDERER_WEBGL").Int())), "Intel") {
		return GPUMedium
	}

	return GPUHigh
}
