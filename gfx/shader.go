//go:build !headless
// +build !headless

package gfx

import (
	"fmt"
	"log"
	"strings"

	"golang.org/x/mobile/gl"
)

// Program is an OpenGL shader.
type Program struct {
	Name     string
	vs2, fs2 string
	vs3, fs3 string
	Program  gl.Program
	uniforms []*Uniform
	attribs  []*Attrib
}

var programs []*Program

// Shader registers an OpenGL shader.
func Shader(name, vertex2, fragment2, vertex3, fragment3 string) *Program {
	p := &Program{Name: name, vs2: vertex2, fs2: fragment2, vs3: vertex3, fs3: fragment3}

	Lock.Lock()

	programs = append(programs, p)

	if GL != nil {
		p.init()()
	}

	Lock.Unlock()

	return p
}

type ErrShaderLink struct {
	Name string
	Vert string
	Frag string
	Link string

	Message string
	Extra   string
}

func (err ErrShaderLink) Error() string {
	return fmt.Sprintf("gl: %s for %q:\n\nvertex:\n%s\n\nfragment:\n%s\n\nlink:\n%s%s", err.Message, err.Name, err.Vert, err.Frag, err.Link, err.Extra)
}

var fixNul = strings.NewReplacer("\x00", "␀")

func (p *Program) init() func() {
	vertex := GL.CreateShader(gl.VERTEX_SHADER)
	fragment := GL.CreateShader(gl.FRAGMENT_SHADER)

	if _, ok := GL.(gl.Context3); ok {
		GL.ShaderSource(vertex, p.vs3)
		GL.ShaderSource(fragment, p.fs3)
	} else {
		GL.ShaderSource(vertex, p.vs2)
		GL.ShaderSource(fragment, p.fs2)
	}

	GL.CompileShader(vertex)
	GL.CompileShader(fragment)

	p.Program = GL.CreateProgram()
	GL.AttachShader(p.Program, vertex)
	GL.AttachShader(p.Program, fragment)

	GL.LinkProgram(p.Program)

	return func() {
		defer GL.DeleteShader(vertex)
		defer GL.DeleteShader(fragment)

		linked := GL.GetProgrami(p.Program, gl.LINK_STATUS) != 0
		vert := strings.TrimSpace(fixNul.Replace(GL.GetShaderInfoLog(vertex)))
		frag := strings.TrimSpace(fixNul.Replace(GL.GetShaderInfoLog(fragment)))
		link := strings.TrimSpace(fixNul.Replace(GL.GetProgramInfoLog(p.Program)))

		if !linked {
			panic(&ErrShaderLink{
				Name: p.Name,
				Vert: vert,
				Frag: frag,
				Link: link,

				Message: "failed to link shader",
				Extra:   gatherRendererInfo(),
			})
		} else if vert != "" || frag != "" || link != "" {
			log.Println("WARNING:", &ErrShaderLink{
				Name: p.Name,
				Vert: vert,
				Frag: frag,
				Link: link,

				Message: "while compiling shader",
				Extra:   gatherRendererInfo(),
			})
		}

		for _, u := range p.uniforms {
			u.Uniform = GL.GetUniformLocation(p.Program, u.Name)
		}

		for _, a := range p.attribs {
			a.Attrib = GL.GetAttribLocation(p.Program, a.Name)
		}
	}
}

func (p *Program) release() {
	GL.DeleteProgram(p.Program)

	p.Program = gl.Program{}
}

// Uniform is a global variable.
type Uniform struct {
	Uniform gl.Uniform
	Name    string
	program *Program
}

// Attrib is a vertex attribute.
type Attrib struct {
	Attrib  gl.Attrib
	Name    string
	program *Program
}

// Uniform gets the location of a global variable.
func (p *Program) Uniform(name string) *Uniform {
	u := &Uniform{
		Name:    name,
		program: p,
	}

	if p.Program.Init {
		u.Uniform = GL.GetUniformLocation(p.Program, name)
	}

	p.uniforms = append(p.uniforms, u)

	return u
}

// Attrib gets the location of a vertex attribute.
func (p *Program) Attrib(name string) *Attrib {
	a := &Attrib{
		Name:    name,
		program: p,
	}

	if p.Program.Init {
		a.Attrib = GL.GetAttribLocation(p.Program, name)
	}

	p.attribs = append(p.attribs, a)

	return a
}
