precision mediump float;

uniform vec4 _ScreenParams;
attribute vec4 in_POSITION0;
attribute vec2 in_TEXCOORD0;
varying vec2 vs_TEXCOORD0;
varying vec4 _ScreenParams2;

void main()
{
	float wantAspect = 16.0 / 9.0;
	float aspect = _ScreenParams.x / _ScreenParams.y;

	vec2 scale = vec2(
		aspect > wantAspect ? wantAspect / aspect : 1.0,
		aspect < wantAspect ? aspect / wantAspect : 1.0
	);

	_ScreenParams2 = vec4(
		_ScreenParams.x * scale.x,
		_ScreenParams.y * scale.y,
		1.0 + 1.0 / (_ScreenParams.x * scale.x),
		1.0 + 1.0 / (_ScreenParams.y * scale.y)
	);

	vs_TEXCOORD0 = in_TEXCOORD0;
	gl_Position = vec4(
		in_POSITION0.x * scale.x,
		in_POSITION0.y * scale.y,
		in_POSITION0.zw
	);
}
