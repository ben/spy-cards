precision mediump float;

// xy: _Offset
// z: _NoiseX
// w: _RGBNoise
uniform vec4 packed_0;

// x: _SineNoiseWidth
// y: _SineNoiseScale
// z: _SineNoiseOffset
// w: _ScanLineTail
uniform vec4 packed_1;

// x: _ScanLineSpeed
// y: _ScreenSize
// z: _Darkening
// w: _FadeOut
uniform vec4 packed_2;

uniform float _Time;

uniform bool _SubPixel;

uniform vec4 _ScreenParams;
uniform sampler2D _MainTex;
varying vec2 vs_TEXCOORD0;
varying vec4 _ScreenParams2;

float doNoise(vec2 seed) {
	float noise = dot(seed, vec2(12.9898005, 78.2330017));
	noise = sin(noise);
	noise = noise * 43758.5469;
	noise = fract(noise);
	return noise;
}

void main() {
	vec2 _Offset = packed_0.xy;
	float _NoiseX = packed_0.z;
	float _RGBNoise = packed_0.w;
	float _SineNoiseWidth = packed_1.x;
	float _SineNoiseScale = packed_1.y;
	float _SineNoiseOffset = packed_1.z;
	float _ScanLineTail = packed_1.w;
	float _ScanLineSpeed = packed_2.x;
	float _ScreenSize = packed_2.y;
	float _Darkening = packed_2.z;
	float _FadeOut = packed_2.w;

	vec2 posCentered = vs_TEXCOORD0 - 0.5;
	float distanceFromCenter = length(posCentered);
	float distanceFromEdge = 1.0 - distanceFromCenter * _ScreenSize;
	vec2 posNormal = posCentered / distanceFromEdge;
	float distancePastEdge = max(abs(posNormal.x) - 0.5, abs(posNormal.y) - 0.5);
	if (distancePastEdge > 0.0) {
		discard;
	}

	vec2 texPos = posNormal + 0.5;

	float noise1 = sin(texPos.y * _SineNoiseWidth + _SineNoiseOffset);
	texPos.x = noise1 * _SineNoiseScale + texPos.x;
	texPos = texPos + vec2(_Offset.x, _Offset.y);

	float noise2 = doNoise(vec2(floor(texPos.y * 500.0) + _Time)) - 0.5;
	texPos.x = noise2 * _NoiseX + texPos.x;
	texPos = texPos - floor(texPos);

	float noiseFactor = doNoise(vec2(floor(texPos.y * 500.0) + _Time));
	noiseFactor = doNoise(vec2(noiseFactor + _Time - 0.5));

	texPos.y = 1.0 - texPos.y;

	vec4 pixelColor;
	if (noiseFactor < _RGBNoise) {
		pixelColor = vec4(
			doNoise(posNormal + vec2(_Time + 123.0, 0.0)),
			doNoise(posNormal + vec2(_Time + 123.0, 1.0)),
			doNoise(posNormal + vec2(_Time + 123.0, 2.0)),
			1.0
		);
	} else {
		vec4 color0 = texture2D(_MainTex, texPos);
		vec4 color1 = texture2D(_MainTex, texPos - vec2(0.002, 0.0));
		vec4 color2 = texture2D(_MainTex, texPos - vec2(0.004, 0.0));

		pixelColor = vec4(color0.r, color1.g, color2.b, 1.0);
	}

	float subPixelPos = vs_TEXCOORD0.x * _ScreenParams2.x;
	subPixelPos = fract(subPixelPos / 3.0) * 3.0;

	vec4 subPixelModifiedColor = pixelColor;
	if (_SubPixel) {
		bool isLeft = subPixelPos < 1.0;
		bool isRight = subPixelPos >= 2.0;
		bool isWeirdPinkColumn = (posCentered.x > 0.0 ?
			posCentered.x <= 5.0/24.0 && posCentered.x + 1.0 / _ScreenParams2.x >= 5.0/24.0 :
			posCentered.x - 1.0 / _ScreenParams2.x >= -5.0/24.0 && posCentered.x - 2.0 / _ScreenParams2.x <= -5.0/24.0) && noiseFactor >= _RGBNoise;
		subPixelModifiedColor = vec4(
			!isLeft && (!isWeirdPinkColumn || !isRight) ? pixelColor.r : 0.0,
			(isLeft && !isWeirdPinkColumn) || isRight ? pixelColor.g : 0.0,
			!isRight && (!isWeirdPinkColumn || isLeft) ? pixelColor.b : 0.0,
			pixelColor.a
		);
	}

	float scanLine = fract(_Time * _ScanLineSpeed + posNormal.y);
	scanLine = (scanLine + _ScanLineTail - 1.0) / min(_ScanLineTail, 1.0);
	scanLine = clamp(scanLine, 0.0, 1.0);

	vec3 finalColor = vec3(scanLine) * subPixelModifiedColor.rgb;
	float edgeDarkening = 1.0 - distanceFromCenter * _Darkening;

	gl_FragColor = vec4(vec3(edgeDarkening) * finalColor * _FadeOut, 1.0);
}
