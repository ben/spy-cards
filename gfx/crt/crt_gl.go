//go:build !headless
// +build !headless

package crt

import (
	_ "embed" // embedded shaders
	"time"

	"git.lubar.me/ben/spy-cards/gfx"
	"git.lubar.me/ben/spy-cards/internal"
	"golang.org/x/mobile/gl"
)

// DisableCRT bypasses the CRT effect.
var DisableCRT = internal.LoadSettings().DisableCRT

var (
	//go:embed crt.vs
	crtVertex string
	//go:embed crt.fs
	crtFragment string

	crtProgram = gfx.Shader("crt", crtVertex, crtFragment, crtVertex, crtFragment)

	crtMainTex      = crtProgram.Uniform("_MainTex")
	crtPacked0      = crtProgram.Uniform("packed_0")
	crtPacked1      = crtProgram.Uniform("packed_1")
	crtPacked2      = crtProgram.Uniform("packed_2")
	crtTime         = crtProgram.Uniform("_Time")
	crtSubPixel     = crtProgram.Uniform("_SubPixel")
	crtScreenParams = crtProgram.Uniform("_ScreenParams")

	posAttrib = crtProgram.Attrib("in_POSITION0")
	texAttrib = crtProgram.Attrib("in_TEXCOORD0")

	crtFrameBuffer  gl.Framebuffer
	crtFrameTexture gl.Texture
	crtFrameDepth   gl.Renderbuffer

	_ = gfx.Custom("crt buffers", crtInit, crtRelease)
)

func crtInit() {
	crtFrameBuffer = gfx.GL.CreateFramebuffer()
	gfx.GL.BindFramebuffer(gl.FRAMEBUFFER, crtFrameBuffer)

	crtFrameTexture = gfx.GL.CreateTexture()
	gfx.GL.ActiveTexture(gl.TEXTURE0)
	gfx.GL.BindTexture(gl.TEXTURE_2D, crtFrameTexture)
	gfx.GL.TexImage2D(gl.TEXTURE_2D, 0, gl.RGBA, 1024, 1024, gl.RGBA, gl.UNSIGNED_BYTE, nil)
	gfx.GL.TexParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.LINEAR)
	gfx.GL.TexParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR)
	gfx.GL.TexParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE)
	gfx.GL.TexParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE)
	gfx.GL.GenerateMipmap(gl.TEXTURE_2D)
	gfx.GL.FramebufferTexture2D(gl.FRAMEBUFFER, gl.COLOR_ATTACHMENT0, gl.TEXTURE_2D, crtFrameTexture, 0)

	crtFrameDepth = gfx.GL.CreateRenderbuffer()
	gfx.GL.BindRenderbuffer(gl.RENDERBUFFER, crtFrameDepth)
	gfx.GL.RenderbufferStorage(gl.RENDERBUFFER, gl.DEPTH_COMPONENT16, 1024, 1024)
	gfx.GL.FramebufferRenderbuffer(gl.FRAMEBUFFER, gl.DEPTH_ATTACHMENT, gl.RENDERBUFFER, crtFrameDepth)

	gfx.GL.BindFramebuffer(gl.FRAMEBUFFER, gl.Framebuffer{})
}

func crtRelease() {
	gfx.GL.DeleteFramebuffer(crtFrameBuffer)
	gfx.GL.DeleteTexture(crtFrameTexture)
	gfx.GL.DeleteRenderbuffer(crtFrameDepth)

	crtFrameBuffer = gl.Framebuffer{}
	crtFrameDepth = gl.Renderbuffer{}
	crtFrameTexture = gl.Texture{}
}

var startTime = time.Now()

// Draw renders a frame.
func (c *CRT) Draw(render func()) {
	width, height := gfx.PixelSize()

	gfx.Lock.Lock()

	gfx.GL.BindFramebuffer(gl.FRAMEBUFFER, crtFrameBuffer)
	gfx.GL.Viewport(0, 0, 1024, 1024)
	gfx.GL.Clear(gl.DEPTH_BUFFER_BIT)

	gfx.Lock.Unlock()

	render()

	gfx.Lock.Lock()

	gfx.GL.BindFramebuffer(gl.FRAMEBUFFER, gl.Framebuffer{})
	gfx.GL.ClearColor(0, 0, 0, 1)
	gfx.GL.Clear(gl.COLOR_BUFFER_BIT)
	gfx.GL.Viewport(0, 0, width, height)
	gfx.GL.UseProgram(crtProgram.Program)

	gfx.GL.ActiveTexture(gl.TEXTURE0)
	gfx.GL.BindTexture(gl.TEXTURE_2D, crtFrameTexture)

	gfx.GL.Uniform1i(crtMainTex.Uniform, 0)
	gfx.GL.Uniform4f(crtScreenParams.Uniform, float32(width), float32(height), 1+1/float32(width), 1+1/float32(height))

	if c.UseTime {
		gfx.GL.Uniform1f(crtTime.Uniform, float32(time.Since(startTime).Seconds()))
	} else {
		gfx.GL.Uniform1f(crtTime.Uniform, 0)
	}

	if DisableCRT {
		gfx.GL.Uniform1i(crtSubPixel.Uniform, 0)
	} else {
		gfx.GL.Uniform1i(crtSubPixel.Uniform, 1)
	}

	gfx.GL.Uniform4f(crtPacked0.Uniform, c.Offset[0], c.Offset[1], c.NoiseX, c.RGBNoise)
	gfx.GL.Uniform4f(crtPacked1.Uniform, c.SineNoiseWidth, c.SineNoiseScale, c.SineNoiseOffset, c.ScanLineTail)
	gfx.GL.Uniform4f(crtPacked2.Uniform, c.ScanLineSpeed, c.ScreenSize, c.Darkening, c.FadeOut)

	// for some reason, the VAO path isn't working here
	// just bind the buffer the old fashioned way
	if _, haveVAO := gfx.GL.(gl.Context3); haveVAO {
		gfx.GL.BindVertexArray(gl.VertexArray{})
	}

	gfx.GL.BindBuffer(gl.ARRAY_BUFFER, square.Data)
	gfx.GL.BindBuffer(gl.ELEMENT_ARRAY_BUFFER, square.Element)
	square.SetPointers()

	gfx.GL.DrawElements(gl.TRIANGLES, square.Count, gl.UNSIGNED_SHORT, 0)

	square.DisableAttribs()

	gfx.InvalidateFramebuffer(crtFrameBuffer, gl.Framebuffer{}, gl.COLOR_ATTACHMENT0, gl.DEPTH_ATTACHMENT)

	gfx.Lock.Unlock()
}

const m1 = ^uint8(0)

var square = gfx.NewStaticBuffer("crt", []uint8{
	m1, +1, m1, 1, 0, 0,
	m1, m1, m1, 1, 0, 1,
	+1, m1, +1, 1, 1, 1,
	+1, +1, +1, 1, 1, 0,
}, []uint8{0, 0, 1, 0, 2, 0, 0, 0, 2, 0, 3, 0}, 2, func() {
	gfx.GL.EnableVertexAttribArray(posAttrib.Attrib)
	gfx.GL.EnableVertexAttribArray(texAttrib.Attrib)

	gfx.GL.VertexAttribPointer(posAttrib.Attrib, 4, gl.BYTE, false, 6, 0)
	gfx.GL.VertexAttribPointer(texAttrib.Attrib, 2, gl.BYTE, false, 6, 4)
}, func() {
	// leave attribs 0 and 1 enabled
})
