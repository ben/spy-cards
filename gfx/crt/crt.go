// Package crt renders a screen-space CRT monitor effect.
package crt

// CRT is a screen-space graphical filter.
type CRT struct {
	NoiseX          float32
	Offset          [2]float32
	RGBNoise        float32
	SineNoiseWidth  float32
	SineNoiseScale  float32
	SineNoiseOffset float32
	ScanLineTail    float32
	ScanLineSpeed   float32
	ScreenSize      float32
	Darkening       float32
	ScanLineDarker  float32
	ScanLineDarker2 float32
	FadeOut         float32
	UseTime         bool
}

// SetDefaults sets the CRT settings to their default values.
func (c *CRT) SetDefaults() {
	c.NoiseX = 0
	c.Offset[0] = 0
	c.Offset[1] = 0
	c.RGBNoise = 0
	c.SineNoiseWidth = 0
	c.SineNoiseScale = 0
	c.SineNoiseOffset = 0
	c.ScanLineTail = 2
	c.ScanLineSpeed = 100
	c.ScreenSize = 0.15
	c.Darkening = 0.1
	c.ScanLineDarker = 4
	c.ScanLineDarker2 = 0.5
	c.FadeOut = 1
}
