//go:build headless
// +build headless

package crt

func (c *CRT) Draw(render func()) {
	render()
}
