//go:build js && wasm && !headless
// +build js,wasm,!headless

package gfx

import (
	"fmt"
	"log"
	"strings"
	"syscall/js"

	"git.lubar.me/ben/spy-cards/internal"
	"git.lubar.me/ben/spy-cards/internal/router"
	"golang.org/x/mobile/gl"
)

// UseSoftWebP is true if loading WebP images should use the WebAssembly-based
// worker rather than using the browser's native decoding.
var UseSoftWebP bool

// UseFirefoxWorkaround is true if the alpha premultiplication state is set
// when uploading the texture rather than when decoding it.
var UseFirefoxWorkaround bool

var emptyFunc = internal.Function.New("", "return function empty() {}").Invoke()

func createDOMImage(blob js.Value, _ bool) js.Value {
	u := internal.CreateObjectURL(blob)

	jsImg := js.Global().Get("Image").New()

	var resolveFunc js.Func

	resolveFunc = js.FuncOf(func(_ js.Value, args []js.Value) interface{} {
		resolveFunc.Release()

		resolve := args[0]

		var onLoadEnd js.Func

		onLoadEnd = js.FuncOf(func(js.Value, []js.Value) interface{} {
			onLoadEnd.Release()

			internal.RevokeObjectURL(u)

			resolve.Invoke(jsImg)

			return js.Undefined()
		})

		jsImg.Set("onload", onLoadEnd)
		jsImg.Set("onerror", onLoadEnd)
		jsImg.Set("src", u)
		jsImg.Set("close", emptyFunc)

		return js.Undefined()
	})

	promise := internal.Promise.New(resolveFunc)

	return promise
}

var createImageBitmap = func() func(js.Value, bool) js.Value {
	// check whether WebP is supported at all.
	if router.FlagForceSoftWebP.IsSet() {
		UseSoftWebP = true
	} else {
		jsImg := js.Global().Get("Image").New()
		f := js.FuncOf(func(_ js.Value, args []js.Value) interface{} {
			jsImg.Set("onload", args[0])
			jsImg.Set("onerror", args[1])
			jsImg.Set("src", "data:image/webp;base64,UklGRi4AAABXRUJQVlA4ICIAAABwAQCdASoBAAEAD8D+JaACdAFAAAD+y7XJ+l7SXS4tAAAA")

			return js.Undefined()
		})
		_, err := internal.AwaitNoRandomFail(js.Global().Get("Promise").New(f))
		f.Release()

		if err != nil {
			log.Println("WARNING: WebP is not supported by this browser. falling back to software decoding.")
			UseSoftWebP = true
		}
	}

	cib := js.Global().Get("createImageBitmap")
	if !cib.Truthy() || router.FlagForceSafariCompat.IsSet() {
		// Safari doesn't support createImageBitmap at all.

		UseFirefoxWorkaround = true

		return createDOMImage
	}

	opts := map[bool]js.Value{
		true: js.ValueOf(map[string]interface{}{
			"premultiplyAlpha": "premultiply",
		}),
		false: js.ValueOf(map[string]interface{}{
			"premultiplyAlpha": "none",
		}),
	}

	// tiny (67 byte) png - https://garethrees.org/2007/11/14/pngcrush/
	testFile := []byte{
		0x89, 0x50, 0x4e, 0x47, 0x0d, 0x0a, 0x1a, 0x0a, 0x00, 0x00, 0x00, 0x0d,
		0x49, 0x48, 0x44, 0x52, 0x00, 0x00, 0x00, 0x01, 0x00, 0x00, 0x00, 0x01,
		0x08, 0x06, 0x00, 0x00, 0x00, 0x1f, 0x15, 0xc4, 0x89, 0x00, 0x00, 0x00,
		0x0a, 0x49, 0x44, 0x41, 0x54, 0x78, 0x9c, 0x63, 0x00, 0x01, 0x00, 0x00,
		0x05, 0x00, 0x01, 0x0d, 0x0a, 0x2d, 0xb4, 0x00, 0x00, 0x00, 0x00, 0x49,
		0x45, 0x4e, 0x44, 0xae, 0x42, 0x60, 0x82,
	}

	u8 := internal.Uint8Array.New(len(testFile))
	js.CopyBytesToJS(u8, testFile)
	blob := internal.Blob.New([]interface{}{u8}, map[string]interface{}{
		"type": "image/png",
	})

	_, err := internal.AwaitNoRandomFail(cib.Invoke(blob, opts[true]))
	if err != nil || router.FlagForceFirefoxCompat.IsSet() {
		UseFirefoxWorkaround = true

		// Firefox doesn't support the options parameter.
		// Just don't pass it and deal with the result requiring re-converting.
		return func(v js.Value, premultiplyAlpha bool) js.Value {
			if !premultiplyAlpha {
				return createDOMImage(v, premultiplyAlpha)
			}

			return cib.Invoke(v)
		}
	}

	// Chrome/Edge support createImageBitmap. Good!
	return func(v js.Value, premultiplyAlpha bool) js.Value {
		return cib.Invoke(v, opts[premultiplyAlpha])
	}
}()

func CreateImageBitmap(blob js.Value, premultiplyAlpha bool) (js.Value, error) {
	if UseSoftWebP && blob.Get("type").String() == "image/webp" {
		return decodeSoftWebP(blob, premultiplyAlpha)
	}

	return internal.AwaitNoRandomFail(createImageBitmap(blob, premultiplyAlpha)) //nolint:wrapcheck
}

func fetchAssetTexture(name string, downScale int, nearest, noMip, noClamp, premultiplyAlpha bool) (*Texture, error) {
	if !strings.Contains(name, ":") {
		name = "/" + name
	}

	native := js.Global().Get("SpyCards").Get("Native")
	if promise := native.Get("preloadedTextures").Get(name); promise.Truthy() {
		native.Get("preloadedTextures").Delete(name)

		jsImg, err := internal.Await(promise)
		if err == nil {
			return jsImgToTexture(name, jsImg, downScale, nearest, noMip, noClamp, premultiplyAlpha)
		}
	}

	req, err := internal.Await(internal.Fetch.Invoke(name, internal.FetchOpts))
	if err != nil {
		return nil, fmt.Errorf("gfx: loading texture %q: %w", name, err)
	}

	blob, err := internal.Await(req.Call("blob"))
	if err != nil {
		return nil, fmt.Errorf("gfx: loading texture %q: %w", name, err)
	}

	jsImg, err := CreateImageBitmap(blob, premultiplyAlpha)
	if err != nil {
		return nil, fmt.Errorf("gfx: loading texture %q: %w", name, err)
	}

	return jsImgToTexture(name, jsImg, downScale, nearest, noMip, noClamp, premultiplyAlpha)
}

func newDataTexture(name string, b []byte, mime string, downScale int, nearest, noMip, noClamp, premultiplyAlpha bool) (*Texture, error) {
	jsBuf := internal.Uint8Array.New(len(b))
	js.CopyBytesToJS(jsBuf, b)

	blob := internal.Blob.New([]interface{}{
		jsBuf,
	}, map[string]interface{}{
		"type": mime,
	})

	jsImg, err := CreateImageBitmap(blob, premultiplyAlpha)
	if !jsImg.Truthy() {
		return nil, fmt.Errorf("gfx: loading texture %q: %w", name, err)
	}

	return jsImgToTexture(name, jsImg, downScale, nearest, noMip, noClamp, premultiplyAlpha)
}

func jsImgToTexture(name string, jsImg js.Value, downScale int, nearest, noMip, noClamp, premultiplyAlpha bool) (*Texture, error) {
	t := &Texture{
		Name:    name,
		nearest: nearest,
		noMip:   noMip,
		noClamp: noClamp,
	}

	t.init = func() {
		jsImgTextureInit(t, jsImg, downScale, nearest, premultiplyAlpha)
	}

	onNewTexture(t)

	return t, nil
}

func TexStorage2D(target gl.Enum, levels int, format, ty, sizedformat gl.Enum, width, height int) {
	ctx := GL.(gl.JSWrapper).JSValue()
	if ctx.Get("texStorage2D").Truthy() {
		ctx.Call("texStorage2D", int(target), levels, int(sizedformat), width, height)
	} else {
		ctx.Call("texImage2D", int(target), 0, int(format), width, height, 0, int(format), int(ty), js.Null())
	}
}

var maxAnisotropy [1]float32

func jsImgTextureInit(t *Texture, jsImg js.Value, downScale int, nearest, premultiplyAlpha bool) {
	defer jsImg.Call("close")

	t.Texture = GL.CreateTexture()
	GL.BindTexture(gl.TEXTURE_2D, t.Texture)

	ctx := GL.(gl.JSWrapper).JSValue()

	ctx.Call("pixelStorei", ctx.Get("UNPACK_PREMULTIPLY_ALPHA_WEBGL"), premultiplyAlpha)

	w, h := jsImg.Get("width").Int(), jsImg.Get("height").Int()
	scale := 1

	if w == 0 || h == 0 {
		js.Global().Get("console").Call("error", jsImg)
	}

	if downScale >= 0 {
		w <<= downScale
		h <<= downScale
	} else {
		w >>= -downScale
		h >>= -downScale
	}

	switch GPULevel {
	case GPUHigh:
		// high-end GPU, can handle basically anything
	case GPUMedium:
		// scale down 8MP and 16MP textures by half
		if w > 2048 || h > 2048 {
			scale = 2
		}
	case GPULow:
		// scale down everything over 1MP
		if w > 1024 || h > 1024 {
			for w/scale > 1024 || h/scale > 1024 {
				scale *= 2
			}
		}
	}

	if downScale >= 0 {
		w >>= downScale
		h >>= downScale
	} else {
		w <<= -downScale
		h <<= -downScale
	}

	if scale != 1 {
		log.Println("INFO: scaling down texture", t.Name, "by a factor of", scale, "to save VRAM")

		if UseFirefoxWorkaround && jsImg.InstanceOf(js.Global().Get("ImageData")) {
			// gotta convert to a canvas *twice*
			canvas := js.Global().Get("document").Call("createElement", "canvas")
			canvas.Set("width", w)
			canvas.Set("height", h)
			canvas.Set("close", emptyFunc)

			ctx2d := canvas.Call("getContext", "2d")
			ctx2d.Call("putImageData", jsImg, 0, 0)

			jsImg = canvas

			defer func() {
				// try to release some memory
				canvas.Set("width", 1)
				canvas.Set("height", 1)
			}()
		}

		w /= scale
		h /= scale

		if UseFirefoxWorkaround {
			canvas := js.Global().Get("document").Call("createElement", "canvas")
			canvas.Set("width", w)
			canvas.Set("height", h)
			canvas.Set("close", emptyFunc)

			ctx2d := canvas.Call("getContext", "2d")
			ctx2d.Set("imageSmoothingEnabled", false)
			ctx2d.Call("drawImage", jsImg, 0, 0, w, h)

			jsImg.Call("close")

			jsImg = canvas

			defer func() {
				// try to release some memory
				canvas.Set("width", 1)
				canvas.Set("height", 1)
			}()
		} else {
			resized, err := internal.AwaitNoRandomFail(js.Global().Call("createImageBitmap", jsImg, map[string]interface{}{
				"premultiplyAlpha": "premultiply",
				"resizeWidth":      w,
				"resizeHeight":     h,
				"resizeQuality":    "pixelated",
			}))
			if err == nil {
				jsImg.Call("close")
				defer resized.Call("close")

				jsImg = resized
			} else {
				log.Println("WARNING: resizing texture:", err)
			}
		}
	}

	if nearest || !isPowerOf2(w) || !isPowerOf2(h) || t.noMip {
		TexStorage2D(gl.TEXTURE_2D, 1, gl.RGBA, gl.UNSIGNED_BYTE, gl.RGBA8, w, h)
	} else {
		ctx.Call("texImage2D", gl.TEXTURE_2D, 0, gl.RGBA, w, h, 0, gl.RGBA, gl.UNSIGNED_BYTE, js.Null())
	}

	ctx.Call("texSubImage2D", gl.TEXTURE_2D, 0, 0, 0, gl.RGBA, gl.UNSIGNED_BYTE, jsImg)

	if t.noClamp {
		GL.TexParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.REPEAT)
		GL.TexParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.REPEAT)
	} else {
		GL.TexParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE)
		GL.TexParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE)
	}

	switch {
	case nearest:
		GL.TexParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST)
		GL.TexParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST)
	case !isPowerOf2(w) || !isPowerOf2(h) || t.noMip:
		// can't use mipmaps on non-power-of-2 texture sizes
		GL.TexParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.LINEAR)
		GL.TexParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR)
	default:
		GL.TexParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.LINEAR)
		GL.TexParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR_MIPMAP_NEAREST)

		const (
			TEXTURE_MAX_ANISOTROPY_EXT     = 0x84FE
			MAX_TEXTURE_MAX_ANISOTROPY_EXT = 0x84FF
		)

		if ext := GL.(gl.JSWrapper).JSValue().Call("getExtension", "EXT_texture_filter_anisotropic"); ext.Truthy() {
			max := maxAnisotropy[0]

			if max == 0 {
				GL.GetFloatv(maxAnisotropy[:], MAX_TEXTURE_MAX_ANISOTROPY_EXT)
				max = maxAnisotropy[0]
			}

			if GPULevel != GPUHigh {
				max /= 2
			}

			GL.TexParameterf(gl.TEXTURE_2D, TEXTURE_MAX_ANISOTROPY_EXT, max)
		}

		GL.GenerateMipmap(gl.TEXTURE_2D)
	}

	// avoid holding onto a reference to jsImg
	t.init = nil
}

var (
	decodeWorker      js.Value
	decodeRequests    = make(map[int]chan<- js.Value)
	nextDecodeRequest int
)

func decodeSoftWebP(blob js.Value, premultiplyAlpha bool) (js.Value, error) {
	buf, err := internal.AwaitNoRandomFail(blob.Call("arrayBuffer"))
	if err != nil {
		return js.Undefined(), err
	}

	if !decodeWorker.Truthy() {
		decodeWorker = internal.Worker.New("/script/webp-decoder-worker.js", map[string]interface{}{
			"name": "webp-decoder",
		})

		decodeWorker.Call("addEventListener", "message", js.FuncOf(func(_ js.Value, args []js.Value) interface{} {
			data := args[0].Get("data")
			id := data.Get("id").Int()

			ch, ok := decodeRequests[id]
			if !ok {
				log.Println("ERROR: no matching webp decoding request for ID", id)

				return js.Undefined()
			}

			delete(decodeRequests, id)
			ch <- data

			return js.Undefined()
		}))
	}

	id := nextDecodeRequest
	nextDecodeRequest++

	ch := make(chan js.Value, 1)
	decodeRequests[id] = ch

	decodeWorker.Call("postMessage", map[string]interface{}{
		"id":          id,
		"buffer":      buf,
		"premultiply": premultiplyAlpha,
		"downscale":   int(2 - GPULevel),
	}, []interface{}{buf})

	response := <-ch
	if outBuf := response.Get("buffer"); outBuf.Truthy() {
		imgData := js.Global().Get("ImageData").New(response.Get("width"), response.Get("height"))
		imgData.Get("data").Call("set", internal.Uint8Array.New(outBuf, 0), 0)
		imgData.Set("close", emptyFunc)
		internal.TryToDetachBuffer.Invoke(outBuf)

		return imgData, nil
	}

	return js.Undefined(), fmt.Errorf("gfx: unknown webp decoding error")
}
