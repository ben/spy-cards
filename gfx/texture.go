package gfx

import (
	"log"
	"runtime"
	"sync"
	"sync/atomic"
	"time"

	"git.lubar.me/ben/spy-cards/internal"
	"git.lubar.me/ben/spy-cards/internal/router"
	"golang.org/x/mobile/gl"
)

// Texture is an OpenGL texture.
type Texture struct {
	Name    string
	Texture gl.Texture
	nearest bool
	noMip   bool
	noClamp bool
	pix     []byte
	width   int
	height  int
	init    func()
}

var textures []*Texture

var onFetchEnd = sync.NewCond(&sync.Mutex{})

const (
	fetchNone     = 0
	fetchComplete = 1
	fetchActive   = 2
	fetchFailed   = 3
)

// AssetTexture is a lazy-loaded texture.
type AssetTexture struct {
	Name       string
	DownScale  int
	tex        *Texture
	embedded   *embeddedTexture
	loadErr    error
	firstWait  time.Time
	fetchState int32
	nearest    bool
	noMip      bool
	NoClamp    bool
}

type embeddedTexture struct {
	data []byte
	mime string

	premultiplyAlpha bool
}

func (a *AssetTexture) maybeFetch(warn bool) {
	switch state := atomic.LoadInt32(&a.fetchState); state {
	case fetchComplete, fetchActive:
		// nothing to do
	case fetchNone:
		if warn {
			log.Println("WARNING: non-preloaded texture:", a.Name)
		}

		fallthrough
	case fetchFailed:
		if atomic.CompareAndSwapInt32(&a.fetchState, state, fetchActive) {
			go a.doFetch()

			runtime.Gosched()
		}
	}
}

// NewAssetTexture creates a lazy-loaded texture from an asset.
func NewAssetTexture(name string, nearest, noMip bool) *AssetTexture {
	if router.FlagNoTextures.IsSet() || router.FlagNoExternalTextures.IsSet() {
		return BlankAssetTexture()
	}

	at := &AssetTexture{
		Name:    name,
		nearest: nearest,
		noMip:   noMip,
	}

	runtime.SetFinalizer(at, (*AssetTexture).Release)

	return at
}

func DummyAssetTexture(name string, tex gl.Texture) *AssetTexture {
	return &AssetTexture{
		Name: name,
		tex: &Texture{
			Name:    name,
			Texture: tex,
			init:    func() {},
		},
		fetchState: fetchComplete,
	}
}

var texCache = internal.Cache{MaxEntries: 256}

// NewCachedAssetTexture creates a lazy-loaded texture from an asset.
func NewCachedAssetTexture(name string) *AssetTexture {
	a, err := texCache.Do(name, func() (interface{}, error) {
		return NewAssetTexture(name, false, false), nil
	})
	if err != nil {
		panic(err)
	}

	return a.(*AssetTexture)
}

// NewEmbeddedTexture creates an AssetTexture that loads from memory.
func NewEmbeddedTexture(name string, nearest, noMip bool, data []byte, mimeType string, premultiplyAlpha bool) *AssetTexture {
	if router.FlagNoTextures.IsSet() {
		return BlankAssetTexture()
	}

	a := &AssetTexture{
		Name:    name,
		nearest: nearest,
		noMip:   noMip,
		embedded: &embeddedTexture{
			data: data,
			mime: mimeType,

			premultiplyAlpha: premultiplyAlpha,
		},
	}

	a.StartPreload()

	runtime.SetFinalizer(a, (*AssetTexture).Release)

	return a
}

// Release deletes the texture handle.
func (a *AssetTexture) Release() {
	if a.tex != nil {
		a.tex.Release()
		a.tex = nil
	}
}

// Preload loads the texture and returns any error encountered.
func (a *AssetTexture) Preload() error {
	var err error

	a.maybeFetch(false)

	onFetchEnd.L.Lock()

	for {
		state := atomic.LoadInt32(&a.fetchState)
		if state == fetchComplete || state == fetchFailed {
			err = a.loadErr

			break
		}

		onFetchEnd.Wait()
	}

	onFetchEnd.L.Unlock()

	return err
}

// StartPreload is a convenience function that calls Preload in a new goroutine.
func (a *AssetTexture) StartPreload() {
	a.maybeFetch(false)
}

// Texture loads the texture and returns the texture handle.
func (a *AssetTexture) Texture() gl.Texture {
	a.maybeFetch(true)

	onFetchEnd.L.Lock()

	var state int32

	for {
		state = atomic.LoadInt32(&a.fetchState)
		if state == fetchComplete || state == fetchFailed {
			break
		}

		onFetchEnd.Wait()
	}

	onFetchEnd.L.Unlock()

	if state != fetchComplete || a.tex == nil {
		return gl.Texture{}
	}

	return a.tex.Texture
}

// LazyTexture loads the texture and returns the texture handle.
func (a *AssetTexture) LazyTexture(def gl.Texture) gl.Texture {
	if atomic.LoadInt32(&a.fetchState) == fetchComplete {
		if a.tex == nil {
			return def
		}

		return a.tex.Texture
	}

	a.maybeFetch(true)

	onFetchEnd.L.Lock()

	if a.firstWait.IsZero() {
		a.firstWait = time.Now()
	}

	onFetchEnd.L.Unlock()

	return def
}

func (a *AssetTexture) Ready() bool {
	return atomic.LoadInt32(&a.fetchState) == fetchComplete
}

// BlankAssetTexture returns a 1x1 white square.
func BlankAssetTexture() *AssetTexture {
	t := &Texture{
		Name:    "(blank)",
		pix:     []uint8{255, 255, 255, 255},
		width:   1,
		height:  1,
		nearest: true,
		noMip:   true,
	}
	t.init = t.defaultInit

	onNewTexture(t)

	ch := make(chan struct{})
	close(ch)

	at := &AssetTexture{
		Name:       "(blank)",
		nearest:    true,
		tex:        t,
		fetchState: fetchComplete,
	}

	return at
}
