//go:build (!js || !wasm) && !headless
// +build !js !wasm
// +build !headless

package gfx

import (
	"git.lubar.me/ben/spy-cards/audio"
	"golang.org/x/mobile/gl"
)

var (
	// FrameSync is an internal variable.
	FrameSync     = make(chan chan struct{}, 1)
	lastFrameSync chan struct{}
)

// NextFrame synchronizes a frame boundary.
func NextFrame() bool {
	if lastFrameSync == nil {
		lastFrameSync = <-FrameSync
	}

	statFinishFrame()

	lastFrameSync <- struct{}{}
	lastFrameSync = <-FrameSync

	statStartFrame()

	Lock.Lock()
	GL.Clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT)
	Lock.Unlock()

	audio.Tick()

	return true
}

func InvalidateFramebuffer(fb, reset gl.Framebuffer, attachments ...gl.Enum) {
	GL.BindFramebuffer(gl.FRAMEBUFFER, reset)
}

func gatherRendererInfo() string {
	return "\n\nVendor: " + GL.GetString(gl.VENDOR) + "\nRenderer: " + GL.GetString(gl.RENDERER)
}

func jsLoadExtension(name string) struct{} {
	return struct{}{}
}

func platformGPULevelDefault() GPUPowerLevel {
	return GPUHigh
}
