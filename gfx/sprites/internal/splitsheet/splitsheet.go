package main

import (
	"fmt"
	"image"
	"image/draw"
	"image/png"
	"os"
	"path/filepath"

	"gopkg.in/yaml.v2"
)

type sheetMetaRoot struct {
	TextureImporter sheetTextureImporter `yaml:"TextureImporter"`
}

type sheetTextureImporter struct {
	SpritePixelsToUnits float32          `yaml:"spritePixelsToUnits"`
	SpriteSheet         sheetSpriteSheet `yaml:"spriteSheet"`
}

type sheetSpriteSheet struct {
	Sprites []sheetSprite `yaml:"sprites"`
}

type sheetSprite struct {
	Name  string     `yaml:"name"`
	Rect  sheetRect  `yaml:"rect"`
	Pivot sheetPivot `yaml:"pivot"`
}

type sheetRect struct {
	X      float32 `yaml:"x"`
	Y      float32 `yaml:"y"`
	Width  float32 `yaml:"width"`
	Height float32 `yaml:"height"`
}

type sheetPivot struct {
	X float32 `yaml:"x"`
	Y float32 `yaml:"y"`
}

func main() {
	name := os.Args[1]

	sheet, err := getSheet(name + ".png")
	if err != nil {
		panic(err)
	}

	meta, err := getMeta(name + ".png.meta")
	if err != nil {
		panic(err)
	}

	if err = writeSheetDesc(name, sheet, meta); err != nil {
		panic(err)
	}

	if err = splitSprites(name+"_sprites", sheet, meta); err != nil {
		panic(err)
	}
}

func getSheet(name string) (*image.NRGBA, error) {
	f, err := os.Open(name)
	if err != nil {
		return nil, fmt.Errorf("opening sprite sheet: %w", err)
	}
	defer f.Close()

	img, err := png.Decode(f)
	if err != nil {
		return nil, fmt.Errorf("decoding sprite sheet: %w", err)
	}

	return img.(*image.NRGBA), nil
}

func getMeta(name string) (*sheetMetaRoot, error) {
	f, err := os.Open(name)
	if err != nil {
		return nil, fmt.Errorf("opening sprite data: %w", err)
	}
	defer f.Close()

	var root sheetMetaRoot

	if err := yaml.NewDecoder(f).Decode(&root); err != nil {
		return nil, fmt.Errorf("decoding sprite data: %w", err)
	}

	return &root, nil
}

func writeSheetDesc(name string, sheet *image.NRGBA, meta *sheetMetaRoot) error {
	f, err := os.OpenFile(name+".go", os.O_CREATE|os.O_EXCL|os.O_WRONLY, 0755)
	if err != nil {
		return fmt.Errorf("creating sheet description file: %w", err)
	}
	defer f.Close()

	if _, err := fmt.Fprintf(f, "package sprites\n\n// Sprites for %s.\nvar (\n\t%sSheet = loadSpriteSheet(%q, %v, %d, %d, false)\n\n", name, name, "img/"+name+".png", meta.TextureImporter.SpritePixelsToUnits, sheet.Rect.Dx(), sheet.Rect.Dy()); err != nil {
		return fmt.Errorf("writing sheet description: %w", err)
	}

	for _, sprite := range meta.TextureImporter.SpriteSheet.Sprites {
		if _, err := fmt.Fprintf(f, "\t%s = %sSheet.sprite(%v, %v, %v, %v, %v, %v)\n", sprite.Name, name, sprite.Rect.X, sprite.Rect.Y, sprite.Rect.Width, sprite.Rect.Height, sprite.Pivot.X, sprite.Pivot.Y); err != nil {
			return fmt.Errorf("writing sheet description: %w", err)
		}
	}

	if _, err := fmt.Fprintf(f, ")\n"); err != nil {
		return fmt.Errorf("writing sheet description: %w", err)
	}

	return nil
}

func splitSprites(dir string, sheet *image.NRGBA, meta *sheetMetaRoot) error {
	if err := os.Mkdir(dir, 0755); err != nil {
		return fmt.Errorf("creating sprite folder: %w", err)
	}

	for _, sprite := range meta.TextureImporter.SpriteSheet.Sprites {
		if err := splitSprite(dir, sheet, sprite); err != nil {
			return err
		}
	}

	return nil
}

func splitSprite(dir string, sheet *image.NRGBA, sprite sheetSprite) error {
	img := image.NewNRGBA(image.Rect(0, 0, int(sprite.Rect.Width), int(sprite.Rect.Height)))

	draw.Draw(img, img.Rect, sheet, image.Pt(int(sprite.Rect.X), sheet.Rect.Dy()-int(sprite.Rect.Y+sprite.Rect.Height)), draw.Src)

	f, err := os.Create(filepath.Join(dir, sprite.Name+".png"))
	if err != nil {
		return fmt.Errorf("splitting sprite: %w", err)
	}
	defer f.Close()

	if err := png.Encode(f, img); err != nil {
		return fmt.Errorf("splitting sprite: %w", err)
	}

	return nil
}
