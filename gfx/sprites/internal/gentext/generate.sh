#!/bin/sh

test -f generate.sh || cd internal/gentext
test -d msdf-atlas-gen || git clone --recursive -b v1.2 https://github.com/Chlumsky/msdf-atlas-gen
test -f msdf-atlas-gen/bin/msdf-atlas-gen || make -C msdf-atlas-gen

msdf-atlas-gen/bin/msdf-atlas-gen \
	-font d3streetism.ttf -charset font-d3.txt -fontscale 1 -fontname "D3 Streetism" -and \
	-font bubblegumsans-regular.ttf -charset font-bubblegum.txt -fontscale 1 -fontname "Bubblegum Sans" -and \
	-font /usr/share/fonts/truetype/msttcorefonts/arial.ttf -charset font-arial.txt -fontscale 1 -fontname "Arial" -and \
	-font /usr/share/fonts/truetype/msttcorefonts/arial.ttf -charset font-arialinf.txt -fontscale 1.75 -fontname "Arial (infinity)" -and \
	-font /usr/share/fonts/truetype/dejavu/DejaVuSans-Bold.ttf -charset font-dejavu.txt -fontscale 1 -fontname "DejaVu Sans Bold" \
	-type mtsdf -emrange 0.2 -format png -potr -minsize 35 -imageout text.png -json text.json

cwebp -exact -lossless -m 6 text.png -o ../../text.webp

rm text.png

go run atlas.go > ../../text-data.go

rm text.json
