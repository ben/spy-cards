package sprites

// Aphid Festival sprites.
var (
	aphidFestivalSheet = loadSpriteSheet("img/festival.webp", 100, 2048, 1024, false)

	FoodLeafRaw    = aphidFestivalSheet.sprite(7, 633, 61, 63, 0.5, 0.5)
	FoodShroomRaw  = aphidFestivalSheet.sprite(3, 568, 50, 59, 0.5, 0.5)
	FoodHoneyRaw   = aphidFestivalSheet.sprite(3, 514, 54, 51, 0.5, 0.5)
	FoodLeafCooked = [3]*Sprite{
		aphidFestivalSheet.sprite(77, 644, 63, 48, 0.5, 0.5),
		aphidFestivalSheet.sprite(150, 641, 60, 47, 0.5, 0.5),
		aphidFestivalSheet.sprite(222, 638, 56, 57, 0.5, 0.5),
	}
	FoodShroomCooked = [3]*Sprite{
		aphidFestivalSheet.sprite(71, 581, 56, 52, 0.5, 0.5),
		aphidFestivalSheet.sprite(143, 584, 55, 48, 0.5, 0.5),
		aphidFestivalSheet.sprite(203, 573, 60, 58, 0.5, 0.5),
	}
	FoodHoneyCooked = [3]*Sprite{
		aphidFestivalSheet.sprite(68, 510, 60, 63, 0.5, 0.5),
		aphidFestivalSheet.sprite(138, 513, 60, 58, 0.5, 0.5),
		aphidFestivalSheet.sprite(209, 519, 58, 50, 0.5, 0.5),
	}
	TanjerinIdle    = aphidFestivalSheet.sprite(4, 307, 136, 191, 0.5, 0.5)
	TanjerinHide    = aphidFestivalSheet.sprite(3, 111, 142, 190, 0.5, 0.5)
	TanjerinCheer   = aphidFestivalSheet.sprite(150, 307, 139, 190, 0.5, 0.5)
	FestivalWordArt = aphidFestivalSheet.sprite(1, 0, 568, 101, 0.5, 0.5)
	KutIdle         = aphidFestivalSheet.sprite(307, 270, 190, 326, 0.3734586, 0.5)
	KutSlice        = [10]*Sprite{
		aphidFestivalSheet.sprite(443, 684, 238, 327, 0.5, 0.5),
		aphidFestivalSheet.sprite(696, 690, 245, 326, 0.5, 0.5),
		aphidFestivalSheet.sprite(960, 689, 254, 323, 0.5, 0.5),
		aphidFestivalSheet.sprite(523, 203, 230, 458, 0.5, 0.3657597),
		aphidFestivalSheet.sprite(779, 189, 239, 458, 0.5, 0.3629283),
		aphidFestivalSheet.sprite(1043, 177, 238, 460, 0.5, 0.3635217),
		aphidFestivalSheet.sprite(1248, 679, 462, 319, 0.5, 0.4638758),
		aphidFestivalSheet.sprite(1329, 214, 169, 363, 0.5104937, 0.4366591),
		aphidFestivalSheet.sprite(1540, 210, 167, 349, 0.5065678, 0.4506916),
		aphidFestivalSheet.sprite(1739, 217, 159, 338, 0.5195193, 0.5),
	}
	SmokeParticle = [2]*Sprite{
		aphidFestivalSheet.sprite(164, 126, 125, 128, 0.5, 0.5),
		aphidFestivalSheet.sprite(321, 145, 70, 72, 0.5, 0.5),
	}
	Joy       = aphidFestivalSheet.sprite(322, 612, 90, 412, 0.5, 0.5)
	AphidBody = [3]*Sprite{
		aphidFestivalSheet.sprite(0, 919, 140, 105, 0.5, 0.5),
		aphidFestivalSheet.sprite(0, 820, 156, 98, 0.5, 0.5),
		aphidFestivalSheet.sprite(0, 712, 148, 105, 0.5, 0.5),
	}
	AphidLeg = [3]*Sprite{
		aphidFestivalSheet.sprite(141, 919, 140, 105, 0.5, 0.5),
		aphidFestivalSheet.sprite(158, 818, 156, 98, 0.5, 0.5),
		aphidFestivalSheet.sprite(158, 709, 148, 105, 0.5, 0.5),
	}

	BtnHistory  = gui.sprite(444, 355, 128, 129, 0.5, 0.5)
	BtnPibu     = gui.sprite(879, 355, 130, 128, 0.5, 0.5)
	BtnKut      = gui.sprite(588, 355, 129, 127, 0.5, 0.5)
	BtnTanjerin = gui.sprite(4, 181, 166, 169, 0.5, 0.5)
	BtnWifeMode = gui.sprite(1120, 1330, 70, 102, 0.5, 0.5)
	BtnSave     = gui.sprite(990, 489, 128, 128, 0.5, 0.5)
	BtnQuit     = gui.sprite(1379, 446, 166, 170, 0.5, 0.5)
)
