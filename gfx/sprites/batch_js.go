//go:build js && wasm && !headless
// +build js,wasm,!headless

package sprites

import (
	"syscall/js"
	"time"
	"unsafe"

	"git.lubar.me/ben/spy-cards/gfx"
	"git.lubar.me/ben/spy-cards/internal"
	"golang.org/x/mobile/gl"
)

var (
	fastBufferData   js.Value
	fastSetUniforms  js.Value
	fastSetPointers  js.Value
	fastDrawElements js.Value

	_ = gfx.Custom("batch-funcs", func() {
		var jsGo js.Value

		*(*uint64)(unsafe.Pointer(&jsGo)) = 0x7FF8000100000006

		fastBufferData = internal.Function.New("jsgo,gl", `return function batchBufferData(buf, dataptr, length, realloc) {
	gl.bindBuffer(gl.ARRAY_BUFFER, buf);
	const data = new Uint8Array(jsgo.mem.buffer, dataptr, length);
	if (realloc) {
		gl.bufferData(gl.ARRAY_BUFFER, data, gl.DYNAMIC_DRAW);
	} else {
		gl.bufferSubData(gl.ARRAY_BUFFER, 0, data);
	}
}`).Invoke(jsGo, gfx.GL.(gl.JSWrapper).JSValue())

		fastSetUniforms = internal.Function.New("jsgo,gl,prog,cuni,tuni,toffset", `toffset -= performance.now();
return function batchSetUniforms(matptr, t0, t1, t2, t3, t4, t5, t6, t7) {
	gl.useProgram(prog);

	gl.uniformMatrix4fv(cuni, false, new Float32Array(jsgo.mem.buffer, matptr, 2*4*4));
	gl.uniform1f(tuni, performance.now() + toffset);

	if (t7) {
		gl.activeTexture(gl.TEXTURE7);
		gl.bindTexture(gl.TEXTURE_2D, t7);
	}
	if (t6) {
		gl.activeTexture(gl.TEXTURE6);
		gl.bindTexture(gl.TEXTURE_2D, t6);
	}
	if (t5) {
		gl.activeTexture(gl.TEXTURE5);
		gl.bindTexture(gl.TEXTURE_2D, t5);
	}
	if (t4) {
		gl.activeTexture(gl.TEXTURE4);
		gl.bindTexture(gl.TEXTURE_2D, t4);
	}
	if (t3) {
		gl.activeTexture(gl.TEXTURE3);
		gl.bindTexture(gl.TEXTURE_2D, t3);
	}
	if (t2) {
		gl.activeTexture(gl.TEXTURE2);
		gl.bindTexture(gl.TEXTURE_2D, t2);
	}
	if (t1) {
		gl.activeTexture(gl.TEXTURE1);
		gl.bindTexture(gl.TEXTURE_2D, t1);
	}
	if (t0) {
		gl.activeTexture(gl.TEXTURE0);
		gl.bindTexture(gl.TEXTURE_2D, t0);
	}
}`).Invoke(jsGo, gfx.GL.(gl.JSWrapper).JSValue(), *spriteProgram.Program.Value, *spriteCamera.Uniform.Value, *spriteTime.Uniform.Value, time.Since(startTime).Seconds())

		fastSetPointers = internal.Function.New("gl,position,texCoord,flagsAndTexture,color", `return function batchSetPointers() {
	gl.vertexAttribPointer(position, 4, gl.FLOAT, false, 28, 0);
	gl.vertexAttribPointer(texCoord, 2, gl.UNSIGNED_SHORT, true, 28, 16);
	gl.vertexAttribPointer(flagsAndTexture, 2, gl.UNSIGNED_SHORT, false, 28, 20);
	gl.vertexAttribPointer(color, 4, gl.UNSIGNED_BYTE, true, 28, 24);
}`).Invoke(gfx.GL.(gl.JSWrapper).JSValue(), *spritePosition.Attrib.Value, *spriteTexCoord.Attrib.Value, *spriteFlagsAndTexture.Attrib.Value, *spriteColor.Attrib.Value)

		fastDrawElements = internal.Function.New("gl", `return function batchDrawElements(elements, count) {
	gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, elements);
	gl.drawElements(gl.TRIANGLES, count, gl.UNSIGNED_INT, 0);
}`).Invoke(gfx.GL.(gl.JSWrapper).JSValue())
	}, func() {
		fastBufferData = js.Undefined()
		fastSetUniforms = js.Undefined()
		fastSetPointers = js.Undefined()
		fastDrawElements = js.Undefined()
	})
)

func (sb *Batch) fastBufferData(shouldReallocate bool) bool {
	if fastBufferData.Truthy() {
		fastBufferData.Invoke(
			*sb.dataBuf.Value,
			uintptr(unsafe.Pointer(&sb.data[0])),
			len(sb.data)*int(unsafe.Sizeof(sb.data[0])),
			shouldReallocate,
		)

		return true
	}

	return false
}

func (sb *Batch) fastSetUniforms(textures *[8]gl.Texture) bool {
	if fastSetUniforms.Truthy() {
		var texturesJS [len(*textures)]js.Value

		for i, t := range textures {
			if t.Value != nil {
				texturesJS[i] = *t.Value
			}
		}

		fastSetUniforms.Invoke(
			uintptr(unsafe.Pointer(&sb.camMatrix[0][0])),
			texturesJS[0],
			texturesJS[1],
			texturesJS[2],
			texturesJS[3],
			texturesJS[4],
			texturesJS[5],
			texturesJS[6],
			texturesJS[7],
		)

		return true
	}

	return false
}

func (sb *Batch) fastSetPointers() bool {
	if fastSetPointers.Truthy() {
		fastSetPointers.Invoke()

		return true
	}

	return false
}

func (sb *Batch) fastDrawElements() bool {
	if fastDrawElements.Truthy() {
		fastDrawElements.Invoke(*sharedElements.get().Value, sb.count)

		return true
	}

	return false
}
