//go:build !headless
// +build !headless

//go:generate sh ./internal/gentext/generate.sh

package sprites

import (
	"fmt"

	"git.lubar.me/ben/spy-cards/gfx"
	"git.lubar.me/ben/spy-cards/input"
	"git.lubar.me/ben/spy-cards/internal"
)

var (
	buttonSprites = [...][10]*Sprite{
		internal.StyleKeyboard: kbSprites,
		internal.StyleGenericGamepad: {
			input.BtnUp:      GamepadButton[input.BtnUp][0],
			input.BtnDown:    GamepadButton[input.BtnDown][0],
			input.BtnLeft:    GamepadButton[input.BtnLeft][0],
			input.BtnRight:   GamepadButton[input.BtnRight][0],
			input.BtnConfirm: GamepadButton[input.BtnConfirm][0],
			input.BtnCancel:  GamepadButton[input.BtnCancel][0],
			input.BtnSwitch:  GamepadButton[input.BtnSwitch][0],
			input.BtnToggle:  GamepadButton[input.BtnToggle][0],
			input.BtnPause:   GamepadButton[input.BtnPause][0],
			input.BtnHelp:    GamepadButton[input.BtnHelp][0],
		},
	}
	textSheets = [...]map[rune]*Sprite{
		FontD3Streetism:   d3Streetism,
		FontBubblegumSans: bubblegumSans,
	}
	textSpriteASCII = [...][128]*Sprite{
		FontD3Streetism:   fontASCIISprites(FontD3Streetism),
		FontBubblegumSans: fontASCIISprites(FontBubblegumSans),
	}
)

// DrawTextFuncEx draws a string to the screen.
func DrawTextFuncEx(tb *Batch, font FontID, text string, x, y, z, scaleX, scaleY float32, getColor func(rune) Color, flags RenderFlag, rx, ry, rz float32) float32 {
	tb.reserve(len(text))

	for _, letter := range text {
		DrawLetter(tb, font, letter, x, y, z, scaleX, scaleY, getColor(letter), flags, rx, ry, rz)

		x += TextAdvance(font, letter, scaleX)
	}

	return x
}

func PreloadText() error {
	if err := buttonSheet.Preload(); err != nil {
		return fmt.Errorf("sprites: loading button sprite sheet: %w", err)
	}

	if err := textSheet.Preload(); err != nil {
		return fmt.Errorf("sprites: loading text sprite sheet: %w", err)
	}

	return nil
}

func TextTexture() *gfx.AssetTexture {
	return textSheet.AssetTexture
}

func TextReady() bool {
	return buttonSheet.Ready() && textSheet.Ready()
}

// DrawLetter draws one letter to the screen.
func DrawLetter(tb *Batch, font FontID, letter rune, x, y, z, scaleX, scaleY float32, tint Color, flags RenderFlag, rx, ry, rz float32) {
	if letter < input.NumButtons {
		flags &^= FlagBorder | FlagIsMTSDF
	} else if flags&(FlagBorder|FlagIsMTSDF) == 0 {
		panic("sprites: expected DrawLetter to have at least one text flag set")
	}

	var sprite *Sprite

	if letter >= 32 && int(letter) < len(textSpriteASCII[font]) {
		sprite = textSpriteASCII[font][letter]
	} else {
		sprite = lookupLetterSpriteSlow(font, letter)
	}

	tb.AppendEx(sprite, x, y, z, scaleX, scaleY, tint, flags, rx, ry, rz)
}

func fontASCIISprites(font FontID) [128]*Sprite {
	var s [128]*Sprite

	for i := range s {
		s[i] = lookupLetterSpriteSlow(font, rune(i))
	}

	return s
}

func lookupLetterSpriteSlow(font FontID, letter rune) *Sprite {
	if letter < input.NumButtons {
		return buttonSprites[ButtonStyle][letter]
	}

	if sprite, ok := textSheets[font][letter]; ok {
		return sprite
	}

	if sprite, ok := arial[letter]; ok {
		return sprite
	}

	if sprite, ok := arialInf[letter]; ok {
		return sprite
	}

	if sprite, ok := dejavu[letter]; ok {
		return sprite
	}

	return textSheets[font][' ']
}
