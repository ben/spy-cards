// Package sprites handles loading and rendering sprites.
package sprites

import (
	"math"

	"git.lubar.me/ben/spy-cards/gfx"
)

// Sprite holds the data needed to render a sprite.
type Sprite struct {
	S0, S1   float32
	T0, T1   float32
	X0, X1   float32
	Y0, Y1   float32
	S0u, S1u uint16
	T0u, T1u uint16

	*spriteSheet
}

// Blank is a white square with a width and height of 1 pixel.
var Blank = (&spriteSheet{
	AssetTexture: gfx.BlankAssetTexture(),

	width:     2,
	height:    2,
	unitPerPx: 2,
}).sprite(0, 0, 2, 2, 0.5, 0.5)

// New creates a new sprite.
func New(tex *gfx.AssetTexture, unitPerPx float32, width, height int, x, y, w, h, px, py float32) *Sprite {
	return (&spriteSheet{
		AssetTexture: tex,

		width:     width,
		height:    height,
		unitPerPx: unitPerPx,
	}).sprite(x, y, w, h, px, py)
}

type spriteSheet struct {
	*gfx.AssetTexture

	width, height int
	unitPerPx     float32
}

func loadSpriteSheet(url string, unitPerPx float32, width, height int, nearest bool) *spriteSheet {
	return &spriteSheet{
		AssetTexture: gfx.NewAssetTexture(url, nearest, false),

		width:     width,
		height:    height,
		unitPerPx: unitPerPx,
	}
}

func (s *spriteSheet) rescale(unitPerPx float32) *spriteSheet {
	return &spriteSheet{
		AssetTexture: s.AssetTexture,

		width:     s.width,
		height:    s.height,
		unitPerPx: unitPerPx,
	}
}

func (s *spriteSheet) extraDownScale(shift int) *spriteSheet {
	s.AssetTexture.DownScale += shift

	return s
}

func (s *spriteSheet) sprite(x, y, w, h, px, py float32) *Sprite {
	anchorX := w * px
	anchorY := h * py

	x0 := -anchorX / s.unitPerPx
	y0 := -anchorY / s.unitPerPx
	x1 := (w - anchorX) / s.unitPerPx
	y1 := (h - anchorY) / s.unitPerPx
	s0 := x / float32(s.width)
	t0 := 1 - (y+h)/float32(s.height)
	s1 := (x + w) / float32(s.width)
	t1 := 1 - y/float32(s.height)

	return &Sprite{
		X0: x0, Y0: y0, X1: x1, Y1: y1,
		S0: s0, T0: t0, S1: s1, T1: t1,
		S0u: uint16(s0 * math.MaxUint16),
		T0u: uint16(t0 * math.MaxUint16),
		S1u: uint16(s1 * math.MaxUint16),
		T1u: uint16(t1 * math.MaxUint16),

		spriteSheet: s,
	}
}

func (s *Sprite) Crop(left, top, right, bottom float32) *Sprite {
	return &Sprite{
		X0:  s.X0*(1-left) + s.X1*left,
		Y0:  s.Y0*(1-bottom) + s.Y1*bottom,
		X1:  s.X1*(1-right) + s.X0*right,
		Y1:  s.Y1*(1-top) + s.Y0*top,
		S0:  s.S0*(1-left) + s.S1*left,
		T0:  s.T0*(1-top) + s.T1*top,
		S1:  s.S1*(1-right) + s.S0*right,
		T1:  s.T1*(1-bottom) + s.T0*bottom,
		S0u: uint16((s.S0*(1-left) + s.S1*left) * math.MaxUint16),
		T0u: uint16((s.T0*(1-top) + s.T1*top) * math.MaxUint16),
		S1u: uint16((s.S1*(1-right) + s.S0*right) * math.MaxUint16),
		T1u: uint16((s.T1*(1-bottom) + s.T0*bottom) * math.MaxUint16),

		spriteSheet: s.spriteSheet,
	}
}
