uniform mat4 camera[2];
attribute vec4 in_pos;
attribute vec2 in_texcoord;
attribute vec2 flags_and_texture;
attribute vec4 in_color;

varying vec4 position;

// xy: tex_coord
// z: texid
varying vec3 packed_0;

// x: fog_distance
// y: rainbow
// z: mk_fog
// w: no_discard
varying vec4 packed_1;

// x: border
// y: is_mtsdf
// z: half_glow
// w: (unused)
varying vec4 packed_2;

varying vec4 color;

void main() {
	color = in_color;
	packed_0.z = flags_and_texture.y + 0.5;

	float flags = flags_and_texture.x;

	packed_1.y = mod(floor(flags / pow(2.0, 0.0)), 2.0);
	packed_1.z = mod(floor(flags / pow(2.0, 1.0)), 2.0);
	packed_1.w = mod(floor(flags / pow(2.0, 2.0)), 2.0);
	packed_2.x = mod(floor(flags / pow(2.0, 3.0)), 2.0);
	packed_2.y = mod(floor(flags / pow(2.0, 4.0)), 2.0);
	packed_2.z = mod(floor(flags / pow(2.0, 5.0)), 2.0);

	packed_0.xy = in_texcoord;
	position = camera[0] * camera[1] * in_pos;
	packed_1.x = length(position.xyz);
	gl_Position = position;
}
