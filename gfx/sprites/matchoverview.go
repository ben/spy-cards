package sprites

import (
	"log"

	"git.lubar.me/ben/spy-cards/gfx"
)

var (
	redSide      = Color{54, 41, 27, 204}
	blueSide     = Color{27, 41, 54, 204}
	betweenSides = Color{0, 0, 0, 204}
)

// MatchOverview renders the versus screen for the start of a
// Spy Cards Online recording.
func (sb *Batch) MatchOverview(x, y, z, sx, sy float32, flip bool) {
	tex := ^uint16(0)

	for i := range sb.texture {
		if sb.texture[i] == nil {
			sb.texture[i] = Blank.AssetTexture
		}

		if sb.texture[i] == Blank.AssetTexture {
			tex = uint16(i)

			break
		}
	}

	if tex > uint16(len(sb.texture)) {
		for i, t := range sb.texture {
			log.Println("DEBUG: sheet", i, "is", t.Name)
		}

		panic(ErrTooManySheets)
	}

	sb.reserve(7)

	side1, side2 := blueSide, redSide
	if flip {
		side1, side2 = side2, side1
	}

	offset := len(sb.data)
	sb.data = sb.data[:offset+3*4]

	sb.data[offset] = batchData{
		Pos:     gfx.Vector{x - sx, y + sy, z, 1},
		S:       Blank.S0u,
		T:       Blank.T0u,
		Texture: tex,
		Color:   side1,
	}
	sb.data[offset+1] = batchData{
		Pos:     gfx.Vector{x - sx, y - sy*23/255, z, 1},
		S:       Blank.S0u,
		T:       Blank.T1u,
		Texture: tex,
		Color:   side1,
	}
	sb.data[offset+2] = batchData{
		Pos:     gfx.Vector{x + sx, y + sy*105/255, z, 1},
		S:       Blank.S1u,
		T:       Blank.T1u,
		Texture: tex,
		Color:   side1,
	}
	sb.data[offset+3] = batchData{
		Pos:     gfx.Vector{x + sx, y + sy, z, 1},
		S:       Blank.S1u,
		T:       Blank.T0u,
		Texture: tex,
		Color:   side1,
	}

	sb.data[offset+4] = batchData{
		Pos:     gfx.Vector{x - sx, y - sy*23/255, z, 1},
		S:       Blank.S0u,
		T:       Blank.T0u,
		Texture: tex,
		Color:   betweenSides,
	}
	sb.data[offset+5] = batchData{
		Pos:     gfx.Vector{x - sx, y - sy*103/255, z, 1},
		S:       Blank.S0u,
		T:       Blank.T1u,
		Texture: tex,
		Color:   betweenSides,
	}
	sb.data[offset+6] = batchData{
		Pos:     gfx.Vector{x + sx, y + sy*25/255, z, 1},
		S:       Blank.S1u,
		T:       Blank.T1u,
		Texture: tex,
		Color:   betweenSides,
	}
	sb.data[offset+7] = batchData{
		Pos:     gfx.Vector{x + sx, y + sy*105/255, z, 1},
		S:       Blank.S1u,
		T:       Blank.T0u,
		Texture: tex,
		Color:   betweenSides,
	}

	sb.data[offset+8] = batchData{
		Pos:     gfx.Vector{x - sx, y - sy*103/255, z, 1},
		S:       Blank.S0u,
		T:       Blank.T0u,
		Texture: tex,
		Color:   side2,
	}
	sb.data[offset+9] = batchData{
		Pos:     gfx.Vector{x - sx, y - sy, z, 1},
		S:       Blank.S0u,
		T:       Blank.T1u,
		Texture: tex,
		Color:   side2,
	}
	sb.data[offset+10] = batchData{
		Pos:     gfx.Vector{x + sx, y - sy, z, 1},
		S:       Blank.S1u,
		T:       Blank.T1u,
		Texture: tex,
		Color:   side2,
	}
	sb.data[offset+11] = batchData{
		Pos:     gfx.Vector{x + sx, y + sy*25/255, z, 1},
		S:       Blank.S1u,
		T:       Blank.T0u,
		Texture: tex,
		Color:   side2,
	}

	sb.index += 4 * 3
	sb.count += 6 * 3

	sb.dirtyBuf = true

	sb.Append(Blank, x-sx-0.05, y, z, 0.1, sy*2, Black)
	sb.Append(Blank, x+sx+0.05, y, z, 0.1, sy*2, Black)
	sb.Append(Blank, x, y-sy-0.05, z, sx*2+0.2, 0.1, Black)
	sb.Append(Blank, x, y+sy+0.05, z, sx*2+0.2, 0.1, Black)
}
