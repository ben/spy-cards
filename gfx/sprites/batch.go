package sprites

import (
	"errors"
	"log"
	"sync"
	"unsafe"

	"git.lubar.me/ben/spy-cards/gfx"
	"git.lubar.me/ben/spy-cards/internal/router"
	"golang.org/x/mobile/gl"
)

var spriteElem = [6]uint32{
	0, 1, 2,
	0, 2, 3,
}

var sharedElements sharedElemBuffer

type sharedElemBuffer struct {
	elem  []byte
	buf   gl.Buffer
	lock  sync.Mutex
	index uint32
	dirty bool
}

func (e *sharedElemBuffer) ensure(index uint32) {
	e.lock.Lock()

	if e.index < index {
		for e.dirty = true; e.index < index; e.index += 4 {
			elem := spriteElem

			for i := range elem {
				elem[i] += e.index
			}

			/*#nosec*/
			e.elem = append(e.elem, (*[unsafe.Sizeof(elem)]byte)(unsafe.Pointer(&elem))[:]...)
		}
	}

	e.lock.Unlock()
}

func (e *sharedElemBuffer) get() gl.Buffer {
	// gfx.Lock should already be held
	e.lock.Lock()

	if e.dirty {
		e.upload()
	}

	buf := e.buf

	e.lock.Unlock()

	return buf
}

// Batch is a sprite batch.
type Batch struct {
	camMatrix [2]gfx.Matrix
	texture   [8]*gfx.AssetTexture
	data      []batchData
	funHouse  gfx.Matrix
	count     int
	lastBuf   int
	dataBuf   gl.Buffer
	vao       gl.VertexArray
	BaseX     float32
	BaseY     float32
	BaseZ     float32
	index     uint32
	haveBuf   bool
	dirtyBuf  bool

	AvoidTextureFeedback bool
}

// NewBatch starts a sprite batch.
func NewBatch(cam *gfx.Camera) *Batch {
	b := &Batch{
		camMatrix: [2]gfx.Matrix{
			cam.Perspective,
			{},
		},
	}
	cam.Combined(&b.camMatrix[1])
	b.funHouse.Identity()

	return b
}

// RenderFlag is a sprite rendering modifier.
type RenderFlag uint16

// Constants for RenderFlag.
const (
	FlagRainbow   RenderFlag = 1 << 0
	FlagMKFog     RenderFlag = 1 << 1
	FlagNoDiscard RenderFlag = 1 << 2
	FlagBorder    RenderFlag = 1 << 3
	FlagIsMTSDF   RenderFlag = 1 << 4
	FlagHalfGlow  RenderFlag = 1 << 5
)

type batchData struct {
	Pos     gfx.Vector
	S, T    uint16
	Flags   RenderFlag
	Texture uint16
	Color   Color
}

// ErrTooManySheets is panic-ed when attempting to Append a sprite that uses
// a unique texture sheet to a batch that already has the maximum number of
// textures.
var ErrTooManySheets = errors.New("sprite: too many sprite sheets in batch")

// Append adds a sprite to the batch.
func (sb *Batch) Append(s *Sprite, x, y, z, sx, sy float32, tint Color) {
	sb.AppendEx(s, x, y, z, sx, sy, tint, 0, 0, 0, 0)
}

// AppendEx adds a sprite to the batch.
func (sb *Batch) AppendEx(s *Sprite, x, y, z, sx, sy float32, tint Color, flags RenderFlag, rx, ry, rz float32) {
	tex := ^uint16(0)

	for i := range sb.texture {
		if sb.texture[i] == nil {
			sb.texture[i] = s.AssetTexture
		}

		if sb.texture[i] == s.AssetTexture {
			tex = uint16(i)

			break
		}
	}

	if tex > uint16(len(sb.texture)) {
		for i, t := range sb.texture {
			log.Println("DEBUG: sheet", i, "is", t.Name)
		}

		panic(ErrTooManySheets)
	}

	if tint == Rainbow {
		flags |= FlagRainbow
		tint = White
	}

	x0, x1 := s.X0*sx, s.X1*sx
	y0, y1 := s.Y0*sy, s.Y1*sy
	s0, s1 := s.S0u, s.S1u
	t0, t1 := s.T0u, s.T1u

	x += sb.BaseX
	y += sb.BaseY
	z += sb.BaseZ

	if sx < 0 {
		x0, x1 = x1, x0
		s0, s1 = s1, s0
	}

	if sy < 0 {
		y0, y1 = y1, y0
		t0, t1 = t1, t0
	}

	sb.reserve(1)

	offset := len(sb.data)
	sb.data = sb.data[:offset+4]

	var matrix, m0, m1 gfx.Matrix

	m1.Translation(x, y, z)
	matrix.Multiply(&sb.funHouse, &m1)

	if rx != 0 || ry != 0 || rz != 0 {
		m0 = matrix

		m1.RotationXYZ(rx, ry, rz)

		matrix.Multiply(&m0, &m1)
	}

	var v gfx.Vector

	v.Multiply(&matrix, &gfx.Vector{x0, y1, 0, 1})

	sb.data[offset] = batchData{
		Pos:     v,
		S:       s0,
		T:       t0,
		Flags:   flags,
		Texture: tex,
		Color:   tint,
	}

	v.Multiply(&matrix, &gfx.Vector{x0, y0, 0, 1})

	sb.data[offset+1] = batchData{
		Pos:     v,
		S:       s0,
		T:       t1,
		Flags:   flags,
		Texture: tex,
		Color:   tint,
	}

	v.Multiply(&matrix, &gfx.Vector{x1, y0, 0, 1})

	sb.data[offset+2] = batchData{
		Pos:     v,
		S:       s1,
		T:       t1,
		Flags:   flags,
		Texture: tex,
		Color:   tint,
	}

	v.Multiply(&matrix, &gfx.Vector{x1, y1, 0, 1})

	sb.data[offset+3] = batchData{
		Pos:     v,
		S:       s1,
		T:       t0,
		Flags:   flags,
		Texture: tex,
		Color:   tint,
	}

	sb.index += 4
	sb.count += 6

	sb.dirtyBuf = true
}

func (sb *Batch) SetFunHouse(data *[8]float32) {
	if data == nil {
		sb.funHouse.Identity()
	} else {
		sb.funHouse.FunHouse(data[0], data[1], data[2], data[3], data[4], data[5], data[6], data[7])
	}
}

func (sb *Batch) reserve(sprites int) {
	if len(sb.data)+4*sprites > cap(sb.data) {
		sb.data = append(sb.data, make([]batchData, 4*sprites)...)[:len(sb.data)]
	}

	sharedElements.ensure(sb.index + 4*uint32(sprites))
}

func (sb *Batch) ReserveTexture(tex *gfx.AssetTexture) bool {
	for i := range sb.texture {
		if sb.texture[i] == nil {
			sb.texture[i] = tex
		}

		if sb.texture[i] == tex {
			return true
		}
	}

	return false
}

// Reset clears the state of the Batch, but keeps its allocated memory,
// allowing it to be reused.
func (sb *Batch) Reset(cam *gfx.Camera) {
	sb.SetCamera(cam)

	for i := range sb.texture {
		sb.texture[i] = nil
	}

	sb.data = sb.data[:0]
	sb.count = 0
	sb.index = 0

	sb.dirtyBuf = true
}

// SetCamera changes the camera of the batch without changing its contents.
func (sb *Batch) SetCamera(cam *gfx.Camera) {
	sb.camMatrix[0] = cam.Perspective
	cam.Combined(&sb.camMatrix[1])
}

type BatchSnapshot struct {
	baseX    float32
	baseY    float32
	baseZ    float32
	funHouse gfx.Matrix
	index    uint32
	count    int
	data     []batchData
	tex      [8]*gfx.AssetTexture
}

func (sb *Batch) Record() func() *BatchSnapshot {
	startLen := len(sb.data)
	startIndex := sb.index
	startCount := sb.count
	startBaseX := sb.BaseX
	startBaseY := sb.BaseY
	startBaseZ := sb.BaseZ
	startFunHouse := sb.funHouse

	return func() *BatchSnapshot {
		if sb.BaseX != startBaseX || sb.BaseY != startBaseY || sb.BaseZ != startBaseZ || sb.funHouse != startFunHouse {
			log.Println("WARNING: batch snapshot invalidated by batch state change during recording")

			return nil
		}

		if router.FlagNoBatchCaching.IsSet() {
			return nil
		}

		data := make([]batchData, len(sb.data)-startLen)
		copy(data, sb.data[startLen:])

		var tex [8]*gfx.AssetTexture

		for i := range data {
			tex[data[i].Texture] = sb.texture[data[i].Texture]
		}

		return &BatchSnapshot{
			baseX:    startBaseX,
			baseY:    startBaseY,
			baseZ:    startBaseZ,
			funHouse: startFunHouse,
			index:    sb.index - startIndex,
			count:    sb.count - startCount,
			data:     data,
			tex:      tex,
		}
	}
}

func (bs *BatchSnapshot) Replay(sb *Batch) bool {
	if bs == nil {
		return false
	}

	if bs.funHouse != sb.funHouse {
		return false
	}

	if bs.baseX != sb.BaseX || bs.baseY != sb.BaseY || bs.baseZ != sb.BaseZ {
		log.Println("WARNING: batch snapshot invalidated by change to base position")

		return false
	}

	for i, t := range bs.tex {
		if t != nil && sb.texture[i] != nil && t != sb.texture[i] {
			log.Printf("WARNING: batch snapshot invalidated by conflicting texture (%q vs %q)", t.Name, sb.texture[i].Name)

			return false
		}
	}

	for i, t := range bs.tex {
		if t != nil {
			sb.texture[i] = t
		}
	}

	sb.data = append(sb.data, bs.data...)
	sb.count += bs.count
	sb.index += bs.index

	sharedElements.ensure(sb.index)

	sb.dirtyBuf = true

	return true
}
