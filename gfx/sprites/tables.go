package sprites

import "git.lubar.me/ben/spy-cards/gfx"

// Bug Tables sprites.
var (
	Main1 = func() *gfx.AssetTexture {
		tex := gfx.NewAssetTexture("img/main1.webp", false, false)
		tex.DownScale--

		return tex
	}()
	Main2   = gfx.NewAssetTexture("img/main2.webp", false, false)
	TreeTex = gfx.NewAssetTexture("img/treetex.webp", false, false)
	Water1  = func() *gfx.AssetTexture {
		tex := gfx.NewAssetTexture("img/water1.webp", false, false)
		tex.NoClamp = true

		return tex
	}()
	Water3 = func() *gfx.AssetTexture {
		tex := gfx.NewAssetTexture("img/water3.webp", false, false)
		tex.NoClamp = true

		return tex
	}()
	Hue   = gfx.NewAssetTexture("img/hue.webp", false, false)
	Grass = gfx.NewAssetTexture("img/grass.webp", false, false)

	bugTablesSheet = loadSpriteSheet("img/tables.webp", 100, 2048, 2048, false)

	TablesFry         = bugTablesSheet.sprite(1, 1753, 248, 294, 0.5, 0)
	TablesDoppel      = bugTablesSheet.sprite(251, 1786, 130, 261, 0.5, 0)
	TablesCrisbee     = bugTablesSheet.sprite(383, 1758, 223, 289, 0.5, 0)
	TablesKut         = bugTablesSheet.sprite(608, 1721, 190, 326, 0.5, 0)
	TablesJayde       = bugTablesSheet.sprite(800, 1715, 163, 332, 0.5, 0)
	TablesMasterSlice = bugTablesSheet.sprite(965, 1782, 213, 265, 0.5, 0)
	TablesMicrowave   = bugTablesSheet.sprite(1180, 1795, 336, 252, 0.5, 0)

	TablesMadameButterfly = bugTablesSheet.sprite(1518, 1782, 187, 265, 0.5, 0)
	TablesCricketly       = bugTablesSheet.sprite(1707, 1732, 134, 315, 0.5, 0)
	TablesSandy           = bugTablesSheet.sprite(1, 1562, 208, 189, 0.5, 0)
	TablesClaire          = bugTablesSheet.sprite(211, 1502, 120, 249, 0.5, 0)
	TablesNeil            = bugTablesSheet.sprite(333, 1511, 153, 245, 0.5, 0)
	TablesXic             = bugTablesSheet.sprite(488, 1380, 141, 376, 0.5, 0)
	TablesGein            = bugTablesSheet.sprite(631, 1377, 142, 342, 0.5, 0)
	TablesYumnum          = bugTablesSheet.sprite(775, 1492, 182, 221, 0.5, 0)
	TablesSkirby          = bugTablesSheet.sprite(959, 1576, 167, 191, 0.5, 0)
	TablesCherryGuy       = bugTablesSheet.sprite(1128, 1532, 176, 248, 0.5, 0)
	TablesTyna            = bugTablesSheet.sprite(1306, 1469, 192, 324, 0.5, 0)
	TablesRina            = bugTablesSheet.sprite(1843, 1730, 122, 317, 0.5, 0)
	TablesSnail           = bugTablesSheet.sprite(775, 1177, 279, 313, 1, 0)
	TablesSnailTired      = bugTablesSheet.sprite(351, 962, 423, 161, 1, 0)

	TablesAnt        = bugTablesSheet.sprite(1498, 1495, 188, 285, 0.5, 0)
	TablesCelia      = bugTablesSheet.sprite(1688, 1494, 199, 236, 0.5, 0)
	TablesMothiva    = bugTablesSheet.sprite(1, 1257, 213, 303, 0.5, 0)
	TablesSlacker    = bugTablesSheet.sprite(1, 753, 348, 502, 0.5, 0)
	TablesMaki       = bugTablesSheet.sprite(1889, 1414, 154, 314, 0.5, 0)
	TablesSnakemouth = bugTablesSheet.sprite(501, 1125, 249, 250, 0.5, 0)

	TablesBugariaCommercial    = bugTablesSheet.sprite(1, 257, 256, 192, 0.5, 0)
	TablesDefiantRoot          = bugTablesSheet.sprite(259, 257, 256, 192, 0.5, 0)
	TablesUndergroundTavern    = bugTablesSheet.sprite(517, 257, 256, 192, 0.5, 0)
	TablesBeeCafeteria         = bugTablesSheet.sprite(775, 257, 256, 192, 0.5, 0)
	TablesGoldenSettlementFine = bugTablesSheet.sprite(1033, 257, 256, 192, 0.5, 0)
	TablesWaspKingdomCart      = bugTablesSheet.sprite(1291, 257, 256, 192, 0.5, 0)
	TablesDineMite             = bugTablesSheet.sprite(1549, 257, 256, 192, 0.5, 0)
	TablesMetalIsland          = bugTablesSheet.sprite(1, 451, 256, 192, 0.5, 0)
	TablesApartment            = bugTablesSheet.sprite(259, 451, 256, 192, 0.5, 0)
	TablesColosseum            = bugTablesSheet.sprite(517, 451, 256, 192, 0.5, 0)

	TablesItems = [...]*Sprite{
		bugTablesSheet.sprite(0, 0, 64, 64, 0.5, 0.5),
		bugTablesSheet.sprite(64, 0, 64, 64, 0.5, 0.5),
		bugTablesSheet.sprite(128, 0, 64, 64, 0.5, 0.5),
		bugTablesSheet.sprite(192, 0, 64, 64, 0.5, 0.5),
		bugTablesSheet.sprite(256, 0, 64, 64, 0.5, 0.5),
		bugTablesSheet.sprite(320, 0, 64, 64, 0.5, 0.5),
		bugTablesSheet.sprite(384, 0, 64, 64, 0.5, 0.5),
		bugTablesSheet.sprite(448, 0, 64, 64, 0.5, 0.5),
		bugTablesSheet.sprite(512, 0, 128, 64, 0.5, 0.5),
		bugTablesSheet.sprite(640, 0, 64, 64, 0.5, 0.5),
		bugTablesSheet.sprite(704, 0, 64, 64, 0.5, 0.5),
		bugTablesSheet.sprite(768, 0, 64, 64, 0.5, 0.5),
		bugTablesSheet.sprite(832, 0, 64, 64, 0.5, 0.5),
		bugTablesSheet.sprite(896, 0, 64, 64, 0.5, 0.5),
		bugTablesSheet.sprite(960, 0, 64, 64, 0.5, 0.5),
		bugTablesSheet.sprite(1024, 0, 64, 64, 0.5, 0.5),
		bugTablesSheet.sprite(1088, 0, 64, 64, 0.5, 0.5),
		bugTablesSheet.sprite(1152, 0, 64, 64, 0.5, 0.5),
		bugTablesSheet.sprite(1216, 0, 64, 64, 0.5, 0.5),
		bugTablesSheet.sprite(1280, 0, 64, 64, 0.5, 0.5),
		bugTablesSheet.sprite(1344, 0, 64, 64, 0.5, 0.5),
		bugTablesSheet.sprite(1408, 0, 64, 64, 0.5, 0.5),
		bugTablesSheet.sprite(1472, 0, 64, 64, 0.5, 0.5),
		bugTablesSheet.sprite(1536, 0, 64, 64, 0.5, 0.5),
		bugTablesSheet.sprite(1600, 0, 64, 64, 0.5, 0.5),
		bugTablesSheet.sprite(1664, 0, 64, 64, 0.5, 0.5),
		bugTablesSheet.sprite(1728, 0, 64, 64, 0.5, 0.5),
		bugTablesSheet.sprite(1792, 0, 64, 64, 0.5, 0.5),
		bugTablesSheet.sprite(1856, 0, 64, 64, 0.5, 0.5),
		bugTablesSheet.sprite(1920, 0, 64, 64, 0.5, 0.5),
		bugTablesSheet.sprite(1984, 0, 64, 64, 0.5, 0.5),
		bugTablesSheet.sprite(0, 64, 64, 64, 0.5, 0.5),
		bugTablesSheet.sprite(64, 64, 64, 64, 0.5, 0.5),
		bugTablesSheet.sprite(128, 64, 64, 64, 0.5, 0.5),
		bugTablesSheet.sprite(192, 64, 64, 64, 0.5, 0.5),
		bugTablesSheet.sprite(256, 64, 64, 64, 0.5, 0.5),
		bugTablesSheet.sprite(320, 64, 64, 64, 0.5, 0.5),
		bugTablesSheet.sprite(384, 64, 64, 64, 0.5, 0.5),
		bugTablesSheet.sprite(448, 64, 64, 64, 0.5, 0.5),
		bugTablesSheet.sprite(512, 64, 64, 64, 0.5, 0.5),
		bugTablesSheet.sprite(576, 64, 64, 64, 0.5, 0.5),
		bugTablesSheet.sprite(640, 64, 64, 64, 0.5, 0.5),
		bugTablesSheet.sprite(704, 64, 64, 64, 0.5, 0.5),
		bugTablesSheet.sprite(768, 64, 64, 64, 0.5, 0.5),
		bugTablesSheet.sprite(832, 64, 64, 64, 0.5, 0.5),
		bugTablesSheet.sprite(896, 64, 64, 64, 0.5, 0.5),
		bugTablesSheet.sprite(960, 64, 64, 64, 0.5, 0.5),
		bugTablesSheet.sprite(1024, 64, 64, 64, 0.5, 0.5),
		bugTablesSheet.sprite(1088, 64, 64, 64, 0.5, 0.5),
		bugTablesSheet.sprite(1152, 64, 64, 64, 0.5, 0.5),
		bugTablesSheet.sprite(1216, 64, 64, 64, 0.5, 0.5),
		bugTablesSheet.sprite(1280, 64, 64, 64, 0.5, 0.5),
		bugTablesSheet.sprite(1344, 64, 64, 64, 0.5, 0.5),
		bugTablesSheet.sprite(1408, 64, 64, 64, 0.5, 0.5),
		bugTablesSheet.sprite(1472, 64, 64, 64, 0.5, 0.5),
		bugTablesSheet.sprite(1536, 64, 64, 64, 0.5, 0.5),
		bugTablesSheet.sprite(1600, 64, 64, 64, 0.5, 0.5),
		bugTablesSheet.sprite(1664, 64, 64, 64, 0.5, 0.5),
		bugTablesSheet.sprite(1728, 64, 64, 64, 0.5, 0.5),
		bugTablesSheet.sprite(1792, 64, 64, 64, 0.5, 0.5),
		bugTablesSheet.sprite(1856, 64, 64, 64, 0.5, 0.5),
		bugTablesSheet.sprite(1920, 64, 64, 64, 0.5, 0.5),
		bugTablesSheet.sprite(1984, 64, 64, 64, 0.5, 0.5),
		bugTablesSheet.sprite(0, 128, 64, 64, 0.5, 0.5),
		bugTablesSheet.sprite(64, 128, 64, 64, 0.5, 0.5),
		bugTablesSheet.sprite(128, 128, 64, 64, 0.5, 0.5),
		bugTablesSheet.sprite(192, 128, 64, 64, 0.5, 0.5),
		bugTablesSheet.sprite(256, 128, 64, 64, 0.5, 0.5),
		bugTablesSheet.sprite(320, 128, 64, 64, 0.5, 0.5),
		bugTablesSheet.sprite(384, 128, 64, 64, 0.5, 0.5),
		bugTablesSheet.sprite(448, 128, 64, 64, 0.5, 0.5),
		bugTablesSheet.sprite(512, 128, 64, 64, 0.5, 0.5),
		bugTablesSheet.sprite(576, 128, 64, 64, 0.5, 0.5),
		bugTablesSheet.sprite(640, 128, 64, 64, 0.5, 0.5),
		bugTablesSheet.sprite(704, 128, 64, 64, 0.5, 0.5),
		bugTablesSheet.sprite(768, 128, 64, 64, 0.5, 0.5),
		bugTablesSheet.sprite(832, 128, 64, 64, 0.5, 0.5),
		bugTablesSheet.sprite(896, 128, 64, 64, 0.5, 0.5),
		bugTablesSheet.sprite(960, 128, 64, 64, 0.5, 0.5),
		bugTablesSheet.sprite(1024, 128, 64, 64, 0.5, 0.5),
		bugTablesSheet.sprite(1088, 128, 64, 64, 0.5, 0.5),
		bugTablesSheet.sprite(1152, 128, 64, 64, 0.5, 0.5),
		bugTablesSheet.sprite(1216, 128, 64, 64, 0.5, 0.5),
		bugTablesSheet.sprite(1280, 128, 64, 64, 0.5, 0.5),
		bugTablesSheet.sprite(1344, 128, 64, 64, 0.5, 0.5),
		bugTablesSheet.sprite(1408, 128, 64, 64, 0.5, 0.5),
		bugTablesSheet.sprite(1472, 128, 64, 64, 0.5, 0.5),
		bugTablesSheet.sprite(1536, 128, 64, 64, 0.5, 0.5),
		bugTablesSheet.sprite(1600, 128, 64, 64, 0.5, 0.5),
		bugTablesSheet.sprite(1664, 128, 64, 64, 0.5, 0.5),
		bugTablesSheet.sprite(1728, 128, 64, 64, 0.5, 0.5),
		bugTablesSheet.sprite(1792, 128, 64, 64, 0.5, 0.5),
		bugTablesSheet.sprite(1856, 128, 64, 64, 0.5, 0.5),
		bugTablesSheet.sprite(1920, 128, 64, 64, 0.5, 0.5),
		bugTablesSheet.sprite(1984, 128, 64, 64, 0.5, 0.5),
		bugTablesSheet.sprite(0, 192, 64, 64, 0.5, 0.5),
		bugTablesSheet.sprite(64, 192, 64, 64, 0.5, 0.5),
		bugTablesSheet.sprite(128, 192, 64, 64, 0.5, 0.5),
		bugTablesSheet.sprite(192, 192, 64, 64, 0.5, 0.5),
		bugTablesSheet.sprite(256, 192, 64, 64, 0.5, 0.5),
		bugTablesSheet.sprite(320, 192, 64, 64, 0.5, 0.5),
		bugTablesSheet.sprite(384, 192, 64, 64, 0.5, 0.5),
		bugTablesSheet.sprite(448, 192, 64, 64, 0.5, 0.5),
		bugTablesSheet.sprite(512, 192, 64, 64, 0.5, 0.5),
		bugTablesSheet.sprite(576, 192, 64, 64, 0.5, 0.5),
	}
)
