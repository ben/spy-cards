package sprites

// GUI sprites for Spy Cards.
var (
	gui = loadSpriteSheet("img/arcade/gui.webp", 75, 2048, 2048, false).extraDownScale(-1)

	WhiteBar       = gui.sprite(1, 975, 438.9732, 48, 0.5000305, 0.5)
	WhiteBarCorner = gui.sprite(1, 1004, 20, 20, 0.5, 0.5)
	HP             = gui.sprite(1493, 26, 125, 132, 0.5082959, 0.5366576)
	ATK            = gui.sprite(1636.027, 17, 140.9733, 144, 0.4999052, 0.5)
	DEF            = gui.sprite(1638, 179, 124, 132, 0.5, 0.5)
	TP             = gui.sprite(783, 96, 82, 81, 0.5, 0.5)
	Berries        = gui.sprite(776, 6, 81, 81, 0.5, 0.5)
	CardNum        = [10]*Sprite{
		gui.sprite(10, 480, 32, 58, 0.5, 0.5),
		gui.sprite(43, 479, 23, 59, 0.5, 0.5),
		gui.sprite(69, 480, 33, 58, 0.5, 0.5),
		gui.sprite(103, 479, 32, 59, 0.5, 0.5),
		gui.sprite(136, 480, 34, 58, 0.5, 0.5),
		gui.sprite(171, 478, 34, 60, 0.5, 0.5),
		gui.sprite(206, 479, 34, 62, 0.5, 0.5),
		gui.sprite(241, 480, 32, 59.97325, 0.5, 0.5085601),
		gui.sprite(275, 479, 34, 59, 0.5, 0.5),
		gui.sprite(311, 477, 34, 62, 0.5, 0.5),
	}
	CardFront   = gui.sprite(540, 1225, 174, 237, 0.5, 0.5)
	Pointer     = gui.sprite(1920.027, 1350, 124.9733, 78, 1, 0.5)
	Capsule     = gui.sprite(1657, 1839, 389, 206, 0.5, 0.5)
	CircleArrow = gui.sprite(0, 919, 52, 52, 0.5, 0.5)
	WideArrow   = gui.sprite(104, 949, 315, 21, 0.5, 0.5)
	Tab         = gui.sprite(3, 550, 429, 134, 0.5, 0.5)
	Damage      = gui.sprite(1788, 9, 258, 256, 0.5131046, 0.4544878)
	Heal        = gui.sprite(1794, 539, 247, 250, 0.4998799, 0.5567427)
	Freeze      = gui.sprite(5, 374, 89, 87, 0.5, 0.5)
	Poison      = gui.sprite(99, 373, 67, 79, 0.5, 0.5)
	Numb        = gui.sprite(169, 369, 95, 89, 0.5, 0.5)
	Sleep       = gui.sprite(268, 371, 97, 87, 0.5, 0.5)
	Burn        = gui.sprite(1257, 356, 71, 85, 0.4837148, 0.3773563)
	Dizzy       = gui.sprite(516, 260.0513, 85, 87.94868, 0.5, 0.4997081)
	Dizzy2      = gui.sprite(517, 177.0761, 76, 78.92388, 0.5, 0.4931825)
	Fortify     = gui.sprite(611, 174, 81, 88, 0.5, 0.5)
	Break       = gui.sprite(699, 263, 81, 90, 0.5, 0.5)
	Empower     = gui.sprite(788, 267, 73, 83, 0.5, 0.5)
	Enfeeble    = gui.sprite(704, 174, 72, 86, 0.5, 0.5)
	Tablet      = gui.sprite(973, 187, 56, 84, 0.5214061, 0.4624701)
	Meter       = gui.sprite(2, 744, 517, 150, 0.06385988, 0.6441321)
	MeterLong   = gui.sprite(0, 1502, 786, 152, 0.04200072, 0.6407889)
	WoodenOrb   = gui.sprite(1552, 606, 229, 301.9665, 0.4842348, 0.371527)

	portraits = loadSpriteSheet("img/portraits2.webp", 100, 2048, 2048, false).extraDownScale(-1)

	Portraits = func() (p [256]*Sprite) {
		for i := range p {
			p[i] = portraits.sprite(float32(i%16)*128, 2048-float32(i/16+1)*128, 128, 128, 0.5, 0.5)
		}

		return
	}()

	customCard = loadSpriteSheet("img/customcard.webp", 75, 1024, 512, false)

	CardFrontWindow = customCard.sprite(1, 274, 174, 237, 0.5, 0.5)
	TPInf           = customCard.sprite(427, 428, 84, 84, 0.5, 0.5)
	TribeBubble     = customCard.sprite(176, 459, 122, 26, 0.5, 0.5)
	TribeBubbleWide = customCard.sprite(176, 487, 252, 25, 0.5, 0.5)
	Description     = customCard.sprite(177, 329, 201.5, 130, 0.5, 0.5)
	BackToken       = customCard.sprite(1, 36, 174, 237, 0.5, 0.5)
	BackAttacker    = customCard.sprite(177, 36, 174, 237, 0.5, 0.5)
	BackEffect      = customCard.sprite(353, 36, 174, 237, 0.5, 0.5)
	BackMiniBoss    = customCard.sprite(529, 36, 174, 237, 0.5, 0.5)
	BackBoss        = customCard.sprite(705, 36, 174, 237, 0.5, 0.5)
	FrontToken      = customCard.sprite(514, 401, 40, 110, 0.5, 0.2)
	FrontAttacker   = customCard.sprite(556, 401, 40, 110, 0.5, 0.2)
	FrontEffect     = customCard.sprite(598, 401, 40, 110, 0.5, 0.2)
	FrontMiniBoss   = customCard.sprite(514, 289, 40, 110, 0.5, 0.2)
	FrontBoss       = customCard.sprite(556, 289, 40, 110, 0.5, 0.2)
	Happy           = customCard.sprite(435, 371, 76, 56, 0.5, 0.5)
	Sad             = customCard.sprite(426, 316, 85, 54, 0.5, 0.5)
	DeleteButton    = customCard.sprite(379, 374, 52, 52, 0.5, 0.5)
	CheapCoin       = customCard.sprite(881, 145, 128, 128, 0.5, 0.5)
	MenuIcon        = [6]*Sprite{
		customCard.sprite(599, 341, 49, 57, 0.5, 0.5),
		customCard.sprite(180, 276, 44, 44, 0.5, 0.5),
		customCard.sprite(227, 276, 41, 44, 0.5, 0.5),
		customCard.sprite(275, 284, 71, 27, 0.5, 0.5),
		customCard.sprite(352, 278, 44, 45, 0.5, 0.5),
		customCard.sprite(600, 279, 50, 60, 0.5, 0.5),
	}
)
