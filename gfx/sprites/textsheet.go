package sprites

import (
	"git.lubar.me/ben/spy-cards/gfx"
)

func embeddedTextSheet(unitPerPx float32, nearest bool, dx, dy int, mtsdf []byte) *spriteSheet {
	return &spriteSheet{
		AssetTexture: gfx.NewEmbeddedTexture("(fonts)", nearest, true, mtsdf, "image/webp", false),

		width:     dx,
		height:    dy,
		unitPerPx: unitPerPx,
	}
}

func embeddedSheet(name string, unitPerPx float32, nearest bool, dx, dy int, src []byte) *spriteSheet {
	return &spriteSheet{
		AssetTexture: gfx.NewEmbeddedTexture(name, nearest, true, src, "image/webp", true),

		width:     dx,
		height:    dy,
		unitPerPx: unitPerPx,
	}
}
