//go:generate stringer -type FontID -trimprefix Font

package sprites

import (
	"math"
	"time"

	"git.lubar.me/ben/spy-cards/gfx"
	"git.lubar.me/ben/spy-cards/input"
	"git.lubar.me/ben/spy-cards/internal"
)

var (
	textAdvance = [...]map[rune]float32{
		FontD3Streetism:   d3StreetismAdvance,
		FontBubblegumSans: bubblegumSansAdvance,
	}
	textAdvanceASCII = [...][128]float32{
		FontD3Streetism:   fontASCIIAdvance(FontD3Streetism),
		FontBubblegumSans: fontASCIIAdvance(FontBubblegumSans),
	}
)

// FontID is a font.
type FontID uint8

// Available fonts.
const (
	FontD3Streetism FontID = iota
	FontBubblegumSans
)

func shadowColor(l rune) Color {
	if l < 32 {
		return Color{0, 0, 0, 0}
	}

	return Color{0, 0, 0, 127}
}

// LetterSupported returns true if a letter can be rendered in a given font.
func LetterSupported(font FontID, letter rune) bool {
	if letter < 32 {
		return letter < input.NumButtons
	}

	if letter == ' ' || letter == '\n' {
		return true
	}

	if _, ok := textAdvance[font][letter]; ok {
		return true
	}

	if _, ok := arialAdvance[letter]; ok {
		return true
	}

	if _, ok := arialInfAdvance[letter]; ok {
		return true
	}

	if _, ok := dejavuAdvance[letter]; ok {
		return true
	}

	return false
}

// TextAdvance returns the number of units the next letter is offset after this letter.
func TextAdvance(font FontID, letter rune, scaleX float32) float32 {
	if int(letter) < len(textAdvanceASCII[font]) {
		return textAdvanceASCII[font][letter] * scaleX
	}

	return lookupLetterAdvanceSlow(font, letter) * scaleX
}

func fontASCIIAdvance(font FontID) [128]float32 {
	var a [128]float32

	for i := range a {
		a[i] = lookupLetterAdvanceSlow(font, rune(i))
	}

	return a
}

func lookupLetterAdvanceSlow(font FontID, letter rune) float32 {
	if letter < 32 {
		return 0.6
	}

	if letter == ' ' {
		return 0.3
	}

	if a, ok := textAdvance[font][letter]; ok {
		return a
	}

	if a, ok := arialAdvance[letter]; ok {
		return a
	}

	if a, ok := arialInfAdvance[letter]; ok {
		return a
	}

	if a, ok := dejavuAdvance[letter]; ok {
		return a
	}

	return 0.3
}

// DrawText draws a string to the screen.
func DrawText(tb *Batch, font FontID, text string, x, y, z, scaleX, scaleY float32, tint Color) {
	DrawTextFunc(tb, font, text, x, y, z, scaleX, scaleY, getFunc(tint))
}

// DrawTextFunc draws a string to the screen.
func DrawTextFunc(tb *Batch, font FontID, text string, x, y, z, scaleX, scaleY float32, getColor func(rune) Color) {
	DrawTextFuncEx(tb, font, text, x, y, z, scaleX, scaleY, getColor, FlagIsMTSDF, 0, 0, 0)
}

// DrawTextShadow draws a string to the screen with a drop shadow.
func DrawTextShadow(tb *Batch, font FontID, text string, x, y, z, scaleX, scaleY float32, tint Color) {
	DrawTextShadowFunc(tb, font, text, x, y, z, scaleX, scaleY, getFunc(tint))
}

// DrawTextShadowFunc draws a string to the screen with a drop shadow.
func DrawTextShadowFunc(tb *Batch, font FontID, text string, x, y, z, scaleX, scaleY float32, getColor func(rune) Color) {
	DrawTextFunc(tb, font, text, x-0.1, y-0.1, z, scaleX, scaleY, shadowColor)
	DrawTextFunc(tb, font, text, x, y, z, scaleX, scaleY, getColor)
}

// DrawTextCentered draws a string to the screen center-justified.
func DrawTextCentered(tb *Batch, font FontID, text string, x, y, z, scaleX, scaleY float32, tint Color, shadow bool) {
	DrawTextCenteredFunc(tb, font, text, x, y, z, scaleX, scaleY, getFunc(tint), shadow)
}

// DrawTextCenteredFunc draws a string to the screen center-justified.
func DrawTextCenteredFunc(tb *Batch, font FontID, text string, x, y, z, scaleX, scaleY float32, getColor func(rune) Color, shadow bool) {
	var width float32

	for _, letter := range text {
		width += TextAdvance(font, letter, scaleX)
	}

	x -= width / 2

	if shadow {
		DrawTextShadowFunc(tb, font, text, x, y, z, scaleX, scaleY, getColor)
	} else {
		DrawTextFunc(tb, font, text, x, y, z, scaleX, scaleY, getColor)
	}
}

// DrawTextBorder draws text to the screen with an outline on each letter.
func DrawTextBorder(tb *Batch, font FontID, text string, x, y, z, scaleX, scaleY float32, textColor, borderColor Color, centered bool) float32 {
	if centered {
		var width float32

		for _, letter := range text {
			width += TextAdvance(font, letter, scaleX)
		}

		x -= width / 2
	}

	if borderColor == Black {
		tb.reserve(len(text))

		return DrawTextFuncEx(tb, font, text, x, y, z, scaleX, scaleY, getFunc(textColor), FlagIsMTSDF|FlagBorder, 0, 0, 0)
	}

	tb.reserve(len(text) * 2)

	DrawTextFuncEx(tb, font, text, x, y, z, scaleX, scaleY, getFunc(borderColor), FlagBorder, 0, 0, 0)

	return DrawTextFuncEx(tb, font, text, x, y, z, scaleX, scaleY, getFunc(textColor), FlagIsMTSDF, 0, 0, 0)
}

// TextHitTest returns true if the cursor position is sufficiently close to the text.
// Assumes the text is displayed in a rectangle-ish region of the screen.
func TextHitTest(cam *gfx.Camera, font FontID, text string, x, y, z, scaleX, scaleY, curX, curY float32, centered bool) bool {
	var width float32

	for _, letter := range text {
		width += TextAdvance(font, letter, scaleX)
	}

	if centered {
		x -= width / 2
	}

	return HitTest(cam, x-scaleX*0.1, y-scaleY*0.2, z, x+width+scaleX*0.1, y+scaleY*0.6, z, curX, curY)
}

// HitTest returns true if the cursor is within the rectangle defined by the
// screen coordinates of (x0, y0, z0) and (x1, y1, z1).
func HitTest(cam *gfx.Camera, x0, y0, z0, x1, y1, z1, curX, curY float32) bool {
	curX = curX*2 - 1
	curY = 1 - curY*2

	var (
		combined gfx.Matrix
		v0, v1   gfx.Vector
		min, max gfx.Vector
	)

	cam.Combined(&combined)

	v0.Multiply(&combined, &gfx.Vector{x0, y0, z0, 1})
	v1.Multiply(&combined, &gfx.Vector{x1, y1, z1, 1})
	min.Multiply(&cam.Perspective, &v0)
	max.Multiply(&cam.Perspective, &v1)

	min[0] /= min[3]
	min[1] /= min[3]
	max[0] /= max[3]
	max[1] /= max[3]

	if min[0] > max[0] {
		min[0], max[0] = max[0], min[0]
	}

	if min[1] > max[1] {
		min[1], max[1] = max[1], min[1]
	}

	return min[0] <= curX && max[0] >= curX && min[1] <= curY && max[1] >= curY
}

func SpriteHitTest(cam *gfx.Camera, sprite *Sprite, x, y, z, sx, sy, curX, curY float32) bool {
	return HitTest(cam, x+sprite.X0*sx, y+sprite.Y0*sy, z, x+sprite.X1*sx, y+sprite.Y1*sy, z, curX, curY)
}

func getFunc(c Color) func(rune) Color {
	if c == Rainbow {
		now := time.Duration(time.Now().UnixNano()).Seconds()
		if internal.PrefersReducedMotion() {
			now = 0
		}

		clamp := func(f float64) uint8 {
			return uint8(math.Min(math.Max(f*255, 0), 255))
		}

		return func(letter rune) Color {
			if letter < 32 {
				return White
			}

			now++

			return Color{
				clamp(math.Sin((now+20.0)*5.9) * 2.0),
				clamp(math.Sin((now)*5.9) * 2.0),
				clamp(math.Sin((now+60.0)*5.9) * 2.0),
				255,
			}
		}
	}

	return func(letter rune) Color {
		return c
	}
}

// Button returns a string that will be rendered as a button icon.
func Button(btn input.Button) string {
	return string(byte(btn))
}

// ButtonStyle determines the icon type used to display buttons in text.
var ButtonStyle = internal.StyleGenericGamepad
