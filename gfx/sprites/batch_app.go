//go:build (!js || !wasm) && !headless
// +build !js !wasm
// +build !headless

package sprites

import "golang.org/x/mobile/gl"

func (sb *Batch) fastBufferData(shouldReallocate bool) bool {
	return false
}

func (sb *Batch) fastSetUniforms(*[8]gl.Texture) bool {
	return false
}

func (sb *Batch) fastSetPointers() bool {
	return false
}

func (sb *Batch) fastDrawElements() bool {
	return false
}
