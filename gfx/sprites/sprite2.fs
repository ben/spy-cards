#extension GL_OES_standard_derivatives : enable

#ifdef GL_FRAGMENT_PRECISION_HIGH
precision highp float;
#else
precision mediump float;
#endif

uniform sampler2D tex[8];

uniform float time;

varying vec4 position;

// xy: tex_coord
// z: texid
varying vec3 packed_0;

// x: fog_distance
// y: rainbow
// z: mk_fog
// w: no_discard
varying vec4 packed_1;

// x: border
// y: is_mtsdf
// z: half_glow
// w: (unused)
varying vec4 packed_2;

varying vec4 color;

vec4 texture2Darr(float tex_id, vec2 coord) {
	if (tex_id < 4.0) {
		if (tex_id < 2.0) {
			if (tex_id < 1.0) {
				return texture2D(tex[0], coord);
			} else {
				return texture2D(tex[1], coord);
			}
		} else {
			if (tex_id < 3.0) {
				return texture2D(tex[2], coord);
			} else {
				return texture2D(tex[3], coord);
			}
		}
	} else {
		if (tex_id < 6.0) {
			if (tex_id < 5.0) {
				return texture2D(tex[4], coord);
			} else {
				return texture2D(tex[5], coord);
			}
		} else {
			if (tex_id < 7.0) {
				return texture2D(tex[6], coord);
			} else {
				return texture2D(tex[7], coord);
			}
		}
	}
}

float median3(vec3 v) {
	if (v.x > v.y) { v.xy = v.yx; }
	if (v.y > v.z) { v.yz = v.zy; }
	if (v.x > v.y) { v.xy = v.yx; }
	return v.y;
}

void main() {
	vec4 texColor = texture2Darr(packed_0.z, packed_0.xy);

	// MTSDF
	if (packed_2.x > 0.5 || packed_2.y > 0.5) {
		// antialiasing from Andrew Cassidy's write-up:
		// https://drewcassidy.me/2020/06/26/sdf-antialiasing/

		float borderDist = 1.0;
		float innerDist = 1.0;
		if (packed_2.x > 0.5) {
			borderDist = 0.25 - texColor.a;
		}
		if (packed_2.y > 0.5) {
			innerDist = 0.5 - median3(texColor.rgb);
		}

#ifdef GL_OES_standard_derivatives
		vec2 dBorder = vec2(dFdx(borderDist), dFdy(borderDist));
		vec2 dInner = vec2(dFdx(innerDist), dFdy(innerDist));

		float pixelBorder = borderDist / length(dBorder);
		float pixelInner = innerDist / length(dInner);
#else
		// cheat a little bit using knowledge of how Spy Cards Online sizes text
		float pixelBorder = borderDist * 4.0;
		float pixelInner = innerDist * 4.0;
		if (length(color.rgb) > 0.25 || color.a < 0.99 || packed_2.x > 0.5) {
			pixelBorder *= 4.0;
			pixelInner *= 4.0;
		}
#endif

		if (pixelBorder > 0.5 && pixelInner > 0.5) {
			discard;
		}

		if (packed_2.x <= 0.5) {
			texColor = vec4(clamp(0.5 - pixelInner, 0.0, 1.0));
		} else if (packed_2.y <= 0.5) {
			texColor = vec4(clamp(0.5 - pixelBorder, 0.0, 1.0));
		} else {
			texColor = vec4(
				vec3(clamp(0.5 - pixelInner, 0.0, 1.0)),
				clamp(0.5 - pixelBorder, 0.0, 1.0)
			);
		}
	} else if (texColor.a < 0.5 && packed_1.w < 0.5) {
		discard;
	}

	vec4 rainbowColor = vec4(1.0);

	if (packed_1.y > 0.5) {
		float variant = position.x * -1.5;

		rainbowColor.r = clamp(sin((time + variant + 20.0) * 5.9) * 2.0, 0.0, 1.0);
		rainbowColor.g = clamp(sin((time + variant) * 5.9) * 2.0, 0.0, 1.0);
		rainbowColor.b = clamp(sin((time + variant + 60.0) * 5.9) * 2.0, 0.0, 1.0);
	}

	gl_FragColor = texColor * rainbowColor * color;

	if (packed_1.z > 0.5) {
		gl_FragColor.rgb *= clamp((5.0 - packed_1.x) / 5.0, 0.0, 1.0);
	}

	if (packed_2.z > 0.5) {
		gl_FragColor.rgb = gl_FragColor.rgb * 3.0 / 4.0 + 0.25 * gl_FragColor.a;
	}
}
