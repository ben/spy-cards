//go:build headless
// +build headless

package sprites

// DrawTextFuncEx draws a string to the screen.
func DrawTextFuncEx(tb *Batch, font FontID, text string, x, y, z, scaleX, scaleY float32, getColor func(rune) Color, flags RenderFlag, rx, ry, rz float32) float32 {
	return 0 // shouldn't be used, so the wrong answer is fine
}

func PreloadText() error {
	return nil
}

func TextReady() bool {
	return true
}

// DrawLetter draws one letter to the screen.
func DrawLetter(tb *Batch, font FontID, letter rune, x, y, z, scaleX, scaleY float32, tint Color, flags RenderFlag, rx, ry, rz float32) {
	// do nothing
}
