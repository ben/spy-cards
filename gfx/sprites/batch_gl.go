//go:build !headless
// +build !headless

package sprites

import (
	_ "embed" // embedded shaders
	"runtime"
	"strconv"
	"time"
	"unsafe"

	"git.lubar.me/ben/spy-cards/gfx"
	"golang.org/x/mobile/gl"
)

var (
	//go:embed sprite2.vs
	spriteVertex2 string
	//go:embed sprite2.fs
	spriteFragment2 string
	//go:embed sprite3.vs
	spriteVertex3 string
	//go:embed sprite3.fs
	spriteFragment3 string

	startTime = time.Now()

	spriteProgram = gfx.Shader("sprite", spriteVertex2, spriteFragment2, spriteVertex3, spriteFragment3)

	spriteCamera          = spriteProgram.Uniform("camera")
	spriteTime            = spriteProgram.Uniform("time")
	spritePosition        = spriteProgram.Attrib("in_pos")
	spriteTexCoord        = spriteProgram.Attrib("in_texcoord")
	spriteFlagsAndTexture = spriteProgram.Attrib("flags_and_texture")
	spriteColor           = spriteProgram.Attrib("in_color")

	_ = gfx.Custom("sprite-tex-ids", func() {
		gfx.GL.UseProgram(spriteProgram.Program)

		for i := len((Batch{}).texture) - 1; i >= 0; i-- {
			gfx.GL.Uniform1i(gfx.GL.GetUniformLocation(spriteProgram.Program, "tex["+strconv.Itoa(i)+"]"), i)
			gfx.GL.ActiveTexture(gl.TEXTURE0 + gl.Enum(i))
			gfx.GL.BindTexture(gl.TEXTURE_2D, Blank.Texture())
		}
	}, func() {})
)

func (e *sharedElemBuffer) upload() {
	// gfx.Lock should already be held and a vertex array should already be
	// set if available
	if e.buf == (gl.Buffer{}) {
		e.buf = gfx.GL.CreateBuffer()
	}

	gfx.GL.BindBuffer(gl.ELEMENT_ARRAY_BUFFER, e.buf)
	gfx.GL.BufferData(gl.ELEMENT_ARRAY_BUFFER, e.elem, gl.STATIC_DRAW)

	gfx.StatRecordBufferData(1)

	e.dirty = false
}

// Render draws the sprite batch to the screen.
func (sb *Batch) Render() {
	if sb.count == 0 {
		return
	}

	gfx.Lock.Lock()

	_, haveVAO := gfx.GL.(gl.Context3)

	if !sb.haveBuf {
		if haveVAO {
			sb.vao = gfx.GL.CreateVertexArray()
			gfx.GL.BindVertexArray(sb.vao)
			sb.enableArrays()
		}

		sb.lastBuf = 0
		sb.dataBuf = gfx.GL.CreateBuffer()
		sb.haveBuf = true
		sb.dirtyBuf = true

		runtime.SetFinalizer(sb, (*Batch).dropBuffers)
	} else if haveVAO {
		gfx.GL.BindVertexArray(sb.vao)
	}

	if sb.dirtyBuf {
		shouldReallocate := sb.lastBuf < len(sb.data)
		if shouldReallocate {
			sb.lastBuf = len(sb.data)
		}

		if !sb.fastBufferData(shouldReallocate) {
			dataBytes := unsafe.Slice((*byte)(unsafe.Pointer(&sb.data[0])), len(sb.data)*int(unsafe.Sizeof(sb.data[0])))

			gfx.GL.BindBuffer(gl.ARRAY_BUFFER, sb.dataBuf)

			if shouldReallocate {
				gfx.GL.BufferData(gl.ARRAY_BUFFER, dataBytes, gl.DYNAMIC_DRAW)
			} else {
				gfx.GL.BufferSubData(gl.ARRAY_BUFFER, 0, dataBytes)
			}
		}

		if haveVAO {
			sb.setPointers()
		}

		gfx.StatRecordBufferData(1)

		sb.dirtyBuf = false
	}

	gfx.StatRecordTriangleBatch(sb.count)

	blankTexture := Blank.Texture()

	var textures [len(sb.texture)]gl.Texture

	for i, t := range sb.texture {
		if t != nil {
			gfx.StatRecordTextureUse(t)

			textures[i] = t.LazyTexture(blankTexture)
		} else if sb.AvoidTextureFeedback {
			textures[i] = blankTexture
		}
	}

	if !sb.fastSetUniforms(&textures) {
		gfx.GL.UseProgram(spriteProgram.Program)

		gfx.GL.UniformMatrix4fv(spriteCamera.Uniform, (*[4 * 4 * 2]float32)(unsafe.Pointer(&sb.camMatrix[0][0]))[:])

		for i := len(textures) - 1; i >= 0; i-- {
			if textures[i] != (gl.Texture{}) {
				gfx.GL.ActiveTexture(gl.TEXTURE0 + gl.Enum(i))
				gfx.GL.BindTexture(gl.TEXTURE_2D, textures[i])
			}
		}

		gfx.GL.Uniform1f(spriteTime.Uniform, float32(time.Since(startTime).Seconds()))
	}

	if !haveVAO {
		gfx.GL.BindBuffer(gl.ARRAY_BUFFER, sb.dataBuf)
		sb.enableArrays()
		sb.setPointers()
	}

	if !sb.fastDrawElements() {
		gfx.GL.BindBuffer(gl.ELEMENT_ARRAY_BUFFER, sharedElements.get())

		gfx.GL.DrawElements(gl.TRIANGLES, sb.count, gl.UNSIGNED_INT, 0)
	}

	if !haveVAO {
		sb.disableArrays()
	}

	gfx.Lock.Unlock()

	runtime.KeepAlive(sb)
}

func (sb *Batch) enableArrays() {
	gfx.GL.EnableVertexAttribArray(spritePosition.Attrib)
	gfx.GL.EnableVertexAttribArray(spriteTexCoord.Attrib)
	gfx.GL.EnableVertexAttribArray(spriteFlagsAndTexture.Attrib)
	gfx.GL.EnableVertexAttribArray(spriteColor.Attrib)
}

func (sb *Batch) disableArrays() {
	// don't disable attrib 0 or 1
	gfx.GL.DisableVertexAttribArray(spriteFlagsAndTexture.Attrib)
	gfx.GL.DisableVertexAttribArray(spriteColor.Attrib)
}

func (sb *Batch) setPointers() {
	if sb.fastSetPointers() {
		return
	}

	gfx.GL.VertexAttribPointer(spritePosition.Attrib, 4, gl.FLOAT, false, 28, 0)
	gfx.GL.VertexAttribPointer(spriteTexCoord.Attrib, 2, gl.UNSIGNED_SHORT, true, 28, 16)
	gfx.GL.VertexAttribPointer(spriteFlagsAndTexture.Attrib, 2, gl.UNSIGNED_SHORT, false, 28, 20)
	gfx.GL.VertexAttribPointer(spriteColor.Attrib, 4, gl.UNSIGNED_BYTE, true, 28, 24)
}

func (sb *Batch) dropBuffers() {
	if sb.haveBuf {
		gfx.Lock.Lock()
		gfx.GL.DeleteBuffer(sb.dataBuf)

		if _, haveVAO := gfx.GL.(gl.Context3); haveVAO {
			gfx.GL.DeleteVertexArray(sb.vao)
		}

		gfx.Lock.Unlock()
		sb.haveBuf = false
		runtime.SetFinalizer(sb, nil)
	}
}
