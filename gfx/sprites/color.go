package sprites

// Color is equivalent to image/color.RGBA, but is a different type
// to avoid having to import the entire package for one structure
// definition.
type Color struct {
	R, G, B, A uint8
}

// Colors for sprites.
var (
	Red      = Color{255, 0, 0, 255}
	Yellow   = Color{255, 235, 4, 255}
	Green    = Color{0, 255, 0, 255}
	Gray     = Color{127, 127, 127, 255}
	LGray    = Color{191, 191, 191, 255}
	DGray    = Color{31, 31, 31, 255}
	White    = Color{255, 255, 255, 255}
	Black    = Color{0, 0, 0, 255}
	Rainbow  = Color{1, 1, 1, 0} // special case
	MKStone  = Color{229, 102, 0, 255}
	MKPurple = Color{127, 0, 165, 255}
	MKBlue   = Color{127, 127, 255, 255}
)
