//go:build !headless
// +build !headless

// Package gfx is a thin abstraction layer for OpenGL.
package gfx

import (
	"sync"

	"git.lubar.me/ben/spy-cards/internal"
	"git.lubar.me/ben/spy-cards/internal/router"
	"golang.org/x/mobile/gl"
)

// GL is the OpenGL context.
var GL gl.Context

// Lock should be held whenever an operation depends on or modifies global OpenGL state.
var Lock sync.Mutex

type GPUPowerLevel int

// Constants for GPUPowerLevel.
const (
	GPULow    GPUPowerLevel = 0
	GPUMedium GPUPowerLevel = 1
	GPUHigh   GPUPowerLevel = 2
)

// GPULevel is 0, 1, or 2, for low-, medium-, or high-powered GPUs.
var GPULevel GPUPowerLevel = -1

var onInit = sync.NewCond(&Lock)

// Init is an internal function that sets up the context.
func Init(ctx gl.Context) {
	internal.PerformanceMark("glInitStart")

	Lock.Lock()

	GL = ctx
	GL.Enable(gl.BLEND)
	GL.DepthFunc(gl.LESS)
	GL.BlendFunc(gl.ONE, gl.ONE_MINUS_SRC_ALPHA)
	GL.ClearColor(0, 0, 0, 1)
	GL.Clear(gl.COLOR_BUFFER_BIT)
	GL.Enable(gl.CULL_FACE)
	GL.CullFace(gl.BACK)

	_ = jsLoadExtension("OES_element_index_uint")
	_ = jsLoadExtension("OES_standard_derivatives")
	_ = jsLoadExtension("WEBGL_depth_texture")
	_ = jsLoadExtension("WEBGL_draw_buffers")
	_ = jsLoadExtension("EXT_frag_depth")

	GPULevel = platformGPULevelDefault()

	if limit := GPUPowerLevel(internal.LoadSettings().LimitGPULevel - 1); limit >= 0 && GPULevel > limit {
		GPULevel = limit
	}

	if router.FlagForceLowGPU.IsSet() {
		GPULevel = GPULow
	} else if router.FlagForceMediumGPU.IsSet() {
		GPULevel = GPUMedium
	}

	switch maxTex := GL.GetInteger(gl.MAX_TEXTURE_SIZE); {
	case maxTex >= 16384 && GPULevel >= GPUHigh:
		GPULevel = GPUHigh
	case maxTex >= 8192 && GPULevel >= GPUMedium:
		GPULevel = GPUMedium
	default:
		GPULevel = GPULow
	}

	if GPULevel == GPULow {
		GL.Hint(gl.FRAGMENT_SHADER_DERIVATIVE_HINT, gl.FASTEST)
		GL.Hint(gl.GENERATE_MIPMAP_HINT, gl.FASTEST)
	} else {
		GL.Hint(gl.FRAGMENT_SHADER_DERIVATIVE_HINT, gl.NICEST)
		GL.Hint(gl.GENERATE_MIPMAP_HINT, gl.NICEST)
	}

	finishProgram := make([]func(), len(programs))
	for i, p := range programs {
		finishProgram[i] = p.init()
	}

	for _, t := range textures {
		t.init()
	}

	for _, fp := range finishProgram {
		fp()
	}

	for _, sb := range buffers {
		sb.init()
	}

	for _, c := range custom {
		c.init()
	}

	onInit.Broadcast()

	Lock.Unlock()

	internal.PerformanceMark("glInitFinish")
	internal.PerformanceMeasure("glInit", "glInitStart", "glInitFinish")
}

// Release is an internal function that shuts down the context.
func Release() {
	Lock.Lock()

	for _, p := range programs {
		p.release()
	}

	for _, sb := range buffers {
		sb.release()
	}

	for _, t := range textures {
		t.release()
	}

	for _, c := range custom {
		c.release()
	}

	GL = nil

	Lock.Unlock()
}

var custom []*CustomInit

// CustomInit is a custom OpenGL init/release function pair.
type CustomInit struct {
	Name    string
	init    func()
	release func()
}

// Custom registers a CustomInit.
func Custom(name string, init, release func()) *CustomInit {
	c := &CustomInit{
		Name:    name,
		init:    init,
		release: release,
	}

	Lock.Lock()

	custom = append(custom, c)

	if GL != nil {
		c.init()
	}

	Lock.Unlock()

	return c
}

func Do(f func()) {
	Lock.Lock()

	if GL == nil {
		go func() {
			Lock.Lock()

			for GL == nil {
				onInit.Wait()
			}

			f()

			Lock.Unlock()
		}()
	} else {
		f()
	}

	Lock.Unlock()
}
