//go:build !headless
// +build !headless

package gfx

import (
	"fmt"
	"log"
	"runtime"
	"time"

	"git.lubar.me/ben/spy-cards/internal/router"
)

type frameTime struct {
	Start time.Time
	End   time.Time
}

type frameStats struct {
	FrameNS    uint32
	DelayNS    uint32
	Batches    uint32
	Triangles  uint32
	Textures   uint32
	BufferData uint32
}

var (
	timeBuf  [300]frameTime
	statBuf  [300]frameStats
	statIdx  = -1
	memStats runtime.MemStats

	statUsedTextures = make(map[*AssetTexture]struct{})

	perfLog [60]string
)

func statStartFrame() {
	now := time.Now()
	prev := now

	if statIdx >= 0 {
		prev = timeBuf[statIdx].End
	}

	statIdx++

	if statIdx >= len(statBuf) {
		statIdx = 0

		logStats()
	}

	timeBuf[statIdx] = frameTime{
		Start: now,
	}
	statBuf[statIdx] = frameStats{
		DelayNS: uint32(now.Sub(prev)),
	}
}

func statFinishFrame() {
	if statIdx < 0 {
		return
	}

	now := time.Now()
	timeBuf[statIdx].End = now
	statBuf[statIdx].FrameNS = uint32(now.Sub(timeBuf[statIdx].Start))
	statBuf[statIdx].Textures = uint32(len(statUsedTextures))

	for x := range statUsedTextures {
		delete(statUsedTextures, x)
	}
}

func StatRecordTriangleBatch(points int) {
	if statIdx >= 0 {
		statBuf[statIdx].Triangles += uint32(points / 3)
		statBuf[statIdx].Batches++
	}
}

func StatRecordTextureUse(tex *AssetTexture) {
	statUsedTextures[tex] = struct{}{}
}

func StatRecordBufferData(count int) {
	if statIdx >= 0 {
		statBuf[statIdx].BufferData += uint32(count)
	}
}

func logStats() {
	totalFrame := time.Duration(0)
	totalDelay := time.Duration(0)
	totalTris := uint32(0)
	totalBatches := uint32(0)
	totalTex := uint32(0)
	totalBuffer := uint32(0)
	count := float64(len(statBuf))

	for i := range statBuf {
		totalFrame += time.Duration(statBuf[i].FrameNS)
		totalDelay += time.Duration(statBuf[i].DelayNS)
		totalTris += statBuf[i].Triangles
		totalBatches += statBuf[i].Batches
		totalTex += statBuf[i].Textures
		totalBuffer += statBuf[i].BufferData
	}

	runtime.ReadMemStats(&memStats)

	w, h := Size()
	realW, realH := PixelSize()

	copy(perfLog[:], perfLog[1:])
	perfLog[len(perfLog)-1] = fmt.Sprintf("res %dx%d (%dx%d) avg time %v time2 %v fps %.2f batches %.2f tris %.2f tex %.2f buffer %.2f mem alloc %d objs %d/%d gc %d %.2f %v",
		w, h, realW, realH,
		(totalFrame / time.Duration(len(statBuf))).Truncate(time.Millisecond/100),
		((totalFrame + totalDelay) / time.Duration(len(statBuf))).Truncate(time.Millisecond/100),
		1/((totalFrame+totalDelay)/time.Duration(len(statBuf))).Seconds(),
		float64(totalBatches)/count,
		float64(totalTris)/count,
		float64(totalTex)/count,
		float64(totalBuffer)/count,
		memStats.Alloc,
		memStats.Mallocs-memStats.Frees,
		memStats.Mallocs,
		memStats.NumGC,
		memStats.GCCPUFraction*100,
		time.Duration(memStats.PauseTotalNs),
	)

	if router.FlagGfxPerf.IsSet() {
		log.Println("DEBUG: graphics performance:", perfLog[len(perfLog)-1])
	}
}
