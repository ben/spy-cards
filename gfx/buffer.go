//go:build !headless
// +build !headless

package gfx

import (
	"runtime"

	"golang.org/x/mobile/gl"
)

var buffers []*StaticBuffer

// StaticBuffer is a convenience layer for OpenGL vertex buffer objects.
type StaticBuffer struct {
	Name     string
	data     []uint8
	elements []uint8
	Data     gl.Buffer
	Element  gl.Buffer
	VAO      gl.VertexArray
	Count    int

	SetPointers    func()
	DisableAttribs func()
}

func (sb *StaticBuffer) init() {
	if sb.data == nil {
		panic("gfx: StaticBuffer has already dropped its memory")
	}

	_, haveVAO := GL.(gl.Context3)
	if haveVAO {
		sb.VAO = GL.CreateVertexArray()
		GL.BindVertexArray(sb.VAO)
	}

	sb.Data = GL.CreateBuffer()
	sb.Element = GL.CreateBuffer()

	GL.BindBuffer(gl.ARRAY_BUFFER, sb.Data)
	GL.BufferData(gl.ARRAY_BUFFER, sb.data, gl.STATIC_DRAW)

	GL.BindBuffer(gl.ELEMENT_ARRAY_BUFFER, sb.Element)
	GL.BufferData(gl.ELEMENT_ARRAY_BUFFER, sb.elements, gl.STATIC_DRAW)

	if haveVAO {
		sb.SetPointers()
	}

	if runtime.GOOS == "js" {
		// let it get garbage collected
		sb.data = nil
		sb.elements = nil
	}
}

func (sb *StaticBuffer) release() {
	if sb.VAO != (gl.VertexArray{}) {
		GL.DeleteVertexArray(sb.VAO)
	}

	GL.DeleteBuffer(sb.Data)
	GL.DeleteBuffer(sb.Element)

	sb.Data = gl.Buffer{}
	sb.Element = gl.Buffer{}
	sb.VAO = gl.VertexArray{}
}

// Delete frees the GPU memory held by StaticBuffer and removes it from the list of buffers.
func (sb *StaticBuffer) Delete() {
	Lock.Lock()
	defer Lock.Unlock()

	for i := range buffers {
		if buffers[i] == sb {
			buffers = append(buffers[:i], buffers[i+1:]...)

			if GL != nil {
				sb.release()
			}

			return
		}
	}
}

func (sb *StaticBuffer) Bind() {
	_, haveVAO := GL.(gl.Context3)
	if haveVAO {
		GL.BindVertexArray(sb.VAO)
	} else {
		GL.BindBuffer(gl.ARRAY_BUFFER, sb.Data)
		GL.BindBuffer(gl.ELEMENT_ARRAY_BUFFER, sb.Element)

		sb.SetPointers()
	}
}

func (sb *StaticBuffer) Unbind() {
	_, haveVAO := GL.(gl.Context3)
	if !haveVAO {
		sb.DisableAttribs()
	}
}

// NewStaticBuffer creates a StaticBuffer.
func NewStaticBuffer(name string, data, elements []uint8, elementSize int, setPointers, disableAttribs func()) *StaticBuffer {
	sb := &StaticBuffer{
		Name:     name,
		data:     make([]uint8, len(data)),
		elements: make([]uint8, len(elements)),
		Count:    len(elements) / elementSize,

		SetPointers:    setPointers,
		DisableAttribs: disableAttribs,
	}

	copy(sb.data, data)
	copy(sb.elements, elements)

	Lock.Lock()

	buffers = append(buffers, sb)

	if GL != nil {
		sb.init()
	}

	Lock.Unlock()

	return sb
}
