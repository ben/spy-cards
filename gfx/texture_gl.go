//go:build !headless
// +build !headless

package gfx

import (
	"log"
	"sync/atomic"
	"time"

	"golang.org/x/mobile/gl"
)

func onNewTexture(t *Texture) {
	Lock.Lock()

	textures = append(textures, t)

	if GL != nil {
		t.init()
	}

	Lock.Unlock()
}

// Release deletes a texture handle.
func (t *Texture) Release() {
	Lock.Lock()

	if GL != nil {
		t.release()
	}

	for i := range textures {
		if textures[i] == t {
			textures = append(textures[:i], textures[i+1:]...)

			break
		}
	}

	Lock.Unlock()
}

func isPowerOf2(i int) bool {
	return i&(i-1) == 0
}

func (t *Texture) defaultInit() {
	t.Texture = GL.CreateTexture()

	GL.BindTexture(gl.TEXTURE_2D, t.Texture)
	GL.TexImage2D(gl.TEXTURE_2D, 0, gl.RGBA, t.width, t.height, gl.RGBA, gl.UNSIGNED_BYTE, t.pix)

	if t.noClamp {
		GL.TexParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.REPEAT)
		GL.TexParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.REPEAT)
	} else {
		GL.TexParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE)
		GL.TexParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE)
	}

	switch {
	case t.nearest:
		GL.TexParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST)
		GL.TexParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST)
	case !isPowerOf2(t.width) || !isPowerOf2(t.height) || t.noMip:
		// can't use mipmaps on non-power-of-2 texture sizes
		GL.TexParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.LINEAR)
		GL.TexParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR)
	default:
		GL.TexParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.LINEAR)
		GL.TexParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR_MIPMAP_NEAREST)

		GL.GenerateMipmap(gl.TEXTURE_2D)
	}
}

func (t *Texture) release() {
	GL.DeleteTexture(t.Texture)

	t.Texture = gl.Texture{}
}

func (a *AssetTexture) doFetch() {
	var (
		tex *Texture
		err error
	)

	if a.embedded == nil {
		tex, err = fetchAssetTexture(a.Name, a.DownScale, a.nearest, a.noMip, a.NoClamp, true)
	} else {
		tex, err = newDataTexture(a.Name, a.embedded.data, a.embedded.mime, a.DownScale, a.nearest, a.noMip, a.NoClamp, a.embedded.premultiplyAlpha)
		if err == nil {
			a.embedded = nil
		}
	}

	if err != nil {
		log.Println("ERROR: loading texture", a.Name, err)

		time.Sleep(5 * time.Second) // to avoid re-requesting the texture too soon

		onFetchEnd.L.Lock()

		a.loadErr = err
		atomic.StoreInt32(&a.fetchState, fetchFailed)

		onFetchEnd.Broadcast()
		onFetchEnd.L.Unlock()

		return
	}

	onFetchEnd.L.Lock()

	a.tex = tex
	a.loadErr = nil
	atomic.StoreInt32(&a.fetchState, fetchComplete)

	if !a.firstWait.IsZero() {
		log.Println("WARNING: texture loading delayed", a.Name, "by", time.Since(a.firstWait))

		a.firstWait = time.Time{}
	}

	onFetchEnd.Broadcast()
	onFetchEnd.L.Unlock()
}
