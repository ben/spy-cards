//go:build !headless
// +build !headless

package gfx

import (
	"math"

	"git.lubar.me/ben/spy-cards/internal"
	"golang.org/x/mobile/event/size"
	"golang.org/x/mobile/geom"
	"golang.org/x/mobile/gl"
)

var sz size.Event

// Size returns the current screen size.
func Size() (w, h int) {
	return int(sz.WidthPt.Px(1)), int(sz.HeightPt.Px(1))
}

// PixelSize returns the screen size without compensation for pixel density.
func PixelSize() (w, h int) {
	return sz.WidthPx, sz.HeightPx
}

// SetSize processes a size.Event.
func SetSize(e size.Event) {
	if !internal.LoadSettings().NoStandardSize {
		aspect := float32(e.WidthPx) / float32(e.HeightPx)
		if aspect > 4.0/3.0 {
			e.PixelsPerPt = float32(e.HeightPx) / 720
			e.WidthPt = geom.Pt(720 * aspect)
			e.HeightPt = 720
		} else {
			e.PixelsPerPt = float32(e.WidthPx) / 640
			e.WidthPt = 640
			e.HeightPt = geom.Pt(640 / aspect)
		}
	}

	sz = e

	Lock.Lock()

	if GL != nil {
		GL.Viewport(0, 0, sz.WidthPx, sz.HeightPx)
		GL.Clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT)
	}

	Lock.Unlock()
}

func HorizontalFOV(angle float64) float32 {
	w, h := PixelSize()
	aspect := float64(h) / float64(w)

	return float32(2 * math.Atan(math.Tan(-angle*math.Pi/180/2)*aspect))
}
