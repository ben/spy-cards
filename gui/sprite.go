package gui

import (
	"log"

	"git.lubar.me/ben/spy-cards/gfx"
	"git.lubar.me/ben/spy-cards/gfx/sprites"
	"git.lubar.me/ben/spy-cards/input"
)

// Sprite is a (clickable) icon.
type Sprite struct {
	Sprite  *sprites.Sprite
	X, Y, Z float32
	Dy      float32
	Sx, Sy  float32
	FocusHandler
	OnSelect     func()
	Color        sprites.Color
	HoverColor   sprites.Color
	DefaultFocus bool
}

func (s *Sprite) Update(cam *gfx.Camera, ictx *input.Context) {
	if s.HasFocus() {
		switch {
		case ictx.Consume(input.BtnConfirm):
			if s.OnSelect != nil {
				s.OnSelect()
			}
		default:
			s.FocusHandler.input(ictx)
		}
	} else if s.DefaultFocus && s.InputFocus != nil && *s.InputFocus == nil {
		if ictx.Consume(input.BtnLeft) || ictx.Consume(input.BtnRight) || ictx.Consume(input.BtnUp) || ictx.Consume(input.BtnDown) {
			s.Focus()
		}
	}

	mx, my, click := ictx.Mouse()
	lastX, lastY, lastClick := ictx.LastMouse()

	if sprites.HitTest(cam, s.X+s.Sprite.X0*s.Sx, s.Y+s.Dy+s.Sprite.Y0*s.Sy, s.Z, s.X+s.Sprite.X1*s.Sx, s.Y+s.Dy+s.Sprite.Y1*s.Sy, s.Z, mx, my) {
		if mx != lastX || my != lastY {
			s.Focus()
		}

		if lastClick && !click && s.OnSelect != nil && !ictx.IsMouseDrag() {
			ictx.ConsumeClick()
			s.OnSelect()
		}
	} else if (mx != lastX || my != lastY) && s.HasFocus() {
		*s.InputFocus = nil
	}
}

func (s *Sprite) Render(b *sprites.Batch) {
	tint := s.Color
	if tint == (sprites.Color{}) {
		tint = sprites.White
	}

	if s.HasFocus() {
		if s.HoverColor != (sprites.Color{}) {
			tint = s.HoverColor
		} else {
			tint = sprites.Yellow
		}
	}

	defer func() {
		if r := recover(); r != nil {
			log.Println("WARNING:", r)
		}
	}()

	b.Append(s.Sprite, s.X, s.Y+s.Dy, s.Z, s.Sx, s.Sy, tint)
}

func (s *Sprite) SetY(y float32) {
	s.Dy = y
}

func (s *Sprite) Height() float32 {
	return (s.Sprite.Y1 - s.Sprite.Y0) * s.Sy
}

func (s *Sprite) Width() float32 {
	return (s.Sprite.X1 - s.Sprite.X0) * s.Sx
}
