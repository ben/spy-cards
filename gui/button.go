package gui

import (
	"strings"

	"git.lubar.me/ben/spy-cards/gfx"
	"git.lubar.me/ben/spy-cards/gfx/sprites"
	"git.lubar.me/ben/spy-cards/input"
)

// ButtonStyle is a render style for a Button control.
type ButtonStyle uint8

const (
	// BtnBorder is outlined text. When the button is hovered, the border
	// changes from black to rainbow.
	BtnBorder ButtonStyle = iota
	// BtnShadow is text with a drop shadow. When the button is hovered,
	// the text color changes to yellow.
	BtnShadow
	// BtnNone is plain text. When the button is hovered, the text color
	// changes to yellow.
	BtnNone
	// BtnTribe is outlined text with a card tribe indicator background.
	// Color changes the color of the background rather than the text.
	BtnTribe
)

// Button is a (clickable) label.
type Button struct {
	Label    string
	X, Y, Z  float32
	Dy       float32
	Sx, Sy   float32
	MaxWidth float32
	FocusHandler
	OnSelect     func()
	Color        sprites.Color
	Font         sprites.FontID
	Centered     bool
	DefaultFocus bool
	Style        ButtonStyle
}

func (b *Button) Update(cam *gfx.Camera, ictx *input.Context) {
	if b.InputFocus != nil && *b.InputFocus == &b.FocusHandler {
		switch {
		case ictx.Consume(input.BtnConfirm):
			if b.OnSelect != nil && b.Label != "" {
				b.OnSelect()
			}
		default:
			b.FocusHandler.input(ictx)
		}
	} else if b.DefaultFocus && b.InputFocus != nil && *b.InputFocus == nil {
		if ictx.Consume(input.BtnLeft) || ictx.Consume(input.BtnRight) || ictx.Consume(input.BtnUp) || ictx.Consume(input.BtnDown) {
			*b.InputFocus = &b.FocusHandler
		}
	}

	mx, my, click := ictx.Mouse()
	lastX, lastY, lastClick := ictx.LastMouse()

	anyOver := false

	if b.Label != "" {
		for i, line := range strings.Split(b.Label, "\n") {
			if sprites.TextHitTest(cam, b.Font, line, b.X, b.Y+b.Dy-0.7*float32(i), b.Z, b.Sx, b.Sy, mx, my, b.Centered) {
				anyOver = true

				if (mx != lastX || my != lastY) && b.InputFocus != nil {
					*b.InputFocus = &b.FocusHandler
				}

				if lastClick && !click && b.OnSelect != nil && !ictx.IsMouseDrag() {
					ictx.ConsumeClick()
					b.OnSelect()
				}
			}
		}
	}

	if (mx != lastX || my != lastY) && !anyOver && b.InputFocus != nil && *b.InputFocus == &b.FocusHandler {
		*b.InputFocus = nil
	}
}

func (b *Button) Render(tb *sprites.Batch) {
	textColor := b.Color
	if textColor == (sprites.Color{}) {
		textColor = sprites.White
	}

	if b.Style != BtnBorder && b.Style != BtnTribe && b.InputFocus != nil && *b.InputFocus == &b.FocusHandler {
		textColor = sprites.Yellow
	}

	if b.Style == BtnTribe {
		var dx float32

		width, height := b.MaxWidth, b.Height()

		if !b.Centered {
			dx = width / 2
		}

		tb.Append(sprites.TribeBubble, b.X+dx, b.Y+b.Dy-0.1*b.Sy+height/2, b.Z, width/1.6, height/0.3, textColor)
	}

	for i, line := range strings.Split(b.Label, "\n") {
		sx := b.Sx

		var width float32

		for _, l := range line {
			width += sprites.TextAdvance(b.Font, l, sx)
		}

		if b.MaxWidth != 0 && b.MaxWidth < width {
			sx *= b.MaxWidth / width
			width = b.MaxWidth
		}

		var dx float32

		if b.Centered {
			dx = width / -2
		}

		switch b.Style {
		case BtnBorder:
			borderColor := sprites.Black

			if b.InputFocus != nil && *b.InputFocus == &b.FocusHandler {
				borderColor = sprites.Rainbow
			}

			sprites.DrawTextBorder(tb, b.Font, line, b.X+dx, b.Y+b.Dy-0.7*b.Sy*float32(i), b.Z, sx, b.Sy, textColor, borderColor, false)
		case BtnShadow:
			sprites.DrawTextShadow(tb, b.Font, line, b.X+dx, b.Y+b.Dy-0.7*b.Sy*float32(i), b.Z, sx, b.Sy, textColor)
		case BtnNone:
			sprites.DrawText(tb, b.Font, line, b.X+dx, b.Y+b.Dy-0.7*b.Sy*float32(i), b.Z, sx, b.Sy, textColor)
		case BtnTribe:
			borderColor := sprites.Black

			if b.InputFocus != nil && *b.InputFocus == &b.FocusHandler {
				borderColor = sprites.Rainbow
			}

			sprites.DrawTextBorder(tb, b.Font, line, b.X+dx, b.Y+b.Dy-0.7*b.Sy*float32(i), b.Z, sx, b.Sy, sprites.White, borderColor, false)
		}
	}
}

func (b *Button) SetY(y float32) {
	b.Dy = y
}

func (b *Button) Width() float32 {
	if b.Style == BtnTribe {
		return b.MaxWidth
	}

	return b.TextWidth()
}

func (b *Button) Height() float32 {
	return 0.7 * b.Sy * float32(strings.Count(b.Label, "\n")+1)
}

func (b *Button) TextWidth() float32 {
	var max float32

	for _, line := range strings.Split(b.Label, "\n") {
		var width float32

		for _, letter := range line {
			width += sprites.TextAdvance(b.Font, letter, b.Sx)
		}

		if b.MaxWidth != 0 && b.MaxWidth < width {
			width = b.MaxWidth
		}

		if max < width {
			max = width
		}
	}

	return max
}
