package gui

import (
	"strconv"

	"git.lubar.me/ben/spy-cards/card"
	"git.lubar.me/ben/spy-cards/gfx"
	"git.lubar.me/ben/spy-cards/gfx/sprites"
	"git.lubar.me/ben/spy-cards/input"
)

type Number struct {
	Toggle   Toggle
	Prefix   string
	Suffix   string
	Value    int64
	Min      int64
	Max      int64
	Step     int64
	ValueInf bool
	PosInf   bool
	NegInf   bool
	OnChange func(i int64, inf bool)
}

func (n *Number) Update(cam *gfx.Camera, ictx *input.Context) {
	if len(n.Toggle.Options) != 3 {
		n.Toggle.Options = make([]ToggleOption, 3)
	}

	format := func(x int64) string {
		return n.Prefix + strconv.FormatInt(x, 10) + n.Suffix
	}

	if n.ValueInf {
		pi := n.Prefix + card.PosInf + n.Suffix
		ni := n.Prefix + card.NegInf + n.Suffix

		if !n.PosInf {
			pi = ni
		} else if !n.NegInf {
			ni = pi
		}

		n.Toggle.Options[0].Label = ni
		n.Toggle.Options[2].Label = pi

		if n.Value < 0 {
			n.Toggle.Options[1].Label = ni
		} else {
			n.Toggle.Options[1].Label = pi
		}
	} else {
		step := n.Step
		if step == 0 {
			step = 1
		}

		n.Toggle.Options[0].Label = format(n.Value - step)
		if n.Value-step <= n.Min || n.Value-step > n.Value {
			n.Toggle.Options[0].Label = format(n.Min)
		}

		n.Toggle.Options[1].Label = format(n.Value)

		n.Toggle.Options[2].Label = format(n.Value + step)
		if n.Value+step >= n.Max || n.Value+step < n.Value {
			n.Toggle.Options[2].Label = format(n.Max)
		}
	}

	n.Toggle.Button.Label = n.Toggle.Options[1].Label
	n.Toggle.Selected = 1

	n.Toggle.OnChange = func(_ int, to ToggleOption) {
		x := to.Label[:len(to.Label)-len(n.Suffix)][len(n.Prefix):]

		switch x {
		case card.PosInf:
			n.Value = 1
			n.ValueInf = true
		case card.NegInf:
			n.Value = -1
			n.ValueInf = true
		default:
			n.Value, _ = strconv.ParseInt(x, 10, 64)
			n.ValueInf = false
		}

		n.OnChange(n.Value, n.ValueInf)
	}

	n.Toggle.Update(cam, ictx)

	if n.PosInf || n.NegInf {
		var totalWidth float32

		for _, letter := range n.Toggle.Button.Label {
			if letter == '\n' {
				break
			}

			totalWidth += sprites.TextAdvance(n.Toggle.Button.Font, letter, n.Toggle.Button.Sx)
		}

		if n.Toggle.Button.MaxWidth != 0 && n.Toggle.Button.MaxWidth < totalWidth {
			totalWidth = n.Toggle.Button.MaxWidth
		}

		offset := totalWidth + 0.85*n.Toggle.Button.Sx

		mx, my, click := ictx.Mouse()
		lastX, lastY, lastClick := ictx.LastMouse()

		overInf := sprites.TextHitTest(cam, n.Toggle.Button.Font, card.PosInf, n.Toggle.Button.X+offset, n.Toggle.Button.Y+n.Toggle.Button.Dy, n.Toggle.Button.Z, 0.45*n.Toggle.Button.Sx, 0.45*n.Toggle.Button.Sy, mx, my, true)

		if n.Toggle.Button.InputFocus != nil && overInf && (mx != lastX || my != lastY) {
			*n.Toggle.Button.InputFocus = &n.Toggle.Button.FocusHandler
		}

		if n.Toggle.Button.InputFocus != nil && *n.Toggle.Button.InputFocus == &n.Toggle.Button.FocusHandler {
			toggleInf := false

			if ictx.Consume(input.BtnSwitch) {
				toggleInf = true
			} else if overInf && lastClick && !click && !ictx.IsMouseDrag() {
				ictx.ConsumeClick()

				toggleInf = true
			}

			if toggleInf {
				n.ValueInf = !n.ValueInf

				if n.ValueInf {
					if (n.Value >= 0 && n.PosInf) || !n.NegInf {
						n.Value = 1
					} else {
						n.Value = -1
					}
				}

				n.OnChange(n.Value, n.ValueInf)
			}
		}
	}
}

func (n *Number) Render(tb *sprites.Batch) {
	n.Toggle.Render(tb)

	if n.PosInf || n.NegInf {
		var totalWidth float32

		for _, letter := range n.Toggle.Button.Label {
			if letter == '\n' {
				break
			}

			totalWidth += sprites.TextAdvance(n.Toggle.Button.Font, letter, n.Toggle.Button.Sx)
		}

		if n.Toggle.Button.MaxWidth != 0 && n.Toggle.Button.MaxWidth < totalWidth {
			totalWidth = n.Toggle.Button.MaxWidth
		}

		offset := totalWidth + 0.85*n.Toggle.Button.Sx

		sprites.DrawLetter(tb, sprites.FontBubblegumSans, '∞', n.Toggle.Button.X+offset-sprites.TextAdvance(sprites.FontBubblegumSans, '∞', 0.45*n.Toggle.Button.Sx)/2, n.Toggle.Button.Y+n.Toggle.Button.Dy, n.Toggle.Button.Z, 0.45*n.Toggle.Button.Sx, 0.45*n.Toggle.Button.Sy, sprites.White, sprites.FlagBorder|sprites.FlagIsMTSDF, 0, 0, 0)
	}
}

func (n *Number) SetY(y float32)                 { n.Toggle.SetY(y) }
func (n *Number) Width() float32                 { return n.Toggle.Width() }
func (n *Number) Height() float32                { return n.Toggle.Height() }
func (n *Number) GetFocusHandler() *FocusHandler { return n.Toggle.GetFocusHandler() }
func (n *Number) Focus()                         { n.Toggle.Focus() }
func (n *Number) HasFocus() bool                 { return n.Toggle.HasFocus() }
