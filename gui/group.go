package gui

import (
	"git.lubar.me/ben/spy-cards/gfx"
	"git.lubar.me/ben/spy-cards/gfx/sprites"
	"git.lubar.me/ben/spy-cards/input"
)

type Horizontal struct {
	Controls []Control
}

func (h *Horizontal) Update(cam *gfx.Camera, ictx *input.Context) {
	for _, c := range h.Controls {
		c.Update(cam, ictx)
	}
}

func (h *Horizontal) Render(b *sprites.Batch) {
	for _, c := range h.Controls {
		c.Render(b)
	}
}

func (h *Horizontal) Width() float32 {
	var width float32

	for _, c := range h.Controls {
		width += c.Width()
	}

	return width
}

func (h *Horizontal) Height() float32 {
	var max float32

	for _, c := range h.Controls {
		if height := c.Height(); height > max {
			max = height
		}
	}

	return max
}

func (h *Horizontal) SetY(y float32) {
	for _, c := range h.Controls {
		c.SetY(y)
	}
}

func (h *Horizontal) HasFocus() bool {
	for _, c := range h.Controls {
		if c.HasFocus() {
			return true
		}
	}

	return false
}

type Vertical struct {
	Controls    []Control
	BaseY       float32
	Gap         float32
	AlwaysFocus bool
	setY        bool
}

func (v *Vertical) Update(cam *gfx.Camera, ictx *input.Context) {
	if !v.setY {
		v.SetY(0)
	}

	for _, c := range v.Controls {
		c.Update(cam, ictx)
	}
}

func (v *Vertical) Render(b *sprites.Batch) {
	for _, c := range v.Controls {
		c.Render(b)
	}
}

func (v *Vertical) Width() float32 {
	var max float32

	for _, c := range v.Controls {
		if w := c.Width(); max < w {
			max = w
		}
	}

	return max
}

func (v *Vertical) Height() float32 {
	height := v.Gap

	for _, c := range v.Controls {
		height += v.Gap
		height += c.Height()
	}

	return height
}

func (v *Vertical) SetY(y float32) {
	v.setY = true

	y += v.BaseY

	for _, c := range v.Controls {
		y -= v.Gap
		c.SetY(y)
		y -= c.Height()
	}
}

func (v *Vertical) HasFocus() bool {
	if v.AlwaysFocus {
		return true
	}

	for _, c := range v.Controls {
		if c.HasFocus() {
			return true
		}
	}

	return false
}
