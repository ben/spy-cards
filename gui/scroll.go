package gui

import (
	"errors"

	"git.lubar.me/ben/spy-cards/gfx"
	"git.lubar.me/ben/spy-cards/gfx/sprites"
	"git.lubar.me/ben/spy-cards/input"
)

type Scroll struct {
	Control    Control
	Scroll     float32
	Target     float32
	HasTarget  bool
	StealFocus bool
	Size       float32
	Screen     float32
	Margin     float32
	Wheel      float32
	Momentum   float32
}

func (s *Scroll) Update(cam *gfx.Camera, ictx *input.Context) {
	var held bool

	_, h := gfx.Size()

	if s.Control.HasFocus() || s.StealFocus {
		_, my, click := ictx.Mouse()
		_, lastY, lastClick := ictx.LastMouse()

		if click && lastClick {
			held = true

			amount := (lastY - my) * float32(h) / s.Screen / s.Size

			s.Scroll += amount
			s.Momentum += amount
		}

		if wheel := ictx.Wheel(); wheel != 0 {
			s.Wheel += wheel * 5 / s.Size
		}
	}

	if held {
		s.Momentum *= 0.9
	} else {
		s.Scroll += s.Momentum * 0.1
		s.Momentum *= 0.95
	}

	s.Scroll += s.Wheel * 0.1
	s.Wheel *= 0.9

	if s.HasTarget {
		s.Scroll = s.Scroll*0.9 + s.Target*0.1
	}

	if !held {
		if s.Scroll < 0 {
			s.Scroll *= 0.9
		}

		height := s.Control.Height()
		if bottom := height - float32(h)/s.Screen*(1-s.Margin)/s.Size; s.Scroll > bottom {
			s.Scroll = (s.Scroll-bottom)*0.9 + bottom
		}
	}

	s.Control.SetY(s.Scroll)
	s.Control.Update(cam, ictx)
}

func (s *Scroll) Render(b *sprites.Batch) {
	s.Control.Render(b)
}

func (s *Scroll) SetY(y float32) {
	if y != 0 {
		panic(errors.New("gui: cannot handle nested Scroll control"))
	}

	s.Control.SetY(s.Scroll)
}

func (s *Scroll) Height() float32 {
	_, h := gfx.Size()

	return float32(h) / s.Screen * (1 - s.Margin)
}

func (s *Scroll) HasFocus() bool {
	return s.Control.HasFocus()
}
