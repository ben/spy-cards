package gui

import (
	"time"

	"git.lubar.me/ben/spy-cards/gfx"
	"git.lubar.me/ben/spy-cards/gfx/sprites"
	"git.lubar.me/ben/spy-cards/input"
	"golang.org/x/mobile/event/key"
)

type Text struct {
	Text        []rune
	Placeholder string
	FocusHandler
	OnChange func(*[]rune)
	X, Y, Z  float32
	Sx, Sy   float32
	MaxWidth float32
	Dy       float32
	AutoGrow bool
	Font     sprites.FontID
	Rows     int
}

func (t *Text) Update(cam *gfx.Camera, ictx *input.Context) {
	if t.HasFocus() {
		switch {
		case ictx.Consume(input.BtnLeft):
			if t.InputFocusLeft != nil {
				t.InputFocusLeft()
			}
		case ictx.Consume(input.BtnRight):
			if t.InputFocusRight != nil {
				t.InputFocusRight()
			}
		case ictx.Consume(input.BtnUp):
			if t.InputFocusUp != nil {
				t.InputFocusUp()
			}
		case ictx.Consume(input.BtnDown):
			if t.InputFocusDown != nil {
				t.InputFocusDown()
			}
		}

		for {
			e, ok := ictx.ConsumeKey()
			if !ok {
				break
			}

			if e.Direction == key.DirRelease {
				continue
			}

			switch {
			case e.Rune >= 0 && (e.Rune != '\n' || t.Rows > 0):
				t.Text = append(t.Text, e.Rune)
			case e.Code == key.CodeReturnEnter && t.Rows > 0:
				t.Text = append(t.Text, '\n')
			case e.Code == key.CodeDeleteBackspace && len(t.Text) != 0:
				t.Text = t.Text[:len(t.Text)-1]
			default:
				continue
			}

			if t.OnChange != nil {
				t.OnChange(&t.Text)
			}
		}
	}

	mx, my, _ := ictx.Mouse()
	lastX, lastY, _ := ictx.LastMouse()

	rows := t.Rows
	if rows < 1 {
		rows = 1
	}

	over := sprites.HitTest(cam, t.X, t.Y+t.Dy+t.Sy*0.5-0.7*t.Sy*float32(rows), t.Z, t.X+t.MaxWidth, t.Y+t.Dy+t.Sy*0.7, t.Z, mx, my)

	if t.InputFocus != nil && (mx != lastX || my != lastY) {
		if over {
			t.Focus()
		} else if *t.InputFocus == &t.FocusHandler {
			*t.InputFocus = nil
		}
	}
}

func (t *Text) Render(b *sprites.Batch) {
	rows := float32(t.Lines())

	// vertical
	b.Append(sprites.Blank, t.X+t.MaxWidth/2, t.Y+t.Dy+0.25*t.Sy-0.7*(rows-1)/2*t.Sy, t.Z, t.MaxWidth-0.125, 0.7*t.Sy*rows+0.25, sprites.White)
	// horizontal
	b.Append(sprites.Blank, t.X+t.MaxWidth/2, t.Y+t.Dy+0.25*t.Sy-0.7*(rows-1)/2*t.Sy, t.Z, t.MaxWidth+0.25, 0.7*t.Sy*rows-0.125, sprites.White)
	// top left
	b.Append(sprites.WhiteBarCorner, t.X, t.Y+t.Dy+0.7*t.Sy-0.1*t.Sy, t.Z, 1, 1, sprites.White)
	// top right
	b.Append(sprites.WhiteBarCorner, t.X+t.MaxWidth, t.Y+t.Dy+0.7*t.Sy-0.1*t.Sy, t.Z, -1, 1, sprites.White)
	// bottom left
	b.Append(sprites.WhiteBarCorner, t.X, t.Y+t.Dy-0.1*t.Sy-(rows-1)*0.7*t.Sy, t.Z, 1, -1, sprites.White)
	// bottom right
	b.Append(sprites.WhiteBarCorner, t.X+t.MaxWidth, t.Y+t.Dy-0.1*t.Sy-(rows-1)*0.7*t.Sy, t.Z, -1, -1, sprites.White)

	x, y := float32(0), t.Y+t.Dy

	lines := t.splitLines()

	for i, line := range lines {
		x = t.X

		var width, lastNonSpace float32
		for _, letter := range line {
			width += sprites.TextAdvance(t.Font, letter, t.Sx)

			if letter != ' ' && letter != '\n' {
				lastNonSpace = width
			}
		}

		if !t.HasFocus() || i != len(lines)-1 {
			width = lastNonSpace
		}

		usePlaceholder := !t.HasFocus() && len(t.Text) == 0
		if usePlaceholder {
			for _, letter := range t.Placeholder {
				width += sprites.TextAdvance(t.Font, letter, t.Sx)
			}
		}

		sx := t.Sx
		if width > t.MaxWidth {
			sx *= t.MaxWidth / width
		}

		if usePlaceholder {
			sprites.DrawText(b, t.Font, t.Placeholder, x, y, t.Z, sx, t.Sy, sprites.Gray)

			break
		}

		for _, letter := range line {
			sprites.DrawLetter(b, t.Font, letter, x, y, t.Z, sx, t.Sy, sprites.Black, sprites.FlagIsMTSDF, 0, 0, 0)

			x += sprites.TextAdvance(t.Font, letter, sx)
		}

		y -= t.Sy * 0.7
	}

	if t.HasFocus() && time.Now().Second()&1 == 0 {
		b.Append(sprites.Blank, x+0.025, y+t.Sy*0.9, t.Z, 0.05, 0.5*t.Sy, sprites.Black)
	}
}

func (t *Text) Width() float32 {
	return t.MaxWidth
}

func (t *Text) Height() float32 {
	rows := t.Lines()

	return 0.7*t.Sy*float32(rows) + 0.125*t.Sy
}

func (t *Text) splitLines() [][]rune {
	if t.Rows <= 0 {
		return [][]rune{t.Text}
	}

	lines := make([][]rune, 0, t.Rows+1)

	remaining, sinceSpace := t.MaxWidth, float32(0)
	lastSpace, lastLine := -1, 0

	for i, letter := range t.Text {
		advance := sprites.TextAdvance(t.Font, letter, t.Sx)
		remaining -= advance

		if letter == ' ' {
			sinceSpace = 0
			lastSpace = i + 1
		} else {
			sinceSpace += advance
		}

		if letter == '\n' {
			lines = append(lines, t.Text[lastLine:i])
			lastSpace = i + 1
			lastLine = i + 1
			remaining = t.MaxWidth
			sinceSpace = 0
		} else if lastSpace != -1 && letter != ' ' && remaining < 0 {
			lines = append(lines, t.Text[lastLine:lastSpace])
			lastLine = lastSpace
			lastSpace = -1
			remaining = t.MaxWidth - sinceSpace
			sinceSpace = 0
		}
	}

	lines = append(lines, t.Text[lastLine:])

	return lines
}

func (t *Text) Lines() int {
	rows := 1

	if t.AutoGrow {
		rows = len(t.splitLines()) + 1
	}

	if rows < t.Rows {
		rows = t.Rows
	}

	return rows
}

func (t *Text) SetY(y float32) {
	t.Dy = y
}
