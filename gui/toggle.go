package gui

import (
	"math"

	"git.lubar.me/ben/spy-cards/audio"
	"git.lubar.me/ben/spy-cards/gfx"
	"git.lubar.me/ben/spy-cards/gfx/sprites"
	"git.lubar.me/ben/spy-cards/input"
)

type Toggle struct {
	Button     Button
	OnChange   func(int, ToggleOption)
	Options    []ToggleOption
	Selected   int
	ForceArrow bool
}

type ToggleOption struct {
	Label string
}

func (t *Toggle) Update(cam *gfx.Camera, ictx *input.Context) {
	if len(t.Options) == 0 {
		t.Button.Update(cam, ictx)

		return
	}

	t.Button.InputFocusLeft = func() { t.move(-1) }
	t.Button.InputFocusRight = func() { t.move(1) }

	t.Button.Update(cam, ictx)

	if t.Button.Label == "" {
		return
	}

	var totalWidth float32

	for _, letter := range t.Button.Label {
		if letter == '\n' {
			break
		}

		totalWidth += sprites.TextAdvance(t.Button.Font, letter, t.Button.Sx)
	}

	if t.Button.MaxWidth != 0 && t.Button.MaxWidth < totalWidth {
		totalWidth = t.Button.MaxWidth
	}

	left := -0.35 * t.Button.Sx
	right := totalWidth + 0.35*t.Button.Sx

	if t.Button.Centered {
		left = -(totalWidth + 0.7*t.Button.Sx) / 2
		right = -left
	}

	mx, my, click := ictx.Mouse()
	lastX, lastY, lastClick := ictx.LastMouse()

	overLeft := sprites.TextHitTest(cam, t.Button.Font, sprites.Button(input.BtnLeft), t.Button.X+left, t.Button.Y+t.Button.Dy+0.1, t.Button.Z, 0.65*t.Button.Sx, 0.65*t.Button.Sy, mx, my, true)
	overRight := sprites.TextHitTest(cam, t.Button.Font, sprites.Button(input.BtnRight), t.Button.X+right, t.Button.Y+t.Button.Dy+0.1, t.Button.Z, 0.65*t.Button.Sx, 0.65*t.Button.Sy, mx, my, true)

	if t.Button.InputFocus != nil && (overLeft || overRight) && (mx != lastX || my != lastY) {
		*t.Button.InputFocus = &t.Button.FocusHandler
	}

	if (overLeft || overRight) && t.Button.InputFocus != nil && *t.Button.InputFocus == &t.Button.FocusHandler && lastClick && !click && !ictx.IsMouseDrag() {
		ictx.ConsumeClick()

		if overLeft {
			t.move(-1)
		} else if overRight {
			t.move(1)
		}
	}
}

func (t *Toggle) Render(tb *sprites.Batch) {
	if len(t.Options) == 0 {
		t.Button.Render(tb)

		return
	}

	t.Button.Render(tb)

	if t.ForceArrow || (t.Button.InputFocus != nil && *t.Button.InputFocus == &t.Button.FocusHandler) {
		var totalWidth float32

		for _, letter := range t.Button.Label {
			if letter == '\n' {
				break
			}

			totalWidth += sprites.TextAdvance(t.Button.Font, letter, t.Button.Sx)
		}

		if t.Button.MaxWidth != 0 && t.Button.MaxWidth < totalWidth {
			totalWidth = t.Button.MaxWidth
		}

		left := -0.35 * t.Button.Sx
		right := totalWidth + 0.35*t.Button.Sx

		if t.Button.Centered {
			left = -(totalWidth + 0.7*t.Button.Sx) / 2
			right = -left
		}

		tb.AppendEx(sprites.CircleArrow, t.Button.X+left, t.Button.Y+t.Button.Dy+0.25, 0, 0.5*t.Button.Sx, 0.5*t.Button.Sy, sprites.White, 0, 0, 0, math.Pi/2)
		tb.AppendEx(sprites.CircleArrow, t.Button.X+right, t.Button.Y+t.Button.Dy+0.25, 0, 0.5*t.Button.Sx, 0.5*t.Button.Sy, sprites.White, 0, 0, 0, -math.Pi/2)
	}
}

func (t *Toggle) move(diff int) {
	audio.Confirm1.PlaySoundGlobal(0, 0, 0)

	t.Selected += diff

	if t.Selected < 0 {
		t.Selected = len(t.Options) - 1
	}

	if t.Selected >= len(t.Options) {
		t.Selected = 0
	}

	t.Button.Label = t.Options[t.Selected].Label

	if t.OnChange != nil {
		t.OnChange(t.Selected, t.Options[t.Selected])
	}
}

func (t *Toggle) SetY(y float32)                 { t.Button.SetY(y) }
func (t *Toggle) Width() float32                 { return t.Button.Width() }
func (t *Toggle) Height() float32                { return t.Button.Height() }
func (t *Toggle) GetFocusHandler() *FocusHandler { return t.Button.GetFocusHandler() }
func (t *Toggle) Focus()                         { t.Button.Focus() }
func (t *Toggle) HasFocus() bool                 { return t.Button.HasFocus() }
