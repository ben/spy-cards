package gui

import (
	"math"

	"git.lubar.me/ben/spy-cards/gfx"
	"git.lubar.me/ben/spy-cards/gfx/sprites"
	"git.lubar.me/ben/spy-cards/internal"
)

type HoverTilt struct {
	X, Y       float32
	X0, Y0     float32
	X1, Y1     float32
	Dx, Dy     float32
	Scale      float32
	OffX, OffY float32
	Falloff    float32
	Divisor    float32
	Mins, Maxs gfx.Vector
	Hover      uint8
}

func (h *HoverTilt) Update(cam *gfx.Camera, cursorX, cursorY float32) {
	var combined gfx.Matrix

	cam.Combined(&combined)

	var v gfx.Vector

	v.Multiply(&combined, &gfx.Vector{h.X + h.Scale*h.X0, h.Y + h.Scale*h.Y0, 0, 1})
	h.Mins.Multiply(&cam.Perspective, &v)

	v.Multiply(&combined, &gfx.Vector{h.X + h.Scale*h.X1, h.Y + h.Scale*h.Y1, 0, 1})
	h.Maxs.Multiply(&cam.Perspective, &v)

	h.Divisor = h.X1 - h.X0
	if y := h.Y1 - h.Y0; y > h.Divisor {
		h.Divisor = y
	}

	h.OffX = (cursorX*2 - 1 - (h.Mins[0]+h.Maxs[0])/2) / (h.Maxs[0] - h.Mins[0])
	h.OffY = (1 - cursorY*2 - (h.Mins[1]+h.Maxs[1])/2) / (h.Maxs[1] - h.Mins[1])
	h.Falloff = float32(math.Pow(
		1+math.Max(
			math.Abs(float64(h.OffX/(h.X1-h.X0))),
			math.Abs(float64(h.OffY/(h.Y1-h.Y0))),
		), 8,
	))

	if internal.PrefersReducedMotion() {
		h.OffX, h.OffY = 0, 0
	}
}

func (h *HoverTilt) Render(cam *gfx.Camera, render func(*gfx.Camera)) {
	var m gfx.Matrix

	m.FunHouse(h.X, h.Y, h.Scale, h.OffX, h.OffY, h.Falloff, h.Divisor, float32(h.Hover)/255)
	cam.PushTransform(&m)
	render(cam)
	cam.PopTransform()
}

func (h *HoverTilt) SetFunHouse(batch *sprites.Batch) {
	batch.SetFunHouse(&[8]float32{
		h.X,
		h.Y,
		h.Divisor,
		h.Falloff,
		h.OffX,
		h.OffY,
		h.Scale,
		float32(h.Hover) / 255,
	})
}
