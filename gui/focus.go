package gui

import (
	"git.lubar.me/ben/spy-cards/input"
)

type Focuser interface {
	GetFocusHandler() *FocusHandler
}

type FocusHandler struct {
	InputFocus      **FocusHandler
	InputFocusLeft  func()
	InputFocusRight func()
	InputFocusUp    func()
	InputFocusDown  func()
}

func (h *FocusHandler) GetFocusHandler() *FocusHandler {
	return h
}

func (h *FocusHandler) Focus() {
	if h.InputFocus != nil {
		*h.InputFocus = h
	}
}

func (h *FocusHandler) HasFocus() bool {
	return h.InputFocus != nil && *h.InputFocus == h
}

func (h *FocusHandler) input(ictx *input.Context) {
	switch {
	case ictx.ConsumeAllowRepeat(input.BtnLeft, 60, 5):
		if h.InputFocusLeft != nil {
			h.InputFocusLeft()
		}
	case ictx.ConsumeAllowRepeat(input.BtnRight, 60, 5):
		if h.InputFocusRight != nil {
			h.InputFocusRight()
		}
	case ictx.ConsumeAllowRepeat(input.BtnUp, 60, 5):
		if h.InputFocusUp != nil {
			h.InputFocusUp()
		}
	case ictx.ConsumeAllowRepeat(input.BtnDown, 60, 5):
		if h.InputFocusDown != nil {
			h.InputFocusDown()
		}
	}
}
