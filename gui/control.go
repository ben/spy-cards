// Package gui implements UI form elements.
package gui

import (
	"git.lubar.me/ben/spy-cards/gfx"
	"git.lubar.me/ben/spy-cards/gfx/sprites"
	"git.lubar.me/ben/spy-cards/input"
)

type Control interface {
	Update(cam *gfx.Camera, ictx *input.Context)
	Render(b *sprites.Batch)

	SetY(float32)
	Width() float32
	Height() float32
	HasFocus() bool
}
