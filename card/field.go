//go:generate stringer -type FieldType -trimprefix Field
//go:generate stringer -type BugFablesVersion -linecomment
//go:generate stringer -type SpecialFlag -linecomment

package card

import (
	"fmt"
	"reflect"
	"sort"

	"git.lubar.me/ben/spy-cards/format"
)

// FieldType is an enumeration of Spy Cards Online game mode field types.
type FieldType uint64

// Constants for FieldType.
const (
	FieldMetadata        FieldType = 0
	FieldBannedCards     FieldType = 1
	FieldGameRules       FieldType = 2
	FieldSummonCard      FieldType = 3
	FieldVariant         FieldType = 4
	FieldUnfilterCard    FieldType = 5
	FieldDeckLimitFilter FieldType = 6
	FieldTimer           FieldType = 7
	FieldTurn0Effect     FieldType = 8
	FieldVanillaVersion  FieldType = 9
	FieldGroup           FieldType = 10
	FieldSpecialFlags    FieldType = 11
)

// Field is an interface implemented by all custom game mode fields.
type Field interface {
	Type() FieldType
	Marshal(w *format.Writer) error
	Unmarshal(r *format.Reader, formatVersion uint64) error
	UpdateID(oldID, newID ID)
}

// NewField constructs a Field with a given FieldType.
func NewField(t FieldType) (Field, error) {
	switch t {
	case FieldMetadata:
		return &Metadata{
			Portrait: PortraitCustomEmbedded,
		}, nil
	case FieldBannedCards:
		return &BannedCards{}, nil
	case FieldGameRules:
		rules := &GameRules{}
		*rules = DefaultGameRules

		return rules, nil
	case FieldSummonCard:
		return &SummonCard{}, nil
	case FieldVariant:
		return &Variant{}, nil
	case FieldUnfilterCard:
		return &UnfilterCard{}, nil
	case FieldDeckLimitFilter:
		return &DeckLimitFilter{}, nil
	case FieldTimer:
		return &Timer{
			StartTime:  150,
			MaxTime:    300,
			PerTurn:    10,
			MaxPerTurn: 90,
		}, nil
	case FieldTurn0Effect:
		return &Turn0Effect{}, nil
	case FieldVanillaVersion:
		return &VanillaVersion{Version: LatestVersion}, nil
	case FieldGroup:
		return &Group{}, nil
	case FieldSpecialFlags:
		return &SpecialFlags{Set: make(map[SpecialFlag]bool)}, nil
	}

	return nil, fmt.Errorf("card: unknown field type %d (%v)", t, t)
}

// Metadata is a custom game mode field holding human-readable descriptions
// of the game mode.
type Metadata struct {
	Title          string
	Author         string
	Description    string
	LatestChanges  string
	Portrait       uint8
	CustomPortrait []byte
}

// Type implements Field.
func (f *Metadata) Type() FieldType { return FieldMetadata }

// Marshal implements Field.
func (f *Metadata) Marshal(w *format.Writer) error {
	w.String(f.Title)
	w.String(f.Author)
	w.String(f.Description)
	w.String(f.LatestChanges)

	if f.Portrait != PortraitCustomEmbedded || len(f.CustomPortrait) != 0 {
		w.Write(f.Portrait)

		if f.Portrait == PortraitCustomEmbedded || f.Portrait == PortraitCustomExternal {
			w.UVarInt(uint64(len(f.CustomPortrait)))
			w.Bytes(f.CustomPortrait)
		}
	}

	return nil
}

// Unmarshal implements Field.
func (f *Metadata) Unmarshal(r *format.Reader, formatVersion uint64) (err error) {
	defer format.Catch(&err)

	f.Title = r.String()
	f.Author = r.String()
	f.Description = r.String()
	f.LatestChanges = r.String()

	if r.Len() == 0 {
		f.Portrait = PortraitCustomEmbedded
		f.CustomPortrait = nil
	} else {
		f.Portrait = r.Byte()

		if f.Portrait == PortraitCustomEmbedded || f.Portrait == PortraitCustomExternal {
			f.CustomPortrait = r.Bytes(int(r.UVarInt()))
		} else {
			f.CustomPortrait = nil
		}
	}

	return nil
}

// UpdateID implements Field.
func (f *Metadata) UpdateID(oldID, newID ID) {}

// BannedCardsFlags is a bitfield of flags for BannedCards.
type BannedCardsFlags uint64

// Constants for BannedCardsFlags.
const (
	BannedCardTypeMask       BannedCardsFlags = 3 << 0
	BannedCardTypeBanned     BannedCardsFlags = 0 << 0
	BannedCardTypeUnpickable BannedCardsFlags = 1 << 0
	BannedCardTypeHiddenHome BannedCardsFlags = 2 << 0
)

// BannedCards is a custom game mode field representing cards that cannot
// be used in decks or summoned via random summons in this game mode.
//
// If Cards is empty, all vanilla cards (that is, cards with an ID less
// than 128) are banned.
type BannedCards struct {
	Flags BannedCardsFlags
	Cards []ID
}

// Type implements Field.
func (f *BannedCards) Type() FieldType { return FieldBannedCards }

// Marshal implements Field.
func (f *BannedCards) Marshal(w *format.Writer) error {
	w.Write(uint8(0))

	w.UVarInt(uint64(f.Flags))
	w.UVarInt(uint64(len(f.Cards)))

	for _, c := range f.Cards {
		w.UVarInt(uint64(c))
	}

	return nil
}

// Unmarshal implements Field.
func (f *BannedCards) Unmarshal(r *format.Reader, formatVersion uint64) (err error) {
	defer format.Catch(&err)

	if r.Len() > 1 && r.Peek(1)[0] == 0 {
		_ = r.Byte() // padding
		f.Flags = BannedCardsFlags(r.UVarInt())
	} else {
		f.Flags = 0
	}

	f.Cards = make([]ID, r.UVarInt())
	for i := range f.Cards {
		f.Cards[i] = ID(r.UVarInt())
	}

	return nil
}

// UpdateID implements Field.
func (f *BannedCards) UpdateID(oldID, newID ID) {
	for i := range f.Cards {
		if f.Cards[i] == oldID {
			f.Cards[i] = newID
		}
	}
}

// GameRules is a custom game mode field holding modified Spy Cards rules.
type GameRules struct {
	MaxHP             uint64
	HandMinSize       uint64
	HandMaxSize       uint64
	DrawPerRound      uint64
	CardsPerDeck      uint64
	MinTP             uint64
	MaxTP             uint64
	TPPerRound        uint64
	BossCards         uint64
	MiniBossCards     uint64
	DuplicateBoss     uint64
	DuplicateMiniBoss uint64
	DuplicateEffect   uint64
	DuplicateAttacker uint64
	CardsPerRound     uint64
}

// DefaultGameRules is the default game rules for Spy Cards Online.
var DefaultGameRules = GameRules{
	MaxHP:             5,
	HandMinSize:       3,
	HandMaxSize:       5,
	DrawPerRound:      2,
	CardsPerDeck:      15,
	MinTP:             2,
	MaxTP:             10,
	TPPerRound:        1,
	BossCards:         1,
	MiniBossCards:     2,
	DuplicateBoss:     1,
	DuplicateMiniBoss: 1,
	DuplicateEffect:   0,
	DuplicateAttacker: 0,
	CardsPerRound:     0,
}

// Type implements Field.
func (f *GameRules) Type() FieldType { return FieldGameRules }

// Marshal implements Field.
func (f *GameRules) Marshal(w *format.Writer) error {
	v := reflect.ValueOf(f).Elem()
	defaults := reflect.ValueOf(DefaultGameRules)

	for i, l := 0, v.NumField(); i < l; i++ {
		field := v.Field(i)
		if field.Interface() == defaults.Field(i).Interface() {
			continue
		}

		w.UVarInt(uint64(i + 1))

		switch kind := field.Kind(); kind {
		case reflect.Uint64:
			w.UVarInt(field.Uint())
		case reflect.Int64:
			w.SVarInt(field.Int())
		default:
			panic(fmt.Errorf("card: unexpected Kind for GameRules field: %v", kind))
		}
	}

	w.UVarInt(0)

	return nil
}

// Unmarshal implements Field.
func (f *GameRules) Unmarshal(r *format.Reader, formatVersion uint64) (err error) {
	defer format.Catch(&err)

	*f = DefaultGameRules
	v := reflect.ValueOf(f).Elem()

	for {
		t := r.UVarInt()
		if t == 0 {
			break
		}

		if uint64(v.NumField()) < t {
			return fmt.Errorf("card: unknown game rule ID %d", t)
		}

		field := v.Field(int(t) - 1)

		switch kind := field.Kind(); kind {
		case reflect.Uint64:
			field.SetUint(r.UVarInt())
		case reflect.Int64:
			field.SetInt(r.SVarInt())
		default:
			panic(fmt.Errorf("card: unexpected Kind for GameRules field: %v", kind))
		}
	}

	if f.HandMaxSize > 50 {
		f.HandMaxSize = 50
	}

	return nil
}

// UpdateID implements Field.
func (f *GameRules) UpdateID(oldID, newID ID) {}

// SummonCardFlags is a bitfield of flags for SummonCards.
type SummonCardFlags uint64

// Constants for SummonCardFlags.
const (
	SummonCardBothPlayers SummonCardFlags = 1 << 0
)

// SummonCard is a custom game mode field that summons a card at the start
// of the match.
type SummonCard struct {
	Flags SummonCardFlags
	ID    ID
}

// Type implements Field.
func (f *SummonCard) Type() FieldType { return FieldSummonCard }

// Marshal implements Field.
func (f *SummonCard) Marshal(w *format.Writer) error {
	w.UVarInt(uint64(f.Flags))
	w.UVarInt(uint64(f.ID))

	return nil
}

// Unmarshal implements Field.
func (f *SummonCard) Unmarshal(r *format.Reader, formatVersion uint64) (err error) {
	defer format.Catch(&err)

	f.Flags = SummonCardFlags(r.UVarInt())
	f.ID = ID(r.UVarInt())

	return nil
}

// UpdateID implements Field.
func (f *SummonCard) UpdateID(oldID, newID ID) {
	if f.ID == oldID {
		f.ID = newID
	}
}

// Variant is a custom game mode field representing a sub-mode.
//
// Variants can be selected when starting a match, and append their rules
// to the game mode's set of fields.
//
// If at least one Variant is present, one variant is always applied.
type Variant struct {
	Title string
	NPC   string
	Rules []Field
}

// Type implements Field.
func (f *Variant) Type() FieldType { return FieldVariant }

// Marshal implements Field.
func (f *Variant) Marshal(w *format.Writer) error {
	w.String(f.Title)
	w.String(f.NPC)

	validFields := make([]Field, 0, len(f.Rules))

	for _, field := range f.Rules {
		if f, ok := field.(*Turn0Effect); ok && !f.Effect.Complete() {
			continue
		}

		validFields = append(validFields, field)
	}

	w.UVarInt(uint64(len(validFields)))

	for i, r := range validFields {
		w.UVarInt(uint64(r.Type()))
		sub, done := w.SubWriter()

		if err := r.Marshal(sub); err != nil {
			return fmt.Errorf("encoding variant field %d: %w", i, err)
		}

		done()
	}

	return nil
}

// Unmarshal implements Field.
func (f *Variant) Unmarshal(r *format.Reader, formatVersion uint64) (err error) {
	defer format.Catch(&err)

	f.Title = r.String()
	f.NPC = r.String()
	f.Rules = make([]Field, r.UVarInt())

	for i := range f.Rules {
		f.Rules[i], err = unmarshalField(r, formatVersion)
		if err != nil {
			return fmt.Errorf("parsing variant field %d: %w", i, err)
		}
	}

	return nil
}

// UpdateID implements Field.
func (f *Variant) UpdateID(oldID, newID ID) {
	for _, r := range f.Rules {
		r.UpdateID(oldID, newID)
	}
}

// UnfilterCardFlags is a bitfield of flags for UnfilterCards.
type UnfilterCardFlags uint64

// UnfilterCard is a custom game mode field that summons a card at the start
// of the match.
type UnfilterCard struct {
	Flags UnfilterCardFlags
	ID    ID
}

// Type implements Field.
func (f *UnfilterCard) Type() FieldType { return FieldUnfilterCard }

// Marshal implements Field.
func (f *UnfilterCard) Marshal(w *format.Writer) error {
	w.UVarInt(uint64(f.Flags))
	w.UVarInt(uint64(f.ID))

	return nil
}

// Unmarshal implements Field.
func (f *UnfilterCard) Unmarshal(r *format.Reader, formatVersion uint64) (err error) {
	defer format.Catch(&err)

	f.Flags = UnfilterCardFlags(r.UVarInt())
	f.ID = ID(r.UVarInt())

	return nil
}

// UpdateID implements Field.
func (f *UnfilterCard) UpdateID(oldID, newID ID) {
	if f.ID == oldID {
		f.ID = newID
	}
}

// DeckLimitFilter limits the number of cards that may appear in a deck based on a filter.
type DeckLimitFilter struct {
	Count     uint64
	Filter    Filter
	CondCount uint64
	Condition Filter
}

// Type implements Field.
func (f *DeckLimitFilter) Type() FieldType { return FieldDeckLimitFilter }

// Marshal implements Field.
func (f *DeckLimitFilter) Marshal(w *format.Writer) error {
	w.UVarInt(f.Count)

	w.Write(uint8(128))

	if err := f.Filter.Marshal(w); err != nil {
		return err
	}

	if f.CondCount != 0 {
		w.UVarInt(f.CondCount)

		if err := f.Condition.Marshal(w); err != nil {
			return err
		}
	}

	return nil
}

// Unmarshal implements Field.
func (f *DeckLimitFilter) Unmarshal(r *format.Reader, formatVersion uint64) (err error) {
	defer format.Catch(&err)

	f.Count = r.UVarInt()

	var rankTribe uint8

	r.Read(&rankTribe)

	if rankTribe == 128 {
		if err := f.Filter.Unmarshal(r); err != nil {
			return err
		}

		if r.Len() == 0 {
			f.CondCount = 0
			f.Condition = nil

			return nil
		}

		f.CondCount = r.UVarInt()

		return f.Condition.Unmarshal(r)
	}

	rank := Rank(rankTribe >> 4)
	if rank == Token {
		rank = Enemy
	}

	tribe, customTribe := Tribe(rankTribe&15), ""

	if tribe == TribeCustom {
		customTribe = r.String()
	}

	switch {
	case rank == RankNone && tribe == TribeNone:
		f.Filter = Filter{
			{
				Type:   FilterSingleCard,
				CardID: ID(r.UVarInt()),
			},
		}
	case tribe == TribeNone && rank == Enemy:
		f.Filter = Filter{
			{
				Type: FilterRank,
				Rank: Attacker,
			},
			{
				Type: FilterRank,
				Rank: Effect,
			},
		}
	case tribe == TribeNone:
		f.Filter = Filter{
			{
				Type: FilterRank,
				Rank: rank,
			},
		}
	case rank == RankNone:
		f.Filter = Filter{
			{
				Type:        FilterTribe,
				Tribe:       tribe,
				CustomTribe: customTribe,
			},
		}
	case rank == Enemy:
		f.Filter = Filter{
			{
				Type: FilterRank,
				Rank: Attacker,
			},
			{
				Type: FilterRank,
				Rank: Effect,
			},
			{
				Type:        FilterTribe,
				Tribe:       tribe,
				CustomTribe: customTribe,
			},
		}
	default:
		f.Filter = Filter{
			{
				Type: FilterRank,
				Rank: rank,
			},
			{
				Type:        FilterTribe,
				Tribe:       tribe,
				CustomTribe: customTribe,
			},
		}
	}

	return nil
}

// UpdateID implements Field.
func (f *DeckLimitFilter) UpdateID(oldID, newID ID) {
	if f.Filter.IsSingleCard() && f.Filter[0].CardID == oldID {
		f.Filter[0].CardID = newID
	}
}

// Timer limits the amount of time players may spend per turn.
type Timer struct {
	StartTime  uint64
	MaxTime    uint64
	PerTurn    uint64
	MaxPerTurn uint64
}

// Type implements Field.
func (f *Timer) Type() FieldType { return FieldTimer }

// Marshal implements Field.
func (f *Timer) Marshal(w *format.Writer) error {
	w.UVarInt(f.StartTime)
	w.UVarInt(f.MaxTime)
	w.UVarInt(f.PerTurn)
	w.UVarInt(f.MaxPerTurn)

	return nil
}

// Unmarshal implements Field.
func (f *Timer) Unmarshal(r *format.Reader, formatVersion uint64) (err error) {
	defer format.Catch(&err)

	f.StartTime = r.UVarInt()
	f.MaxTime = r.UVarInt()
	f.PerTurn = r.UVarInt()
	f.MaxPerTurn = r.UVarInt()

	return nil
}

// UpdateID implements Field.
func (f *Timer) UpdateID(oldID, newID ID) {}

// Turn0Effect applies an effect before the first round of play.
type Turn0Effect struct {
	Flags  uint64
	Effect *EffectDef
}

// Type implements Field.
func (f *Turn0Effect) Type() FieldType { return FieldTurn0Effect }

// Marshal implements Field.
func (f *Turn0Effect) Marshal(w *format.Writer) error {
	w.UVarInt(f.Flags)
	w.UVarInt(5)

	return f.Effect.Marshal(w)
}

// Unmarshal implements Field.
func (f *Turn0Effect) Unmarshal(r *format.Reader, formatVersion uint64) (err error) {
	defer format.Catch(&err)

	f.Flags = r.UVarInt()

	f.Effect = &EffectDef{}

	switch effectFormat := r.UVarInt(); effectFormat {
	case 0, 1:
		return f.Effect.unmarshalV1(r, effectFormat)
	case 2, 4:
		return f.Effect.unmarshalV4(r, effectFormat)
	case 5:
		return f.Effect.Unmarshal(r, effectFormat)
	default:
		return fmt.Errorf("card: unhandled Turn0Effect format version: %d", effectFormat)
	}
}

// UpdateID implements Field.
func (f *Turn0Effect) UpdateID(oldID, newID ID) {
	f.Effect.UpdateID(oldID, newID)
}

// BugFablesVersion is a version of Bug Fables: The Everlasting Sapling.
type BugFablesVersion uint8

// Constants for BugFablesVersion.
const (
	BugFables105 BugFablesVersion = iota // Bug Fables 1.0.5
	BugFables11                          // Bug Fables 1.1
	BugFables111                         // Bug Fables 1.1.1
	BugFables121                         // Bug Fables 1.2.1

	LatestVersion = BugFables121
)

// VanillaVersion overrides the vanilla Spy Cards card set.
type VanillaVersion struct {
	Version BugFablesVersion
}

// Type implements Field.
func (f *VanillaVersion) Type() FieldType { return FieldVanillaVersion }

// Marshal implements Field.
func (f *VanillaVersion) Marshal(w *format.Writer) error {
	w.UVarInt(uint64(f.Version))

	return nil
}

// Unmarshal implements Field.
func (f *VanillaVersion) Unmarshal(r *format.Reader, formatVersion uint64) (err error) {
	defer format.Catch(&err)

	f.Version = BugFablesVersion(r.UVarInt())

	return nil
}

// UpdateID implements Field.
func (f *VanillaVersion) UpdateID(oldID, newID ID) {
	// no card IDs
}

type GroupFlags uint64

type Group struct {
	Flags GroupFlags
	Title string
	Cards []ID
}

func (f *Group) Type() FieldType { return FieldGroup }

func (f *Group) Marshal(w *format.Writer) error {
	w.UVarInt(uint64(f.Flags))
	w.String(f.Title)
	w.UVarInt(uint64(len(f.Cards)))

	for _, id := range f.Cards {
		w.UVarInt(uint64(id))
	}

	return nil
}

func (f *Group) Unmarshal(r *format.Reader, formatVersion uint64) error {
	f.Flags = GroupFlags(r.UVarInt())
	f.Title = r.String()
	f.Cards = make([]ID, r.UVarInt())

	for i := range f.Cards {
		f.Cards[i] = ID(r.UVarInt())
	}

	return nil
}

func (f *Group) UpdateID(oldID, newID ID) {
	for i, id := range f.Cards {
		if id == oldID {
			f.Cards[i] = newID
		}
	}
}

type SpecialFlagsFlags uint64

type SpecialFlag uint64

// Values for SpecialFlag.
const (
	_                       SpecialFlag = iota // first flag is reserved
	SpecialSubtractInf                         // Allow Subtracting Infinities
	SpecialEffectBack                          // Use Effect Card Back
	SpecialNoDamageOnWin                       // No Damage on Win
	SpecialRevealSelection                     // Reveal Card Selection
	SpecialBothPlayersAreP1                    // Both Players are Player 1
	SpecialAutoReady                           // Auto-Ready if Negative TP
	SpecialHideGameLog                         // Hide Game Log
	SpecialFlagCount                           // (not a special flag; denotes end of special flags)
)

type SpecialFlags struct {
	Flags SpecialFlagsFlags
	Set   map[SpecialFlag]bool
}

func (f *SpecialFlags) Type() FieldType { return FieldSpecialFlags }

func (f *SpecialFlags) Marshal(w *format.Writer) error {
	w.UVarInt(uint64(f.Flags))

	var flags []SpecialFlag

	for x, set := range f.Set {
		if set {
			flags = append(flags, x)
		}
	}

	sort.Slice(flags, func(i, j int) bool {
		return flags[i] < flags[j]
	})

	w.UVarInt(uint64(len(flags)))

	for _, x := range flags {
		w.UVarInt(uint64(x))
	}

	return nil
}

func (f *SpecialFlags) Unmarshal(r *format.Reader, formatVersion uint64) error {
	f.Flags = SpecialFlagsFlags(r.UVarInt())

	count := r.UVarInt()
	f.Set = make(map[SpecialFlag]bool, count)

	for i := uint64(0); i < count; i++ {
		f.Set[SpecialFlag(r.UVarInt())] = true
	}

	return nil
}

func (f *SpecialFlags) UpdateID(oldID, newID ID) {
	// no card IDs
}
