package card

import (
	"context"
	"encoding/base64"
	"html"
	"strconv"
	"strings"

	"git.lubar.me/ben/spy-cards/format"
	"git.lubar.me/ben/spy-cards/gfx/sprites"
	"git.lubar.me/ben/spy-cards/internal"
)

var infWrap = strings.NewReplacer(PosInf, `<span class="infinity-symbol">`+PosInf+`</span>`)

func squishX(font sprites.FontID, text string, space, min float32) string {
	width := float32(0)

	space /= 0.72

	for _, letter := range text {
		if letter == ' ' || letter == '\u00A0' {
			// spaces have a different width in browsers than ingame
			switch font {
			case sprites.FontBubblegumSans:
				width += 0.2 * space
			case sprites.FontD3Streetism:
				width += 0.5 * space
			}

			continue
		}

		width += sprites.TextAdvance(font, letter, space)
	}

	if width > 1/min {
		return `<span class="squish" style="transform: scaleX(` + strconv.FormatFloat(float64(1/width), 'f', 6, 32) + `);">`
	}

	return `<span class="squish">`
}

func squishY(font sprites.FontID, text string, space, vspace, min float32) string {
	width, height := float32(0), vspace

	space /= 0.72

	for _, letter := range text {
		if letter == '\n' || width >= 1 {
			width = 0
			height += vspace

			continue
		}

		if letter == ' ' || letter == '\u00A0' {
			// spaces have a different width in browsers than ingame
			switch font {
			case sprites.FontBubblegumSans:
				width += 0.2 * space
			case sprites.FontD3Streetism:
				width += 0.5 * space
			}

			continue
		}

		width += sprites.TextAdvance(font, letter, space)
	}

	if height > 1/min {
		return ` style="transform: scaleY(` + strconv.FormatFloat(float64(1/height), 'f', 6, 32) + `);">`
	}

	return `>`
}

func cardToHTML(ctx context.Context, cd *Def, rank Rank, set *Set) string {
	const hex = "0123456789abcdef"

	effectBack := false

	for _, f := range set.Mode.GetAll(FieldSpecialFlags) {
		if f.(*SpecialFlags).Set[SpecialEffectBack] {
			effectBack = true

			break
		}
	}

	back := rank.Back(effectBack)

	banned, unpickable := false, false

	if cd != nil {
		for _, f := range set.Mode.GetAll(FieldBannedCards) {
			bc := f.(*BannedCards)

			found := len(bc.Cards) == 0 && cd.ID < 128

			for _, id := range bc.Cards {
				if cd.ID == id {
					found = true

					break
				}
			}

			if !found {
				continue
			}

			switch bc.Flags & BannedCardTypeMask {
			case BannedCardTypeBanned:
				banned = true
			case BannedCardTypeUnpickable:
				unpickable = true
			}
		}
	}

	hideTP := (banned && unpickable) || (cd != nil && cd.Rank == Token && cd.TP == 0)
	isStackingToken := cd != nil && cd.Rank == Token && cd.TP == 0 && !banned

	var visibleTribes []TribeDef

	if cd != nil {
		visibleTribes = make([]TribeDef, 0, len(cd.Tribes))

		for _, t := range cd.Tribes {
			if t.Tribe != TribeCustom || !strings.HasPrefix(t.CustomName, "_") {
				visibleTribes = append(visibleTribes, t)
			}
		}
	}

	isSimple := false

	switch {
	case cd == nil:
		// can't be simple without a face
	case len(cd.Effects) == 1:
		isSimple = cd.Effects[0] != nil && cd.Effects[0].Type == EffectStat
	case len(cd.Effects) == 2:
		isSimple = cd.Effects[0] != nil && cd.Effects[1] != nil &&
			cd.Effects[0].Type == EffectStat && cd.Effects[1].Type == EffectStat &&
			cd.Effects[0].Flags&FlagStatDEF == 0 && cd.Effects[1].Flags&FlagStatDEF != 0
	}

	b := make([]byte, 0, 2048)

	b = append(b, `<div class="card card-type-`...)
	b = append(b, strings.ToLower(back.String())...)

	if cd != nil && cd.Rank != back {
		b = append(b, ` card-type-`...)
		b = append(b, strings.ToLower(cd.Rank.String())...)
	}

	switch {
	case isStackingToken:
		b = append(b, ` card-stacking`...)
	case hideTP:
		b = append(b, ` no-tp`...)
	}

	b = append(b, `" role="img" aria-label="`...)

	if cd != nil {
		b = append(b, cd.Rank.String()...)
		b = append(b, ` card: `...)
		b = append(b, html.EscapeString(cd.DisplayName())...)
	} else {
		b = append(b, `Unknown `...)
		b = append(b, back.String()...)
		b = append(b, ` card`...)
	}

	b = append(b, `">`...)

	if cd != nil {
		nameSpace := float32(0.2)

		if isStackingToken {
			nameSpace = 0.225
		} else if hideTP {
			nameSpace = 0.16875
		}

		b = append(b, `<div class="front"><span class="card-name">`...)
		b = append(b, squishX(sprites.FontBubblegumSans, cd.DisplayName(), nameSpace, 1)...)
		b = append(b, html.EscapeString(cd.DisplayName())...)
		b = append(b, `</span></span>`...)

		switch {
		case isStackingToken:
			b = append(b, `<span class="card-stack-counter"><span class="squish">x1</span></span>`...)
		case hideTP:
			// no TP indicator
		default:
			b = append(b, `<span class="card-tp"`...)

			if cd.TP < 0 {
				b = append(b, ` data-negative`...)
			}

			b = append(b, `>`...)
			b = append(b, squishX(sprites.FontD3Streetism, strconv.FormatInt(cd.TP, 10), 7.0/6.0, 0.8)...)
			b = strconv.AppendInt(b, cd.TP, 10)
			b = append(b, `</span></span>`...)
		}

		b = append(b, `<div class="portrait`...)

		switch cd.Portrait {
		case PortraitCustomEmbedded:
			b = append(b, ` embedded`...)
		case PortraitCustomExternal:
			b = append(b, ` external`...)
		default:
			b = append(b, ` builtin`...)
		}

		b = append(b, `" data-x="`...)
		b = strconv.AppendUint(b, uint64(cd.Portrait&15), 10)
		b = append(b, `" data-y="`...)
		b = strconv.AppendUint(b, uint64(cd.Portrait>>4), 10)
		b = append(b, `">`...)

		switch cd.Portrait {
		case PortraitCustomEmbedded:
			b = append(b, `<img src="data:image/png;base64,`...)
			b = append(b, base64.StdEncoding.EncodeToString(cd.CustomPortrait)...)
			b = append(b, `" alt="" crossorigin="anonymous" loading="lazy">`...)
		case PortraitCustomExternal:
			b = append(b, `<img src="`...)
			b = append(b, internal.GetConfig(ctx).UserImageBaseURL...)
			b = append(b, format.Encode32(cd.CustomPortrait)...)
			b = append(b, `.webp" alt="" crossorigin="anonymous" loading="lazy">`...)
		}

		b = append(b, `</div><div class="card-desc-wrapper"><ul class="card-desc `...)

		if isSimple {
			b = append(b, `card-simple-desc">`...)

			for _, e := range cd.Effects {
				b = append(b, `<li class="card-`...)

				if e.Flags&FlagStatDEF == 0 {
					b = append(b, `atk`...)
				} else {
					b = append(b, `def`...)
				}

				b = append(b, `"`...)

				if e.Amount < 0 {
					b = append(b, ` data-negative`...)
				}

				b = append(b, ` title="`...)
				b = append(b, html.EscapeString((&RichDescription{
					Content: e.Description(cd, set),
				}).String())...)
				b = append(b, `">`...)
				b = append(b, squishX(sprites.FontD3Streetism, e.describeAmount(FlagStatInfinity), 0.6*float32(len(cd.Effects)), 1)...)
				b = append(b, infWrap.Replace(e.describeAmount(FlagStatInfinity))...)
				b = append(b, `</span></li>`...)
			}
		} else {
			b = append(b, `squishy"`...)
			b = append(b, squishY(sprites.FontBubblegumSans, cd.Description(set).String(), 32.0/283.0, 32.0/80.0*0.7, 1.2)...)

			for _, e := range cd.Effects {
				b = append(b, `<li class="card-effect`...)

				switch {
				case !e.Complete():
					b = append(b, ` card-null`...)
				case e.Type == FlavorText:
					b = append(b, ` card-flavor-text`...)

					if e.Flags&FlagFlavorTextHideRemaining != 0 {
						b = append(b, ` hide-remaining-effects`...)
					}

					if e.Flags&FlagFlavorTextCustomColor != 0 {
						b = append(b, `" style="color:#`...)
						b = append(b, hex[(e.Amount>>4)&15], hex[e.Amount&15], hex[(e.Amount>>12)&15], hex[(e.Amount>>8)&15], hex[(e.Amount>>20)&15], hex[(e.Amount>>16)&15], ';')
					}
				}

				b = append(b, `">`...)
				b = append(b, infWrap.Replace(html.EscapeString((&RichDescription{
					Content: e.Description(cd, set),
				}).String()))...)
				b = append(b, `</li>`...)
			}
		}

		tribeSpace := float32(len(visibleTribes))
		if len(visibleTribes) == 0 || (len(visibleTribes) == 1 && visibleTribes[0].Tribe != TribeDeadLander && visibleTribes[0].Tribe != TribeWideCustom) {
			tribeSpace = 2
		}

		b = append(b, `</ul></div><ul class="card-tribe-placeholders" style="--numtribes:`...)
		b = strconv.AppendUint(b, uint64(tribeSpace), 10)
		b = append(b, `;">`...)

		tribeSpace = 36 / (335/tribeSpace - 22)

		for _, t := range visibleTribes {
			b = append(b, `<li class="card-tribe-placeholder" data-tribe="`...)
			b = append(b, html.EscapeString(t.Name())...)
			b = append(b, `" title="Tribe: `...)
			b = append(b, html.EscapeString(t.Name())...)

			if t.Tribe == TribeCustom || t.Tribe == TribeWideCustom {
				b = append(b, `" style="--custom-color:#`...)
				b = append(b, hex[t.Red>>4], hex[t.Red&15], hex[t.Green>>4], hex[t.Green&15], hex[t.Blue>>4], hex[t.Blue&15], ';')
			}

			b = append(b, `">`...)
			b = append(b, squishX(sprites.FontD3Streetism, t.Name(), tribeSpace, 1)...)
			b = append(b, html.EscapeString(t.Name())...)
			b = append(b, `</span></li>`...)
		}

		b = append(b, `</ul></div>`...)
	}

	b = append(b, `<div class="back"></div></div>`...)

	return string(b)
}

func (cd *Def) ToHTML(ctx context.Context, set *Set) string {
	return cardToHTML(ctx, cd, cd.Rank, set)
}

func (r Rank) ToHTML(ctx context.Context, set *Set) string {
	return cardToHTML(ctx, nil, r, set)
}
