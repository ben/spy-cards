//go:generate stringer -type Tribe -linecomment

package card

import (
	"git.lubar.me/ben/spy-cards/gfx/sprites"
)

// Tribe is a 4-bit identifier for a Bug Fables Spy Cards tribe.
type Tribe uint8

// Constants for Tribe.
const (
	TribeSeedling   Tribe = 0  // Seedling
	TribeWasp       Tribe = 1  // Wasp
	TribeFungi      Tribe = 2  // Fungi
	TribeZombie     Tribe = 3  // Zombie
	TribePlant      Tribe = 4  // Plant
	TribeBug        Tribe = 5  // Bug
	TribeBot        Tribe = 6  // Bot
	TribeThug       Tribe = 7  // Thug
	TribeUnknown    Tribe = 8  // ???
	TribeChomper    Tribe = 9  // Chomper
	TribeLeafbug    Tribe = 10 // Leafbug
	TribeDeadLander Tribe = 11 // Dead Lander
	TribeMothfly    Tribe = 12 // Mothfly
	TribeSpider     Tribe = 13 // Spider
	TribeCustom     Tribe = 14 // (custom)
	TribeNone       Tribe = 15 // (none)
	TribeWideCustom Tribe = 16 // (wide tribe for UI)
)

var tribeColors = [16]sprites.Color{
	TribeSeedling:   {R: 186, G: 239, B: 80, A: 255},
	TribeWasp:       {R: 223, G: 174, B: 21, A: 255},
	TribeFungi:      {R: 96, G: 112, B: 163, A: 255},
	TribeZombie:     {R: 227, G: 220, B: 162, A: 255},
	TribePlant:      {R: 244, G: 143, B: 190, A: 255},
	TribeBug:        {R: 158, G: 88, B: 47, A: 255},
	TribeBot:        {R: 249, G: 226, B: 70, A: 255},
	TribeThug:       {R: 72, G: 159, B: 202, A: 255},
	TribeUnknown:    {R: 130, G: 143, B: 149, A: 255},
	TribeChomper:    {R: 166, G: 55, B: 32, A: 255},
	TribeLeafbug:    {R: 90, G: 63, B: 106, A: 255},
	TribeDeadLander: {R: 44, G: 11, B: 1, A: 255},
	TribeMothfly:    {R: 44, G: 44, B: 44, A: 255},
	TribeSpider:     {R: 135, G: 115, B: 79, A: 255},
	TribeCustom:     {R: 255, G: 0, B: 255, A: 255},
	TribeNone:       {R: 255, G: 0, B: 255, A: 255},
}

// TribeDef represents a built-in or custom tribe.
type TribeDef struct {
	Tribe Tribe

	Red   uint8
	Green uint8
	Blue  uint8

	CustomName string
}

// Name returns the display name of this TribeDef.
func (t TribeDef) Name() string {
	if t.Tribe == TribeCustom || t.Tribe == TribeWideCustom {
		return t.CustomName
	}

	return t.Tribe.String()
}

// Color returns the color associated with this TribeDef.
func (t TribeDef) Color() sprites.Color {
	if t.Tribe == TribeCustom || t.Tribe == TribeWideCustom {
		return sprites.Color{R: t.Red, G: t.Green, B: t.Blue, A: 255}
	}

	return tribeColors[t.Tribe]
}
