package card

import (
	"bytes"
	"context"
	"encoding/base64"
	"fmt"
	"math/rand"
	"net/url"
	"sync"
	"time"

	"git.lubar.me/ben/spy-cards/gfx"
	"git.lubar.me/ben/spy-cards/internal"
)

// Set is a set of custom cards.
type Set struct {
	External string
	Mode     *GameMode
	Spoiler  []*BannedCards
	Variant  int
	Cards    []*Def
	vanilla  sync.Map // map[ID]*Def

	SpoilerGuard [2]*[32]byte

	PortraitSheet *gfx.AssetTexture
	PortraitIndex map[string]int

	AppliedPR         bool
	RandomByBackOrder bool
	FastAnimation     bool
}

// VanillaVersion returns the Bug Fables version used for vanilla cards.
func (cs *Set) VanillaVersion() BugFablesVersion {
	if v, ok := cs.Mode.Get(FieldVanillaVersion).(*VanillaVersion); ok {
		return v.Version
	}

	return LatestVersion
}

// Card returns the card with the given ID.
func (cs *Set) Card(id ID) *Def {
	for _, c := range cs.Cards {
		if id == c.ID {
			return c
		}
	}

	if c, ok := cs.vanilla.Load(id); ok {
		return c.(*Def)
	}

	c, _ := cs.vanilla.LoadOrStore(id, Vanilla(cs.VanillaVersion(), id))

	return c.(*Def)
}

// MarshalText implements encoding.TextMarshaler.
func (cs *Set) MarshalText() ([]byte, error) {
	if cs.External != "" && cs.Cards == nil {
		return []byte(cs.External), nil
	}

	parts := make([][]byte, 1+len(cs.Cards))
	cards := parts[1:]

	if cs.Mode != nil {
		b, err := cs.Mode.MarshalText()
		if err != nil {
			return nil, fmt.Errorf("card: encoding game mode: %w", err)
		}

		parts[0] = b
	} else {
		parts = parts[1:]
	}

	for i, c := range cs.Cards {
		b, err := c.MarshalText()
		if err != nil {
			return nil, fmt.Errorf("card: encoding card at index %d: %w", i, err)
		}

		cards[i] = b
	}

	return bytes.Join(parts, []byte{','}), nil
}

func (cs *Set) BuildPortraitIndex() {
	if cs.PortraitIndex != nil {
		return
	}

	// map of binary portrait IDs to their index in the set
	portraits := make(map[string]int)
	addPortrait := func(custom []byte) {
		s := string(custom)
		if _, ok := portraits[s]; !ok {
			portraits[s] = len(portraits)
		}
	}

	for _, c := range cs.Cards {
		if c.Portrait == PortraitCustomExternal {
			addPortrait(c.CustomPortrait)
		}
	}

	if cs.Mode != nil {
		for _, f := range cs.Mode.Fields {
			if md, ok := f.(*Metadata); ok && md.Portrait == PortraitCustomExternal {
				addPortrait(md.CustomPortrait)
			}

			if v, ok := f.(*Variant); ok {
				for _, r := range v.Rules {
					if md, ok := r.(*Metadata); ok && md.Portrait == PortraitCustomExternal {
						addPortrait(md.CustomPortrait)
					}
				}
			}
		}
	}

	cs.PortraitIndex = portraits
}

// CopyFrom copies non-cache fields.
//
// The vanilla map cannot be copied as it contains sync primitives.
func (cs *Set) CopyFrom(s *Set) {
	cs.External = s.External
	cs.Mode = s.Mode
	cs.Spoiler = s.Spoiler
	cs.Variant = s.Variant
	cs.Cards = s.Cards

	cs.SpoilerGuard = s.SpoilerGuard

	cs.PortraitSheet = s.PortraitSheet
	cs.PortraitIndex = s.PortraitIndex

	cs.AppliedPR = s.AppliedPR
	cs.RandomByBackOrder = s.RandomByBackOrder
	cs.FastAnimation = s.FastAnimation
}

// ExternalMode is a Spy Cards Online game mode as returned by the server.
type ExternalMode struct {
	Name         string
	Cards        string
	Revision     int
	QuickJoin    bool
	AnyRecording bool
}

// FetchMode downloads a game mode from the server.
func FetchMode(ctx context.Context, name, revision string) (*ExternalMode, error) {
	u := "latest/" + url.PathEscape(name)
	if revision != "0" {
		u = "get-revision/" + url.PathEscape(name) + "/" + url.PathEscape(revision)
	}

	var em ExternalMode

	err := internal.FetchJSON(ctx, internal.GetConfig(ctx).CustomCardAPIBaseURL+u, &em)
	if err != nil {
		return nil, fmt.Errorf("card: downloading game mode: %w", err)
	}

	return &em, nil
}

// UnmarshalText implements encoding.TextUnmarshaler.
func (cs *Set) UnmarshalText(b []byte) error {
	cs.External = ""

	parts := bytes.Split(b, []byte{','})
	if len(parts) == 1 && bytes.Contains(parts[0], []byte{'.'}) {
		cs.External = string(parts[0])

		dot := bytes.IndexByte(parts[0], '.')
		name := string(parts[0][:dot])
		revision := string(parts[0][dot+1:])

		data, err := FetchMode(context.TODO(), name, revision)
		if err != nil {
			return err
		}

		b = []byte(data.Cards)
		parts = bytes.Split(b, []byte{','})
	}

	if len(parts) == 1 && len(parts[0]) == 0 {
		cs.Mode = nil
		cs.Variant = -1
		cs.Cards = nil

		return nil
	}

	var mode *GameMode

	cardDefs := make([]Def, len(parts))
	cards := make([]*Def, len(parts))

	for i, part := range parts {
		p, err := base64.StdEncoding.DecodeString(string(part))
		if err != nil {
			return fmt.Errorf("card: decoding card at index %d: %w", i, err)
		}

		if i == 0 && len(p) != 0 && p[0] == 3 {
			mode = &GameMode{}

			if err = mode.UnmarshalBinary(p); err != nil {
				return fmt.Errorf("card: parsing game mode data: %w", err)
			}

			continue
		}

		cards[i] = &cardDefs[i]

		if err = cards[i].UnmarshalBinary(p); err != nil {
			return fmt.Errorf("card: parsing card at index %d: %w", i, err)
		}
	}

	cs.Mode = mode
	cs.Variant = -1

	if mode != nil {
		cs.Cards = cards[1:]
	} else {
		cs.Cards = cards
	}

	var unpickable *BannedCards

	for _, c := range cs.Cards {
		if c.legacyUnpickable {
			if unpickable == nil {
				if cs.Mode == nil {
					cs.Mode = &GameMode{}
				}

				unpickable = &BannedCards{
					Flags: BannedCardTypeUnpickable,
				}
				cs.Mode.Fields = append(cs.Mode.Fields, unpickable)
			}

			unpickable.Cards = append(unpickable.Cards, c.ID)
		}
	}

	return nil
}

// ByBack returns the cards in this Set with the specified back design that
// are available for deck building.
func (cs *Set) ByBack(back Rank, d Deck) []*Def {
	unpickable := make(map[ID]bool)

	if cs.Mode != nil {
		for _, f := range cs.Mode.GetAll(FieldBannedCards) {
			bc := f.(*BannedCards)

			if typ := bc.Flags & BannedCardTypeMask; typ != BannedCardTypeBanned && typ != BannedCardTypeUnpickable {
				continue
			}

			if len(bc.Cards) == 0 {
				for i := ID(0); i < 128; i++ {
					unpickable[i] = true
				}
			} else {
				for _, id := range bc.Cards {
					unpickable[id] = true
				}
			}
		}
	}

	for _, bc := range cs.Spoiler {
		for _, id := range bc.Cards {
			unpickable[id] = true
		}
	}

	var vanilla []ID

	switch back {
	case Enemy:
		vanilla = vanillaOrder[0]
	case MiniBoss:
		vanilla = vanillaOrder[1]
	case Boss:
		vanilla = vanillaOrder[2]
	}

	cards := make([]*Def, 0, len(vanilla))

	for _, id := range vanilla {
		if unpickable[id] {
			continue
		}

		overridden := false

		for _, c := range cs.Cards {
			if c.ID == id {
				overridden = true

				break
			}
		}

		cd := cs.Card(id)
		if cd != nil && !overridden {
			cards = append(cards, cd)
		}
	}

	for _, c := range cs.Cards {
		if c.Rank.Back(false) != back || unpickable[c.ID] {
			continue
		}

		cards = append(cards, c)
	}

	type limitCount struct {
		f, c uint64
	}

	limits := make(map[*DeckLimitFilter]*limitCount)
	for _, f := range cs.Mode.GetAll(FieldDeckLimitFilter) {
		limits[f.(*DeckLimitFilter)] = &limitCount{}
	}

	for _, id := range d {
		for l := range limits {
			if l.Filter.IsMatch(cs.Card(id), ^ID(0)) {
				limits[l].f++
			}

			if l.Condition.IsMatch(cs.Card(id), ^ID(0)) {
				limits[l].c++
			}
		}
	}

	for l, count := range limits {
		if count.f < l.Count {
			continue
		}

		if count.c >= l.CondCount {
			for i := 0; i < len(cards); i++ {
				if l.Filter.IsMatch(cards[i], ^ID(0)) {
					cards = append(cards[:i], cards[i+1:]...)
					i--
				}
			}
		} else if count.c == l.CondCount-1 && count.f > l.Count {
			for i := 0; i < len(cards); i++ {
				if l.Condition.IsMatch(cards[i], ^ID(0)) {
					cards = append(cards[:i], cards[i+1:]...)
					i--
				}
			}
		}
	}

	rules, ok := cs.Mode.Get(FieldGameRules).(*GameRules)
	if !ok {
		rules = &DefaultGameRules
	}

	duplicatePerRank := [4]uint64{
		Boss:     rules.DuplicateBoss,
		MiniBoss: rules.DuplicateMiniBoss,
		Effect:   rules.DuplicateEffect,
		Attacker: rules.DuplicateAttacker,
	}

	seen := make(map[ID]uint64)
	for _, id := range d {
		seen[id]++
	}

	// handle max duplicates
	for i := 0; i < len(cards); i++ {
		if max := duplicatePerRank[cards[i].Rank]; max != 0 && seen[cards[i].ID] >= max {
			cards = append(cards[:i], cards[i+1:]...)
			i--
		}
	}

	if cs.RandomByBackOrder {
		r := rand.New(rand.NewSource(time.Now().UnixNano())) /*#nosec*/

		r.Shuffle(len(cards), func(i, j int) {
			cards[i], cards[j] = cards[j], cards[i]
		})
	}

	return cards
}

func (cs *Set) sortCardGroups() {
	for _, f := range cs.Mode.GetAll(FieldGroup) {
		cg := f.(*Group)
		if len(cg.Cards) == 0 {
			continue
		}

		first := -1

		for i, c := range cs.Cards {
			if c.ID == cg.Cards[0] {
				first = i

				break
			}
		}

		if first == -1 {
			first = len(cs.Cards)
			cs.Cards = append(cs.Cards, Vanilla(cs.VanillaVersion(), cg.Cards[0]))
		}

		for i, id := range cg.Cards {
			if first+i < len(cs.Cards) && cs.Cards[first+i].ID == id {
				continue
			}

			found := false

			for j, c := range cs.Cards {
				if c.ID == id {
					found = true

					if j < first {
						first--
					}

					cs.Cards = append(cs.Cards[:j], cs.Cards[j+1:]...)
					cs.Cards = append(cs.Cards[:first+i], append([]*Def{c}, cs.Cards[first+i:]...)...)

					break
				}
			}

			if !found {
				cs.Cards = append(cs.Cards[:first+i], append([]*Def{
					Vanilla(cs.VanillaVersion(), id),
				}, cs.Cards[first+i:]...)...)
			}
		}
	}
}

// Sets is a combination of sets of cards.
type Sets []Set

// Apply creates a Set that is the combination of cards in these sets,
// replacing IDs to ensure they don't overlap.
func (cs Sets) Apply() (*Set, error) {
	for i := range cs {
		cs[i].sortCardGroups()
	}

	if len(cs) == 1 {
		return &cs[0], nil
	}

	var combined Set

	usedID := make(map[ID]bool)

	for i := range cs {
		replaceID := make(map[ID]ID)

		setUsedID := make(map[ID]bool)
		for _, c := range cs[i].Cards {
			setUsedID[c.ID] = true
		}

		for _, c := range cs[i].Cards {
			if !usedID[c.ID] {
				usedID[c.ID] = true

				continue
			}

			id := ID(128)

			for usedID[id] || setUsedID[id] {
				id++
			}

			replaceID[c.ID] = id
			usedID[id] = true
		}

		for _, c := range cs[i].Cards {
			for from, to := range replaceID {
				c.UpdateID(from, to)
			}
		}

		combined.Cards = append(combined.Cards, cs[i].Cards...)

		if cs[i].Mode != nil {
			if combined.Mode == nil {
				combined.Mode = &GameMode{}
			}

			for _, f := range cs[i].Mode.Fields {
				for from, to := range replaceID {
					f.UpdateID(from, to)
				}
			}

			combined.Mode.Fields = append(combined.Mode.Fields, cs[i].Mode.Fields...)
		}
	}

	return &combined, nil
}

// MarshalText implements encoding.TextMarshaler.
func (cs Sets) MarshalText() ([]byte, error) {
	parts := make([][]byte, len(cs))

	for i := range cs {
		p, err := cs[i].MarshalText()
		if err != nil {
			return nil, fmt.Errorf("card: encoding card set %d: %w", i, err)
		}

		parts[i] = p
	}

	return bytes.Join(parts, []byte{';'}), nil
}

// UnmarshalText implements encoding.TextUnmarshaler.
func (cs *Sets) UnmarshalText(b []byte) error {
	parts := bytes.Split(b, []byte{';'})
	sets := make([]Set, len(parts))

	for i, p := range parts {
		if err := sets[i].UnmarshalText(p); err != nil {
			return fmt.Errorf("card: parsing card set %d: %w", i, err)
		}
	}

	*cs = sets

	return nil
}
