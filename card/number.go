package card

import "strconv"

// Number is an integer that may be infinite or invalid.
type Number struct {
	Amount    int64
	AmountInf int64
	NaN       bool
}

// Num is a convenience function that returns a Number representing the specified amount.
func Num(amount int64) Number {
	return Number{Amount: amount}
}

func (n Number) String() string {
	if n.NaN {
		return "NaN"
	}

	switch {
	case n.AmountInf > 0:
		return PosInf
	case n.AmountInf < 0:
		return NegInf
	default:
		return strconv.FormatInt(n.Amount, 10)
	}
}

// Negate sets n's value to -n.
func (n *Number) Negate() {
	n.Amount, n.AmountInf = -n.Amount, -n.AmountInf
}

// Add increases the value of n by x.
func (n *Number) Add(x Number, allowSubtractingInf bool) {
	if n.NaN || x.NaN || (!allowSubtractingInf && ((n.AmountInf < 0 && x.AmountInf > 0) || (n.AmountInf > 0 && x.AmountInf < 0))) {
		n.NaN = true
	}

	n.Amount += x.Amount
	n.AmountInf += x.AmountInf
}

// Less returns true if n is less than x.
func (n Number) Less(x Number) bool {
	switch {
	case n.NaN, x.NaN:
		return false
	case n.AmountInf > 0 && x.AmountInf > 0:
		return false
	case n.AmountInf < 0 && x.AmountInf < 0:
		return false
	case n.AmountInf > 0, x.AmountInf < 0:
		return false
	case n.AmountInf < 0, x.AmountInf > 0:
		return true
	default:
		return n.Amount < x.Amount
	}
}
