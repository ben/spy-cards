package card

// EffectFlag is a combination bitfield type for all effect flags.
type EffectFlag uint64

// Constants for EffectFlag.
const (
	FlagFlavorTextHideRemaining EffectFlag = 1 << 0
	FlagFlavorTextCustomColor   EffectFlag = 1 << 1

	FlagStatDEF      EffectFlag = 1 << 0
	FlagStatInfinity EffectFlag = 1 << 1
	FlagStatOpponent EffectFlag = 1 << 2

	FlagSummonOpponent  EffectFlag = 1 << 0
	FlagSummonReplace   EffectFlag = 1 << 1
	FlagSummonInvisible EffectFlag = 1 << 2

	FlagHealInfinity EffectFlag = 1 << 0
	FlagHealOpponent EffectFlag = 1 << 1
	FlagHealRaw      EffectFlag = 1 << 2

	FlagTPInfinity EffectFlag = 1 << 0

	FlagNumbInfinity      EffectFlag = 1 << 0
	FlagNumbSelf          EffectFlag = 1 << 1
	FlagNumbHighest       EffectFlag = 1 << 2
	FlagNumbIgnoreATK     EffectFlag = 1 << 3
	FlagNumbIgnoreDEF     EffectFlag = 1 << 4
	FlagNumbAvoidZero     EffectFlag = 1 << 5
	FlagNumbResult        EffectFlag = 1 << 6
	FlagNumbResult2       EffectFlag = 1 << 7
	FlagNumbSummon        EffectFlag = 1 << 8
	FlagNumbIgnorePrevent EffectFlag = 1 << 9

	FlagMultiplyHealingOpponent     EffectFlag = 1 << 0
	FlagMultiplyHealingFuture       EffectFlag = 1 << 1
	FlagMultiplyHealingTypeMask     EffectFlag = 3 << 2
	FlagMultiplyHealingTypeAll      EffectFlag = 0 << 2
	FlagMultiplyHealingTypeNegative EffectFlag = 1 << 2
	FlagMultiplyHealingTypePositive EffectFlag = 2 << 2

	FlagPreventNumbOpponent EffectFlag = 1 << 0

	FlagModifyCardsOpponent        EffectFlag = 1 << 0
	FlagModifyCardsTypeMask        EffectFlag = 3 << 1
	FlagModifyCardsTypeAdd         EffectFlag = 0 << 1
	FlagModifyCardsTypeRemove      EffectFlag = 1 << 1
	FlagModifyCardsTypeMove        EffectFlag = 2 << 1
	FlagModifyCardsTypeReplace     EffectFlag = 3 << 1
	FlagModifyCardsTargetMask      EffectFlag = 3 << 3
	FlagModifyCardsTargetAny       EffectFlag = 0 << 3
	FlagModifyCardsTargetHand      EffectFlag = 1 << 3
	FlagModifyCardsTargetDeck      EffectFlag = 2 << 3
	FlagModifyCardsTargetField     EffectFlag = 3 << 3
	FlagModifyCardsRequireMask     EffectFlag = 3 << 5
	FlagModifyCardsRequireNone     EffectFlag = 0 << 5
	FlagModifyCardsRequireSelf     EffectFlag = 1 << 5
	FlagModifyCardsRequireOpponent EffectFlag = 2 << 5
	FlagModifyCardsResult          EffectFlag = 1 << 7
	FlagModifyCardsResult2         EffectFlag = 1 << 8
	FlagModifyCardsNamedGroup      EffectFlag = 1 << 9

	FlagModifyCardCostInfinity EffectFlag = 1 << 0
	FlagModifyCardCostSet      EffectFlag = 1 << 1

	FlagDrawCardOpponent  EffectFlag = 1 << 0
	FlagDrawCardIgnoreMax EffectFlag = 1 << 1
	FlagDrawCardUseFilter EffectFlag = 1 << 2
	FlagDrawCardResult    EffectFlag = 1 << 3
	FlagDrawCardResult2   EffectFlag = 1 << 4

	FlagModifyGameRuleRelative       EffectFlag = 1 << 0
	FlagModifyGameRulePlayerMask     EffectFlag = 3 << 1
	FlagModifyGameRulePlayerSelf     EffectFlag = 0 << 1
	FlagModifyGameRulePlayerOpponent EffectFlag = 1 << 1
	FlagModifyGameRulePlayerBoth     EffectFlag = 2 << 1

	FlagDelaySetupOpponent EffectFlag = 1 << 0
	FlagDelaySetupDelete   EffectFlag = 1 << 1

	FlagCondCardOpponent         EffectFlag = 1 << 0
	FlagCondCardTypeMask         EffectFlag = 3 << 1
	FlagCondCardTypeGreaterEqual EffectFlag = 0 << 1
	FlagCondCardTypeLessThan     EffectFlag = 1 << 1
	FlagCondCardTypeEach         EffectFlag = 2 << 1
	FlagCondCardAllowNumb        EffectFlag = 1 << 3
	FlagCondCardLocationMask     EffectFlag = 3 << 4
	FlagCondCardLocationField    EffectFlag = 0 << 4
	FlagCondCardLocationHand     EffectFlag = 1 << 4
	FlagCondCardLocationDeck     EffectFlag = 2 << 4

	FlagCondLimitGreaterThan EffectFlag = 1 << 0

	FlagCondWinnerTypeMask      EffectFlag = 7 << 0
	FlagCondWinnerTypeWinner    EffectFlag = 0 << 0
	FlagCondWinnerTypeLoser     EffectFlag = 1 << 0
	FlagCondWinnerTypeTie       EffectFlag = 2 << 0
	FlagCondWinnerTypeNotTie    EffectFlag = 3 << 0
	FlagCondWinnerTypeNotWinner EffectFlag = 4 << 0
	FlagCondWinnerTypeNotLoser  EffectFlag = 5 << 0

	FlagCondApplyNextRound        EffectFlag = 1 << 0
	FlagCondApplyOpponent         EffectFlag = 1 << 1
	FlagCondApplyShowMask         EffectFlag = 3 << 2
	FlagCondApplyShowJustEffect   EffectFlag = 0 << 2
	FlagCondApplyShowOriginalText EffectFlag = 1 << 2
	FlagCondApplyShowNothing      EffectFlag = 2 << 2

	FlagCondCoinTails    EffectFlag = 1 << 0
	FlagCondCoinNegative EffectFlag = 1 << 1
	FlagCondCoinStat     EffectFlag = 1 << 2
	FlagCondCoinWeighted EffectFlag = 1 << 3

	FlagCondStatOpponent     EffectFlag = 1 << 0
	FlagCondStatLessThan     EffectFlag = 1 << 1
	FlagCondStatTypeMask     EffectFlag = 3 << 2
	FlagCondStatTypeATK      EffectFlag = 0 << 2
	FlagCondStatTypeDEF      EffectFlag = 1 << 2
	FlagCondStatTypeHP       EffectFlag = 2 << 2
	FlagCondStatTypeTP       EffectFlag = 3 << 2
	FlagCondStatMultiple     EffectFlag = 1 << 4
	FlagCondStatMathMask     EffectFlag = 3 << 5
	FlagCondStatMathNone     EffectFlag = 0 << 5
	FlagCondStatMathAdd      EffectFlag = 1 << 5
	FlagCondStatMathSubtract EffectFlag = 2 << 5
	FlagCondStatMathOpponent EffectFlag = 1 << 7
	FlagCondStatMathTypeMask EffectFlag = 3 << 8
	FlagCondStatMathTypeATK  EffectFlag = 0 << 8
	FlagCondStatMathTypeDEF  EffectFlag = 1 << 8
	FlagCondStatMathTypeHP   EffectFlag = 2 << 8
	FlagCondStatMathTypeTP   EffectFlag = 3 << 8

	FlagCondInHandHide       EffectFlag = 1 << 0
	FlagCondInHandOnPlayMask EffectFlag = 3 << 1
	FlagCondInHandOnPlayNone EffectFlag = 0 << 1
	FlagCondInHandOnPlayAlso EffectFlag = 1 << 1
	FlagCondInHandOnPlayOnly EffectFlag = 2 << 1

	FlagCondCompareTargetCardInvert EffectFlag = 1 << 0

	FlagCondOnExileGroup EffectFlag = 1 << 0
)
