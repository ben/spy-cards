//go:build gofuzzbeta
// +build gofuzzbeta

package card

import (
	"reflect"
	"testing"

	"github.com/davecgh/go-spew/spew"
)

func FuzzCard(f *testing.F) {
	for _, back := range []Rank{Boss, MiniBoss, Enemy} {
		for _, id := range VanillaOrder(back) {
			b, err := Vanilla(LatestVersion, id).MarshalBinary()
			if err != nil {
				f.Error(err)

				continue
			}

			f.Add(b)
		}
	}

	f.Fuzz(func(t *testing.T, b []byte) {
		t.Parallel()

		var def1, def2 Def

		err := def1.UnmarshalBinary(b)
		if err != nil {
			t.Skip()
		}

		// not an interesting field for differences
		def1.legacyUnpickable = false

		t.Log(spew.Sdump(&def1))

		b1, err := def1.MarshalBinary()
		if err != nil {
			t.Fatalf("marshaling error: %v", err)
		}

		err = def2.UnmarshalBinary(b1)
		if err != nil {
			t.Fatalf("unmarshaling error: %v", err)
		}

		t.Log(spew.Sdump(&def2))

		if !reflect.DeepEqual(&def1, &def2) {
			t.Errorf("not equal: %v / %v", &def1, &def2)
		}
	})
}
