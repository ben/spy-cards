//go:generate stringer -type ExtensionType -trimprefix Extension

package card

import (
	"bytes"
	"encoding/base32"
	"fmt"
	"log"
	"strings"

	"git.lubar.me/ben/spy-cards/format"
)

type AssetStage struct {
	Name        string
	DisplayName string
	CIDs        []ContentIdentifier
}

// AssetStages are the stages included with Spy Cards Online, with data about the previous encodings of their IDs.
var AssetStages = []*AssetStage{
	{
		DisplayName: "Metal Island Auditorium",
		Name:        "stage0001",
		CIDs: []ContentIdentifier{
			ContentIdentifier("\x01\x70\x12\x20\xee\xb4\x57\xb7\x33\x4b\xa0\xea\x6e\x1e\xc3\x61\x8a\xae\xea\x25\x3c\xa6\xf5\xdb\x79\xda\xee\x01\xfd\x9e\xb8\x1b\xe2\x2e\xb2\x1f"),
		},
	},
	{
		DisplayName: "Golden Hills",
		Name:        "stage0002",
		CIDs: []ContentIdentifier{
			ContentIdentifier("\x01\x70\x12\x20\x57\xfb\x38\x4d\x61\x01\x58\x3f\x31\xd1\xc7\x94\x2c\x69\xb3\x16\x85\x36\x8b\x33\x41\x08\xae\x21\x6e\x55\xc7\x1d\x83\x45\x09\x81"),
		},
	},
	{
		DisplayName: "Forsaken Lands",
		Name:        "stage0003",
		CIDs: []ContentIdentifier{
			ContentIdentifier("\x01\x70\x12\x20\x27\x68\x54\xff\x4b\x10\xf4\xbb\xb0\xea\xf0\x8d\xac\x1b\x35\x16\xf1\xd2\x13\xde\xa6\xa1\x42\x7b\x2c\xfd\x70\x9e\x7e\xe2\xa7\xd0"),
		},
	},
	{
		DisplayName: "Doppel's Underground Tavern",
		Name:        "stage0004",
	},
	{
		DisplayName: "Broodmother",
		Name:        "broodmother",
		CIDs: []ContentIdentifier{
			ContentIdentifier("\x01\x70\x12\x20\xc0\xfe\x0d\x55\x32\x99\x6a\xd9\x08\x38\x65\x26\xc5\x6c\x8e\x94\x39\x8f\x6b\xe8\xc0\xf6\xae\x54\x62\x7c\x7e\x00\xba\x84\x05\x35"),
			ContentIdentifier("\x01\x70\x12\x20\x19\x4a\x41\xc7\xa4\x58\x17\x1a\x43\xea\x80\x58\xa7\x0c\x09\x47\xb8\x63\x18\xea\x0b\xe7\x1e\xc4\x06\xb1\x84\x75\xc7\xd3\x7a\x30"),
		},
	},
	{
		DisplayName: "Abandoned Tent",
		Name:        "abandoned-tent",
		CIDs: []ContentIdentifier{
			ContentIdentifier("\x01\x70\x12\x20\xff\xc1\x94\x32\xfd\x47\x84\xa3\x22\xaa\xa3\x40\xd3\x5f\xdd\x1c\x8f\xac\x66\xab\x50\x09\xe3\xed\x96\x4c\xf2\x4d\xdd\xd9\x06\x85"),
			ContentIdentifier("\x01\x70\x12\x20\x25\x36\x35\x3a\x3f\x93\xed\xeb\x30\x70\xda\x67\x4f\xd1\x59\xf8\x0c\x1e\xd3\x55\x93\x84\xb6\xc9\x66\x0e\x8a\xbe\xac\x5e\x74\xbe"),
		},
	},
	{
		DisplayName: "Ant Kingdom Mine",
		Name:        "mine",
		CIDs: []ContentIdentifier{
			ContentIdentifier("\x01\x70\x12\x20\x84\x04\x70\x53\xf8\x48\xf4\xb8\xf8\x5e\xaa\x6f\xd9\xe7\x63\xb5\x4a\xaf\xd1\x42\xac\x00\x27\xae\xe5\xcc\x97\xe5\x3f\x32\x6f\xe3"),
		},
	},
	{
		DisplayName: "Ant Kingdom Plaza",
		Name:        "bugaria-main",
		CIDs: []ContentIdentifier{
			ContentIdentifier("\x01\x70\x12\x20\x02\x72\x20\x2b\x09\x81\x50\x51\x9b\xb6\xdf\xe5\x43\xd4\xb5\x3d\x36\xea\x05\x35\x55\x41\x28\x5d\xfd\x35\xb0\xc7\xf7\x18\x8d\x66"),
		},
	},
}

// ExtensionType is an extension field type.
type ExtensionType uint64

// Constants for ExtensionType.
const (
	ExtensionSetStageIPFS ExtensionType = 0
	ExtensionSetMusic     ExtensionType = 1
	ExtensionPlaySound    ExtensionType = 2
	ExtensionSetStage     ExtensionType = 3
)

// Extension flags.
const (
	// ExtensionOverrideCustom (SetStageIPFS, SetMusic, SetStage): override even
	// if a custom override is already active.
	ExtensionOverrideCustom uint64 = 1 << 0
)

// Extension is an extension field that allows cards to have meta-game effects.
type Extension struct {
	Type      ExtensionType
	Flags     uint64
	CID       ContentIdentifier
	Name      string
	LoopStart float32
	LoopEnd   float32
	unparsed  []byte
}

// Marshal encodes the extension field.
func (e *Extension) Marshal(w *format.Writer) error {
	switch e.Type {
	case ExtensionSetStageIPFS:
		w.UVarInt(e.Flags)
		w.UVarInt(uint64(len(e.CID)))
		w.Bytes([]byte(e.CID))
	case ExtensionSetMusic:
		w.UVarInt(e.Flags)
		w.UVarInt(uint64(len(e.CID)))
		w.Bytes([]byte(e.CID))
		w.Write(&e.LoopStart)
		w.Write(&e.LoopEnd)
	case ExtensionPlaySound:
		w.UVarInt(e.Flags)
		w.UVarInt(uint64(len(e.CID)))
		w.Bytes([]byte(e.CID))
	case ExtensionSetStage:
		w.UVarInt(e.Flags)
		w.String(e.Name)
	default:
		return fmt.Errorf("card: unhandled extension type: %v", e.Type)
	}

	w.Write(e.unparsed)

	return nil
}

// Unmarshal decodes the extension field.
func (e *Extension) Unmarshal(r *format.Reader, formatVersion uint64) (err error) {
	defer format.Catch(&err)

	switch e.Type {
	case ExtensionSetStageIPFS:
		e.Flags = r.UVarInt()
		e.CID = r.Bytes(int(r.UVarInt()))

		for _, s := range AssetStages {
			for _, c := range s.CIDs {
				if bytes.Equal(c, e.CID) {
					e.Type = ExtensionSetStage
					e.CID = nil
					e.Name = s.Name

					break
				}
			}

			if e.Type != ExtensionSetStageIPFS {
				break
			}
		}

		if e.Type == ExtensionSetStageIPFS {
			log.Println("WARNING: no replacement for legacy SetStageIPFS CID", e.CID)
		}
	case ExtensionSetMusic:
		e.Flags = r.UVarInt()
		e.CID = r.Bytes(int(r.UVarInt()))
		r.Read(&e.LoopStart)
		r.Read(&e.LoopEnd)
	case ExtensionPlaySound:
		e.Flags = r.UVarInt()
		e.CID = r.Bytes(int(r.UVarInt()))
	case ExtensionSetStage:
		e.Flags = r.UVarInt()
		e.Name = r.String()
	default:
		return fmt.Errorf("card: unhandled extension type: %v", e.Type)
	}

	if r.Len() != 0 {
		e.unparsed = r.Bytes(r.Len())

		return fmt.Errorf("card: %d extra bytes in extension field", len(e.unparsed))
	}

	e.unparsed = nil

	return nil
}

// ContentIdentifier is an IPFS CID.
type ContentIdentifier []byte

func (cid ContentIdentifier) String() string {
	encoding := base32.StdEncoding.WithPadding(base32.NoPadding)

	return "b" + strings.ToLower(encoding.EncodeToString([]byte(cid)))
}
