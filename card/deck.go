package card

import (
	"errors"
	"fmt"

	"git.lubar.me/ben/spy-cards/format"
)

// Deck is a partially ordered list of Spy Cards cards, with boss first,
// then mini-boss, and then enemy (attacker and effect) cards.
type Deck []ID

// Validate returns a non-nil error if the deck is not a valid deck
// for the given card set.
func (d Deck) Validate(set *Set) error {
	banned := make(map[ID]bool)
	rules := DefaultGameRules

	if set == nil {
		set = &Set{
			Variant: -1,
		}
	}

	type limitInstance struct {
		Filter *DeckLimitFilter
		Count  uint64
	}

	var limits []limitInstance

	if set.Mode != nil {
		mode, _ := set.Mode.Variant(set.Variant)

		for _, f := range mode.GetAll(FieldBannedCards) {
			ban := f.(*BannedCards)
			if typ := ban.Flags & BannedCardTypeMask; typ != BannedCardTypeBanned && typ != BannedCardTypeUnpickable {
				continue
			}

			if len(ban.Cards) == 0 {
				for i := ID(0); i < 128; i++ {
					banned[i] = true
				}
			} else {
				for _, id := range ban.Cards {
					banned[id] = true
				}
			}
		}

		if r := mode.Get(FieldGameRules); r != nil {
			rules = *r.(*GameRules)
		}

		limitFields := mode.GetAll(FieldDeckLimitFilter)
		limits = make([]limitInstance, 0, len(limitFields))

		for i := range limitFields {
			f := limitFields[i].(*DeckLimitFilter)

			var condCount uint64

			for _, id := range d {
				if f.Condition.IsMatch(set.Card(id), ^ID(0)) {
					condCount++
				}
			}

			if condCount >= f.CondCount {
				limits = append(limits, limitInstance{
					Filter: f,
				})
			}
		}
	}

	for _, ban := range set.Spoiler {
		for _, id := range ban.Cards {
			banned[id] = true
		}
	}

	if uint64(len(d)) != rules.CardsPerDeck {
		return fmt.Errorf("card: wrong number of cards in deck (expected %d, actual %d)", rules.CardsPerDeck, len(d))
	}

	numTotalBosses := rules.BossCards + rules.MiniBossCards

	for i, id := range d {
		var expectBack Rank

		switch {
		case uint64(i) < rules.BossCards:
			expectBack = Boss
		case uint64(i) < numTotalBosses:
			expectBack = MiniBoss
		default:
			expectBack = Enemy
		}

		c := set.Card(id)
		if id >= 128 {
			// don't allow "vanilla" custom cards
			c = nil

			for _, def := range set.Cards {
				if def.ID == id {
					c = def

					break
				}
			}
		}

		if c == nil {
			return fmt.Errorf("card: for card %d (ID %d): no such card", i, id)
		}

		if c.Rank.Back(false) != expectBack {
			return fmt.Errorf("card: card %d in deck (%q) has wrong card back (expected %v, actual %v)", i, c.DisplayName(), expectBack, id.Rank().Back(false))
		}

		if banned[id] {
			return fmt.Errorf("card: card %d in deck (%q) is banned", i, c.DisplayName())
		}

		for i := range limits {
			if limits[i].Filter.Filter.IsMatch(c, ^ID(0)) {
				limits[i].Count++

				if limits[i].Count > limits[i].Filter.Count {
					return fmt.Errorf("card: card %d in deck (%q) exceeds limit", i, c.DisplayName())
				}
			}
		}
	}

	duplicatePerRank := [4]uint64{
		Boss:     rules.DuplicateBoss,
		MiniBoss: rules.DuplicateMiniBoss,
		Effect:   rules.DuplicateEffect,
		Attacker: rules.DuplicateAttacker,
	}

	used := make(map[ID]uint64)
	for i, id := range d {
		used[id]++

		c := set.Card(id)
		if c == nil {
			return fmt.Errorf("card: for card %d (ID %d): no such card", i, id)
		}

		if duplicatePerRank[c.Rank] != 0 && used[id] > duplicatePerRank[c.Rank] {
			return fmt.Errorf("card: card %d in deck (%q) has already occurred the maximum number of times (%d) for a %v card", i, c.DisplayName(), duplicatePerRank[c.Rank], c.Rank)
		}
	}

	return nil
}

// MarshalText implements encoding.TextMarshaler.
func (d Deck) MarshalText() ([]byte, error) {
	b, err := d.MarshalBinary()
	if err != nil {
		return nil, err
	}

	return []byte(format.Encode32(b)), nil
}

// MarshalBinary implements encoding.BinaryMarshaler.
func (d Deck) MarshalBinary() ([]byte, error) {
	if len(d) == 0 {
		return nil, nil
	}

	var anyNonBasic, anyAbove255 bool

	noVanilla := true

	for i, c := range d {
		if c < 128 {
			noVanilla = false
		}

		if c > 255 {
			anyAbove255 = true
		}

		if c.BasicIndex() == -1 {
			anyNonBasic = true
		}

		switch c.Rank() {
		case Boss:
			if i != 0 {
				anyNonBasic = true
			}
		case MiniBoss:
			if i != 1 && i != 2 {
				anyNonBasic = true
			}
		case Attacker, Effect:
			if i <= 2 {
				anyNonBasic = true
			}
		}
	}

	if len(d) <= 3 {
		anyNonBasic = true
	}

	var w format.Writer

	switch {
	case noVanilla:
		w.Write(uint8(0x82))

		for _, c := range d {
			w.UVarInt(uint64(c - 128))
		}
	case anyAbove255:
		w.Write(uint8(0x81))

		for _, c := range d {
			w.UVarInt(uint64(c))
		}
	case anyNonBasic:
		w.Write(uint8(0x80))

		for _, c := range d {
			w.Write(uint8(c))
		}
	default:
		w.Write(uint8((d[0].BasicIndex() << 2) | (d[1].BasicIndex() >> 3)))
		w.Write(uint8((d[1].BasicIndex() << 5) | (d[2].BasicIndex())))

		d = d[3:]

		var buf [3]uint8

		for len(d) >= 4 {
			buf[0] = uint8((d[0].BasicIndex() << 2) | (d[1].BasicIndex() >> 4))
			buf[1] = uint8((d[1].BasicIndex() << 4) | (d[2].BasicIndex() >> 2))
			buf[2] = uint8((d[2].BasicIndex() << 6) | (d[3].BasicIndex()))

			w.Bytes(buf[:])

			d = d[4:]
		}

		if len(d) >= 1 {
			buf[0] = uint8(d[0].BasicIndex() << 2)
		}

		if len(d) >= 2 {
			buf[0] |= uint8(d[1].BasicIndex() >> 4)
			buf[1] = uint8(d[1].BasicIndex() << 4)
		}

		if len(d) >= 3 {
			buf[1] |= uint8(d[2].BasicIndex() >> 2)
			buf[2] = uint8(d[2].BasicIndex()<<6) | 63
		}

		w.Bytes(buf[:len(d)])
	}

	return w.Data(), nil
}

// UnmarshalText implements encoding.TextUnmarshaler.
func (d *Deck) UnmarshalText(b []byte) error {
	dec, err := format.Decode32(string(b))
	if err != nil {
		return fmt.Errorf("card: decoding deck: %w", err)
	}

	return d.UnmarshalBinary(dec)
}

// UnmarshalBinary implements encoding.BinaryUnmarshaler.
func (d *Deck) UnmarshalBinary(b []byte) (err error) {
	if len(b) == 0 {
		*d = nil

		return nil
	}

	var r format.Reader

	defer format.Catch(&err)

	r.Init(b[1:])

	var deck Deck

	switch {
	case b[0] < 0x80:
		if len(b) < 2 {
			return errors.New("deck: invalid deck code: too short")
		}

		cardIndex := func(back int, index uint8) {
			if len(shortIndex[back]) <= int(index) {
				format.Throw(fmt.Errorf("deck: invalid card index %d for short code type %d", index, back))
			}

			deck = append(deck, shortIndex[back][index])
		}

		cardIndex(2, b[0]>>2)
		cardIndex(1, ((b[0]&3)<<3)|(b[1]>>5))
		cardIndex(1, b[1]&31)

		b = b[2:]
		for len(b) > 3 {
			cardIndex(0, b[0]>>2)
			cardIndex(0, ((b[0]&3)<<4)|(b[1]>>4))
			cardIndex(0, ((b[1]&15)<<2)|(b[2]>>6))
			cardIndex(0, b[2]&63)
			b = b[3:]
		}

		cardIndex(0, b[0]>>2)

		if len(b) >= 2 {
			cardIndex(0, ((b[0]&3)<<4)|(b[1]>>4))

			if len(b) >= 3 {
				cardIndex(0, ((b[1]&15)<<2)|(b[2]>>6))

				if b[2]&63 != 63 {
					cardIndex(0, b[2]&63)
				}
			}
		}
	case b[0] == 0x80:
		for r.Len() != 0 {
			var c uint8

			r.Read(&c)
			deck = append(deck, ID(c))
		}
	case b[0] == 0x81:
		for r.Len() != 0 {
			c := r.UVarInt()
			deck = append(deck, ID(c))
		}
	case b[0] == 0x82:
		for r.Len() != 0 {
			c := r.UVarInt()
			deck = append(deck, ID(c+128))
		}
	default:
		return fmt.Errorf("deck: invalid first byte of deck code: %02x", b[0])
	}

	*d = deck

	return nil
}

var shortIndex = func() (index [3][]ID) {
	for i, filter := range []func(Rank) bool{
		func(r Rank) bool { return r == Attacker || r == Effect },
		func(r Rank) bool { return r == MiniBoss },
		func(r Rank) bool { return r == Boss },
	} {
		found := true
		for found {
			found = false
			for id := ID(0); id < 128; id++ {
				if filter(id.Rank()) && id.BasicIndex() == len(index[i]) {
					found = true
					index[i] = append(index[i], id)

					break
				}
			}
		}
	}

	return
}()

// VanillaOrder returns the order of default cards for a specified back design.
func VanillaOrder(back Rank) []ID {
	switch back {
	case Enemy:
		return vanillaOrder[0]
	case MiniBoss:
		return vanillaOrder[1]
	case Boss:
		return vanillaOrder[2]
	default:
		return nil
	}
}

// UnknownDeck is a Deck that only knows the backs of the cards within it.
type UnknownDeck []Rank

// MarshalBinary implements encoding.BinaryMarshaler.
func (ud UnknownDeck) MarshalBinary() ([]byte, error) {
	useShort := true

	for _, r := range ud {
		if r != Enemy && r != MiniBoss && r != Boss {
			useShort = false

			break
		}
	}

	if useShort {
		b := make([]byte, (len(ud)+3)>>2)

		for i, back := range ud {
			var x uint8

			switch back {
			case Enemy:
				x = 0
			case MiniBoss:
				x = 1
			case Boss:
				x = 2
			default:
				panic(fmt.Errorf("card: unexpected card back type at index %d: %v", i, back))
			}

			switch i & 3 {
			case 0:
				b[i>>2] = x << 6
			case 1:
				b[i>>2] |= x << 4
			case 2:
				b[i>>2] |= x << 2
			case 3:
				b[i>>2] |= x
			}
		}

		switch len(ud) & 3 {
		case 1:
			b[len(b)-1] |= 0x3f
		case 2:
			b[len(b)-1] |= 0xf
		case 3:
			b[len(b)-1] |= 0x3
		}

		return b, nil
	}

	b := make([]byte, 1+(len(ud)*3+7)/8)
	b[0] = 0xc0

	i, bits := 0, 0

	for _, r := range ud {
		if bits < 3 {
			b[i] |= uint8(r >> (3 - bits))
			bits += 8
			i++
		}

		bits -= 3
		b[i] |= uint8(r << bits)
	}

	b[i] |= 1<<bits - 1

	return b, nil
}

// UnmarshalBinary implements encoding.BinaryUnmarshaler.
func (ud *UnknownDeck) UnmarshalBinary(b []byte) error {
	if len(b) == 0 || b[0]>>6 != 3 {
		d := make(UnknownDeck, 0, len(b)<<2)

		for i, x := range b {
			for shift := 6; shift >= 0; shift -= 2 {
				switch (x >> shift) & 3 {
				case 0:
					d = append(d, Enemy)
				case 1:
					d = append(d, MiniBoss)
				case 2:
					d = append(d, Boss)
				case 3:
					if i != len(b)-1 {
						return fmt.Errorf("card: invalid unknown deck: ends at index %d of %d", i, len(b))
					}

					for shift2 := shift; shift2 >= 0; shift2 -= 2 {
						if v := (x >> shift2) & 3; v != 3 {
							return fmt.Errorf("card: invalid unknown deck: ends before non-padding value %d", v)
						}
					}

					*ud = d

					return nil
				}
			}
		}

		*ud = d

		return nil
	}

	switch b[0] {
	case 0xc0:
		d := make([]Rank, 0, (len(b)-1)*3/8)

		i, bits, count := 1, uint16(0), 0

		for {
			if count < 3 {
				if i == len(b) {
					if bits != ^uint16(0)<<(16-count) {
						return fmt.Errorf("card: unexpected padding at end of unknown deck: %0[2]*[1]b", bits>>(8-count), count)
					}

					*ud = d

					return nil
				}

				bits |= uint16(b[i]) << (8 - count)
				count += 8
				i++
			}

			r := Rank(bits >> 13)
			if r == RankNone {
				if count >= 8 || i != len(b) || bits != ^uint16(0)<<(16-count) {
					return errors.New("card: unexpected RankNone in unknown deck")
				}

				*ud = d

				return nil
			}

			count -= 3
			bits <<= 3

			d = append(d, r)
		}
	default:
		return fmt.Errorf("card: invalid unknown deck: unknown version %02X", b[0])
	}
}
