package card_test

import (
	"testing"

	"git.lubar.me/ben/spy-cards/card"
)

func TestVanilla(t *testing.T) {
	t.Parallel()

	for i := card.BugFables105; i <= card.BugFables11; i++ {
		v := i

		for j := 0; j < 256; j++ {
			id := card.ID(j)

			t.Run(v.String()+"/"+id.String(), func(t *testing.T) {
				t.Parallel()

				// this will panic if there is a decoding error
				_ = card.Vanilla(v, id)
			})
		}
	}
}
