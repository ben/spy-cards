//go:generate stringer -type FilterType -trimprefix Filter

package card

import (
	"errors"
	"fmt"
	"strconv"

	"git.lubar.me/ben/spy-cards/format"
)

// Filter is a card filter, introduced in card format 5.
type Filter []FilterComponent

// FilterType is a type of card filter.
type FilterType uint8

// Constants for FilterType.
const (
	FilterSingleCard FilterType = 1
	FilterTribe      FilterType = 16
	FilterNotTribe   FilterType = 32
	FilterRank       FilterType = 48
	FilterTP         FilterType = 64
	FilterTarget     FilterType = 65
)

// FilterComponent is a single component of a card filter.
type FilterComponent struct {
	Type        FilterType
	Rank        Rank
	Tribe       Tribe
	CustomTribe string
	CardID      ID
	TP          int64
}

func (fc FilterComponent) String() string {
	switch fc.Type {
	case FilterSingleCard:
		return fc.Type.String() + ":" + fc.CardID.String()
	case FilterTribe, FilterNotTribe:
		tribe := fc.CustomTribe
		if fc.Tribe != TribeCustom {
			tribe = fc.Tribe.String()
		}

		return fc.Type.String() + ":" + tribe
	case FilterRank:
		return fc.Type.String() + ":" + fc.Rank.String()
	case FilterTP:
		return fc.Type.String() + ":" + strconv.FormatInt(fc.TP, 10)
	case FilterTarget:
		return fc.Type.String()
	default:
		panic("internal error: unhandled filter component type in FilterComponent.String: " + fc.Type.String())
	}
}

// Marshal encodes the card filter.
func (f *Filter) Marshal(w *format.Writer) error {
	if len(*f) == 1 && (*f)[0].Type == FilterSingleCard {
		w.Write(FilterSingleCard)
		w.UVarInt(uint64((*f)[0].CardID))

		return nil
	}

	if len(*f) == 1 && (*f)[0].Type == FilterTarget {
		w.Write(FilterTarget)

		return nil
	}

	for _, e := range *f {
		switch e.Type {
		case FilterTribe, FilterNotTribe:
			w.Write(uint8(e.Type) | uint8(e.Tribe))

			if e.Tribe == TribeCustom {
				w.String(e.CustomTribe)
			} else if e.Tribe >= 15 {
				return fmt.Errorf("card: invalid filter tribe: %v", e.Tribe)
			}
		case FilterRank:
			w.Write(uint8(e.Type) | uint8(e.Rank))

			if e.Rank >= 6 {
				return fmt.Errorf("card: invalid filter rank: %v", e.Rank)
			}
		case FilterTP:
			w.Write(uint8(e.Type))

			w.SVarInt(e.TP)
		default:
			return fmt.Errorf("card: invalid filter component type: %v", e.Type)
		}
	}

	w.Write(uint8(0))

	return nil
}

// Unmarshal decodes a card filter.
func (f *Filter) Unmarshal(r *format.Reader) (err error) {
	defer format.Catch(&err)

	var filter Filter

	for {
		switch ty := FilterType(r.Byte()); {
		case ty == 0:
			*f = filter

			return nil
		case ty == FilterSingleCard:
			if filter != nil {
				return errors.New("card: SingleCard cannot be a filter component")
			}

			*f = []FilterComponent{
				{
					Type:   FilterSingleCard,
					CardID: ID(r.UVarInt()),
				},
			}

			return nil
		case ty&0xf0 == FilterTribe || ty&0xf0 == FilterNotTribe:
			filter = append(filter, FilterComponent{
				Type:  ty & 0xf0,
				Tribe: Tribe(ty & 0xf),
			})

			switch filter[len(filter)-1].Tribe {
			case TribeCustom:
				filter[len(filter)-1].CustomTribe = r.String()
			case TribeNone:
				return fmt.Errorf("card: invalid filter tribe: %v", filter[len(filter)-1].Tribe)
			}
		case ty&0xf8 == FilterRank:
			filter = append(filter, FilterComponent{
				Type: ty & 0xf8,
				Rank: Rank(ty & 0x7),
			})

			if filter[len(filter)-1].Rank > Enemy {
				return fmt.Errorf("card: invalid filter rank: %v", filter[len(filter)-1].Rank)
			}
		case ty == FilterTP:
			filter = append(filter, FilterComponent{
				Type: FilterTP,
				TP:   r.SVarInt(),
			})
		case ty == FilterTarget:
			if filter != nil {
				return errors.New("card: SingleCard cannot be a filter component")
			}

			*f = []FilterComponent{
				{
					Type: FilterTarget,
				},
			}

			return nil
		}
	}
}

// IsSingleCard returns true if this filter is a single card filter.
func (f Filter) IsSingleCard() bool {
	return len(f) == 1 && f[0].Type == FilterSingleCard
}

// IsMatch returns true if the specified card matches this filter.
func (f Filter) IsMatch(card *Def, target ID) bool {
	if f.IsSingleCard() {
		return f[0].CardID == card.ID
	}

	if len(f) == 1 && f[0].Type == FilterTarget {
		return card.ID == target
	}

	anyRank, matchRank := false, false
	anyTP, matchTP := false, false

	for _, fc := range f {
		switch fc.Type {
		case FilterRank:
			anyRank = true

			if card.Rank == fc.Rank {
				matchRank = true
			}
		case FilterTribe:
			found := false

			for _, t := range card.Tribes {
				if t.Tribe == fc.Tribe && t.CustomName == fc.CustomTribe {
					found = true
				}
			}

			if !found {
				return false
			}
		case FilterNotTribe:
			for _, t := range card.Tribes {
				if t.Tribe == fc.Tribe && t.CustomName == fc.CustomTribe {
					return false
				}
			}
		case FilterTP:
			anyTP = true

			if card.TP == fc.TP {
				matchTP = true
			}
		}
	}

	return (!anyRank || matchRank) && (!anyTP || matchTP)
}
