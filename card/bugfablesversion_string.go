// Code generated by "stringer -type BugFablesVersion -linecomment"; DO NOT EDIT.

package card

import "strconv"

func _() {
	// An "invalid array index" compiler error signifies that the constant values have changed.
	// Re-run the stringer command to generate them again.
	var x [1]struct{}
	_ = x[BugFables105-0]
	_ = x[BugFables11-1]
	_ = x[BugFables111-2]
	_ = x[BugFables121-3]
}

const _BugFablesVersion_name = "Bug Fables 1.0.5Bug Fables 1.1Bug Fables 1.1.1Bug Fables 1.2.1"

var _BugFablesVersion_index = [...]uint8{0, 16, 30, 46, 62}

func (i BugFablesVersion) String() string {
	if i >= BugFablesVersion(len(_BugFablesVersion_index)-1) {
		return "BugFablesVersion(" + strconv.FormatInt(int64(i), 10) + ")"
	}
	return _BugFablesVersion_name[_BugFablesVersion_index[i]:_BugFablesVersion_index[i+1]]
}
