//go:generate stringer -type EffectType -trimprefix Effect

package card

import (
	"errors"
	"fmt"

	"git.lubar.me/ben/spy-cards/format"
)

// EffectType is an enumeration of effect types.
type EffectType uint8

// Constants for EffectType.
const (
	FlavorText                 EffectType = 0
	EffectStat                 EffectType = 1
	EffectEmpower              EffectType = 2
	EffectSummon               EffectType = 3
	EffectHeal                 EffectType = 4
	EffectTP                   EffectType = 5
	EffectNumb                 EffectType = 6
	EffectRawStat              EffectType = 7
	EffectMultiplyHealing      EffectType = 8
	EffectPreventNumb          EffectType = 9
	EffectModifyAvailableCards EffectType = 10
	EffectModifyCardCost       EffectType = 11
	EffectDrawCard             EffectType = 12
	EffectModifyGameRule       EffectType = 13
	EffectDelaySetup           EffectType = 14
	CondCard                   EffectType = 128
	CondLimit                  EffectType = 129
	CondWinner                 EffectType = 130
	CondApply                  EffectType = 131
	CondCoin                   EffectType = 132
	CondStat                   EffectType = 133
	CondInHand                 EffectType = 134
	CondLastEffect             EffectType = 135
	CondOnNumb                 EffectType = 136
	CondMultipleEffects        EffectType = 137
	CondOnDiscard              EffectType = 138
	CondCompareTargetCard      EffectType = 139
	CondOnExile                EffectType = 140
)

func (et EffectType) IsRoot() bool {
	return et == FlavorText || et == CondInHand || et == CondOnNumb || et == CondOnDiscard || et == CondOnExile
}

func (et EffectType) IsPassive() bool {
	return et == EffectTP || et == EffectModifyCardCost
}

func (et EffectType) IsCondition() bool {
	return et >= 128
}

// EffectDef is a Spy Cards Online card effect definition.
type EffectDef struct {
	Type     EffectType
	Priority uint8
	Flags    EffectFlag

	Text    string
	Amount  int64
	Amount2 int64
	Amount3 int64
	Filter  Filter

	Result  *EffectDef
	Result2 *EffectDef
}

// Complete returns false if the EffectDef expects a result but does not have
// a result.
func (e *EffectDef) Complete() bool {
	if e == nil {
		return false
	}

	if e.Result != nil && !e.Result.Complete() {
		return false
	}

	if e.Result2 != nil && !e.Result2.Complete() {
		return false
	}

	if !e.Type.IsCondition() {
		return true
	}

	if e.Result == nil {
		return false
	}

	if e.Type == CondMultipleEffects && e.Result2 == nil {
		return false
	}

	return true
}

// Marshal encodes the EffectDef in a binary format.
func (e *EffectDef) Marshal(w *format.Writer) (err error) {
	defer format.Catch(&err)

	switch e.Type {
	case EffectNumb:
		e.Flags &^= FlagNumbResult | FlagNumbResult2

		if e.Result != nil {
			e.Flags |= FlagNumbResult
		}

		if e.Result2 != nil {
			e.Flags |= FlagNumbResult2
		}
	case EffectModifyAvailableCards:
		e.Flags &^= FlagModifyCardsResult | FlagModifyCardsResult2 | FlagModifyCardsNamedGroup

		if e.Result != nil {
			e.Flags |= FlagModifyCardsResult
		}

		if e.Result2 != nil {
			e.Flags |= FlagModifyCardsResult2
		}

		if e.Text != "" {
			e.Flags |= FlagModifyCardsNamedGroup
		}
	case EffectDrawCard:
		e.Flags &^= FlagDrawCardUseFilter | FlagDrawCardResult | FlagDrawCardResult2

		if len(e.Filter) != 0 {
			e.Flags |= FlagDrawCardUseFilter
		}

		if e.Result != nil {
			e.Flags |= FlagDrawCardResult
		}

		if e.Result2 != nil {
			e.Flags |= FlagDrawCardResult2
		}
	case CondCoin:
		e.Flags &^= FlagCondCoinTails | FlagCondCoinWeighted

		if e.Result2 != nil {
			e.Flags |= FlagCondCoinTails
		}

		if e.Amount2 != e.Amount3 {
			e.Flags |= FlagCondCoinWeighted
		}
	}

	w.Write(e.Type)
	w.Write(e.Priority)
	w.UVarInt(uint64(e.Flags))

	switch e.Type {
	case FlavorText:
		if e.Flags&FlagFlavorTextCustomColor != 0 {
			w.Write(uint8(e.Amount))
			w.Write(uint8(e.Amount >> 8))
			w.Write(uint8(e.Amount >> 16))
		}

		w.String(e.Text)
	case EffectStat, EffectHeal, EffectTP, EffectRawStat, EffectMultiplyHealing:
		w.SVarInt(e.Amount)
	case EffectEmpower, EffectModifyCardCost:
		w.SVarInt(e.Amount)

		if err := e.Filter.Marshal(w); err != nil {
			return fmt.Errorf("in filter: %w", err)
		}
	case EffectSummon, EffectPreventNumb:
		w.UVarInt(uint64(e.Amount))

		if err := e.Filter.Marshal(w); err != nil {
			return fmt.Errorf("in filter: %w", err)
		}
	case EffectNumb:
		w.UVarInt(uint64(e.Amount))

		if err := e.Filter.Marshal(w); err != nil {
			return fmt.Errorf("in filter: %w", err)
		}

		if e.Flags&FlagNumbResult != 0 {
			if err := e.Result.Marshal(w); err != nil {
				return fmt.Errorf("in result 1: %w", err)
			}
		}

		if e.Flags&FlagNumbResult2 != 0 {
			if err := e.Result2.Marshal(w); err != nil {
				return fmt.Errorf("in result 2: %w", err)
			}
		}
	case EffectModifyAvailableCards:
		w.UVarInt(uint64(e.Amount))

		if e.Flags&FlagModifyCardsTypeMask == FlagModifyCardsTypeReplace {
			w.UVarInt(uint64(e.Amount3))
		}

		if err := e.Filter.Marshal(w); err != nil {
			return fmt.Errorf("in filter: %w", err)
		}

		if e.Flags&FlagModifyCardsResult != 0 {
			if err := e.Result.Marshal(w); err != nil {
				return fmt.Errorf("in result 1: %w", err)
			}
		}

		if e.Flags&FlagModifyCardsResult2 != 0 {
			if err := e.Result2.Marshal(w); err != nil {
				return fmt.Errorf("in result 2: %w", err)
			}
		}

		if e.Flags&FlagModifyCardsNamedGroup != 0 {
			w.String(e.Text)
		}
	case EffectDrawCard:
		w.SVarInt(e.Amount)

		if e.Flags&FlagDrawCardUseFilter != 0 {
			if err := e.Filter.Marshal(w); err != nil {
				return fmt.Errorf("in filter: %w", err)
			}
		}

		if e.Flags&FlagDrawCardResult != 0 {
			if err := e.Result.Marshal(w); err != nil {
				return fmt.Errorf("in result: %w", err)
			}
		}

		if e.Flags&FlagDrawCardResult2 != 0 {
			if err := e.Result2.Marshal(w); err != nil {
				return fmt.Errorf("in result 2: %w", err)
			}
		}
	case EffectModifyGameRule:
		w.UVarInt(uint64(e.Amount2))
		w.SVarInt(e.Amount)
	case EffectDelaySetup:
		w.SVarInt(e.Amount)

		if err := e.Filter.Marshal(w); err != nil {
			return fmt.Errorf("in filter: %w", err)
		}
	case CondWinner, CondApply, CondLastEffect, CondOnNumb:
		if err := e.Result.Marshal(w); err != nil {
			return fmt.Errorf("in result: %w", err)
		}
	case CondStat:
		w.SVarInt(e.Amount)

		if err := e.Result.Marshal(w); err != nil {
			return fmt.Errorf("in result: %w", err)
		}
	case CondLimit:
		w.UVarInt(uint64(e.Amount))

		if err := e.Result.Marshal(w); err != nil {
			return fmt.Errorf("in result: %w", err)
		}
	case CondCard:
		w.UVarInt(uint64(e.Amount))

		if err := e.Filter.Marshal(w); err != nil {
			return fmt.Errorf("in filter: %w", err)
		}

		if err := e.Result.Marshal(w); err != nil {
			return fmt.Errorf("in result: %w", err)
		}
	case CondCoin:
		w.UVarInt(uint64(e.Amount))

		if e.Flags&FlagCondCoinWeighted != 0 {
			w.UVarInt(uint64(e.Amount2 - 1))
			w.UVarInt(uint64(e.Amount3))
		}

		if err := e.Result.Marshal(w); err != nil {
			return fmt.Errorf("in heads result: %w", err)
		}

		if e.Flags&FlagCondCoinTails != 0 {
			if err := e.Result2.Marshal(w); err != nil {
				return fmt.Errorf("in tails result: %w", err)
			}
		}
	case CondInHand:
		w.UVarInt(uint64(e.Amount))
		w.UVarInt(uint64(e.Amount2))

		if err := e.Result.Marshal(w); err != nil {
			return fmt.Errorf("in result: %w", err)
		}
	case CondMultipleEffects:
		if err := e.Result.Marshal(w); err != nil {
			return fmt.Errorf("in result 1: %w", err)
		}

		if err := e.Result2.Marshal(w); err != nil {
			return fmt.Errorf("in result 2: %w", err)
		}
	case CondOnDiscard:
		if err := e.Result.Marshal(w); err != nil {
			return fmt.Errorf("in result: %w", err)
		}
	case CondCompareTargetCard:
		if err := e.Filter.Marshal(w); err != nil {
			return fmt.Errorf("in filter: %w", err)
		}

		if err := e.Result.Marshal(w); err != nil {
			return fmt.Errorf("in result: %w", err)
		}
	case CondOnExile:
		if e.Flags&FlagCondOnExileGroup != 0 {
			w.String(e.Text)
		}

		if err := e.Result.Marshal(w); err != nil {
			return fmt.Errorf("in result: %w", err)
		}
	default:
		return fmt.Errorf("card: unhandled effect type: %v", e.Type)
	}

	return nil
}

// Unmarshal decodes the EffectDef from a binary format.
func (e *EffectDef) Unmarshal(r *format.Reader, formatVersion uint64) (err error) {
	defer format.Catch(&err)

	switch formatVersion {
	case 0, 1:
		return e.unmarshalV1(r, formatVersion)
	case 2, 4:
		return e.unmarshalV4(r, formatVersion)
	case 5:
		break
	default:
		return fmt.Errorf("card: unhandled effect version: %d", formatVersion)
	}

	r.Read(&e.Type)
	r.Read(&e.Priority)
	e.Flags = EffectFlag(r.UVarInt())

	e.Text = ""
	e.Amount = 0
	e.Amount2 = 0
	e.Amount3 = 0
	e.Filter = nil
	e.Result = nil
	e.Result2 = nil

	switch e.Type {
	case FlavorText:
		if e.Flags&FlagFlavorTextCustomColor != 0 {
			red := r.Byte()
			green := r.Byte()
			blue := r.Byte()

			e.Amount = int64(red) | (int64(green) << 8) | (int64(blue) << 16)
		}

		e.Text = r.String()
	case EffectStat, EffectHeal, EffectTP, EffectRawStat, EffectMultiplyHealing:
		e.Amount = r.SVarInt()
	case EffectEmpower, EffectModifyCardCost:
		e.Amount = r.SVarInt()

		if err := e.Filter.Unmarshal(r); err != nil {
			return fmt.Errorf("in filter: %w", err)
		}
	case EffectSummon, EffectPreventNumb:
		e.Amount = int64(r.UVarInt())

		if err := e.Filter.Unmarshal(r); err != nil {
			return fmt.Errorf("in filter: %w", err)
		}
	case EffectNumb:
		e.Amount = int64(r.UVarInt())

		if err := e.Filter.Unmarshal(r); err != nil {
			return fmt.Errorf("in filter: %w", err)
		}

		if e.Flags&FlagNumbResult != 0 {
			e.Result = &EffectDef{}

			if err := e.Result.Unmarshal(r, formatVersion); err != nil {
				return fmt.Errorf("in result: %w", err)
			}
		}

		if e.Flags&FlagNumbResult2 != 0 {
			e.Result2 = &EffectDef{}

			if err := e.Result2.Unmarshal(r, formatVersion); err != nil {
				return fmt.Errorf("in result 2: %w", err)
			}
		}
	case EffectModifyAvailableCards:
		e.Amount = int64(r.UVarInt())

		if e.Flags&FlagModifyCardsTypeMask == FlagModifyCardsTypeReplace {
			e.Amount3 = int64(r.UVarInt())
		}

		if err := e.Filter.Unmarshal(r); err != nil {
			return fmt.Errorf("in filter: %w", err)
		}

		if e.Flags&FlagModifyCardsResult != 0 {
			e.Result = &EffectDef{}

			if err := e.Result.Unmarshal(r, formatVersion); err != nil {
				return fmt.Errorf("in success effect: %w", err)
			}
		}

		if e.Flags&FlagModifyCardsResult2 != 0 {
			e.Result2 = &EffectDef{}

			if err := e.Result2.Unmarshal(r, formatVersion); err != nil {
				return fmt.Errorf("in failure effect: %w", err)
			}
		}

		if e.Flags&FlagModifyCardsNamedGroup != 0 {
			e.Text = r.String()
		}
	case EffectDrawCard:
		e.Amount = r.SVarInt()
		if e.Flags&FlagDrawCardUseFilter != 0 {
			if err := e.Filter.Unmarshal(r); err != nil {
				return fmt.Errorf("in filter: %w", err)
			}
		}

		if e.Flags&FlagDrawCardResult != 0 {
			e.Result = &EffectDef{}
			if err := e.Result.Unmarshal(r, formatVersion); err != nil {
				return fmt.Errorf("in result: %w", err)
			}
		}

		if e.Flags&FlagDrawCardResult2 != 0 {
			e.Result2 = &EffectDef{}
			if err := e.Result2.Unmarshal(r, formatVersion); err != nil {
				return fmt.Errorf("in result 2: %w", err)
			}
		}
	case EffectModifyGameRule:
		e.Amount2 = int64(r.UVarInt())
		e.Amount = r.SVarInt()
	case EffectDelaySetup:
		e.Amount = r.SVarInt()

		if err := e.Filter.Unmarshal(r); err != nil {
			return fmt.Errorf("in filter: %w", err)
		}
	case CondWinner, CondApply, CondLastEffect, CondOnNumb, CondOnDiscard:
		e.Result = &EffectDef{}

		if err := e.Result.Unmarshal(r, formatVersion); err != nil {
			return fmt.Errorf("in result: %w", err)
		}
	case CondStat:
		e.Amount = r.SVarInt()
		e.Result = &EffectDef{}

		if err := e.Result.Unmarshal(r, formatVersion); err != nil {
			return fmt.Errorf("in result: %w", err)
		}
	case CondLimit:
		e.Amount = int64(r.UVarInt())
		e.Result = &EffectDef{}

		if err := e.Result.Unmarshal(r, formatVersion); err != nil {
			return fmt.Errorf("in result: %w", err)
		}
	case CondCard:
		e.Amount = int64(r.UVarInt())

		if err := e.Filter.Unmarshal(r); err != nil {
			return fmt.Errorf("in filter: %w", err)
		}

		e.Result = &EffectDef{}

		if err := e.Result.Unmarshal(r, formatVersion); err != nil {
			return fmt.Errorf("in result: %w", err)
		}
	case CondCoin:
		e.Amount = int64(r.UVarInt())

		if e.Flags&FlagCondCoinWeighted != 0 {
			e.Amount2 = int64(r.UVarInt()) + 1
			e.Amount3 = int64(r.UVarInt())
		} else {
			e.Amount2 = 1
			e.Amount3 = 1
		}

		e.Result = &EffectDef{}

		if err := e.Result.Unmarshal(r, formatVersion); err != nil {
			return fmt.Errorf("in heads result: %w", err)
		}

		if e.Flags&FlagCondCoinTails != 0 {
			e.Result2 = &EffectDef{}

			if err := e.Result2.Unmarshal(r, formatVersion); err != nil {
				return fmt.Errorf("in tails result: %w", err)
			}
		}
	case CondInHand:
		e.Amount = int64(r.UVarInt())
		e.Amount2 = int64(r.UVarInt())
		e.Result = &EffectDef{}

		if err := e.Result.Unmarshal(r, formatVersion); err != nil {
			return fmt.Errorf("in result: %w", err)
		}
	case CondMultipleEffects:
		e.Result = &EffectDef{}
		e.Result2 = &EffectDef{}

		if err := e.Result.Unmarshal(r, formatVersion); err != nil {
			return fmt.Errorf("in result 1: %w", err)
		}

		if err := e.Result2.Unmarshal(r, formatVersion); err != nil {
			return fmt.Errorf("in result 2: %w", err)
		}
	case CondCompareTargetCard:
		e.Result = &EffectDef{}

		if err := e.Filter.Unmarshal(r); err != nil {
			return fmt.Errorf("in filter: %w", err)
		}

		if err := e.Result.Unmarshal(r, formatVersion); err != nil {
			return fmt.Errorf("in result: %w", err)
		}
	case CondOnExile:
		if e.Flags&FlagCondOnExileGroup != 0 {
			e.Text = r.String()
		}

		e.Result = &EffectDef{}

		if err := e.Result.Unmarshal(r, formatVersion); err != nil {
			return fmt.Errorf("in result: %w", err)
		}
	default:
		return fmt.Errorf("card: unhandled effect type: %v", e.Type)
	}

	var maxFlag int

	switch e.Type {
	case FlavorText:
		maxFlag = 1
	case EffectStat:
		maxFlag = 1
	case EffectEmpower:
		maxFlag = 2
	case EffectSummon:
		maxFlag = 2
	case EffectHeal:
		maxFlag = 2
	case EffectTP:
		maxFlag = 0
	case EffectNumb:
		maxFlag = 9
	case EffectRawStat:
		maxFlag = 2
	case EffectMultiplyHealing:
		maxFlag = 3

		if e.Flags&FlagMultiplyHealingTypeMask == FlagMultiplyHealingTypeMask {
			return errors.New("card: invalid flag value for MultiplyHealing")
		}
	case EffectPreventNumb:
		maxFlag = 0
	case EffectModifyAvailableCards:
		maxFlag = 9

		if e.Flags&FlagModifyCardsTypeMask == FlagModifyCardsTypeReplace && e.Flags&FlagModifyCardsRequireMask != FlagModifyCardsRequireNone {
			return errors.New("card: invalid flag value for ModifyAvailableCards")
		}

		if e.Flags&(FlagModifyCardsTypeMask|FlagModifyCardsTargetMask) == FlagModifyCardsTypeMove|FlagModifyCardsTargetField {
			return errors.New("card: invalid flag value for ModifyAvailableCards")
		}

		if e.Flags&FlagModifyCardsRequireMask == FlagModifyCardsRequireMask {
			return errors.New("card: invalid flag value for ModifyAvailableCards")
		}
	case EffectModifyCardCost:
		maxFlag = 1
	case EffectDrawCard:
		maxFlag = 4
	case EffectModifyGameRule:
		maxFlag = 2

		if e.Flags&FlagModifyGameRulePlayerMask == FlagModifyGameRulePlayerMask {
			return errors.New("card: invalid flag value for ModifyGameRule")
		}
	case EffectDelaySetup:
		maxFlag = 1
	case CondCard:
		maxFlag = 5

		if e.Flags&FlagCondCardTypeMask == FlagCondCardTypeMask {
			return errors.New("card: invalid flag value for CondCard")
		}

		if e.Flags&FlagCondCardLocationMask == FlagCondCardLocationMask {
			return errors.New("card: invalid flag value for CondCard")
		}
	case CondLimit:
		maxFlag = 0
	case CondWinner:
		maxFlag = 2

		if e.Flags&FlagCondWinnerTypeMask > FlagCondWinnerTypeNotLoser {
			return errors.New("card: invalid flag value for CondWinner")
		}
	case CondApply:
		maxFlag = 3

		if e.Flags&FlagCondApplyShowMask == FlagCondApplyShowMask {
			return errors.New("card: invalid flag value for CondApply")
		}
	case CondCoin:
		maxFlag = 3
	case CondStat:
		maxFlag = 9

		if e.Flags&FlagCondStatMathMask == FlagCondStatMathMask {
			return errors.New("card: invalid flag value for CondStat")
		}
	case CondInHand:
		maxFlag = 2

		if e.Flags&FlagCondInHandOnPlayMask == FlagCondInHandOnPlayMask {
			return errors.New("card: invalid flag value for CondInHand")
		}
	case CondLastEffect:
		maxFlag = -1
	case CondOnNumb:
		maxFlag = -1
	case CondMultipleEffects:
		maxFlag = -1
	case CondOnDiscard:
		maxFlag = -1
	case CondCompareTargetCard:
		maxFlag = 0
	case CondOnExile:
		maxFlag = 0
	default:
		return fmt.Errorf("card: unhandled effect type for flags: %v", e.Type)
	}

	if e.Flags & ^(1<<(maxFlag+1)-1) != 0 {
		return fmt.Errorf("card: invalid flag value for %v", e.Type)
	}

	return nil
}

// UpdateID changes all references to oldID to newID.
func (e *EffectDef) UpdateID(oldID, newID ID) {
	if e.Filter.IsSingleCard() && e.Filter[0].CardID == oldID {
		e.Filter[0].CardID = newID
	}

	if e.Result != nil {
		e.Result.UpdateID(oldID, newID)
	}

	if e.Result2 != nil {
		e.Result2.UpdateID(oldID, newID)
	}
}
