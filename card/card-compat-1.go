package card

import (
	"errors"
	"fmt"

	"git.lubar.me/ben/spy-cards/format"
)

var (
	errNotExternalPortrait = errors.New("card: external portrait bit is set but portrait is not custom")
	errNoneTribe           = errors.New("card: (none) tribe cannot be used in this location")
)

func (cd *Def) unmarshalV1(r *format.Reader, loc *string, formatVersion uint64) (Def, error) {
	var card Def

	var basic struct {
		ID       uint8
		Tribes   Tribe
		RankTP   uint8
		Portrait uint8
	}

	*loc = "reading basic card data (v1)"

	r.Read(&basic)

	card.ID = ID(basic.ID)
	card.Rank = card.ID.Rank()

	if basic.Tribes>>4 == TribeNone {
		return Def{}, errNoneTribe
	}

	if basic.Tribes&15 == TribeNone {
		card.Tribes = make([]TribeDef, 1)
	} else {
		card.Tribes = make([]TribeDef, 2)
		card.Tribes[1].Tribe = basic.Tribes & 15
	}

	card.Tribes[0].Tribe = basic.Tribes >> 4

	customPortraitID := PortraitCustomEmbedded
	if basic.RankTP&0x40 != 0 {
		customPortraitID = PortraitCustomExternal
	}

	codeRank := Rank(basic.RankTP>>4) & 3
	if actualRank := card.ID.Rank(); actualRank != codeRank {
		return Def{}, fmt.Errorf("card: expected rank %v but card code specifies %v", actualRank, codeRank)
	}

	switch tp := basic.RankTP & 15; {
	case tp <= 10:
		card.TP = int64(tp)
		card.legacyUnpickable = false
	case tp == 15:
		card.TP = 1
		card.legacyUnpickable = true
	default:
		return Def{}, fmt.Errorf("card: invalid value for TP byte: %02x", basic.RankTP)
	}

	if basic.Portrait > 234 && basic.Portrait != PortraitCustomExternal {
		return Def{}, fmt.Errorf("card: invalid value for portrait byte: %02x", basic.Portrait)
	}

	card.Portrait = basic.Portrait

	switch {
	case card.Portrait == PortraitCustomExternal:
		card.Portrait = customPortraitID
	case customPortraitID == PortraitCustomExternal:
		return Def{}, errNotExternalPortrait
	case card.Portrait >= 228 && card.Portrait <= 235:
		card.Portrait += 19
	}

	*loc = "reading card name (v1)"

	card.Name = r.String1()

	*loc = "reading card effects (v1)"

	if formatVersion == 0 && card.ID.Rank() == Attacker && (r.Len() == 0 || r.Peek(1)[0] == 137) {
		card.Effects = []*EffectDef{
			{
				Type:   EffectStat,
				Amount: card.TP,
			},
		}

		if card.legacyUnpickable {
			card.Effects[0].Flags = 2
		}
	} else {
		var effectCount uint8
		r.Read(&effectCount)
		effectBuf := make([]EffectDef, effectCount)
		card.Effects = make([]*EffectDef, effectCount)
		for i := range card.Effects {
			card.Effects[i] = &effectBuf[i]
			if err := card.Effects[i].Unmarshal(r, formatVersion); err != nil {
				return Def{}, fmt.Errorf("card: decoding effect %d: %w", i, err)
			}
		}
	}

	*loc = "reading card tribes (v1)"

	if len(card.Tribes) >= 1 && card.Tribes[0].Tribe == TribeCustom {
		r.Read(&card.Tribes[0].Red)
		r.Read(&card.Tribes[0].Green)
		r.Read(&card.Tribes[0].Blue)
		card.Tribes[0].CustomName = r.String1()
	}

	if len(card.Tribes) >= 2 && card.Tribes[1].Tribe == TribeCustom {
		r.Read(&card.Tribes[1].Red)
		r.Read(&card.Tribes[1].Green)
		r.Read(&card.Tribes[1].Blue)
		card.Tribes[1].CustomName = r.String1()
	}

	*loc = "reading card custom portrait (v1)"

	if card.Portrait == PortraitCustomEmbedded || card.Portrait == PortraitCustomExternal {
		card.CustomPortrait = r.Bytes(r.Len())
	} else {
		card.CustomPortrait = nil
	}

	card.Extensions = make([]*Extension, 0)

	return card, nil
}
