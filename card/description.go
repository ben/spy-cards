package card

import (
	"sort"
	"strconv"
	"strings"

	"git.lubar.me/ben/spy-cards/gfx/sprites"
)

// Infinity symbol strings.
const (
	PosInf = "∞"
	NegInf = "-∞"
)

// RichDescription is a segment of a Spy Cards card description.
type RichDescription struct {
	Effect *EffectDef

	Text    string
	Content []*RichDescription

	// Color overrides the parent's color if it is non-zero.
	Color sprites.Color
}

// Typeset arranges the text in an area for rendering.
func (d *RichDescription) Typeset(font sprites.FontID, x, y, z, sx, sy, spaceX, spaceY float32, effect *EffectDef, center bool, render func(x, y, z, sx, sy float32, text string, tint sprites.Color, isEffect bool)) {
	if spaceY == 0 {
		// horizontal squish
		var width float32

		for _, l := range d.String() {
			width += sprites.TextAdvance(font, l, sx)
		}

		if width > spaceX {
			sx *= spaceX / width

			if center {
				x -= spaceX / 2
			}
		} else if center {
			x -= width / 2
		}

		d.walk(func(text string, tint sprites.Color, isEffect bool) {
			render(x, y, z, sx, sy, text, tint, isEffect)

			for _, l := range text {
				x += sprites.TextAdvance(font, l, sx)
			}
		}, d.Color, false, effect)

		return
	}

	// vertical squish
	var (
		height, width, lastSpace float32
		x0                       []float32
		scale                    []float32
		newLines                 []int
		lastSpaceBytes           int
	)

	for i, l := range d.String() {
		width += sprites.TextAdvance(font, l, sx)

		if l == ' ' {
			lastSpace = width
			lastSpaceBytes = i + 1
		}

		if l == '\n' {
			if center {
				x0 = append(x0, x-width/2)
			} else {
				x0 = append(x0, x)
			}

			if width > spaceX {
				scale = append(scale, sx*spaceX/width)
			} else {
				scale = append(scale, sx)
			}

			width = 0
			lastSpace = 0
			height += sy * 0.7

			newLines = append(newLines, i+1)
		}

		if width > spaceX && lastSpace != 0 {
			if center {
				x0 = append(x0, x-(lastSpace-0.3*sx)/2)
			} else {
				x0 = append(x0, x)
			}

			if width > spaceX {
				scale = append(scale, sx*spaceX/width)
			} else {
				scale = append(scale, sx)
			}

			width -= lastSpace
			lastSpace = 0
			height += sy * 0.7

			newLines = append(newLines, lastSpaceBytes)
		}
	}

	if width != 0 {
		height += sy * 0.7

		if center {
			x0 = append(x0, x-width/2)
		} else {
			x0 = append(x0, x)
		}

		if width > spaceX {
			scale = append(scale, sx*spaceX/width)
		} else {
			scale = append(scale, sx)
		}
	}

	if height > spaceY {
		y += 0.5 * sy
		sy *= spaceY / height
		y -= 0.5 * sy
	}

	if width > spaceX {
		scale = append(scale, sx*spaceX/width)
	} else {
		scale = append(scale, sx)
	}

	x0 = append(x0, x)
	y0 := y
	i := 0

	d.walk(func(text string, tint sprites.Color, isEffect bool) {
		for len(newLines) != 0 && newLines[0] < i+len(text) {
			render(x0[0], y0, z, scale[0], sy, text[:newLines[0]-i], tint, isEffect)

			x0 = x0[1:]
			scale = scale[1:]
			y0 -= 0.7 * sy

			text = text[newLines[0]-i:]
			i = newLines[0]
			newLines = newLines[1:]
		}

		render(x0[0], y0, z, scale[0], sy, text, tint, isEffect)

		for _, l := range text {
			x0[0] += sprites.TextAdvance(font, l, scale[0])
		}

		i += len(text)
	}, d.Color, false, effect)
}

func (d *RichDescription) walk(f func(string, sprites.Color, bool), tint sprites.Color, isEffect bool, effect *EffectDef) {
	if d.Color != (sprites.Color{}) {
		tint = d.Color
	}

	if d.Effect == effect {
		isEffect = true
	}

	f(d.Text, tint, isEffect)

	for _, c := range d.Content {
		c.walk(f, tint, isEffect, effect)
	}
}

// String implements fmt.Stringer.
func (d *RichDescription) String() string {
	return string(d.string(nil))
}

func (d *RichDescription) string(b []byte) []byte {
	b = append(b, d.Text...)

	for _, c := range d.Content {
		b = c.string(b)
	}

	return b
}

// Description returns a description of this card.
func (cd *Def) Description(set *Set) *RichDescription {
	desc := &RichDescription{Color: sprites.Black}

	for _, e := range cd.Effects {
		effectDesc := e.Description(cd, set)

		if len(desc.Content) != 0 && len(effectDesc) != 0 {
			desc.Content = append(desc.Content, &RichDescription{Text: "\n"})
		}

		desc.Content = append(desc.Content, effectDesc...)

		if e != nil && e.Type == FlavorText && e.Flags&FlagFlavorTextHideRemaining != 0 {
			break
		}
	}

	return desc
}

// Description returns a description of this effect.
func (e *EffectDef) Description(card *Def, set *Set, parents ...*EffectDef) []*RichDescription {
	for _, f := range [...]func(e *EffectDef, card *Def, set *Set, parents []*EffectDef) []*RichDescription{
		(*EffectDef).describeUnselected, // must be first
		(*EffectDef).describeEffectFreeze,
		(*EffectDef).describeEffectPierce,
		(*EffectDef).describeCondPay,
		(*EffectDef).describeEffectUnity,
		(*EffectDef).describeEffectLosePerRound,
		(*EffectDef).describeEffectLifesteal,
		(*EffectDef).describeEffectPassive,
		(*EffectDef).describeEffectReveal,
	} {
		if d := f(e, card, set, parents); d != nil {
			return d
		}
	}

	switch e.Type {
	case FlavorText:
		return e.describeFlavorText()
	case EffectStat:
		return e.describeEffectStat(parents)
	case EffectEmpower:
		return e.describeEffectEmpower(set)
	case EffectSummon:
		return e.describeEffectSummon(card, set)
	case EffectHeal:
		return e.describeEffectHeal()
	case EffectTP:
		return e.describeEffectTP()
	case EffectNumb:
		return e.describeEffectNumb(card, set, parents)
	case EffectRawStat:
		return e.describeEffectRawStat()
	case EffectMultiplyHealing:
		return e.describeEffectMultiplyHealing()
	case EffectPreventNumb:
		return e.describeEffectPreventNumb(set)
	case EffectModifyAvailableCards:
		return e.describeEffectModifyAvailableCards(card, set, parents)
	case EffectModifyCardCost:
		return e.describeEffectModifyCardCost(set)
	case EffectDrawCard:
		return e.describeEffectDrawCard(card, set, parents)
	case EffectModifyGameRule:
		return e.describeEffectModifyGameRule()
	case EffectDelaySetup:
		return e.describeEffectDelaySetup(set)
	case CondCard:
		return e.describeCondCard(card, set, parents)
	case CondLimit:
		return e.describeCondLimit(card, set, parents)
	case CondWinner:
		return e.describeCondWinner(card, set, parents)
	case CondApply:
		return e.describeCondApply(card, set, parents)
	case CondCoin:
		return e.describeCondCoin(card, set, parents)
	case CondStat:
		return e.describeCondStat(card, set, parents)
	case CondInHand:
		return e.describeCondInHand(card, set, parents)
	case CondLastEffect:
		return e.describeCondLastEffect(card, set, parents)
	case CondOnNumb:
		return e.describeCondOnNumb(card, set, parents)
	case CondMultipleEffects:
		return e.describeCondMultipleEffects(card, set, parents)
	case CondOnDiscard:
		return e.describeCondOnDiscard(card, set, parents)
	case CondCompareTargetCard:
		return e.describeCondCompareTargetCard(card, set, parents)
	case CondOnExile:
		return e.describeCondOnExile(card, set, parents)
	}

	return []*RichDescription{
		{
			Effect: e,
			Text:   "ERROR: unhandled effect type " + e.Type.String(),
			Color:  sprites.Red,
		},
	}
}

func (e *EffectDef) isNull() bool {
	if e == nil {
		return false
	}

	if e.Type == CondStat || e.Type == EffectRawStat || e.Type == EffectEmpower || e.Type == EffectHeal {
		return e.Amount == 0
	}

	if e.Type == EffectMultiplyHealing {
		return e.Amount == 1
	}

	return false
}

func (e *EffectDef) describeAmount(infFlag EffectFlag) string {
	if e.Flags&infFlag != 0 {
		if e.Amount < 0 {
			return NegInf
		}

		return PosInf
	}

	return strconv.FormatInt(e.Amount, 10)
}

func (e *EffectDef) describeCardName(set *Set, id ID) string {
	if c := set.Card(id); c != nil {
		return c.DisplayName()
	}

	return "MissingCard?" + strconv.FormatUint(uint64(id), 10) + "?"
}

func (e *EffectDef) describeFilter(set *Set) string {
	if e.Filter.IsSingleCard() {
		return "Card " + e.describeCardName(set, e.Filter[0].CardID)
	}

	if len(e.Filter) == 1 && e.Filter[0].Type == FilterTarget {
		return "Target Card"
	}

	var (
		tribes []string
		ranks  uint64
		tp     []int64
	)

	for _, f := range e.Filter {
		switch f.Type {
		case FilterTribe:
			if f.Tribe == TribeCustom {
				tribes = append(tribes, f.CustomTribe)
			} else {
				tribes = append(tribes, f.Tribe.String())
			}
		case FilterNotTribe:
			if f.Tribe == TribeCustom {
				tribes = append(tribes, "NOT "+f.CustomTribe)
			} else {
				tribes = append(tribes, "NOT "+f.Tribe.String())
			}
		case FilterRank:
			ranks |= 1 << f.Rank
		case FilterTP:
			tp = append(tp, f.TP)
		}
	}

	var tpDesc []byte

	if len(tp) != 0 {
		sort.Slice(tp, func(i, j int) bool { return tp[i] < tp[j] })

		tpDesc = strconv.AppendInt(tpDesc, tp[0], 10)

		if len(tp) >= 2 {
			if len(tp) > 2 {
				for i := 1; i < len(tp)-1; i++ {
					tpDesc = append(tpDesc, ", "...)
					tpDesc = strconv.AppendInt(tpDesc, tp[i], 10)
				}

				tpDesc = append(tpDesc, ", "...)
			}

			tpDesc = append(tpDesc, " or "...)
			tpDesc = strconv.AppendInt(tpDesc, tp[len(tp)-1], 10)
		}

		tpDesc = append(tpDesc, " TP "...)
	}

	var rankNames []string

	switch ranks {
	case 1<<Attacker | 1<<Effect:
		rankNames = []string{"Enemy"}
	case 1<<Attacker | 1<<Effect | 1<<MiniBoss | 1<<Boss:
		rankNames = []string{"Non-Token"}
	default:
		for r := Attacker; r < RankNone; r++ {
			if ranks&(1<<r) != 0 {
				rankNames = append(rankNames, r.String())
			}
		}
	}

	if len(rankNames) == 0 {
		if len(tribes) == 0 {
			if len(tpDesc) != 0 {
				return string(tpDesc[:len(tpDesc)-1])
			}

			return "any"
		}

		return string(tpDesc) + strings.Join(tribes, "/")
	}

	rankName := strings.Join(rankNames, " or ")

	if len(tribes) == 0 {
		return string(tpDesc) + rankName
	}

	return string(tpDesc) + strings.Join(tribes, "/") + " " + rankName
}

func (e *EffectDef) describeUnselected(*Def, *Set, []*EffectDef) []*RichDescription {
	if e == nil {
		return []*RichDescription{
			{
				Text:  "Unselected effect",
				Color: sprites.Red,
			},
		}
	}

	return nil
}

func (e *EffectDef) describeFlavorText() []*RichDescription {
	tint := sprites.Color{R: 0x22, G: 0x66, B: 0x88, A: 0xff}

	if e.Flags&FlagFlavorTextCustomColor != 0 {
		tint.R = uint8(e.Amount)
		tint.G = uint8(e.Amount >> 8)
		tint.B = uint8(e.Amount >> 16)
	}

	return []*RichDescription{
		{
			Effect: e,

			Text: e.Text,

			Color: tint,
		},
	}
}

func (e *EffectDef) describeEffectStat(parents []*EffectDef) []*RichDescription {
	var text []byte

	if e.Flags&FlagStatDEF != 0 {
		text = []byte("DEF")
	} else {
		text = []byte("ATK")
	}

	if len(parents) == 0 || (len(parents) == 1 && parents[0].Type == CondLimit) {
		text = append(text, ": "...)
	} else if e.Amount >= 0 {
		text = append(text, '+')
	}

	text = append(text, e.describeAmount(FlagStatInfinity)...)

	return []*RichDescription{
		{
			Effect: e,

			Text: string(text),
		},
	}
}

func (e *EffectDef) describeEffectEmpower(set *Set) []*RichDescription {
	var text []byte

	if e.Flags&FlagStatDEF != 0 {
		if e.Amount < 0 {
			text = []byte("Break ")
		} else {
			text = []byte("Fortify ")
		}
	} else {
		if e.Amount < 0 {
			text = []byte("Enfeeble ")
		} else {
			text = []byte("Empower ")
		}
	}

	if e.Amount >= 0 {
		text = append(text, '+')
	}

	text = append(text, e.describeAmount(FlagStatInfinity)...)

	text = append(text, " ("...)
	text = append(text, e.describeFilter(set)...)
	text = append(text, ')')

	if e.Flags&FlagStatOpponent != 0 {
		text = append(text, " (opponent)"...)
	}

	return []*RichDescription{
		{
			Effect: e,

			Text: string(text),
		},
	}
}

func (e *EffectDef) describeEffectSummon(card *Def, set *Set) []*RichDescription {
	var text []byte

	if e.Flags&FlagSummonReplace != 0 {
		text = []byte("Turns into ")
	} else {
		text = []byte("Summon ")
	}

	if e.Amount != 1 {
		text = strconv.AppendInt(text, e.Amount, 10)
		text = append(text, ' ')

		if !e.Filter.IsSingleCard() {
			text = append(text, "random "...)
		}
	} else if !e.Filter.IsSingleCard() {
		text = append(text, "a random "...)
	}

	if e.Filter.IsSingleCard() {
		if e.Flags&FlagSummonReplace != 0 && card != nil && e.Filter[0].CardID == card.ID && e.Amount == 1 {
			text = []byte("Resummon")
		} else {
			c := set.Card(e.Filter[0].CardID)

			if c != nil {
				text = append(text, c.DisplayName()...)
			} else {
				text = append(text, "MissingCard?"...)
				text = strconv.AppendUint(text, uint64(e.Filter[0].CardID), 10)
				text = append(text, '?')
			}
		}
	} else {
		text = append(text, e.describeFilter(set)...)

		if e.Amount == 1 {
			text = append(text, " card"...)
		} else {
			text = append(text, " cards"...)
		}
	}

	if e.Flags&FlagSummonOpponent != 0 {
		text = append(text, " as opponent"...)
	}

	return []*RichDescription{
		{
			Effect: e,

			Text: string(text),
		},
	}
}

func (e *EffectDef) describeEffectHeal() []*RichDescription {
	var before, amount, after string

	if e.Flags&FlagHealOpponent != 0 {
		if e.Amount < 0 {
			before = "Damage Opponent "
		} else {
			before = "Heal Opponent "
		}
	} else {
		if e.Amount < 0 {
			before = "Take "
			after = " Damage"
		} else {
			before = "Heal\u00a0"
		}
	}

	if e.Flags&FlagHealRaw != 0 {
		after += " (ignoring modifiers)"
	}

	switch {
	case e.Flags&FlagHealInfinity != 0:
		amount = PosInf
	case e.Amount >= 0:
		amount = strconv.FormatInt(e.Amount, 10)
	default:
		amount = strconv.FormatInt(-e.Amount, 10)
	}

	return []*RichDescription{
		{
			Effect: e,

			Text: before + amount + after,
		},
	}
}

func (e *EffectDef) describeEffectTP() []*RichDescription {
	text := []byte("TP")

	if e.Amount >= 0 {
		text = append(text, '+')
	}

	text = append(text, e.describeAmount(FlagTPInfinity)...)

	return []*RichDescription{
		{
			Effect: e,

			Text: string(text),
		},
	}
}

func (e *EffectDef) describeEffectFreeze(card *Def, set *Set, parents []*EffectDef) []*RichDescription {
	if e.Type != EffectNumb || e.Flags&(FlagNumbSummon|FlagNumbResult|FlagNumbResult2) != FlagNumbResult {
		return nil
	}

	if !e.Filter.IsSingleCard() || e.Result == nil || e.Result.Type != EffectSummon {
		return nil
	}

	if e.Result.Amount != 1 || !e.Result.Filter.IsSingleCard() || e.Result.Flags&(FlagSummonInvisible|FlagSummonReplace) != 0 {
		return nil
	}

	if (e.Flags&FlagNumbSelf == 0) == (e.Result.Flags&FlagSummonOpponent == 0) || set.Card(e.Filter[0].CardID) == nil || set.Card(e.Result.Filter[0].CardID) == nil {
		return nil
	}

	if "Frozen "+set.Card(e.Filter[0].CardID).DisplayName() == set.Card(e.Result.Filter[0].CardID).DisplayName() {
		text := []byte("Freeze ")

		if e.Flags&FlagNumbSelf != 0 {
			text = append(text, "Own "...)
		}

		text = append(text, e.describeCardName(set, e.Filter[0].CardID)...)
		text = append(text, " ("...)
		text = append(text, e.describeAmount(FlagNumbInfinity)...)
		text = append(text, ")"...)

		return []*RichDescription{
			{
				Effect: e,
				Content: []*RichDescription{
					{
						Effect: e.Result,
						Text:   string(text),
					},
				},
			},
		}
	}

	return nil
}

func (e *EffectDef) describeEffectNumb(card *Def, set *Set, parents []*EffectDef) []*RichDescription {
	text := []byte("Numb")

	if e.Flags&FlagNumbSummon != 0 {
		text = []byte("Steal")
	}

	if e.Flags&FlagNumbIgnorePrevent != 0 {
		text = append([]byte("True "), text...)
	}

	if e.Flags&FlagNumbSelf != 0 {
		text = append(text, " Own"...)
	}

	if e.Flags&FlagNumbSummon != 0 || len(e.Filter) != 1 || e.Filter[0].Type != FilterRank || e.Filter[0].Rank != Attacker {
		text = append(text, ' ')
		text = append(text, e.describeFilter(set)...)
	}

	text = append(text, "\u00a0("...)

	text = append(text, e.describeAmount(FlagNumbInfinity)...)

	text = append(text, ')')

	results := make([]*RichDescription, 0, 4)

	if e.Flags&FlagNumbResult != 0 {
		results = append(results, &RichDescription{
			Text: "\nOn Success: ",
		})

		results = append(results, e.Result.Description(card, set, append(parents, e)...)...)
	}

	if e.Flags&FlagNumbResult2 != 0 {
		results = append(results, &RichDescription{
			Text: "\nOn Failure: ",
		})

		results = append(results, e.Result2.Description(card, set, append(parents, e)...)...)
	}

	return []*RichDescription{
		{
			Effect:  e,
			Text:    string(text),
			Content: results,
		},
	}
}

func (e *EffectDef) describeEffectPierce(*Def, *Set, []*EffectDef) []*RichDescription {
	if e.Type != EffectRawStat || e.Flags&(FlagStatDEF|FlagStatOpponent) != FlagStatDEF|FlagStatOpponent || e.Amount > 0 {
		return nil
	}

	e.Amount = -e.Amount
	amount := e.describeAmount(FlagStatInfinity)
	e.Amount = -e.Amount

	return []*RichDescription{
		{
			Effect: e,

			Text: "Pierce (" + amount + ")",
		},
	}
}

func (e *EffectDef) describeEffectRawStat() []*RichDescription {
	var text []byte

	if e.Flags&FlagStatDEF != 0 {
		text = []byte("DEF")
	} else {
		text = []byte("ATK")
	}

	if e.Amount >= 0 {
		text = append(text, '+')
	}

	text = append(text, e.describeAmount(FlagStatInfinity)...)

	if e.Flags&FlagStatOpponent != 0 {
		text = append(text, " for opponent"...)
	} else {
		text = append(text, " for self"...)
	}

	return []*RichDescription{
		{
			Effect: e,

			Text: string(text),
		},
	}
}

func (e *EffectDef) describeEffectMultiplyHealing() []*RichDescription {
	var when, what, who string

	switch e.Flags & FlagMultiplyHealingTypeMask {
	case FlagMultiplyHealingTypeAll:
		what = "Health Changes"
	case FlagMultiplyHealingTypePositive:
		what = "Healing"
	case FlagMultiplyHealingTypeNegative:
		what = "Damage Effects"
	}

	if e.Flags&FlagMultiplyHealingFuture != 0 {
		when = "Future "
	}

	if e.Flags&FlagMultiplyHealingOpponent != 0 {
		who = " for opponent"
	}

	return []*RichDescription{
		{
			Effect: e,

			Text: "Multiply " + when + what + " by " + strconv.FormatInt(e.Amount, 10) + who,
		},
	}
}

func (e *EffectDef) describeEffectPreventNumb(set *Set) []*RichDescription {
	text := []byte("Prevent Numb ")

	if e.Flags&FlagPreventNumbOpponent != 0 {
		text = append(text, "Opponent "...)
	}

	text = append(text, e.describeFilter(set)...)

	text = append(text, " ("...)
	text = strconv.AppendInt(text, e.Amount, 10)
	text = append(text, ')')

	return []*RichDescription{
		{
			Effect: e,

			Text: string(text),
		},
	}
}

func (e *EffectDef) describeEffectModifyAvailableCards(card *Def, set *Set, parents []*EffectDef) []*RichDescription {
	var text []byte

	isSelf := e.Amount == 1 && e.Filter.IsSingleCard() && card != nil && card.ID == e.Filter[0].CardID

	// shorten names
	const (
		Opponent        = FlagModifyCardsOpponent
		TypeMask        = FlagModifyCardsTypeMask
		TypeAdd         = FlagModifyCardsTypeAdd
		TypeRemove      = FlagModifyCardsTypeRemove
		TypeMove        = FlagModifyCardsTypeMove
		TypeReplace     = FlagModifyCardsTypeReplace
		TargetMask      = FlagModifyCardsTargetMask
		TargetAny       = FlagModifyCardsTargetAny
		TargetHand      = FlagModifyCardsTargetHand
		TargetDeck      = FlagModifyCardsTargetDeck
		TargetField     = FlagModifyCardsTargetField
		RequireMask     = FlagModifyCardsRequireMask
		RequireNone     = FlagModifyCardsRequireNone
		RequireSelf     = FlagModifyCardsRequireSelf
		RequireOpponent = FlagModifyCardsRequireOpponent
	)

	switch e.Flags & (Opponent | TypeMask | TargetMask | RequireMask) {
	case TypeAdd | TargetAny | RequireNone:
		text = []byte("Summon To Hand")
	case Opponent | TypeAdd | TargetAny | RequireNone:
		text = []byte("Summon To Opponent's Hand")
	case TypeAdd | TargetHand | RequireNone:
		text = []byte("Add To Hand")
	case Opponent | TypeAdd | TargetHand | RequireNone:
		text = []byte("Add To Opponent's Hand")
	case TypeAdd | TargetDeck | RequireNone:
		text = []byte("Add To Deck")
	case Opponent | TypeAdd | TargetDeck | RequireNone:
		text = []byte("Add To Opponent's Deck")
	case TypeAdd | TargetField | RequireNone:
		text = []byte("Add To Field")
	case Opponent | TypeAdd | TargetField | RequireNone:
		text = []byte("Add To Opponent's Field")
	case TypeRemove | TargetAny | RequireNone:
		text = []byte("Exile")
	case Opponent | TypeRemove | TargetAny | RequireNone:
		text = []byte("Exile Opponent")
	case TypeRemove | TargetHand | RequireNone:
		text = []byte("Exile From Hand")
	case Opponent | TypeRemove | TargetHand | RequireNone:
		text = []byte("Exile From Opponent's Hand")
	case TypeRemove | TargetDeck | RequireNone:
		text = []byte("Exile From Deck")
	case Opponent | TypeRemove | TargetDeck | RequireNone:
		text = []byte("Exile From Opponent's Deck")
	case TypeRemove | TargetField | RequireNone:
		if isSelf {
			text = []byte("Exile Self")
		} else {
			text = []byte("Exile From Field")
		}
	case Opponent | TypeRemove | TargetField | RequireNone:
		text = []byte("Exile From Opponent's Field")
	case TypeMove | TargetAny | RequireNone:
		text = []byte("Play")
	case Opponent | TypeMove | TargetAny | RequireNone:
		text = []byte("Play Opponent")
	case TypeMove | TargetHand | RequireNone:
		text = []byte("Play From Hand")
	case Opponent | TypeMove | TargetHand | RequireNone:
		text = []byte("Play From Opponent's Hand")
	case TypeMove | TargetDeck | RequireNone:
		text = []byte("Play From Deck")
	case Opponent | TypeMove | TargetDeck | RequireNone:
		text = []byte("Play From Opponent's Deck")
	case TypeAdd | TargetAny | RequireSelf:
		text = []byte("Clone Summon To Hand")
	case Opponent | TypeAdd | TargetAny | RequireSelf:
		text = []byte("Clone Summon To Opponent's Hand")
	case TypeAdd | TargetHand | RequireSelf:
		text = []byte("Clone To Hand")
	case Opponent | TypeAdd | TargetHand | RequireSelf:
		text = []byte("Clone To Opponent's Hand")
	case TypeAdd | TargetDeck | RequireSelf:
		text = []byte("Clone To Deck")
	case Opponent | TypeAdd | TargetDeck | RequireSelf:
		text = []byte("Clone To Opponent's Deck")
	case TypeAdd | TargetField | RequireSelf:
		text = []byte("Clone To Field")
	case Opponent | TypeAdd | TargetField | RequireSelf:
		text = []byte("Clone To Opponent's Field")
	case TypeRemove | TargetAny | RequireSelf:
		text = []byte("Return From Exile To Own Hand (temp)")
	case Opponent | TypeRemove | TargetAny | RequireSelf:
		text = []byte("Defect From Exile To Opponent's Hand (temp)")
	case TypeRemove | TargetHand | RequireSelf:
		text = []byte("Return From Exile To Own Hand")
	case Opponent | TypeRemove | TargetHand | RequireSelf:
		text = []byte("Defect From Exile To Opponent's Hand")
	case TypeRemove | TargetDeck | RequireSelf:
		text = []byte("Return From Exile To Own Deck")
	case Opponent | TypeRemove | TargetDeck | RequireSelf:
		text = []byte("Defect From Exile To Opponent's Deck")
	case TypeRemove | TargetField | RequireSelf:
		text = []byte("Return From Exile To Own Field")
	case Opponent | TypeRemove | TargetField | RequireSelf:
		text = []byte("Defect From Exile To Opponent's Field")
	case TypeMove | TargetAny | RequireSelf:
		text = []byte("Follow")
	case Opponent | TypeMove | TargetAny | RequireSelf:
		text = []byte("Meet Opponent")
	case TypeMove | TargetHand | RequireSelf:
		text = []byte("Follow From Hand")
	case Opponent | TypeMove | TargetHand | RequireSelf:
		text = []byte("Meet From Opponent's Hand")
	case TypeMove | TargetDeck | RequireSelf:
		text = []byte("Follow From Deck")
	case Opponent | TypeMove | TargetDeck | RequireSelf:
		text = []byte("Meet From Opponent's Deck")
	case TypeAdd | TargetAny | RequireOpponent:
		text = []byte("Clone Summon From Opponent To Hand")
	case Opponent | TypeAdd | TargetAny | RequireOpponent:
		text = []byte("Clone Summon From Opponent To Opponent's Hand")
	case TypeAdd | TargetHand | RequireOpponent:
		text = []byte("Clone From Opponent To Hand")
	case Opponent | TypeAdd | TargetHand | RequireOpponent:
		text = []byte("Clone From Opponent To Opponent's Hand")
	case TypeAdd | TargetDeck | RequireOpponent:
		text = []byte("Clone From Opponent To Deck")
	case Opponent | TypeAdd | TargetDeck | RequireOpponent:
		text = []byte("Clone From Opponent To Opponent's Deck")
	case TypeAdd | TargetField | RequireOpponent:
		text = []byte("Clone From Opponent To Field")
	case Opponent | TypeAdd | TargetField | RequireOpponent:
		text = []byte("Clone From Opponent To Opponent's Field")
	case TypeRemove | TargetAny | RequireOpponent:
		text = []byte("Defect From Opponent's Exile To Own Hand (temp)")
	case Opponent | TypeRemove | TargetAny | RequireOpponent:
		text = []byte("Return From Opponent's Exile To Hand (temp)")
	case TypeRemove | TargetHand | RequireOpponent:
		text = []byte("Defect From Opponent's Exile To Own Hand")
	case Opponent | TypeRemove | TargetHand | RequireOpponent:
		text = []byte("Return From Opponent's Exile To Hand")
	case TypeRemove | TargetDeck | RequireOpponent:
		text = []byte("Defect From Opponent's Exile To Own Deck")
	case Opponent | TypeRemove | TargetDeck | RequireOpponent:
		text = []byte("Return From Opponent's Exile To Deck")
	case TypeRemove | TargetField | RequireOpponent:
		text = []byte("Defect From Opponent's Exile To Own Field")
	case Opponent | TypeRemove | TargetField | RequireOpponent:
		text = []byte("Return From Opponent's Exile To Field")
	case TypeMove | TargetAny | RequireOpponent:
		text = []byte("Meet")
	case Opponent | TypeMove | TargetAny | RequireOpponent:
		text = []byte("Follow Opponent")
	case TypeMove | TargetHand | RequireOpponent:
		text = []byte("Meet From Hand")
	case Opponent | TypeMove | TargetHand | RequireOpponent:
		text = []byte("Follow From Opponent's Hand")
	case TypeMove | TargetDeck | RequireOpponent:
		text = []byte("Meet From Deck")
	case Opponent | TypeMove | TargetDeck | RequireOpponent:
		text = []byte("Follow From Opponent's Deck")
	case TypeReplace | TargetAny | RequireNone:
		text = []byte("Replace With ")
	case Opponent | TypeReplace | TargetAny | RequireNone:
		text = []byte("Replace Opponent With ")
	case TypeReplace | TargetField | RequireNone:
		text = []byte("Replace Played With ")
	case Opponent | TypeReplace | TargetField | RequireNone:
		text = []byte("Replace Opponent's Played With ")
	case TypeReplace | TargetHand | RequireNone:
		text = []byte("Replace In Hand With ")
	case Opponent | TypeReplace | TargetHand | RequireNone:
		text = []byte("Replace In Opponent's Hand With ")
	case TypeReplace | TargetDeck | RequireNone:
		text = []byte("Replace In Deck With ")
	case Opponent | TypeReplace | TargetDeck | RequireNone:
		text = []byte("Replace In Opponent's Deck With ")
	}

	if e.Flags&TypeMask == TypeReplace {
		text = append(text, e.describeCardName(set, ID(e.Amount3))...)
	}

	if !isSelf || e.Flags&(Opponent|TypeMask|TargetMask|RequireMask) != TypeRemove|TargetField|RequireNone {
		text = append(text, " ("...)
		text = strconv.AppendInt(text, e.Amount, 10)
		text = append(text, "): "...)

		text = append(text, e.describeFilter(set)...)
	}

	results := make([]*RichDescription, 0, 4)

	if e.Flags&FlagModifyCardsResult != 0 {
		results = append(results, &RichDescription{
			Text: "\nOn Success: ",
		})

		results = append(results, e.Result.Description(card, set, append(parents, e)...)...)
	}

	if e.Flags&FlagModifyCardsResult2 != 0 {
		results = append(results, &RichDescription{
			Text: "\nOn Failure: ",
		})

		results = append(results, e.Result2.Description(card, set, append(parents, e)...)...)
	}

	return []*RichDescription{
		{
			Effect:  e,
			Text:    string(text),
			Content: results,
		},
	}
}

func (e *EffectDef) describeEffectModifyCardCost(set *Set) []*RichDescription {
	text := []byte("Modify TP Cost (")

	if e.Flags&FlagModifyCardCostSet != 0 {
		text = append(text, "Set To "...)
	} else if e.Amount >= 0 {
		text = append(text, '+')
	}

	text = append(text, e.describeAmount(FlagModifyCardCostInfinity)...)
	text = append(text, "): "...)

	text = append(text, e.describeFilter(set)...)

	return []*RichDescription{
		{
			Effect: e,

			Text: string(text),
		},
	}
}

func (e *EffectDef) describeEffectDrawCard(card *Def, set *Set, parents []*EffectDef) []*RichDescription {
	var text []byte

	amount := e.Amount
	ignoreMax := e.Flags&FlagDrawCardIgnoreMax != 0

	if amount < 0 {
		amount = -amount
		ignoreMax = false

		if e.Flags&FlagDrawCardOpponent != 0 {
			text = []byte("Opponent Discards ")
		} else {
			text = []byte("Discard ")
		}
	} else {
		if e.Flags&FlagDrawCardOpponent != 0 {
			text = []byte("Opponent Draws ")
		} else {
			text = []byte("Draw ")
		}
	}

	text = strconv.AppendInt(text, amount, 10)

	if len(e.Filter) != 0 {
		text = append(text, ' ')
		text = append(text, e.describeFilter(set)...)
	}

	if amount == 1 {
		text = append(text, " Card"...)
	} else {
		text = append(text, " Cards"...)
	}

	if ignoreMax {
		text = append(text, " (ignoring limit)"...)
	}

	desc := []*RichDescription{
		{
			Effect: e,
			Text:   string(text),
		},
	}

	if e.Result != nil {
		if e.Amount < 0 {
			desc[0].Text += "\nIf Discard: "
		} else {
			desc[0].Text += "\nIf Draw: "
		}

		desc[0].Content = e.Result.Description(card, set, append(parents, e)...)
	}

	if e.Result2 != nil {
		desc[0].Content = append(desc[0].Content, &RichDescription{
			Effect:  e,
			Text:    "\nOn Fail: ",
			Content: e.Result2.Description(card, set, append(parents, e)...),
		})
	}

	return desc
}

func (e *EffectDef) describeEffectModifyGameRule() []*RichDescription {
	var name string

	switch e.Amount2 {
	case 1:
		name = "Max HP"
	case 2:
		name = "Min Hand Size"
	case 3:
		name = "Max Hand Size"
	case 4:
		name = "Draw Per Turn"
	case 6:
		name = "Min TP"
	case 7:
		name = "Max TP"
	case 8:
		name = "TP Per Turn"
	default:
		name = "rule?" + strconv.FormatInt(e.Amount2, 10) + "?"
	}

	amount := strconv.FormatInt(e.Amount, 10)

	switch {
	case e.Flags&FlagModifyGameRuleRelative == 0:
		switch e.Flags & FlagModifyGameRulePlayerMask {
		case FlagModifyGameRulePlayerSelf:
			name = "Set Own " + name + " to " + amount
		case FlagModifyGameRulePlayerOpponent:
			name = "Set Opponent's " + name + " to " + amount
		case FlagModifyGameRulePlayerBoth:
			name = "Set " + name + " to " + amount + " For Both Players"
		}
	case e.Amount < 0:
		switch e.Flags & FlagModifyGameRulePlayerMask {
		case FlagModifyGameRulePlayerSelf:
			name = "Subtract " + amount[1:] + " From Own " + name
		case FlagModifyGameRulePlayerOpponent:
			name = "Subtract " + amount[1:] + " From Opponent's " + name
		case FlagModifyGameRulePlayerBoth:
			name = "Subtract " + amount[1:] + " From " + name + " For Both Players"
		}
	default:
		switch e.Flags & FlagModifyGameRulePlayerMask {
		case FlagModifyGameRulePlayerSelf:
			name = "Add " + amount + " To Own " + name
		case FlagModifyGameRulePlayerOpponent:
			name = "Add " + amount + " To Opponent's " + name
		case FlagModifyGameRulePlayerBoth:
			name = "Add " + amount + " To " + name + " For Both Players"
		}
	}

	return []*RichDescription{
		{
			Effect: e,
			Text:   name,
		},
	}
}

func (e *EffectDef) describeEffectDelaySetup(set *Set) []*RichDescription {
	var text []byte

	if e.Flags&FlagDelaySetupDelete != 0 && e.Amount < 0 {
		text = []byte("Destroy ")
		text = strconv.AppendInt(text, -e.Amount, 10)
		text = append(text, " Rounds of "...)

		if e.Flags&FlagDelaySetupOpponent != 0 {
			text = append(text, "Opponent's "...)
		}

		if len(e.Filter) != 0 {
			text = append(text, e.describeFilter(set)...)
			text = append(text, ' ')
		}

		text = append(text, "Setup Effects"...)
	} else {
		amount := e.Amount

		if amount < 0 {
			amount = -amount

			text = []byte("Advance ")
		} else {
			text = []byte("Delay ")
		}

		if e.Flags&FlagDelaySetupOpponent != 0 {
			text = append(text, "Opponent's "...)
		}

		if len(e.Filter) != 0 {
			text = append(text, e.describeFilter(set)...)
			text = append(text, ' ')
		}

		text = append(text, "Setup Effects By "...)

		text = strconv.AppendInt(text, amount, 10)

		if amount == 1 {
			text = append(text, " Round"...)
		} else {
			text = append(text, " Rounds"...)
		}
	}

	return []*RichDescription{
		{
			Effect: e,

			Text: string(text),
		},
	}
}

func (e *EffectDef) describeCondPay(card *Def, set *Set, parents []*EffectDef) []*RichDescription {
	if e.Type != CondCard || e.Flags&(FlagCondCardAllowNumb|FlagCondCardLocationMask|FlagCondCardOpponent|FlagCondCardTypeMask) != FlagCondCardLocationField|FlagCondCardTypeGreaterEqual {
		return nil
	}

	if !e.Filter.IsSingleCard() || e.Result == nil || e.Result.Type != CondMultipleEffects {
		return nil
	}

	if e.Result.Result == nil || e.Result.Result.Priority >= e.Priority || e.Result.Result.Type != EffectNumb {
		return nil
	}

	if e.Result.Result.Flags&(FlagNumbInfinity|FlagNumbSelf|FlagNumbSummon|FlagNumbResult|FlagNumbResult2) != FlagNumbSelf || e.Result.Result.Amount != e.Amount {
		return nil
	}

	if !e.Result.Result.Filter.IsSingleCard() || e.Filter[0].CardID != e.Result.Result.Filter[0].CardID {
		return nil
	}

	return []*RichDescription{
		{
			Effect: e,
			Text:   "Pay " + set.Card(e.Filter[0].CardID).DisplayName() + " (" + strconv.FormatInt(e.Amount, 10) + "): ",

			Content: e.Result.Result2.Description(card, set, append(parents, e, e.Result)...),
		},
	}
}

func (e *EffectDef) describeCondCard(card *Def, set *Set, parents []*EffectDef) []*RichDescription {
	var text []byte

	if len(parents) == 0 || parents[len(parents)-1].Type != CondCard || parents[len(parents)-1].Flags&(FlagCondCardTypeMask|FlagCondCardOpponent) != e.Flags&(FlagCondCardTypeMask|FlagCondCardOpponent) {
		switch e.Flags & (FlagCondCardTypeMask | FlagCondCardOpponent) {
		case FlagCondCardTypeGreaterEqual:
			text = []byte("If ")
		case FlagCondCardTypeLessThan:
			text = []byte("Unless ")
		case FlagCondCardTypeGreaterEqual | FlagCondCardOpponent:
			text = []byte("VS ")
		case FlagCondCardTypeLessThan | FlagCondCardOpponent:
			text = []byte("Unless VS ")
		case FlagCondCardTypeEach:
			text = []byte("Per ")
		case FlagCondCardTypeEach | FlagCondCardOpponent:
			if e.Amount != 1 {
				text = []byte("VS every ")
			} else {
				text = []byte("VS each ")
			}
		}
	}

	if e.Flags&FlagCondCardTypeMask == FlagCondCardTypeEach && e.Amount != 1 {
		text = strconv.AppendInt(text, e.Amount, 10)
		text = append(text, ' ')
	}

	if e.Filter.IsSingleCard() {
		text = append(text, e.describeCardName(set, e.Filter[0].CardID)...)
	} else {
		text = append(text, e.describeFilter(set)...)
	}

	if e.Flags&FlagCondCardTypeMask != FlagCondCardTypeEach && (!e.Filter.IsSingleCard() || e.Amount != 1) {
		text = append(text, " ("...)
		text = strconv.AppendInt(text, e.Amount, 10)
		text = append(text, ')')
	}

	switch e.Flags & FlagCondCardLocationMask {
	case FlagCondCardLocationField:
		// no extra text
	case FlagCondCardLocationHand:
		text = append(text, " in Hand"...)
	case FlagCondCardLocationDeck:
		text = append(text, " in Deck"...)
	}

	if e.Result != nil && e.Result.Type == CondCard && e.Flags&(FlagCondCardOpponent|FlagCondCardTypeMask) == e.Result.Flags&(FlagCondCardOpponent|FlagCondCardTypeMask) {
		switch e.Flags & FlagCondCardTypeMask {
		case FlagCondCardTypeGreaterEqual:
			text = append(text, " and "...)
		case FlagCondCardTypeLessThan:
			text = append(text, " or "...)
		case FlagCondCardTypeEach:
			text = append(text, ", per "...)
		}
	} else {
		text = append(text, ": "...)
	}

	return []*RichDescription{
		{
			Effect: e,

			Text:    string(text),
			Content: e.Result.Description(card, set, append(parents, e)...),
		},
	}
}

func (e *EffectDef) describeEffectUnity(card *Def, set *Set, parents []*EffectDef) []*RichDescription {
	if e.Type == CondLimit && e.Amount == 1 && e.Flags&FlagCondLimitGreaterThan == 0 && e.Result != nil && e.Result.Type == EffectEmpower && e.Result.Flags&(FlagStatOpponent|FlagStatDEF) == 0 {
		return []*RichDescription{
			{
				Effect: e,

				Content: []*RichDescription{
					{
						Effect: e.Result,

						Text: "Unity (" + e.Result.describeAmount(FlagStatInfinity) + ", " + e.Result.describeFilter(set) + ")",
					},
				},
			},
		}
	}

	return nil
}

func (e *EffectDef) describeEffectLosePerRound(card *Def, set *Set, parents []*EffectDef) []*RichDescription {
	if e.Type == CondLimit && e.Result.IsPassive(card) && card.Rank == Token && card.TP == 0 && e.Flags&FlagCondLimitGreaterThan != 0 {
		return []*RichDescription{
			{
				Effect: e,
				Content: []*RichDescription{
					{
						Effect: e.Result,
						Content: []*RichDescription{
							{
								Effect: e.Result.Result,
								Text:   "Lose " + strconv.FormatInt(e.Amount, 10) + " per round",
							},
						},
					},
				},
			},
		}
	}

	return nil
}

func (e *EffectDef) describeCondLimit(card *Def, set *Set, parents []*EffectDef) []*RichDescription {
	if len(parents) == 0 {
		var suffix string

		switch {
		case e.Amount == 1 && e.Flags&FlagCondLimitGreaterThan == 0:
			suffix = " (only once)"
		case e.Flags&FlagCondLimitGreaterThan == 0:
			suffix = " (max. " + strconv.FormatInt(e.Amount, 10) + " times)"
		case e.Amount == 1:
			suffix = " (except the first time)"
		default:
			suffix = " (except the first " + strconv.FormatInt(e.Amount, 10) + " times)"
		}

		return []*RichDescription{
			{
				Effect: e,

				Content: append(e.Result.Description(card, set, append(parents, e)...), &RichDescription{
					Effect: e,

					Text: suffix,
				}),
			},
		}
	}

	var prefix string

	switch {
	case e.Amount == 1 && e.Flags&FlagCondLimitGreaterThan == 0:
		prefix = "Only Once: "
	case e.Flags&FlagCondLimitGreaterThan == 0:
		prefix = "Max. " + strconv.FormatInt(e.Amount, 10) + " Times: "
	case e.Amount == 1:
		prefix = "Except First Time: "
	default:
		prefix = "Except First " + strconv.FormatInt(e.Amount, 10) + " Times: "
	}

	return []*RichDescription{
		{
			Effect: e,

			Text:    prefix,
			Content: e.Result.Description(card, set, append(parents, e)...),
		},
	}
}

func (e *EffectDef) describeEffectLifesteal(card *Def, set *Set, parents []*EffectDef) []*RichDescription {
	if e.Type != CondWinner || e.Flags&FlagCondWinnerTypeMask != FlagCondWinnerTypeWinner {
		return nil
	}

	if e.Result != nil && e.Result.Type == EffectHeal && e.Result.Flags&FlagHealOpponent == 0 && e.Result.Amount >= 0 {
		return []*RichDescription{
			{
				Effect: e,

				Content: []*RichDescription{
					{
						Effect: e.Result,

						Text: "Lifesteal\u00a0(" + strconv.FormatInt(e.Result.Amount, 10) + ")",
					},
				},
			},
		}
	}

	return nil
}

func (e *EffectDef) describeCondWinner(card *Def, set *Set, parents []*EffectDef) []*RichDescription {
	var text string

	switch e.Flags & FlagCondWinnerTypeMask {
	case FlagCondWinnerTypeWinner:
		text = "On Win: "
	case FlagCondWinnerTypeLoser:
		text = "On Lose: "
	case FlagCondWinnerTypeTie:
		text = "On Tie: "
	case FlagCondWinnerTypeNotTie:
		text = "Unless Tie: "
	case FlagCondWinnerTypeNotWinner:
		text = "Unless Win: "
	case FlagCondWinnerTypeNotLoser:
		text = "Unless Lose: "
	}

	return []*RichDescription{
		{
			Effect: e,

			Text:    text,
			Content: e.Result.Description(card, set, append(parents, e)...),
		},
	}
}

func (e *EffectDef) IsPassive(card *Def) bool {
	return card != nil &&
		e != nil &&
		e.Type == CondApply &&
		e.Flags&(FlagCondApplyNextRound|FlagCondApplyOpponent|FlagCondApplyShowMask) == FlagCondApplyNextRound|FlagCondApplyShowOriginalText &&
		e.Result != nil &&
		e.Result.Type == EffectSummon &&
		e.Result.Priority == 0 &&
		e.Result.Flags&(FlagSummonInvisible|FlagSummonOpponent|FlagSummonReplace) == FlagSummonReplace &&
		e.Result.Amount == 1 &&
		e.Result.Filter.IsSingleCard() &&
		e.Result.Filter[0].CardID == card.ID
}

func (e *EffectDef) describeEffectPassive(card *Def, _ *Set, _ []*EffectDef) []*RichDescription {
	if !e.IsPassive(card) {
		return nil
	}

	return []*RichDescription{
		{
			Effect: e,

			Content: []*RichDescription{
				{
					Effect: e.Result,

					Text: "Passive",
				},
			},
		},
	}
}

func (e *EffectDef) describeCondApply(card *Def, set *Set, parents []*EffectDef) []*RichDescription {
	d := &RichDescription{
		Effect: e,

		Content: e.Result.Description(card, set, append(parents, e)...),
	}

	if e.Flags&FlagCondApplyOpponent != 0 {
		d.Content = append(d.Content, &RichDescription{
			Effect: e,

			Text: " as opponent",
		})
	}

	if e.Flags&FlagCondApplyNextRound != 0 && (len(parents) == 0 || parents[len(parents)-1].Type != CondApply || parents[len(parents)-1].Flags&(FlagCondApplyShowMask|FlagCondApplyOpponent|FlagCondApplyNextRound) != e.Flags&(FlagCondApplyShowMask|FlagCondApplyNextRound)) {
		d.Text = "Setup ("

		rounds := 0

		for c := e; c != nil && c.Type == CondApply && c.Flags&(FlagCondApplyShowMask|FlagCondApplyNextRound) == e.Flags&(FlagCondApplyShowMask|FlagCondApplyNextRound); c = c.Result {
			rounds++

			if c.Flags&FlagCondApplyOpponent != 0 {
				break
			}
		}

		if rounds != 1 {
			d.Text += strconv.Itoa(rounds) + " rounds) ("
		}

		d.Content = append(d.Content, &RichDescription{
			Effect: e,

			Text: ")",
		})
	}

	return []*RichDescription{d}
}

func (e *EffectDef) describeCondCoin(card *Def, set *Set, parents []*EffectDef) []*RichDescription {
	d := &RichDescription{
		Effect: e,

		Text:    "Coin\u00a0(" + strconv.FormatInt(e.Amount, 10) + "): ",
		Content: e.Result.Description(card, set, append(parents, e)...),
	}

	if e.Flags&FlagCondCoinTails != 0 {
		d.Content = append(d.Content, &RichDescription{
			Effect: e,
			Text:   " or ",
		})

		d.Content = append(d.Content, e.Result2.Description(card, set, append(parents, e)...)...)
	}

	return []*RichDescription{d}
}

func (e *EffectDef) describeCondStat(card *Def, set *Set, parents []*EffectDef) []*RichDescription {
	var text []byte

	switch e.Flags & (FlagCondStatLessThan | FlagCondStatOpponent) {
	case 0:
		text = []byte("If ")
	case FlagCondStatLessThan:
		text = []byte("Unless ")
	case FlagCondStatOpponent:
		text = []byte("VS ")
	case FlagCondStatLessThan | FlagCondStatOpponent:
		text = []byte("Unless VS ")
	}

	switch e.Flags & FlagCondStatTypeMask {
	case FlagCondStatTypeATK:
		text = append(text, "ATK"...)
	case FlagCondStatTypeDEF:
		text = append(text, "DEF"...)
	case FlagCondStatTypeHP:
		text = append(text, "HP"...)
	case FlagCondStatTypeTP:
		text = append(text, "TP"...)
	}

	otherName := ""

	if e.Flags&FlagCondStatMathOpponent != 0 && e.Flags&FlagCondStatOpponent == 0 {
		otherName = "opponent's "
	} else if e.Flags&FlagCondStatMathOpponent == 0 && e.Flags&FlagCondStatOpponent != 0 {
		otherName = "own "
	}

	switch e.Flags & FlagCondStatMathTypeMask {
	case FlagCondStatMathTypeATK:
		otherName += "ATK"
	case FlagCondStatMathTypeDEF:
		otherName += "DEF"
	case FlagCondStatMathTypeHP:
		otherName += "HP"
	case FlagCondStatMathTypeTP:
		otherName += "TP"
	}

	switch e.Flags & FlagCondStatMathMask {
	case FlagCondStatMathNone:
		// no math
	case FlagCondStatMathAdd:
		text = append(text, " plus "...)
		text = append(text, otherName...)
	case FlagCondStatMathSubtract:
		text = append(text, " minus "...)
		text = append(text, otherName...)
	}

	text = append(text, " ("...)

	text = strconv.AppendInt(text, e.Amount, 10)

	if e.Flags&FlagCondStatMultiple != 0 {
		text = append(text, '*')
	}

	text = append(text, "): "...)

	return []*RichDescription{
		{
			Effect: e,

			Text:    string(text),
			Content: e.Result.Description(card, set, append(parents, e)...),
		},
	}
}

func (e *EffectDef) describeEffectReveal(*Def, *Set, []*EffectDef) []*RichDescription {
	if e.Type == CondInHand && e.Result.isNull() && e.Flags&FlagCondInHandHide == 0 {
		return []*RichDescription{
			{
				Effect: e,

				Text: "Reveal",
			},
		}
	}

	return nil
}

func (e *EffectDef) describeCondInHand(card *Def, set *Set, parents []*EffectDef) []*RichDescription {
	var text string

	switch e.Flags & FlagCondInHandOnPlayMask {
	case FlagCondInHandOnPlayAlso:
		text = "In Hand"
	case FlagCondInHandOnPlayNone:
		text = "Not Played"
	case FlagCondInHandOnPlayOnly:
		text = "On Play"
	}

	switch {
	case e.Flags&FlagCondInHandOnPlayMask == FlagCondInHandOnPlayAlso && e.Amount == 0 && e.Amount2 == 1:
		text = "On Draw"
	case e.Amount != 0 && e.Amount2 == 1:
		text += " (for " + strconv.FormatInt(e.Amount2, 10) + " round after " + strconv.FormatInt(e.Amount, 10) + ")"
	case e.Amount != 0 && e.Amount2 != 0:
		text += " (for " + strconv.FormatInt(e.Amount2, 10) + " rounds after " + strconv.FormatInt(e.Amount, 10) + ")"
	case e.Amount == 1:
		text += " (after " + strconv.FormatInt(e.Amount, 10) + " round)"
	case e.Amount != 0:
		text += " (after " + strconv.FormatInt(e.Amount, 10) + " rounds)"
	case e.Amount2 == 1:
		text += " (for " + strconv.FormatInt(e.Amount2, 10) + " round)"
	case e.Amount2 != 0:
		text += " (for " + strconv.FormatInt(e.Amount2, 10) + " rounds)"
	}

	return []*RichDescription{
		{
			Effect: e,

			Text:    text + ": ",
			Content: e.Result.Description(card, set, append(parents, e)...),
		},
	}
}

func (e *EffectDef) describeCondLastEffect(card *Def, set *Set, parents []*EffectDef) []*RichDescription {
	return []*RichDescription{
		{
			Effect: e,

			Content: append(
				e.Result.Description(card, set, append(parents, e)...),
				&RichDescription{
					Text: " (final)",
				},
			),
		},
	}
}

func (e *EffectDef) describeCondOnNumb(card *Def, set *Set, parents []*EffectDef) []*RichDescription {
	return []*RichDescription{
		{
			Effect: e,

			Text:    "On Numb: ",
			Content: e.Result.Description(card, set, append(parents, e)...),
		},
	}
}

func (e *EffectDef) describeCondMultipleEffects(card *Def, set *Set, parents []*EffectDef) []*RichDescription {
	return []*RichDescription{
		{
			Effect: e,

			Content: append(
				append(
					e.Result.Description(card, set, append(parents, e)...),
					&RichDescription{
						Effect: e,
						Text:   " and ",
					},
				),
				e.Result2.Description(card, set, append(parents, e)...)...,
			),
		},
	}
}

func (e *EffectDef) describeCondOnDiscard(card *Def, set *Set, parents []*EffectDef) []*RichDescription {
	return []*RichDescription{
		{
			Effect: e,

			Text:    "On Discard: ",
			Content: e.Result.Description(card, set, append(parents, e)...),
		},
	}
}

func (e *EffectDef) describeCondCompareTargetCard(card *Def, set *Set, parents []*EffectDef) []*RichDescription {
	maybeNot := ""
	if e.Flags&FlagCondCompareTargetCardInvert != 0 {
		maybeNot = "not "
	}

	return []*RichDescription{
		{
			Effect: e,

			Text:    "If target card is " + maybeNot + e.describeFilter(set) + ": ",
			Content: e.Result.Description(card, set, append(parents, e)...),
		},
	}
}

func (e *EffectDef) describeCondOnExile(card *Def, set *Set, parents []*EffectDef) []*RichDescription {
	return []*RichDescription{
		{
			Effect: e,

			Text:    "On Exile: ",
			Content: e.Result.Description(card, set, append(parents, e)...),
		},
	}
}
