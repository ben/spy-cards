package card

import (
	"context"
	"encoding/base64"
	"fmt"
	"net/url"
	"strings"
	"time"

	"git.lubar.me/ben/spy-cards/format"
	"git.lubar.me/ben/spy-cards/internal"
)

// Recording is a recording of a Spy Cards Online match.
type Recording struct {
	FormatVersion  uint64
	Version        [3]uint64
	ModeName       string
	Perspective    uint8 // 0 = none, n = Player n
	Cosmetic       [2]CosmeticData
	RematchCount   uint64
	WonMatches     uint64
	Start          time.Time
	SpoilerGuard   [2]*[32]byte
	SharedSeed     [32]byte
	PrivateSeed    [2][4]byte
	CustomCards    Set
	CustomCardsRaw string
	InitialDeck    [2]Deck
	Rounds         []RecordingRound
}

// CosmeticData is non-essential data representing choices made by each player
// before the match began.
type CosmeticData struct {
	CharacterName string `json:"character"`
}

// RecordingRound is data for one round of Spy Cards Online.
type RecordingRound struct {
	TurnSeed  [8]byte
	TurnSeed2 [8]byte
	Ready     [2]uint64
}

// TurnData is the data stored for one round of Spy Cards Online to allow
// verification.
type TurnData struct {
	Ready    [2]uint64
	Played   [2][]ID
	InHand   [2]uint64
	HandID   [2][]ID
	Modified [2][][]ModifiedCardPosition
}

// ModifiedCardPosition is a card modified by the Modify Available Cards effect.
type ModifiedCardPosition struct {
	InHand   bool
	Position uint64
	CardID   ID
}

// MarshalText implements encoding.TextMarshaler.
func (sc *Recording) MarshalText() ([]byte, error) {
	b, err := sc.MarshalBinary()
	if err != nil {
		return nil, err
	}

	return []byte(base64.StdEncoding.EncodeToString(b)), nil
}

// UnmarshalText implements encoding.TextUnmarshaler.
func (sc *Recording) UnmarshalText(b []byte) error {
	dec, err := base64.StdEncoding.DecodeString(string(b))
	if err != nil {
		return fmt.Errorf("recording: decoding failed: %w", err)
	}

	return sc.UnmarshalBinary(dec)
}

// MarshalBinary implements encoding.BinaryMarshaler.
func (sc *Recording) MarshalBinary() ([]byte, error) {
	var w format.Writer

	w.UVarInt(sc.FormatVersion)
	w.UVarInt(sc.Version[0])
	w.UVarInt(sc.Version[1])
	w.UVarInt(sc.Version[2])
	w.String1(sc.ModeName)
	w.Write(sc.Perspective)

	for _, c := range sc.Cosmetic {
		w.String1(c.CharacterName)
	}

	if sc.FormatVersion >= 1 {
		w.UVarInt(sc.RematchCount)
	}

	if sc.FormatVersion >= 2 {
		w.UVarInt(sc.WonMatches)

		w.UVarInt(uint64(sc.Start.UnixNano()) / uint64(time.Millisecond))

		var extraFields uint64

		if sc.SpoilerGuard[0] != nil {
			extraFields |= 1 << 0
		}

		if sc.SpoilerGuard[1] != nil {
			extraFields |= 1 << 1
		}

		w.UVarInt(extraFields)

		if sc.SpoilerGuard[0] != nil {
			w.Write(sc.SpoilerGuard[0])
		}

		if sc.SpoilerGuard[1] != nil {
			w.Write(sc.SpoilerGuard[1])
		}
	}

	w.Write(sc.SharedSeed)
	w.Write(sc.PrivateSeed)

	cards, err := sc.marshalCards()
	if err != nil {
		return nil, err
	}

	w.UVarInt(uint64(len(cards)))

	for _, c := range cards {
		sub, done := w.SubWriter()

		sub.Bytes(c)

		done()
	}

	for _, d := range sc.InitialDeck {
		b, err := d.MarshalBinary()
		if err != nil {
			return nil, err
		}

		sub, done := w.SubWriter()

		sub.Bytes(b)

		done()
	}

	w.UVarInt(uint64(len(sc.Rounds)))

	for _, r := range sc.Rounds {
		w.Write(r.TurnSeed)

		if sc.FormatVersion >= 1 {
			w.Write(r.TurnSeed2)
		}

		for _, mask := range r.Ready {
			w.UVarInt(mask)
		}
	}

	return w.Data(), nil
}

// UnmarshalBinary implements encoding.BinaryUnmarshaler.
func (sc *Recording) UnmarshalBinary(b []byte) (err error) {
	defer format.Catch(&err)

	var r format.Reader

	r.Init(b)

	sc.FormatVersion = r.UVarInt()

	if sc.FormatVersion > 2 {
		return fmt.Errorf("recording: invalid recording version %d", sc.FormatVersion)
	}

	sc.Version[0] = r.UVarInt()
	sc.Version[1] = r.UVarInt()
	sc.Version[2] = r.UVarInt()
	sc.ModeName = r.String1()
	sc.Perspective = r.Byte()

	for i := range sc.Cosmetic {
		sc.Cosmetic[i].CharacterName = r.String1()
	}

	if sc.FormatVersion >= 1 {
		sc.RematchCount = r.UVarInt()
	}

	if sc.FormatVersion >= 2 {
		sc.WonMatches = r.UVarInt()

		start := r.UVarInt()
		sc.Start = time.Unix(int64(start/1000), int64(start%1000)*int64(time.Millisecond))

		extraFields := r.UVarInt()

		if extraFields&(1<<0) != 0 {
			sc.SpoilerGuard[0] = new([32]byte)
			r.Read(sc.SpoilerGuard[0])
		} else {
			sc.SpoilerGuard[0] = nil
		}

		if extraFields&(1<<1) != 0 {
			sc.SpoilerGuard[1] = new([32]byte)
			r.Read(sc.SpoilerGuard[1])
		} else {
			sc.SpoilerGuard[1] = nil
		}
	}

	r.Read(&sc.SharedSeed)
	r.Read(&sc.PrivateSeed)

	cards := make([]*format.Reader, r.UVarInt())
	for i := range cards {
		cards[i] = r.SubReader()
	}

	if err = sc.unmarshalCards(cards); err != nil {
		return err
	}

	for i := range sc.InitialDeck {
		sub := r.SubReader()

		if err = sc.InitialDeck[i].UnmarshalBinary(sub.Bytes(sub.Len())); err != nil {
			return err
		}
	}

	sc.Rounds = make([]RecordingRound, r.UVarInt())
	for i := range sc.Rounds {
		r.Read(&sc.Rounds[i].TurnSeed)

		if sc.FormatVersion >= 1 {
			r.Read(&sc.Rounds[i].TurnSeed2)
		}

		for j := range sc.Rounds[i].Ready {
			sc.Rounds[i].Ready[j] = r.UVarInt()
		}
	}

	return nil
}

func (sc *Recording) marshalCards() ([][]byte, error) {
	buffers := make([][]byte, 0, len(sc.CustomCards.Cards)+2)

	if sc.CustomCards.Mode != nil {
		b, err := sc.CustomCards.Mode.MarshalBinary()
		if err != nil {
			return nil, fmt.Errorf("card: encoding game mode: %w", err)
		}

		buffers = append(buffers, b)

		if sc.CustomCards.Variant != -1 {
			var w format.Writer

			w.UVarInt(uint64(sc.CustomCards.Variant))
			buffers = append(buffers, w.Data())
		}
	}

	for _, c := range sc.CustomCards.Cards {
		b, err := c.MarshalBinary()
		if err != nil {
			return nil, fmt.Errorf("card: encoding card: %w", err)
		}

		buffers = append(buffers, b)
	}

	return buffers, nil
}

func (sc *Recording) unmarshalCards(readers []*format.Reader) error {
	if sc.ModeName == "" && len(readers) != 0 {
		return fmt.Errorf("recording: %d custom cards for modeless recording", len(readers))
	}

	if sc.ModeName != "" && len(readers) == 0 {
		return fmt.Errorf("recording: no custom cards for mode=%q recording", sc.ModeName)
	}

	sc.CustomCards = Set{
		External: sc.ModeName,
		Variant:  -1,
	}

	if sc.CustomCards.External == "custom" {
		sc.CustomCards.External = ""
	}

	raw := make([]string, len(readers))
	rawCards := raw

	if len(readers) != 0 && readers[0].Len() != 0 && readers[0].Peek(1)[0] == 3 {
		b := readers[0].Bytes(readers[0].Len())

		raw[0] = base64.StdEncoding.EncodeToString(b)

		sc.CustomCards.Mode = &GameMode{}
		if err := sc.CustomCards.Mode.UnmarshalBinary(b); err != nil {
			return fmt.Errorf("recording: parsing game mode: %w", err)
		}

		readers = readers[1:]

		sc.CustomCards.Variant = -1

		if numVariants := len(sc.CustomCards.Mode.GetAll(FieldVariant)); numVariants != 0 && len(readers) != 0 {
			var r0 format.Reader

			r0.Init(readers[0].Peek(readers[0].Len()))

			err := func() (err error) {
				defer format.Catch(&err)

				sc.CustomCards.Variant = int(r0.UVarInt())

				if r0.Len() != 0 {
					return format.ErrExpectedEOF
				}

				return nil
			}()
			if err == nil {
				if sc.CustomCards.Variant >= numVariants {
					return fmt.Errorf("recording: variant number %d outside of range [0, %d)", sc.CustomCards.Variant, numVariants)
				}

				readers = readers[1:]
				rawCards = rawCards[1:]
			}
		}
	}

	cardBuf := make([]Def, len(readers))
	sc.CustomCards.Cards = make([]*Def, len(readers))

	for i, r := range readers {
		b := r.Bytes(r.Len())

		rawCards[i] = base64.StdEncoding.EncodeToString(b)

		sc.CustomCards.Cards[i] = &cardBuf[i]

		if err := sc.CustomCards.Cards[i].UnmarshalBinary(b); err != nil {
			return fmt.Errorf("recording: parsing card %d: %w", i, err)
		}
	}

	sc.CustomCardsRaw = strings.Join(raw, ",")

	if sc.Version[0] == 0 && sc.Version[1] <= 2 {
		if sc.CustomCards.Mode == nil {
			sc.CustomCards.Mode = &GameMode{}
		}

		sc.CustomCards.Mode.Fields = append(sc.CustomCards.Mode.Fields, &VanillaVersion{
			Version: BugFables105,
		})
	}

	if sc.Version[0] == 0 && sc.Version[1] == 3 && sc.Version[2] <= 8 {
		if sc.CustomCards.Mode == nil {
			sc.CustomCards.Mode = &GameMode{}
		}

		sc.CustomCards.Mode.Fields = append(sc.CustomCards.Mode.Fields, &VanillaVersion{
			Version: BugFables11,
		})
	} else if sc.Version[0] == 0 && sc.Version[1] == 3 && sc.Version[2] <= 26 {
		if sc.CustomCards.Mode == nil {
			sc.CustomCards.Mode = &GameMode{}
		}

		sc.CustomCards.Mode.Fields = append(sc.CustomCards.Mode.Fields, &VanillaVersion{
			Version: BugFables111,
		})
	}

	return nil
}

func RandomRecording(ctx context.Context, mode string, not ...string) (string, error) {
	u := []byte(internal.GetConfig(ctx).MatchRecordingBaseURL + "random")

	first := true

	if mode != "" && mode != "custom" {
		u = append(u, "?mode="...)
		first = false

		if i := strings.Index(mode, "."); i != -1 {
			mode = mode[:i]
		}

		if mode != "vanilla" {
			u = append(u, url.QueryEscape(mode)...)
		}
	}

	for _, n := range not {
		if first {
			u = append(u, '?')
			first = false
		} else {
			u = append(u, '&')
		}

		u = append(u, "not="...)
		u = append(u, n...)
	}

	b, err := internal.FetchBytes(ctx, string(u))
	if err != nil {
		return "", fmt.Errorf("card: fetching random recording id: %w", err)
	}

	return string(b), nil
}

// FetchRecording downloads and decodes a recording from the server, or
// decodes a base64-encoded recording, depending on the length of the code.
func FetchRecording(ctx context.Context, code string) (*Recording, error) {
	var (
		b   []byte
		err error
	)

	if len(code) < 30 {
		b, err = internal.FetchBytes(ctx, internal.GetConfig(ctx).MatchRecordingBaseURL+"get/"+code)
		if err != nil {
			return nil, fmt.Errorf("card: fetching recording %q: %w", code, err)
		}
	} else {
		b, err = base64.StdEncoding.DecodeString(code)
		if err != nil {
			return nil, fmt.Errorf("card: decoding recording: %w", err)
		}
	}

	var rec Recording

	return &rec, rec.UnmarshalBinary(b)
}
