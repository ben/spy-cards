//go:generate stringer -type Button -trimprefix Btn

package input

// Button represents a gamepad button.
type Button uint8

// Constants for Button.
const (
	BtnUp      Button = 0
	BtnDown    Button = 1
	BtnLeft    Button = 2
	BtnRight   Button = 3
	BtnConfirm Button = 4
	BtnCancel  Button = 5
	BtnSwitch  Button = 6
	BtnToggle  Button = 7
	BtnPause   Button = 8
	BtnHelp    Button = 9

	NumButtons = 10
)

// PackButtons combines the specified buttons into a bitmask.
// Order is not preserved, and any repeated buttons are only stored once.
func PackButtons(buttons ...Button) uint64 {
	var n uint64

	for _, btn := range buttons {
		n |= 1 << btn
	}

	return n
}

// UnpackButtons extracts the buttons from a given bitmask.
func UnpackButtons(mask uint64) []Button {
	var buttons []Button

	for i := 0; mask != 0; i, mask = i+1, mask>>1 {
		if mask&1 != 0 {
			buttons = append(buttons, Button(i))
		}
	}

	return buttons
}

// Held returns true if a button is currently held.
func (c *Context) Held(btn Button) bool {
	return c.state.held[btn]
}

// Consume returns true if a button is pressed,
// and consumes the button press indefinitely.
func (c *Context) Consume(btn Button) bool {
	if c.state.pressed[btn] {
		c.state.repeats[btn] = 0
		c.state.pressed[btn] = false

		return true
	}

	return false
}

// ConsumeAllowRepeat returns true if a button is pressed,
// and consumes the button press for a number of ticks.
func (c *Context) ConsumeAllowRepeat(btn Button, defaultDelay, minDelay uint64) bool {
	if c.state.pressed[btn] {
		delayTicks := minDelay
		if defaultDelay-minDelay > c.state.repeats[btn]*5 {
			delayTicks = defaultDelay - c.state.repeats[btn]*5
		}

		c.state.pressed[btn] = false
		c.state.wasPressed[btn] = c.state.tick + delayTicks
		c.state.repeats[btn]++

		return true
	}

	return false
}
