package input

import (
	"math"

	"golang.org/x/mobile/event/key"
)

// Mouse returns the mouse position and pressed state.
//
// This API may change in the future.
func (c *Context) Mouse() (x, y float32, click bool) {
	return c.state.x, c.state.y, c.state.click
}

// LastMouse returns the mouse state for the previous tick.
//
// This API may change in the future.
func (c *Context) LastMouse() (x, y float32, click bool) {
	return c.state.lastX, c.state.lastY, c.state.lastClick
}

// ConsumeClick sets the mouse drag distance to infinity.
//
// This API may change in the future.
func (c *Context) ConsumeClick() {
	c.state.drag = float32(math.Inf(1))
}

// IsMouseDrag returns true if the mouse has been dragged at least this many pixels.
//
// This API may change in the future.
func (c *Context) IsMouseDrag() bool {
	return c.state.drag >= 10
}

// Wheel returns the mouse wheel delta for this tick.
//
// This API may change in the future.
func (c *Context) Wheel() float32 {
	return c.state.wheel
}

// ConsumeKey takes a key event from the queue.
//
// This API may change in the future.
func (c *Context) ConsumeKey() (key.Event, bool) {
	if len(c.state.key) == 0 {
		return key.Event{}, false
	}

	e := c.state.key[0]
	c.state.key = c.state.key[1:]

	return e, true
}
