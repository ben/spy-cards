package arcade

import (
	"errors"
	"fmt"
	"time"

	"git.lubar.me/ben/spy-cards/format"
	"git.lubar.me/ben/spy-cards/input"
)

// Recording is a record of the buttons pressed in a Termacade game.
type Recording struct {
	Version        [3]uint64
	Game           Game
	Rules          GameRules
	Seed           string
	Start          time.Time
	Inputs         []RecordedInput
	FinalRandCount uint64
	PlayerName     string
}

// RecordedInput is one set of held buttons during a Termacade recording.
// The Buttons are held for 1+AdditionalTicks sixtieths of a second.
type RecordedInput struct {
	Buttons         []input.Button
	AdditionalTicks uint64
}

var errExpectedEOF = errors.New("arcade: extra data after end of recording")

type ErrRecordingVersion uint64

func (err ErrRecordingVersion) Error() string {
	return fmt.Sprintf("arcade: invalid recording format version %d", uint64(err))
}

// MarshalBinary implements encoding.BinaryMarshaler.
func (rec *Recording) MarshalBinary() ([]byte, error) {
	var w format.Writer

	w.UVarInt(1) // format version
	w.UVarInt(rec.Version[0])
	w.UVarInt(rec.Version[1])
	w.UVarInt(rec.Version[2])
	w.UVarInt(uint64(rec.Game))

	sub, done := w.SubWriter()

	if rec.Rules != nil {
		MarshalRules(sub, rec.Rules)
	}

	done()

	w.String(rec.Seed)
	w.UVarInt(uint64(rec.Start.Unix()*1000) + uint64(rec.Start.Nanosecond()/int(time.Millisecond)))

	sub, done = w.SubWriter()

	for _, i := range rec.Inputs {
		sub.UVarInt(input.PackButtons(i.Buttons...))
		sub.UVarInt(i.AdditionalTicks)
	}

	done()

	w.UVarInt(rec.FinalRandCount)
	w.String(rec.PlayerName)

	return w.Data(), nil
}

// UnmarshalBinary implements encoding.BinaryUnmarshaler.
func (rec *Recording) UnmarshalBinary(b []byte) (err error) {
	defer format.Catch(&err)

	var r format.Reader

	r.Init(b)

	var dec Recording

	switch formatVersion := r.UVarInt(); formatVersion {
	case 0, 1:
		dec.Version[0] = r.UVarInt()
		dec.Version[1] = r.UVarInt()
		dec.Version[2] = r.UVarInt()

		dec.Game = Game(r.UVarInt())

		switch dec.Game {
		case MiteKnight:
			dec.Rules = &MiteKnightRules{}
		case FlowerJourney:
			dec.Rules = &FlowerJourneyRules{}
		default:
			return fmt.Errorf("arcade: don't know how to read game rules for %v: %w", dec.Game, ErrUnknownGame)
		}

		UnmarshalRules(r.SubReader(), dec.Rules)

		dec.Seed = r.String()
		start := r.UVarInt()
		dec.Start = time.Unix(int64(start/1000), int64(start%1000)*int64(time.Millisecond))

		sub := r.SubReader()

		if formatVersion == 0 && dec.Game == FlowerJourney {
			dec.Inputs = append(dec.Inputs, RecordedInput{})
			rules := dec.Rules.(*FlowerJourneyRules)

			for i := 60 + uint64(rules.CountdownLength)*uint64(rules.CountdownTime*60) + uint64(rules.GoTime*60) + 5; i > 0; i-- {
				dec.Inputs = append(dec.Inputs, RecordedInput{})
			}

			for sub.Len() != 0 {
				n := int(sub.UVarInt())
				for i := 0; i < n; i++ {
					dec.Inputs = append(dec.Inputs, RecordedInput{})
				}

				dec.Inputs[len(dec.Inputs)-1].Buttons = []input.Button{input.BtnConfirm}
			}

			dec.Inputs[len(dec.Inputs)-1].Buttons = nil
			dec.Inputs = append(dec.Inputs, RecordedInput{})
		} else {
			for sub.Len() != 0 {
				dec.Inputs = append(dec.Inputs, RecordedInput{
					Buttons:         input.UnpackButtons(sub.UVarInt()),
					AdditionalTicks: sub.UVarInt(),
				})
			}
		}

		dec.FinalRandCount = r.UVarInt()
		dec.PlayerName = r.String()
	default:
		return ErrRecordingVersion(formatVersion)
	}

	if r.Len() != 0 {
		return errExpectedEOF
	}

	*rec = dec

	return nil
}
