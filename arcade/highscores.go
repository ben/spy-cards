//go:generate stringer -type HighScoresMode

package arcade

// HighScoresMode is a high scores filtering mode.
type HighScoresMode uint8

// Constants for Mode.
const (
	Recent    HighScoresMode = 'r'
	Weekly    HighScoresMode = 'w'
	Quarterly HighScoresMode = 'q'
	AllTime   HighScoresMode = 'a'
)
