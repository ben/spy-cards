package flowerjourney

import (
	"math"

	"git.lubar.me/ben/spy-cards/arcade/game"
	"git.lubar.me/ben/spy-cards/gfx/sprites"
	"git.lubar.me/ben/spy-cards/input"
)

// InitAI implements game.Interface.
func (fj *FlowerJourney) InitAI() *game.AIConfig {
	return &game.AIConfig{
		NumInput: 1 + // x velocity
			1 + // y velocity
			1 + // distance to ceiling
			1 + // distance to floor
			1 + // distance to nearest wall bottom
			1 + // distance to nearest wall top
			1 + // x distance to nearest wall
			2 + // item coordinate
			2 + // enemy coordinate
			1 + // invuln timer
			1, // item type

		Press: []game.WeightedButton{
			{
				Button: input.BtnConfirm,
				Weight: 0.045,
			},
		},

		InputDesc: []game.AIInputDesc{
			{Min: -0.2, Max: 0},
			{Min: -10, Max: 10},
			{Min: -5, Max: 5},
			{Min: -5, Max: 5},
			{Min: -5, Max: 5},
			{Min: -5, Max: 5},
			{Min: 0, Max: 20},
			{Min: -10, Max: 10},
			{Min: -3, Max: 3},
			{Min: -10, Max: 10},
			{Min: -3, Max: 3},
			{Min: 0, Max: 5},
			{Min: -1, Max: 1, Enum: map[float64]sprites.Color{
				-1:                  {R: 0, G: 0, B: 0, A: 255},
				float64(itemFlower): {R: 255, G: 102, B: 255, A: 255},
				float64(itemHoney):  {R: 255, G: 136, B: 0, A: 255},
			}},
		},
	}
}

// AIInput implements game.Interface.
func (fj *FlowerJourney) AIInput() (input []float64, reward float64) {
	reward = -fj.lastScore
	fj.lastScore = fj.state.Score
	reward += fj.state.Score

	if fj.gameState == stateEnding && !fj.didLastAITick {
		reward -= 99
		fj.didLastAITick = true
	} else if fj.gameState != stateActive {
		return nil, math.NaN()
	}

	reward /= 100

	nearestWall := fj.walls[0]
	if nearestWall.x < fj.playerX-0.5 {
		nearestWall.x += 100
	}

	for _, w := range fj.walls {
		if w.x < fj.playerX-0.5 {
			w.x += 100
		}

		if nearestWall.x > w.x {
			nearestWall = w
		}
	}

	distanceFromTop := 5.15 - (fj.playerY - 0.05 + 0.25)
	distanceFromBottom := (fj.playerY - 0.05 - 0.25) - 0.2012
	distanceFromLower := (fj.playerY - 0.05 - 0.25) - (nearestWall.y + 1.8375)
	distanceFromUpper := (nearestWall.y + nearestWall.gap - 1.8375) - (fj.playerY - 0.05 + 0.25)

	distanceReward := math.Min(math.Min(distanceFromTop, distanceFromBottom), math.Min(distanceFromLower, distanceFromUpper))

	reward += distanceReward

	var itemY, enemyY, itemType float64

	if fj.itemEnabled {
		itemY = fj.itemY - fj.playerY
		itemType = float64(fj.itemType)

		itemReward := 1 - math.Abs(itemY)
		dx := fj.itemX - fj.playerX - 1

		if dx >= 0 {
			if dx < 0.125 {
				dx = 0.125
			}

			itemReward /= dx
		} else if dx > -1 {
			itemReward -= 2 + dx*2
		}

		reward += itemReward
	} else {
		itemType = -1
	}

	if fj.enemyEnabled {
		enemyY = fj.enemyY - fj.playerY
	}

	input = []float64{
		fj.speed,
		fj.playerYVelocity,
		distanceFromTop,
		distanceFromBottom,
		distanceFromUpper,
		distanceFromLower,
		nearestWall.x - fj.playerX,
		math.Min(10, fj.itemX-fj.playerX),
		itemY,
		math.Min(10, fj.enemyX-fj.playerX),
		enemyY,
		float64(fj.invul) / game.FrameRate,
		itemType,
	}

	return input, reward
}
