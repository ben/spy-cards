package miteknight

import (
	"git.lubar.me/ben/spy-cards/input"
)

type direction = input.Button

const (
	dirUp    direction = input.BtnUp
	dirDown  direction = input.BtnDown
	dirLeft  direction = input.BtnLeft
	dirRight direction = input.BtnRight
)

var modDir = [4][4]direction{
	{
		dirUp,
		dirDown,
		dirLeft,
		dirRight,
	},
	{
		dirDown,
		dirUp,
		dirRight,
		dirLeft,
	},
	{
		dirLeft,
		dirRight,
		dirDown,
		dirUp,
	},
	{
		dirRight,
		dirLeft,
		dirUp,
		dirDown,
	},
}

func getFrontPos(x, z int, dir direction) (int, int) {
	switch dir {
	case dirUp:
		return x, z + 1
	case dirDown:
		return x, z - 1
	case dirLeft:
		return x - 1, z
	case dirRight:
		return x + 1, z
	default:
		return x, z
	}
}

type coord3 struct {
	x, y, z float32
}

type roomBounds struct {
	x0, y0, x1, y1 int
}
