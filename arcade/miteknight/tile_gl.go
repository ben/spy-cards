//go:build !headless
// +build !headless

package miteknight

import (
	_ "embed" // embedded shaders
	"encoding/binary"

	"git.lubar.me/ben/spy-cards/gfx"
	"git.lubar.me/ben/spy-cards/gfx/sprites"
)

func (d *dungeonFloor) appendTileGeometry(staticData, staticElements []uint8, x, y, baseX, baseY int) ([]uint8, []uint8) {
	tt := tileTypes[d.floorMap[x][y]]
	if tt.wallSprite != nil {
		for dir := direction(0); dir < 4; dir++ {
			var dx, dy, up, down, left, right int

			switch dir {
			case dirUp:
				dy = 1
				up = 1
			case dirDown:
				dy = -1
				down = 1
			case dirLeft:
				dx = -1
				left = 1
			case dirRight:
				dx = 1
				right = 1
			}

			if x+dx >= 0 && x+dx < d.sizeX &&
				y+dy >= 0 && y+dy < d.sizeY &&
				d.floorMap[x+dx][y+dy] != tileNone &&
				tileTypes[d.floorMap[x+dx][y+dy]].wallSprite == nil {
				n := uint16(len(staticData) / 8)

				dx2 := (dx + 1) >> 1
				dy2 := (dy + 1) >> 1

				staticData = append(staticData,
					uint8(x+dx2+up-baseX), 0, uint8(y+dy2+left-baseY),
					texCoord(tt.wallSprite, false, false),
					texCoord(tt.wallSprite, true, true),
					tt.color.R, tt.color.G, tt.color.B,

					uint8(x+dx2+down-baseX), 0, uint8(y+dy2+right-baseY),
					texCoord(tt.wallSprite, false, true),
					texCoord(tt.wallSprite, true, true),
					tt.color.R, tt.color.G, tt.color.B,

					uint8(x+dx2+down-baseX), 1, uint8(y+dy2+right-baseY),
					texCoord(tt.wallSprite, false, true),
					texCoord(tt.wallSprite, true, false),
					tt.color.R, tt.color.G, tt.color.B,

					uint8(x+dx2+up-baseX), 1, uint8(y+dy2+left-baseY),
					texCoord(tt.wallSprite, false, false),
					texCoord(tt.wallSprite, true, false),
					tt.color.R, tt.color.G, tt.color.B,
				)

				i := len(staticElements)
				staticElements = append(staticElements, make([]byte, 6*2)...)
				binary.LittleEndian.PutUint16(staticElements[i:], n+0)
				binary.LittleEndian.PutUint16(staticElements[i+2:], n+1)
				binary.LittleEndian.PutUint16(staticElements[i+4:], n+2)
				binary.LittleEndian.PutUint16(staticElements[i+6:], n+2)
				binary.LittleEndian.PutUint16(staticElements[i+8:], n+3)
				binary.LittleEndian.PutUint16(staticElements[i+10:], n+0)
			}
		}
	} else {
		n := uint16(len(staticData) / 8)

		staticData = append(staticData,
			uint8(x-baseX), 0, uint8(y-baseY),
			texCoord(sprites.MKFloor, false, false),
			texCoord(sprites.MKFloor, true, false),
			tt.color.R, tt.color.G, tt.color.B,

			uint8(x+1-baseX), 0, uint8(y-baseY),
			texCoord(sprites.MKFloor, false, false),
			texCoord(sprites.MKFloor, true, true),
			tt.color.R, tt.color.G, tt.color.B,

			uint8(x+1-baseX), 0, uint8(y+1-baseY),
			texCoord(sprites.MKFloor, false, true),
			texCoord(sprites.MKFloor, true, true),
			tt.color.R, tt.color.G, tt.color.B,

			uint8(x-baseX), 0, uint8(y+1-baseY),
			texCoord(sprites.MKFloor, false, true),
			texCoord(sprites.MKFloor, true, false),
			tt.color.R, tt.color.G, tt.color.B,

			uint8(x-baseX), 1, uint8(y-baseY),
			texCoord(sprites.MKCeiling, false, false),
			texCoord(sprites.MKCeiling, true, false),
			tt.color.R, tt.color.G, tt.color.B,

			uint8(x-baseX), 1, uint8(y+1-baseY),
			texCoord(sprites.MKCeiling, false, false),
			texCoord(sprites.MKCeiling, true, true),
			tt.color.R, tt.color.G, tt.color.B,

			uint8(x+1-baseX), 1, uint8(y+1-baseY),
			texCoord(sprites.MKCeiling, false, true),
			texCoord(sprites.MKCeiling, true, true),
			tt.color.R, tt.color.G, tt.color.B,

			uint8(x+1-baseX), 1, uint8(y-baseY),
			texCoord(sprites.MKCeiling, false, true),
			texCoord(sprites.MKCeiling, true, false),
			tt.color.R, tt.color.G, tt.color.B,
		)

		i := len(staticElements)
		staticElements = append(staticElements, make([]byte, 6*2*2)...)
		binary.LittleEndian.PutUint16(staticElements[i:], n+0)
		binary.LittleEndian.PutUint16(staticElements[i+2:], n+1)
		binary.LittleEndian.PutUint16(staticElements[i+4:], n+2)
		binary.LittleEndian.PutUint16(staticElements[i+6:], n+2)
		binary.LittleEndian.PutUint16(staticElements[i+8:], n+3)
		binary.LittleEndian.PutUint16(staticElements[i+10:], n+0)
		binary.LittleEndian.PutUint16(staticElements[i+12:], n+4)
		binary.LittleEndian.PutUint16(staticElements[i+14:], n+5)
		binary.LittleEndian.PutUint16(staticElements[i+16:], n+6)
		binary.LittleEndian.PutUint16(staticElements[i+18:], n+6)
		binary.LittleEndian.PutUint16(staticElements[i+20:], n+7)
		binary.LittleEndian.PutUint16(staticElements[i+22:], n+4)
	}

	return staticData, staticElements
}

func texCoord(sprite *sprites.Sprite, y, far bool) uint8 {
	var coord float32

	if y {
		if far {
			coord = sprite.T1
		} else {
			coord = sprite.T0
		}
	} else {
		if far {
			coord = sprite.S1
		} else {
			coord = sprite.S0
		}
	}

	return uint8(coord * 8)
}

var (
	//go:embed mkgeom.vs
	geomVertex string
	//go:embed mkgeom.fs
	geomFragment string

	geomProgram = gfx.Shader("mkgeom", geomVertex, geomFragment, geomVertex, geomFragment)

	geomTex = geomProgram.Uniform("tex")

	geomTime        = geomProgram.Uniform("time")
	geomPerspective = geomProgram.Uniform("perspective")
	geomCamera      = geomProgram.Uniform("camera")
	geomFogEnd      = geomProgram.Uniform("fog_end")

	posAttrib   = geomProgram.Attrib("in_pos")
	texAttrib   = geomProgram.Attrib("in_tex")
	colorAttrib = geomProgram.Attrib("in_color")
)

var tileTypes = [...]struct {
	wallSprite *sprites.Sprite
	color      sprites.Color
}{
	tileWall: {
		wallSprite: sprites.MKWall,
		color:      sprites.MKStone,
	},
	tileFree: {
		wallSprite: nil,
		color:      sprites.MKStone,
	},
	tileKey: {
		wallSprite: nil,
		color:      sprites.MKStone,
	},
	tilePotion: {
		wallSprite: nil,
		color:      sprites.MKStone,
	},
	tileDoor: {
		wallSprite: nil,
		color:      sprites.Yellow,
	},
	tileStairs: {
		wallSprite: sprites.MKStairs,
		color:      sprites.Red,
	},
	tileWall2: {
		wallSprite: sprites.MKWall2,
		color:      sprites.White,
	},
}
