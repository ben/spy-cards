// Package miteknight implements the Termacade game Mite Knight.
package miteknight

import (
	"context"
	"fmt"
	"math"
	"math/rand"
	"strings"

	"git.lubar.me/ben/spy-cards/arcade"
	"git.lubar.me/ben/spy-cards/arcade/game"
	"git.lubar.me/ben/spy-cards/audio"
	"git.lubar.me/ben/spy-cards/gfx/sprites"
	"git.lubar.me/ben/spy-cards/input"
	"git.lubar.me/ben/spy-cards/internal"
)

// MiteKnight holds the game state for Mite Knight.
type MiteKnight struct {
	rules arcade.MiteKnightRules
	state *game.State

	camPos            coord3
	camTarget         *coord3
	camRotationTarget float32
	camRotation       float32
	timeMin           float64
	timeSec           float64
	floor             int
	floors            []*dungeonFloor
	particleRand      *rand.Rand
	particles         []*particleEffect
	pause             uint64
	timeText          string
	stopDelay         float64
	inputDelay        float64
	compassPos        coord3
	compassRotation   float32
	aiReward          float64
	lastScoreTick     uint64
	scoreBefore       float64
	recentTiles       [][3]int
	clockWait         uint64
	inputWait         uint64
	floorWait         uint64
	fadeWait          uint64
	exitWait          uint64
	allowExitAfter    uint64
	allowCancelExit   bool
	winner            bool
	canInput          bool
	rotateX           bool
	compassEnabled    bool
	oldBlock          bool
	overrideHUD       func()

	batch *sprites.Batch
	tb    *sprites.Batch
}

// New returns a game.CreateFunc for Mite Knight.
func New(rules arcade.MiteKnightRules) game.CreateFunc {
	return func(state *game.State) game.Interface {
		return &MiteKnight{
			rules: rules,
			state: state,

			allowExitAfter: ^uint64(0),
		}
	}
}

// Type implements game.Interface.
func (mk *MiteKnight) Type() arcade.Game {
	return arcade.MiteKnight
}

// Rules implements game.Interface.
func (mk *MiteKnight) Rules() arcade.GameRules {
	return &mk.rules
}

// HighScore implements game.Interface.
func (mk *MiteKnight) HighScore() float64 {
	return 9500
}

// Init implements game.Interface.
func (mk *MiteKnight) Init(ctx context.Context) error {
	if mk.state.Playback != nil {
		internal.SetTitle("Mite Knight Recording (" + mk.state.Playback.PlayerName + ", " + mk.state.Playback.Start.Format("Jan 2, 2006") + ") - Termacade")
	} else {
		internal.SetTitle("Mite Knight - Termacade")
	}

	mk.state.Camera.SetDefaults()
	mk.state.CRT.SetDefaults()
	audio.StopMusic()

	if mk.state.Playback == nil {
		audio.MiteKnightIntro.PlayMusic(0, true)
	}

	return nil
}

// Generate implements game.Interface.
func (mk *MiteKnight) Generate(ctx context.Context, progress func(percent int)) error {
	mk.floors = make([]*dungeonFloor, int(math.Ceil(mk.rules.FloorCount)))

	for i := range mk.floors {
		progress(int(float64(i) / mk.rules.FloorCount * 100))

		sizeX := mk.rules.MapSize[0] + math.Floor(mk.rules.MapSizePerFloor[0]*float64(i))
		sizeY := mk.rules.MapSize[1] + math.Floor(mk.rules.MapSizePerFloor[1]*float64(i))

		mk.floors[i] = &dungeonFloor{floor: i, sizeX: int(sizeX), sizeY: int(sizeY)}

		if err := mk.floors[i].generate(ctx, mk); err != nil {
			return fmt.Errorf("miteknight: generating floor %d: %w", i+1, err)
		}
	}

	return nil
}

// StartPressed implements game.Interface.
func (mk *MiteKnight) StartPressed() {
	audio.MKKey.PlaySoundGlobal(0, 0, 0)
}

// StartGameplay implements game.Interface.
func (mk *MiteKnight) StartGameplay(_ context.Context) error {
	mk.camPos = coord3{x: 1, y: 2.25, z: -7.25}
	mk.timeMin = math.Floor(mk.rules.TimeLimit / 60)
	mk.timeSec = math.Mod(mk.rules.TimeLimit, 60)

	mk.state.FadeTo(1)
	mk.clockWait = mk.state.ComputeWait(1)
	mk.inputWait = mk.clockWait

	mk.state.AIClearButtons()

	mk.StartMusic()

	return nil
}

// StartMusic implements game.Interface.
func (mk *MiteKnight) StartMusic() {
	audio.MiteKnight.PlayMusic(0, true)
}

func (mk *MiteKnight) clock() bool {
	if mk.pause == 0 && mk.canInput {
		mk.timeSec--
		if mk.timeSec < 0 && mk.timeMin > 0 {
			mk.timeMin--
			mk.timeSec = 59
		}

		if mk.timeMin <= 0 && mk.timeSec < 0 && mk.canInput {
			mk.canInput = false
			floor := mk.floors[mk.floor]
			floor.player.hp = 0
			floor.doDamage(floor.player)

			return false
		}

		if mk.timeMin <= 0 && mk.timeSec <= 0 {
			mk.timeMin = 0
			mk.timeSec = 0
		}

		mk.timeText = fmt.Sprintf("%.0f:%02.0f", mk.timeMin, mk.timeSec)
	}

	return true
}

// Logic implements game.Interface.
func (mk *MiteKnight) Logic(ctx context.Context) error {
	mk.particleRand = nil

	if mk.inputWait != 0 {
		mk.inputWait--
		if mk.inputWait == 0 {
			mk.canInput = true
		}
	}

	if mk.clockWait != 0 {
		mk.clockWait--
		if mk.clockWait == 0 && mk.clock() {
			mk.clockWait = mk.state.ComputeWait(1)
		}
	}

	if mk.floorWait != 0 {
		mk.floorWait--
		if mk.floorWait == 0 {
			mk.floor++
			mk.camPos = coord3{x: 1, y: 0, z: 1}
			mk.camRotation = 0
			mk.camRotationTarget = 0
		}
	}

	if mk.fadeWait != 0 {
		mk.fadeWait--
		if mk.fadeWait == 0 {
			mk.state.FadeTo(1)
		}
	}

	if mk.exitWait != 0 {
		mk.exitWait--
		if mk.exitWait == 0 {
			mk.state.Exit = true
		}
	}

	if mk.state.TickCount >= mk.allowExitAfter && (mk.state.Input.Consume(input.BtnConfirm) || (mk.allowCancelExit && mk.state.Input.Consume(input.BtnCancel))) {
		mk.state.AIClearButtons()
		mk.state.FadeTo(0)
		mk.exitWait = mk.state.ComputeWait(1) + 1
	}

	if mk.floor >= len(mk.floors) {
		return nil
	}

	floor := mk.floors[mk.floor]

	if !mk.rules.PauseStopsAnimations || mk.pause == 0 {
		floor.player.processMove(mk)

		for _, enemy := range floor.enemies {
			enemy.processMove(mk)
		}
	}

	mk.rotateX = false

	camTarget := coord3{
		x: float32(floor.player.x) + floor.player.ox(),
		y: floor.player.height,
		z: float32(floor.player.z) + floor.player.oz(),
	}

	if !mk.rules.PauseStopsAnimations || mk.pause == 0 {
		if mk.camTarget != nil {
			camTarget = *mk.camTarget
		}

		mk.camPos = coord3{
			x: game.Lerp32(mk.camPos.x, camTarget.x, float32(mk.rules.CamFollowSpeed)),
			y: game.Lerp32(mk.camPos.y, camTarget.y, float32(mk.rules.CamFollowSpeed)),
			z: game.Lerp32(mk.camPos.z, camTarget.z, float32(mk.rules.CamFollowSpeed)),
		}

		rotationOffset := mk.camRotationTarget - mk.camRotation

		for rotationOffset < -math.Pi {
			rotationOffset += math.Pi * 2
		}

		for rotationOffset > math.Pi {
			rotationOffset -= math.Pi * 2
		}

		mk.camRotation += rotationOffset * float32(mk.rules.CamRotationSpeed)
	}

	if mk.pause != 0 && mk.rules.PauseOverview {
		mk.camPos = coord3{
			x: float32(floor.sizeX) / 2,
			y: float32(math.Max(float64(floor.sizeX), float64(floor.sizeY)) / 2.25),
			z: float32(floor.sizeY) / 4,
		}
		mk.camRotation = 0
		mk.rotateX = true
	}

	for _, item := range floor.items {
		tick := mk.state.TickCount

		if mk.rules.PauseStopsAnimations && mk.pause != 0 {
			tick = mk.pause
		}

		item.height = float32(math.Sin(float64(tick)/game.FrameRate)*0.1 + 0.5)
	}

	if mk.canInput {
		switch {
		case mk.pause == 0:
			mk.enemyAI()

			diff := coord3{
				x: camTarget.x - mk.camPos.x,
				y: camTarget.y - mk.camPos.y,
				z: camTarget.z - mk.camPos.z,
			}

			if diff.x*diff.x+diff.y*diff.y+diff.z*diff.z > 10 {
				mk.camPos = camTarget
			}

			mk.processInput()

			mk.state.Score = math.Min(math.Max(mk.state.Score, 0), math.Pow(10, mk.rules.ScoreDigits)-1)
		case mk.state.Input.Consume(input.BtnConfirm):
			mk.pause = 0
		case mk.state.Input.Consume(input.BtnCancel):
			mk.endGame()
		}
	}

	return nil
}

func (mk *MiteKnight) processInput() {
	floor := mk.floors[mk.floor]

	if mk.stopDelay < mk.rules.CompassDelay*60 {
		mk.stopDelay++
		mk.compassEnabled = false
	} else if !mk.compassEnabled {
		mk.compassPos = coord3{
			x: float32(floor.player.x),
			y: 0.65,
			z: float32(floor.player.z),
		}

		target := floor.compassTarget[0]

		if floor.gotKey {
			target = floor.compassTarget[1]
		}

		mk.compassRotation = float32(math.Atan2(float64(target.x-float32(floor.player.x)), float64(target.z-float32(floor.player.z))))

		mk.compassEnabled = true
	}

	floor.player.blocking = mk.state.Input.Held(input.BtnConfirm)
	if floor.player.blocking != mk.oldBlock {
		sound := audio.MKKey

		if floor.player.iframeBlock {
			sound = audio.FunnyStep

			// making loud noises isn't cool
			mk.aiReward -= 0.25
		}

		floor.player.iframeBlock = true

		if floor.player.blocking {
			sound.PlaySound(0, 0.9, 0, float32(floor.player.x), 0, float32(floor.player.z))
		} else {
			sound.PlaySound(0, 0, 0, float32(floor.player.x), 0, float32(floor.player.z))
		}

		mk.oldBlock = floor.player.blocking
	}

	if floor.player.iframes <= 0 {
		floor.player.iframeBlock = false
	}

	if floor.player.actionCooldown <= 0 {
		if floor.player.blocking {
			floor.player.sprite = sprites.MKKnightBlock
		} else {
			floor.player.sprite = sprites.MKKnightIdle
		}
	} else {
		floor.player.actionCooldown--
	}

	if floor.player.iframes > 0 {
		floor.player.iframes--
	}

	if mk.inputDelay > 0 {
		mk.inputDelay--
	} else if floor.player.playerMoving == 0 && mk.inputDelay <= 0 {
		localDir := modDir[floor.player.direction]

		switch {
		case mk.state.Input.Held(input.BtnUp):
			if floor.isFrontFree(floor.player.x, floor.player.z, floor.player.direction) {
				audio.MKWalk.PlaySound(0, 0, 0, float32(floor.player.x), 0, float32(floor.player.z))
				floor.moveForward(floor.player, floor.player.direction, true)
				mk.stopDelay = 0
			} else {
				x, z := getFrontPos(floor.player.x, floor.player.z, floor.player.direction)
				entityInPos := floor.entityInPos(x, z)

				switch {
				case entityInPos != nil:
					if floor.player.blocking {
						// trying to block an enemy to death isn't cool
						mk.aiReward -= 0.125
					} else {
						if !entityInPos.blocking {
							floor.doDamage(entityInPos)
							floor.doAction(actionAttacking, 15)

							// hurting enemies is cool
							mk.aiReward++
						}

						mk.inputDelay = 15
					}
				case floor.hasKey:
					if floor.floorMap[x][z] == tileDoor {
						// unlocking doors is cool
						mk.aiReward++

						audio.MKOpen.PlaySound(0, 0, 0, float32(x), 0, float32(z))
						mk.state.Score += mk.rules.ScorePerDoor
						floor.floorMap[x][z] = tileFree
						floor.hasKey = false
					}
				case floor.floorMap[x][z] == tileStairs && (mk.timeMin > 0 || mk.timeSec > 1):
					mk.floorChange(x, z)
				default:
					// walking into walls isn't cool
					mk.aiReward -= 0.125
				}
				floor.player.iframes = 25
			}
		case mk.state.Input.Held(input.BtnDown):
			if floor.isFrontFree(floor.player.x, floor.player.z, localDir[dirDown]) {
				floor.moveForward(floor.player, localDir[dirDown], true)
				audio.MKWalk.PlaySound(0, 0, 0, float32(floor.player.x), 0, float32(floor.player.z))
				mk.stopDelay = 0
			}
		case mk.state.Input.Consume(input.BtnPause) && floor.player.hp > 0:
			mk.pause = mk.state.TickCount
			mk.stopDelay = 0
			mk.compassEnabled = false
			audio.PeacockSpiderNPCSummonSuccess.PlaySoundGlobal(0, 0, 0)
		case floor.player.blocking:
			if mk.state.Input.Held(input.BtnConfirm) {
				if mk.state.Input.Held(input.BtnLeft) {
					if floor.isFrontFree(floor.player.x, floor.player.z, localDir[dirLeft]) {
						mk.stopDelay = 0
						floor.moveForward(floor.player, localDir[dirLeft], true)
						audio.MKWalk.PlaySound(0, 0, 0, float32(floor.player.x), 0, float32(floor.player.z))
					} else {
						// strafing into stuff isn't cool
						mk.aiReward -= 0.125
					}
				} else if mk.state.Input.Held(input.BtnRight) {
					if floor.isFrontFree(floor.player.x, floor.player.z, localDir[dirRight]) {
						mk.stopDelay = 0
						floor.moveForward(floor.player, localDir[dirRight], true)
						audio.MKWalk.PlaySound(0, 0, 0, float32(floor.player.x), 0, float32(floor.player.z))
					} else {
						// strafing into stuff isn't cool
						mk.aiReward -= 0.125
					}
				}
			}
		case mk.state.Input.Consume(input.BtnLeft):
			floor.switchDirection(true)
			mk.inputDelay = 10
			mk.stopDelay = 0
			audio.MKWalk.PlaySound(0, 0.9, 0, float32(floor.player.x), 0, float32(floor.player.z))
		case mk.state.Input.Consume(input.BtnRight):
			floor.switchDirection(false)
			mk.inputDelay = 10
			mk.stopDelay = 0
			audio.MKWalk.PlaySound(0, 0.9, 0, float32(floor.player.x), 0, float32(floor.player.z))
		}
	}
}

func (mk *MiteKnight) enemyAI() {
	floor := mk.floors[mk.floor]
	for _, enemy := range floor.enemies {
		if enemy.iframes > 0 {
			enemy.iframes--
		}

		if enemy.cooldown <= 0 && enemy.iframes <= 0 {
			if !enemy.dead {
				dx := float64(enemy.x - floor.player.x)
				dz := float64(enemy.z - floor.player.z)

				switch enemy.entityType {
				case entityAnt:
					if dx*dx+dz*dz < 3.5*3.5 {
						enemy.direction = floor.dirToPlayer(enemy)
						enemy.cooldown = mk.state.RNG.RangeInt(25, 35)
					} else {
						enemy.direction = direction(mk.state.RNG.RangeInt(0, 4))
						enemy.cooldown = mk.state.RNG.RangeInt(25, 70)
					}

					if floor.isFrontFree(enemy.x, enemy.z, enemy.direction) {
						floor.moveForward(enemy, enemy.direction, false)
					} else {
						x, z := getFrontPos(enemy.x, enemy.z, enemy.direction)
						if floor.player.x == x && floor.player.z == z {
							if floor.player.iframes <= 0 && !floor.player.blocking {
								// getting hurt isn't cool
								mk.aiReward--

								floor.doDamage(floor.player)
								enemy.cooldown += mk.state.RNG.RangeInt(25, 35)
							}
						}
					}
				case entityWizard:
					if !enemy.special {
						enemy.direction = floor.dirToPlayer(enemy)

						x, z := getFrontPos(enemy.x, enemy.z, enemy.direction)

						if floor.player.x == x && floor.player.z == z {
							if floor.player.iframes <= 0 && !floor.player.blocking {
								// getting hit point-blank by a fireball isn't cool
								mk.aiReward--

								floor.doDamage(floor.player)
							}

							floor.tempBounce(enemy)
							enemy.cooldown = 30
							enemy.special = true
						} else {
							if enemy.child == nil {
								fireball := floor.newEnemy(entityFireball, -2, -2)
								fireball.dead = true
								floor.enemies = append(floor.enemies, fireball)
								enemy.child = fireball
							}
							if enemy.child.dead && floor.player.iframes <= 0 && dx*dx+dz*dz < 5.5*5.5 {
								floor.tempBounce(enemy)
								enemy.special = true
								enemy.cooldown = 50
								audio.Shot2.PlaySound(0, 0, 0, float32(enemy.x), 0, float32(enemy.z))
								enemy.child.direction = enemy.direction
								enemy.child.height = 0.5
								enemy.child.dead = false
								enemy.child.cooldown = 0
								enemy.child.hp = 1
								enemy.child.x = enemy.x
								enemy.child.z = enemy.z
							}
						}
					} else {
						enemy.special = false
						enemy.cooldown = mk.state.RNG.RangeInt(25, 50)
					}
				case entityFireball:
					if floor.isFrontFree(enemy.x, enemy.z, enemy.direction) {
						floor.moveForward(enemy, enemy.direction, false)
					} else {
						x, z := getFrontPos(enemy.x, enemy.z, enemy.direction)

						if floor.player.x == x && floor.player.z == z {
							if floor.player.iframes <= 0 && !floor.player.blocking {
								// getting hit by fireballs isn't cool
								mk.aiReward--

								floor.doDamage(floor.player)
							}
						}

						enemy.hp = 0
						floor.doDamage(enemy)
					}

					enemy.cooldown = 10
				}
			}
		} else {
			enemy.cooldown--
		}

		if !enemy.dead {
			direction := floor.directionToPlayer(enemy.direction)
			if direction == dirRight {
				direction = dirLeft
				enemy.flipX = false
			} else {
				enemy.flipX = true
			}

			switch enemy.entityType {
			case entityAnt:
				switch direction {
				case dirUp:
					enemy.sprite = sprites.MKAntFront
				case dirLeft:
					enemy.sprite = sprites.MKAntSide
				case dirDown:
					enemy.sprite = sprites.MKAntBack
				}
			case entityWizard:
				if enemy.special {
					switch direction {
					case dirUp:
						enemy.sprite = sprites.MKWizardCastFront
					case dirLeft:
						enemy.sprite = sprites.MKWizardCastSide
					case dirDown:
						enemy.sprite = sprites.MKWizardCastBack
					}
				} else {
					switch direction {
					case dirUp:
						enemy.sprite = sprites.MKWizardFront
					case dirLeft:
						enemy.sprite = sprites.MKWizardSide
					case dirDown:
						enemy.sprite = sprites.MKWizardBack
					}
				}
			case entityFireball:
				enemy.rotationZ += 10 * math.Pi / 180
			}
		}
	}
}

func (mk *MiteKnight) floorChange(x, z int) {
	audio.MKStairs.PlaySound(0, 0, 0, float32(x), 0, float32(z))

	// climbing staircases is cool
	mk.aiReward++

	anyAlive, potions := false, 0

	for _, e := range mk.floors[mk.floor].enemies {
		// ...unless you left enemies alive
		if (e.entityType == entityAnt || e.entityType == entityWizard) && !e.dead {
			if !anyAlive {
				mk.aiReward--

				anyAlive = true
			}

			mk.aiReward -= 0.125
		}
	}

	if mk.floor+1 == len(mk.floors) {
		for _, e := range mk.floors[mk.floor].items {
			// ...or you left health potions you needed behind
			if e.entityType == entityPotion && mk.floors[mk.floor].player.hp+potions < mk.floors[mk.floor].player.maxhp {
				mk.aiReward -= 0.5

				potions++
			}
		}
	}

	mk.canInput = false

	if mk.floor+1 >= len(mk.floors) {
		mk.winner = true

		mk.endGame()
	} else {
		mk.floors[mk.floor+1].player.hp = mk.floors[mk.floor].player.hp

		mk.state.FadeTo(0)

		totalWait := mk.state.ComputeWait(1)
		mk.floorWait = totalWait

		totalWait++
		totalWait += mk.state.ComputeWait(0.1)
		mk.fadeWait = totalWait

		totalWait += mk.state.ComputeWait(0.25)
		mk.inputWait = totalWait
	}
}

func (mk *MiteKnight) endGame() {
	mk.state.FinalizeRecording()

	mk.canInput = false

	type hudFunc struct {
		tick uint64
		fn   func(*sprites.Batch)
	}

	var hudFuncs []hudFunc

	mk.overrideHUD = func() {
		mk.state.Camera.SetDefaults()
		mk.state.Camera.Position.Translation(0, 90, -2)
		mk.state.Camera.PushTranslation(0, 70, 0)

		if mk.tb == nil {
			mk.tb = sprites.NewBatch(&mk.state.Camera)
		} else {
			mk.tb.Reset(&mk.state.Camera)
		}

		for _, hf := range hudFuncs {
			if mk.state.TickCount >= hf.tick {
				hf.fn(mk.tb)
			}
		}

		mk.tb.Render()

		mk.state.Camera.PopTransform()
	}

	mk.clockWait = 0

	audio.StopMusic()

	mk.state.FadeTo(0)

	if mk.pause != 0 {
		mk.exitWait = mk.state.ComputeWait(1) + 1

		return
	}

	mk.fadeWait = mk.state.ComputeWait(1)
	totalWait := mk.state.TickCount + mk.fadeWait

	mk.state.AISpamButton(input.BtnConfirm)

	hudFuncs = append(hudFuncs, hudFunc{
		tick: totalWait,
		fn:   func(_ *sprites.Batch) { clearHUD() },
	})

	totalWait++

	if mk.winner {
		floor := mk.floors[len(mk.floors)-1]

		audio.MiteKnightIntro.PlaySoundGlobal(1, 1.05, 0)

		hudFuncs = append(hudFuncs, hudFunc{
			tick: totalWait,
			fn: func(tb *sprites.Batch) {
				sprites.DrawTextCentered(tb, sprites.FontD3Streetism, "DUNGEON COMPLETE!", 0, 25.5, 0, 2, 2, sprites.Rainbow, true)
			},
		})

		totalWait += mk.state.ComputeWait(1)

		hudFuncs = append(hudFuncs, hudFunc{
			tick: totalWait,
			fn: func(tb *sprites.Batch) {
				sprites.DrawTextCentered(tb, sprites.FontD3Streetism, fmt.Sprintf("Time Left: %.0f:%02.0f", mk.timeMin, mk.timeSec), 0, 23.85, 0, 2, 2, sprites.White, true)
			},
		})

		totalWait += mk.state.ComputeWait(0.5)

		hearts := "[" + strings.Repeat("♡", floor.player.hp) + strings.Repeat("-", floor.player.maxhp-floor.player.hp) + "]"

		var heartsWidth float32

		for _, letter := range "Life Left: " {
			heartsWidth += sprites.TextAdvance(sprites.FontD3Streetism, letter, 2)
		}

		lifeLeftWidth := heartsWidth

		for _, letter := range hearts {
			heartsWidth += sprites.TextAdvance(sprites.FontD3Streetism, letter, 1.5)
		}

		lifeLeftX := -heartsWidth / 2
		heartsX := -heartsWidth/2 + lifeLeftWidth

		hudFuncs = append(hudFuncs, hudFunc{
			tick: totalWait,
			fn: func(tb *sprites.Batch) {
				sprites.DrawTextShadow(tb, sprites.FontD3Streetism, "Life Left: ", lifeLeftX, 22.33, 0, 2, 2, sprites.White)
				sprites.DrawTextShadow(tb, sprites.FontD3Streetism, hearts, heartsX, 22.33, 0, 1.5, 2, sprites.White)
			},
		})

		totalWait += mk.state.ComputeWait(1)

		timeFactor := (mk.timeMin*60 + mk.timeSec) / mk.rules.TimeLimit
		healthFactor := float64(floor.player.hp) / float64(floor.player.maxhp)

		mk.state.Score = math.Ceil(mk.state.Score * (1 + timeFactor/2) * (1 + healthFactor/2))

		hudFuncs = append(hudFuncs, hudFunc{
			tick: totalWait,
			fn: func(tb *sprites.Batch) {
				sprites.DrawTextCentered(tb, sprites.FontD3Streetism, fmt.Sprintf("Score: %.0f", mk.state.Score), 0, 20, 0, 2, 2, sprites.White, true)
			},
		})

		totalWait += mk.state.ComputeWait(0.5)

		hudFuncs = append(hudFuncs, hudFunc{
			tick: totalWait,
			fn: func(tb *sprites.Batch) {
				sprites.DrawTextCentered(tb, sprites.FontD3Streetism, "Press "+sprites.Button(input.BtnConfirm)+" or "+sprites.Button(input.BtnCancel)+" to exit.", 0, 17.85, -0.1, 2, 2, sprites.White, true)
			},
		})

		mk.allowExitAfter = totalWait
		mk.allowCancelExit = true

		totalWait += mk.state.ComputeWait(0.5)

		hudFuncs = append(hudFuncs, hudFunc{
			tick: totalWait,
			fn: func(tb *sprites.Batch) {
				sprites.DrawText(tb, sprites.FontD3Streetism, "Seed: "+mk.state.Seed, -10, 16, -0.1, 1, 1, sprites.Gray)
			},
		})
	} else {
		audio.MKGameOver.PlaySoundGlobal(1, 0.9, 0)

		hudFuncs = append(hudFuncs, hudFunc{
			tick: totalWait,
			fn: func(tb *sprites.Batch) {
				sprites.DrawTextCentered(tb, sprites.FontD3Streetism, "GAME OVER", 0, 21.75, 0, 2, 2, sprites.White, true)
			},
		})

		totalWait++
		totalWait += mk.state.ComputeWait(0.5)

		hudFuncs = append(hudFuncs, hudFunc{
			tick: totalWait,
			fn: func(tb *sprites.Batch) {
				sprites.DrawText(tb, sprites.FontD3Streetism, "Seed: "+mk.state.Seed, -10, 16, -0.1, 1, 1, sprites.Gray)
			},
		})

		totalWait += mk.state.ComputeWait(0.5)

		mk.allowExitAfter = totalWait
	}
}
