//go:build !headless
// +build !headless

package miteknight

import (
	"context"
	"fmt"
	"math"
	"time"

	"git.lubar.me/ben/spy-cards/arcade/game"
	"git.lubar.me/ben/spy-cards/audio"
	"git.lubar.me/ben/spy-cards/gfx"
	"git.lubar.me/ben/spy-cards/gfx/sprites"
	"git.lubar.me/ben/spy-cards/input"
	"git.lubar.me/ben/spy-cards/internal/router"
	"golang.org/x/mobile/gl"
)

func clearHUD() {
	gfx.GL.ClearColor(0, 0, 0, 1)
	gfx.GL.Clear(gl.COLOR_BUFFER_BIT)
}

// Render implements game.Interface.
func (mk *MiteKnight) Render() {
	mk.renderWorld()
	mk.renderHUD()
}

func (mk *MiteKnight) renderWorld() {
	mk.state.Camera.Perspective.Perspective(-60*math.Pi/180, 16.0/9.0, 0.1, 50)
	mk.state.Camera.Offset.Translation(0, 0.5, -0.5)
	mk.state.Camera.Rotation.RotationY(mk.camRotation)

	if mk.rotateX {
		mk.state.Camera.Perspective.Perspective(-90*math.Pi/180, 16.0/9.0, 5, 250)
		mk.state.Camera.Rotation.RotationX(75 * math.Pi / 180)
	}

	mk.state.Camera.Position.Translation(mk.camPos.x, mk.camPos.y, mk.camPos.z)
	mk.state.Camera.MarkDirty()

	sr, cr := math.Sincos(float64(mk.camRotation))

	audio.SetListenerPosition(mk.camPos.x-float32(sr)/2, mk.camPos.y, mk.camPos.z-float32(cr)/2, 0, mk.camRotation, 0)

	gfx.Lock.Lock()

	gfx.GL.ClearColor(0, 0, 0, 1)
	gfx.GL.Clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT)

	if mk.floor >= len(mk.floors) {
		gfx.Lock.Unlock()

		return
	}

	floor := mk.floors[mk.floor]

	gfx.GL.Disable(gl.BLEND)
	gfx.GL.Enable(gl.DEPTH_TEST)

	var combined gfx.Matrix

	mk.state.Camera.Combined(&combined)

	gfx.GL.Enable(gl.CULL_FACE)
	gfx.GL.UseProgram(geomProgram.Program)
	gfx.GL.Uniform1i(geomTex.Uniform, 0)
	gfx.GL.UniformMatrix4fv(geomPerspective.Uniform, mk.state.Camera.Perspective[:])
	gfx.GL.UniformMatrix4fv(geomCamera.Uniform, combined[:])
	gfx.GL.Uniform1f(geomTime.Uniform, float32(mk.state.TickCount)/game.FrameRate)

	if mk.rotateX {
		gfx.GL.Uniform1f(geomFogEnd.Uniform, 10000)
	} else {
		gfx.GL.Uniform1f(geomFogEnd.Uniform, 5)
	}

	if err := sprites.MKWall.Preload(); err != nil {
		gfx.Lock.Unlock()

		return
	}

	gfx.GL.ActiveTexture(gl.TEXTURE0)
	gfx.GL.BindTexture(gl.TEXTURE_2D, sprites.MKWall.Texture())

	for _, b := range floor.staticGeometry {
		b.Bind()
		gfx.GL.DrawElements(gl.TRIANGLES, b.Count, gl.UNSIGNED_SHORT, 0)
		b.Unbind()
	}

	gfx.GL.Disable(gl.CULL_FACE)

	gfx.Lock.Unlock()

	for i := 0; i < len(mk.particles); i++ {
		mk.drawParticle(mk.particles[i])

		if mk.particles[i].end.Before(time.Now()) {
			mk.particles = append(mk.particles[:i], mk.particles[i+1:]...)
			i--
		}
	}

	if mk.batch == nil {
		mk.batch = sprites.NewBatch(&mk.state.Camera)
	} else {
		mk.batch.Reset(&mk.state.Camera)
	}

	if !floor.gotKey || floor.hasKey {
		mk.drawEntity(mk.batch, floor.door)
	}

	for _, item := range floor.items {
		mk.drawEntity(mk.batch, item)
	}

	for _, enemy := range floor.enemies {
		if !enemy.dead || enemy.dying {
			mk.drawEntity(mk.batch, enemy)
		}
	}

	mk.batch.Render()

	gfx.Lock.Lock()
	gfx.GL.Disable(gl.DEPTH_TEST)
	gfx.Lock.Unlock()

	mk.batch.Reset(&mk.state.Camera)

	mk.drawEntity(mk.batch, floor.player)

	mk.batch.Render()

	gfx.Lock.Lock()
	gfx.GL.Enable(gl.BLEND)
	gfx.Lock.Unlock()

	if mk.compassEnabled {
		var m gfx.Matrix

		mk.state.Camera.PushTranslation(mk.compassPos.x, mk.compassPos.y, mk.compassPos.z)
		m.RotationXYZ(-math.Pi/2, 0, mk.compassRotation)
		mk.state.Camera.PushTransform(&m)

		mk.batch.Reset(&mk.state.Camera)

		mk.batch.Append(sprites.MKCompass, 0, 0, 0, 0.55, 0.55, sprites.Yellow)

		mk.batch.Render()

		mk.state.Camera.PopTransform()
		mk.state.Camera.PopTransform()
	}
}

func (mk *MiteKnight) drawEntity(batch *sprites.Batch, entity *entity) {
	x := float32(entity.x) + entity.ox()
	y := entity.height
	z := float32(entity.z) + entity.oz()

	bounce := entity.bounce
	if mk.state.TickCount < entity.tempBounce {
		bounce = 1.5
	}

	sx, sy := entityScale[entity.entityType], entityScale[entity.entityType]
	if entity.flipX {
		sx = -sx
	}

	bounceTime := float32(mk.state.TickCount)
	if mk.rules.PauseStopsAnimations && mk.pause != 0 {
		bounceTime = float32(mk.pause)
	}

	bounceTime = bounceTime / game.FrameRate * 7 * bounce

	sx += float32(math.Sin(float64(bounceTime))) * 0.1 * bounce * 0.75
	sy += float32(math.Cos(float64(bounceTime+1))) * 0.1 * bounce * 0.75

	blink := uint64(3)

	if entity.blinkDelay != 0 {
		blink = (mk.state.TickCount - entity.blinkStart) / entity.blinkDelay

		if blink > 2 {
			entity.blinkDelay = 0
		}
	}

	color := entityColor[entity.entityType]
	if blink == 0 || blink == 2 {
		color = sprites.White
	}

	rx, ry := float32(0), -mk.camRotation
	if entity.entityType == entityDoor {
		ry = 0
	}

	var flags sprites.RenderFlag

	if entity.billboard {
		bx, by := mk.billboard(x, y, z)
		rx += bx
		ry += by
	}

	if entity.fog {
		flags |= sprites.FlagMKFog
	}

	if entity.iframes != 0 && router.FlagMKAssist.IsSet() {
		if entity.blocking {
			color = sprites.White
		} else {
			flags |= sprites.FlagRainbow
		}
	}

	batch.AppendEx(entity.sprite, x, y, z, sx, sy, color, flags, rx, ry, entity.rotationZ)
}

func (mk *MiteKnight) drawParticle(p *particleEffect) {
	lerp := float32(time.Since(p.start)) / float32(p.end.Sub(p.start))
	if lerp < 0 || lerp > 1 {
		return
	}

	dampened := lerp * float32(math.Pow(float64(1-p.def.dampen), float64(lerp*p.def.lifetime)))

	if p.def.additive {
		gfx.Lock.Lock()
		gfx.GL.Enable(gl.BLEND)
		gfx.GL.BlendFunc(gl.ONE, gl.ONE)
		gfx.Lock.Unlock()
	}

	var flags sprites.RenderFlag

	if p.def.additive {
		flags |= sprites.FlagNoDiscard
	}

	if mk.batch == nil {
		mk.batch = sprites.NewBatch(&mk.state.Camera)
	} else {
		mk.batch.Reset(&mk.state.Camera)
	}

	x := p.initialPosition.x + p.initialOffset.x + p.velocity.x*dampened
	y := p.initialPosition.y + p.initialOffset.y + p.velocity.y*dampened - p.def.gravity*(lerp*p.def.lifetime)*(lerp*p.def.lifetime+0.25)
	z := p.initialPosition.z + p.initialOffset.z + p.velocity.z*dampened
	scale := p.size * p.def.sizeFunc(lerp)
	rx := float32(0)
	ry := -mk.camRotation
	rz := p.initialAngle + p.angular*lerp*p.def.lifetime

	bx, by := mk.billboard(x, y, z)

	mk.batch.AppendEx(p.sprite, x, y, z, scale, scale, p.color, flags, rx+bx, ry+by, rz)

	mk.batch.Render()

	if p.def.additive {
		gfx.Lock.Lock()
		gfx.GL.Disable(gl.BLEND)
		gfx.GL.BlendFunc(gl.ONE, gl.ONE_MINUS_SRC_ALPHA)
		gfx.Lock.Unlock()
	}
}

func (mk *MiteKnight) billboard(x, y, z float32) (rx, ry float32) {
	var combined gfx.Matrix
	var relative gfx.Vector

	mk.state.Camera.Combined(&combined)
	relative.Multiply(&combined, &gfx.Vector{x, y, z, 1})

	rx = -float32(math.Atan2(-float64(relative[1]), -float64(relative[2])))
	ry = float32(math.Atan2(-float64(relative[0]), float64(relative[2])))

	return
}

// Cleanup implements game.Interface.
func (mk *MiteKnight) Cleanup(ctx context.Context) error {
	for _, floor := range mk.floors {
		for _, b := range floor.staticGeometry {
			b.Delete()
		}
	}

	gfx.GL.DisableVertexAttribArray(posAttrib.Attrib)
	gfx.GL.DisableVertexAttribArray(texAttrib.Attrib)
	gfx.GL.DisableVertexAttribArray(colorAttrib.Attrib)

	return nil
}

// RenderAttract implements game.Interface.
func (mk *MiteKnight) RenderAttract(progress int, fadeIn, fadeOut bool) {
	gfx.GL.ClearColor(0, 0, 0, 1)
	gfx.GL.Clear(gl.COLOR_BUFFER_BIT)

	if mk.batch == nil {
		mk.batch = sprites.NewBatch(&mk.state.Camera)
	} else {
		mk.batch.Reset(&mk.state.Camera)
	}

	mk.batch.Append(sprites.AttractMK, 0, 70, 0, 1, 1, sprites.White)

	mk.batch.Render()

	if mk.tb == nil {
		mk.tb = sprites.NewBatch(&mk.state.Camera)
	} else {
		mk.tb.Reset(&mk.state.Camera)
	}

	sprites.DrawTextCentered(mk.tb, sprites.FontD3Streetism, "Mite Knight", 0, 74.8, 0, 2, 2, sprites.White, true)

	if !fadeIn && !fadeOut {
		if progress <= 100 {
			sprites.DrawTextCentered(mk.tb, sprites.FontD3Streetism, fmt.Sprintf("Preparing Dungeon... (%d%%)", progress), 0, 68, 0, 1.5, 1.5, sprites.White, true)
		} else if math.Sin(float64(mk.state.TickCount)*5/60) > 0 {
			sprites.DrawTextCentered(mk.tb, sprites.FontD3Streetism, "PRESS "+sprites.Button(input.BtnConfirm)+" TO START!", 0, 68, 0, 1.5, 1.5, sprites.White, true)
		}
	}

	mk.tb.Render()
}

func (mk *MiteKnight) renderHUD() {
	mk.state.Camera.SetDefaults()
	mk.state.Camera.Rotation.Identity()
	mk.state.Camera.Offset.Identity()

	if mk.overrideHUD != nil {
		mk.overrideHUD()
	} else {
		var m gfx.Matrix
		m.Translation(0, 70, 0)
		mk.state.Camera.PushTransform(&m)

		if mk.tb == nil {
			mk.tb = sprites.NewBatch(&mk.state.Camera)
		} else {
			mk.tb.Reset(&mk.state.Camera)
		}

		mk.state.DrawButtons(mk.tb, -10, -4.5, 10, 2)

		scoreColor := sprites.White
		scoreSuffix := ""

		if router.FlagMKAssist.IsSet() && mk.floor < len(mk.floors) {
			liveEnemies := 0
			enemyHP := 0

			for _, e := range mk.floors[mk.floor].enemies {
				if !e.dead && (e.entityType == entityAnt || e.entityType == entityWizard) {
					liveEnemies++
					enemyHP += e.hp
				}
			}

			if liveEnemies == 0 {
				scoreColor = sprites.Green
			} else {
				scoreSuffix = fmt.Sprintf(" / %0[2]*.0[1]f", mk.state.Score+mk.rules.ScorePerHit*float64(enemyHP)+mk.rules.ScorePerKill*float64(liveEnemies), int(mk.rules.ScoreDigits))
			}
		}

		sprites.DrawText(mk.tb, sprites.FontD3Streetism, fmt.Sprintf("Score: %0[2]*.0[1]f%[3]s", mk.state.Score, int(mk.rules.ScoreDigits), scoreSuffix), -10, -5.5, 10, 1.25, 1.25, scoreColor)
		sprites.DrawText(mk.tb, sprites.FontD3Streetism, fmt.Sprintf("Floor %d", mk.floor+1), 6.5, -5.5, 10, 1.25, 1.25, sprites.White)
		sprites.DrawText(mk.tb, sprites.FontD3Streetism, mk.timeText, -1, 4.35, 10, 1.5, 1.5, sprites.White)

		if mk.batch == nil {
			mk.batch = sprites.NewBatch(&mk.state.Camera)
		} else {
			mk.batch.Reset(&mk.state.Camera)
		}

		floor := mk.floors[mk.floor]

		for i := 0; i < floor.player.maxhp; i++ {
			filled := floor.player.hp > i

			sprite := sprites.MKHeartEmpty
			if filled {
				sprite = sprites.MKHeartFull
			}

			color := sprites.Gray
			if filled {
				color = sprites.Red
			}

			mk.batch.Append(sprite, -9+float32(i)*1.25, 4.8, 10, 3, 3, color)
		}

		if floor.hasKey {
			mk.batch.Append(sprites.MKKey, 9, 4.5, 10, 3, 3, sprites.Yellow)
		}

		mk.batch.Render()

		if mk.state.Playback != nil {
			sprites.DrawTextCentered(mk.tb, sprites.FontD3Streetism, "Recorded by "+mk.state.Playback.PlayerName, 4.7, 4.8, 10, 0.8, 0.8, sprites.White, false)
			sprites.DrawTextCentered(mk.tb, sprites.FontD3Streetism, mk.state.Playback.Start.Format("Jan 2, 2006"), 4.7, 4.1, 10, 0.8, 0.8, sprites.White, false)
		}

		mk.tb.Render()

		if mk.pause != 0 {
			m.Translation(-0.25, 0.2, 0.7)
			mk.state.Camera.PushTransform(&m)
			m.Scale(0.075, 0.075, 0.075)
			mk.state.Camera.PushTransform(&m)

			mk.tb.Reset(&mk.state.Camera)

			sprites.DrawTextShadow(mk.tb, sprites.FontD3Streetism, "PAUSED", 0, 0, 0, 1, 1, sprites.White)
			sprites.DrawTextShadow(mk.tb, sprites.FontD3Streetism, "PRESS "+sprites.Button(input.BtnConfirm)+" TO CONTINUE", 0, -0.7*2, 0, 1, 1, sprites.White)
			sprites.DrawTextShadow(mk.tb, sprites.FontD3Streetism, "PRESS "+sprites.Button(input.BtnCancel)+" TO EXIT", 0, -0.7*3.5, 0, 1, 1, sprites.White)

			mk.tb.Render()

			mk.state.Camera.PopTransform()
			mk.state.Camera.PopTransform()
		}

		mk.state.Camera.PopTransform()
	}
}

func (d *dungeonFloor) appendStaticGeometry(staticData, staticElements []byte) {
	d.staticGeometry = append(d.staticGeometry, gfx.NewStaticBuffer(fmt.Sprintf("mk floor %d geom %d", d.floor, len(d.staticGeometry)), staticData, staticElements, 2, func() {
		gfx.GL.EnableVertexAttribArray(posAttrib.Attrib)
		gfx.GL.EnableVertexAttribArray(texAttrib.Attrib)
		gfx.GL.EnableVertexAttribArray(colorAttrib.Attrib)
		gfx.GL.VertexAttribPointer(posAttrib.Attrib, 3, gl.UNSIGNED_BYTE, false, 8, 0)
		gfx.GL.VertexAttribPointer(texAttrib.Attrib, 2, gl.UNSIGNED_BYTE, false, 8, 3)
		gfx.GL.VertexAttribPointer(colorAttrib.Attrib, 3, gl.UNSIGNED_BYTE, true, 8, 5)
	}, func() {
		gfx.GL.DisableVertexAttribArray(posAttrib.Attrib)
		gfx.GL.DisableVertexAttribArray(texAttrib.Attrib)
		gfx.GL.DisableVertexAttribArray(colorAttrib.Attrib)
	}))
}
