uniform mat4 perspective;
uniform mat4 camera;
attribute vec3 in_pos;
attribute vec2 in_tex;
attribute vec3 in_color;
varying vec2 tex_coord;
varying vec4 color;
varying float fog_distance;

void main() {
	tex_coord = in_tex / 8.0;
	gl_Position = perspective * camera * vec4(in_pos - vec3(0.5, 0.0, 0.5), 1.0);
	fog_distance = length(gl_Position.xyz);
	color = vec4(in_color, 1.0);
}
