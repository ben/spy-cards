package miteknight

import (
	"math"

	"git.lubar.me/ben/spy-cards/arcade/game"
	"git.lubar.me/ben/spy-cards/gfx/sprites"
	"git.lubar.me/ben/spy-cards/input"
)

// InitAI implements game.Interface.
func (mk *MiteKnight) InitAI() *game.AIConfig {
	mapStateRange := make([]game.AIInputDesc, 0, 5*7*2+4*7)

	tileVisColors := map[float64]sprites.Color{
		float64(tileFree):   {R: 204, G: 153, B: 68, A: 255},
		float64(tileWall):   {R: 68, G: 68, B: 68, A: 255},
		float64(tileWall2):  {R: 238, G: 238, B: 238, A: 255},
		float64(tilePotion): {R: 0, G: 255, B: 0, A: 255},
		float64(tileStairs): {R: 255, G: 0, B: 0, A: 255},
		float64(tileDoor):   {R: 204, G: 204, B: 85, A: 255},
		float64(tileKey):    {R: 255, G: 255, B: 0, A: 255},
	}
	entityVisColors := map[float64]sprites.Color{
		-1:                      {R: 0, G: 0, B: 0, A: 136},
		float64(entityAnt):      {R: 255, G: 0, B: 0, A: 255},
		float64(entityWizard):   {R: 136, G: 0, B: 102, A: 255},
		float64(entityFireball): {R: 204, G: 136, B: 0, A: 255},
		float64(entityDoor):     {R: 204, G: 204, B: 85, A: 255},
		float64(entityKey):      {R: 255, G: 255, B: 0, A: 255},
		float64(entityPotion):   {R: 0, G: 255, B: 0, A: 255},
	}

	for i := 0; i < 5*7; i++ {
		if i == 10 {
			mapStateRange = append(mapStateRange,
				game.AIInputDesc{
					Min: 0,
					Max: float64(tileWall2),
					Enum: map[float64]sprites.Color{
						float64(tileFree):   {R: 68, G: 153, B: 204, A: 255},
						float64(tileWall):   {R: 68, G: 68, B: 68, A: 255},
						float64(tileWall2):  {R: 238, G: 238, B: 238, A: 255},
						float64(tilePotion): {R: 0, G: 255, B: 0, A: 255},
						float64(tileStairs): {R: 255, G: 0, B: 0, A: 255},
						float64(tileDoor):   {R: 204, G: 204, B: 85, A: 255},
						float64(tileKey):    {R: 255, G: 255, B: 0, A: 255},
					},
				},
				game.AIInputDesc{
					Min: -1,
					Max: float64(entityDoor),
					Enum: map[float64]sprites.Color{
						-1:                      {R: 68, G: 153, B: 204, A: 255},
						float64(entityAnt):      {R: 255, G: 0, B: 0, A: 255},
						float64(entityWizard):   {R: 136, G: 0, B: 102, A: 255},
						float64(entityFireball): {R: 204, G: 136, B: 0, A: 255},
						float64(entityDoor):     {R: 204, G: 204, B: 85, A: 255},
						float64(entityKey):      {R: 255, G: 255, B: 0, A: 255},
						float64(entityPotion):   {R: 0, G: 255, B: 0, A: 255},
					},
				},
			)
		} else {
			mapStateRange = append(mapStateRange,
				game.AIInputDesc{
					Min:  0,
					Max:  float64(tileWall2),
					Enum: tileVisColors,
				},
				game.AIInputDesc{
					Min:  -1,
					Max:  float64(entityDoor),
					Enum: entityVisColors,
				},
			)
		}
	}

	for i := 0; i < 4*7; i++ {
		mapStateRange = append(mapStateRange,
			game.AIInputDesc{
				Min:  -1,
				Max:  float64(entityDoor),
				Enum: entityVisColors,
			},
		)
	}

	return &game.AIConfig{
		NumInput: 1 + // percentage of enemies killed
			2 + // position in map
			2 + // offset of compass target
			2 + // facing direction
			1 + // HP percentage
			1 + // have key
			7 + // (reserved)
			5*7*2 + 4*7, // map state
		Press: []game.WeightedButton{
			{Button: input.BtnUp, Weight: 5},
			{Button: input.BtnLeft, Weight: 1},
			{Button: input.BtnRight, Weight: 1},
		},
		Toggle: []game.WeightedButton{
			{Button: input.BtnConfirm, Weight: 2},
		},
		PositionOverride: []int{
			1, 2, 0, -1, -1, -1, -1, -1, -1, -1, -1, -1, 107, 108, 109, 110, 111, 112, 113,
			3, 4, 7, -1, -1, -1, -1, -1, -1, -1, -1, -1, 100, 101, 102, 103, 104, 105, 106,
			5, 6, 8, -1, -1, -1, -1, -1, -1, -1, -1, -1, 93, 94, 95, 96, 97, 98, 99,
			-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 86, 87, 88, 89, 90, 91, 92,
			-1, -1, -1, -1, 72, 74, 76, 78, 80, 82, 84, -1, 73, 75, 77, 79, 81, 83, 85,
			-1, -1, -1, -1, 58, 60, 62, 64, 66, 68, 70, -1, 59, 61, 63, 65, 67, 69, 71,
			-1, -1, -1, -1, 44, 46, 48, 50, 52, 54, 56, -1, 45, 47, 49, 51, 53, 55, 57,
			-1, -1, -1, -1, 30, 32, 34, 36, 38, 40, 42, -1, 31, 33, 35, 37, 39, 41, 43,
			-1, -1, -1, -1, 16, 18, 20, 22, 24, 26, 28, -1, 17, 19, 21, 23, 25, 27, 29,
		},
		InputDesc: append(
			[]game.AIInputDesc{
				{Min: 0, Max: 1},
				{Min: 0, Max: 1},
				{Min: 0, Max: 1},
				{Min: -1, Max: 1},
				{Min: -1, Max: 1},
				{Min: -1, Max: 1},
				{Min: -1, Max: 1},
				{Min: 0, Max: 1},
				{Min: -1, Max: 1},
				{Min: 0, Max: 0},
				{Min: 0, Max: 0},
				{Min: 0, Max: 0},
				{Min: 0, Max: 0},
				{Min: 0, Max: 0},
				{Min: 0, Max: 0},
				{Min: 0, Max: 0},
			},
			mapStateRange...,
		),
	}
}

// AIInput implements game.Interface.
func (mk *MiteKnight) AIInput() (input []float64, reward float64) {
	if mk.scoreBefore != mk.state.Score {
		mk.lastScoreTick = mk.state.TickCount
	}

	floor := mk.floors[mk.floor]

	if len(mk.recentTiles) == 0 || mk.recentTiles[len(mk.recentTiles)-1][0] != floor.player.x || mk.recentTiles[len(mk.recentTiles)-1][1] != floor.player.z || mk.recentTiles[len(mk.recentTiles)-1][2] != int(floor.player.direction) {
		for _, recent := range mk.recentTiles {
			if recent[0] == floor.player.x && recent[1] == floor.player.z && recent[2] == int(floor.player.direction) {
				// running in circles isn't cool
				mk.aiReward -= 0.25
			}
		}

		mk.recentTiles = append(mk.recentTiles, [3]int{floor.player.x, floor.player.z, int(floor.player.direction)})

		if len(mk.recentTiles) > 10 {
			mk.recentTiles = mk.recentTiles[len(mk.recentTiles)-10:]
		}
	}

	if mk.canInput && mk.inputDelay <= 0 && floor.player.ox() == 0 && floor.player.oz() == 0 {
		if mk.stopDelay > 20 {
			// standing still isn't cool
			mk.aiReward -= 0.25
		}

		ddx, ddz := getFrontPos(0, 0, floor.player.direction)

		compassTarget := floor.compassTarget[0]
		if floor.gotKey {
			compassTarget = floor.compassTarget[1]
		}

		deadEnemies := 0

		for _, e := range floor.enemies {
			if e.dead {
				deadEnemies++
			}
		}

		var keyStatus float64

		if !floor.gotKey {
			keyStatus = -1
		} else if floor.hasKey {
			keyStatus = 1
		}

		input = []float64{
			float64(deadEnemies) / float64(len(floor.enemies)),
			float64(floor.player.x) / float64(floor.sizeX),
			float64(floor.player.z) / float64(floor.sizeY),
			(float64(compassTarget.x) - float64(floor.player.x)) / float64(floor.sizeX),
			(float64(compassTarget.z) - float64(floor.player.z)) / float64(floor.sizeY),
			float64(ddx), float64(ddz),
			float64(floor.player.hp) / float64(floor.player.maxhp),
			keyStatus,
			0, 0, 0, 0, 0, 0, 0,
		}

		for front := -1; front <= 7; front++ {
			for side := -3; side <= 3; side++ {
				dx := side*ddz + front*ddx
				dz := side*-ddx + front*ddz

				x := floor.player.x + dx
				z := floor.player.z + dz

				ent := floor.entityInPos(x, z)

				tile := tileNone
				if x >= 0 && x < floor.sizeX && z >= 0 && z < floor.sizeY {
					tile = floor.floorMap[x][z]
				}

				if front < 4 {
					if tile == tileNone {
						input = append(input, float64(tileWall))
					} else {
						input = append(input, float64(tile))
					}
				}

				switch {
				case ent != nil:
					input = append(input, float64(ent.entityType))

					if ent.entityType != entityPlayer {
						// being able to see enemies is cool
						mk.aiReward += 0.0625 / float64(front*front+side*side)
					}
				case tile == tileDoor:
					input = append(input, float64(entityDoor))
				case tile == tileKey:
					// being near the key is cool
					mk.aiReward += 0.75 / float64(front*front+side*side)

					input = append(input, float64(entityKey))
				case tile == tilePotion:
					if floor.player.hp < floor.player.maxhp {
						// being near a potion when you're hurt is cool
						mk.aiReward += 0.5 / float64(front*front+side*side)
					}

					input = append(input, float64(entityPotion))
				default:
					input = append(input, -1)
				}
			}
		}

		reward = mk.aiReward * 4
		mk.aiReward = 0.0625
	} else {
		reward = math.NaN()
	}

	mk.scoreBefore = mk.state.Score

	return input, reward
}
