//go:build headless
// +build headless

package miteknight

import "context"

func clearHUD() {
}

func (mk *MiteKnight) Cleanup(ctx context.Context) error {
	return nil
}

func (mk *MiteKnight) Render() {
}

func (mk *MiteKnight) RenderAttract(int, bool, bool) {
}

func (d *dungeonFloor) appendTileGeometry(staticData, staticElements []uint8, x, y, baseX, baseY int) ([]uint8, []uint8) {
	return staticData, staticElements
}

func (d *dungeonFloor) appendStaticGeometry(data, elements []uint8) {
}
