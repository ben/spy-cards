precision mediump float;

uniform sampler2D tex;
uniform float time;
uniform float fog_end;
varying vec2 tex_coord;
varying vec4 color;
varying float fog_distance;

void main() {
	vec4 texColor = texture2D(tex, tex_coord);
	gl_FragColor = texColor * color;

	gl_FragColor.rgb *= clamp((fog_end - fog_distance) / fog_end, 0.0, 1.0);

	if (texColor.a < 0.5) {
		discard;
	}
}
