//go:generate stringer -type Game

// Package arcade implements data structures used by the Termacade.
package arcade

import "errors"

// Game is a Termacade game.
type Game uint8

// Constants for Game.
const (
	MiteKnight    Game = 1
	FlowerJourney Game = 2
)

// ErrUnknownGame is returned when an unhandled game ID is used.
var ErrUnknownGame = errors.New("arcade: unknown game")
