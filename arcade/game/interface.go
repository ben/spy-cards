package game

import (
	"context"

	"git.lubar.me/ben/spy-cards/arcade"
)

// Interface is the set of methods implemented by each Termacade game.
type Interface interface {
	Type() arcade.Game
	Rules() arcade.GameRules
	HighScore() float64

	Init(context.Context) error
	Generate(context.Context, func(percent int)) error
	StartGameplay(context.Context) error
	Logic(context.Context) error
	Render()
	Cleanup(context.Context) error

	RenderAttract(progress int, fadeIn, fadeOut bool)
	StartPressed()
	StartMusic()

	InitAI() *AIConfig
	AIInput() (input []float64, reward float64)

	SaveState() interface{}
	LoadState(interface{})
}
