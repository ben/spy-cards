package game

import (
	"errors"
	"fmt"
	"log"
	"math"
	"strconv"

	"git.lubar.me/ben/spy-cards/arcade"
	"git.lubar.me/ben/spy-cards/gfx/sprites"
	"git.lubar.me/ben/spy-cards/input"
	"git.lubar.me/ben/spy-cards/internal"
	"git.lubar.me/ben/spy-cards/internal/router"
	"github.com/BenLubar/convnet/deepqlearn"
)

// WeightedButton is a button with a weight (1 is normal).
type WeightedButton struct {
	Button input.Button
	Weight float64
}

// AIInputDesc describes an input to the AI.
type AIInputDesc struct {
	Name string
	Min  float64
	Max  float64
	Enum map[float64]sprites.Color
}

// AIConfig is the configuration for the AI.
type AIConfig struct {
	NumInput         int
	Press, Toggle    []WeightedButton
	PositionOverride []int
	InputDesc        []AIInputDesc
}

// AIForceButtons forces the AI to hold a set of buttons.
func (s *State) AIForceButtons(buttons ...input.Button) {
	s.aiButtons = buttons
	s.aiButtonSpam = false
}

// AIClearButtons returns control of the input to the AI.
func (s *State) AIClearButtons() {
	s.aiButtons = nil
	s.aiButtonSpam = false
}

// AISpamButton forces the AI to press a button every other frame.
func (s *State) AISpamButton(btn input.Button) {
	s.aiButtons = []input.Button{btn}
	s.aiButtonSpam = true
}

func (s *State) aiInput() []input.Button {
	if s.aiButtons != nil {
		if s.aiButtonSpam && s.TickCount%2 == 1 {
			return nil
		}

		return s.aiButtons
	}

	return s.aiOutput
}

var errNilAIConfig = errors.New("game: nil AI configuration")

var cachedBrain *deepqlearn.Brain

func (s *State) initAI() error {
	if !router.FlagUseAI.IsSet() {
		return nil
	}

	cfg := s.Interface.InitAI()
	if cfg == nil {
		return fmt.Errorf("game: for %v: %w", s.Interface.Type(), errNilAIConfig)
	}

	if cfg.PositionOverride == nil {
		cfg.PositionOverride = make([]int, cfg.NumInput)

		for i := range cfg.PositionOverride {
			cfg.PositionOverride[i] = i
		}
	}

	s.aiVisWidth = 16 * 19
	s.aiVisHeight = (len(cfg.PositionOverride) + 18) / 19 * 8

	var (
		outputWeights []float64
		totalWeight   float64
	)

	s.aiOutputs = make([][]input.Button, 0, (1<<len(cfg.Toggle))*(1+len(cfg.Press)))

	for i := 0; i < 1<<len(cfg.Toggle); i++ {
		for j := -1; j < len(cfg.Press); j++ {
			var (
				weight float64
				output []input.Button
			)

			for k := 0; k < len(cfg.Toggle); k++ {
				if i&(1<<k) != 0 {
					output = append(output, cfg.Toggle[k].Button)
					weight += cfg.Toggle[k].Weight
				} else {
					weight++
				}
			}

			if j != -1 {
				output = append(output, cfg.Press[j].Button)
				weight += cfg.Press[j].Weight
			} else {
				weight++
			}

			s.aiOutputs = append(s.aiOutputs, output)
			outputWeights = append(outputWeights, weight)

			totalWeight += weight
		}
	}

	opts := deepqlearn.DefaultBrainOptions

	if s.Interface.Type() == arcade.MiteKnight {
		opts.TemporalWindow = 2
	} else {
		opts.TemporalWindow = 15
	}

	opts.LearningStepsBurnin = 120

	opts.RandomActionDistribution = make([]float64, len(outputWeights))

	for i, w := range outputWeights {
		opts.RandomActionDistribution[i] = w / totalWeight
	}

	opts.HiddenLayerSizes = []int{50, 50}

	if cachedBrain == nil {
		brain, err := deepqlearn.NewBrain(cfg.NumInput, len(s.aiOutputs), opts)
		if err != nil {
			return fmt.Errorf("game: failed to initialize AI: %w", err)
		}

		b, err := internal.LoadData("spy-cards-game-ai-memory-v0-" + strconv.Itoa(int(s.Interface.Type())))

		if b != nil && err == nil {
			err = brain.ValueNet.UnmarshalJSON(b)
		}

		if err != nil {
			log.Println("WARNING: failed to load cached neural network memory:", err)
		}

		cachedBrain = brain
		s.aiBrain = brain
	} else {
		s.aiBrain = cachedBrain
	}

	for len(cfg.InputDesc) < cfg.NumInput {
		cfg.InputDesc = append(cfg.InputDesc, AIInputDesc{})
	}

	s.aiConfig = cfg
	s.aiOutput = nil
	s.aiCurInput = nil
	s.aiPrevInput = nil

	s.initAIVis()

	return nil
}

func (s *State) updateAI(inputs []float64, reward float64) {
	if !router.FlagUseAI.IsSet() {
		return
	}

	s.PlaybackInput.Tick()

	if s.PlaybackInput.Consume(input.BtnConfirm) {
		s.aiBrain.Learning = !s.aiBrain.Learning
	}

	if s.PlaybackInput.Consume(input.BtnToggle) {
		router.FlagSuperSpeed.ForceSet(!router.FlagSuperSpeed.IsSet())
	}

	if !math.IsNaN(reward) {
		s.aiBrain.Backward(math.Min(math.Max(reward, -5), 5))
	}

	selectedChoice := s.aiBrain.Forward(inputs)
	s.aiOutput = s.aiOutputs[selectedChoice]

	s.aiPrevInput = s.aiCurInput
	s.aiCurInput = inputs

	s.updateAIVis()
}
