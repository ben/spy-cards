//go:build !headless
// +build !headless

package game

import (
	"math"
	"strconv"

	"git.lubar.me/ben/spy-cards/gfx"
	"git.lubar.me/ben/spy-cards/gfx/sprites"
)

func (s *State) initAIVis() {
	s.aiVisCam.SetDefaults()
	s.aiVisCam.Perspective.Identity()
	s.aiVisCam.Position.Translation(float32(s.aiVisWidth), -float32(s.aiVisHeight)-10, 0)
	s.aiVisCam.Rotation.Identity()
	s.aiVisCam.Offset.Translation(1.0, -1.0, 0.0)
	s.aiVisBatch = sprites.NewBatch(&s.aiVisCam)
}

func (s *State) updateAIVis() {
}

func (s *State) renderAIVis() {
	w, h := gfx.Size()
	s.aiVisCam.Rotation.Scale(-2.0/float32(w), -2.0/float32(h), -0.01)
	s.aiVisCam.MarkDirty()
	s.aiVisBatch.Reset(&s.aiVisCam)

	for i, r := range s.aiBrain.AverageRewardWindow.V {
		pos := (i + 1 - len(s.aiBrain.AverageRewardWindow.V) - s.aiBrain.AverageRewardWindow.Index) % s.aiBrain.AverageRewardWindow.Size

		tint := sprites.Red
		if r >= 0 {
			tint = sprites.Green
		}

		s.aiVisBatch.Append(sprites.Blank, float32(pos)/float32(s.aiBrain.AverageRewardWindow.Size)*float32(s.aiVisWidth)+float32(s.aiVisWidth), 180+float32(r)*5, 0, float32(s.aiVisWidth)/float32(s.aiBrain.AverageRewardWindow.Size), float32(r)*10, tint)
	}

	sprites.DrawTextBorder(s.aiVisBatch, sprites.FontBubblegumSans, "experience replay size: "+strconv.Itoa(len(s.aiBrain.Experience)), 0, 5*32*0.7, 0, 32, 32, sprites.White, sprites.Black, false)
	sprites.DrawTextBorder(s.aiVisBatch, sprites.FontBubblegumSans, "random actions: "+strconv.FormatFloat(s.aiBrain.Epsilon*100, 'f', 5, 64)+"%", 0, 4*32*0.7, 0, 32, 32, sprites.White, sprites.Black, false)
	sprites.DrawTextBorder(s.aiVisBatch, sprites.FontBubblegumSans, "average reward: "+strconv.FormatFloat(s.aiBrain.AverageRewardWindow.Average(), 'f', 5, 64), 0, 3*32*0.7, 0, 32, 32, sprites.White, sprites.Black, false)
	sprites.DrawTextBorder(s.aiVisBatch, sprites.FontBubblegumSans, "age: "+strconv.Itoa(s.aiBrain.Age), 0, 2*32*0.7, 0, 32, 32, sprites.White, sprites.Black, false)

	switch {
	case s.aiButtons != nil:
		sprites.DrawTextBorder(s.aiVisBatch, sprites.FontBubblegumSans, "using hard-coded inputs", 0, 32*0.7, 0, 32, 32, sprites.White, sprites.Black, false)
	case len(s.aiBrain.Experience) < 1000:
		sprites.DrawTextBorder(s.aiVisBatch, sprites.FontBubblegumSans, "learning: not yet", 0, 32*0.7, 0, 32, 32, sprites.White, sprites.Black, false)
	default:
		sprites.DrawTextBorder(s.aiVisBatch, sprites.FontBubblegumSans, "learning: "+strconv.FormatBool(s.aiBrain.Learning), 0, 32*0.7, 0, 32, 32, sprites.White, sprites.Black, false)
	}

	for i, slot := range s.aiConfig.PositionOverride {
		if slot < 0 || s.aiCurInput == nil {
			continue
		}

		desc := &s.aiConfig.InputDesc[slot]

		desc.Min = math.Min(desc.Min, s.aiCurInput[slot])
		desc.Max = math.Max(desc.Max, s.aiCurInput[slot])
		inputRange := math.Max(desc.Max-desc.Min, 0.1)
		normalized := (s.aiCurInput[slot]-desc.Min)/inputRange*2 - 1
		changedBy := 0.0

		if s.aiPrevInput != nil {
			changedBy = s.aiCurInput[slot] - s.aiPrevInput[slot]
		}

		fill := sprites.Color{
			R: uint8(math.Max(-normalized*255, 0)),
			G: uint8(math.Max(normalized*255, 0)),
			B: 64,
			A: 255,
		}
		if c, ok := desc.Enum[s.aiCurInput[slot]]; ok {
			fill = c
		}

		stroke := sprites.Color{
			R: uint8(math.Min(math.Max(-changedBy*255, 0), 255)),
			G: uint8(math.Min(math.Max(changedBy*255, 0), 255)),
			B: uint8(math.Min(math.Abs(changedBy*255), 255)),
			A: 255,
		}

		s.aiVisBatch.Append(sprites.Blank, float32(i%19*16+8), -float32(i/19*8+4), 0, 16, 8, stroke)
		s.aiVisBatch.Append(sprites.Blank, float32(i%19*16+8), -float32(i/19*8+4), 0, 14, 6, fill)
	}

	s.aiVisBatch.Render()
}
