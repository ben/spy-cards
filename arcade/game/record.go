package game

import (
	"errors"
	"fmt"

	"git.lubar.me/ben/spy-cards/arcade"
	"git.lubar.me/ben/spy-cards/input"
	"git.lubar.me/ben/spy-cards/internal"
)

func (s *State) initRecording() {
	s.recording = &arcade.Recording{
		Version: internal.Version,
		Game:    s.Interface.Type(),
		Rules:   s.Interface.Rules(),
		Seed:    s.Seed,

		Inputs: []arcade.RecordedInput{
			{},
		},
	}

	s.lastInputTick = s.TickCount
}

func (s *State) recordTick() {
	var curInput uint64

	for btn := input.BtnUp; btn <= input.BtnHelp; btn++ {
		if s.Input.Held(btn) {
			curInput |= 1 << btn
		}
	}

	if s.lastInput != curInput {
		s.recording.Inputs[len(s.recording.Inputs)-1].AdditionalTicks = s.TickCount - s.lastInputTick - 1

		s.recording.Inputs = append(s.recording.Inputs, arcade.RecordedInput{
			Buttons: input.UnpackButtons(curInput),
		})

		s.lastInput = curInput
		s.lastInputTick = s.TickCount
	}
}

var errFinalizeAlreadyCalled = errors.New("game: FinalizeRecording has already been called")

// FinalizeRecording does bookkeeping to finish the game recording.
func (s *State) FinalizeRecording() {
	if s.recording == nil {
		return
	}

	if s.recording.FinalRandCount != 0 {
		panic(fmt.Errorf("game: internal error: %w", errFinalizeAlreadyCalled))
	}

	// don't subtract 1 as this happens *on* the final tick rather than after
	s.recording.Inputs[len(s.recording.Inputs)-1].AdditionalTicks = s.TickCount - s.lastInputTick
	s.recording.FinalRandCount = s.RNG.Count
}
