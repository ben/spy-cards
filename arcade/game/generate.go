package game

import (
	"context"
	"fmt"
)

func (s *State) generate(ctx context.Context, progress chan int, done chan<- error) {
	defer func() {
		if r := recover(); r != nil {
			select {
			case done <- fmt.Errorf("game: generation function panic: %w", r):
			default:
			}
		}
	}()

	done <- s.Interface.Generate(ctx, func(percent int) {
		if percent < 0 {
			percent = 0
		}

		if percent > 100 {
			percent = 100
		}

		for {
			select {
			case <-progress:
				continue
			case progress <- percent:
			}

			break
		}
	})

	for {
		select {
		case <-progress:
			continue
		case progress <- 101:
		}

		break
	}
}
