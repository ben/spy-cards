//go:build headless
// +build headless

package game

import (
	"context"
	"errors"
)

const isHeadless = true

func (s *State) highScore(ctx context.Context) error {
	return errors.New("game: highScore should not have been called on server")
}

func (s *State) initAIVis()   {}
func (s *State) updateAIVis() {}
func (s *State) renderAIVis() {}
