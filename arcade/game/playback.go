package game

import (
	"time"

	"git.lubar.me/ben/spy-cards/audio"
	"git.lubar.me/ben/spy-cards/gfx"
	"git.lubar.me/ben/spy-cards/input"
)

func (s *State) playbackInput() []input.Button {
	tick := s.TickCount

	for _, frame := range s.Playback.Inputs {
		if tick <= frame.AdditionalTicks {
			return frame.Buttons
		}

		tick -= 1 + frame.AdditionalTicks
	}

	if isHeadless {
		s.Exit = true
	}

	return nil
}

func (s *State) playbackTick() {
	var maxInputTick uint64
	for _, i := range s.Playback.Inputs {
		maxInputTick += 1 + i.AdditionalTicks
	}

	if s.TickCount%60 == 1 && len(s.savedTicks) <= int(s.TickCount/60) {
		s.savedTicks = append(s.savedTicks, s.Save())
	}

	if s.goalTick != 0 {
		audio.IgnoreAudio = 1
		s.outstandingFrames -= 2
		s.nextTick = time.Time{}

		if s.outstandingFrames%30 == 0 {
			s.CRT.UseTime = true
			s.CRT.NoiseX = -float32(s.outstandingFrames) / 10000
			s.CRT.Draw(s.Interface.Render)
			s.CRT.UseTime = false

			gfx.NextFrame()
		}

		goalState := int(s.goalTick-1) / 60
		curState := int(s.TickCount-1) / 60

		if s.goalTick < s.TickCount {
			s.Load(s.savedTicks[goalState])
		} else if goalState > curState && len(s.savedTicks)-1 > curState {
			if max := len(s.savedTicks) - 1; max < goalState {
				goalState = max
			}

			s.Load(s.savedTicks[goalState])
		}

		if s.goalTick == s.TickCount {
			s.goalTick = 0
			s.nextTick = time.Now()
			audio.IgnoreAudio = 0
			s.CRT.NoiseX = 0
		}
	}

	s.PlaybackInput.Tick()

	if s.PlaybackInput.Consume(input.BtnLeft) {
		if s.goalTick == 0 {
			s.goalTick = s.TickCount
		}

		if s.goalTick > maxInputTick {
			s.goalTick = maxInputTick
		}

		if s.goalTick > 10*FrameRate {
			s.goalTick -= 10 * FrameRate
		} else {
			s.goalTick = 1
		}

		if s.TickCount >= maxInputTick {
			s.Interface.StartMusic()
		}
	}

	if s.PlaybackInput.Consume(input.BtnRight) {
		if s.goalTick == 0 {
			s.goalTick = s.TickCount
		}

		s.goalTick += 5 * FrameRate

		if s.goalTick > maxInputTick {
			s.goalTick = maxInputTick
		}
	}

	if s.PlaybackInput.Consume(input.BtnCancel) || (s.TickCount > maxInputTick && s.PlaybackInput.Consume(input.BtnConfirm)) {
		s.Exit = true
	}
}
