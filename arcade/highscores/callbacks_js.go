//go:build js && wasm && !headless
// +build js,wasm,!headless

package highscores

import (
	"syscall/js"

	"git.lubar.me/ben/spy-cards/internal"
	"git.lubar.me/ben/spy-cards/internal/router"
)

func (hs *HighScores) setURL() {
	router.Replace(&router.PageInfo{
		Page:    router.PageArcadeHighScores,
		Game:    hs.Game,
		HSMode:  hs.Mode,
		PageNum: hs.Page,
	})
}

func (v *RecordingViewer) pushURL() {
	v.wasPushed = true

	info := &router.PageInfo{
		Page: router.PageArcadeRecording,
		Code: v.Code,
	}

	b, err := info.MarshalText()
	if err != nil {
		panic(err)
	}

	internal.History.Call("pushState", js.Null(), "", string(b))
}

func (v *RecordingViewer) popURL() {
	if v.wasPushed {
		v.wasPushed = false

		internal.History.Call("back")
	}
}
