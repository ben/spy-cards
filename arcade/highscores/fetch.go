package highscores

import (
	"context"
	"fmt"
	"net/url"
	"strconv"
	"time"

	"git.lubar.me/ben/spy-cards/arcade"
	"git.lubar.me/ben/spy-cards/internal"
)

func (hs *HighScores) prevPage(ctx context.Context) {
	hs.Page--
	hs.next = hs.cur
	hs.cur = hs.prev
	hs.prev = nil
	hs.loaded = time.Now()
	hs.loading = hs.loaded

	hs.fetchPages(ctx)
}

func (hs *HighScores) nextPage(ctx context.Context) {
	hs.Page++
	hs.prev = hs.cur
	hs.cur = hs.next
	hs.next = nil
	hs.loaded = time.Now()
	hs.loading = hs.loaded

	hs.fetchPages(ctx)
}

func (hs *HighScores) resetPages(ctx context.Context) {
	hs.Index = 0
	hs.Page = 0
	hs.prev = nil
	hs.cur = nil
	hs.next = nil

	hs.fetchPages(ctx)
}

func (hs *HighScores) fetchPages(ctx context.Context) {
	origGame := hs.Game
	origMode := hs.Mode
	origPage := hs.Page

	hs.setURL()

	if overlap(hs.next, hs.cur) || overlap(hs.prev, hs.cur) {
		// scores moved between pages; reset just to be sure we're consistent
		hs.next = nil
		hs.prev = nil
		hs.cur = nil
	}

	fetchCur := hs.cur == nil
	fetchNext := hs.next == nil
	fetchPrev := hs.prev == nil && hs.Page > 0

	if fetchCur {
		hs.loading = time.Now()
		hs.loaded = time.Time{}
	}

	go func() {
		next := make(chan struct{}, 1)

		if fetchCur {
			entries, err := FetchScores(ctx, origGame, origMode, origPage)
			if len(entries) == 0 {
				fetchNext = false
			}

			hs.fetched <- func() error {
				if hs.Game != origGame || hs.Mode != origMode || hs.Page != origPage || err != nil {
					fetchNext = false
					fetchPrev = false
					next <- struct{}{}

					return err
				}

				hs.loaded = time.Now()
				hs.cur = entries
				next <- struct{}{}

				return nil
			}

			<-next
		}

		if fetchNext {
			entries, err := FetchScores(ctx, origGame, origMode, origPage+1)

			hs.fetched <- func() error {
				if hs.Game != origGame || hs.Mode != origMode || hs.Page != origPage || err != nil {
					fetchPrev = false
					next <- struct{}{}

					return err
				}

				hs.next = entries
				next <- struct{}{}

				return nil
			}

			<-next
		}

		if fetchPrev {
			entries, err := FetchScores(ctx, origGame, origMode, origPage-1)
			hs.fetched <- func() error {
				if hs.Game != origGame || hs.Mode != origMode || hs.Page != origPage || err != nil {
					return err
				}

				hs.prev = entries

				return nil
			}
		}
	}()
}

// FetchScores downloads a page of high scores data from the server.
func FetchScores(ctx context.Context, game arcade.Game, mode arcade.HighScoresMode, page int) ([]Entry, error) {
	baseURL := internal.GetConfig(ctx).ArcadeAPIBaseURL + "scores?"

	var entries []Entry

	err := internal.FetchJSON(ctx, baseURL+url.Values{
		"g": {strconv.Itoa(int(game))},
		"m": {string(byte(mode))},
		"p": {strconv.Itoa(page)},
	}.Encode(), &entries)
	if err != nil {
		return nil, fmt.Errorf("highscores: fetching scores: %w", err)
	}

	return entries, nil
}

// FetchRecording downloads a high scores recording.
func FetchRecording(ctx context.Context, code string) (*arcade.Recording, error) {
	b, err := internal.FetchBytes(ctx, internal.GetConfig(ctx).ArcadeAPIBaseURL+"get/"+code)
	if err != nil {
		return nil, fmt.Errorf("highscores: fetching recording: %w", err)
	}

	var rec arcade.Recording

	err = rec.UnmarshalBinary(b)

	if err != nil {
		return nil, fmt.Errorf("highscores: decoding fetched recording: %w", err)
	}

	return &rec, nil
}

func overlap(a, b []Entry) bool {
	for _, ae := range a {
		for _, be := range b {
			if ae.Code == be.Code {
				return true
			}
		}
	}

	return false
}
