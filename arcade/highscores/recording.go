package highscores

import (
	"context"
	"errors"
	"fmt"

	"git.lubar.me/ben/spy-cards/arcade"
	"git.lubar.me/ben/spy-cards/arcade/flowerjourney"
	"git.lubar.me/ben/spy-cards/arcade/game"
	"git.lubar.me/ben/spy-cards/arcade/miteknight"
)

var errUnhandledGame = errors.New("highscores: recording viewer unhandled game")

// RecordingViewer displays a game recording.
type RecordingViewer struct {
	Code      string
	wasPushed bool
}

// Run plays the recording.
func (v *RecordingViewer) Run(ctx context.Context) error {
	rec, err := FetchRecording(ctx, v.Code)
	if err != nil {
		return err
	}

	var create game.CreateFunc

	switch rec.Game {
	case arcade.MiteKnight:
		create = miteknight.New(*rec.Rules.(*arcade.MiteKnightRules))
	case arcade.FlowerJourney:
		create = flowerjourney.New(*rec.Rules.(*arcade.FlowerJourneyRules))
	default:
		return fmt.Errorf("highscores: for %v: %w", rec.Game, errUnhandledGame)
	}

	_, err = game.PlayRecording(ctx, create, rec)

	if err != nil {
		return fmt.Errorf("highscores: playing recording: %w", err)
	}

	return nil
}
