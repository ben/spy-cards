//go:build !js || !wasm || headless
// +build !js !wasm headless

package highscores

func (hs *HighScores) setURL()      {}
func (v *RecordingViewer) pushURL() {}
func (v *RecordingViewer) popURL()  {}
