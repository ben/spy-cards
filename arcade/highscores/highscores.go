// Package highscores implements the Termacade high scores viewer.
package highscores

import (
	"context"
	"fmt"
	"time"

	"git.lubar.me/ben/spy-cards/arcade"
	"git.lubar.me/ben/spy-cards/audio"
	"git.lubar.me/ben/spy-cards/gfx"
	"git.lubar.me/ben/spy-cards/gfx/crt"
	"git.lubar.me/ben/spy-cards/gfx/sprites"
	"git.lubar.me/ben/spy-cards/input"
	"git.lubar.me/ben/spy-cards/internal/router"
)

var nextGame = map[arcade.Game]arcade.Game{
	arcade.FlowerJourney: arcade.MiteKnight,
	arcade.MiteKnight:    arcade.FlowerJourney,
}

var nextMode = map[arcade.HighScoresMode]arcade.HighScoresMode{
	arcade.Recent:    arcade.Weekly,
	arcade.Weekly:    arcade.Quarterly,
	arcade.Quarterly: arcade.AllTime,
	arcade.AllTime:   arcade.Recent,
}

// Entry is a single item in the high scores list.
type Entry struct {
	Code   string `json:"c"`
	Score  int64  `json:"s"`
	Player string `json:"p"`
}

// HighScores is the high scores viewer.
type HighScores struct {
	Camera gfx.Camera
	CRT    crt.CRT

	first bool
	Game  arcade.Game
	Mode  arcade.HighScoresMode
	Page  int
	Index int

	prev []Entry
	cur  []Entry
	next []Entry

	loading time.Time
	loaded  time.Time
	fetched chan<- func() error

	batch *sprites.Batch
	tb    *sprites.Batch
}

// Run displays the high scores viewer.
func (hs *HighScores) Run(ctx context.Context) (*router.PageInfo, error) {
	hs.Camera.SetDefaults()
	hs.Camera.Position.Identity()
	hs.CRT.SetDefaults()

	audio.TermiteLoop.PlayMusic(0, false)

	fetched := make(chan func() error, 1)
	hs.fetched = fetched

	hs.first = true

	ictx := input.GetContext(ctx)

	if hs.Game == 0 {
		hs.Game = arcade.FlowerJourney
	}

	if hs.Mode == 0 {
		hs.Mode = arcade.AllTime
	}

	hs.fetchPages(ctx)

	for {
		ictx.Tick()

		select {
		case f := <-fetched:
			if err := f(); err != nil {
				return nil, err
			}
		default:
		}

		switch {
		case ictx.Consume(input.BtnConfirm):
			if hs.Index < len(hs.cur) {
				audio.Confirm.PlaySoundGlobal(0, 0, 0)

				var viewer RecordingViewer

				viewer.Code = hs.cur[hs.Index].Code

				viewer.pushURL()

				if err := viewer.Run(ctx); err != nil {
					return nil, err
				}

				audio.TermiteLoop.PlayMusic(0, false)

				viewer.popURL()
			} else {
				audio.Buzzer.PlaySoundGlobal(0, 0, 0)
			}
		case ictx.Consume(input.BtnCancel):
			return &router.PageInfo{
				Page: router.PageArcadeMain,
			}, nil
		case ictx.Consume(input.BtnToggle):
			hs.first = true
			hs.Game = nextGame[hs.Game]
			hs.resetPages(ctx)
			audio.Confirm.PlaySoundGlobal(0, 0, 0)
		case ictx.Consume(input.BtnSwitch):
			hs.first = true
			hs.Mode = nextMode[hs.Mode]
			hs.resetPages(ctx)
			audio.Confirm.PlaySoundGlobal(0, 0, 0)
		case ictx.ConsumeAllowRepeat(input.BtnLeft, 60, 60):
			hs.first = false
			if hs.Page != 0 {
				hs.prevPage(ctx)
				audio.PageFlip.PlaySoundGlobal(0, 0, 0)
			} else if hs.Index != 0 {
				hs.Index = 0
				audio.Confirm1.PlaySoundGlobal(0, 0, 0)
			}
		case ictx.ConsumeAllowRepeat(input.BtnRight, 60, 60):
			hs.first = false

			switch {
			case len(hs.next) != 0:
				hs.nextPage(ctx)

				if last := len(hs.cur) - 1; hs.Index > last {
					hs.Index = last
				}

				audio.PageFlip.PlaySoundGlobal(0, 0, 0)
			case hs.cur != nil && hs.Index != len(hs.cur)-1:
				hs.Index = len(hs.cur) - 1

				audio.Confirm1.PlaySoundGlobal(0, 0, 0)
			case hs.next != nil:
				audio.Buzzer.PlaySoundGlobal(0, 0, 0)
			}
		case ictx.ConsumeAllowRepeat(input.BtnUp, 12, 12):
			if hs.cur != nil {
				hs.first = false
				if hs.Index == 0 {
					if hs.Page != 0 {
						hs.prevPage(ctx)
						hs.Index = 4

						audio.PageFlip.PlaySoundGlobal(0, 0, 0)
					} else {
						audio.Buzzer.PlaySoundGlobal(0, 0, 0)
					}
				} else {
					hs.Index--
					audio.Confirm1.PlaySoundGlobal(0, 0, 0)
				}
			}
		case ictx.ConsumeAllowRepeat(input.BtnDown, 12, 12):
			if hs.cur != nil {
				hs.first = false
				if hs.Index == 4 {
					if len(hs.next) != 0 {
						hs.nextPage(ctx)
						hs.Index = 0

						audio.PageFlip.PlaySoundGlobal(0, 0, 0)
					} else {
						audio.Buzzer.PlaySoundGlobal(0, 0, 0)
					}
				} else {
					if hs.Index+1 >= len(hs.cur) {
						audio.Buzzer.PlaySoundGlobal(0, 0, 0)
					} else {
						hs.Index++
						audio.Confirm1.PlaySoundGlobal(0, 0, 0)
					}
				}
			}
		}

		hs.CRT.Draw(hs.render)

		gfx.NextFrame()

		if err := ctx.Err(); err != nil {
			return nil, fmt.Errorf("highscores: timed out: %w", err)
		}
	}
}

func whiteOrGray(white bool) sprites.Color {
	if white {
		return sprites.White
	}

	return sprites.Gray
}
