// Package mainmenu handles the Termacade main menu.
package mainmenu

import (
	"context"
	"fmt"
	"log"
	"runtime"
	"sync"
	"time"

	"git.lubar.me/ben/spy-cards/audio"
	"git.lubar.me/ben/spy-cards/gfx"
	"git.lubar.me/ben/spy-cards/gfx/crt"
	"git.lubar.me/ben/spy-cards/gfx/sprites"
	"git.lubar.me/ben/spy-cards/input"
	"git.lubar.me/ben/spy-cards/internal"
	"git.lubar.me/ben/spy-cards/internal/router"
)

func preload(funcs ...func() error) (<-chan float32, <-chan error) {
	progress := make(chan float32, 1)
	result := make(chan error, 1)

	var (
		lock   sync.Mutex
		loaded float32
		total  = float32(len(funcs))
	)

	run := func(async bool, f func()) {
		// invert async on android
		if runtime.GOOS == "android" {
			async = !async
		}

		if async {
			go f()
		} else {
			f()
		}
	}

	run(false, func() {
		for i := range funcs {
			fn := funcs[i]

			run(true, func() {
				err := fn()
				if err != nil {
					// don't actually fail to load the Termacade, just go on with the missing asset
					log.Println("ERROR: Preloading Termacade asset failed:", err)
				}

				lock.Lock()

				loaded++
				select {
				case <-progress:
				default:
				}
				progress <- loaded / total

				if loaded == total {
					close(result)
				}

				lock.Unlock()
			})
		}
	})

	return progress, result
}

// MainMenu is the Termacade main menu.
type MainMenu struct {
	Camera     gfx.Camera
	CRT        crt.CRT
	neonLoaded bool

	batch *sprites.Batch
	tb    *sprites.Batch

	Selection           int
	LastSelectionChange time.Time
}

// Run runs the Termacade main menu.
func (mm *MainMenu) Run(ctx context.Context) (*router.PageInfo, error) {
	internal.SetTitle("Termacade")

	for i := 0; i < len(Options); i++ {
		if Options[i].Hidden {
			Options = append(Options[:i], Options[i+1:]...)
			i--
		}
	}

	ictx := input.GetContext(ctx)

	mm.CRT.SetDefaults()
	mm.Camera.SetDefaults()
	mm.Camera.PushTranslation(0, 70, 3)

	mm.Selection = internal.LoadSettings().LastTermacadeOption
	if mm.Selection >= len(Options) {
		mm.Selection = len(Options) - 1
	}

	neonLoaded := make(chan error, 1)

	go func() {
		neonLoaded <- sprites.NeonFJBee.Preload()
	}()

	var (
		preloadProgress, preloadDone = preload(
			// Sprites
			sprites.Blank.Preload,
			sprites.GamepadButton[0][0].Preload,
			sprites.FJBack.Preload,
			sprites.NeonFJBee.Preload,
			sprites.AttractFJ.Preload,
			sprites.AttractMK.Preload,
			sprites.ParticleGrassPlaceholder.Preload,
			sprites.ParticleSmoke.Preload,
			sprites.ParticleStar.Preload,

			// Songs
			audio.TermiteLoop.Preload,
			audio.MiteKnight.Preload,
			audio.FlyingBee.Preload,

			// Main Menu
			audio.Buzzer.Preload,
			audio.Confirm.Preload,
			audio.Confirm1.Preload,

			// Mite Knight
			audio.MiteKnightIntro.Preload,
			audio.PeacockSpiderNPCSummonSuccess.Preload,
			audio.Shot2.Preload,
			audio.MKDeath.Preload,
			audio.MKGameOver.Preload,
			audio.MKHit.Preload,
			audio.MKHit2.Preload,
			audio.MKKey.Preload,
			audio.MKOpen.Preload,
			audio.MKPotion.Preload,
			audio.MKStairs.Preload,
			audio.MKWalk.Preload,

			// Flower Journey
			audio.FBCountdown.Preload,
			audio.FBDeath.Preload,
			audio.FBFlower.Preload,
			audio.FBGameOver.Preload,
			audio.FBPoint.Preload,
			audio.FBStart.Preload,
		)
		progress     float32
		preloadReady bool
	)

	audio.TermiteLoop.PlayMusic(0, false)

	for !preloadReady || !mm.neonLoaded {
		select {
		case err := <-neonLoaded:
			if err != nil {
				return nil, err
			}

			mm.neonLoaded = true
		default:
		}

		select {
		case progress = <-preloadProgress:
		default:
		}

		if !preloadReady {
			select {
			case err := <-preloadDone:
				if err != nil {
					return nil, err
				}

				preloadReady = true
			default:
			}
		}

		mm.CRT.UseTime = true
		mm.CRT.RGBNoise = 0
		mm.CRT.NoiseX = 1 - progress

		mm.CRT.Draw(mm.render)
		mm.CRT.UseTime = false

		gfx.NextFrame()
	}

	mm.CRT.RGBNoise = 0
	mm.CRT.NoiseX = 0

	mm.LastSelectionChange = time.Now()

	for {
		ictx.Tick()

		switch {
		case ictx.Consume(input.BtnConfirm):
			if fn := Options[mm.Selection].Func; fn != nil {
				audio.Confirm.PlaySoundGlobal(0, 0, 0)

				if redirect, err := fn(ctx); err != nil || redirect != nil {
					return redirect, err
				}

				internal.SetTitle("Termacade")
				audio.TermiteLoop.PlayMusic(0, false)
			} else {
				audio.Buzzer.PlaySoundGlobal(0, 0, 0)
			}
		case ictx.ConsumeAllowRepeat(input.BtnUp, 30, 30):
			mm.moveSelection(-1)
		case ictx.ConsumeAllowRepeat(input.BtnDown, 30, 30):
			mm.moveSelection(1)
		}

		mm.CRT.Draw(mm.render)

		gfx.NextFrame()

		if err := ctx.Err(); err != nil {
			return nil, fmt.Errorf("mainmenu: context was cancelled: %w", err)
		}
	}
}

func (mm *MainMenu) moveSelection(direction int) {
	mm.Selection += direction

	audio.Confirm1.PlaySoundGlobal(0, 0, 0)

	if mm.Selection < 0 {
		mm.Selection = len(Options) - 1
	}

	if mm.Selection >= len(Options) {
		mm.Selection = 0
	}

	settings := internal.LoadSettings()
	settings.LastTermacadeOption = mm.Selection
	internal.SaveSettings(settings)

	mm.LastSelectionChange = time.Now()
}
