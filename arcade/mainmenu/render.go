//go:build !headless
// +build !headless

package mainmenu

import (
	"fmt"
	"math"
	"time"

	"git.lubar.me/ben/spy-cards/gfx"
	"git.lubar.me/ben/spy-cards/gfx/sprites"
	"git.lubar.me/ben/spy-cards/input"
	"git.lubar.me/ben/spy-cards/internal"
	"golang.org/x/mobile/gl"
)

var (
	versionText   = fmt.Sprintf("Version %d.%d.%d", internal.Version[0], internal.Version[1], internal.Version[2])
	defaultAction = "Press " + sprites.Button(input.BtnConfirm) + " to play!"
	selectText    = "Select with " + sprites.Button(input.BtnUp) + " and " + sprites.Button(input.BtnDown)
)

func (mm *MainMenu) render() {
	gfx.GL.ClearColor(0.25, 0.25, 0.25, 1)
	gfx.GL.Clear(gl.COLOR_BUFFER_BIT)

	mm.Camera.PushTranslation(0, float32(math.Max(0, 1.65*float64(mm.Selection-1))), 0)

	if mm.tb == nil {
		mm.tb = sprites.NewBatch(&mm.Camera)
	} else {
		mm.tb.Reset(&mm.Camera)
	}

	for i, opt := range Options {
		color := sprites.White
		if i == mm.Selection {
			color = sprites.Rainbow
		}

		sprites.DrawTextShadow(mm.tb, sprites.FontD3Streetism, opt.Name, -7, 2-1.65*float32(i), -3, 1.5, 2, color)
	}

	mm.tb.Render()

	mm.Camera.PopTransform()

	if mm.batch == nil {
		mm.batch = sprites.NewBatch(&mm.Camera)
	} else {
		mm.batch.Reset(&mm.Camera)
	}

	mm.batch.Append(sprites.Blank, 0, 6.1, 3, 30, 3.1, sprites.Color{R: 31, G: 31, B: 31, A: 191})
	mm.batch.Render()

	mm.tb.Reset(&mm.Camera)

	sprites.DrawTextCentered(mm.tb, sprites.FontD3Streetism, versionText, 7.5, 7, 0, 0.75, 0.75, sprites.White, true)

	counter := 31 - int(time.Now().Unix()%31)

	sprites.DrawTextCenteredFunc(mm.tb, sprites.FontD3Streetism, "Termacade", 1, 4.8, 0, 3, 3, func(rune) sprites.Color {
		counter++
		switch (counter % 31) / 10 {
		default:
			return sprites.Color{R: 191, G: 255, B: 255, A: 255}
		case 1:
			return sprites.Color{R: 191, G: 255, B: 191, A: 255}
		case 2:
			return sprites.Color{R: 255, G: 255, B: 191, A: 255}
		case 3:
			return sprites.Color{R: 255, G: 191, B: 191, A: 255}
		}
	}, true)

	mm.tb.Render()

	if mm.neonLoaded {
		mm.batch.Reset(&mm.Camera)
		RenderNeon(mm.batch, neonMain...)
		mm.batch.Render()
	}

	if !mm.LastSelectionChange.IsZero() {
		var m gfx.Matrix

		m.RotationX(-10 * math.Pi / 180)
		mm.Camera.PushTransform(&m)

		selectionChangeAgo := time.Since(mm.LastSelectionChange)

		if mm.neonLoaded &&
			Options[mm.Selection].Neon != nil &&
			(selectionChangeAgo > time.Millisecond*1250 ||
				(selectionChangeAgo > time.Millisecond*600 &&
					selectionChangeAgo < time.Millisecond*1200 &&
					selectionChangeAgo%(time.Millisecond*200) < time.Millisecond*150)) {
			mm.batch.Reset(&mm.Camera)
			mm.batch.Append(sprites.Blank, 6.2, 0.5, 0, 8, 6, sprites.Color{R: 0, G: 0, B: 0, A: 191})

			if selectionChangeAgo > time.Millisecond*1250 {
				RenderNeon(mm.batch, Options[mm.Selection].Neon...)
			}

			mm.batch.Render()
		}

		action := Options[mm.Selection].Action
		if action == "" {
			action = defaultAction
		}

		mm.tb.Reset(&mm.Camera)

		sprites.DrawTextCentered(mm.tb, sprites.FontD3Streetism, action, 6.2, -3.3, 0, 1, 1, sprites.White, true)
		sprites.DrawTextCentered(mm.tb, sprites.FontD3Streetism, selectText, 6.2, -4.2, 0, 1, 1, sprites.White, true)

		mm.tb.Render()

		mm.Camera.PopTransform()
	}
}
