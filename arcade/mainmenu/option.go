package mainmenu

import (
	"context"
	"runtime"
	"time"

	"git.lubar.me/ben/spy-cards/arcade"
	"git.lubar.me/ben/spy-cards/gfx/sprites"
	"git.lubar.me/ben/spy-cards/input"
	"git.lubar.me/ben/spy-cards/internal/router"
)

// Option is a Termacade main menu option.
type Option struct {
	Hidden bool
	Game   arcade.Game
	Name   string
	Func   func(ctx context.Context) (*router.PageInfo, error)
	Neon   []*Neon
	Action string
}

// Options are the default Termacade main menu options.
var Options = []*Option{
	{
		Game: arcade.FlowerJourney,
		Name: "Flower Journey",
		Func: func(ctx context.Context) (*router.PageInfo, error) {
			return &router.PageInfo{
				Page: router.PageArcadeGame,
				Game: arcade.FlowerJourney,
			}, nil
		},
		Neon: neonFJ,
	},
	{
		Game: arcade.MiteKnight,
		Name: "Mite Knight",
		Func: func(ctx context.Context) (*router.PageInfo, error) {
			return &router.PageInfo{
				Page: router.PageArcadeGame,
				Game: arcade.MiteKnight,
			}, nil
		},
		Neon: neonMK,
	},
	{
		Name:   "Fortmite",
		Func:   nil,
		Hidden: time.Now().Month() != time.April || time.Now().Day() != 1,
	},
	{
		Name: "High Scores",
		Func: func(ctx context.Context) (*router.PageInfo, error) {
			return &router.PageInfo{
				Page:   router.PageArcadeHighScores,
				Game:   arcade.FlowerJourney,
				HSMode: arcade.AllTime,
			}, nil
		},
		Action: "Press " + sprites.Button(input.BtnConfirm) + " to view!",
		Neon:   neonHS,
	},
	{
		Name: "Settings",
		Func: func(context.Context) (*router.PageInfo, error) {
			return &router.PageInfo{
				Page: router.PageStatic,
				Raw:  "/settings.html",
			}, nil
		},
		Action: "Press " + sprites.Button(input.BtnConfirm) + " to edit",
		Hidden: runtime.GOOS != "js",
	},
}
