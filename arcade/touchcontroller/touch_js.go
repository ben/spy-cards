//go:build js && wasm
// +build js,wasm

package touchcontroller

import (
	"time"

	"git.lubar.me/ben/spy-cards/internal"
)

func platformSpecific() {
	if internal.LikelyTouch() && sz.PixelsPerPt > 1 {
		lastTouch = time.Now()
	}
}
