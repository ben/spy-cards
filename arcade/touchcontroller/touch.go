// Package touchcontroller implements a touch screen-based gamepad.
package touchcontroller

import (
	"math"
	"time"

	"git.lubar.me/ben/spy-cards/gfx"
	"git.lubar.me/ben/spy-cards/gfx/sprites"
	"git.lubar.me/ben/spy-cards/input"
	"golang.org/x/mobile/event/size"
	"golang.org/x/mobile/event/touch"
)

var (
	// SkipFrame causes the touch controller to be inactive for 1 frame.
	SkipFrame bool
	// StartSelect causes the touch controller to display the start/select buttons for 1 frame.
	StartSelect bool

	screenSize float32
	aspect     float32
	touches    []touchPoint
	firstTouch time.Time
	lastTouch  time.Time
	sz         size.Event

	batch *sprites.Batch

	cam = func() gfx.Camera {
		var cam gfx.Camera

		cam.SetDefaults()
		const scale = -1.0
		cam.Perspective.Scale(scale, scale, scale)
		cam.Offset.Identity()
		cam.Rotation.Identity()
		cam.Position.Identity()

		return cam
	}()
)

type touchPoint struct {
	X, Y    float32
	Pressed bool
}

type buttonState struct {
	Pressed bool
	cx, cy  float32 // corner
	ox, oy  float32 // offset
	width   float32
	height  float32
	radius  float32
}

var buttons = [10]*buttonState{
	input.BtnUp: {
		cx:     0,
		cy:     1,
		ox:     1.825,
		oy:     -2.875,
		width:  1.5,
		height: 1.5,
		radius: 0.75,
	},
	input.BtnDown: {
		cx:     0,
		cy:     1,
		ox:     1.825,
		oy:     -0.875,
		width:  1.5,
		height: 1.5,
		radius: 0.75,
	},
	input.BtnLeft: {
		cx:     0,
		cy:     1,
		ox:     0.825,
		oy:     -1.875,
		width:  1.5,
		height: 1.5,
		radius: 0.75,
	},
	input.BtnRight: {
		cx:     0,
		cy:     1,
		ox:     2.825,
		oy:     -1.875,
		width:  1.5,
		height: 1.5,
		radius: 0.75,
	},
	input.BtnConfirm: {
		cx:     1,
		cy:     1,
		ox:     -1.875,
		oy:     -0.875,
		width:  1.5,
		height: 1.5,
		radius: 0.75,
	},
	input.BtnCancel: {
		cx:     1,
		cy:     1,
		ox:     -0.875,
		oy:     -1.875,
		width:  1.5,
		height: 1.5,
		radius: 0.75,
	},
	input.BtnSwitch: {
		cx:     1,
		cy:     1,
		ox:     -2.875,
		oy:     -1.875,
		width:  1.5,
		height: 1.5,
		radius: 0.75,
	},
	input.BtnToggle: {
		cx:     1,
		cy:     1,
		ox:     -1.875,
		oy:     -2.875,
		width:  1.5,
		height: 1.5,
		radius: 0.75,
	},
	input.BtnPause: {
		cx:     1,
		cy:     0,
		ox:     -0.875,
		oy:     0.725,
		width:  1.5,
		height: 1.125,
		radius: 0.25,
	},
	input.BtnHelp: {
		cx:     0,
		cy:     0,
		ox:     0.825,
		oy:     0.725,
		width:  1.5,
		height: 1.125,
		radius: 0.25,
	},
}

// Held returns the currently held buttons.
func Held() ([]input.Button, time.Time) {
	var held []input.Button

	if !SkipFrame {
		for btn, state := range buttons {
			if state.Pressed {
				held = append(held, input.Button(btn))
			}
		}
	}

	return held, lastTouch
}

// Size is a callback for screen resize events.
func Size(e size.Event) {
	sz = e

	aspect = float32(sz.WidthPx) / float32(sz.HeightPx)
	screenSize = float32(sz.HeightPt) / 72

	if screenSize < 8 {
		screenSize = 8
	}

	platformSpecific()
}

// Touch is a callback for touchscreen or mouse pointer events.
func Touch(e touch.Event) {
	for e.Sequence >= touch.Sequence(len(touches)) {
		touches = append(touches, touchPoint{})
	}

	switch e.Type {
	case touch.TypeBegin:
		touches[e.Sequence].Pressed = true
	case touch.TypeEnd:
		touches[e.Sequence].Pressed = false
	}

	lastTouch = time.Now()

	if firstTouch.IsZero() {
		firstTouch = lastTouch
	}

	touches[e.Sequence].X = e.X / float32(sz.WidthPx)
	touches[e.Sequence].Y = e.Y / float32(sz.HeightPx)

	updateButtons()
}

func updateButtons() {
	for _, state := range buttons {
		state.Pressed = false

		for _, t := range touches {
			if !t.Pressed {
				continue
			}

			x := t.X - state.cx
			y := t.Y - state.cy

			x *= screenSize * aspect
			y *= screenSize

			x -= state.ox
			y -= state.oy

			if x < 0 {
				x = -x
			}

			if y < 0 {
				y = -y
			}

			x -= state.width/2 - state.radius
			y -= state.height/2 - state.radius

			if x < 0 {
				x = 0
			}

			if y < 0 {
				y = 0
			}

			if math.Hypot(float64(x), float64(y)) <= float64(state.radius) {
				state.Pressed = true

				break
			}
		}
	}
}

// Render draws the touch controller to the screen.
func Render() {
	if SkipFrame {
		SkipFrame = false

		return
	}

	for _, state := range buttons {
		if state.Pressed {
			lastTouch = time.Now()

			break
		}
	}

	sinceTouch := time.Since(lastTouch).Seconds()/2 - 4
	if sinceTouch > 1 {
		firstTouch = time.Time{}

		return
	}

	if sinceTouch < 0 {
		sinceTouch = math.Max(0.25-time.Since(firstTouch).Seconds(), 0)
	}

	activeColor := sprites.White
	inactiveColor := sprites.Color{A: uint8(127 * (1 - sinceTouch))}
	inactiveColor.R, inactiveColor.G, inactiveColor.B = inactiveColor.A, inactiveColor.A, inactiveColor.A
	drawButton := func(btn input.Button, style int, ox, oy float32) {
		c := inactiveColor
		if buttons[btn].Pressed {
			c = activeColor
		}

		batch.AppendEx(sprites.GamepadButton[btn][style], ox, oy, 0, 2, 2, c, sprites.FlagNoDiscard, 0, 0, 0)
	}

	var t, s gfx.Matrix

	scale := 2 / screenSize
	t.Translation(-1+1.2*scale/aspect, -1+1.5*scale, 0)
	cam.PushTransform(&t)
	s.Scale(scale/aspect, scale, 1)
	cam.PushTransform(&s)

	if batch == nil {
		batch = sprites.NewBatch(&cam)
	} else {
		batch.Reset(&cam)
	}

	drawButton(input.BtnUp, 1, 0, 1)
	drawButton(input.BtnDown, 1, 0, -1)
	drawButton(input.BtnLeft, 1, -1, 0)
	drawButton(input.BtnRight, 1, 1, 0)

	batch.Render()

	cam.PopTransform()
	cam.PopTransform()

	t.Translation(1-2.5*scale/aspect, -1+1.5*scale, 0)
	cam.PushTransform(&t)
	s.Scale(scale/aspect, scale, 1)
	cam.PushTransform(&s)

	batch.Reset(&cam)

	drawButton(input.BtnConfirm, 2, 0, -1)
	drawButton(input.BtnCancel, 2, 1, 0)
	drawButton(input.BtnSwitch, 2, -1, 0)
	drawButton(input.BtnToggle, 2, 0, 1)

	batch.Render()

	cam.PopTransform()
	cam.PopTransform()

	if StartSelect {
		t.Translation(-1+0.2*scale/aspect, 1-scale, 0)
		cam.PushTransform(&t)
		s.Scale(scale/aspect, scale, 1)
		cam.PushTransform(&s)

		batch.Reset(&cam)

		drawButton(input.BtnHelp, 1, 0, 0)

		batch.Render()

		cam.PopTransform()
		cam.PopTransform()

		t.Translation(1-1.5*scale/aspect, 1-scale, 0)
		cam.PushTransform(&t)
		s.Scale(scale/aspect, scale, 1)
		cam.PushTransform(&s)

		batch.Reset(&cam)

		drawButton(input.BtnPause, 1, 0, 0)

		batch.Render()

		cam.PopTransform()
		cam.PopTransform()

		StartSelect = false
	}
}
