//go:build !js || !wasm
// +build !js !wasm

package match

import "runtime"

func (r *Recording) yielder() (func(), func()) {
	return runtime.Gosched, func() {}
}
