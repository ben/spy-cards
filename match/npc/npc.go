// Package npc implements computer-controlled Spy Cards Online players.
package npc

import (
	"fmt"
	"strings"
	"time"

	"git.lubar.me/ben/spy-cards/card"
	"git.lubar.me/ben/spy-cards/match"
	"git.lubar.me/ben/spy-cards/room"
)

// NPC is a computer-controlled Spy Cards Online player.
type NPC interface {
	// PickPlayer returns a player character to represent this NPC.
	PickPlayer() *room.Character

	// CreateDeck returns a slice of cards to use as a deck.
	CreateDeck(set *card.Set) card.Deck

	// PlayRound returns a bitmask of cards from the player's hand to play.
	PlayRound(m *match.Match) uint64

	// AfterRound is called after a round's winner has been determined.
	AfterRound(m *match.Match)

	fmt.Stringer
}

func mustDeck(s string) card.Deck {
	var d card.Deck

	if err := d.UnmarshalText([]byte(s)); err != nil {
		panic(err)
	}

	return d
}

// Get returns an NPC for the specified identifier.
func Get(name string) (NPC, error) {
	if strings.HasPrefix(name, "cm-") {
		parts := strings.SplitN(name, "-", 3)

		if len(parts) < 3 {
			parts = append(parts, "")
		}

		var deck card.Deck

		if err := deck.UnmarshalText([]byte(parts[1])); err != nil {
			return nil, fmt.Errorf("npc: decoding cm- NPC identifier: %w", err)
		}

		return &CardMaster{
			Character: room.CharacterByName[parts[2]],
			Deck:      deck,
		}, nil
	}

	switch name {
	case "janet":
		return &Janet{}, nil
	case "bu-gi", "johnny", "kage", "ritchee", "serene", "carmina":
		return &TourneyPlayer{Character: room.CharacterByName[name]}, nil
	case "saved-decks":
		return &SavedDecks{}, nil
	case "tutorial":
		return &CardMaster{
			Character: room.CharacterByName["carmina"],
			Deck:      mustDeck("01H00000000013HSMR"),
		}, nil
	case "carmina2":
		return &CardMaster{
			Character: room.CharacterByName["carmina"],
			Deck:      mustDeck("3P7T52H8MA5273HG842YF7KR"),
		}, nil
	case "chuck":
		return &CardMaster{
			Character: room.CharacterByName["chuck"],
			Deck:      mustDeck("4HH0000007VXYZFG84210GG8"),
		}, nil
	case "arie":
		return &CardMaster{
			Character: room.CharacterByName["arie"],
			Deck:      mustDeck("310J10G84212NANCPAD6K7VW"),
		}, nil
	case "shay":
		return &CardMaster{
			Character: room.CharacterByName["shay"],
			Deck:      mustDeck("511KHRWE631GRD6K9MMA5840"),
		}, nil
	case "crow":
		return &CardMaster{
			Character: room.CharacterByName["crow"],
			Deck:      mustDeck("101AXEPH8MCJ8G845G"),
		}, nil
	case "genow":
		return &CardMaster{
			Character: room.CharacterByName["genow"],
			Deck:      mustDeck("6N5Q3HRWE6VDQ6K9G8"),
		}, nil
	case "mender-spam":
		return &MenderSpam{
			Mothiva: time.Now().Unix()&1 == 0,
		}, nil
	case "mender-spam-mothiva":
		return &MenderSpam{
			Mothiva: true,
		}, nil
	case "mender-spam-kali":
		return &MenderSpam{
			Mothiva: false,
		}, nil
	case "tp2-generic":
		return &TourneyPlayer2{
			AdjustWeight: genericNPCWeight,
		}, nil
	case "tp2-janet":
		return &TourneyPlayer2{
			TourneyPlayer: TourneyPlayer{
				Character: room.CharacterByName["janet"],
			},
			AdjustWeight: janetWeight,
			WeightDesc:   " that favors Janet's signature card",
		}, nil
	case "tp2-bu-gi":
		return &TourneyPlayer2{
			TourneyPlayer: TourneyPlayer{
				Character: room.CharacterByName["bu-gi"],
			},
			AdjustWeight: buGiWeight,
			WeightDesc:   " that favors attacker cards",
		}, nil
	case "tp2-johnny":
		return &TourneyPlayer2{
			TourneyPlayer: TourneyPlayer{
				Character: room.CharacterByName["johnny"],
			},
			AdjustWeight: johnnyWeight,
			WeightDesc:   " that favors cards with non-random conditions and mini-boss and boss cards with empower",
		}, nil
	case "tp2-kage":
		return &TourneyPlayer2{
			TourneyPlayer: TourneyPlayer{
				Character: room.CharacterByName["kage"],
			},
			AdjustWeight: kageWeight,
			WeightDesc:   " that favors cards with multiple tribes and cards with unity or empower",
		}, nil
	case "tp2-ritchee":
		return &TourneyPlayer2{
			TourneyPlayer: TourneyPlayer{
				Character: room.CharacterByName["ritchee"],
			},
			AdjustWeight: ritcheeWeight,
			WeightDesc:   " that favors cards with high TP cost",
		}, nil
	case "tp2-serene":
		return &TourneyPlayer2{
			TourneyPlayer: TourneyPlayer{
				Character: room.CharacterByName["serene"],
			},
			AdjustWeight: sereneWeight,
			WeightDesc:   " that favors cards with low TP cost",
		}, nil
	case "tp2-carmina":
		return &TourneyPlayer2{
			TourneyPlayer: TourneyPlayer{
				Character: room.CharacterByName["carmina"],
			},
			AdjustWeight: carminaWeight,
			WeightDesc:   " that favors cards with random chances",
		}, nil
	case "tp2-chuck":
		return &TourneyPlayer2{
			TourneyPlayer: TourneyPlayer{
				Character: room.CharacterByName["chuck"],
			},
			AdjustWeight: chuckWeight,
			WeightDesc:   " that favors seedlings",
		}, nil
	case "tp2-arie":
		return &TourneyPlayer2{
			TourneyPlayer: TourneyPlayer{
				Character: room.CharacterByName["arie"],
			},
			AdjustWeight: arieWeight,
			WeightDesc:   " that favors cards that cost near to 2 TP and avoids cards that are fungi or are bug and another tribe",
		}, nil
	case "tp2-shay":
		return &TourneyPlayer2{
			TourneyPlayer: TourneyPlayer{
				Character: room.CharacterByName["shay"],
			},
			AdjustWeight: shayWeight,
			WeightDesc:   " that favors thugs and cards with numb",
		}, nil
	case "tp2-crow":
		return &TourneyPlayer2{
			TourneyPlayer: TourneyPlayer{
				Character: room.CharacterByName["crow"],
			},
			AdjustWeight: crowWeight,
			WeightDesc:   " that favors non-boss ??? cards, bot cards below 6 TP, and cards with both ATK and a non-card-based condition",
		}, nil
	case "", "generic":
		return &GenericNPC{}, nil
	default:
		return nil, fmt.Errorf("npc: unknown NPC identifier %q", name)
	}
}
