package npc

import (
	"log"

	"git.lubar.me/ben/spy-cards/card"
	"git.lubar.me/ben/spy-cards/match"
	"git.lubar.me/ben/spy-cards/room"
)

// SavedDecks is an NPC that selects a prebuilt deck from the player's saved
// decks, if possible.
type SavedDecks struct {
	GenericNPC
}

// CreateDeck returns a random valid saved deck, falling back to the default
// deck construction algorithm if no saved decks are valid.
func (s *SavedDecks) CreateDeck(set *card.Set) card.Deck {
	decks := match.LoadDecks()

	for i := 0; i < len(decks); i++ {
		if err := decks[i].Validate(set); err != nil {
			decks = append(decks[:i], decks[i+1:]...)
			i--
		}
	}

	if len(decks) != 0 {
		return decks[s.randn(len(decks))]
	}

	return s.GenericNPC.CreateDeck(set)
}

func (s *SavedDecks) String() string {
	return "Saved Decks: picks a random saved deck, and plays using simple AI"
}

// MenderSpam implements the "mender spam" playstyle.
type MenderSpam struct {
	GenericNPC

	Mothiva bool
}

// PickPlayer selects a player based on the miniboss cards in the deck.
func (m *MenderSpam) PickPlayer() *room.Character {
	if m.randn(2) == 1 {
		if m.Mothiva {
			return room.CharacterByName["zasp"]
		}

		return room.CharacterByName["kabbu"]
	}

	if m.Mothiva {
		return room.CharacterByName["mothiva"]
	}

	return room.CharacterByName["kali"]
}

// CreateDeck creates a "mender spam" deck.
func (m *MenderSpam) CreateDeck(set *card.Set) card.Deck {
	rules, _ := set.Mode.Get(card.FieldGameRules).(*card.GameRules)
	if rules == nil {
		rules = &card.DefaultGameRules
	}

	if rules.BossCards != 1 || rules.MiniBossCards != 2 {
		log.Println("ERROR: npc: cannot create mender deck with non-standard number of (mini-)boss cards")

		return nil
	}

	deck := make(card.Deck, rules.CardsPerDeck)

	deck[0] = card.HeavyDroneB33

	if m.Mothiva {
		deck[1] = card.Mothiva
		deck[2] = card.Zasp
	} else {
		deck[1] = card.Kali
		deck[2] = card.Kabbu
	}

	for i := 3; i < len(deck); i++ {
		deck[i] = card.Mender
	}

	return deck
}

// PlayRound implements the logic for the "mender spam" playstyle.
func (m *MenderSpam) PlayRound(ctx *match.Match) uint64 {
	hand := ctx.State.Sides[ctx.Perspective-1].Hand
	mothiva, zasp, b33 := -1, -1, -1

	for i, c := range hand {
		switch c.Def.ID {
		case card.Mothiva, card.Kali:
			mothiva = i
		case card.Zasp, card.Kabbu:
			zasp = i
		case card.HeavyDroneB33:
			b33 = i
		}
	}

	tp := ctx.State.Sides[ctx.Perspective-1].TP

	if mothiva != -1 && zasp != -1 {
		cost := card.Num(hand[mothiva].Def.TP + hand[zasp].Def.TP)
		cost.Add(ctx.State.Sides[ctx.Perspective-1].ModTP[hand[mothiva].Def.ID], ctx.SpecialFlags[card.SpecialSubtractInf])
		cost.Add(ctx.State.Sides[ctx.Perspective-1].ModTP[hand[zasp].Def.ID], ctx.SpecialFlags[card.SpecialSubtractInf])

		if !tp.Less(cost) {
			return 1<<mothiva | 1<<zasp
		}
	}

	if len(hand) < 5 {
		return 0
	}

	mothivaOrZasp := mothiva
	if mothiva == -1 {
		mothivaOrZasp = zasp
	}

	if mothivaOrZasp != -1 {
		cost := card.Num(hand[mothivaOrZasp].Def.TP)
		cost.Add(ctx.State.Sides[ctx.Perspective-1].ModTP[hand[mothivaOrZasp].Def.ID], ctx.SpecialFlags[card.SpecialSubtractInf])

		if !tp.Less(cost) {
			return 1 << mothivaOrZasp
		}
	}

	if card.Num(2).Less(ctx.State.Sides[ctx.Perspective-1].HP) {
		return 0
	}

	if b33 != -1 {
		cost := card.Num(hand[b33].Def.TP)
		cost.Add(ctx.State.Sides[ctx.Perspective-1].ModTP[hand[b33].Def.ID], ctx.SpecialFlags[card.SpecialSubtractInf])

		if !tp.Less(cost) {
			return 0
		}
	}

	menderCost := card.Num(ctx.Set.Card(card.Mender).TP * 5)

	for i := 0; i < 5; i++ {
		menderCost.Add(ctx.State.Sides[ctx.Perspective-1].ModTP[card.Mender], ctx.SpecialFlags[card.SpecialSubtractInf])
	}

	if b33 == -1 && tp.Less(menderCost) {
		return 0
	}

	var toPlay uint64

	if b33 != -1 {
		cost := card.Num(hand[b33].Def.TP)
		cost.Add(ctx.State.Sides[ctx.Perspective-1].ModTP[hand[b33].Def.ID], ctx.SpecialFlags[card.SpecialSubtractInf])
		cost.Negate()

		tp.Add(cost, ctx.SpecialFlags[card.SpecialSubtractInf])

		toPlay |= 1 << b33
	}

	for i := range hand {
		if toPlay&(1<<i) != 0 {
			continue
		}

		cost := card.Num(hand[i].Def.TP)
		cost.Add(ctx.State.Sides[ctx.Perspective-1].ModTP[hand[i].Def.ID], ctx.SpecialFlags[card.SpecialSubtractInf])

		if !tp.Less(cost) {
			cost.Negate()

			tp.Add(cost, ctx.SpecialFlags[card.SpecialSubtractInf])

			toPlay |= 1 << i
		}
	}

	return toPlay
}

func (m *MenderSpam) String() string {
	return "Mender Spam: uses a stalling gameplay style known as Mender Spam"
}
