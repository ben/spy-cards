package npc

import (
	"log"
	"math/rand"
	"time"

	"git.lubar.me/ben/spy-cards/card"
	"git.lubar.me/ben/spy-cards/match"
	"git.lubar.me/ben/spy-cards/room"
)

// GenericNPC implements the Bug Fables Spy Cards AI.
type GenericNPC struct {
	r *rand.Rand
}

func (g *GenericNPC) rng() *rand.Rand {
	if g.r == nil {
		g.r = rand.New(rand.NewSource(time.Now().UnixNano())) /*#nosec*/
	}

	return g.r
}

func (g *GenericNPC) randn(n int) int {
	return g.rng().Intn(n)
}

// PickPlayer picks a random player character.
func (g *GenericNPC) PickPlayer() *room.Character {
	c := room.PlayerCharacters[g.randn(len(room.PlayerCharacters))]

	for c.Hidden {
		c = room.PlayerCharacters[g.randn(len(room.PlayerCharacters))]
	}

	return c
}

// CreateDeck creates a completely random deck, avoiding ELK.
func (g *GenericNPC) CreateDeck(set *card.Set) card.Deck {
	rules, _ := set.Mode.Get(card.FieldGameRules).(*card.GameRules)
	if rules == nil {
		rules = &card.DefaultGameRules
	}

	d := make(card.Deck, rules.CardsPerDeck)

	for i := range d {
		var available []*card.Def

		switch {
		case i < int(rules.BossCards):
			available = set.ByBack(card.Boss, append(card.Deck{card.TheEverlastingKing}, d[:i]...))
		case i < int(rules.BossCards+rules.MiniBossCards):
			available = set.ByBack(card.MiniBoss, d[:i])
		default:
			available = set.ByBack(card.Enemy, d[:i])
		}

		if len(available) == 0 {
			log.Printf("ERROR: cannot pick %d-th card: no cards available", i)

			return nil
		}

		d[i] = available[g.randn(len(available))].ID
	}

	return d
}

// PlayRound plays cards, if possible, from left to right.
func (g *GenericNPC) PlayRound(m *match.Match) uint64 {
	var choices uint64

	hand := m.State.Sides[m.Perspective-1].Hand
	tp := m.State.Sides[m.Perspective-1].TP

	if tp.NaN {
		return 0
	}

	if tp.AmountInf > 0 {
		return (1 << len(hand)) - 1
	}

	maxCards := m.Rules[m.Perspective-1].CardsPerRound

	for i, c := range hand {
		cost := card.Num(c.Def.TP)
		cost.Add(m.State.Sides[m.Perspective-1].ModTP[c.Def.ID], m.SpecialFlags[card.SpecialSubtractInf])

		if !tp.Less(cost) {
			cost.Negate()
			tp.Add(cost, m.SpecialFlags[card.SpecialSubtractInf])

			choices |= 1 << i

			if maxCards != 0 {
				maxCards--

				if maxCards == 0 {
					break
				}
			}
		}
	}

	return choices
}

// AfterRound does nothing.
func (g *GenericNPC) AfterRound(m *match.Match) {}

func (g *GenericNPC) String() string {
	return "Generic: uses a completely random deck and simple AI"
}

// CardMaster is a generic NPC with a predefined deck and character.
type CardMaster struct {
	GenericNPC

	Character *room.Character
	Deck      card.Deck
}

// PickPlayer returns a predefined player character.
func (cm *CardMaster) PickPlayer() *room.Character {
	if cm.Character != nil {
		return cm.Character
	}

	return cm.GenericNPC.PickPlayer()
}

// CreateDeck returns a predefined deck.
func (cm *CardMaster) CreateDeck(set *card.Set) card.Deck {
	return cm.Deck
}

func (cm *CardMaster) String() string {
	char := ""
	if cm.Character != nil {
		char = " (" + cm.Character.DisplayName + ")"
	}

	return "Card Master" + char + ": uses a predefined deck and simple AI"
}

var predefinedDecks11 = [...]string{
	"511KHRRE631GRD6K1M",
	"101AXEGH8MCJ8G845G",
	"01H00000000013HS4R",
	"3J7T52H8MA5273HG04",
	"4HH0000007VHYZFG04",
	"310J10G84212NANC68",
	"0K0WP8H9MT90J8NA4R",
	"74S6K98NJS8036K340",
	"5QBNJS93BNTJ73C608",
	"406H0G852H8GC63H0W",
	"6KM6VDGVE73HRWKS4W",
	"2GSQKSRXEQBHVGRC64",
	"7PAQKSRXEQBHVGRC64",
	"22RBDPSDQBNJXFQV5W",
	"81H0000007VHYZFG04",
}

// TourneyPlayer is a generic NPC with a predefined character.
type TourneyPlayer struct {
	GenericNPC

	Character *room.Character
}

func (tp *TourneyPlayer) predefined(set *card.Set, janet int) card.Deck {
	// can't use predefined decks if they're illegal.
	if rules, _ := set.Mode.Get(card.FieldGameRules).(*card.GameRules); rules != nil && (rules.CardsPerDeck != 15 || rules.BossCards != 1 || rules.MiniBossCards != 2) {
		return nil
	}

	for attempts := 0; attempts < 10; attempts++ {
		i := tp.randn(len(predefinedDecks11) - 1 + janet + 3)
		if i >= len(predefinedDecks11)-1+janet {
			return nil
		}

		var d card.Deck

		if err := d.UnmarshalText([]byte(predefinedDecks11[i])); err != nil {
			panic(err)
		}

		if d.Validate(set) != nil {
			continue // not valid with these game rules or bans. try again.
		}

		return d
	}

	// give up.
	return nil
}

// CreateDeck returns a predefined deck with a 14 in 17 chance.
func (tp *TourneyPlayer) CreateDeck(set *card.Set) card.Deck {
	if d := tp.predefined(set, 0); d != nil {
		return d
	}

	return tp.GenericNPC.CreateDeck(set)
}

// PickPlayer returns a predefined player character.
func (tp *TourneyPlayer) PickPlayer() *room.Character {
	if tp.Character != nil {
		return tp.Character
	}

	return tp.GenericNPC.PickPlayer()
}

func (tp *TourneyPlayer) String() string {
	char := ""
	if tp.Character != nil {
		char = " (" + tp.Character.DisplayName + ")"
	}

	return "Tourney Player" + char + ": uses the Bug Fables deck building logic and simple AI"
}

// Janet is... Janet.
type Janet struct {
	TourneyPlayer
}

// PickPlayer returns janet.
func (j *Janet) PickPlayer() *room.Character {
	return room.CharacterByName["janet"]
}

// CreateDeck has a 50% chance of picking ELK as a boss card.
func (j *Janet) CreateDeck(set *card.Set) card.Deck {
	if d := j.predefined(set, 1); d != nil {
		return d
	}

	d := j.GenericNPC.CreateDeck(set)

	rules, _ := set.Mode.Get(card.FieldGameRules).(*card.GameRules)
	if rules == nil {
		rules = &card.DefaultGameRules
	}

	pickable := false

	for _, c := range set.ByBack(card.Boss, d) {
		if c.ID == card.TheEverlastingKing {
			pickable = true

			break
		}
	}

	for i := 0; i < int(rules.CardsPerDeck) && i < int(rules.BossCards); i++ {
		if pickable && j.randn(2) == 1 {
			d[i] = card.TheEverlastingKing

			break
		}
	}

	return d
}

func (j *Janet) String() string {
	return "Tourney Player (Janet): uses the Bug Fables deck building logic and simple AI"
}
