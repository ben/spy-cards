package match

import (
	"fmt"
	"log"
	"math/bits"
	"reflect"
	"sort"
	"time"

	"git.lubar.me/ben/spy-cards/card"
	"git.lubar.me/ben/spy-cards/format"
	"git.lubar.me/ben/spy-cards/gfx/sprites"
	"git.lubar.me/ben/spy-cards/internal/router"
	"git.lubar.me/ben/spy-cards/rng"
	"git.lubar.me/ben/spy-cards/scnet"
)

func (m *Match) IndexSet() {
	rules, _ := m.Set.Mode.Get(card.FieldGameRules).(*card.GameRules)
	if rules == nil {
		m.Rules[0] = card.DefaultGameRules
	} else {
		m.Rules[0] = *rules
	}

	m.Rules[1] = m.Rules[0]

	t, _ := m.Set.Mode.Get(card.FieldTimer).(*card.Timer)
	if t != nil {
		m.Timer = *t
	} else {
		m.Timer = card.Timer{}
	}

	m.Banned = make(map[card.ID]bool)
	m.Unpickable = make(map[card.ID]bool)
	m.Unfiltered = make(map[card.ID]bool)
	m.HasReplaceSummon = make(map[card.ID]bool)
	m.OrderForSummon = make(map[card.ID]int)
	m.SpecialFlags = make(map[card.SpecialFlag]bool)

	for _, f := range m.Set.Mode.GetAll(card.FieldUnfilterCard) {
		uc := f.(*card.UnfilterCard)

		m.Unfiltered[uc.ID] = true
	}

	banVanilla := false

	for _, f := range m.Set.Mode.GetAll(card.FieldBannedCards) {
		bc := f.(*card.BannedCards)

		banMap := m.Banned

		switch bc.Flags & card.BannedCardTypeMask {
		case card.BannedCardTypeBanned:
		case card.BannedCardTypeUnpickable:
			banMap = m.Unpickable
		default:
			continue
		}

		if len(bc.Cards) == 0 {
			if bc.Flags&card.BannedCardTypeMask == card.BannedCardTypeBanned {
				banVanilla = true
			}

			for i := card.ID(0); i < 128; i++ {
				banMap[i] = true
			}
		} else {
			for _, id := range bc.Cards {
				banMap[id] = true
			}
		}
	}

	for _, bc := range m.Set.Spoiler {
		banMap := m.Banned
		if bc.Flags&card.BannedCardTypeMask == card.BannedCardTypeUnpickable {
			banMap = m.Unpickable
		}

		for _, id := range bc.Cards {
			banMap[id] = true
		}
	}

	carminaReplaced := false

	for _, c := range m.Set.Cards {
		if c.ID == card.Carmina {
			carminaReplaced = true
		}

		for _, e := range c.Effects {
			if e.Type == card.EffectSummon && e.Flags&card.FlagSummonReplace != 0 {
				m.HasReplaceSummon[c.ID] = true
			}
		}
	}

	if !carminaReplaced {
		m.HasReplaceSummon[card.Carmina] = true
	}

	for i := card.ID(0); i < 128; i++ {
		if bi := i.BasicIndex(); bi != -1 {
			switch i.Rank() {
			case card.Boss:
				m.OrderForSummon[i] = bi
			case card.MiniBoss:
				m.OrderForSummon[i] = bi + 32
			case card.Attacker, card.Effect:
				m.OrderForSummon[i] = bi + 64
			}
		}
	}

	for i, c := range m.Set.Cards {
		m.OrderForSummon[c.ID] = i + 128
	}

	m.filterCache = m.filterCache[:0]

	for _, f := range m.Set.Mode.GetAll(card.FieldSpecialFlags) {
		sf := f.(*card.SpecialFlags)
		for x, set := range sf.Set {
			if set {
				m.SpecialFlags[x] = true
			}
		}
	}

	m.NoStats = true

	if !banVanilla {
		for i := card.ID(0); i < 128; i++ {
			c := m.Set.Card(i)
			if c == nil {
				continue
			}

			for _, e := range c.Effects {
				if anyStat(e) {
					m.NoStats = false

					break
				}
			}

			if !m.NoStats {
				break
			}
		}
	}

	if m.NoStats {
		for _, c := range m.Set.Cards {
			for _, e := range c.Effects {
				if anyStat(e) {
					m.NoStats = false

					break
				}
			}

			if !m.NoStats {
				break
			}
		}
	}
}

func anyStat(e *card.EffectDef) bool {
	if e == nil {
		return false
	}

	switch e.Type {
	case card.EffectStat:
		return true
	case card.EffectEmpower:
		return true
	case card.EffectRawStat:
		return true
	default:
		return anyStat(e.Result) || anyStat(e.Result2)
	}
}

func (m *Match) cardDescription(c *card.Def) *card.RichDescription {
	return c.Description(m.Set)
}

func (m *Match) InitState(rng *rng.RNG, network Network, onSummon func(*ActiveCard)) {
	m.Start = time.Now()

	for i := range m.State.Sides {
		side := &m.State.Sides[i]
		side.TP = card.Num(int64(m.Rules[i].MinTP))
		side.HP = card.Num(int64(m.Rules[i].MaxHP))

		for _, f := range m.Set.Mode.GetAll(card.FieldSummonCard) {
			sc := f.(*card.SummonCard)

			if sc.Flags&card.SummonCardBothPlayers == 0 && i != 0 {
				continue
			}

			c := m.Set.Card(sc.ID)

			mode := ModeSetupOriginal

			for _, e := range c.Effects {
				if e.Type == card.CondApply && e.Flags&(card.FlagCondApplyNextRound|card.FlagCondApplyOpponent|card.FlagCondApplyShowMask) == card.FlagCondApplyNextRound|card.FlagCondApplyShowOriginalText && e.Result != nil && e.Result.Type == card.EffectSummon && e.Result.Flags&(card.FlagSummonInvisible|card.FlagSummonOpponent|card.FlagSummonReplace) == card.FlagSummonReplace && e.Result.Amount == 1 && e.Result.Filter.IsSingleCard() && e.Result.Filter[0].CardID == sc.ID {
					mode = ModeSummoned

					break
				}
			}

			effects := make([]CardEffect, len(c.Effects))
			for i, e := range c.Effects {
				effects[i].Effect = e
				effects[i].Target = c.ID
			}

			side.Setup = append(side.Setup, &ActiveCard{
				Card: &Card{
					Set:  m.Set,
					Def:  c,
					Back: c.Rank.Back(m.SpecialFlags[card.SpecialEffectBack]),
				},
				Desc:    m.cardDescription(c),
				Effects: effects,
				Mode:    mode,
			})
		}
	}

	for _, f := range m.Set.Mode.GetAll(card.FieldTurn0Effect) {
		t0e := f.(*card.Turn0Effect)

		m.State.Queue = append(m.State.Queue, CardEffect{
			Card: &ActiveCard{
				Mode: ModeSetup,
				Card: &Card{
					Set: m.Set,
					Def: &card.Def{
						ID:   ^card.ID(0),
						Name: "Turn 0 Effect",
						Rank: card.Token,
					},
					Back: card.Token,
				},
			},
			Effect: t0e.Effect,
			Target: ^card.ID(0),
			Player: 0,
		})
	}

	m.Log.Push("Round 0")

	m.State.Sides[0].Limit = make(map[*card.EffectDef]int64)
	m.State.Sides[1].Limit = make(map[*card.EffectDef]int64)

	for {
		ce := m.ProcessQueuedEffect(rng, network)
		if len(ce) == 0 {
			break
		}

		if ce[0].Effect.Type == card.EffectSummon || ce[0].Effect.Type == card.EffectModifyAvailableCards {
			for i := 1; i < len(ce); i++ {
				onSummon(ce[i].Card)
			}
		}

		if ce[0].Effect.Type == card.EffectNumb && ce[0].Effect.Flags&card.FlagNumbSummon != 0 {
			for i := 2; i < len(ce); i += 2 {
				onSummon(ce[i].Card)
			}
		}
	}
}

func (m *Match) ShuffleAndDraw(scg *scnet.SecureCardGame, haveRemoteSeed bool) {
	m.State.TurnData = nil

	for player := range m.State.Sides {
		side := &m.State.Sides[player]

		if haveRemoteSeed || int(m.Perspective-1) == player {
			PrivateShuffle(scg, side.Deck, int(2-m.Perspective) == player)
		} else {
			PublicShuffle(scg, side.Deck)
		}

		for i := 0; (i < int(m.Rules[player].DrawPerRound) || len(side.Hand) < int(m.Rules[player].HandMinSize)) && len(side.Hand) < int(m.Rules[player].HandMaxSize) && len(side.Deck) != 0; i++ {
			c := side.Deck[len(side.Deck)-1]
			side.Deck = side.Deck[:len(side.Deck)-1]
			side.Hand = append(side.Hand, HandCard{Card: c})
		}

		for i := range side.Hand {
			side.Hand[i].InHandTurns++
		}

		if int(m.UnknownDeck)-1 == player {
			for _, c := range side.Discard {
				c.Def = nil
			}
		}

		side.Deck = append(side.Deck, side.Discard...)
		side.Discard = nil

		for i := 0; i < len(side.Setup); i++ {
			if side.Setup[i].Mode != ModeSetupOriginal {
				continue
			}

			for j, ce := range side.Setup[i].Effects {
				e := ce.Effect

				if e.Priority == 0 && e.Type == card.EffectSummon && e.Filter.IsSingleCard() && (e.Filter[0].CardID == side.Setup[i].Card.Def.ID || len(side.Setup[i].Effects) == 1) && e.Amount == 1 && e.Flags&(card.FlagSummonReplace|card.FlagSummonOpponent) == card.FlagSummonReplace {
					if e.Filter[0].CardID != side.Setup[i].Card.Def.ID {
						side.Setup[i].Card.Def = side.Setup[i].Card.Set.Card(e.Filter[0].CardID)
						side.Setup[i].Desc = m.cardDescription(side.Setup[i].Card.Def)
					}

					side.Setup[i].Effects = append(side.Setup[i].Effects[:j], side.Setup[i].Effects[j+1:]...)
					for _, se := range side.Setup[i].Card.Def.Effects {
						side.Setup[i].Effects = append(side.Setup[i].Effects, CardEffect{
							Effect: se,
							Target: side.Setup[i].Card.Def.ID,
						})
					}

					if e.Flags&card.FlagSummonInvisible == 0 {
						if side.Setup[i].Card.Def.Rank == card.Token && side.Setup[i].Card.Def.TP == 0 && !m.Banned[side.Setup[i].Card.Def.ID] {
							side.Setup[i].Mode = ModeStackedToken
						} else {
							side.Setup[i].Mode = ModeSummoned
						}
					} else {
						side.Setup[i].Mode = ModeInvisibleSummoned
					}

					break
				}
			}
		}
	}

	m.RecomputePassives(&card.TurnData{})

	m.State.MaxPriority = 0
}

func (m *Match) RecomputePassives(turnData *card.TurnData) {
	for player := range m.State.Sides {
		side := &m.State.Sides[player]

		for id := range side.ModTP {
			delete(side.ModTP, id)
		}

		side.TP = card.Num(int64((m.State.Round-1)*m.Rules[player].TPPerRound + m.Rules[player].MinTP))
		if side.TP.Amount > int64(m.Rules[player].MaxTP) {
			side.TP.Amount = int64(m.Rules[player].MaxTP)
		}

		limits := make(map[*card.EffectDef]int64)

		for _, setup := range side.Setup {
			for _, ce := range setup.Effects {
				e := ce.Effect

				if e.Type == card.CondLimit {
					limits[e]++

					allow := limits[e] <= e.Amount

					if e.Flags&card.FlagCondLimitGreaterThan != 0 {
						allow = limits[e] > e.Amount
					}

					if allow {
						e = e.Result
					}
				}

				switch e.Type {
				case card.EffectTP:
					num := card.Num(e.Amount)

					if e.Flags&card.FlagTPInfinity != 0 {
						num.Amount, num.AmountInf = 0, num.Amount
					}

					side.TP.Add(num, m.SpecialFlags[card.SpecialSubtractInf])
				case card.EffectModifyCardCost:
					if side.ModTP == nil {
						side.ModTP = make(map[card.ID]card.Number)
					}

					for _, id := range m.FilterCards(e.Filter, setup.Card.Def.ID, true, false) {
						num := card.Num(e.Amount)

						if e.Flags&card.FlagModifyCardCostInfinity != 0 {
							num.Amount, num.AmountInf = 0, num.Amount
						}

						if e.Flags&card.FlagModifyCardCostSet != 0 {
							base := card.Num(m.Set.Card(id).TP)
							base.Negate()
							num.Add(base, m.SpecialFlags[card.SpecialSubtractInf])
						} else {
							num.Add(side.ModTP[id], m.SpecialFlags[card.SpecialSubtractInf])
						}

						side.ModTP[id] = num
					}
				}
			}
		}

		if side.TP.Less(card.Num(0)) {
			side.TP = card.Number{NaN: true}
		}

		for i, inHand := range side.Hand {
			if inHand.Def == nil {
				continue
			}

			for _, e := range inHand.Def.Effects {
				if e.Type != card.CondInHand {
					continue
				}

				if inHand.InHandTurns <= e.Amount {
					if router.FlagInHandDebug.IsSet() {
						m.Log.Logf("Not applying in-hand effect for player %d card %d %q: %d rounds in hand under %d requirement", i+1, int64(inHand.Def.ID), inHand.Def.DisplayName(), inHand.InHandTurns, e.Amount)
					}

					continue
				}

				if e.Amount2 != 0 && inHand.InHandTurns > e.Amount+e.Amount2 {
					if router.FlagInHandDebug.IsSet() {
						m.Log.Logf("Not applying in-hand effect for player %d card %d %q: %d rounds in hand exceeds %d+%d=%d limit", i+1, int64(inHand.Def.ID), inHand.Def.DisplayName(), inHand.InHandTurns, e.Amount, e.Amount2, e.Amount+e.Amount2)
					}

					continue
				}

				switch e.Flags & card.FlagCondInHandOnPlayMask {
				case card.FlagCondInHandOnPlayNone:
					if turnData.Ready[player]&(1<<i) != 0 {
						if router.FlagInHandDebug.IsSet() {
							m.Log.Logf("Not applying in-hand effect for player %d card %d %q: played this round", i+1, int64(inHand.Def.ID), inHand.Def.DisplayName())
						}

						continue
					}
				case card.FlagCondInHandOnPlayOnly:
					if turnData.Ready[player]&(1<<i) == 0 {
						if router.FlagInHandDebug.IsSet() {
							m.Log.Logf("Not applying in-hand effect for player %d card %d %q: not played this round", i+1, int64(inHand.Def.ID), inHand.Def.DisplayName())
						}

						continue
					}
				}

				if !m.Log.Disabled {
					if router.FlagInHandDebug.IsSet() {
						m.Log.Logf("Applying in-hand effect for player %d card %d %q: %v", i+1, int64(inHand.Def.ID), inHand.Def.DisplayName(), (&card.RichDescription{
							Content: e.Result.Description(inHand.Def, m.Set, e),
						}))
					}
				}

				if e.Result.Type == card.CondLimit {
					limits[e]++

					allow := limits[e] <= e.Result.Amount

					if e.Result.Flags&card.FlagCondLimitGreaterThan != 0 {
						allow = limits[e] > e.Result.Amount
					}

					if allow {
						e = e.Result
					}
				}

				switch e.Result.Type {
				case card.EffectTP:
					num := card.Num(e.Result.Amount)

					if e.Result.Flags&card.FlagTPInfinity != 0 {
						num.Amount, num.AmountInf = 0, num.Amount
					}

					side.TP.Add(num, m.SpecialFlags[card.SpecialSubtractInf])
				case card.EffectModifyCardCost:
					if side.ModTP == nil {
						side.ModTP = make(map[card.ID]card.Number)
					}

					for _, id := range m.FilterCards(e.Result.Filter, inHand.Def.ID, true, false) {
						num := card.Num(e.Result.Amount)

						if e.Result.Flags&card.FlagModifyCardCostInfinity != 0 {
							num.Amount, num.AmountInf = 0, num.Amount
						}

						if e.Flags&card.FlagModifyCardCostSet != 0 {
							base := card.Num(m.Set.Card(id).TP)
							base.Negate()
							num.Add(base, m.SpecialFlags[card.SpecialSubtractInf])
						} else {
							num.Add(side.ModTP[id], m.SpecialFlags[card.SpecialSubtractInf])
						}

						side.ModTP[id] = num
					}
				}
			}
		}
	}
}

func (m *Match) BeginTurn(turnData *card.TurnData) error {
	var err error

	m.Log.Pushf("Round %d", m.State.Round)

	m.RecomputePassives(turnData)

	m.State.Sides[0].Field = nil
	m.State.Sides[1].Field = nil

	for player := range m.State.Sides {
		if perRound := int(m.Rules[player].CardsPerRound); perRound != 0 && bits.OnesCount64(turnData.Ready[player]) > perRound {
			err = fmt.Errorf("match: player %d played %d cards but rules limit cards played to %d", player+1, bits.OnesCount64(turnData.Ready[player]), perRound)
		}

		side := &m.State.Sides[player]

		field := &side.Field
		if m.SpecialFlags[card.SpecialBothPlayersAreP1] {
			field = &m.State.Sides[0].Field
		}

		fieldBefore := len(*field)

		*field = append(*field, side.Setup...)
		side.Setup = nil
		side.ATK = card.Num(0)
		side.DEF = card.Num(0)
		side.RawATK = card.Num(0)
		side.RawDEF = card.Num(0)
		side.FinalATK = card.Num(0)
		side.FinalDEF = card.Num(0)
		side.HealP = card.Num(0)
		side.HealN = card.Num(0)
		side.HealMulP = 1
		side.HealMulN = 1
		side.Limit = make(map[*card.EffectDef]int64)

		played := make([]HandCard, 0, len(turnData.Played[player]))

		for i := len(side.Hand) - 1; i >= 0; i-- {
			if side.Hand[i].Def != nil {
				var ac *ActiveCard

				for _, e := range side.Hand[i].Def.Effects {
					if e.Type != card.CondInHand {
						continue
					}

					if side.Hand[i].InHandTurns <= e.Amount {
						continue
					}

					if e.Amount2 != 0 && side.Hand[i].InHandTurns > e.Amount+e.Amount2 {
						continue
					}

					if e.Result.Type == card.CondLimit && e.Result.Result != nil && (e.Result.Result.Type == card.EffectTP || e.Result.Result.Type == card.EffectModifyCardCost) {
						continue
					}

					if e.Result.Type == card.EffectTP || e.Result.Type == card.EffectModifyCardCost {
						continue
					}

					switch e.Flags & card.FlagCondInHandOnPlayMask {
					case card.FlagCondInHandOnPlayNone:
						if turnData.Ready[player]&(1<<i) != 0 {
							continue
						}
					case card.FlagCondInHandOnPlayOnly:
						if turnData.Ready[player]&(1<<i) == 0 {
							continue
						}
					}

					if ac == nil {
						ac = &ActiveCard{
							Card: &Card{
								Def:  side.Hand[i].Def,
								Back: side.Hand[i].Back,
								Set:  side.Hand[i].Set,
							},
							Mode: ModeInHand,
							Desc: m.cardDescription(side.Hand[i].Def),
							Effects: []CardEffect{
								{
									Effect: e.Result,
									Target: side.Hand[i].Def.ID,
								},
							},
							CreatedBy: side.Hand[i],
						}

						*field = append(*field, ac)
					} else {
						ac.Effects = append(ac.Effects, CardEffect{
							Effect: e.Result,
							Target: side.Hand[i].Def.ID,
						})
					}
				}
			}

			if turnData.Ready[player]&(1<<i) != 0 {
				played = append(played, side.Hand[i])

				if !side.Hand[i].Temporary {
					side.Discard = append(side.Discard, side.Hand[i].Card)
				}

				side.Hand = append(side.Hand[:i], side.Hand[i+1:]...)
			}
		}

		for i, j := 0, len(played)-1; i < j; i, j = i+1, j-1 {
			played[i], played[j] = played[j], played[i]
		}

		for i, j := 0, len(side.Discard)-1; i < j; i, j = i+1, j-1 {
			side.Discard[i], side.Discard[j] = side.Discard[j], side.Discard[i]
		}

		var tpTotal card.Number

		for _, id := range turnData.Played[player] {
			tpTotal.Add(card.Num(m.Set.Card(id).TP), m.SpecialFlags[card.SpecialSubtractInf])
			tpTotal.Add(side.ModTP[id], m.SpecialFlags[card.SpecialSubtractInf])
		}

		if (side.TP.Less(tpTotal) || side.TP.NaN) && len(turnData.Played[player]) != 0 {
			err = fmt.Errorf("match: player %d played %v TP of cards but only has %v TP", player+1, tpTotal, side.TP)
		}

		tpTotal.Negate()
		side.TP.Add(tpTotal, m.SpecialFlags[card.SpecialSubtractInf])

		for i, id := range turnData.Played[player] {
			c := m.Set.Card(id)

			played[i].Def = c

			effects := make([]CardEffect, len(c.Effects))
			for j, e := range c.Effects {
				effects[j].Effect = e
				effects[j].Target = id
			}

			*field = append(*field, &ActiveCard{
				Card: &Card{
					Set:  m.Set,
					Def:  c,
					Back: c.Rank.Back(m.SpecialFlags[card.SpecialEffectBack]),
				},
				Desc:      m.cardDescription(c),
				Effects:   effects,
				Mode:      ModeDefault,
				CreatedBy: played[i],
			})
		}

		m.Log.Pushf("Player %d plays cards:", player+1)

		playerNum := uint8(player)
		if m.SpecialFlags[card.SpecialBothPlayersAreP1] {
			playerNum = 0
		}

		for _, c := range (*field)[fieldBefore:] {
			m.Log.Logf("%v (%v)", c.Card.Def.DisplayName(), c.Mode)

			for _, ce := range c.Effects {
				m.State.Queue = append(m.State.Queue, CardEffect{
					Card:   c,
					Effect: ce.Effect,
					Target: ce.Target,
					Player: playerNum,
				})
			}
		}

		m.Log.Pop()
	}

	sort.SliceStable(m.State.Queue, func(i, j int) bool {
		if m.State.Queue[i].Effect.Priority == m.State.Queue[j].Effect.Priority {
			return m.State.Queue[i].Player < m.State.Queue[j].Player
		}

		return m.State.Queue[i].Effect.Priority < m.State.Queue[j].Effect.Priority
	})

	m.State.OnNumb = nil
	m.State.MaxPriority = 0
	m.State.RoundWinner = 0

	return err
}

type cachedFilter struct {
	filter     card.Filter
	banned     bool
	unfiltered bool
	cards      []card.ID
}

func (m *Match) FilterCards(f card.Filter, target card.ID, allowBanned, allowUnfiltered bool) []card.ID {
	if f.IsSingleCard() {
		return []card.ID{f[0].CardID}
	}

	if len(f) == 1 && f[0].Type == card.FilterTarget {
		return []card.ID{target}
	}

	for _, cf := range m.filterCache {
		if cf.banned != allowBanned || cf.unfiltered != allowUnfiltered || len(cf.filter) != len(f) {
			continue
		}

		isMatch := true

		for i, fc := range f {
			if fc != cf.filter[i] {
				isMatch = false

				break
			}
		}

		if isMatch {
			return cf.cards
		}
	}

	possible := make(map[*card.Def]bool)

	for i := card.ID(0); i < 128; i++ {
		if m.Banned[i] && !allowBanned {
			continue
		}

		if m.Unfiltered[i] && !allowUnfiltered {
			continue
		}

		if c := m.Set.Card(i); c != nil {
			possible[c] = true
		}
	}

	for _, c := range m.Set.Cards {
		if m.Banned[c.ID] && !allowBanned {
			continue
		}

		if m.Unfiltered[c.ID] && !allowUnfiltered {
			continue
		}

		possible[c] = true
	}

	ranks := make(map[card.Rank]bool)
	tp := make(map[int64]bool)

	for _, fc := range f {
		switch fc.Type {
		case card.FilterRank:
			ranks[fc.Rank] = true
		case card.FilterTribe:
			for c, ok := range possible {
				if !ok {
					continue
				}

				found := false

				for _, t := range c.Tribes {
					if t.Tribe == fc.Tribe && (fc.Tribe != card.TribeCustom || t.CustomName == fc.CustomTribe) {
						found = true

						break
					}
				}

				if !found {
					possible[c] = false
				}
			}
		case card.FilterNotTribe:
			for c, ok := range possible {
				if !ok {
					continue
				}

				for _, t := range c.Tribes {
					if t.Tribe == fc.Tribe && (fc.Tribe != card.TribeCustom || t.CustomName == fc.CustomTribe) {
						possible[c] = false

						break
					}
				}
			}
		case card.FilterTP:
			tp[fc.TP] = true
		}
	}

	if len(ranks) != 0 {
		for c := range possible {
			if !ranks[c.Rank] {
				possible[c] = false
			}
		}
	}

	if len(tp) != 0 {
		for c := range possible {
			if !tp[c.TP] {
				possible[c] = false
			}
		}
	}

	ids := make([]card.ID, 0, len(possible))

	for c, ok := range possible {
		if ok {
			ids = append(ids, c.ID)
		}
	}

	sort.Slice(ids, func(i, j int) bool {
		return ids[i] < ids[j]
	})

	m.filterCache = append(m.filterCache, &cachedFilter{
		filter:     f,
		banned:     allowBanned,
		unfiltered: allowUnfiltered,
		cards:      ids,
	})

	return ids
}

func cardInSet(set []card.ID, id card.ID) bool {
	x := sort.Search(len(set), func(i int) bool {
		return set[i] >= id
	})

	return x < len(set) && set[x] == id
}

func (m *Match) ProcessQueuedEffect(r *rng.RNG, network Network) []CardEffect {
	if len(m.State.OnNumbQueue) == 0 {
		for _, q := range []*[]CardEffect{&m.State.PreQueue, &m.State.Queue} {
			for len(*q) != 0 && (*q)[0].Effect.Type != card.EffectNumb && (*q)[0].Card.Mode == ModeNumb {
				ce := (*q)[0]
				*q = (*q)[1:]

				if ce.Effect.Type != card.CondOnNumb && !m.Log.Disabled {
					m.Log.Logf("Skipping player %d card %q effect %q: card is numb", ce.Player+1, ce.Card.Card.Def.DisplayName(), &card.RichDescription{
						Content: ce.Effect.Description(ce.Card.Card.Def, m.Set),
					})
				}
			}

			if len(*q) != 0 {
				break
			}
		}
	}

	if len(m.State.OnNumbQueue) == 0 && len(m.State.PreQueue) == 0 && len(m.State.Queue) == 0 {
		if m.State.MaxPriority < 224 {
			m.computeRoundWinner()
		}

		m.State.MaxPriority = 255

		m.Log.Pop()

		return nil
	}

	var ce CardEffect

	switch {
	case len(m.State.OnNumbQueue) != 0:
		ce = m.State.OnNumbQueue[0]
		m.State.OnNumbQueue = m.State.OnNumbQueue[1:]
	case len(m.State.PreQueue) != 0:
		ce = m.State.PreQueue[0]
		m.State.PreQueue = m.State.PreQueue[1:]
	default:
		ce = m.State.Queue[0]
		m.State.Queue = m.State.Queue[1:]
	}

	ret := []CardEffect{ce}

	if ce.Effect.Priority > m.State.MaxPriority {
		if m.State.MaxPriority < 224 && ce.Effect.Priority >= 224 {
			m.computeRoundWinner()
		}

		m.State.MaxPriority = ce.Effect.Priority
	}

	return m.applyEffect(ret, ce, r, network)
}

func sameCardEffect(a, b CardEffect) bool {
	if a == b {
		return true
	}

	return a.Effect == b.Effect &&
		a.Player == b.Player &&
		a.Card.Mode == ModeStackedToken &&
		b.Card.Mode == ModeStackedToken &&
		a.Card.Card.Def.ID == b.Card.Card.Def.ID
}

func (m *Match) consumeMultiple(ce CardEffect) int64 {
	multiplier := int64(1)

	for len(m.State.PreQueue) != 0 {
		if !sameCardEffect(m.State.PreQueue[0], ce) {
			return multiplier
		}

		m.State.PreQueue = m.State.PreQueue[1:]
		multiplier++
	}

	for len(m.State.Queue) != 0 {
		if !sameCardEffect(m.State.Queue[0], ce) {
			return multiplier
		}

		m.State.Queue = m.State.Queue[1:]
		multiplier++
	}

	return multiplier
}

func (m *Match) computeRoundWinner() {
	p1ATK := m.State.Sides[0].ATK
	p2ATK := m.State.Sides[1].ATK
	p1DEF := m.State.Sides[0].DEF
	p2DEF := m.State.Sides[1].DEF

	p1ModATK := p1ATK
	p2ModATK := p2ATK
	p1ModDEF := p1DEF
	p2ModDEF := p2DEF

	p1ModDEF.Amount = -p1ModDEF.Amount
	p1ModDEF.AmountInf = -p1ModDEF.AmountInf
	p2ModDEF.Amount = -p2ModDEF.Amount
	p2ModDEF.AmountInf = -p2ModDEF.AmountInf

	if p1ModDEF.AmountInf > 0 || (p1ModDEF.AmountInf == 0 && p1ModDEF.Amount > 0) {
		p1ModDEF.Amount = 0
		p1ModDEF.AmountInf = 0
	}

	if p2ModDEF.AmountInf > 0 || (p2ModDEF.AmountInf == 0 && p2ModDEF.Amount > 0) {
		p2ModDEF.Amount = 0
		p2ModDEF.AmountInf = 0
	}

	p1ModATK.Add(p2ModDEF, m.SpecialFlags[card.SpecialSubtractInf])
	p2ModATK.Add(p1ModDEF, m.SpecialFlags[card.SpecialSubtractInf])

	if p1ModATK.AmountInf < 0 || (p1ModATK.AmountInf == 0 && p1ModATK.Amount < 0) {
		p1ModATK.Amount = 0
		p1ModATK.AmountInf = 0
	}

	if p2ModATK.AmountInf < 0 || (p2ModATK.AmountInf == 0 && p2ModATK.Amount < 0) {
		p2ModATK.Amount = 0
		p2ModATK.AmountInf = 0
	}

	m.State.Sides[0].ATK = p1ModATK
	m.State.Sides[1].ATK = p2ModATK

	switch {
	case p1ATK.NaN:
		m.Log.Log("Player 1 ATK is invalid. Round cannot have a winner.")
		m.State.RoundWinner = 0
	case p2ATK.NaN:
		m.Log.Log("Player 2 ATK is invalid. Round cannot have a winner.")
		m.State.RoundWinner = 0
	case p1DEF.NaN:
		m.Log.Log("Player 1 DEF is invalid. Round cannot have a winner.")
		m.State.RoundWinner = 0
	case p2DEF.NaN:
		m.Log.Log("Player 2 DEF is invalid. Round cannot have a winner.")
		m.State.RoundWinner = 0
	case p1ModATK.AmountInf > 0 && p2ModATK.AmountInf > 0:
		m.Log.Log("Both players have infinite ATK. Round cannot have a winner.")
		m.State.RoundWinner = 0
	case p1ModATK.AmountInf > 0:
		m.Log.Logf("Player 1 has %v ATK. Player 2 has %v ATK. Player 1 wins!", p1ModATK, p2ModATK)
		m.State.RoundWinner = 1
	case p2ModATK.AmountInf > 0:
		m.Log.Logf("Player 1 has %v ATK. Player 2 has %v ATK. Player 2 wins!", p1ModATK, p2ModATK)
		m.State.RoundWinner = 2
	case p1ModATK.Amount > p2ModATK.Amount:
		m.Log.Logf("Player 1 has %v ATK. Player 2 has %v ATK. Player 1 wins!", p1ModATK, p2ModATK)
		m.State.RoundWinner = 1
	case p1ModATK.Amount < p2ModATK.Amount:
		m.Log.Logf("Player 1 has %v ATK. Player 2 has %v ATK. Player 2 wins!", p1ModATK, p2ModATK)
		m.State.RoundWinner = 2
	default:
		m.Log.Logf("Player 1 has %v ATK. Player 2 has %v ATK. Round is a tie.", p1ModATK, p2ModATK)
		m.State.RoundWinner = 0
	}

	if m.State.RoundWinner != 0 && !m.SpecialFlags[card.SpecialNoDamageOnWin] {
		m.State.Sides[2-m.State.RoundWinner].HP.Amount--
	}

	m.Log.Logf("Player 1 HP is %v. Player 2 HP is %v.", m.State.Sides[0].HP, m.State.Sides[1].HP)
}

func (m *Match) queueEffect(ce CardEffect, isSummon bool) {
	if !isSummon && ce.Effect.Priority <= m.State.MaxPriority {
		m.State.PreQueue = append(m.State.PreQueue, ce)
	} else {
		i := sort.Search(len(m.State.Queue), func(i int) bool {
			if ce.Effect.Priority > m.State.MaxPriority && m.State.Queue[i].Effect.Priority == ce.Effect.Priority {
				return m.State.Queue[i].Player > ce.Player
			}

			return m.State.Queue[i].Effect.Priority > ce.Effect.Priority
		})

		m.State.Queue = append(m.State.Queue, CardEffect{})
		copy(m.State.Queue[i+1:], m.State.Queue[i:])
		m.State.Queue[i] = ce
	}
}

func (m *Match) applyEffect(ret []CardEffect, ce CardEffect, r *rng.RNG, network Network) []CardEffect {
	if ce.Effect.Type != card.FlavorText && ce.Effect.Type != card.CondOnNumb && ce.Effect.Type == card.CondOnDiscard && !m.Log.Disabled {
		m.Log.Logf("Applying effect %v for player %d card %q: %v",
			ce.Effect.Type,
			ce.Player+1,
			ce.Card.Card.Def.DisplayName(),
			&card.RichDescription{
				Content: ce.Effect.Description(ce.Card.Card.Def, m.Set),
			},
		)
	}

	switch ce.Effect.Type {
	case card.FlavorText:
		return m.applyEffectFlavorText(ret, ce, r, network)

	case card.EffectStat:
		return m.applyEffectStat(ret, ce, r, network)

	case card.EffectEmpower:
		return m.applyEffectEmpower(ret, ce, r, network)

	case card.EffectSummon:
		return m.applyEffectSummon(ret, ce, r, network)

	case card.EffectHeal:
		return m.applyEffectHeal(ret, ce, r, network)

	case card.EffectTP:
		return m.applyEffectTP(ret, ce, r, network)

	case card.EffectNumb:
		return m.applyEffectNumb(ret, ce, r, network)

	case card.EffectRawStat:
		return m.applyEffectRawStat(ret, ce, r, network)

	case card.EffectMultiplyHealing:
		return m.applyEffectMultiplyHealing(ret, ce, r, network)

	case card.EffectPreventNumb:
		return m.applyEffectPreventNumb(ret, ce, r, network)

	case card.EffectModifyAvailableCards:
		return m.applyEffectModifyAvailableCards(ret, ce, r, network)

	case card.EffectModifyCardCost:
		return m.applyEffectModifyCardCost(ret, ce, r, network)

	case card.EffectDrawCard:
		return m.applyEffectDrawCard(ret, ce, r, network)

	case card.EffectModifyGameRule:
		return m.applyEffectModifyGameRule(ret, ce, r, network)

	case card.EffectDelaySetup:
		return m.applyEffectDelaySetup(ret, ce, r, network)

	case card.CondCard:
		return m.applyCondCard(ret, ce, r, network)

	case card.CondLimit:
		return m.applyCondLimit(ret, ce, r, network)

	case card.CondWinner:
		return m.applyCondWinner(ret, ce, r, network)

	case card.CondApply:
		return m.applyCondApply(ret, ce, r, network)

	case card.CondCoin:
		return m.applyCondCoin(ret, ce, r, network)

	case card.CondStat:
		return m.applyCondStat(ret, ce, r, network)

	case card.CondInHand:
		return m.applyCondInHand(ret, ce, r, network)

	case card.CondLastEffect:
		return m.applyCondLastEffect(ret, ce, r, network)

	case card.CondOnNumb:
		return m.applyCondOnNumb(ret, ce, r, network)

	case card.CondMultipleEffects:
		return m.applyCondMultipleEffects(ret, ce, r, network)

	case card.CondOnDiscard:
		return m.applyCondOnDiscard(ret, ce, r, network)

	case card.CondCompareTargetCard:
		return m.applyCondCompareTargetCard(ret, ce, r, network)

	case card.CondOnExile:
		return m.applyCondOnExile(ret, ce, r, network)

	default:
		desc := &card.RichDescription{Content: ce.Effect.Description(ce.Card.Card.Def, m.Set)}
		log.Panicf("TODO: process queued effect for player %d card %q: %+v %q", ce.Player+1, ce.Card.Card.Def.DisplayName(), *ce.Effect, desc)

		return ret
	}
}

func (m *Match) applyEffectFlavorText(ret []CardEffect, ce CardEffect, r *rng.RNG, network Network) []CardEffect {
	// no effect
	return ret
}

func (m *Match) applyEffectStat(ret []CardEffect, ce CardEffect, r *rng.RNG, network Network) []CardEffect {
	num := card.Num(ce.Effect.Amount)
	if ce.Effect.Flags&card.FlagStatInfinity != 0 {
		num.Amount, num.AmountInf = 0, num.Amount
	}

	if ce.Effect.Flags&card.FlagStatDEF == 0 {
		before := ce.Card.ATK
		ce.Card.ATK.Add(num, m.SpecialFlags[card.SpecialSubtractInf])
		m.Log.Logf("Card ATK changed from %v to %v", before, ce.Card.ATK)

		before = m.State.Sides[ce.Player].ATK
		m.State.Sides[ce.Player].ATK.Add(num, m.SpecialFlags[card.SpecialSubtractInf])

		if m.State.MaxPriority < 224 {
			m.State.Sides[ce.Player].FinalATK = m.State.Sides[ce.Player].ATK
		}

		m.Log.Logf("Player ATK changed from %v to %v", before, m.State.Sides[ce.Player].ATK)
	} else {
		before := ce.Card.DEF
		ce.Card.DEF.Add(num, m.SpecialFlags[card.SpecialSubtractInf])
		m.Log.Logf("Card DEF changed from %v to %v", before, ce.Card.DEF)

		before = m.State.Sides[ce.Player].DEF
		m.State.Sides[ce.Player].DEF.Add(num, m.SpecialFlags[card.SpecialSubtractInf])
		if m.State.MaxPriority < 224 {
			m.State.Sides[ce.Player].FinalDEF = m.State.Sides[ce.Player].DEF
		}
		m.Log.Logf("Player DEF changed from %v to %v", before, m.State.Sides[ce.Player].DEF)
	}

	return ret
}

func (m *Match) applyEffectEmpower(ret []CardEffect, ce CardEffect, r *rng.RNG, network Network) []CardEffect {
	matches := m.FilterCards(ce.Effect.Filter, ce.Target, true, false)
	num := card.Num(ce.Effect.Amount)

	player := ce.Player
	if ce.Effect.Flags&card.FlagStatOpponent != 0 {
		player = 1 - player
	}

	if ce.Effect.Flags&card.FlagStatInfinity != 0 {
		num.Amount, num.AmountInf = 0, num.Amount
	}

	tooMany := 0

	for _, c := range m.State.Sides[player].Field {
		if c.Mode.Ignore() {
			continue
		}

		if cardInSet(matches, c.Card.Def.ID) {
			tooMany++

			if ce.Effect.Flags&card.FlagStatDEF == 0 {
				before := c.ATK
				c.ATK.Add(num, m.SpecialFlags[card.SpecialSubtractInf])

				if tooMany < 100 {
					m.Log.Logf("Matching card %q ATK changed from %v to %v.", c.Card.Def.DisplayName(), before, c.ATK)
				}

				before = m.State.Sides[player].ATK
				m.State.Sides[player].ATK.Add(num, m.SpecialFlags[card.SpecialSubtractInf])

				if m.State.MaxPriority < 224 {
					m.State.Sides[player].FinalATK = m.State.Sides[player].ATK
				}

				if tooMany < 100 {
					m.Log.Logf("Player %d ATK changed from %v to %v.", player+1, before, m.State.Sides[player].ATK)
				}
			} else {
				before := c.DEF
				c.DEF.Add(num, m.SpecialFlags[card.SpecialSubtractInf])

				if tooMany < 100 {
					m.Log.Logf("Matching card %q DEF changed from %v to %v.", c.Card.Def.DisplayName(), before, c.DEF)
				}

				before = m.State.Sides[player].DEF
				m.State.Sides[player].DEF.Add(num, m.SpecialFlags[card.SpecialSubtractInf])
				if m.State.MaxPriority < 224 {
					m.State.Sides[player].FinalDEF = m.State.Sides[player].DEF
				}

				if tooMany < 100 {
					m.Log.Logf("Player %d DEF changed from %v to %v.", player+1, before, m.State.Sides[player].DEF)
				}
			}
		}
	}

	if tooMany >= 100 {
		m.Log.Logf("%d additional matching cards not shown.", tooMany-99)

		if ce.Effect.Flags&card.FlagStatDEF == 0 {
			m.Log.Logf("Player %d ATK changed to %v.", player+1, m.State.Sides[player].ATK)
		} else {
			m.Log.Logf("Player %d DEF changed to %v.", player+1, m.State.Sides[player].DEF)
		}
	}

	return ret
}

func (m *Match) applyEffectSummon(ret []CardEffect, ce CardEffect, r *rng.RNG, network Network) []CardEffect {
	allowBanned := false
	if m.Init.Version[0] == 0 && m.Init.Version[1] == 2 && m.Init.Version[2] < 18 {
		allowBanned = true
	}

	choices := m.FilterCards(ce.Effect.Filter, ce.Target, allowBanned, true)
	if len(choices) == 0 {
		m.Log.Log("no cards available to summon")

		return ret
	}

	selected := choices[0]

	mode := ModeSummoned
	if ce.Effect.Filter.IsSingleCard() && m.Set.Card(selected).Rank == card.Token && m.Set.Card(selected).TP == 0 && !m.Banned[selected] {
		mode = ModeStackedToken
	}

	if !ce.Effect.Filter.IsSingleCard() {
		anyTribe := false
		for _, fc := range ce.Effect.Filter {
			anyTribe = fc.Type == card.FilterTribe || fc.Type == card.FilterNotTribe
			if anyTribe {
				break
			}
		}

		sort.Slice(choices, func(i, j int) bool {
			return m.OrderForSummon[choices[i]] < m.OrderForSummon[choices[j]]
		})

		if ce.Effect.Flags&card.FlagSummonReplace != 0 && !anyTribe {
			for i := 0; i < len(choices); i++ {
				if m.HasReplaceSummon[choices[i]] {
					choices = append(choices[:i], choices[i+1:]...)
					i--
				}
			}
		}
	}

	multiplier := m.consumeMultiple(ce)
	for count := int64(0); count < ce.Effect.Amount*multiplier; count++ {
		if !ce.Effect.Filter.IsSingleCard() {
			i := r.RangeInt(0, len(choices))
			selected = choices[i]

			m.Log.Logf("selecting card %d of %d (%q)", i+1, len(choices), m.Set.Card(selected).DisplayName())
		}

		after := ce.Card
		player := ce.Player

		if ce.Effect.Flags&card.FlagSummonOpponent != 0 {
			after = nil
			player = 1 - player
		}

		selectedCard := m.Set.Card(selected)

		effects := make([]CardEffect, len(selectedCard.Effects))
		for i, e := range selectedCard.Effects {
			effects[i].Effect = e
			effects[i].Target = selected
		}

		toSummon := &ActiveCard{
			Card: &Card{
				Set:  m.Set,
				Def:  selectedCard,
				Back: selectedCard.Rank.Back(m.SpecialFlags[card.SpecialEffectBack]),
			},
			Effects:   effects,
			Desc:      m.cardDescription(selectedCard),
			Mode:      mode,
			CreatedBy: ce.Card,
		}

		ret = append(ret, CardEffect{
			Card:   toSummon,
			Player: player,
		})

		if ce.Effect.Flags&card.FlagSummonInvisible != 0 {
			toSummon.Mode = ModeInvisibleSummoned
		}

		switch replace := ce.Effect.Flags&card.FlagSummonReplace != 0; {
		case replace && after == nil:
			for i, c := range m.State.Sides[ce.Player].Field {
				if c == ce.Card {
					m.State.Sides[ce.Player].Field = append(m.State.Sides[ce.Player].Field[:i], m.State.Sides[ce.Player].Field[i+1:]...)

					break
				}
			}

			m.State.Sides[player].Field = append(m.State.Sides[player].Field, toSummon)
		case after != nil:
			found := false

			for i, c := range m.State.Sides[player].Field {
				if c == after {
					found = true

					if replace {
						m.State.Sides[player].Field[i] = toSummon
					} else {
						m.State.Sides[player].Field = append(m.State.Sides[player].Field[:i+1], append([]*ActiveCard{toSummon}, m.State.Sides[player].Field[i+1:]...)...)
					}

					break
				}
			}

			if !found {
				m.State.Sides[player].Field = append(m.State.Sides[player].Field, toSummon)
			}
		default:
			m.State.Sides[player].Field = append(m.State.Sides[player].Field, toSummon)
		}

		for _, ce := range toSummon.Effects {
			m.queueEffect(CardEffect{
				Card:   toSummon,
				Effect: ce.Effect,
				Target: ce.Target,
				Player: player,
			}, true)
		}
	}

	return ret
}

func (m *Match) applyEffectHeal(ret []CardEffect, ce CardEffect, r *rng.RNG, network Network) []CardEffect {
	player := ce.Player
	if ce.Effect.Flags&card.FlagHealOpponent != 0 {
		player = 1 - player
	}

	num := card.Num(ce.Effect.Amount)

	if ce.Effect.Flags&card.FlagHealRaw == 0 {
		if num.Amount < 0 {
			num.Amount *= m.State.Sides[player].HealMulN
		} else {
			num.Amount *= m.State.Sides[player].HealMulP
		}
	}

	if ce.Effect.Flags&card.FlagHealInfinity != 0 {
		if num.Amount > 0 {
			num.Amount, num.AmountInf = 0, 1
		} else if num.Amount < 0 {
			num.Amount, num.AmountInf = 0, -1
		}
	}

	if ce.Effect.Flags&card.FlagHealRaw == 0 {
		m.Log.Logf("Amount after multipliers: %v", num)

		if ce.Effect.Amount < 0 {
			m.State.Sides[player].HealN.Add(num, m.SpecialFlags[card.SpecialSubtractInf])
		} else {
			m.State.Sides[player].HealP.Add(num, m.SpecialFlags[card.SpecialSubtractInf])
		}
	} else {
		m.Log.Log("Ignoring multipliers.")
	}

	m.State.Sides[player].HP.Add(num, m.SpecialFlags[card.SpecialSubtractInf])
	m.Log.Logf("Player %d HP is now %v.", player+1, m.State.Sides[player].HP)

	if m.State.Sides[player].HP.AmountInf > 0 || m.State.Sides[player].HP.Amount > int64(m.Rules[player].MaxHP) {
		m.State.Sides[player].HP = card.Num(int64(m.Rules[player].MaxHP))
		m.Log.Log("Limiting to maximum HP.")
	}

	return ret
}

func (m *Match) applyEffectTP(ret []CardEffect, ce CardEffect, r *rng.RNG, network Network) []CardEffect {
	// no effect during round
	return ret
}

func (m *Match) applyEffectNumb(ret []CardEffect, ce CardEffect, r *rng.RNG, network Network) []CardEffect {
	player := 1 - ce.Player
	if ce.Effect.Flags&card.FlagNumbSelf != 0 {
		player = 1 - player
	}

	matches := m.FilterCards(ce.Effect.Filter, ce.Target, true, false)
	candidates := make([]*ActiveCard, 0, len(m.State.Sides[player].Field))

	for _, c := range m.State.Sides[player].Field {
		if c.Mode.Ignore() {
			continue
		}

		if cardInSet(matches, c.Card.Def.ID) {
			if c.UnNumb > 0 && ce.Effect.Flags&card.FlagNumbIgnorePrevent == 0 {
				c.UnNumb--
			} else if !c.ATK.NaN && !c.DEF.NaN {
				candidates = append(candidates, c)
			}
		}
	}

	sort.SliceStable(candidates, func(i, j int) bool {
		a1 := candidates[i].ATK
		d1 := candidates[i].DEF
		a2 := candidates[j].ATK
		d2 := candidates[j].DEF

		h1, h2 := a1, a2
		l1, l2 := d1, d2
		switch {
		case ce.Effect.Flags&card.FlagNumbIgnoreATK != 0:
			h1, h2 = d1, d2
		case ce.Effect.Flags&card.FlagNumbIgnoreDEF != 0:
			l1, l2 = a1, a2
		default:
			if h1.Less(l1) {
				h1, l1 = l1, h1
			}

			if h2.Less(l2) {
				h2, l2 = l2, h2
			}
		}

		if m.Init.Version[0] == 0 && m.Init.Version[1] == 2 && m.Init.Version[2] < 77 {
			d1, d2 = card.Num(0), card.Num(0)
			l1, l2 = card.Num(0), card.Num(0)
		}

		if ce.Effect.Flags&card.FlagNumbAvoidZero != 0 {
			if h1.AmountInf == 0 && h1.Amount == 0 {
				return false
			}

			if h2.AmountInf == 0 && h2.Amount == 0 {
				return true
			}
		}

		if ce.Effect.Flags&card.FlagNumbHighest != 0 {
			a1, a2 = a2, a1
			d1, d2 = d2, d1
			h1, h2 = h2, h1
			l1, l2 = l2, l1
		}

		if h1.Less(h2) {
			return true
		}

		if h2.Less(h1) {
			return false
		}

		if l1.Less(l2) {
			return true
		}

		if l2.Less(l1) {
			return false
		}

		if h1 != a1 && h2 == a2 {
			return true
		}

		if h1 == a1 && h2 == a2 {
			return d1.Less(d2)
		}

		return false
	})

	if ce.Effect.Flags&card.FlagNumbInfinity == 0 && int64(len(candidates)) > ce.Effect.Amount {
		candidates = candidates[:ce.Effect.Amount]
	}

	for _, c := range candidates {
		m.Log.Logf("Numbing card %q (ATK %v DEF %v)", c.Card.Def.DisplayName(), c.ATK, c.DEF)

		if ce.Effect.Flags&card.FlagNumbResult != 0 {
			m.queueEffect(CardEffect{
				Card:   ce.Card,
				Effect: ce.Effect.Result,
				Target: c.Card.Def.ID,
				Player: ce.Player,
			}, false)
		}

		c.Mode = ModeNumb

		ret = append(ret, CardEffect{
			Card:   c,
			Player: player,
		})

		for _, q := range [][]CardEffect{m.State.OnNumb, m.State.PreQueue, m.State.Queue} {
			for _, o := range q {
				if o.Card == c && o.Effect.Type == card.CondOnNumb {
					m.Log.Logf("Queueing On Numb effect for player %d card %q.", player+1, c.Card.Def.DisplayName())

					m.State.OnNumbQueue = append(m.State.OnNumbQueue, CardEffect{
						Card:   c,
						Effect: o.Effect.Result,
						Target: ce.Card.Card.Def.ID,
						Player: o.Player,
					})
				}
			}
		}

		if ce.Effect.Flags&card.FlagNumbSummon != 0 {
			effects := make([]CardEffect, len(c.Card.Def.Effects))
			for i, e := range c.Card.Def.Effects {
				effects[i].Effect = e
				effects[i].Target = c.Card.Def.ID
			}

			toSummon := &ActiveCard{
				Card: &Card{
					Set:  m.Set,
					Def:  c.Card.Def,
					Back: c.Card.Def.Rank.Back(m.SpecialFlags[card.SpecialEffectBack]),
				},
				Effects:   effects,
				Desc:      m.cardDescription(c.Card.Def),
				Mode:      ModeSummoned,
				UnNumb:    1e15, // a trillion should be enough
				CreatedBy: ce.Card,
			}

			ret = append(ret, CardEffect{
				Card:   toSummon,
				Player: 1 - player,
			})

			m.State.Sides[1-player].Field = append(m.State.Sides[1-player].Field, toSummon)

			for _, ce := range toSummon.Effects {
				m.queueEffect(CardEffect{
					Card:   toSummon,
					Effect: ce.Effect,
					Target: ce.Target,
					Player: 1 - player,
				}, true)
			}
		}
	}

	if ce.Effect.Flags&card.FlagNumbResult2 != 0 {
		for i := int64(len(candidates)); i < ce.Effect.Amount; i++ {
			m.queueEffect(CardEffect{
				Card:   ce.Card,
				Effect: ce.Effect.Result2,
				Target: ce.Target,
				Player: ce.Player,
			}, false)
		}
	}

	atkBefore := m.State.Sides[player].ATK
	defBefore := m.State.Sides[player].DEF

	m.State.Sides[player].ATK = m.State.Sides[player].RawATK
	m.State.Sides[player].DEF = m.State.Sides[player].RawDEF

	for _, c := range m.State.Sides[player].Field {
		if c.Mode == ModeNumb {
			continue
		}

		m.State.Sides[player].ATK.Add(c.ATK, m.SpecialFlags[card.SpecialSubtractInf])
		m.State.Sides[player].DEF.Add(c.DEF, m.SpecialFlags[card.SpecialSubtractInf])
	}

	if m.State.MaxPriority < 224 {
		m.State.Sides[player].FinalATK = m.State.Sides[player].ATK
		m.State.Sides[player].FinalDEF = m.State.Sides[player].DEF
	}

	m.Log.Logf("Player %d ATK recalculated from %v to %v.", player+1, atkBefore, m.State.Sides[player].ATK)
	m.Log.Logf("Player %d DEF recalculated from %v to %v.", player+1, defBefore, m.State.Sides[player].DEF)

	return ret
}

func (m *Match) applyEffectRawStat(ret []CardEffect, ce CardEffect, r *rng.RNG, network Network) []CardEffect {
	num := card.Num(ce.Effect.Amount)
	if ce.Effect.Flags&card.FlagStatInfinity != 0 {
		num.Amount, num.AmountInf = 0, num.Amount
	}

	player := ce.Player
	if ce.Effect.Flags&card.FlagStatOpponent != 0 {
		player = 1 - player
	}

	if ce.Effect.Flags&card.FlagStatDEF == 0 {
		before := m.State.Sides[player].ATK
		m.State.Sides[player].RawATK.Add(num, m.SpecialFlags[card.SpecialSubtractInf])
		m.State.Sides[player].ATK.Add(num, m.SpecialFlags[card.SpecialSubtractInf])

		if m.State.MaxPriority < 224 {
			m.State.Sides[player].FinalATK = m.State.Sides[player].ATK
		}

		m.Log.Logf("Player %d ATK changed from %v to %v.", player+1, before, m.State.Sides[ce.Player].ATK)
	} else {
		before := m.State.Sides[player].DEF
		m.State.Sides[player].RawDEF.Add(num, m.SpecialFlags[card.SpecialSubtractInf])
		m.State.Sides[player].DEF.Add(num, m.SpecialFlags[card.SpecialSubtractInf])
		if m.State.MaxPriority < 224 {
			m.State.Sides[player].FinalDEF = m.State.Sides[player].DEF
		}
		m.Log.Logf("Player %d DEF changed from %v to %v.", player+1, before, m.State.Sides[player].DEF)
	}

	return ret
}

func (m *Match) applyEffectMultiplyHealing(ret []CardEffect, ce CardEffect, r *rng.RNG, network Network) []CardEffect {
	player := ce.Player
	if ce.Effect.Flags&card.FlagMultiplyHealingOpponent != 0 {
		player = 1 - player
	}

	pos := ce.Effect.Flags&card.FlagMultiplyHealingTypeMask != card.FlagMultiplyHealingTypeNegative
	neg := ce.Effect.Flags&card.FlagMultiplyHealingTypeMask != card.FlagMultiplyHealingTypePositive

	if pos {
		m.State.Sides[player].HealMulP *= ce.Effect.Amount
	}

	if neg {
		m.State.Sides[player].HealMulN *= ce.Effect.Amount
	}

	if ce.Effect.Flags&card.FlagMultiplyHealingFuture == 0 {
		if pos {
			if ce.Effect.Amount < 0 {
				m.State.Sides[player].HealP.AmountInf = -m.State.Sides[player].HealP.AmountInf
			}

			add := m.State.Sides[player].HealP.Amount * (ce.Effect.Amount - 1)
			m.State.Sides[player].HealP.Amount += add
			m.State.Sides[player].HP.Amount += add
		}

		if neg {
			if ce.Effect.Amount < 0 {
				m.State.Sides[player].HealN.AmountInf = -m.State.Sides[player].HealN.AmountInf
			}

			add := m.State.Sides[player].HealN.Amount * (ce.Effect.Amount - 1)
			m.State.Sides[player].HealN.Amount += add
			m.State.Sides[player].HP.Amount += add
		}

		if m.State.Sides[player].HP.AmountInf > 0 || (m.State.Sides[player].HP.AmountInf == 0 && m.State.Sides[player].HP.Amount > int64(m.Rules[player].MaxHP)) {
			m.State.Sides[player].HP.Amount, m.State.Sides[player].HP.AmountInf = int64(m.Rules[player].MaxHP), 0
		}
	}

	return ret
}

func (m *Match) applyEffectPreventNumb(ret []CardEffect, ce CardEffect, r *rng.RNG, network Network) []CardEffect {
	player := ce.Player
	if ce.Effect.Flags&card.FlagPreventNumbOpponent != 0 {
		player = 1 - player
	}

	matches := m.FilterCards(ce.Effect.Filter, ce.Target, true, false)

	for _, c := range m.State.Sides[player].Field {
		if c.Mode.Ignore() {
			continue
		}

		if cardInSet(matches, c.Card.Def.ID) {
			c.UnNumb += ce.Effect.Amount
		}
	}

	return ret
}

func (m *Match) applyEffectModifyAvailableCards(ret []CardEffect, ce CardEffect, r *rng.RNG, network Network) []CardEffect {
	player := ce.Player
	if ce.Effect.Flags&card.FlagModifyCardsOpponent != 0 {
		player = 1 - player
	}

	var success []card.ID

	if ce.Effect.Flags&card.FlagModifyCardsRequireMask != card.FlagModifyCardsRequireNone && ce.Effect.Flags&card.FlagModifyCardsTypeMask == card.FlagModifyCardsTypeRemove {
		// special case: return from exile
		var exile map[string][]*Card

		switch ce.Effect.Flags & card.FlagModifyCardsRequireMask {
		case card.FlagModifyCardsRequireSelf:
			exile = m.State.Sides[ce.Player].Exile
		case card.FlagModifyCardsRequireOpponent:
			exile = m.State.Sides[1-ce.Player].Exile
		}

		matches := m.FilterCards(ce.Effect.Filter, ce.Target, true, false)

		for i := int64(0); i < ce.Effect.Amount; i++ {
			var match *Card

			for j := len(exile[ce.Effect.Text]) - 1; j >= 0; j-- {
				for _, id := range matches {
					if id == exile[ce.Effect.Text][j].Def.ID {
						match = exile[ce.Effect.Text][j]
						exile[ce.Effect.Text] = append(exile[ce.Effect.Text][:j], exile[ce.Effect.Text][j+1:]...)

						break
					}
				}

				if match != nil {
					break
				}
			}

			if match == nil {
				break
			}

			c := match.Def

			success = append(success, c.ID)

			if m.UnknownDeck-1 == player {
				match.Def = nil
			}

			switch ce.Effect.Flags & card.FlagModifyCardsTargetMask {
			case card.FlagModifyCardsTargetAny:
				m.State.Sides[player].Hand = append(m.State.Sides[player].Hand, HandCard{
					Card:      match,
					Temporary: true,
				})
			case card.FlagModifyCardsTargetHand:
				m.State.Sides[player].Hand = append(m.State.Sides[player].Hand, HandCard{Card: match})
			case card.FlagModifyCardsTargetDeck:
				m.State.Sides[player].Deck = append(m.State.Sides[player].Deck, match)
			case card.FlagModifyCardsTargetField:
				effects := make([]CardEffect, len(c.Effects))
				for i, e := range c.Effects {
					effects[i].Effect = e
					effects[i].Target = c.ID
				}

				ac := &ActiveCard{
					Card: &Card{
						Def:  c,
						Back: c.Rank.Back(m.SpecialFlags[card.SpecialEffectBack]),
						Set:  m.Set,
					},
					Desc:      m.cardDescription(c),
					Mode:      ModeSummoned,
					Effects:   effects,
					CreatedBy: ce.Card,
				}
				m.State.Sides[player].Field = append(m.State.Sides[player].Field, ac)

				for _, ce := range ac.Effects {
					m.queueEffect(CardEffect{
						Card:   ac,
						Effect: ce.Effect,
						Target: ce.Target,
						Player: player,
					}, false)
				}

				m.State.Sides[player].Discard = append(m.State.Sides[player].Discard, match)
			}
		}
	} else {
		isAdd := ce.Effect.Flags&card.FlagModifyCardsTypeMask == card.FlagModifyCardsTypeAdd

		matches := m.FilterCards(ce.Effect.Filter, ce.Target, !isAdd, isAdd)

		switch ce.Effect.Flags & card.FlagModifyCardsRequireMask {
		case card.FlagModifyCardsRequireNone:
			// no requirement
		case card.FlagModifyCardsRequireSelf:
			filtered := make([]card.ID, 0, len(matches))

			for _, id := range matches {
				for _, c := range m.State.Sides[ce.Player].Field {
					if !c.Mode.Ignore() && c.Card.Def.ID == id {
						filtered = append(filtered, id)

						break
					}
				}
			}

			matches = filtered
		case card.FlagModifyCardsRequireOpponent:
			filtered := make([]card.ID, 0, len(matches))

			for _, id := range matches {
				for _, c := range m.State.Sides[1-ce.Player].Field {
					if !c.Mode.Ignore() && c.Card.Def.ID == id {
						filtered = append(filtered, id)

						break
					}
				}
			}

			matches = filtered
		}

		if len(matches) == 0 {
			return ret
		}

		switch ce.Effect.Flags & card.FlagModifyCardsTypeMask {
		case card.FlagModifyCardsTypeAdd:
			for i := int64(0); i < ce.Effect.Amount; i++ {
				var temp bool

				switch ce.Effect.Flags & card.FlagModifyCardsTargetMask {
				case card.FlagModifyCardsTargetAny:
					temp = true

					fallthrough
				case card.FlagModifyCardsTargetHand:
					var c *card.Def

					if ce.Effect.Filter.IsSingleCard() {
						c = m.Set.Card(ce.Effect.Filter[0].CardID)
					} else {
						j := r.RangeInt(0, len(matches))
						c = m.Set.Card(matches[j])
					}

					success = append(success, c.ID)
					m.State.Sides[player].Hand = append(m.State.Sides[player].Hand, HandCard{
						Card: &Card{
							Def:  c,
							Back: c.Rank.Back(m.SpecialFlags[card.SpecialEffectBack]),
							Set:  m.Set,
						},
						Temporary: temp,
					})
				case card.FlagModifyCardsTargetDeck:
					var c *card.Def

					if ce.Effect.Filter.IsSingleCard() {
						c = m.Set.Card(ce.Effect.Filter[0].CardID)
					} else {
						j := r.RangeInt(0, len(matches))
						c = m.Set.Card(matches[j])
					}

					def := c

					if m.UnknownDeck-1 == player {
						def = nil
					}

					success = append(success, c.ID)
					m.State.Sides[player].Deck = append(m.State.Sides[player].Deck, &Card{
						Def:  def,
						Back: c.Rank.Back(m.SpecialFlags[card.SpecialEffectBack]),
						Set:  m.Set,
					})
				case card.FlagModifyCardsTargetField:
					var c *card.Def

					if ce.Effect.Filter.IsSingleCard() {
						c = m.Set.Card(ce.Effect.Filter[0].CardID)
					} else {
						j := r.RangeInt(0, len(matches))
						c = m.Set.Card(matches[j])
					}

					def := c

					if m.UnknownDeck-1 == player {
						def = nil
					}

					effects := make([]CardEffect, len(c.Effects))
					for i, e := range c.Effects {
						effects[i].Effect = e
						effects[i].Target = c.ID
					}

					success = append(success, c.ID)
					ac := &ActiveCard{
						Card: &Card{
							Def:  c,
							Back: c.Rank.Back(m.SpecialFlags[card.SpecialEffectBack]),
							Set:  m.Set,
						},
						Desc:      m.cardDescription(c),
						Mode:      ModeSummoned,
						Effects:   effects,
						CreatedBy: ce.Card,
					}
					m.State.Sides[player].Field = append(m.State.Sides[player].Field, ac)

					ret = append(ret, CardEffect{
						Card:   ac,
						Effect: nil,
						Player: player,
					})

					for _, ce := range ac.Effects {
						m.queueEffect(CardEffect{
							Card:   ac,
							Effect: ce.Effect,
							Target: ce.Target,
							Player: player,
						}, true)
					}

					m.State.Sides[player].Discard = append(m.State.Sides[player].Discard, &Card{
						Def:  def,
						Back: c.Rank.Back(m.SpecialFlags[card.SpecialEffectBack]),
						Set:  m.Set,
					})
				}
			}

		case card.FlagModifyCardsTypeRemove:
			var modified []card.ModifiedCardPosition

			if m.UnknownDeck-1 == player {
				modified = network.RecvModifiedCards(true)
			} else {
				target := ce.Effect.Flags & card.FlagModifyCardsTargetMask

				if target == card.FlagModifyCardsTargetAny || target == card.FlagModifyCardsTargetField {
					for i, c := range m.State.Sides[player].Discard {
						if int64(len(modified)) >= ce.Effect.Amount {
							break
						}

						if cardInSet(matches, c.Def.ID) {
							modified = append(modified, card.ModifiedCardPosition{
								InHand:   true,
								Position: uint64(len(m.State.Sides[player].Hand) + i),
								CardID:   c.Def.ID,
							})
						}
					}
				}

				if target == card.FlagModifyCardsTargetAny || target == card.FlagModifyCardsTargetHand {
					for i, c := range m.State.Sides[player].Hand {
						if int64(len(modified)) >= ce.Effect.Amount {
							break
						}

						if cardInSet(matches, c.Def.ID) {
							modified = append(modified, card.ModifiedCardPosition{
								InHand:   true,
								Position: uint64(i),
								CardID:   c.Def.ID,
							})
						}
					}
				}

				if target == card.FlagModifyCardsTargetAny || target == card.FlagModifyCardsTargetDeck {
					for i, c := range m.State.Sides[player].Deck {
						if int64(len(modified)) >= ce.Effect.Amount {
							break
						}

						if cardInSet(matches, c.Def.ID) {
							modified = append(modified, card.ModifiedCardPosition{
								InHand:   false,
								Position: uint64(i),
								CardID:   c.Def.ID,
							})
						}
					}
				}

				if 2-m.UnknownDeck == player {
					if err := network.SendModifiedCards(modified, true); err != nil {
						log.Printf("ERROR: failed to send modified cards: %+v", err)
					}
				}
			}

			if m.State.TurnData != nil {
				m.State.TurnData.Modified[player] = append(m.State.TurnData.Modified[player], modified)
			}

			var removedField, removedHand, removedDeck int

			for _, c := range modified {
				var (
					exiled   *Card
					exileKey interface{}
				)

				success = append(success, c.CardID)

				switch handSize := len(m.State.Sides[player].Hand) + removedHand; {
				case c.InHand && c.Position >= uint64(handSize):
					i := int(c.Position) - handSize - removedField

					exiled = m.State.Sides[player].Discard[i]
					exileKey = exiled

					m.State.Sides[player].Discard = append(m.State.Sides[player].Discard[:i], m.State.Sides[player].Discard[i+1:]...)
					removedField++
				case c.InHand:
					i := int(c.Position) - removedHand

					if !m.State.Sides[player].Hand[i].Temporary {
						exiled = m.State.Sides[player].Hand[i].Card
					}

					exileKey = m.State.Sides[player].Hand[i]

					m.State.Sides[player].Hand = append(m.State.Sides[player].Hand[:i], m.State.Sides[player].Hand[i+1:]...)
					removedHand++
				default:
					i := int(c.Position) - removedDeck

					exiled = m.State.Sides[player].Deck[i]
					exileKey = exiled

					m.State.Sides[player].Deck = append(m.State.Sides[player].Deck[:i], m.State.Sides[player].Deck[i+1:]...)
					removedDeck++
				}

				if exileKey != nil {
					exiledCard := exiled
					if exiledCard == nil {
						exiledCard = exileKey.(HandCard).Card
					}

					for _, e := range m.Set.Card(c.CardID).Effects {
						if e.Type == card.CondOnExile && (e.Flags&card.FlagCondOnExileGroup == 0 || e.Text == ce.Effect.Text) {
							var ac *ActiveCard

							for _, fc := range m.State.Sides[player].Field {
								if fc.CreatedBy == exileKey {
									ac = fc

									break
								}

								if hc, ok := fc.CreatedBy.(HandCard); ok && hc.Card == exileKey {
									ac = fc

									break
								}
							}

							if ac == nil {
								ac = &ActiveCard{
									Card:      exiledCard,
									Mode:      ModeInHand,
									Desc:      m.Set.Card(c.CardID).Description(m.Set),
									CreatedBy: exileKey,
								}

								m.State.Sides[player].Field = append(m.State.Sides[player].Field, ac)
							}

							m.queueEffect(CardEffect{
								Card:   ac,
								Effect: e.Result,
								Target: ce.Card.Card.Def.ID,
								Player: player,
							}, false)
						}
					}
				}

				if exiled != nil {
					if exiled.Def == nil {
						exiled.Def = m.Set.Card(c.CardID)
					}

					if m.State.Sides[player].Exile == nil {
						m.State.Sides[player].Exile = make(map[string][]*Card)
					}

					m.State.Sides[player].Exile[ce.Effect.Text] = append(m.State.Sides[player].Exile[ce.Effect.Text], exiled)
				}
			}

		case card.FlagModifyCardsTypeMove:
			var modified []card.ModifiedCardPosition

			if m.UnknownDeck-1 == player {
				modified = network.RecvModifiedCards(true)
			} else {
				target := ce.Effect.Flags & card.FlagModifyCardsTargetMask

				if target == card.FlagModifyCardsTargetAny || target == card.FlagModifyCardsTargetHand {
					for i, c := range m.State.Sides[player].Hand {
						if int64(len(modified)) >= ce.Effect.Amount {
							break
						}

						if cardInSet(matches, c.Def.ID) {
							modified = append(modified, card.ModifiedCardPosition{
								InHand:   true,
								Position: uint64(i),
								CardID:   c.Def.ID,
							})
						}
					}
				}

				if target == card.FlagModifyCardsTargetAny || target == card.FlagModifyCardsTargetDeck {
					for i, c := range m.State.Sides[player].Deck {
						if int64(len(modified)) >= ce.Effect.Amount {
							break
						}

						if cardInSet(matches, c.Def.ID) {
							modified = append(modified, card.ModifiedCardPosition{
								InHand:   false,
								Position: uint64(i),
								CardID:   c.Def.ID,
							})
						}
					}
				}

				if 2-m.UnknownDeck == player {
					if err := network.SendModifiedCards(modified, true); err != nil {
						log.Printf("ERROR: failed to send modified cards: %+v", err)
					}
				}
			}

			if m.State.TurnData != nil {
				m.State.TurnData.Modified[player] = append(m.State.TurnData.Modified[player], modified)
			}

			var movedHand, movedDeck int

			for _, c := range modified {
				if c.InHand {
					i := int(c.Position) - movedHand

					if m.UnknownDeck-1 == player {
						m.State.Sides[player].Hand[i].Def = nil
					}

					m.State.Sides[player].Discard = append(m.State.Sides[player].Discard, m.State.Sides[player].Hand[i].Card)
					m.State.Sides[player].Hand = append(m.State.Sides[player].Hand[:i], m.State.Sides[player].Hand[i+1:]...)
					movedHand++
				} else {
					i := int(c.Position) - movedDeck
					m.State.Sides[player].Discard = append(m.State.Sides[player].Discard, m.State.Sides[player].Deck[i])
					m.State.Sides[player].Deck = append(m.State.Sides[player].Deck[:i], m.State.Sides[player].Deck[i+1:]...)
					movedDeck++
				}

				def := m.Set.Card(c.CardID)

				effects := make([]CardEffect, len(def.Effects))
				for i, e := range def.Effects {
					effects[i].Effect = e
					effects[i].Target = c.CardID
				}

				ac := &ActiveCard{
					Card: &Card{
						Def:  def,
						Back: def.Rank.Back(m.SpecialFlags[card.SpecialEffectBack]),
						Set:  m.Set,
					},
					Desc:      m.cardDescription(def),
					Mode:      ModeSummoned,
					Effects:   effects,
					CreatedBy: ce.Card,
				}

				ret = append(ret, CardEffect{
					Card:   ac,
					Effect: nil,
					Player: player,
				})

				success = append(success, c.CardID)
				m.State.Sides[player].Field = append(m.State.Sides[player].Field, ac)

				for _, ce := range ac.Effects {
					m.queueEffect(CardEffect{
						Card:   ac,
						Effect: ce.Effect,
						Target: ce.Target,
						Player: player,
					}, true)
				}
			}

		case card.FlagModifyCardsTypeReplace:
			var modified []card.ModifiedCardPosition

			if m.UnknownDeck-1 == player {
				modified = network.RecvModifiedCards(true)
			} else {
				target := ce.Effect.Flags & card.FlagModifyCardsTargetMask

				if target == card.FlagModifyCardsTargetAny || target == card.FlagModifyCardsTargetField {
					for i, c := range m.State.Sides[player].Discard {
						if int64(len(modified)) >= ce.Effect.Amount {
							break
						}

						if cardInSet(matches, c.Def.ID) {
							modified = append(modified, card.ModifiedCardPosition{
								InHand:   true,
								Position: uint64(len(m.State.Sides[player].Hand) + i),
								CardID:   c.Def.ID,
							})
						}
					}
				}

				if target == card.FlagModifyCardsTargetAny || target == card.FlagModifyCardsTargetHand {
					for i, c := range m.State.Sides[player].Hand {
						if int64(len(modified)) >= ce.Effect.Amount {
							break
						}

						if cardInSet(matches, c.Def.ID) {
							modified = append(modified, card.ModifiedCardPosition{
								InHand:   true,
								Position: uint64(i),
								CardID:   c.Def.ID,
							})
						}
					}
				}

				if target == card.FlagModifyCardsTargetAny || target == card.FlagModifyCardsTargetDeck {
					for i, c := range m.State.Sides[player].Deck {
						if int64(len(modified)) >= ce.Effect.Amount {
							break
						}

						if cardInSet(matches, c.Def.ID) {
							modified = append(modified, card.ModifiedCardPosition{
								InHand:   false,
								Position: uint64(i),
								CardID:   c.Def.ID,
							})
						}
					}
				}

				if 2-m.UnknownDeck == player {
					if err := network.SendModifiedCards(modified, true); err != nil {
						log.Printf("ERROR: failed to send modified cards: %+v", err)
					}
				}
			}

			if m.State.TurnData != nil {
				m.State.TurnData.Modified[player] = append(m.State.TurnData.Modified[player], modified)
			}

			replacement := m.Set.Card(card.ID(ce.Effect.Amount3))

			for _, c := range modified {
				success = append(success, c.CardID)

				switch handSize := len(m.State.Sides[player].Hand); {
				case c.InHand && c.Position >= uint64(handSize):
					i := int(c.Position) - handSize

					m.State.Sides[player].Discard[i] = &Card{
						Set:  m.Set,
						Def:  replacement,
						Back: replacement.Rank.Back(m.SpecialFlags[card.SpecialEffectBack]),
					}
				case c.InHand:
					i := int(c.Position)

					m.State.Sides[player].Hand[i].Card = &Card{
						Set:  m.Set,
						Def:  replacement,
						Back: replacement.Rank.Back(m.SpecialFlags[card.SpecialEffectBack]),
					}

					if m.UnknownDeck == player+1 {
						m.State.Sides[player].Hand[i].Card.Def = nil
					}
				default:
					i := int(c.Position)

					m.State.Sides[player].Deck[i] = &Card{
						Set:  m.Set,
						Def:  replacement,
						Back: replacement.Rank.Back(m.SpecialFlags[card.SpecialEffectBack]),
					}

					if m.UnknownDeck == player+1 {
						m.State.Sides[player].Deck[i].Def = nil
					}
				}
			}
		}
	}

	if ce.Effect.Flags&card.FlagModifyCardsResult != 0 {
		for _, id := range success {
			m.queueEffect(CardEffect{
				Card:   ce.Card,
				Effect: ce.Effect.Result,
				Target: id,
				Player: ce.Player,
			}, false)
		}
	}

	if ce.Effect.Flags&card.FlagModifyCardsResult2 != 0 {
		for i := int64(len(success)); i < ce.Effect.Amount; i++ {
			m.queueEffect(CardEffect{
				Card:   ce.Card,
				Effect: ce.Effect.Result2,
				Target: ce.Target,
				Player: ce.Player,
			}, false)
		}
	}

	return ret
}

func (m *Match) applyEffectModifyCardCost(ret []CardEffect, ce CardEffect, r *rng.RNG, network Network) []CardEffect {
	// no effect during round
	return ret
}

func (m *Match) applyEffectDrawCard(ret []CardEffect, ce CardEffect, r *rng.RNG, network Network) []CardEffect {
	player := ce.Player
	if ce.Effect.Flags&card.FlagDrawCardOpponent != 0 {
		player = 1 - player
	}

	var candidates [][2]int

	shouldSend, shouldRecv := false, false

	switch {
	case m.UnknownDeck-1 == player:
		shouldRecv = true
	case ce.Effect.Amount < 0:
		for i, c := range m.State.Sides[player].Hand {
			if ce.Effect.Filter.IsMatch(c.Def, ce.Target) {
				candidates = append(candidates, [2]int{i, i})
			}
		}

		shouldSend = 2-m.UnknownDeck == player
	default:
		for i, c := range m.State.Sides[player].Deck {
			if ce.Effect.Filter.IsMatch(c.Def, ce.Target) {
				candidates = append(candidates, [2]int{i, i})
			}
		}

		shouldSend = 2-m.UnknownDeck == player
	}

	drawRNG := r.Clone()

	if m.Init.Version[0] == 0 && (m.Init.Version[1] < 3 || (m.Init.Version[1] == 3 && m.Init.Version[2] < 13)) {
		drawRNG = r
	} else {
		r.RangeInt(0, 2)
	}

	var chosen []card.ModifiedCardPosition
	if shouldRecv {
		chosen = network.RecvModifiedCards(ce.Effect.Amount < 0 || ce.Effect.Result != nil)
	} else {
		count := ce.Effect.Amount
		if count < 0 {
			count = -count
		}
		if numCandidates := int64(len(candidates)); count > numCandidates {
			count = numCandidates
		}
		if space := int64(m.Rules[player].HandMaxSize) - int64(len(m.State.Sides[player].Hand)); ce.Effect.Flags&card.FlagDrawCardIgnoreMax == 0 && ce.Effect.Amount >= 0 && count > space {
			count = space

			if count < 0 {
				count = 0
			}
		}

		chosen = make([]card.ModifiedCardPosition, count)
		for i := range chosen {
			x := drawRNG.RangeInt(0, len(candidates))
			if ce.Effect.Amount < 0 {
				chosen[i] = card.ModifiedCardPosition{
					InHand:   true,
					Position: uint64(candidates[x][0]),
					CardID:   m.State.Sides[player].Hand[candidates[x][1]].Def.ID,
				}
			} else {
				chosen[i] = card.ModifiedCardPosition{
					InHand:   false,
					Position: uint64(candidates[x][0]),
					CardID:   m.State.Sides[player].Deck[candidates[x][1]].Def.ID,
				}
			}

			candidates = append(candidates[:x], candidates[x+1:]...)
			for j := x; j < len(candidates); j++ {
				candidates[j][0]--
			}
		}

		if shouldSend {
			if err := network.SendModifiedCards(chosen, ce.Effect.Amount < 0 || ce.Effect.Result != nil); err != nil {
				log.Printf("ERROR: failed to send modified cards: %+v", err)
			}
		}
	}

	for _, mc := range chosen {
		if mc.InHand {
			if m.UnknownDeck == 0 || router.FlagDebugRevealModified.IsSet() {
				m.Log.Logf("Player %d discards card %d #%d %q", player+1, mc.Position, uint64(mc.CardID), m.Set.Card(mc.CardID).DisplayName())
			}

			c := m.State.Sides[player].Hand[mc.Position]

			def := m.Set.Card(mc.CardID)
			for _, e := range def.Effects {
				if e.Type == card.CondOnDiscard {
					ac := &ActiveCard{
						Card: &Card{
							Set:  m.Set,
							Def:  def,
							Back: def.Rank.Back(m.SpecialFlags[card.SpecialEffectBack]),
						},
						Mode:      ModeInHand,
						CreatedBy: c,
					}

					ret = m.applyEffect(ret, CardEffect{
						Card:   ac,
						Effect: e.Result,
						Target: ce.Card.Card.Def.ID,
						Player: player,
					}, r, network)
				}
			}

			if m.UnknownDeck-1 == player {
				c.Def = nil
			}

			if !c.Temporary {
				m.State.Sides[player].Deck = append(m.State.Sides[player].Deck, c.Card)
			}

			m.State.Sides[player].Hand = append(m.State.Sides[player].Hand[:mc.Position], m.State.Sides[player].Hand[mc.Position+1:]...)

			if ce.Effect.Result != nil {
				m.queueEffect(CardEffect{
					Card:   ce.Card,
					Effect: ce.Effect.Result,
					Target: def.ID,
					Player: ce.Player,
				}, false)
			}
		} else {
			if m.UnknownDeck == 0 || router.FlagDebugRevealModified.IsSet() {
				m.Log.Logf("Player %d draws card %d/%d #%d %q", player+1, mc.Position+1, len(m.State.Sides[player].Deck), uint64(mc.CardID), m.Set.Card(mc.CardID).DisplayName())
			}

			m.State.Sides[player].Hand = append(m.State.Sides[player].Hand, HandCard{Card: m.State.Sides[player].Deck[mc.Position]})
			m.State.Sides[player].Deck = append(m.State.Sides[player].Deck[:mc.Position], m.State.Sides[player].Deck[mc.Position+1:]...)

			if ce.Effect.Result != nil {
				m.queueEffect(CardEffect{
					Card:   ce.Card,
					Effect: ce.Effect.Result,
					Target: mc.CardID,
					Player: ce.Player,
				}, false)
			}
		}
	}

	if ce.Effect.Result2 != nil {
		for i := int64(len(chosen)); i < ce.Effect.Amount; i++ {
			m.queueEffect(CardEffect{
				Card:   ce.Card,
				Effect: ce.Effect.Result2,
				Target: ce.Target,
				Player: ce.Player,
			}, false)
		}
	}

	return ret
}

func (m *Match) applyEffectModifyGameRule(ret []CardEffect, ce CardEffect, r *rng.RNG, network Network) []CardEffect {
	for player := range m.Rules {
		if player == int(ce.Player) && ce.Effect.Flags&card.FlagModifyGameRulePlayerMask == card.FlagModifyGameRulePlayerOpponent {
			continue
		}

		if player == int(1-ce.Player) && ce.Effect.Flags&card.FlagModifyGameRulePlayerMask == card.FlagModifyGameRulePlayerSelf {
			continue
		}

		if ce.Effect.Amount2 <= 0 {
			log.Printf("ERROR: ModifyGameRule rule ID %d out of range", ce.Effect.Amount2)

			continue
		}

		r := reflect.ValueOf(&m.Rules[player]).Elem()
		if int(ce.Effect.Amount2) > r.NumField() {
			log.Printf("ERROR: ModifyGameRule rule ID %d out of range", ce.Effect.Amount2)

			continue
		}

		rf := r.Field(int(ce.Effect.Amount2) - 1)

		amount := ce.Effect.Amount

		if ce.Effect.Flags&card.FlagModifyGameRuleRelative != 0 {
			switch kind := rf.Kind(); kind {
			case reflect.Int64:
				amount += rf.Int()
			case reflect.Uint64:
				amount += int64(rf.Uint())
			default:
				panic(fmt.Errorf("match: unexpected Kind for GameRules field: %v", kind))
			}
		}

		m.Log.Logf("Player %d rule %d modified from %d to %d.", player+1, ce.Effect.Amount2, rf.Interface(), amount)

		if ce.Effect.Amount2 == 3 && amount > 50 {
			// special case: HandMaxSize
			amount = 50

			m.Log.Log("Game rule value clamped to 50.")
		}

		switch kind := rf.Kind(); kind {
		case reflect.Int64:
			rf.SetInt(amount)
		case reflect.Uint64:
			if amount < 0 {
				amount = 0

				m.Log.Log("Game rule value clamped to 0.")
			}

			rf.SetUint(uint64(amount))
		default:
			panic(fmt.Errorf("match: unexpected Kind for GameRules field: %v", kind))
		}
	}

	return ret
}

func (m *Match) applyEffectDelaySetup(ret []CardEffect, ce CardEffect, r *rng.RNG, network Network) []CardEffect {
	player := ce.Player
	if ce.Effect.Flags&card.FlagDelaySetupOpponent != 0 {
		player = 1 - player
	}

	matches := m.FilterCards(ce.Effect.Filter, ce.Target, true, false)

	for i := 0; i < len(m.State.Sides[player].Setup); i++ {
		c := m.State.Sides[player].Setup[i]

		if len(c.Effects) == 0 {
			continue
		}

		found := false

		for _, id := range matches {
			if c.Card.Def.ID == id {
				found = true

				break
			}
		}

		if !found {
			continue
		}

		m.Log.Logf("Matching setup card: %d %q", uint64(c.Card.Def.ID), c.Card.Def.DisplayName())

		ret = append(ret, CardEffect{
			Card:   c,
			Player: player,
		})

		if ce.Effect.Amount > 0 {
			for j := range c.Effects {
				m.Log.Logf("Processing setup effect: %v", &card.RichDescription{
					Content: c.Effects[j].Effect.Description(c.Card.Def, m.Set),
				})

				flag := card.FlagCondApplyNextRound

				switch c.Mode {
				case ModeSetup:
					flag |= card.FlagCondApplyShowJustEffect
				case ModeSetupOriginal:
					flag |= card.FlagCondApplyShowOriginalText
				case ModeSetupHidden:
					flag |= card.FlagCondApplyShowNothing
				}

				for k := int64(0); k < ce.Effect.Amount; k++ {
					c.Effects[j] = CardEffect{
						Effect: &card.EffectDef{
							Type:     card.CondApply,
							Priority: 1,
							Flags:    flag,

							Result: c.Effects[j].Effect,
						},
						Target: c.Effects[j].Target,
					}
				}

				m.Log.Logf("After processing: %v", &card.RichDescription{
					Content: c.Effects[j].Effect.Description(c.Card.Def, m.Set),
				})
			}
		} else if ce.Effect.Amount < 0 {
			for j := 0; j < len(c.Effects); j++ {
				m.Log.Logf("Processing setup effect: %v", &card.RichDescription{
					Content: c.Effects[j].Effect.Description(c.Card.Def, m.Set),
				})

				removed := false

				for k := int64(0); k > ce.Effect.Amount; k-- {
					if c.Effects[j].Effect.Type != card.CondApply || c.Effects[j].Effect.Flags&(card.FlagCondApplyNextRound|card.FlagCondApplyOpponent) != card.FlagCondApplyNextRound {
						e := c.Effects[j]

						removed = true
						c.Effects = append(c.Effects[:j], c.Effects[j+1:]...)

						if ce.Effect.Flags&card.FlagDelaySetupDelete == 0 {
							m.queueEffect(CardEffect{
								Card:   c,
								Effect: e.Effect,
								Target: e.Target,
								Player: player,
							}, false)
						}

						c.Desc = &card.RichDescription{
							Effect: e.Effect,

							Content: e.Effect.Description(c.Card.Def, m.Set),
							Color:   sprites.Black,
						}

						break
					}

					c.Effects[j].Effect = c.Effects[j].Effect.Result
				}

				if removed {
					if ce.Effect.Flags&card.FlagDelaySetupDelete == 0 {
						m.Log.Log("Applying effect this round.")
					} else {
						m.Log.Log("Destroying setup effect.")
					}
				} else {
					m.Log.Logf("After processing: %v", &card.RichDescription{
						Content: c.Effects[j].Effect.Description(c.Card.Def, m.Set),
					})
				}
			}
		}

		if len(c.Effects) == 0 {
			m.State.Sides[player].Setup = append(m.State.Sides[player].Setup[:i], m.State.Sides[player].Setup[i+1:]...)
			i--

			continue
		}

		if c.Mode == ModeSetup {
			c.Desc = &card.RichDescription{
				Effect: c.Effects[0].Effect,

				Content: (&card.EffectDef{
					Type:   card.CondApply,
					Flags:  card.FlagCondApplyNextRound,
					Result: c.Effects[0].Effect,
				}).Description(c.Card.Def, m.Set),
				Color: sprites.Black,
			}
		}
	}

	return ret
}

func (m *Match) applyCondCard(ret []CardEffect, ce CardEffect, r *rng.RNG, network Network) []CardEffect {
	player := ce.Player
	if ce.Effect.Flags&card.FlagCondCardOpponent != 0 {
		player = 1 - player
	}

	var count []card.ID

	matches := m.FilterCards(ce.Effect.Filter, ce.Target, true, false)

	switch {
	case ce.Effect.Flags&card.FlagCondCardLocationMask == card.FlagCondCardLocationField:
		for _, c := range m.State.Sides[player].Field {
			if c.Mode.Ignore() && (c.Mode != ModeNumb || ce.Effect.Flags&card.FlagCondCardAllowNumb == 0) {
				continue
			}

			if cardInSet(matches, c.Card.Def.ID) {
				m.Log.Logf("Matching card: %v", c.Card.Def.DisplayName())
				count = append(count, c.Card.Def.ID)
			}
		}
	case m.UnknownDeck-1 == player:
		cards := network.RecvModifiedCards(true)

		for _, mc := range cards {
			if m.UnknownDeck == 0 || router.FlagDebugRevealModified.IsSet() {
				m.Log.Logf("Matching card: %v", m.Set.Card(mc.CardID).DisplayName())
			}

			count = append(count, mc.CardID)
		}
	default:
		var positions []card.ModifiedCardPosition

		if ce.Effect.Flags&card.FlagCondCardLocationMask == card.FlagCondCardLocationHand {
			for i, c := range m.State.Sides[player].Hand {
				if cardInSet(matches, c.Card.Def.ID) {
					m.Log.Logf("Matching card: %v", c.Card.Def.DisplayName())

					positions = append(positions, card.ModifiedCardPosition{
						InHand:   true,
						Position: uint64(i),
						CardID:   c.Card.Def.ID,
					})

					count = append(count, c.Card.Def.ID)
				}
			}
		} else {
			for i, c := range m.State.Sides[player].Deck {
				if cardInSet(matches, c.Def.ID) {
					m.Log.Logf("Matching card: %v", c.Def.DisplayName())

					positions = append(positions, card.ModifiedCardPosition{
						InHand:   false,
						Position: uint64(i),
						CardID:   c.Def.ID,
					})

					count = append(count, c.Def.ID)
				}
			}
		}

		if 2-m.UnknownDeck == player {
			if err := network.SendModifiedCards(positions, true); err != nil {
				log.Printf("ERROR: failed to send modified cards: %+v", err)
			}
		}
	}

	var success []card.ID

	switch ce.Effect.Flags & card.FlagCondCardTypeMask {
	case card.FlagCondCardTypeGreaterEqual:
		if int64(len(count)) >= ce.Effect.Amount {
			m.Log.Logf("Matching card count %d is at least %d.", len(count), ce.Effect.Amount)

			success = count[:1]
		} else {
			m.Log.Logf("Matching card count %d is not at least %d.", len(count), ce.Effect.Amount)
		}
	case card.FlagCondCardTypeLessThan:
		if int64(len(count)) < ce.Effect.Amount {
			m.Log.Logf("Matching card count %d is less than %d.", len(count), ce.Effect.Amount)

			success = []card.ID{ce.Target}
		} else {
			m.Log.Logf("Matching card count %d is not less than %d.", len(count), ce.Effect.Amount)
		}
	case card.FlagCondCardTypeEach:
		successes := int64(len(count)) / ce.Effect.Amount
		if successes < 0 {
			successes = 0
		}

		success = count[:successes]

		m.Log.Logf("Matching card count is %d. Applying effect %d times.", len(count), successes)
	}

	for _, id := range success {
		ce2 := CardEffect{
			Card:   ce.Card,
			Effect: ce.Effect.Result,
			Target: id,
			Player: ce.Player,
		}

		ret = append(ret, ce2)
		m.queueEffect(ce2, false)
	}

	return ret
}

func (m *Match) applyCondLimit(ret []CardEffect, ce CardEffect, r *rng.RNG, network Network) []CardEffect {
	m.State.Sides[ce.Player].Limit[ce.Effect]++

	n := m.State.Sides[ce.Player].Limit[ce.Effect]
	gt := ce.Effect.Flags&card.FlagCondLimitGreaterThan != 0
	success := false

	switch {
	case !gt && n <= ce.Effect.Amount:
		m.Log.Logf("Effect has occurred %d times, which is at most %d.", n, ce.Effect.Amount)

		success = true
	case gt && n <= ce.Effect.Amount:
		m.Log.Logf("Effect has occurred %d times, which is not greater than %d.", n, ce.Effect.Amount)
	case !gt && n > ce.Effect.Amount:
		m.Log.Logf("Effect has occurred %d times, which is not at most %d.", n, ce.Effect.Amount)
	case gt && n > ce.Effect.Amount:
		m.Log.Logf("Effect has occurred %d times, which is greater than %d.", n, ce.Effect.Amount)

		success = true
	}

	if success {
		ce2 := CardEffect{
			Card:   ce.Card,
			Effect: ce.Effect.Result,
			Target: ce.Target,
			Player: ce.Player,
		}

		ret = append(ret, ce2)
		m.queueEffect(ce2, false)
	}

	return ret
}

func (m *Match) applyCondWinner(ret []CardEffect, ce CardEffect, r *rng.RNG, network Network) []CardEffect {
	success := false

	switch ce.Effect.Flags & card.FlagCondWinnerTypeMask {
	case card.FlagCondWinnerTypeWinner:
		success = m.State.RoundWinner == ce.Player+1
	case card.FlagCondWinnerTypeLoser:
		success = m.State.RoundWinner == 2-ce.Player
	case card.FlagCondWinnerTypeTie:
		success = m.State.RoundWinner == 0
	case card.FlagCondWinnerTypeNotTie:
		success = m.State.RoundWinner != 0
	case card.FlagCondWinnerTypeNotWinner:
		success = m.State.RoundWinner != ce.Player+1
	case card.FlagCondWinnerTypeNotLoser:
		success = m.State.RoundWinner != 2-ce.Player
	}

	if success {
		ce2 := CardEffect{
			Card:   ce.Card,
			Effect: ce.Effect.Result,
			Target: ce.Target,
			Player: ce.Player,
		}

		ret = append(ret, ce2)
		m.queueEffect(ce2, false)
	}

	return ret
}

func (m *Match) applyCondApply(ret []CardEffect, ce CardEffect, r *rng.RNG, network Network) []CardEffect {
	player := ce.Player
	if ce.Effect.Flags&card.FlagCondApplyOpponent != 0 {
		player = 1 - player
	}

	var desc *card.RichDescription

	if ce.Effect.Flags&card.FlagCondApplyShowMask == card.FlagCondApplyShowJustEffect {
		e := ce.Effect
		if ce.Effect.Flags&card.FlagCondApplyNextRound == 0 {
			e = e.Result
		}

		desc = &card.RichDescription{
			Effect: e,

			Content: e.Description(ce.Card.Card.Def, m.Set),
			Color:   sprites.Black,
		}
	}

	target := &m.State.Sides[player].Field
	if ce.Effect.Flags&card.FlagCondApplyNextRound != 0 {
		target = &m.State.Sides[player].Setup
	}

	var ghostCard *ActiveCard

	wantMode := ModeSetup

	switch ce.Effect.Flags & card.FlagCondApplyShowMask {
	case card.FlagCondApplyShowOriginalText:
		wantMode = ModeSetupOriginal
	case card.FlagCondApplyShowNothing:
		wantMode = ModeSetupHidden
	}

	if desc == nil {
		for _, c := range *target {
			if c.Mode == wantMode && c.CreatedBy == ce.Card {
				ghostCard = c

				break
			}
		}

		if ghostCard == nil {
			ghostCard = &ActiveCard{
				Card: &Card{
					Set:  m.Set,
					Def:  ce.Card.Card.Def,
					Back: ce.Card.Card.Back,
				},
				Desc:        m.cardDescription(ce.Card.Card.Def),
				Mode:        wantMode,
				CreatedBy:   ce.Card,
				StayOnField: true,
			}

			*target = append(*target, ghostCard)
		}
	} else {
		ghostCard = &ActiveCard{
			Card: &Card{
				Set:  m.Set,
				Def:  ce.Card.Card.Def,
				Back: ce.Card.Card.Back,
			},
			Desc:        desc,
			Mode:        wantMode,
			CreatedBy:   ce.Card,
			StayOnField: true,
		}

		*target = append(*target, ghostCard)
	}

	ghostCard.Effects = append(ghostCard.Effects, CardEffect{
		Effect: ce.Effect.Result,
		Target: ce.Target,
	})

	ce2 := CardEffect{
		Card:   ghostCard,
		Effect: ce.Effect.Result,
		Target: ce.Target,
		Player: player,
	}

	if ce.Effect.Flags&card.FlagCondApplyNextRound == 0 {
		m.queueEffect(ce2, false)
	}

	ret = append(ret, ce2)

	return ret
}

func (m *Match) applyCondCoin(ret []CardEffect, ce CardEffect, r *rng.RNG, network Network) []CardEffect {
	multiplier := m.consumeMultiple(ce)
	for i := int64(0); i < ce.Effect.Amount*multiplier; i++ {
		chanceHeads := int(ce.Effect.Amount2)
		chanceTails := int(ce.Effect.Amount3)

		heads := r.RangeInt(0, chanceHeads+chanceTails) >= chanceTails

		ce2 := CardEffect{
			Card:   ce.Card,
			Target: ce.Target,
			Player: ce.Player,
		}

		if heads {
			m.Log.Logf("Coin %d of %d lands on heads.", i+1, ce.Effect.Amount*multiplier)
			ce2.Effect = ce.Effect.Result
		} else {
			m.Log.Logf("Coin %d of %d lands on tails.", i+1, ce.Effect.Amount*multiplier)
			ce2.Effect = ce.Effect.Result2
		}

		ret = append(ret, ce2)

		if ce2.Effect != nil {
			m.queueEffect(ce2, false)
		}
	}

	return ret
}

func (m *Match) applyCondStat(ret []CardEffect, ce CardEffect, r *rng.RNG, network Network) []CardEffect {
	player := ce.Player
	if ce.Effect.Flags&card.FlagCondStatOpponent != 0 {
		player = 1 - player
	}

	var (
		num  card.Number
		name string
	)

	switch ce.Effect.Flags & card.FlagCondStatTypeMask {
	case card.FlagCondStatTypeATK:
		num = m.State.Sides[player].ATK
		name = "ATK"
	case card.FlagCondStatTypeDEF:
		num = m.State.Sides[player].DEF
		name = "DEF"
	case card.FlagCondStatTypeHP:
		num = m.State.Sides[player].HP
		name = "HP"
	case card.FlagCondStatTypeTP:
		num = m.State.Sides[player].TP
		name = "TP"
	}

	var (
		otherNum    card.Number
		otherName   string
		otherPlayer = player
	)

	if ce.Effect.Flags&card.FlagCondStatMathOpponent != 0 {
		otherPlayer = 1 - otherPlayer

		if ce.Effect.Flags&card.FlagCondStatOpponent == 0 {
			otherName = "opponent's "
		}
	} else if ce.Effect.Flags&card.FlagCondStatOpponent != 0 {
		otherName = "own "
	}

	switch ce.Effect.Flags & card.FlagCondStatMathTypeMask {
	case card.FlagCondStatMathTypeATK:
		otherNum = m.State.Sides[otherPlayer].ATK
		otherName += "ATK"
	case card.FlagCondStatMathTypeDEF:
		otherNum = m.State.Sides[otherPlayer].DEF
		otherName += "DEF"
	case card.FlagCondStatMathTypeHP:
		otherNum = m.State.Sides[otherPlayer].HP
		otherName += "HP"
	case card.FlagCondStatMathTypeTP:
		otherNum = m.State.Sides[otherPlayer].TP
		otherName += "TP"
	}

	beforeMath := num

	switch ce.Effect.Flags & card.FlagCondStatMathMask {
	case card.FlagCondStatMathNone:
		// no math
	case card.FlagCondStatMathAdd:
		num.Add(otherNum, m.SpecialFlags[card.SpecialSubtractInf])
		m.Log.Logf("Adding %s to %s: %v + %v = %v", otherName, name, beforeMath, otherNum, num)
	case card.FlagCondStatMathSubtract:
		negOtherNum := otherNum
		negOtherNum.Negate()
		num.Add(negOtherNum, m.SpecialFlags[card.SpecialSubtractInf])
		m.Log.Logf("Subtracting %s from %s: %v - %v = %v", otherName, name, beforeMath, otherNum, num)
	}

	var success int64

	switch less, each := ce.Effect.Flags&card.FlagCondStatLessThan != 0, ce.Effect.Flags&card.FlagCondStatMultiple != 0; {
	case num.NaN:
		m.Log.Logf("Player %d's %s stat is invalid; skipping comparison.", player+1, name)
	case !each && num.AmountInf != 0:
		switch {
		case !less && num.AmountInf > 0:
			m.Log.Logf("Player %d's %s stat is positive infinity, which is at least %d.", player+1, name, ce.Effect.Amount)

			success = 1
		case less && num.AmountInf > 0:
			m.Log.Logf("Player %d's %s stat is positive infinity, which is not less than %d.", player+1, name, ce.Effect.Amount)
		case !less:
			m.Log.Logf("Player %d's %s stat is negative infinity, which is not at least %d.", player+1, name, ce.Effect.Amount)
		default:
			m.Log.Logf("Player %d's %s stat is negative infinity, which is less than %d.", player+1, name, ce.Effect.Amount)

			success = 1
		}
	case !each:
		switch {
		case !less && num.Amount >= ce.Effect.Amount:
			m.Log.Logf("Player %d's %s stat is %d, which is at least %d.", player+1, name, num.Amount, ce.Effect.Amount)

			success = 1
		case less && num.Amount >= ce.Effect.Amount:
			m.Log.Logf("Player %d's %s stat is %d, which is not less than %d.", player+1, name, num.Amount, ce.Effect.Amount)
		case !less && num.Amount < ce.Effect.Amount:
			m.Log.Logf("Player %d's %s stat is %d, which is not at least %d.", player+1, name, num.Amount, ce.Effect.Amount)
		case less && num.Amount < ce.Effect.Amount:
			m.Log.Logf("Player %d's %s stat is %d, which is less than %d.", player+1, name, num.Amount, ce.Effect.Amount)

			success = 1
		}
	case num.AmountInf < 0:
		m.Log.Logf("Player %d's %s stat is negative infinity. Applying effect 0 times.", player+1, name)
	default:
		if ce.Effect.Amount <= 0 || (less && ce.Effect.Amount <= 1) {
			success = ce.Effect.Amount - num.Amount
		} else {
			success = num.Amount / ce.Effect.Amount
		}

		if success < 0 {
			success = 0
		}

		m.Log.Logf("Player %d's %s stat (ignoring infinities) is %d. Applying effect %d times.", player+1, name, num.Amount, success)
	}

	ce2 := CardEffect{
		Card:   ce.Card,
		Effect: ce.Effect.Result,
		Target: ce.Target,
		Player: ce.Player,
	}

	for i := int64(0); i < success; i++ {
		ret = append(ret, ce2)
		m.queueEffect(ce2, false)
	}

	return ret
}

func (m *Match) applyCondInHand(ret []CardEffect, ce CardEffect, r *rng.RNG, network Network) []CardEffect {
	// no effect during round
	return ret
}

func (m *Match) applyCondLastEffect(ret []CardEffect, ce CardEffect, r *rng.RNG, network Network) []CardEffect {
	ce2 := CardEffect{
		Card:   ce.Card,
		Effect: ce.Effect.Result,
		Target: ce.Target,
		Player: ce.Player,
	}

	ret = append(ret, ce2)
	ret = m.applyEffect(ret, ce2, r, network)

	for _, q := range []*[]CardEffect{&m.State.PreQueue, &m.State.Queue} {
		for i := 0; i < len(*q); i++ {
			if (*q)[i].Card == ce.Card {
				if !m.Log.Disabled {
					m.Log.Logf("Cancelling queued effect: %v", &card.RichDescription{
						Content: (*q)[i].Effect.Description(ce.Card.Card.Def, m.Set),
					})
				}

				*q = append((*q)[:i], (*q)[i+1:]...)
				i--
			}
		}
	}

	return ret
}

func (m *Match) applyCondOnNumb(ret []CardEffect, ce CardEffect, r *rng.RNG, network Network) []CardEffect {
	m.State.OnNumb = append(m.State.OnNumb, ce)

	return ret
}

func (m *Match) applyCondMultipleEffects(ret []CardEffect, ce CardEffect, r *rng.RNG, network Network) []CardEffect {
	m.queueEffect(CardEffect{
		Card:   ce.Card,
		Effect: ce.Effect.Result,
		Target: ce.Target,
		Player: ce.Player,
	}, false)

	m.queueEffect(CardEffect{
		Card:   ce.Card,
		Effect: ce.Effect.Result2,
		Target: ce.Target,
		Player: ce.Player,
	}, false)

	return ret
}

func (m *Match) applyCondOnDiscard(ret []CardEffect, ce CardEffect, r *rng.RNG, network Network) []CardEffect {
	// no effect
	return ret
}

func (m *Match) applyCondCompareTargetCard(ret []CardEffect, ce CardEffect, r *rng.RNG, network Network) []CardEffect {
	matches := m.FilterCards(ce.Effect.Filter, ce.Target, true, false)

	isMatch := false
	for _, id := range matches {
		if ce.Target == id {
			isMatch = true

			break
		}
	}

	if isMatch {
		m.Log.Logf("Target card %d %q matches filter.", uint64(ce.Target), m.Set.Card(ce.Target).DisplayName())
	} else {
		m.Log.Logf("Target card %d %q does NOT match filter.", uint64(ce.Target), m.Set.Card(ce.Target).DisplayName())
	}

	if isMatch == (ce.Effect.Flags&card.FlagCondCompareTargetCardInvert == 0) {
		m.queueEffect(CardEffect{
			Card:   ce.Card,
			Effect: ce.Effect.Result,
			Target: ce.Target,
			Player: ce.Player,
		}, false)
	}

	return ret
}

func (m *Match) applyCondOnExile(ret []CardEffect, ce CardEffect, r *rng.RNG, network Network) []CardEffect {
	// no effect
	return ret
}

func (m *Match) SyncInHand(sync func([]byte) []byte, turnData *card.TurnData) error {
	m.State.TurnData = turnData

	turnData.InHand[m.Perspective-1] = 0
	turnData.HandID[m.Perspective-1] = nil

	for i, c := range m.State.Sides[m.Perspective-1].Hand {
		for _, e := range c.Def.Effects {
			if e.Type == card.CondInHand {
				turnData.InHand[m.Perspective-1] |= 1 << i
				turnData.HandID[m.Perspective-1] = append(turnData.HandID[m.Perspective-1], c.Def.ID)

				break
			}
		}
	}

	var w format.Writer

	w.UVarInt(turnData.InHand[m.Perspective-1])

	for _, id := range turnData.HandID[m.Perspective-1] {
		w.UVarInt(uint64(id))
	}

	var r format.Reader

	r.Init(sync(w.Data()))

	turnData.InHand[2-m.Perspective] = r.UVarInt()
	turnData.HandID[2-m.Perspective] = make([]card.ID, bits.OnesCount64(turnData.InHand[2-m.Perspective]))

	for i := range turnData.HandID[2-m.Perspective] {
		turnData.HandID[2-m.Perspective][i] = card.ID(r.UVarInt())
	}

	for player := range m.State.Sides {
		if expectedCards := (64 - bits.LeadingZeros64(turnData.InHand[player])); expectedCards > len(m.State.Sides[player].Hand) {
			return fmt.Errorf("match: InHand data desync for player %d: expected at least %d cards in hand, but only %d are present", player+1, expectedCards, len(m.State.Sides[player].Hand))
		}

		inHand := turnData.HandID[player]

		if bitsSet := bits.OnesCount64(turnData.InHand[player]); bitsSet != len(inHand) {
			return fmt.Errorf("match: InHand data desync for player %d: %d placement bits set, but %d card IDs sent", player+1, bitsSet, len(inHand))
		}

		for i, c := range m.State.Sides[player].Hand {
			if turnData.InHand[player]&(1<<i) == 0 {
				continue
			}

			if c.Def == nil {
				c.Def = m.Set.Card(inHand[0])

				if c.Def.Rank.Back(m.SpecialFlags[card.SpecialEffectBack]) != c.Back {
					return fmt.Errorf("match: InHand data desync for player %d: card %d in hand is %q, but has back %v (not %v)", player+1, i+1, c.Def.DisplayName(), c.Back, c.Def.Rank.Back(m.SpecialFlags[card.SpecialEffectBack]))
				}
			} else if c.Def.ID != inHand[0] {
				return fmt.Errorf("match: InHand data desync for player %d: card %d in hand is %q; expected %q", player+1, i+1, c.Def.DisplayName(), m.Set.Card(inHand[0]).DisplayName())
			}

			inHand = inHand[1:]
		}
	}

	return nil
}
