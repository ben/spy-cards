package match

import (
	"git.lubar.me/ben/spy-cards/card"
	"git.lubar.me/ben/spy-cards/scnet"
)

// PublicShuffle shuffles a set of cards where only the backs are known.
func PublicShuffle(scg *scnet.SecureCardGame, cards []*Card) {
	scg.Shuffle(scnet.RNGShared, len(cards), func(i, j int) {
		cards[i], cards[j] = cards[j], cards[i]
	})
}

// PrivateShuffle shuffles a set of cards.
func PrivateShuffle(scg *scnet.SecureCardGame, cards []*Card, remote bool) {
	private := scnet.RNGLocal
	if remote {
		private = scnet.RNGRemote
	}

	var (
		cardArrays []*[]*Card
		cardBacks  = make([]card.Rank, 0, len(cards))
		byBack     = make(map[card.Rank]*[]*Card)
	)

	for _, c := range cards {
		bb, ok := byBack[c.Back]
		if !ok {
			bb = new([]*Card)
			cardArrays = append(cardArrays, bb)
			byBack[c.Back] = bb
		}

		*bb = append(*bb, c)

		cardBacks = append(cardBacks, c.Back)
	}

	scg.Shuffle(scnet.RNGShared, len(cardBacks), func(i, j int) {
		cardBacks[i], cardBacks[j] = cardBacks[j], cardBacks[i]
	})

	for _, a := range cardArrays {
		arr := *a

		scg.Shuffle(private, len(arr), func(i, j int) {
			arr[i], arr[j] = arr[j], arr[i]
		})
	}

	for i := range cards {
		bb := byBack[cardBacks[i]]
		c := (*bb)[0]
		*bb = (*bb)[1:]
		cards[i] = c
	}
}
