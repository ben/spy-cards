package match

import (
	"errors"
	"fmt"
	"log"
	"strings"
)

// GameLog is a human-readable description of the events that occurred in a
// Spy Cards Online match.
type GameLog struct {
	Root     *LogGroup
	stack    []*LogGroup
	Disabled bool
	Debug    bool
}

type LogGroup struct {
	Title     string
	Index     int
	Messages  []string
	SubGroups []*LogGroup
}

func (l *GameLog) cur() *LogGroup {
	if len(l.stack) == 0 {
		l.Root = &LogGroup{}
		l.stack = []*LogGroup{l.Root}
	}

	return l.stack[len(l.stack)-1]
}

// Log adds a message to the current log group.
func (l *GameLog) Log(message string) {
	if l.Disabled {
		return
	}

	g := l.cur()
	g.Messages = append(g.Messages, message)

	if l.Debug {
		log.Printf("DEBUG: game log:%s %s", strings.Repeat("  ", len(l.stack)-1), message)
	}
}

// Logf adds a formatted message to the current log group.
func (l *GameLog) Logf(message string, args ...interface{}) {
	if l.Disabled {
		return
	}

	l.Log(fmt.Sprintf(message, args...))
}

// Push adds a child log group to the current log group, making it current.
func (l *GameLog) Push(title string) {
	if l.Disabled {
		return
	}

	g := l.cur()

	sg := &LogGroup{
		Title: title,
		Index: len(g.Messages),
	}

	g.Messages = append(g.Messages, "")
	g.SubGroups = append(g.SubGroups, sg)
	l.stack = append(l.stack, sg)

	if l.Debug {
		log.Printf("DEBUG: game log:%s %s", strings.Repeat("##", len(l.stack)-1), title)
	}
}

// Pushf adds a log group with a formatted title.
func (l *GameLog) Pushf(title string, args ...interface{}) {
	if l.Disabled {
		return
	}

	l.Push(fmt.Sprintf(title, args...))
}

// Pop moves to the parent log level, closing the current log group.
func (l *GameLog) Pop() {
	if l.Disabled {
		return
	}

	g := l.cur()

	if g == l.Root {
		panic(errors.New("match: internal error: game log level underflow"))
	}

	l.stack = l.stack[:len(l.stack)-1]
}

func diffGameLogs(a, b *LogGroup) {
	if a == nil {
		log.Println("WARNING: left game log is nil")

		return
	}

	if b == nil {
		log.Println("WARNING: right game log is nil")

		return
	}

	ch1, ch2 := make(chan string), make(chan string)

	go writeGameLog(ch1, a)
	go writeGameLog(ch2, b)

	for {
		m1, ok1 := <-ch1
		m2, ok2 := <-ch2

		if !ok1 && !ok2 {
			break
		}

		switch {
		case !ok1:
			log.Println("WARNING: left game log ends early")

			for range ch2 {
				// discard
			}
		case !ok2:
			log.Println("WARNING: right game log ends early")

			for range ch1 {
				// discard
			}
		case m1 != m2:
			log.Println("WARNING: game log messages differ:", m1, "<=>", m2)
		default:
			log.Println("DEBUG: game log message:", m1)
		}
	}
}

func writeGameLog(ch chan<- string, g *LogGroup) {
	defer close(ch)

	writeGameLogGroup(ch, g)
}

func writeGameLogGroup(ch chan<- string, g *LogGroup) {
	ch <- "# " + g.Title

	for i, j := 0, 0; i < len(g.Messages); i++ {
		if j < len(g.SubGroups) && g.SubGroups[j].Index == i {
			writeGameLogGroup(ch, g.SubGroups[j])
			j++
		} else {
			ch <- g.Messages[i]
		}
	}
}
