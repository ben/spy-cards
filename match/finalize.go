package match

import (
	"fmt"

	"git.lubar.me/ben/spy-cards/card"
	"git.lubar.me/ben/spy-cards/scnet"
)

// Finalize verifies a match and returns a recording of the match.
func (m *Match) Finalize(g *scnet.Game) (*card.Recording, error) {
	var rec card.Recording

	rec.FormatVersion = 2
	rec.Version = m.Init.Version
	rec.ModeName = m.Init.Mode
	rec.Perspective = m.Perspective
	rec.Cosmetic = m.Cosmetic
	rec.RematchCount = m.Rematches
	rec.WonMatches = m.Wins
	rec.Start = m.Start
	rec.SpoilerGuard = m.Set.SpoilerGuard

	if m.Perspective == 2 {
		rec.SpoilerGuard[0], rec.SpoilerGuard[1] = rec.SpoilerGuard[1], rec.SpoilerGuard[0]
	}

	var vm Match

	vm.Init = m.Init
	vm.Start = m.Start
	vm.Rematches = m.Rematches
	vm.Perspective = m.Perspective
	vm.UnknownDeck = 0
	vm.Cosmetic = m.Cosmetic
	vm.Set = m.Set
	vm.Rules = m.Rules
	vm.Timer = m.Timer
	vm.Banned = m.Banned
	vm.Unpickable = m.Unpickable
	vm.Unfiltered = m.Unfiltered
	vm.HasReplaceSummon = m.HasReplaceSummon
	vm.OrderForSummon = m.OrderForSummon

	vm.IndexSet() // reset game rules

	err := g.FinalizeMatch(func(myDeck, theirDeck card.Deck, backs card.UnknownDeck, sharedSeed, localSeed, remoteSeed []byte) error {
		copy(rec.SharedSeed[:], sharedSeed)
		copy(rec.PrivateSeed[m.Perspective-1][:], localSeed)
		copy(rec.PrivateSeed[2-m.Perspective][:], remoteSeed)
		rec.CustomCards.CopyFrom(m.Set)
		rec.CustomCardsRaw = m.Init.Custom
		rec.InitialDeck[m.Perspective-1] = myDeck
		rec.InitialDeck[2-m.Perspective] = theirDeck
		rec.Rounds = make([]card.RecordingRound, 0, m.State.Round)

		if len(theirDeck) != len(backs) {
			return fmt.Errorf("opponent's deck of %d cards cannot match %d card backs", len(theirDeck), len(backs))
		}

		for i, b := range backs {
			c := m.Set.Card(theirDeck[i])

			if realBack := c.Rank.Back(m.SpecialFlags[card.SpecialEffectBack]); b != realBack {
				return fmt.Errorf("card %d of %d in opponent's deck (%d %q) has back %v, not %v", i+1, len(theirDeck), uint64(c.ID), c.DisplayName(), realBack, b)
			}
		}

		for player, deck := range rec.InitialDeck {
			if err := deck.Validate(m.Set); err != nil {
				return fmt.Errorf("player %d deck failed validation: %w", player+1, err)
			}

			vm.State.Sides[player].Deck = make([]*Card, len(deck))

			for i, id := range deck {
				c := vm.Set.Card(id)

				vm.State.Sides[player].Deck[i] = &Card{
					Set:  vm.Set,
					Def:  c,
					Back: c.Rank.Back(m.SpecialFlags[card.SpecialEffectBack]),
				}
			}
		}

		return nil
	}, func(seed, seed2 []byte, turn *card.TurnData) error {
		var round card.RecordingRound

		copy(round.TurnSeed[:], seed)
		copy(round.TurnSeed2[:], seed2)
		round.Ready = turn.Ready

		rec.Rounds = append(rec.Rounds, round)

		if vm.State.Round == 0 {
			vm.InitState(g.SCG.Rand(scnet.RNGShared), nil, func(ac *ActiveCard) {})
		}

		vm.State.Round++

		vm.ShuffleAndDraw(g.SCG, true)

		for player := range turn.InHand {
			mask := turn.InHand[player]
			cards := turn.HandID[player]

			for i := 0; mask>>i != 0; i++ {
				if mask&(1<<i) != 0 {
					if cards[0] != vm.State.Sides[player].Hand[i].Def.ID {
						return fmt.Errorf("in-hand card mismatch for player %d, card %d/%d: claimed %q, but card is %q", player+1, i+1, len(vm.State.Sides[player].Hand), vm.Set.Card(cards[0]).DisplayName(), vm.State.Sides[player].Hand[i].Def.DisplayName())
					}

					cards = cards[1:]
				}
			}
		}

		// TODO: also check for cards that *should* have been declared but weren't

		m.RecomputePassives(turn)

		for player := range turn.Ready {
			mask := turn.Ready[player]
			cards := turn.Played[player]

			for i := 0; mask>>i != 0; i++ {
				if mask&(1<<i) != 0 {
					if cards[0] != vm.State.Sides[player].Hand[i].Def.ID {
						return fmt.Errorf("played card mismatch for player %d, card %d/%d: claimed %q, but card is %q", player+1, i+1, len(vm.State.Sides[player].Hand), vm.Set.Card(cards[0]).DisplayName(), vm.State.Sides[player].Hand[i].Def.DisplayName())
					}

					cards = cards[1:]
				}
			}
		}

		g.SCG.WhenConfirmedTurn(round.TurnSeed2[:])

		if err := vm.BeginTurn(turn); err != nil {
			return err
		}

		rng := g.SCG.Rand(scnet.RNGShared)

		q := vm.ProcessQueuedEffect(rng, nil)
		for len(q) != 0 {
			q = vm.ProcessQueuedEffect(rng, nil)
		}

		// TODO: verify turn.Modified

		return nil
	})
	if err != nil {
		if m.Perspective == 2 {
			diffGameLogs(m.Log.Root, vm.Log.Root)
		}

		return &rec, fmt.Errorf("match: finalizing: %w", err)
	}

	return &rec, nil
}
