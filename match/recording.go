package match

import (
	"context"
	"fmt"
	"log"
	"math/bits"
	"sync"

	"git.lubar.me/ben/spy-cards/card"
	"git.lubar.me/ben/spy-cards/scnet"
)

type Recording struct {
	Recording  *card.Recording
	Set        *card.Set
	match      *Match
	scg        *scnet.SecureCardGame
	summon0    []*ActiveCard
	rounds     []recordingRound
	processErr error
	lock       sync.Mutex
}

type recordingRound struct {
	start *State
	final *State
	ready card.TurnData
}

func NewRecording(ctx context.Context, rec *card.Recording) (*Recording, error) {
	r := &Recording{
		Recording: rec,
	}

	if err := r.initMatch(ctx); err != nil {
		return nil, err
	}

	return r, nil
}

func (r *Recording) CreateMatch() *Match {
	m := &Match{
		Init: &Init{
			Mode:    r.Recording.ModeName,
			Version: r.Recording.Version,
			Custom:  r.Recording.CustomCardsRaw,
			Variant: uint64(r.Recording.CustomCards.Variant),
		},
		Rematches:   r.Recording.RematchCount,
		Wins:        r.Recording.WonMatches,
		Losses:      r.Recording.RematchCount - r.Recording.WonMatches,
		Perspective: r.Recording.Perspective,
		Cosmetic:    r.Recording.Cosmetic,
		Set:         r.Set,
	}

	if r.Recording.Perspective != 2 {
		m.Perspective = 1
	}

	if m.Set == nil {
		m.Set = &card.Set{}
		m.Set.CopyFrom(&r.Recording.CustomCards)

		if m.Set.Variant != -1 && m.Init.Version[0] == 0 && m.Init.Version[1] < 3 {
			m.Set.Mode, _ = m.Set.Mode.Variant(m.Set.Variant)
		}

		r.Set = m.Set
	}

	m.IndexSet()

	return m
}

func (r *Recording) initMatch(ctx context.Context) error {
	r.match = r.CreateMatch()
	r.match.Log.Disabled = true

	var err error

	r.scg, err = scnet.NewSecureCardGame(&scnet.SecureCardGameOptions{
		ForReplay: &r.Recording.Version,
	})
	if err != nil {
		return fmt.Errorf("creating SCG context for replay: %w", err)
	}

	copy(r.scg.Seed(scnet.RNGShared), r.Recording.SharedSeed[:])

	if r.match.Perspective == 2 {
		copy(r.scg.Seed(scnet.RNGLocal), r.Recording.PrivateSeed[1][:])
		copy(r.scg.Seed(scnet.RNGRemote), r.Recording.PrivateSeed[0][:])
	} else {
		copy(r.scg.Seed(scnet.RNGLocal), r.Recording.PrivateSeed[0][:])
		copy(r.scg.Seed(scnet.RNGRemote), r.Recording.PrivateSeed[1][:])
	}

	for player := range r.match.State.Sides {
		r.match.State.Sides[player].Deck = make([]*Card, len(r.Recording.InitialDeck[player]))

		for i, id := range r.Recording.InitialDeck[player] {
			c := r.match.Set.Card(id)

			r.match.State.Sides[player].Deck[i] = &Card{
				Set:  r.match.Set,
				Def:  c,
				Back: c.Rank.Back(r.match.SpecialFlags[card.SpecialEffectBack]),
			}
		}
	}

	r.rounds = make([]recordingRound, len(r.Recording.Rounds)+1)

	go r.process(ctx)

	return nil
}

// Turn0Summon returns the cards summoned on turn 0.
func (r *Recording) Turn0Summon() []*ActiveCard {
	r.lock.Lock()
	s := r.summon0
	r.lock.Unlock()

	return s
}

// NumRounds returns the total number of rounds in the recording (the final
// round in the recording is after the winner of the match is determined).
func (r *Recording) NumRounds() int {
	return len(r.rounds)
}

// Round returns a processed round from the match recording.
func (r *Recording) Round(i uint64) (*State, *card.TurnData, error) {
	if i >= uint64(len(r.rounds)) {
		return nil, nil, nil
	}

	r.lock.Lock()
	s := r.rounds[i]
	err := r.processErr
	r.lock.Unlock()

	if s.start == nil {
		return nil, nil, err
	}

	state := s.start.clone()

	if i == uint64(len(r.rounds))-1 {
		return state, nil, err
	}

	m := &Match{
		Log:   GameLog{Disabled: true},
		State: *state,
		Set:   r.match.Set,
		Rules: r.match.Rules,
	}

	m.RecomputePassives(&s.ready)

	*state = m.State

	return state, &s.ready, err
}

// RoundEnd returns the final state of a round from the match recording.
func (r *Recording) RoundEnd(i uint64) (*State, error) {
	if i >= uint64(len(r.rounds)) {
		return nil, nil
	}

	r.lock.Lock()
	s := r.rounds[i]
	err := r.processErr
	r.lock.Unlock()

	if s.final == nil {
		return nil, err
	}

	return s.final.clone(), err
}

// RoundProcessed returns true if the specified round has finished processing.
func (r *Recording) RoundProcessed(i uint64) bool {
	r.lock.Lock()
	s := r.rounds[i]
	err := r.processErr
	r.lock.Unlock()

	return s.start != nil || err != nil
}

func (r *Recording) process(ctx context.Context) {
	yield, cancel := r.yielder()
	defer cancel()

	for i, round := range r.Recording.Rounds {
		local, remote := r.Recording.PrivateSeed[0][:], r.Recording.PrivateSeed[1][:]
		if r.Recording.Perspective == 2 {
			local, remote = remote, local
		}

		r.scg.InitTurnSeed(round.TurnSeed[:], local, remote)

		if i == 0 {
			r.match.InitState(r.scg.Rand(scnet.RNGShared), nil, func(ac *ActiveCard) {
				r.lock.Lock()
				r.summon0 = append(r.summon0, ac)
				r.lock.Unlock()
			})
		}

		r.match.State.Round++

		r.match.ShuffleAndDraw(r.scg, true)

		ready := card.TurnData{
			Ready: round.Ready,
			Played: [2][]card.ID{
				make([]card.ID, 0, bits.OnesCount64(round.Ready[0])),
				make([]card.ID, 0, bits.OnesCount64(round.Ready[1])),
			},
		}

		for player := range ready.Played {
			for i, c := range r.match.State.Sides[player].Hand {
				if ready.Ready[player]&(1<<i) != 0 {
					ready.Played[player] = append(ready.Played[player], c.Def.ID)
				}
			}
		}

		r.match.RecomputePassives(&ready)

		state := r.match.State.clone()

		r.lock.Lock()
		r.rounds[i].start = state
		r.rounds[i].ready = ready
		r.lock.Unlock()

		if r.Recording.FormatVersion >= 1 {
			r.scg.WhenConfirmedTurn(round.TurnSeed2[:])
		}

		if err := r.match.BeginTurn(&ready); err != nil {
			log.Printf("WARNING: desync in recording at round %d: %+v", r.match.State.Round, err)
		}

		rng := r.scg.Rand(scnet.RNGShared)

		for len(r.match.ProcessQueuedEffect(rng, nil)) != 0 {
			yield()

			if err := ctx.Err(); err != nil {
				r.lock.Lock()
				r.processErr = err
				r.lock.Unlock()

				return
			}
		}

		state = r.match.State.clone()

		r.lock.Lock()
		r.rounds[i].final = state
		r.lock.Unlock()
	}

	r.lock.Lock()
	r.rounds[len(r.Recording.Rounds)].start = r.match.State.clone()
	r.lock.Unlock()

	if r.match.Log.Debug {
		log.Printf("DEBUG: at end of match: Player 1 HP: %v // Player 2 HP: %v // Round winner: %v // Match winner: %v", r.match.State.Sides[0].HP, r.match.State.Sides[1].HP, r.match.State.RoundWinner, r.match.Winner())
	}
}
