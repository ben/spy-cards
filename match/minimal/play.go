// Package minimal provides functions for playing Spy Cards Online in a
// non-interactive context.
package minimal

import (
	"context"
	"fmt"
	"time"

	"git.lubar.me/ben/spy-cards/card"
	"git.lubar.me/ben/spy-cards/match"
	"git.lubar.me/ben/spy-cards/match/npc"
	"git.lubar.me/ben/spy-cards/scnet"
)

// PlayMatch plays a networked Spy Cards Online match and returns the recording.
func PlayMatch(ctx context.Context, m *match.Match, g *scnet.Game, n npc.NPC) (*card.Recording, error) {
	g.WaitMatchReady()

	m.IndexSet()

	if m.Rematches == 0 {
		if c := n.PickPlayer(); c != nil {
			m.Cosmetic[m.Perspective-1].CharacterName = c.Name
		}

		if err := g.SendCosmeticData(m.Cosmetic[m.Perspective-1]); err != nil {
			return nil, fmt.Errorf("minimal: sending cosmetic data: %w", err)
		}
	}

	d := n.CreateDeck(m.Set)

	m.State.Sides[m.Perspective-1].Deck = make([]*match.Card, len(d))

	for i, id := range d {
		c := m.Set.Card(id)

		m.State.Sides[m.Perspective-1].Deck[i] = &match.Card{
			Set:  m.Set,
			Def:  c,
			Back: c.Rank.Back(m.SpecialFlags[card.SpecialEffectBack]),
		}
	}

	if d == nil {
		panic("Failed to create deck for NPC.")
	}

	g.SendDeck(d, m.SpecialFlags[card.SpecialEffectBack])

	od := g.WaitRecvDeck()
	m.State.Sides[2-m.Perspective].Deck = make([]*match.Card, len(od))

	for i, b := range od {
		m.State.Sides[2-m.Perspective].Deck[i] = &match.Card{
			Set:  m.Set,
			Back: b,
		}
	}

	if m.Rematches == 0 {
		if c := g.RecvCosmeticData(); c != nil {
			m.Cosmetic[2-m.Perspective].CharacterName = c.CharacterName
		}
	}

	recvData := make(chan *card.TurnData, 1)

	for m.Winner() == 0 {
		g.BeginTurn(recvData)
		turnData := <-recvData

		if m.State.Round == 0 {
			m.InitState(g.SCG.Rand(scnet.RNGShared), g, func(ac *match.ActiveCard) {})

			m.Start = time.Now()
		}

		m.State.Round++

		m.ShuffleAndDraw(g.SCG, false)

		if err := m.SyncInHand(g.ExchangeInHand, turnData); err != nil {
			return nil, fmt.Errorf("minimal: failed to sync hand state: %w", err)
		}

		m.RecomputePassives(turnData)

		turnData.Ready[m.Perspective-1] = n.PlayRound(m)

		for i, c := range m.State.Sides[m.Perspective-1].Hand {
			if turnData.Ready[m.Perspective-1]&(1<<i) != 0 {
				turnData.Played[m.Perspective-1] = append(turnData.Played[m.Perspective-1], c.Def.ID)
			}
		}

		if m.SpecialFlags[card.SpecialRevealSelection] {
			g.SendPreviewReady(turnData.Ready[m.Perspective-1])
		}

		g.SendReady(turnData)
		g.WaitRecvReady(turnData)

		if err := m.BeginTurn(turnData); err != nil {
			return nil, fmt.Errorf("minimal: desync at round %d: %w", m.State.Round, err)
		}

		rng := g.SCG.Rand(scnet.RNGShared)

		q := m.ProcessQueuedEffect(rng, g)
		for len(q) != 0 {
			q = m.ProcessQueuedEffect(rng, g)

			if err := ctx.Err(); err != nil {
				return nil, fmt.Errorf("minimal: timed out: %w", err)
			}
		}

		n.AfterRound(m)
	}

	if m.Winner() == m.Perspective {
		m.Wins++
	} else {
		m.Losses++
	}

	rec, err := m.Finalize(g)
	if err != nil {
		return rec, fmt.Errorf("minimal: verifying match: %w", err)
	}

	return rec, nil
}
