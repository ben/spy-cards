package minimal_test

import (
	"context"
	"testing"

	"git.lubar.me/ben/spy-cards/card"
	"git.lubar.me/ben/spy-cards/internal"
	"git.lubar.me/ben/spy-cards/match"
	"git.lubar.me/ben/spy-cards/match/minimal"
	"git.lubar.me/ben/spy-cards/match/npc"
	"git.lubar.me/ben/spy-cards/scnet"
)

func TestMinimal(t *testing.T) {
	t.Parallel()

	ctx := context.Background()
	sc1, sc2 := scnet.MockSignal(ctx)

	gc1, err := scnet.NewGame(ctx)
	if err != nil {
		t.Fatal(err)
	}

	gc2, err := scnet.NewGame(ctx)
	if err != nil {
		t.Fatal(err)
	}

	err = sc1.Init(scnet.Player1, "")
	if err != nil {
		t.Fatal(err)
	}

	err = sc2.Init(scnet.Player2, "")
	if err != nil {
		t.Fatal(err)
	}

	sid := sc1.WaitSession()
	if sid == "" {
		t.Fatal("no session ID")
	}

	err = sc2.SetSession(sid)
	if err != nil {
		t.Fatal(err)
	}

	var m1, m2 match.Match

	m1.Perspective = 1
	m1.UnknownDeck = 2
	m2.Perspective = 2
	m2.UnknownDeck = 1

	m1.Init = &match.Init{Version: internal.Version}
	m2.Init = &match.Init{Version: internal.Version}

	m1.Set, err = m1.Init.Cards(ctx)
	if err != nil {
		t.Fatal(err)
	}

	m2.Set, err = m2.Init.Cards(ctx)
	if err != nil {
		t.Fatal(err)
	}

	gc1.Cards = m1.Set
	gc2.Cards = m2.Set

	gc1.UseSignal(sc1, &m1.Init.Mode)
	gc2.UseSignal(sc2, &m2.Init.Mode)

	genericNPC, err := npc.Get("")
	if err != nil {
		t.Fatal(err)
	}

	type result struct {
		rec *card.Recording
		err error
	}

	p1 := make(chan result, 1)

	go func() {
		rec, err := minimal.PlayMatch(context.Background(), &m1, gc1, genericNPC)
		p1 <- result{rec, err}
	}()

	rec2, err2 := minimal.PlayMatch(ctx, &m2, gc2, genericNPC)

	p1Result := <-p1
	rec1, err1 := p1Result.rec, p1Result.err

	if err1 != nil {
		t.Error(err1)
	}

	if err2 != nil {
		t.Error(err2)
	}

	_, _ = rec1, rec2
}
