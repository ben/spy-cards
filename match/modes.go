package match

import (
	"log"
	"strings"

	"git.lubar.me/ben/spy-cards/card"
	"git.lubar.me/ben/spy-cards/gfx/sprites"
)

type AvailableModes struct {
	Categories []ModeCategory  `json:"c"`
	Modes      []AvailableMode `json:"m"`
}

type ModeCategory struct {
	ID          int64  `json:"i"`
	Name        string `json:"t"`
	Description string `json:"d"`
	Color       int32  `json:"c"`
}

type AvailableMode struct {
	Client        string `json:"s"`
	Name          string `json:"t"`
	Revisions     int    `json:"r"`
	ModeData      []byte `json:"m"`
	FirstCard     []byte `json:"fc"`
	CategoryColor int32  `json:"c"`
	CategoryID    int64  `json:"ci"`
}

var allowMultipleParagraphs = map[string]bool{
	"shapes": true,
	"jimmy":  true,
}

func MakeModesPseudoSet(set *card.Set, modes *AvailableModes, each func(*card.Def, *card.Metadata, *AvailableMode)) {
	*set = card.Set{Mode: &card.GameMode{Fields: []card.Field{
		&card.BannedCards{Flags: card.BannedCardTypeBanned},
		&card.BannedCards{Flags: card.BannedCardTypeUnpickable},
	}}}

	var versionPortrait [card.LatestVersion + 1]uint8 = [...]uint8{
		card.BugFables105: 140,
		card.BugFables11:  230,
		card.BugFables111: 106,
		card.BugFables121: 220,
	}

	for v := card.LatestVersion; ; v-- {
		desc := "Use an older revision of Spy Cards."
		if v == card.LatestVersion {
			desc = "Play with the latest revision of Spy Cards!"
		}

		c := &card.Def{
			ID:       card.ID(v) + 4,
			Name:     v.String(),
			Rank:     card.Attacker,
			Portrait: versionPortrait[v],
			Effects: []*card.EffectDef{
				{
					Type:  card.FlavorText,
					Flags: card.FlagFlavorTextCustomColor,
					Text:  desc,
				},
			},
		}

		set.Cards = append(set.Cards, c)

		each(c, nil, nil)

		if v == 0 {
			break
		}
	}

	{
		c := &card.Def{
			ID:       0,
			Name:     "Custom Cards",
			Rank:     card.MiniBoss,
			Portrait: 165,
			Effects: []*card.EffectDef{
				{
					Type:  card.FlavorText,
					Flags: card.FlagFlavorTextCustomColor,
					Text:  "Create your own cards!",
				},
			},
		}

		set.Cards = append(set.Cards, c)

		each(c, nil, nil)
	}

	{
		c := &card.Def{
			ID:       1,
			Name:     "Settings",
			Rank:     card.Token,
			Portrait: 36,
			Effects: []*card.EffectDef{
				{
					Type:  card.FlavorText,
					Flags: card.FlagFlavorTextCustomColor,
					Text:  "Modify preferences!",
				},
			},
		}

		set.Cards = append(set.Cards, c)

		each(c, nil, nil)
	}

	{
		c := &card.Def{
			ID:       2,
			Name:     "Termacade",
			Rank:     card.Token,
			Portrait: 224,
			Effects: []*card.EffectDef{
				{
					Type:  card.FlavorText,
					Flags: card.FlagFlavorTextCustomColor,
					Text:  "Play arcade games!",
				},
			},
		}

		set.Cards = append(set.Cards, c)

		each(c, nil, nil)
	}

	{
		c := &card.Def{
			ID:       3,
			Name:     "Aphid Festival",
			Rank:     card.Token,
			Portrait: 141,
			Effects: []*card.EffectDef{
				{
					Type:  card.FlavorText,
					Flags: card.FlagFlavorTextCustomColor,
					Text:  "Raise aphids!",
				},
			},
		}

		set.Cards = append(set.Cards, c)

		each(c, nil, nil)
	}

	for j, m := range modes.Modes {
		var mode card.GameMode

		if err := mode.UnmarshalBinary(m.ModeData); err != nil {
			log.Printf("WARNING: failed to decode game mode: %+v", err)
		}

		var firstCard card.Def

		if len(m.FirstCard) != 0 {
			if err := firstCard.UnmarshalBinary(m.FirstCard); err != nil {
				log.Printf("WARNING: failed to decode first card of mode: %+v", err)
			}
		}

		descColor := sprites.Black
		md, _ := mode.Get(card.FieldMetadata).(*card.Metadata)

		if md == nil {
			descColor = sprites.Red
			md = &card.Metadata{Description: "Missing Data", Portrait: card.PortraitCustomEmbedded}
		} else if len(m.FirstCard) != 0 && m.FirstCard[0] < 5 {
			md.Portrait = firstCard.Portrait
			md.CustomPortrait = firstCard.CustomPortrait

			if md.Portrait == 174 {
				// avoid final boss portrait
				md.Portrait = 107
			}
		}

		tribes := []card.TribeDef{
			{
				Tribe:      card.TribeWideCustom,
				CustomName: md.Author,
				Red:        uint8(m.CategoryColor >> 16),
				Green:      uint8(m.CategoryColor >> 8),
				Blue:       uint8(m.CategoryColor),
			},
		}
		if md.Author == "" {
			tribes = nil
		}

		description := md.Description
		if i := strings.Index(description, "\n\n"); i != -1 && !allowMultipleParagraphs[m.Client] {
			description = description[:i]
		}

		c := &card.Def{
			ID:             card.ID(128 + j),
			Name:           m.Name,
			TP:             int64(m.Revisions),
			Rank:           card.Boss,
			Portrait:       md.Portrait,
			CustomPortrait: md.CustomPortrait,
			Tribes:         tribes,
			Effects: []*card.EffectDef{
				{
					Type:   card.FlavorText,
					Flags:  card.FlagFlavorTextCustomColor,
					Text:   description,
					Amount: int64(descColor.R) | int64(descColor.G)<<8 | int64(descColor.B)<<16,
				},
			},
		}

		set.Cards = append(set.Cards, c)

		am := m
		each(c, md, &am)
	}
}

func CardsForHome(s *card.Set) []card.ID {
	cards := make([]card.ID, 0, 92+len(s.Cards))

	vanillaBanned := false
	hiddenCard := make(map[card.ID]bool)
	mode, _ := s.Mode.Variant(s.Variant)

	for _, f := range mode.GetAll(card.FieldBannedCards) {
		bc := f.(*card.BannedCards)
		if len(bc.Cards) == 0 && bc.Flags&card.BannedCardTypeMask == card.BannedCardTypeBanned {
			vanillaBanned = true
		}

		if bc.Flags&card.BannedCardTypeMask == card.BannedCardTypeHiddenHome {
			for _, id := range bc.Cards {
				hiddenCard[id] = true
			}

			if len(bc.Cards) == 0 {
				for i := card.ID(0); i < 128; i++ {
					hiddenCard[i] = true
				}
			}
		}
	}

	if !vanillaBanned {
		for _, back := range []card.Rank{card.Boss, card.MiniBoss, card.Enemy} {
			for _, id := range card.VanillaOrder(back) {
				found := false

				if hiddenCard[id] {
					continue
				}

				for _, c := range s.Cards {
					if c.ID == id {
						found = true

						break
					}
				}

				if !found {
					def := s.Card(id)
					if def == nil {
						continue
					}

					cards = append(cards, id)
				}
			}
		}
	}

	for _, def := range s.Cards {
		if hiddenCard[def.ID] {
			continue
		}

		cards = append(cards, def.ID)
	}

	return cards
}
