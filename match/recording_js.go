//go:build js && wasm
// +build js,wasm

package match

import (
	"runtime"
	"syscall/js"
	"time"
)

func (r *Recording) yielder() (func(), func()) {
	ric := js.Global().Get("requestIdleCallback")
	if !ric.Truthy() {
		return func() {
			time.Sleep(time.Millisecond)
		}, func() {}
	}

	var deadline js.Value

	ignoreDeadline := 0

	ch := make(chan struct{}, 1)
	cb := js.FuncOf(func(_ js.Value, args []js.Value) interface{} {
		deadline = args[0]

		ch <- struct{}{}

		return js.Undefined()
	})

	return func() {
		if ignoreDeadline > 0 {
			ignoreDeadline--

			runtime.Gosched()

			return
		}

		if deadline.Truthy() && deadline.Call("timeRemaining").Float() > 0 {
			runtime.Gosched()

			return
		}

		if deadline.Truthy() && deadline.Get("didTimeout").Bool() {
			// browser doesn't think it can safely run the recording processor
			// as a *background* task. handle ~100 effects per second, then
			// return control to the render loop.
			deadline = js.Undefined()
			ignoreDeadline = 100

			runtime.Gosched()

			return
		}

		ric.Invoke(cb, map[string]interface{}{
			"timeout": 1000,
		})
		<-ch
	}, cb.Release
}
