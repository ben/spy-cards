//go:build !headless
// +build !headless

package main

import (
	"context"
	"runtime"
	"strings"

	"git.lubar.me/ben/spy-cards/audio"
	"git.lubar.me/ben/spy-cards/gfx"
	"git.lubar.me/ben/spy-cards/gfx/sprites"
	"git.lubar.me/ben/spy-cards/internal/router"
	"git.lubar.me/ben/spy-cards/room"
)

type preloadSet struct {
	Textures []*gfx.AssetTexture
	Sprites  []*sprites.Sprite
	Audio    []*audio.Sound
	Stages   []string
	Page     router.Page
}

func (s preloadSet) do(ctx context.Context, wait bool) <-chan struct{} {
	sprites.Blank.StartPreload()

	for _, tex := range s.Textures {
		tex.StartPreload()
	}

	for _, sprite := range s.Sprites {
		sprite.StartPreload()
	}

	for _, sound := range s.Audio {
		sound.StartPreload()
	}

	for _, stage := range s.Stages {
		go func(name string) {
			_ = room.PreloadStage(ctx, name, nil)
		}(stage)
	}

	ch := make(chan struct{})

	if !wait {
		close(ch)
	} else {
		go func() {
			// don't care whether it succeeded, just that it's not in-progress
			_ = sprites.Blank.Preload()
			_ = sprites.PreloadText()

			for _, tex := range s.Textures {
				_ = tex.Preload()
			}

			for _, sprite := range s.Sprites {
				_ = sprite.Preload()
			}

			for _, sound := range s.Audio {
				_ = sound.Preload()
			}

			for _, stage := range s.Stages {
				_ = room.PreloadStage(ctx, stage, nil)
			}

			runtime.GC()

			close(ch)
		}()
	}

	runtime.Gosched()

	return ch
}

func preloadTermacade() preloadSet {
	return preloadSet{
		Sprites: []*sprites.Sprite{
			sprites.GamepadButton[0][0],
			sprites.FJBack,
			sprites.NeonFJBee,
			sprites.AttractMK,
			sprites.ParticleGrassPlaceholder,
			sprites.ParticleSmoke,
			sprites.ParticleStar,
			sprites.AttractFJ,
		},
		Audio: []*audio.Sound{
			audio.Buzzer,
			audio.Confirm,
			audio.Confirm1,
			audio.TermiteLoop,
			audio.MiteKnight,
			audio.MiteKnightIntro,
			audio.PeacockSpiderNPCSummonSuccess,
			audio.Shot2,
			audio.MKDeath,
			audio.MKGameOver,
			audio.MKHit,
			audio.MKHit2,
			audio.MKKey,
			audio.MKOpen,
			audio.MKPotion,
			audio.MKStairs,
			audio.MKWalk,
			audio.FlyingBee,
			audio.FBCountdown,
			audio.FBDeath,
			audio.FBFlower,
			audio.FBGameOver,
			audio.FBPoint,
			audio.FBStart,
			audio.FunnyStep,
		},
	}
}

func preloadELF() preloadSet {
	return preloadSet{
		Sprites: []*sprites.Sprite{
			sprites.GamepadButton[0][0],
			sprites.Skybox1,
			sprites.ELFTanjerin[0],
			sprites.ELFMaki[0],
			sprites.Tanjerin1,
			sprites.Maki1,
		},
		Audio: []*audio.Sound{
			audio.Battle4,
			audio.PageFlip,
			audio.Damage0,
		},
		Stages: []string{
			"img/room/bugaria-main.stage",
		},
	}
}

func preloadCard(info *router.PageInfo) preloadSet {
	defaultMusic := audio.Miniboss
	if info.Page == router.PageCardsJoin && strings.HasPrefix(info.Code, "CSTM") {
		defaultMusic = audio.Bounty
	}

	return preloadSet{
		Sprites: []*sprites.Sprite{
			sprites.GamepadButton[0][0],
			sprites.CardFront,
			sprites.CardFrontWindow,
			sprites.Portraits[0],
			sprites.Tanjerin1,
			sprites.Cerise1,
			sprites.Audience[0][0],
			sprites.Audience[0][4],
			sprites.HP,
			sprites.CardBattle,
		},
		Audio: []*audio.Sound{
			defaultMusic,
			audio.Confirm1,
			audio.Inside2,
			audio.BattleStart0,
			audio.Confirm,
			audio.Buzzer,
			audio.PageFlip,
			audio.Coin,
			audio.AtkSuccess,
			audio.AtkFail,
			audio.CardSound2,
			audio.Lazer,
			audio.CrowdClap,
			audio.CrowdGasp,
			audio.CrowdCheer2,
			audio.Toss11,
			audio.Damage0,
			audio.Death3,
			audio.Fail,
			audio.Heal,
			audio.StatUp,
			audio.StatDown,
		},
		Stages: []string{
			"img/room/stage0001.stage",
			"img/room/stage0002.stage",
			"img/room/stage0003.stage",
			"img/room/stage0004.stage",
			"img/room/mine.stage",
		},
	}
}

func preloadFestival() preloadSet {
	return preloadSet{
		Sprites: []*sprites.Sprite{
			sprites.AphidBody[0],
			sprites.HP,
			sprites.Description,
		},
		Audio: []*audio.Sound{
			audio.Battle0,
			audio.Chef1,
			audio.Field1,
			audio.Inn,
			audio.AtkSuccess,
			audio.Kut1,
			audio.Kut2,
			audio.Damage1,
			audio.Damage2,
			audio.FunnyStep,
		},
		Stages: []string{
			"img/room/festival.stage",
		},
	}
}

func preloadTables() preloadSet {
	return preloadSet{
		Textures: []*gfx.AssetTexture{
			sprites.Main1,
			sprites.Main2,
			sprites.TreeTex,
			sprites.Water1,
			sprites.Water3,
			sprites.Hue,
			sprites.Grass,
		},
		Sprites: []*sprites.Sprite{
			sprites.WideArrow,
			sprites.TablesMicrowave,
			sprites.Skybox1,
			sprites.Skybox4,
		},
		Audio: []*audio.Sound{
			audio.Confirm,
			audio.Confirm1,
			audio.Buzzer,
		},
	}
}
