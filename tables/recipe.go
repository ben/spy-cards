package tables

type RecipeData struct {
	Result  ItemID
	Method1 CookType
}

var (
	MysteryBerry1 = [11]ItemID{
		Mistake,
		Mistake,
		Mistake,
		Mistake,
		BigMistake,
		BigMistake,
		RoastedBerries,
		RoastedBerries,
		BerryJuice,
		SucculentPlatter,
		BerrySmoothie,
	}
	MysteryBerry2 = [13]ItemID{
		Mistake,
		BigMistake,
		BigMistake,
		SucculentPlatter,
		SucculentPlatter,
		SucculentPlatter,
		BerrySmoothie,
		BerrySmoothie,
		RoastedBerries,
		BerryJuice,
		TangyJam,
		SpicyBomb,
		BurlyBomb,
	}
	Recipe1 = map[ItemID]RecipeData{
		AgaricShroom: {
			Result:  CookedShroom,
			Method1: CookWiggle,
		},
		AphidEgg: {
			Result:  FriedEgg,
			Method1: CookTimed,
		},
		BagOfFlour: {
			Result:  DryBread,
			Method1: CookTimed,
		},
		BigMistake: {
			Result:  BigMistake,
			Method1: CookRandom,
		},
		BurlyBerry: {
			Result:  RoastedBerries,
			Method1: CookChop,
		},
		ClearWater: {
			Result:  ClearWater,
			Method1: CookStir,
		},
		CrunchyLeaf: {
			Result:  LeafSalad,
			Method1: CookChop,
		},
		DangerShroom: {
			Result:  CookedDanger,
			Method1: CookWiggle,
		},
		DangerSpud: {
			Result:  BakedYam,
			Method1: CookTimed,
		},
		DarkCherries: {
			Result:  SucculentPlatter,
			Method1: CookChop,
		},
		GlazedHoney: {
			Result:  Abomihoney,
			Method1: CookStir,
		},
		HardSeed: {
			Result:  RoastedBerries,
			Method1: CookChop,
		},
		HoneyDrop: {
			Result:  GlazedHoney,
			Method1: CookStir,
		},
		HustleBerry: {
			Result:  RoastedBerries,
			Method1: CookChop,
		},
		MagicIce: {
			Result:  ShavedIce,
			Method1: CookChop,
		},
		Mistake: {
			Result:  BigMistake,
			Method1: CookRandom,
		},
		Mushroom: {
			Result:  CookedShroom,
			Method1: CookWiggle,
		},
		ShockBerry: {
			Result:  RoastedBerries,
			Method1: CookChop,
		},
		SpicyBerry: {
			Result:  RoastedBerries,
			Method1: CookChop,
		},
		Squash: {
			Result:  SquashPuree,
			Method1: CookStir,
		},
		SucculentBerry: {
			Result:  SucculentPlatter,
			Method1: CookChop,
		},
		TangyBerry: {
			Result:  TangyCarpaccio,
			Method1: CookChop,
		},
	}
	Recipe2 = map[[2]ItemID]RecipeData{
		{Abomihoney, Abomihoney}: {
			Result:  BigMistake,
			Method1: CookStir,
		},
		{Abomihoney, BurlyBomb}: {
			Result:  Abombination,
			Method1: CookRandom,
		},
		{Abomihoney, FrostBomb}: {
			Result:  Abombination,
			Method1: CookRandom,
		},
		{Abomihoney, NumbBomb}: {
			Result:  Abombination,
			Method1: CookRandom,
		},
		{Abomihoney, PoisonBomb}: {
			Result:  Abombination,
			Method1: CookRandom,
		},
		{Abomihoney, SleepBomb}: {
			Result:  Abombination,
			Method1: CookRandom,
		},
		{Abomihoney, SpicyBomb}: {
			Result:  Abombination,
			Method1: CookRandom,
		},
		{AgaricShroom, BagOfFlour}: {
			Result:  ShockCandy,
			Method1: CookTimed,
		},
		{AgaricShroom, BagOfFlour}: {
			Result:  ShockCandy,
			Method1: CookTimed,
		},
		{AgaricShroom, GlazedHoney}: {
			Result:  SweetShroom,
			Method1: CookWiggle,
		},
		{AgaricShroom, LeafSalad}: {
			Result:  MushroomSalad,
			Method1: CookChop,
		},
		{AgaricShroom, SpicyBomb}: {
			Result:  NumbBomb,
			Method1: CookRandom,
		},
		{AphidDew, BagOfFlour}: {
			Result:  SweetPudding,
			Method1: CookStir,
		},
		{AphidDew, FriedEgg}: {
			Result:  AphidShake,
			Method1: CookStir,
		},
		{AphidDew, GlazedHoney}: {
			Result:  SweetDew,
			Method1: CookStir,
		},
		{AphidDew, MagicIce}: {
			Result:  IceCream,
			Method1: CookStir,
		},
		{AphidDew, ShavedIce}: {
			Result:  IceCream,
			Method1: CookStir,
		},
		{AphidEgg, AphidDew}: {
			Result:  AphidShake,
			Method1: CookStir,
		},
		{AphidEgg, BagOfFlour}: {
			Result:  SweetPudding,
			Method1: CookStir,
		},
		{AphidEgg, CookedShroom}: {
			Result:  HeartyBreakfast,
			Method1: CookChop,
		},
		{AphidEgg, LeafSalad}: {
			Result:  LeafOmelet,
			Method1: CookWiggle,
		},
		{AphidEgg, Mushroom}: {
			Result:  HeartyBreakfast,
			Method1: CookChop,
		},
		{BagOfFlour, Abomihoney}: {
			Result:  HoneyPancakes,
			Method1: CookTimed,
		},
		{BagOfFlour, BakedYam}: {
			Result:  YamBread,
			Method1: CookTimed,
		},
		{BagOfFlour, BurlyBerry}: {
			Result:  BurlyCandy,
			Method1: CookTimed,
		},
		{BagOfFlour, DangerSpud}: {
			Result:  PoisonCake,
			Method1: CookTimed,
		},
		{BagOfFlour, DangerSpud}: {
			Result:  YamBread,
			Method1: CookTimed,
		},
		{BagOfFlour, DarkCherries}: {
			Result:  CherryPie,
			Method1: CookTimed,
		},
		{BagOfFlour, FriedEgg}: {
			Result:  SweetPudding,
			Method1: CookStir,
		},
		{BagOfFlour, GlazedHoney}: {
			Result:  CrisbeeDonut,
			Method1: CookTimed,
		},
		{BagOfFlour, HustleBerry}: {
			Result:  HustleCandy,
			Method1: CookTimed,
		},
		{BagOfFlour, IceCream}: {
			Result:  FrostPie,
			Method1: CookTimed,
		},
		{BagOfFlour, LeafSalad}: {
			Result:  LeafCroissant,
			Method1: CookTimed,
		},
		{BagOfFlour, MagicIce}: {
			Result:  FrostPie,
			Method1: CookTimed,
		},
		{BagOfFlour, MushroomSkewer}: {
			Result:  MushroomGummies,
			Method1: CookTimed,
		},
		{BagOfFlour, MysteryBerry}: {
			Result:  NuttyCake,
			Method1: CookTimed,
		},
		{BagOfFlour, NumbnailDart}: {
			Result:  DrowsyCake,
			Method1: CookTimed,
		},
		{BagOfFlour, PoisonDart}: {
			Result:  PoisonCake,
			Method1: CookTimed,
		},
		{BagOfFlour, RoastedBerries}: {
			Result:  NuttyCake,
			Method1: CookTimed,
		},
		{BagOfFlour, ShavedIce}: {
			Result:  FrostPie,
			Method1: CookTimed,
		},
		{BagOfFlour, ShockBerry}: {
			Result:  ShockCandy,
			Method1: CookTimed,
		},
		{BagOfFlour, SpicyBerry}: {
			Result:  SpicyCandy,
			Method1: CookTimed,
		},
		{BagOfFlour, Squash}: {
			Result:  SquashCandy,
			Method1: CookTimed,
		},
		{BagOfFlour, SquashPuree}: {
			Result:  SquashCandy,
			Method1: CookTimed,
		},
		{BagOfFlour, SucculentBerry}: {
			Result:  SucculentCookies,
			Method1: CookTimed,
		},
		{BagOfFlour, SucculentPlatter}: {
			Result:  SucculentCookies,
			Method1: CookTimed,
		},
		{BagOfFlour, TangyBerry}: {
			Result:  TangyPie,
			Method1: CookTimed,
		},
		{BagOfFlour, TangyJam}: {
			Result:  TangyPie,
			Method1: CookTimed,
		},
		{BerryJuice, TangyJam}: {
			Result:  TangyJuice,
			Method1: CookStir,
		},
		{BurlyBerry, BakedYam}: {
			Result:  BurlyChips,
			Method1: CookWiggle,
		},
		{BurlyBerry, PlainTea}: {
			Result:  BurlyTea,
			Method1: CookStir,
		},
		{BurlyBerry, SquashCandy}: {
			Result:  PlumplingPie,
			Method1: CookTimed,
		},
		{BurlyBerry, SquashPuree}: {
			Result:  PlumplingPie,
			Method1: CookTimed,
		},
		{BurlyBerry, SquashTart}: {
			Result:  PlumplingPie,
			Method1: CookTimed,
		},
		{ClearWater, Abombination}: {
			Result:  ClearBomb,
			Method1: CookRandom,
		},
		{ClearWater, BurlyBerry}: {
			Result:  BerryJuice,
			Method1: CookStir,
		},
		{ClearWater, BurlyBomb}: {
			Result:  ClearBomb,
			Method1: CookRandom,
		},
		{ClearWater, CherryBombs}: {
			Result:  ClearBomb,
			Method1: CookRandom,
		},
		{ClearWater, ClearWater}: {
			Result:  ClearWater,
			Method1: CookStir,
		},
		{ClearWater, FrostBomb}: {
			Result:  ClearBomb,
			Method1: CookRandom,
		},
		{ClearWater, HustleBerry}: {
			Result:  HotDrink,
			Method1: CookStir,
		},
		{ClearWater, LeafSalad}: {
			Result:  PlainTea,
			Method1: CookStir,
		},
		{ClearWater, NumbBomb}: {
			Result:  ClearBomb,
			Method1: CookRandom,
		},
		{ClearWater, PoisonBomb}: {
			Result:  ClearBomb,
			Method1: CookRandom,
		},
		{ClearWater, ShockBerry}: {
			Result:  BerryJuice,
			Method1: CookStir,
		},
		{ClearWater, SleepBomb}: {
			Result:  ClearBomb,
			Method1: CookRandom,
		},
		{ClearWater, SpicyBerry}: {
			Result:  BerryJuice,
			Method1: CookStir,
		},
		{ClearWater, SpicyBomb}: {
			Result:  ClearBomb,
			Method1: CookRandom,
		},
		{ClearWater, TangyBerry}: {
			Result:  TangyJuice,
			Method1: CookStir,
		},
		{CookedDanger, SpicyBomb}: {
			Result:  PoisonBomb,
			Method1: CookRandom,
		},
		{CookedDanger, SweetShroom}: {
			Result:  MushroomSkewer,
			Method1: CookChop,
		},
		{CookedShroom, CookedDanger}: {
			Result:  MushroomSkewer,
			Method1: CookChop,
		},
		{CookedShroom, FriedEgg}: {
			Result:  HeartyBreakfast,
			Method1: CookChop,
		},
		{CookedShroom, SweetDanger}: {
			Result:  MushroomSkewer,
			Method1: CookChop,
		},
		{CrunchyLeaf, AgaricShroom}: {
			Result:  MushroomSalad,
			Method1: CookChop,
		},
		{CrunchyLeaf, AphidEgg}: {
			Result:  LeafOmelet,
			Method1: CookWiggle,
		},
		{CrunchyLeaf, BagOfFlour}: {
			Result:  LeafCroissant,
			Method1: CookTimed,
		},
		{CrunchyLeaf, ClearWater}: {
			Result:  PlainTea,
			Method1: CookStir,
		},
		{CrunchyLeaf, CookedShroom}: {
			Result:  MushroomSalad,
			Method1: CookChop,
		},
		{CrunchyLeaf, CrunchyLeaf}: {
			Result:  LeafSalad,
			Method1: CookChop,
		},
		{CrunchyLeaf, CrunchyLeaf}: {
			Result:  LeafSalad,
			Method1: CookChop,
		},
		{CrunchyLeaf, DryBread}: {
			Result:  LeafCroissant,
			Method1: CookTimed,
		},
		{CrunchyLeaf, FriedEgg}: {
			Result:  LeafOmelet,
			Method1: CookWiggle,
		},
		{CrunchyLeaf, GlazedHoney}: {
			Result:  HoneydLeaf,
			Method1: CookWiggle,
		},
		{CrunchyLeaf, HoneyDrop}: {
			Result:  HoneydLeaf,
			Method1: CookWiggle,
		},
		{CrunchyLeaf, MagicIce}: {
			Result:  ColdSalad,
			Method1: CookChop,
		},
		{CrunchyLeaf, Mushroom}: {
			Result:  MushroomSalad,
			Method1: CookChop,
		},
		{CrunchyLeaf, ShavedIce}: {
			Result:  ColdSalad,
			Method1: CookChop,
		},
		{DangerDish, BurlyBomb}: {
			Result:  PoisonBomb,
			Method1: CookRandom,
		},
		{DangerDish, SpicyBomb}: {
			Result:  PoisonBomb,
			Method1: CookRandom,
		},
		{DangerShroom, AgaricShroom}: {
			Result:  MushroomSkewer,
			Method1: CookChop,
		},
		{DangerShroom, BagOfFlour}: {
			Result:  PoisonCake,
			Method1: CookTimed,
		},
		{DangerShroom, BurlyBomb}: {
			Result:  PoisonBomb,
			Method1: CookRandom,
		},
		{DangerShroom, CookedShroom}: {
			Result:  MushroomSkewer,
			Method1: CookChop,
		},
		{DangerShroom, DangerSpud}: {
			Result:  DangerDish,
			Method1: CookChop,
		},
		{DangerShroom, GlazedHoney}: {
			Result:  SweetDanger,
			Method1: CookWiggle,
		},
		{DangerShroom, PoisonCake}: {
			Result:  DangerDish,
			Method1: CookChop,
		},
		{DangerShroom, PoisonDart}: {
			Result:  DangerDish,
			Method1: CookChop,
		},
		{DangerShroom, SpicyBomb}: {
			Result:  PoisonBomb,
			Method1: CookRandom,
		},
		{DangerShroom, SweetShroom}: {
			Result:  MushroomSkewer,
			Method1: CookChop,
		},
		{DangerSpud, BurlyBerry}: {
			Result:  BurlyChips,
			Method1: CookWiggle,
		},
		{DangerSpud, BurlyBomb}: {
			Result:  PoisonBomb,
			Method1: CookRandom,
		},
		{DangerSpud, PoisonCake}: {
			Result:  DangerDish,
			Method1: CookChop,
		},
		{DangerSpud, PoisonDart}: {
			Result:  DangerDish,
			Method1: CookChop,
		},
		{DangerSpud, SpicyBerry}: {
			Result:  SpicyFries,
			Method1: CookWiggle,
		},
		{DangerSpud, SpicyBomb}: {
			Result:  PoisonBomb,
			Method1: CookRandom,
		},
		{DangerSpud, SweetDanger}: {
			Result:  DangerDish,
			Method1: CookChop,
		},
		{DarkCherries, BurlyBomb}: {
			Result:  CherryBombs,
			Method1: CookRandom,
		},
		{DarkCherries, MagicSeed}: {
			Result:  MiracleShake,
			Method1: CookStir,
		},
		{DarkCherries, SpicyBomb}: {
			Result:  CherryBombs,
			Method1: CookRandom,
		},
		{DrowsyCake, BurlyBomb}: {
			Result:  SleepBomb,
			Method1: CookRandom,
		},
		{DrowsyCake, SpicyBomb}: {
			Result:  SleepBomb,
			Method1: CookRandom,
		},
		{GlazedHoney, GlazedHoney}: {
			Result:  Abomihoney,
			Method1: CookStir,
		},
		{GlazedHoney, IceCream}: {
			Result:  HoneyIceCream,
			Method1: CookStir,
		},
		{GlazedHoney, ShavedIce}: {
			Result:  HoneyIceCream,
			Method1: CookStir,
		},
		{HardSeed, BagOfFlour}: {
			Result:  NuttyCake,
			Method1: CookTimed,
		},
		{HardSeed, BurlyBerry}: {
			Result:  BurlyBomb,
			Method1: CookRandom,
		},
		{HardSeed, ClearWater}: {
			Result:  BerryJuice,
			Method1: CookStir,
		},
		{HardSeed, HardSeed}: {
			Result:  RoastedBerries,
			Method1: CookChop,
		},
		{HardSeed, SpicyBerry}: {
			Result:  SpicyBomb,
			Method1: CookRandom,
		},
		{HoneyDrop, AgaricShroom}: {
			Result:  SweetShroom,
			Method1: CookWiggle,
		},
		{HoneyDrop, AphidDew}: {
			Result:  SweetDew,
			Method1: CookStir,
		},
		{HoneyDrop, BagOfFlour}: {
			Result:  HoneyPancakes,
			Method1: CookTimed,
		},
		{HoneyDrop, DangerShroom}: {
			Result:  SweetDanger,
			Method1: CookWiggle,
		},
		{HoneyDrop, GlazedHoney}: {
			Result:  Abomihoney,
			Method1: CookStir,
		},
		{HoneyDrop, HoneyDrop}: {
			Result:  Abomihoney,
			Method1: CookStir,
		},
		{HoneyDrop, IceCream}: {
			Result:  HoneyIceCream,
			Method1: CookStir,
		},
		{HoneyDrop, LeafSalad}: {
			Result:  HoneydLeaf,
			Method1: CookWiggle,
		},
		{HoneyDrop, MagicIce}: {
			Result:  HoneyIceCream,
			Method1: CookStir,
		},
		{HoneyDrop, Mushroom}: {
			Result:  SweetShroom,
			Method1: CookWiggle,
		},
		{HoneyDrop, ShavedIce}: {
			Result:  HoneyIceCream,
			Method1: CookStir,
		},
		{HoneyDrop, TangyBerry}: {
			Result:  TangyJam,
			Method1: CookStir,
		},
		{LeafSalad, CookedShroom}: {
			Result:  MushroomSalad,
			Method1: CookChop,
		},
		{LeafSalad, DryBread}: {
			Result:  LeafCroissant,
			Method1: CookTimed,
		},
		{LeafSalad, FriedEgg}: {
			Result:  LeafOmelet,
			Method1: CookWiggle,
		},
		{LeafSalad, GlazedHoney}: {
			Result:  HoneydLeaf,
			Method1: CookWiggle,
		},
		{LeafSalad, ShavedIce}: {
			Result:  ColdSalad,
			Method1: CookChop,
		},
		{MagicIce, BurlyBomb}: {
			Result:  FrostBomb,
			Method1: CookRandom,
		},
		{MagicIce, LeafSalad}: {
			Result:  ColdSalad,
			Method1: CookChop,
		},
		{MagicIce, MagicIce}: {
			Result:  ShavedIce,
			Method1: CookChop,
		},
		{MagicIce, SpicyBomb}: {
			Result:  FrostBomb,
			Method1: CookRandom,
		},
		{MagicIce, SweetDew}: {
			Result:  HoneyIceCream,
			Method1: CookStir,
		},
		{Mistake, GlazedHoney}: {
			Result:  Abomihoney,
			Method1: CookStir,
		},
		{Mistake, HoneyDrop}: {
			Result:  Abomihoney,
			Method1: CookStir,
		},
		{Mistake, Mistake}: {
			Result:  BigMistake,
			Method1: CookRandom,
		},
		{Mushroom, AgaricShroom}: {
			Result:  MushroomSkewer,
			Method1: CookChop,
		},
		{Mushroom, BagOfFlour}: {
			Result:  MushroomGummies,
			Method1: CookWiggle,
		},
		{Mushroom, CookedDanger}: {
			Result:  MushroomSkewer,
			Method1: CookChop,
		},
		{Mushroom, DangerShroom}: {
			Result:  MushroomSkewer,
			Method1: CookChop,
		},
		{Mushroom, FriedEgg}: {
			Result:  HeartyBreakfast,
			Method1: CookChop,
		},
		{Mushroom, GlazedHoney}: {
			Result:  SweetShroom,
			Method1: CookWiggle,
		},
		{Mushroom, LeafSalad}: {
			Result:  MushroomSalad,
			Method1: CookChop,
		},
		{Mushroom, SweetDanger}: {
			Result:  MushroomSkewer,
			Method1: CookChop,
		},
		{NumbnailDart, BurlyBomb}: {
			Result:  SleepBomb,
			Method1: CookRandom,
		},
		{NumbnailDart, SpicyBomb}: {
			Result:  SleepBomb,
			Method1: CookRandom,
		},
		{PoisonDart, SpicyBomb}: {
			Result:  PoisonBomb,
			Method1: CookRandom,
		},
		{ShavedIce, BurlyBomb}: {
			Result:  FrostBomb,
			Method1: CookRandom,
		},
		{ShavedIce, SpicyBomb}: {
			Result:  FrostBomb,
			Method1: CookRandom,
		},
		{ShockBerry, BurlyBomb}: {
			Result:  NumbBomb,
			Method1: CookRandom,
		},
		{ShockBerry, SpicyBomb}: {
			Result:  NumbBomb,
			Method1: CookRandom,
		},
		{ShockCandy, BurlyBomb}: {
			Result:  NumbBomb,
			Method1: CookRandom,
		},
		{ShockCandy, SpicyBomb}: {
			Result:  NumbBomb,
			Method1: CookRandom,
		},
		{SpicyBerry, BakedYam}: {
			Result:  SpicyFries,
			Method1: CookWiggle,
		},
		{SpicyBerry, BurlyBerry}: {
			Result:  BerryJam,
			Method1: CookStir,
		},
		{SpicyBerry, DarkCherries}: {
			Result:  CherryBombs,
			Method1: CookRandom,
		},
		{SpicyBerry, MysteryBerry}: {
			Result:  SpicyBomb,
			Method1: CookRandom,
		},
		{SpicyBerry, PlainTea}: {
			Result:  SpicyTea,
			Method1: CookStir,
		},
		{SpicyBerry, SquashCandy}: {
			Result:  SquashTart,
			Method1: CookTimed,
		},
		{SpicyBerry, SquashPuree}: {
			Result:  SquashTart,
			Method1: CookTimed,
		},
		{Squash, BurlyBerry}: {
			Result:  PlumplingPie,
			Method1: CookTimed,
		},
		{Squash, SpicyBerry}: {
			Result:  SquashTart,
			Method1: CookTimed,
		},
		{SweetDanger, SpicyBomb}: {
			Result:  PoisonBomb,
			Method1: CookRandom,
		},
		{SweetDew, ShavedIce}: {
			Result:  HoneyIceCream,
			Method1: CookStir,
		},
		{SweetShroom, SweetDanger}: {
			Result:  MushroomSkewer,
			Method1: CookChop,
		},
		{TangyBerry, BerryJuice}: {
			Result:  TangyJuice,
			Method1: CookStir,
		},
		{TangyBerry, DarkCherries}: {
			Result:  BerrySmoothie,
			Method1: CookStir,
		},
		{TangyBerry, GlazedHoney}: {
			Result:  TangyJam,
			Method1: CookStir,
		},
		{TangyCarpaccio, CrisbeeDonut}: {
			Result:  QueensDinner,
			Method1: CookRandom,
		},
	}
)

func ComputeRecipe(item1, item2 ItemID) RecipeData {
	if item1 > item2 {
		item1, item2 = item2, item1
	}

	if item1 == ItemNone {
		output, ok := Recipe1[item2]
		if ok {
			return output
		}
	}

	output, ok := Recipe2[[2]ItemID{item1, item2}]
	if ok {
		return output
	}

	if item1 == MysteryBerry && item2 == MysteryBerry {
		return RecipeData{
			Result:  RandomMysteryBerry2,
			Method1: CookRandom,
		}
	}

	if item1 == MysteryBerry || item2 == MysteryBerry {
		return RecipeData{
			Result:  RandomMysteryBerry1,
			Method1: CookRandom,
		}
	}

	return RecipeData{
		Result:  Mistake,
		Method1: CookRandom,
	}
}
