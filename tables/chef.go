//go:generate go run golang.org/x/tools/cmd/stringer -type=ChefID -linecomment

package tables

import (
	"git.lubar.me/ben/spy-cards/gfx/sprites"
	"git.lubar.me/ben/spy-cards/tables/tables3d"
)

type ChefID uint8

const (
	ChefNone        ChefID = iota // (none)
	ChefFry                       // Fry
	ChefDoppel                    // Doppel
	ChefCrisbee                   // Crisbee
	ChefKut                       // Kut
	ChefJayde                     // Jayde
	ChefMasterSlice               // Master Slice
	ChefMicrowave                 // Microwave
)

type ChefFlags uint32

const (
	FlagMicrowave ChefFlags = 1 << iota // can only cook one item at a time, double cooking speed, auto eject cooked food, randomly changes Mistake/Big Mistake/Abomihoney into Mite Burger
)

type Pronoun uint8

const (
	PronounHeHim Pronoun = iota
	PronounSheHer
	PronounItIts
)

type ChefData struct {
	Flags           ChefFlags
	Pronouns        Pronoun
	IsMerchant      MerchantID
	PassiveItem     ItemID
	PassiveCost     uint8
	PassiveInterval uint8
	Sprite          *sprites.Sprite
	HighQuality     func(*State, ItemID) bool
	Discount        func(MerchantID, ItemID) int
}

func (c ChefID) Model() string {
	if c == ChefMicrowave {
		return "Microwave"
	}

	return tables3d.SpriteObject("", Chefs[c].Sprite).Name
}

var Chefs = [...]ChefData{
	ChefFry: {
		Flags:    0,
		Pronouns: PronounHeHim,
		Sprite:   sprites.TablesFry,
		HighQuality: func(s *State, id ItemID) bool {
			panic("TODO: HighQuality(Fry)") // TODO: return true if multiple different foods are being cooked simultaenously
		},
	},
	ChefDoppel: {
		Flags:           0,
		Pronouns:        PronounHeHim,
		PassiveItem:     BerryJuice,
		PassiveInterval: 120,
		Sprite:          sprites.TablesDoppel,
		HighQuality: func(_ *State, id ItemID) bool {
			return id == BerryJuice
		},
	},
	ChefCrisbee: {
		Flags:       0,
		Pronouns:    PronounHeHim,
		IsMerchant:  MerchantCrisbee,
		PassiveItem: BagOfFlour,
		Sprite:      sprites.TablesCrisbee,
		HighQuality: func(s *State, id ItemID) bool {
			panic("TODO: HighQuality(Crisbee)") // TODO: return true if same as previous food
		},
		Discount: func(m MerchantID, i ItemID) int {
			if m == MerchantCrisbee && i == BagOfFlour {
				return 4
			}

			return 0
		},
	},
	ChefKut: {
		Flags:    0,
		Pronouns: PronounHeHim,
		Sprite:   sprites.TablesKut,
		HighQuality: func(*State, ItemID) bool {
			return true
		},
	},
	ChefJayde: {
		Flags:       0,
		Pronouns:    PronounSheHer,
		IsMerchant:  MerchantJayde,
		PassiveItem: JaydesStew,
		PassiveCost: 7,
		Sprite:      sprites.TablesJayde,
		HighQuality: func(_ *State, id ItemID) bool {
			return id == JaydesStew
		},
		Discount: func(m MerchantID, i ItemID) int {
			if m == MerchantJayde && i == JaydesStew {
				return 18
			}

			return 0
		},
	},
	ChefMasterSlice: {
		Flags:    0,
		Pronouns: PronounHeHim,
		Sprite:   sprites.TablesMasterSlice,
		HighQuality: func(_ *State, id ItemID) bool {
			return Items[id].Flags&FlagTangy != 0
		},
		Discount: func(_ MerchantID, i ItemID) int {
			switch i {
			case TangyBerry:
				return 10
			case DarkCherries:
				return 10
			default:
				return 0
			}
		},
	},
	ChefMicrowave: {
		Flags:    FlagMicrowave,
		Pronouns: PronounItIts,
		Sprite:   sprites.TablesMicrowave,
		HighQuality: func(_ *State, id ItemID) bool {
			return id == Mistake || id == BigMistake || id == Abomihoney || id == MiteBurger
		},
	},
}
