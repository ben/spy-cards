package tables_test

import (
	"testing"

	"git.lubar.me/ben/spy-cards/gfx/sprites"
	"git.lubar.me/ben/spy-cards/tables"
)

func TestRecipeOrder(t *testing.T) {
	t.Parallel()

	for input := range tables.Recipe2 {
		if input[0] > input[1] {
			t.Errorf("Out-of-order recipe: %v", input)
		}
	}

	for i, rank := range tables.Unlocks {
		for _, input := range rank.ForceRecipe {
			if input[0] > input[1] {
				t.Errorf("Out-of-order recipe in rank %d: %v", i, input)
			}
		}
	}
}

func TestSprites(t *testing.T) {
	t.Parallel()

	for i := range tables.ForagingLocations {
		if tables.ForagingID(i) == tables.ForagingNone {
			continue
		}

		if tables.ForagingLocations[i].Sprite == nil || tables.ForagingLocations[i].Sprite == sprites.Blank {
			t.Errorf("Foraging Location is missing sprite: %v", tables.ForagingID(i))
		}
	}

	for i := range tables.Locations {
		if tables.LocationID(i) == tables.LocationNone {
			continue
		}

		if tables.Locations[i].Sprite == nil || tables.Locations[i].Sprite == sprites.Blank {
			t.Errorf("Location is missing sprite: %v", tables.LocationID(i))
		}
	}

	for i := range tables.Chefs {
		if tables.ChefID(i) == tables.ChefNone {
			continue
		}

		if tables.Chefs[i].Sprite == nil || tables.Chefs[i].Sprite == sprites.Blank {
			t.Errorf("Chef is missing sprite: %v", tables.ChefID(i))
		}
	}

	for i := range tables.Merchants {
		if tables.MerchantID(i) == tables.MerchantNone {
			continue
		}

		if tables.Merchants[i].Sprite == nil || tables.Merchants[i].Sprite == sprites.Blank {
			t.Errorf("Merchant is missing sprite: %v", tables.MerchantID(i))
		}
	}

	for i := range tables.Explorers {
		if tables.ExplorerID(i) == tables.ExplorerNone {
			continue
		}

		if tables.Explorers[i].Sprite == nil || tables.Explorers[i].Sprite == sprites.Blank {
			t.Errorf("Explorer is missing sprite: %v", tables.ExplorerID(i))
		}
	}

	for i := range tables.Items {
		if tables.ItemID(i) == tables.ItemNone {
			continue
		}

		if tables.Items[i].Sprite == nil || tables.Items[i].Sprite == sprites.Blank {
			t.Errorf("Item is missing sprite: %v", tables.ItemID(i))
		}
	}
}
