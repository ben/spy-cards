package tables

import (
	"context"
	"strconv"
	"time"

	"git.lubar.me/ben/spy-cards/audio"
	"git.lubar.me/ben/spy-cards/gfx"
	"git.lubar.me/ben/spy-cards/gfx/sprites"
	"git.lubar.me/ben/spy-cards/input"
	"git.lubar.me/ben/spy-cards/internal/router"
)

type menuItemRecipe struct {
	Input  [2]ItemID
	Output RecipeData
}

type menuItemCandidate struct {
	Item      ItemID
	Recipes   []menuItemRecipe
	Merchants []MerchantID
}

func (s *State) computePossibleMenuItems() []menuItemCandidate {
	s.Persistent.EnsureRankRecipes()

	haveChef := make(map[ChefID]bool)

	for i, rank := 0, int(s.Persistent.Rank()); i <= rank && i < len(Unlocks); i++ {
		for _, c := range Unlocks[i].Chefs {
			haveChef[c] = true
		}
	}

	var candidates []menuItemCandidate

nextKnownRecipe:
	for _, inputs := range s.Persistent.KnownRecipes {
		output := ComputeRecipe(inputs[0], inputs[1])
		if Items[output.Result].Flags&(FlagGarbage|FlagExplodes|FlagRaw) != 0 {
			continue
		}

		if req := Items[output.Result].RequiredChef; req != ChefNone && !haveChef[req] {
			continue
		}

		for i := range candidates {
			if candidates[i].Item == output.Result {
				candidates[i].Recipes = append(candidates[i].Recipes, menuItemRecipe{
					Input:  inputs,
					Output: output,
				})

				continue nextKnownRecipe
			}
		}

		candidates = append(candidates, menuItemCandidate{
			Item: output.Result,
			Recipes: []menuItemRecipe{
				{
					Input:  inputs,
					Output: output,
				},
			},
		})
	}

	if Chefs[s.Data.Chef].Flags&FlagMicrowave != 0 {
		for i := range candidates {
			if candidates[i].Item == MiteBurger {
				break
			}
		}

		candidates = append(candidates, menuItemCandidate{
			Item: MiteBurger,
		})
	}

	for i, rank := 0, int(s.Persistent.Rank()); i <= rank && i < len(Unlocks); i++ {
		for _, m := range Unlocks[i].Merchants {
		nextMerchantItem:
			for _, item := range Merchants[m].Items {
				if Items[item.Item].Flags&(FlagGarbage|FlagExplodes|FlagRaw) != 0 {
					continue
				}

				for i := range candidates {
					if candidates[i].Item == item.Item {
						candidates[i].Merchants = append(candidates[i].Merchants, m)

						continue nextMerchantItem
					}
				}

				candidates = append(candidates, menuItemCandidate{
					Item:      item.Item,
					Merchants: []MerchantID{m},
				})
			}
		}
	}

	return candidates
}

func (s *State) updateSetupMenu(ctx context.Context) (bool, *router.PageInfo, error) {
	w, h := gfx.Size()

	if s.menuCandidates == nil {
		s.menuCandidates = s.computePossibleMenuItems()
	}

	mx, my, click := s.ictx.Mouse()
	lastX, lastY, lastClick := s.ictx.LastMouse()

	if s.inEditCost {
		wheel := s.ictx.Wheel()

		if (s.ictx.ConsumeAllowRepeat(input.BtnUp, 60, 5) || wheel < 0) && s.Data.Menu[s.menuOffset[0]].Cost < 250 {
			s.Data.Menu[s.menuOffset[0]].Cost++

			audio.Confirm1.PlaySoundGlobal(0, 0, 0)
		}

		if (s.ictx.ConsumeAllowRepeat(input.BtnDown, 60, 5) || wheel > 0) && s.Data.Menu[s.menuOffset[0]].Cost > 0 {
			s.Data.Menu[s.menuOffset[0]].Cost--

			audio.Confirm1.PlaySoundGlobal(0, 0, 0)
		}

		if s.ictx.ConsumeAllowRepeat(input.BtnRight, 60, 5) && s.Data.Menu[s.menuOffset[0]].Cost < 250 {
			if s.Data.Menu[s.menuOffset[0]].Cost < 240 {
				s.Data.Menu[s.menuOffset[0]].Cost += 10
			} else {
				s.Data.Menu[s.menuOffset[0]].Cost = 250
			}

			audio.Confirm1.PlaySoundGlobal(0, 0, 0)
		}

		if s.ictx.ConsumeAllowRepeat(input.BtnLeft, 60, 5) && s.Data.Menu[s.menuOffset[0]].Cost > 0 {
			if s.Data.Menu[s.menuOffset[0]].Cost > 10 {
				s.Data.Menu[s.menuOffset[0]].Cost -= 10
			} else {
				s.Data.Menu[s.menuOffset[0]].Cost = 0
			}

			audio.Confirm1.PlaySoundGlobal(0, 0, 0)
		}

		if lastClick && !click && !s.ictx.IsMouseDrag() {
			top := float32(h)/64 - 3
			x := (mx - 0.5) * float32(w) / 32
			y := (0.5 - my) * float32(h) / 32

			switch {
			case x > -4 && x < 4 && y < top-1.5 && y > top-4.25:
				if s.Data.Menu[s.menuOffset[0]].Cost < 250 {
					s.Data.Menu[s.menuOffset[0]].Cost++

					audio.Confirm1.PlaySoundGlobal(0, 0, 0)
				}
			case x > -4 && x < 4 && y < top-5.75 && y > top-8.75:
				if s.Data.Menu[s.menuOffset[0]].Cost > 0 {
					s.Data.Menu[s.menuOffset[0]].Cost--

					audio.Confirm1.PlaySoundGlobal(0, 0, 0)
				}
			case sprites.TextHitTest(&s.cam, sprites.FontD3Streetism, sprites.Button(input.BtnConfirm)+" Done", 0, top-10, 0, 2, 2, mx, my, true):
				s.ictx.ConsumeClick()

				s.inEditCost = false

				audio.Confirm.PlaySoundGlobal(0, 0, 0)
				s.Save()
			}
		}

		if s.ictx.Consume(input.BtnConfirm) {
			s.inEditCost = false

			audio.Confirm.PlaySoundGlobal(0, 0, 0)
			s.Save()
		}

		return false, nil, nil
	}

	if s.inputIndex == 2 && len(s.Data.Menu) == 0 {
		s.inputIndex = 0
	}

	rank := int(s.Persistent.Rank())

	maxMenuItems := 0

	for i := range Unlocks {
		if i > rank {
			break
		}

		maxMenuItems += int(Unlocks[i].MenuItems)
	}

	wheel := s.ictx.Wheel()
	_ = wheel // TODO: handle more recipes than fit on screen

	if ((mx != lastX || my != lastY) && !click) || (!lastClick && click) {
		top := float32(h)/64 - 3

		switch x, y := (mx-0.5)*float32(w)/32, (0.5-my)*float32(h)/32; {
		case sprites.TextHitTest(&s.cam, sprites.FontD3Streetism, "Next", float32(w)/64-3, float32(h)/64-1.5, 0, 2, 2, mx, my, true):
			invalidMenu := len(s.Data.Menu) == 0
			if s.Data.Objective.Limit == ObjectiveMenuSize {
				invalidMenu = len(s.Data.Menu) != int(s.Data.Objective.LimitNum)
			}

			if !invalidMenu && s.inputIndex != 4 {
				audio.Confirm1.PlaySoundGlobal(0, 0, 0)
			}

			s.inputIndex = 4
		case sprites.TextHitTest(&s.cam, sprites.FontD3Streetism, "Back", -float32(w)/64+3, float32(h)/64-8.75, 0, 2, 2, mx, my, true):
			if s.inputIndex != 1 {
				audio.Confirm1.PlaySoundGlobal(0, 0, 0)
			}

			s.inputIndex = 1
		case x > -8.25 && x < -0.75:
			for i := range s.Data.Menu {
				if y > top-float32(i)-1.2 && y < top-float32(i)-0.3 {
					if s.inputIndex != 2 || s.menuOffset[0] != uint8(i) {
						audio.Confirm1.PlaySoundGlobal(0, 0, 0)
					}

					s.inputIndex = 2
					s.menuOffset[0] = uint8(i)

					break
				}
			}
		case x > 0 && x < 7.5:
			section := 1

			for i := range s.menuCandidates {
				if section == 1 && len(s.menuCandidates[i].Recipes) == 0 && len(s.menuCandidates[i].Merchants) != 0 {
					section++
				}

				if y > top-float32(i)-float32(section)*1.25+0.05 && y < top-float32(i)-float32(section)*1.25+0.95 {
					if s.inputIndex != 3 || s.menuOffset[1] != uint8(i) {
						audio.Confirm1.PlaySoundGlobal(0, 0, 0)
					}

					s.inputIndex = 3
					s.menuOffset[1] = uint8(i)

					break
				}
			}
		default:
			s.inputIndex = 0
		}

		s.updateSetupRank()
	}

	if s.inputIndex == 1 && (s.ictx.Consume(input.BtnConfirm) || (lastClick && !click && !s.ictx.IsMouseDrag())) {
		s.ictx.ConsumeClick()

		s.offset[0] = 0
		s.offset[1] = 0

	findChef:
		for i := range Unlocks {
			for _, c := range Unlocks[i].Chefs {
				if c == s.Data.Chef {
					break findChef
				}

				s.offset[0]++
			}
		}

	findLoc:
		for i := range Unlocks {
			for _, l := range Unlocks[i].Locations {
				if l == s.Data.Location {
					break findLoc
				}

				s.offset[1]++
			}
		}

		s.Page = PageSetupChefLoc
		s.inputIndex = 0
		s.useTargetOffset[0] = false
		s.useTargetOffset[1] = false

		audio.Confirm.PlaySoundGlobal(0, 0, 0)

		s.Save()

		return true, nil, nil
	}

	if s.inputIndex == 2 && s.menuOffset[0] < uint8(len(s.Data.Menu)) && (s.ictx.Consume(input.BtnConfirm) || (lastClick && !click && !s.ictx.IsMouseDrag())) {
		s.ictx.ConsumeClick()

		s.inEditCost = true

		audio.Confirm.PlaySoundGlobal(0, 0, 0)
	}

	if s.inputIndex == 3 && (s.ictx.Consume(input.BtnConfirm) || (lastClick && !click && !s.ictx.IsMouseDrag())) {
		s.ictx.ConsumeClick()

		remove := -1

		if s.menuOffset[1] < uint8(len(s.menuCandidates)) {
			for i, item := range s.Data.Menu {
				if item.Item == s.menuCandidates[s.menuOffset[1]].Item {
					remove = i

					break
				}
			}
		}

		switch {
		case remove != -1:
			s.Data.Menu = append(s.Data.Menu[:remove], s.Data.Menu[remove+1:]...)

			audio.Confirm.PlaySoundGlobal(0, 0, 0)
			s.Save()
		case len(s.Data.Menu) < maxMenuItems && s.menuOffset[1] < uint8(len(s.menuCandidates)):
			item := s.menuCandidates[s.menuOffset[1]].Item

			s.Data.Menu = append(s.Data.Menu, ItemCost{
				Item: item,
				Cost: Items[item].Value,
			})

			if !router.FlagDisableExploits.IsSet() {
				down := s.ictx.ConsumeAllowRepeat(input.BtnDown, 60, 5)
				up := s.ictx.ConsumeAllowRepeat(input.BtnUp, 60, 5)
				ok := false

				if down && s.menuOffset[1]+1 < uint8(len(s.menuCandidates)) {
					s.menuOffset[1]++
					ok = true
				} else if up && s.menuOffset[1] > 0 {
					s.menuOffset[1]--
					ok = true
				}

				if ok {
					for i, item := range s.Data.Menu {
						if item.Item == s.menuCandidates[s.menuOffset[1]].Item {
							ok = false

							s.Data.Menu = append(s.Data.Menu[:i], s.Data.Menu[i+1:]...)

							break
						}
					}
				}

				if ok {
					item := s.menuCandidates[s.menuOffset[1]].Item

					s.Data.Menu = append(s.Data.Menu, ItemCost{
						Item: item,
						Cost: Items[item].Value,
					})
				}
			}

			s.inputIndex = 2
			s.menuOffset[0] = uint8(len(s.Data.Menu)) - 1

			audio.Confirm.PlaySoundGlobal(0, 0, 0)
			s.Save()
		default:
			audio.Buzzer.PlaySoundGlobal(0, 0, 0)
		}
	}

	if s.inputIndex == 4 && (s.ictx.Consume(input.BtnConfirm) || (lastClick && !click && !s.ictx.IsMouseDrag())) {
		invalidMenu := len(s.Data.Menu) == 0
		if s.Data.Objective.Limit == ObjectiveMenuSize {
			invalidMenu = len(s.Data.Menu) != int(s.Data.Objective.LimitNum)
		}

		if !invalidMenu {
			s.Page = PagePreDay
			s.Data.Start = time.Now()

			audio.Confirm.PlaySoundGlobal(0, 0, 0)

			s.Save()

			return true, nil, nil
		}
	}

	if s.inputIndex == 3 && s.ictx.ConsumeAllowRepeat(input.BtnRight, 60, 5) {
		s.inputIndex = 4

		audio.Confirm1.PlaySoundGlobal(0, 0, 0)
	}

	if s.inputIndex == 4 && s.ictx.ConsumeAllowRepeat(input.BtnLeft, 60, 5) {
		s.inputIndex = 3

		audio.Confirm1.PlaySoundGlobal(0, 0, 0)
	}

	if s.inputIndex == 3 && s.ictx.ConsumeAllowRepeat(input.BtnUp, 60, 5) {
		if s.menuOffset[1] > 0 {
			s.menuOffset[1]--

			audio.Confirm1.PlaySoundGlobal(0, 0, 0)
		}
	}

	if s.inputIndex == 3 && s.ictx.ConsumeAllowRepeat(input.BtnDown, 60, 5) {
		if s.menuOffset[1]+1 < uint8(len(s.menuCandidates)) {
			s.menuOffset[1]++

			audio.Confirm1.PlaySoundGlobal(0, 0, 0)
		}
	}

	if s.inputIndex == 2 && s.ictx.ConsumeAllowRepeat(input.BtnUp, 60, 5) {
		if s.menuOffset[0] > 0 {
			s.menuOffset[0]--

			audio.Confirm1.PlaySoundGlobal(0, 0, 0)
		}
	}

	if s.inputIndex == 2 && s.ictx.ConsumeAllowRepeat(input.BtnDown, 60, 5) {
		if s.menuOffset[0]+1 < uint8(len(s.menuCandidates)) {
			s.menuOffset[0]++

			audio.Confirm1.PlaySoundGlobal(0, 0, 0)
		}
	}

	if s.inputIndex == 3 && s.ictx.ConsumeAllowRepeat(input.BtnLeft, 60, 5) {
		if len(s.Data.Menu) == 0 {
			s.inputIndex = 1
		} else {
			s.inputIndex = 2
		}

		audio.Confirm1.PlaySoundGlobal(0, 0, 0)
	}

	if s.inputIndex == 2 && s.ictx.ConsumeAllowRepeat(input.BtnRight, 60, 5) {
		s.inputIndex = 3

		audio.Confirm1.PlaySoundGlobal(0, 0, 0)
	}

	if s.inputIndex == 2 && s.ictx.ConsumeAllowRepeat(input.BtnLeft, 60, 5) {
		s.inputIndex = 1

		audio.Confirm1.PlaySoundGlobal(0, 0, 0)
	}

	if s.inputIndex == 1 && s.ictx.ConsumeAllowRepeat(input.BtnRight, 60, 5) {
		if len(s.Data.Menu) == 0 {
			s.inputIndex = 3
		} else {
			s.inputIndex = 2
		}

		audio.Confirm1.PlaySoundGlobal(0, 0, 0)
	}

	if s.inputIndex == 0 && s.ictx.ConsumeAllowRepeat(input.BtnLeft, 60, 5) {
		s.inputIndex = 3

		audio.Confirm1.PlaySoundGlobal(0, 0, 0)
	}

	if s.inputIndex == 0 && s.ictx.ConsumeAllowRepeat(input.BtnRight, 60, 5) {
		s.inputIndex = 3

		audio.Confirm1.PlaySoundGlobal(0, 0, 0)
	}

	return false, nil, nil
}

func (s *State) renderSetupMenu(ctx context.Context) error {
	w, h := gfx.Size()

	rank := int(s.Persistent.Rank())

	s.batch.AppendEx(Locations[s.Data.Location].Sprite, -float32(w)/64+3.84, float32(h)/64-7.5, 0, 3, 3, sprites.White, sprites.FlagNoDiscard, 0, 0, 0)

	s.batch.AppendEx(Chefs[s.Data.Chef].Sprite, -float32(w)/64+2, float32(h)/64-7.5, 0, -1.25, 1.25, sprites.White, sprites.FlagNoDiscard, 0, 0, 0)

	backColor := sprites.White
	if s.inputIndex == 1 {
		backColor = sprites.Yellow
	}

	if s.inEditCost {
		backColor = sprites.Gray
	}

	sprites.DrawTextBorder(s.batch, sprites.FontD3Streetism, "Back", -float32(w)/64+3, float32(h)/64-8.75, 0, 2, 2, backColor, sprites.Black, true)

	top := float32(h)/64 - 3

	if s.inEditCost {
		for _, c := range s.menuCandidates {
			if c.Item != s.Data.Menu[s.menuOffset[0]].Item {
				continue
			}

			s.batch.AppendEx(sprites.Berries, 5, top-5, 0, 3, 3, sprites.White, sprites.FlagNoDiscard, 0, 0, 0)

			sprites.DrawTextBorder(s.batch, sprites.FontD3Streetism, strconv.FormatUint(uint64(s.Data.Menu[s.menuOffset[0]].Cost), 10), 0, top-6.2, 0, 5, 5, sprites.Yellow, sprites.Black, true)
			sprites.DrawTextBorder(s.batch, sprites.FontD3Streetism, sprites.Button(input.BtnConfirm)+" Done", 0, top-10, 0, 2, 2, sprites.White, sprites.Black, true)

			s.batch.AppendEx(Items[c.Item].Sprite, 11, top-1.5, 0, 7, 7, sprites.White, sprites.FlagNoDiscard, 0, 0, 0)

			sprites.DrawTextBorder(s.batch, sprites.FontD3Streetism, c.Item.String(), 0, top, 0, 1, 1, sprites.White, sprites.Black, true)

			y := top - 1

			x := sprites.DrawTextBorder(s.batch, sprites.FontBubblegumSans, "Recommended Price: "+strconv.FormatUint(uint64(Items[c.Item].Value), 10), -0.25, y, 0, 1, 1, sprites.White, sprites.Black, true)

			s.batch.AppendEx(sprites.Berries, x+0.4, y+0.2, 0, 0.5, 0.5, sprites.White, sprites.FlagNoDiscard, 0, 0, 0)

			y -= 3

			merchantItems := []ItemID{c.Item}

			for _, recipe := range c.Recipes {
				y--

				x := sprites.DrawTextBorder(s.batch, sprites.FontBubblegumSans, "Recipe: ", 8.5, y, 0, 1, 1, sprites.White, sprites.Black, false)

				if recipe.Input[0] == ItemNone {
					s.batch.AppendEx(Items[recipe.Input[1]].Sprite, x+0.2, y+0.2, 0, 1, 1, sprites.White, sprites.FlagNoDiscard, 0, 0, 0)

					found := false

					for _, i := range merchantItems {
						if i == recipe.Input[1] {
							found = true

							break
						}
					}

					if !found {
						merchantItems = append(merchantItems, recipe.Input[1])
					}
				} else {
					s.batch.AppendEx(Items[recipe.Input[0]].Sprite, x+0.2, y+0.2, 0, 1, 1, sprites.White, sprites.FlagNoDiscard, 0, 0, 0)
					x = sprites.DrawTextBorder(s.batch, sprites.FontBubblegumSans, "+", x+0.8, y, 0, 1, 1, sprites.White, sprites.Black, true)
					s.batch.AppendEx(Items[recipe.Input[1]].Sprite, x+0.4, y+0.2, 0, 1, 1, sprites.White, sprites.FlagNoDiscard, 0, 0, 0)

					found0, found1 := false, false

					for _, i := range merchantItems {
						if i == recipe.Input[0] {
							found0 = true
						}

						if i == recipe.Input[1] {
							found1 = true
						}
					}

					if !found0 {
						merchantItems = append(merchantItems, recipe.Input[0])
					}

					if !found1 {
						merchantItems = append(merchantItems, recipe.Input[1])
					}
				}
			}

			if len(c.Recipes) != 0 {
				y -= 0.5
			}

			for _, item := range merchantItems {
				for i := range Unlocks {
					if i > rank {
						break
					}

					for _, m := range Unlocks[i].Merchants {
						for _, cost := range Merchants[m].Items {
							if cost.Item == item {
								y--

								discount := 0
								if d := Chefs[s.Data.Chef].Discount; d != nil {
									discount = d(m, item)
								}

								s.batch.AppendEx(Items[item].Sprite, 8.75, y+0.2, 0, 1, 1, sprites.White, sprites.FlagNoDiscard, 0, 0, 0)

								x := sprites.DrawTextBorder(s.batch, sprites.FontBubblegumSans, " from ", 9, y, 0, 1, 1, sprites.White, sprites.Black, false)

								s.batch.AppendEx(Merchants[m].Sprite, x+0.3, y-0.2, 0, 0.3, 0.3, sprites.White, sprites.FlagNoDiscard, 0, 0, 0)

								x = sprites.DrawTextBorder(s.batch, sprites.FontBubblegumSans, ": ", x+0.6, y, 0, 1, 1, sprites.White, sprites.Black, false)

								costTint := sprites.White

								switch {
								case discount < 0:
									costTint = sprites.Red
								case discount > 0:
									costTint = sprites.Green
								}

								x = sprites.DrawTextBorder(s.batch, sprites.FontBubblegumSans, strconv.Itoa(int(cost.Cost)-discount), x, y, 0, 1, 1, costTint, sprites.Black, false)

								s.batch.AppendEx(sprites.Berries, x+0.4, y+0.2, 0, 0.5, 0.5, sprites.White, sprites.FlagNoDiscard, 0, 0, 0)
							}
						}
					}
				}
			}

			break
		}
	} else {
		maxMenuItems := 0

		nextMenuRank := 0

		for i := range Unlocks {
			if i > rank && Unlocks[i].MenuItems != 0 {
				nextMenuRank = i

				break
			}

			maxMenuItems += int(Unlocks[i].MenuItems)
		}

		renderDots := func(x0, x1, y float32, prefix, suffix string) {
			for _, letter := range prefix {
				x0 += sprites.TextAdvance(sprites.FontBubblegumSans, letter, 1)
			}

			for _, letter := range suffix {
				x1 -= sprites.TextAdvance(sprites.FontBubblegumSans, letter, 1)
			}

			dx := sprites.TextAdvance(sprites.FontBubblegumSans, '.', 1)
			x0 += dx
			dx += dx

			if suffix != "" {
				x1 -= dx
			}

			for x := float32(-9); x < x1; x += dx {
				if x0 < x {
					sprites.DrawLetter(s.batch, sprites.FontBubblegumSans, '.', x, y, 0, 1, 1, sprites.White, sprites.FlagBorder|sprites.FlagIsMTSDF, 0, 0, 0)
				}
			}
		}

		sprites.DrawTextBorder(s.batch, sprites.FontD3Streetism, "Restaurant Menu", -4.625, top, 0, 1, 1, sprites.White, sprites.Black, true)

		for i, item := range s.Data.Menu {
			s.batch.AppendEx(Items[item.Item].Sprite, -8, top-float32(i)-0.8, 0, 1, 1, sprites.White, sprites.FlagNoDiscard, 0, 0, 0)

			textTint := sprites.White
			if s.inputIndex == 2 && s.menuOffset[0] == uint8(i) {
				textTint = sprites.Yellow
			}

			sprites.DrawTextBorder(s.batch, sprites.FontBubblegumSans, item.Item.String(), -7.5, top-float32(i)-1, 0, 1, 1, textTint, sprites.Black, false)

			cost := strconv.FormatUint(uint64(item.Cost), 10)

			costX := float32(-1)
			for _, letter := range cost {
				costX -= sprites.TextAdvance(sprites.FontBubblegumSans, letter, 1)
			}

			sprites.DrawTextBorder(s.batch, sprites.FontBubblegumSans, cost, costX, top-float32(i)-1, 0, 1, 1, textTint, sprites.Black, false)

			renderDots(-7.5, -1, top-float32(i)-1, item.Item.String(), cost)
		}

		for i := len(s.Data.Menu); i < maxMenuItems; i++ {
			renderDots(-8.5, -1, top-float32(i)-1, "", "")
		}

		if nextMenuRank != 0 {
			sprites.DrawTextBorder(s.batch, sprites.FontBubblegumSans, "Expands at Rank "+strconv.Itoa(nextMenuRank), -4.625, top-float32(maxMenuItems)-1, 0, 1, 1, sprites.Gray, sprites.Black, true)
		}

		section := 0

		for i := range s.menuCandidates {
			if section == 0 {
				sprites.DrawTextBorder(s.batch, sprites.FontD3Streetism, "Known Recipes", 4.25, top-float32(i)-float32(section)*1.25, 0, 1, 1, sprites.White, sprites.Black, true)

				section++
			}

			if section == 1 && len(s.menuCandidates[i].Recipes) == 0 && len(s.menuCandidates[i].Merchants) != 0 {
				sprites.DrawTextBorder(s.batch, sprites.FontD3Streetism, "From Merchants", 4.25, top-float32(i)-float32(section)*1.25, 0, 1, 1, sprites.White, sprites.Black, true)

				section++
			}

			tint, textTint := sprites.White, sprites.White
			suffix := ""

			if s.inputIndex == 3 && s.menuOffset[1] == uint8(i) {
				textTint = sprites.Yellow
			}

			for _, item := range s.Data.Menu {
				if item.Item == s.menuCandidates[i].Item {
					tint, textTint = sprites.Gray, sprites.Gray
					suffix = " (added)"

					if s.inputIndex == 3 && s.menuOffset[1] == uint8(i) {
						textTint = sprites.Red
						suffix = " (remove)"
					}

					break
				}
			}

			s.batch.AppendEx(Items[s.menuCandidates[i].Item].Sprite, 0.5, top-float32(i)-float32(section)*1.25+0.25+0.2, 0, 1, 1, tint, sprites.FlagNoDiscard, 0, 0, 0)

			sprites.DrawTextBorder(s.batch, sprites.FontBubblegumSans, s.menuCandidates[i].Item.String()+suffix, 1, top-float32(i)-float32(section)*1.25+0.25, 0, 1, 1, textTint, sprites.Black, false)
		}

		var viewCandidate *menuItemCandidate

		if s.inputIndex == 2 && s.menuOffset[0] < uint8(len(s.Data.Menu)) {
			for i := range s.menuCandidates {
				if s.menuCandidates[i].Item == s.Data.Menu[s.menuOffset[0]].Item {
					viewCandidate = &s.menuCandidates[i]

					break
				}
			}
		} else if s.menuOffset[1] < uint8(len(s.menuCandidates)) {
			viewCandidate = &s.menuCandidates[s.menuOffset[1]]
		}

		if viewCandidate != nil {
			s.batch.AppendEx(Items[viewCandidate.Item].Sprite, 14, top-0.2, 0, 3, 3, sprites.White, sprites.FlagNoDiscard, 0, 0, 0)

			sprites.DrawTextBorder(s.batch, sprites.FontD3Streetism, viewCandidate.Item.String(), 11.5, top, 0, 1, 1, sprites.White, sprites.Black, true)

			y := top - 1

			x := sprites.DrawTextBorder(s.batch, sprites.FontBubblegumSans, "Rec. Price: "+strconv.FormatUint(uint64(Items[viewCandidate.Item].Value), 10), 8.5, y, 0, 1, 1, sprites.White, sprites.Black, false)

			s.batch.AppendEx(sprites.Berries, x+0.4, y+0.2, 0, 0.5, 0.5, sprites.White, sprites.FlagNoDiscard, 0, 0, 0)

			merchantItems := []ItemID{viewCandidate.Item}

			for _, recipe := range viewCandidate.Recipes {
				y--

				x := sprites.DrawTextBorder(s.batch, sprites.FontBubblegumSans, "Recipe: ", 8.5, y, 0, 1, 1, sprites.White, sprites.Black, false)

				if recipe.Input[0] == ItemNone {
					s.batch.AppendEx(Items[recipe.Input[1]].Sprite, x+0.2, y+0.2, 0, 1, 1, sprites.White, sprites.FlagNoDiscard, 0, 0, 0)

					found := false

					for _, i := range merchantItems {
						if i == recipe.Input[1] {
							found = true

							break
						}
					}

					if !found {
						merchantItems = append(merchantItems, recipe.Input[1])
					}
				} else {
					s.batch.AppendEx(Items[recipe.Input[0]].Sprite, x+0.2, y+0.2, 0, 1, 1, sprites.White, sprites.FlagNoDiscard, 0, 0, 0)
					x = sprites.DrawTextBorder(s.batch, sprites.FontBubblegumSans, "+", x+0.8, y, 0, 1, 1, sprites.White, sprites.Black, true)
					s.batch.AppendEx(Items[recipe.Input[1]].Sprite, x+0.4, y+0.2, 0, 1, 1, sprites.White, sprites.FlagNoDiscard, 0, 0, 0)

					found0, found1 := false, false

					for _, i := range merchantItems {
						if i == recipe.Input[0] {
							found0 = true
						}

						if i == recipe.Input[1] {
							found1 = true
						}
					}

					if !found0 {
						merchantItems = append(merchantItems, recipe.Input[0])
					}

					if !found1 {
						merchantItems = append(merchantItems, recipe.Input[1])
					}
				}
			}

			if len(viewCandidate.Recipes) != 0 {
				y -= 0.5
			}

			for _, item := range merchantItems {
				for i := range Unlocks {
					if i > rank {
						break
					}

					for _, m := range Unlocks[i].Merchants {
						for _, cost := range Merchants[m].Items {
							if cost.Item == item {
								y--

								discount := 0
								if d := Chefs[s.Data.Chef].Discount; d != nil {
									discount = d(m, item)
								}

								s.batch.AppendEx(Items[item].Sprite, 8.75, y+0.2, 0, 1, 1, sprites.White, sprites.FlagNoDiscard, 0, 0, 0)

								x := sprites.DrawTextBorder(s.batch, sprites.FontBubblegumSans, " from ", 9, y, 0, 1, 1, sprites.White, sprites.Black, false)

								s.batch.AppendEx(Merchants[m].Sprite, x+0.3, y-0.2, 0, 0.3, 0.3, sprites.White, sprites.FlagNoDiscard, 0, 0, 0)

								x = sprites.DrawTextBorder(s.batch, sprites.FontBubblegumSans, ": ", x+0.6, y, 0, 1, 1, sprites.White, sprites.Black, false)

								costTint := sprites.White

								switch {
								case discount < 0:
									costTint = sprites.Red
								case discount > 0:
									costTint = sprites.Green
								}

								x = sprites.DrawTextBorder(s.batch, sprites.FontBubblegumSans, strconv.Itoa(int(cost.Cost)-discount), x, y, 0, 1, 1, costTint, sprites.Black, false)

								s.batch.AppendEx(sprites.Berries, x+0.4, y+0.2, 0, 0.5, 0.5, sprites.White, sprites.FlagNoDiscard, 0, 0, 0)
							}
						}
					}
				}
			}
		}
	}

	s.renderSetupRank()

	sprites.DrawTextBorder(s.batch, sprites.FontD3Streetism, "Objective: "+s.Data.Objective.String(), 0, float32(h)/64-1.25, 0, 1, 1, sprites.White, sprites.Black, true)

	nextColor := sprites.White

	if s.inputIndex == 4 {
		nextColor = sprites.Yellow
	}

	if s.inEditCost {
		nextColor = sprites.Gray
	}

	if s.Data.Objective.Limit == ObjectiveMenuSize {
		if len(s.Data.Menu) != int(s.Data.Objective.LimitNum) {
			nextColor = sprites.Gray
		}
	} else if len(s.Data.Menu) == 0 {
		nextColor = sprites.Gray
	}

	sprites.DrawTextBorder(s.batch, sprites.FontD3Streetism, "Next", float32(w)/64-3, float32(h)/64-1.5, 0, 2, 2, nextColor, sprites.Black, true)

	return nil
}
