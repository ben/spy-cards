package tables

type UnlockData struct {
	Chefs       []ChefID
	Merchants   []MerchantID
	Locations   []LocationID
	Foraging    []ForagingID
	Explorers   []ExplorerID
	Patrons     []PatronID
	ForceRecipe [][2]ItemID
	MenuItems   uint8
}

var Unlocks = [...]UnlockData{
	{
		Chefs:     []ChefID{ChefFry},
		Merchants: []MerchantID{MerchantMadameButterfly},
		Locations: []LocationID{LocationBugariaCommercial},
		// TODO: default patrons
		ForceRecipe: [][2]ItemID{
			{ItemNone, CrunchyLeaf},
			{ItemNone, HoneyDrop},
			{ItemNone, Mushroom},
			{ItemNone, AphidEgg},
			{CrunchyLeaf, AphidEgg},
			{AphidEgg, Mushroom},
			{Mushroom, DangerShroom},
			{HoneyDrop, Mushroom},
		},
		MenuItems: 4,
	},
	{
		Foraging:  []ForagingID{ForagingBugariaOutskirts},
		Explorers: []ExplorerID{ExplorerAnt},
		// TODO: patrons
	},
	{
		Chefs:     []ChefID{ChefDoppel},
		Merchants: []MerchantID{MerchantCricketly},
		// TODO: patrons
	},
	{
		Locations: []LocationID{LocationUndergroundTavern},
		Foraging:  []ForagingID{ForagingSnakemouthDen},
		// TODO: patrons
	},
	{
		Merchants: []MerchantID{MerchantSandy, MerchantNeil, MerchantXic},
		Locations: []LocationID{LocationDefiantRoot},
		// TODO: patrons
	},
	{
		Merchants: []MerchantID{MerchantClaire},
		Explorers: []ExplorerID{ExplorerCelia},
		// TODO: patrons
		MenuItems: 2,
	},
	{
		Locations: []LocationID{LocationBeeCafeteria},
		Foraging:  []ForagingID{ForagingHoneyFactory},
		// TODO: patrons (bees)
	},
	{
		Chefs:     []ChefID{ChefCrisbee},
		Merchants: []MerchantID{MerchantCrisbee},
		// TODO: patrons
		ForceRecipe: [][2]ItemID{
			{BagOfFlour, GlazedHoney},
		},
	},
	{
		Explorers: []ExplorerID{ExplorerMothiva},
		Merchants: []MerchantID{MerchantGein, MerchantYumnum},
		// TODO: patrons
	},
	{
		Chefs:     []ChefID{ChefKut},
		Locations: []LocationID{LocationGoldenSettlementFine},
		Foraging:  []ForagingID{ForagingGoldenHills},
		// TODO: patrons
		ForceRecipe: [][2]ItemID{
			{ItemNone, TangyBerry},
			{TangyCarpaccio, CrisbeeDonut},
		},
	},
	{
		Merchants: []MerchantID{MerchantSkirby, MerchantCherryGuy},
		// TODO: patrons
		MenuItems: 3,
	},
	{
		Explorers: []ExplorerID{ExplorerSlacker},
		// TODO: patrons
	},
	{
		Foraging: []ForagingID{ForagingLostSands},
		// TODO: patrons
	},
	{
		Chefs:     []ChefID{ChefJayde},
		Merchants: []MerchantID{MerchantJayde},
		Locations: []LocationID{LocationWaspKingdomCart},
		// TODO: patrons (wasp)
	},
	{
		Merchants: []MerchantID{MerchantTyna},
		Locations: []LocationID{LocationDineMite},
		// TODO: patrons (termite)
	},
	{
		Explorers: []ExplorerID{ExplorerMaki},
		Foraging:  []ForagingID{ForagingForsakenLands},
		// TODO: patrons
		MenuItems: 4,
	},
	{
		Chefs:     []ChefID{ChefMasterSlice},
		Locations: []LocationID{LocationMetalIsland},
		// TODO: patrons (wealthy)
	},
	{
		Merchants: []MerchantID{MerchantRina},
		// TODO: patrons (roach)
	},
	{
		Explorers: []ExplorerID{ExplorerSnakemouth},
		// TODO: patrons
	},
	{
		// TODO: patrons
	},
	{
		// TODO: patrons
		MenuItems: 5,
	},
	{
		// TODO: patrons
	},
	{
		// TODO: patrons
	},
	{
		// TODO: patrons
	},
	{
		// TODO: patrons
	},
	{
		// TODO: patrons
		MenuItems: 7,
	},
	{
		// TODO: patrons
	},
	{
		Chefs:     []ChefID{ChefMicrowave},
		Locations: []LocationID{LocationApartment, LocationColosseum},
		// TODO: patrons (tanjerin)
	},
}
