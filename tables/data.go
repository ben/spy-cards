package tables

import (
	"math/big"
	"time"
)

const PointsPerRank = 5

type PersistentData struct {
	RankPoints   uint64                `json:"p"`
	KnownRecipes [][2]ItemID           `json:"r"`
	History      []HistoryData         `json:"h"`
	LastLayout   map[LocationID]Layout `json:"l"`
}

func (p *PersistentData) Rank() uint64 {
	return p.RankPoints / PointsPerRank
}

func (p *PersistentData) EnsureRankRecipes() {
	for i := 0; i <= int(p.Rank()) && i < len(Unlocks); i++ {
		for _, r := range Unlocks[i].ForceRecipe {
			found := false

			for _, k := range p.KnownRecipes {
				if r == k {
					found = true

					break
				}
			}

			if !found {
				p.KnownRecipes = append(p.KnownRecipes, r)
			}
		}
	}
}

type GameData struct {
	Start       time.Time                 `json:"s"`
	Berries     big.Int                   `json:"b"`
	Day         uint64                    `json:"d"`
	DayProgress uint32                    `json:"t"`
	Location    LocationID                `json:"a"`
	Chef        ChefID                    `json:"c"`
	Inventory   map[ItemID]uint64         `json:"i"`
	Foraging    map[ForagingID]ExplorerID `json:"f"`
	Menu        []ItemCost                `json:"m"`
	Kitchen     KitchenState              `json:"k"`
	Restaurant  RestaurantState           `json:"r"`
	Layout      Layout                    `json:"l"`
	Objective   Objective                 `json:"o"`
	Served      uint64                    `json:"p"`
	Wrong       uint64                    `json:"w"`
}

func NewGameData() *GameData {
	d := &GameData{
		Start:     time.Now(),
		Inventory: make(map[ItemID]uint64),
		Foraging:  make(map[ForagingID]ExplorerID),
	}

	d.Berries.SetInt64(1000)

	return d
}

type HistoryData struct {
	Start     time.Time         `json:"s"`
	Berries   big.Int           `json:"b"`
	Location  LocationID        `json:"a"`
	Chef      ChefID            `json:"c"`
	Inventory map[ItemID]uint64 `json:"i"`
	Menu      []ItemCost        `json:"m"`
	Layout    Layout            `json:"l"`
	Objective Objective         `json:"o"`
	Served    uint64            `json:"p"`
}

type KitchenState struct {
	// TODO
}

type RestaurantState struct {
	// TODO
}
