package tables

import "strconv"

type ObjectiveTargetType uint8

const (
	ObjectiveTargetMissing ObjectiveTargetType = iota
	ObjectivePatrons
	ObjectiveBerries
	ObjectiveRating
	ObjectiveMeals
)

type ObjectiveLimitType uint8

const (
	ObjectiveLimitMissing ObjectiveLimitType = iota
	ObjectiveNoLimit
	ObjectiveMenuSize
	ObjectiveMissedPatrons
	ObjectiveWrongOrders
)

type Objective struct {
	Target    ObjectiveTargetType `json:"t"`
	Limit     ObjectiveLimitType  `json:"l"`
	TargetNum uint64              `json:"x"`
	LimitNum  uint64              `json:"y"`
	Days      uint64              `json:"d"`
}

func (o *Objective) String() string {
	var buf []byte

	switch o.Target {
	case ObjectivePatrons:
		buf = []byte("Attract at least ")
		buf = strconv.AppendUint(buf, o.TargetNum, 10)
		buf = append(buf, " patrons in the first "...)
		buf = strconv.AppendUint(buf, o.Days, 10)
		buf = append(buf, " days"...)
	case ObjectiveBerries:
		buf = []byte("Have at least ")
		buf = strconv.AppendUint(buf, o.TargetNum, 10)
		buf = append(buf, " berries by day "...)
		buf = strconv.AppendUint(buf, o.Days, 10)
	case ObjectiveRating:
		buf = []byte("Have an average rating of at least ")
		buf = strconv.AppendUint(buf, o.TargetNum>>16, 10)
		buf = append(buf, " out of 5 stars on day "...)
		buf = strconv.AppendUint(buf, o.Days, 10)
	case ObjectiveMeals:
		buf = []byte("Serve at least ")
		buf = strconv.AppendUint(buf, o.TargetNum, 10)
		buf = append(buf, " meals by day "...)
		buf = strconv.AppendUint(buf, o.Days, 10)
	}

	switch o.Limit {
	case ObjectiveMenuSize:
		buf = append(buf, " with a menu containing exactly "...)
		buf = strconv.AppendUint(buf, o.LimitNum, 10)
		buf = append(buf, " items"...)
	case ObjectiveMissedPatrons:
		buf = append(buf, " with at most "...)
		buf = strconv.AppendUint(buf, o.LimitNum, 10)
		buf = append(buf, " unserved patrons"...)
	case ObjectiveWrongOrders:
		buf = append(buf, " with at most "...)
		buf = strconv.AppendUint(buf, o.LimitNum, 10)
		buf = append(buf, " incorrect orders"...)
	}

	return string(append(buf, '.'))
}
