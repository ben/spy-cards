package tables

import (
	"context"

	"git.lubar.me/ben/spy-cards/gfx/sprites"
	"git.lubar.me/ben/spy-cards/internal/router"
)

func (s *State) updateForaging(ctx context.Context) (bool, *router.PageInfo, error) {
	// TODO: foraging setup

	return false, nil, nil
}

func (s *State) renderForaging(ctx context.Context) error {
	// TODO: foraging setup

	sprites.DrawTextBorder(s.batch, sprites.FontBubblegumSans, "TODO: foraging setup", 0, 0, 0, 2, 2, sprites.White, sprites.Black, true)

	return nil
}
