// Code generated by "stringer -type=ItemID -linecomment"; DO NOT EDIT.

package tables

import "strconv"

func _() {
	// An "invalid array index" compiler error signifies that the constant values have changed.
	// Re-run the stringer command to generate them again.
	var x [1]struct{}
	_ = x[ItemNone-0]
	_ = x[Mistake-1]
	_ = x[BigMistake-2]
	_ = x[CrunchyLeaf-3]
	_ = x[HoneyDrop-4]
	_ = x[AphidEgg-5]
	_ = x[Mushroom-6]
	_ = x[DangerShroom-7]
	_ = x[AgaricShroom-8]
	_ = x[AphidDew-9]
	_ = x[HardSeed-10]
	_ = x[BagOfFlour-11]
	_ = x[ClearWater-12]
	_ = x[MagicIce-13]
	_ = x[DangerSpud-14]
	_ = x[Squash-15]
	_ = x[SpicyBerry-16]
	_ = x[BurlyBerry-17]
	_ = x[ShockBerry-18]
	_ = x[SucculentBerry-19]
	_ = x[HustleBerry-20]
	_ = x[TangyBerry-21]
	_ = x[DarkCherries-22]
	_ = x[MagicSeed-23]
	_ = x[NumbnailDart-24]
	_ = x[PoisonDart-25]
	_ = x[MysteryBerry-26]
	_ = x[LeafSalad-27]
	_ = x[GlazedHoney-28]
	_ = x[Abomihoney-29]
	_ = x[CookedShroom-30]
	_ = x[CookedDanger-31]
	_ = x[SweetShroom-32]
	_ = x[SweetDanger-33]
	_ = x[FriedEgg-34]
	_ = x[LeafOmelet-35]
	_ = x[HoneydLeaf-36]
	_ = x[RoastedBerries-37]
	_ = x[SucculentPlatter-38]
	_ = x[SucculentCookies-39]
	_ = x[BerryJuice-40]
	_ = x[MushroomSkewer-41]
	_ = x[HeartyBreakfast-42]
	_ = x[MushroomSalad-43]
	_ = x[SweetDew-44]
	_ = x[BakedYam-45]
	_ = x[SpicyFries-46]
	_ = x[BurlyChips-47]
	_ = x[DangerDish-48]
	_ = x[ShavedIce-49]
	_ = x[ColdSalad-50]
	_ = x[IceCream-51]
	_ = x[HoneyIceCream-52]
	_ = x[DryBread-53]
	_ = x[NuttyCake-54]
	_ = x[PoisonCake-55]
	_ = x[FrostPie-56]
	_ = x[YamBread-57]
	_ = x[ShockCandy-58]
	_ = x[MushroomGummies-59]
	_ = x[SweetPudding-60]
	_ = x[LeafCroissant-61]
	_ = x[HoneyPancakes-62]
	_ = x[DrowsyCake-63]
	_ = x[HustleCandy-64]
	_ = x[SpicyCandy-65]
	_ = x[BurlyCandy-66]
	_ = x[BerryJam-67]
	_ = x[HotDrink-68]
	_ = x[PlainTea-69]
	_ = x[SpicyTea-70]
	_ = x[BurlyTea-71]
	_ = x[AphidShake-72]
	_ = x[SquashPuree-73]
	_ = x[SquashCandy-74]
	_ = x[SquashTart-75]
	_ = x[PlumplingPie-76]
	_ = x[TangyJam-77]
	_ = x[TangyJuice-78]
	_ = x[TangyPie-79]
	_ = x[SpicyBomb-80]
	_ = x[BurlyBomb-81]
	_ = x[PoisonBomb-82]
	_ = x[SleepBomb-83]
	_ = x[NumbBomb-84]
	_ = x[FrostBomb-85]
	_ = x[ClearBomb-86]
	_ = x[Abombination-87]
	_ = x[CherryBombs-88]
	_ = x[CherryPie-89]
	_ = x[BerrySmoothie-90]
	_ = x[MiracleShake-91]
	_ = x[TangyCarpaccio-92]
	_ = x[CrisbeeDonut-93]
	_ = x[QueensDinner-94]
	_ = x[MegaRush-95]
	_ = x[MiteBurger-96]
	_ = x[JaydesStew-97]
	_ = x[RandomMysteryBerry1-98]
	_ = x[RandomMysteryBerry2-99]
}

const _ItemID_name = "(none)MistakeBig MistakeCrunchy LeafHoney DropAphid EggMushroomDanger ShroomAgaric ShroomAphid DewHard SeedBag of FlourClear WaterMagic IceDanger SpudSquashSpicy BerryBurly BerryShock BerrySucculent BerryHustle BerryTangy BerryDark CherriesMagic SeedNumbnail DartPoison DartMystery BerryLeaf SaladGlazed HoneyAbomihoneyCooked ShroomCooked DangerSweet ShroomSweet DangerFried EggLeaf OmeletHoney'd LeafRoasted BerriesSucculent PlatterSucculent CookiesBerry JuiceMushroom SkewerHearty BreakfastMushroom SaladSweet DewBaked YamSpicy FriesBurly ChipsDanger DishShaved IceCold SaladIce CreamHoney Ice CreamDry BreadNutty CakePoison CakeFrost PieYam BreadShock CandyMushroom GummiesSweet PuddingLeaf CroissantHoney PancakesDrowsy CakeHustle CandySpicy CandyBurly CandyBerry JamHot DrinkPlain TeaSpicy TeaBurly TeaAphid ShakeSquash PureeSquash CandySquash TartPlumpling PieTangy JamTangy JuiceTangy PieSpicy BombBurly BombPoison BombSleep BombNumb BombFrost BombClear BombAbombinationCherry BombsCherry PieBerry SmoothieMiracle ShakeTangy CarpaccioCrisbee DonutQueen's DinnerMega-Rush™Mite BurgerJayde's StewMB1 (should not be displayed)MB2 (should not be displayed)"

var _ItemID_index = [...]uint16{0, 6, 13, 24, 36, 46, 55, 63, 76, 89, 98, 107, 119, 130, 139, 150, 156, 167, 178, 189, 204, 216, 227, 240, 250, 263, 274, 287, 297, 309, 319, 332, 345, 357, 369, 378, 389, 401, 416, 433, 450, 461, 476, 492, 506, 515, 524, 535, 546, 557, 567, 577, 586, 601, 610, 620, 631, 640, 649, 660, 676, 689, 703, 717, 728, 740, 751, 762, 771, 780, 789, 798, 807, 818, 830, 842, 853, 866, 875, 886, 895, 905, 915, 926, 936, 945, 955, 965, 977, 989, 999, 1013, 1026, 1041, 1054, 1068, 1080, 1091, 1103, 1132, 1161}

func (i ItemID) String() string {
	if i >= ItemID(len(_ItemID_index)-1) {
		return "ItemID(" + strconv.FormatInt(int64(i), 10) + ")"
	}
	return _ItemID_name[_ItemID_index[i]:_ItemID_index[i+1]]
}
