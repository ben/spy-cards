//go:generate go run golang.org/x/tools/cmd/stringer -type=ForagingID -linecomment

package tables

import "git.lubar.me/ben/spy-cards/gfx/sprites"

type ForagingID uint8

const (
	ForagingNone             ForagingID = iota // (none)
	ForagingBugariaOutskirts                   // Bugaria Outskirts
	ForagingSnakemouthDen                      // Snakemouth Den
	ForagingHoneyFactory                       // Honey Factory
	ForagingLostSands                          // Lost Sands
	ForagingGoldenHills                        // Golden Hills
	ForagingForsakenLands                      // Forsaken Lands
)

type ForagingData struct {
	Description string
	Sprite      *sprites.Sprite
	Common      []ItemID
	Uncommon    []ItemID
	Rare        []ItemID
	SuperRare   []ItemID
}

var ForagingLocations = [...]ForagingData{
	ForagingBugariaOutskirts: {
		Description: "Send a team of Explorers to forage for ingredients near the Association.",
		Sprite:      sprites.Blank,
		Common: []ItemID{
			CrunchyLeaf,
			DangerSpud,
		},
		Uncommon: []ItemID{
			HoneyDrop,
			SpicyBerry,
			BurlyBerry,
		},
		Rare: []ItemID{
			AphidEgg,
			Mushroom,
			HardSeed,
			ClearWater,
			NumbnailDart,
			MysteryBerry,
		},
		SuperRare: []ItemID{
			DangerShroom,
			TangyBerry,
			DarkCherries,
			MagicSeed,
		},
	},
	ForagingSnakemouthDen: {
		Description: "Send a team of Explorers to forage for ingredients in Snakemouth Den, which is safer now that that spider has been dealt with.",
		Sprite:      sprites.Blank,
		Common: []ItemID{
			CrunchyLeaf,
			HoneyDrop,
			AphidEgg,
			Mushroom,
		},
		Uncommon: []ItemID{
			SpicyBerry,
			BurlyBerry,
		},
		Rare: []ItemID{
			HardSeed,
			ClearWater,
			MysteryBerry,
		},
		SuperRare: []ItemID{
			DangerShroom,
			DangerSpud,
			TangyBerry,
			DarkCherries,
			MagicSeed,
		},
	},
	ForagingHoneyFactory: {
		Description: "Send a team of Explorers to help out at the Honey Factory.",
		Sprite:      sprites.Blank,
		Common: []ItemID{
			HoneyDrop,
			ShockBerry,
		},
		Uncommon: []ItemID{
			CrunchyLeaf,
			SpicyBerry,
			BurlyBerry,
		},
		Rare: []ItemID{
			AphidEgg,
			Mushroom,
			HardSeed,
			ClearWater,
			MagicSeed,
			MysteryBerry,
		},
		SuperRare: []ItemID{
			DangerShroom,
			DangerSpud,
			TangyBerry,
			DarkCherries,
		},
	},
	ForagingLostSands: {
		Description: "Send a team of Explorers to forage near Defiant Root.",
		Sprite:      sprites.Blank,
		Common: []ItemID{
			AgaricShroom,
			SucculentBerry,
		},
		Uncommon: []ItemID{
			CrunchyLeaf,
			HoneyDrop,
			HardSeed,
			SpicyBerry,
			BurlyBerry,
		},
		Rare: []ItemID{
			AphidEgg,
			Mushroom,
			ClearWater,
			HustleBerry,
			MysteryBerry,
		},
		SuperRare: []ItemID{
			DangerShroom,
			DangerSpud,
			TangyBerry,
			DarkCherries,
			MagicSeed,
		},
	},
	ForagingGoldenHills: {
		Description: "Send a team of Explorers to forage near Golden Settlement.",
		Sprite:      sprites.Blank,
		Common: []ItemID{
			HardSeed,
			ClearWater,
			NumbnailDart,
		},
		Uncommon: []ItemID{
			CrunchyLeaf,
			HoneyDrop,
			SpicyBerry,
			BurlyBerry,
		},
		Rare: []ItemID{
			AphidEgg,
			Mushroom,
			AphidDew,
			PoisonDart,
			MysteryBerry,
		},
		SuperRare: []ItemID{
			DangerShroom,
			DangerSpud,
			TangyBerry,
			DarkCherries,
			MagicSeed,
		},
	},
	ForagingForsakenLands: {
		Description: "Send a team of Explorers to forage near the Termite Kingdom.",
		Sprite:      sprites.Blank,
		Common: []ItemID{
			Squash,
		},
		Uncommon: []ItemID{
			CrunchyLeaf,
			HoneyDrop,
			SpicyBerry,
			BurlyBerry,
		},
		Rare: []ItemID{
			AphidEgg,
			Mushroom,
			HardSeed,
			ClearWater,
			MysteryBerry,
		},
		SuperRare: []ItemID{
			DangerShroom,
			DangerSpud,
			TangyBerry,
			DarkCherries,
			MagicSeed,
		},
	},
}
