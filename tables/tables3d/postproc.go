package tables3d

import (
	_ "embed"
	"math/rand"
	"time"
	"unsafe"

	"git.lubar.me/ben/spy-cards/gfx"
	"golang.org/x/mobile/exp/f32"
	"golang.org/x/mobile/gl"
)

//go:embed postproc2.vs
var postproc2V string

//go:embed postproc2.fs
var postproc2F string

//go:embed postproc3.vs
var postproc3V string

//go:embed postproc3.fs
var postproc3F string

var (
	postprocShader                = gfx.Shader("tables-postproc", postproc2V, postproc2F, postproc3V, postproc3F)
	postprocColorUniform          = postprocShader.Uniform("color")
	postprocNormalUniform         = postprocShader.Uniform("normal")
	postprocDataUniform           = postprocShader.Uniform("data")
	postprocDepthUniform          = postprocShader.Uniform("depth")
	postprocLightUniform          = postprocShader.Uniform("light")
	postprocPerspectiveUniform    = postprocShader.Uniform("perspective")
	postprocInvPerspectiveUniform = postprocShader.Uniform("invPerspective")
	postprocLightViewUniform      = postprocShader.Uniform("lightView")
	postprocInvLightViewUniform   = postprocShader.Uniform("invLightView")
	postprocLightNormUniform      = postprocShader.Uniform("lightNorm")
	postprocKernelUniform         = postprocShader.Uniform("kernel")
	postprocKernelSizeUniform     = postprocShader.Uniform("kernelSize")
	postprocAOSettingsUniform     = postprocShader.Uniform("aoSettings")
	postprocLightSettingsUniform  = postprocShader.Uniform("lightSettings")
	postprocLightAmbientUniform   = postprocShader.Uniform("lightAmbient")
	postprocLightIntensityUniform = postprocShader.Uniform("lightIntensity")
	postprocTimeUniform           = postprocShader.Uniform("time")
	postprocPosAttrib             = postprocShader.Attrib("pos")
	postprocTexAttrib             = postprocShader.Attrib("tex")
)

const m1 = ^uint8(0)

var postprocSquare = gfx.NewStaticBuffer("tables-postproc", []uint8{
	m1, +1, m1, 1, 0, 1,
	m1, m1, m1, 1, 0, 0,
	+1, m1, +1, 1, 1, 0,
	+1, +1, +1, 1, 1, 1,
}, []uint8{0, 0, 1, 0, 2, 0, 0, 0, 2, 0, 3, 0}, 2, func() {
	gfx.GL.EnableVertexAttribArray(postprocPosAttrib.Attrib)
	gfx.GL.EnableVertexAttribArray(postprocTexAttrib.Attrib)

	gfx.GL.VertexAttribPointer(postprocPosAttrib.Attrib, 4, gl.BYTE, false, 6, 0)
	gfx.GL.VertexAttribPointer(postprocTexAttrib.Attrib, 2, gl.BYTE, false, 6, 4)
}, func() {
	gfx.GL.DisableVertexAttribArray(postprocPosAttrib.Attrib)
	gfx.GL.DisableVertexAttribArray(postprocTexAttrib.Attrib)
})

var (
	frameBuffer       [3]gl.Framebuffer
	frameTexture      gl.Texture
	normalTexture     gl.Texture
	dataTexture       gl.Texture
	depthTexture      gl.Texture
	depthRenderBuffer gl.Renderbuffer
	depth2FrameBuffer gl.Framebuffer
	depth2Texture     gl.Texture
	lightFrameBuffer  [3]gl.Framebuffer
	lightTexture      [3]gl.Texture

	lastPostProcW         int
	lastPostProcH         int
	postprocReferenceTime = time.Now()

	_ = gfx.Custom("tables postproc buffers", postprocInit, postprocRelease)
)

func postprocInit() {
	lastPostProcW, lastPostProcH = gfx.PixelSize()
	if lastPostProcW == 0 || lastPostProcH == 0 {
		// resize check will cause another init
		return
	}

	switch gfx.GPULevel {
	case gfx.GPUHigh:
		kernelSize = 16
		lightMinDist = 0.05
		lightMaxDist = 0.2
		skipTransparencyForAO = true
	case gfx.GPUMedium:
		kernelSize = 4
		lightMinDist = 0.1
		lightMaxDist = 0.3
		skipTransparencyForAO = true
	case gfx.GPULow:
		kernelSize = 2
		lightMinDist = 0.2
		lightMaxDist = 0.5
		skipTransparencyForAO = false
	}

	gfx.GL.UseProgram(postprocShader.Program)

	var kernel [64 * 3]float32

	r := rand.New(rand.NewSource(0)) //#nosec

	for i := 0; i < len(kernel); i += 3 {
		kernel[i] = r.Float32()*2 - 1
		kernel[i+1] = r.Float32()*2 - 1
		kernel[i+2] = r.Float32()

		mag := f32.Sqrt(kernel[i]*kernel[i] + kernel[i+1]*kernel[i+1] + kernel[i+2]*kernel[i+2])

		kernel[i] /= mag
		kernel[i+1] /= mag
		kernel[i+2] /= mag
	}

	gfx.GL.Uniform1i(postprocColorUniform.Uniform, 0)
	gfx.GL.Uniform1i(postprocNormalUniform.Uniform, 1)
	gfx.GL.Uniform1i(postprocDataUniform.Uniform, 7)

	gfx.GL.Uniform1iv(postprocLightUniform.Uniform, []int32{4, 5, 6})

	gfx.GL.Uniform3fv(postprocKernelUniform.Uniform, kernel[:])

	frameBuffer[0] = gfx.GL.CreateFramebuffer()
	gfx.GL.BindFramebuffer(gl.FRAMEBUFFER, frameBuffer[0])

	frameTexture = newRenderTexture(true)
	gfx.GL.FramebufferTexture2D(gl.FRAMEBUFFER, gl.COLOR_ATTACHMENT0, gl.TEXTURE_2D, frameTexture, 0)

	normalTexture = newRenderTexture(false)
	gfx.GL.FramebufferTexture2D(gl.FRAMEBUFFER, gl.COLOR_ATTACHMENT1, gl.TEXTURE_2D, normalTexture, 0)

	dataTexture = newRenderTexture(false)
	gfx.GL.FramebufferTexture2D(gl.FRAMEBUFFER, gl.COLOR_ATTACHMENT2, gl.TEXTURE_2D, dataTexture, 0)

	gfx.GL.TexParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST)
	gfx.GL.TexParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST)

	depthTexture = newDepthTexture(false)
	gfx.GL.FramebufferTexture2D(gl.FRAMEBUFFER, gl.DEPTH_STENCIL_ATTACHMENT, gl.TEXTURE_2D, depthTexture, 0)

	setupDrawBuffers()

	if noDrawBuffers {
		frameBuffer[1] = gfx.GL.CreateFramebuffer()
		gfx.GL.BindFramebuffer(gl.FRAMEBUFFER, frameBuffer[1])

		gfx.GL.BindTexture(gl.TEXTURE_2D, normalTexture)
		gfx.GL.FramebufferTexture2D(gl.FRAMEBUFFER, gl.COLOR_ATTACHMENT0, gl.TEXTURE_2D, normalTexture, 0)

		depthRenderBuffer = gfx.GL.CreateRenderbuffer()
		gfx.GL.BindRenderbuffer(gl.RENDERBUFFER, depthRenderBuffer)
		gfx.GL.RenderbufferStorage(gl.RENDERBUFFER, gl.DEPTH_COMPONENT16, lastPostProcW, lastPostProcH)
		gfx.GL.FramebufferRenderbuffer(gl.FRAMEBUFFER, gl.DEPTH_ATTACHMENT, gl.RENDERBUFFER, depthRenderBuffer)

		frameBuffer[2] = gfx.GL.CreateFramebuffer()
		gfx.GL.BindFramebuffer(gl.FRAMEBUFFER, frameBuffer[2])

		gfx.GL.BindTexture(gl.TEXTURE_2D, dataTexture)
		gfx.GL.FramebufferTexture2D(gl.FRAMEBUFFER, gl.COLOR_ATTACHMENT0, gl.TEXTURE_2D, dataTexture, 0)

		gfx.GL.FramebufferRenderbuffer(gl.FRAMEBUFFER, gl.DEPTH_ATTACHMENT, gl.RENDERBUFFER, depthRenderBuffer)
	}

	depth2FrameBuffer = gfx.GL.CreateFramebuffer()
	gfx.GL.BindFramebuffer(gl.FRAMEBUFFER, depth2FrameBuffer)

	depth2Texture = newDepthTexture(false)
	gfx.GL.FramebufferTexture2D(gl.FRAMEBUFFER, gl.DEPTH_STENCIL_ATTACHMENT, gl.TEXTURE_2D, depth2Texture, 0)

	for i := range lightFrameBuffer {
		lightFrameBuffer[i] = gfx.GL.CreateFramebuffer()
		gfx.GL.BindFramebuffer(gl.FRAMEBUFFER, lightFrameBuffer[i])

		lightTexture[i] = newDepthTexture(true)
		gfx.GL.FramebufferTexture2D(gl.FRAMEBUFFER, gl.DEPTH_STENCIL_ATTACHMENT, gl.TEXTURE_2D, lightTexture[i], 0)
	}

	gfx.GL.BindFramebuffer(gl.FRAMEBUFFER, gl.Framebuffer{})

	fastPostprocInit()
}

func postprocCheckResize() {
	w, h := gfx.PixelSize()

	if lastPostProcW != w || lastPostProcH != h {
		postprocRelease()
		postprocInit()
	}
}

func newRenderTexture(srgb bool) gl.Texture {
	w, h := gfx.PixelSize()

	var format, sizedFormat gl.Enum = gl.RGBA, gl.RGBA8

	if srgb && srgbSupported() {
		format = gl.SRGB
		sizedFormat = gl.SRGB8_ALPHA8
	}

	tex := gfx.GL.CreateTexture()
	gfx.GL.BindTexture(gl.TEXTURE_2D, tex)
	gfx.TexStorage2D(gl.TEXTURE_2D, 1, format, gl.UNSIGNED_BYTE, sizedFormat, w, h)
	gfx.GL.TexParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.LINEAR)
	gfx.GL.TexParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR)
	gfx.GL.TexParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE)
	gfx.GL.TexParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE)
	gfx.GL.GenerateMipmap(gl.TEXTURE_2D)

	return tex
}

func newDepthTexture(light bool) gl.Texture {
	w, h := gfx.PixelSize()
	if light {
		w = 512 << gfx.GPULevel
		h = 512 << gfx.GPULevel
	}

	tex := gfx.GL.CreateTexture()
	gfx.GL.BindTexture(gl.TEXTURE_2D, tex)

	gfx.TexStorage2D(gl.TEXTURE_2D, 1, gl.DEPTH_STENCIL, gl.UNSIGNED_INT_24_8, gl.DEPTH24_STENCIL8, w, h)
	gfx.GL.TexParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST)
	gfx.GL.TexParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST)
	gfx.GL.TexParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE)
	gfx.GL.TexParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE)

	return tex
}

func postprocRelease() {
	for i := range frameBuffer {
		if frameBuffer[i] != (gl.Framebuffer{}) {
			gfx.GL.DeleteFramebuffer(frameBuffer[i])
		}
	}

	gfx.GL.DeleteTexture(frameTexture)
	gfx.GL.DeleteTexture(normalTexture)
	gfx.GL.DeleteTexture(dataTexture)
	gfx.GL.DeleteTexture(depthTexture)

	if depthRenderBuffer != (gl.Renderbuffer{}) {
		gfx.GL.DeleteRenderbuffer(depthRenderBuffer)
	}

	gfx.GL.DeleteFramebuffer(depth2FrameBuffer)
	gfx.GL.DeleteTexture(depth2Texture)

	frameBuffer[0] = gl.Framebuffer{}
	frameBuffer[1] = gl.Framebuffer{}
	frameBuffer[2] = gl.Framebuffer{}
	frameTexture = gl.Texture{}
	normalTexture = gl.Texture{}
	dataTexture = gl.Texture{}
	depthTexture = gl.Texture{}
	depthRenderBuffer = gl.Renderbuffer{}
	depth2FrameBuffer = gl.Framebuffer{}
	depth2Texture = gl.Texture{}

	fastPostprocRelease()
}

var (
	kernelSize            int
	aoRadius              float32 = 0.1
	aoMinDist             float32 = 0.25
	aoMaxDist             float32 = 100
	lightMinDist          float32
	lightMaxDist          float32
	nearZ                 float32 = 10
	farZ                  float32 = 150
	skipTransparencyForAO bool
)

func postprocess(proj, invProj *gfx.Matrix, lightProj, invLightProj *[MaxLights]gfx.Matrix, lightAmbient *[3]float32, lightNorm, lightIntensity *[3 * MaxLights]float32) {
	if fastPostprocess(proj, invProj, lightProj, invLightProj, lightAmbient, lightNorm, lightIntensity) {
		return
	}

	gfx.GL.Clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT)

	gfx.GL.UseProgram(postprocShader.Program)

	if skipTransparencyForAO {
		gfx.GL.Uniform1iv(postprocDepthUniform.Uniform, []int32{2, 3})
	} else {
		gfx.GL.Uniform1iv(postprocDepthUniform.Uniform, []int32{2, 2})
	}

	gfx.GL.Uniform1f(postprocTimeUniform.Uniform, float32((time.Since(postprocReferenceTime) % time.Hour).Seconds()))
	gfx.GL.Uniform1i(postprocKernelSizeUniform.Uniform, kernelSize)
	gfx.GL.Uniform3f(postprocAOSettingsUniform.Uniform, aoRadius, aoMinDist, aoMaxDist)
	gfx.GL.Uniform4f(postprocLightSettingsUniform.Uniform, lightMinDist, lightMaxDist, nearZ, farZ)
	gfx.GL.Uniform3f(postprocLightAmbientUniform.Uniform, lightAmbient[0], lightAmbient[1], lightAmbient[2])
	gfx.GL.Uniform3fv(postprocLightIntensityUniform.Uniform, lightIntensity[:])
	gfx.GL.ActiveTexture(gl.TEXTURE7)
	gfx.GL.BindTexture(gl.TEXTURE_2D, dataTexture)
	gfx.GL.ActiveTexture(gl.TEXTURE6)
	gfx.GL.BindTexture(gl.TEXTURE_2D, lightTexture[2])
	gfx.GL.ActiveTexture(gl.TEXTURE5)
	gfx.GL.BindTexture(gl.TEXTURE_2D, lightTexture[1])
	gfx.GL.ActiveTexture(gl.TEXTURE4)
	gfx.GL.BindTexture(gl.TEXTURE_2D, lightTexture[0])
	gfx.GL.ActiveTexture(gl.TEXTURE3)
	gfx.GL.BindTexture(gl.TEXTURE_2D, depth2Texture)
	gfx.GL.ActiveTexture(gl.TEXTURE2)
	gfx.GL.BindTexture(gl.TEXTURE_2D, depthTexture)
	gfx.GL.ActiveTexture(gl.TEXTURE1)
	gfx.GL.BindTexture(gl.TEXTURE_2D, normalTexture)
	gfx.GL.ActiveTexture(gl.TEXTURE0)
	gfx.GL.BindTexture(gl.TEXTURE_2D, frameTexture)
	gfx.GL.UniformMatrix4fv(postprocLightViewUniform.Uniform, (*[unsafe.Sizeof(*lightProj) / unsafe.Sizeof(float32(0))]float32)(unsafe.Pointer(lightProj))[:])
	gfx.GL.UniformMatrix4fv(postprocInvLightViewUniform.Uniform, (*[unsafe.Sizeof(*invLightProj) / unsafe.Sizeof(float32(0))]float32)(unsafe.Pointer(invLightProj))[:])
	gfx.GL.Uniform3fv(postprocLightNormUniform.Uniform, lightNorm[:])
	gfx.GL.UniformMatrix4fv(postprocPerspectiveUniform.Uniform, proj[:])
	gfx.GL.UniformMatrix4fv(postprocInvPerspectiveUniform.Uniform, invProj[:])

	postprocSquare.Bind()

	gfx.GL.DrawElements(gl.TRIANGLES, postprocSquare.Count, gl.UNSIGNED_SHORT, 0)

	postprocSquare.Unbind()
}
