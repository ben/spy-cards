//go:build !js
// +build !js

package tables3d

import "git.lubar.me/ben/spy-cards/gfx"

func (baked *BakedModelObject) fastRender(transform *gfx.Matrix) bool {
	return false
}
