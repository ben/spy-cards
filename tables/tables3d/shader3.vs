#version 300 es

precision highp float;
precision highp int;
precision highp sampler2D;

uniform mat4 perspective;
uniform mat4 camera;
uniform mat4 modelView;
uniform int mode;
uniform bool clampZ;

in vec4 vertex;
in vec2 texCoord;
in vec3 normal;

out vec2 tc;
out vec3 norm;

void main() {
	tc = texCoord;
	norm = normalize((camera * modelView * vec4(normal, 0.0)).xyz);

	gl_Position = perspective * camera * modelView * vertex;

	if (clampZ) {
		// clamp nearZ and farZ instead of clipping
		gl_Position.z = clamp(gl_Position.z, -gl_Position.w, gl_Position.w - 0.000001);
	}

	if (mode == 6) {
		gl_Position = vertex;
		norm = normal;
	}
}
