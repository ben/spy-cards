#version 300 es

precision highp float;

in vec4 pos;
in vec2 tex;

out vec2 texCoord;

void main() {
	texCoord = tex;
	gl_Position = pos;
}
