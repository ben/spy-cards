#version 300 es

precision highp float;
precision highp int;
precision highp sampler2D;

uniform sampler2D tex;
uniform int mode;
uniform int pass;
uniform float time;

in vec2 tc;
in vec3 norm;

layout(location = 0) out vec4 fragColor;
layout(location = 1) out vec4 fragNormal;
layout(location = 2) out vec4 fragData;

void main() {
	int make_sure_pass_is_referenced = pass;

	vec2 texCoord = tc;

	if (mode == 2) {
		texCoord *= 2.0;
		texCoord += time / 256.0;
	} else if (mode == 3) {
		texCoord.y -= time;
	} else if (mode == 4) {
		texCoord.y -= time / 16.0;
	}

	vec4 color = texture(tex, texCoord);
	if (color.a < 0.5) {
		discard;
	}

	if (mode == 1) {
		color.r *= 157.0 / 255.0;
		color.g = 0.0;
	}

	fragColor = vec4(color.rgb / color.a, 1.0);
	fragNormal = vec4(norm * 0.5 + 0.5, 1.0);
	fragData = vec4(0.0, 0.0, 0.0, 1.0);

	if (mode == 2) {
		fragData.x = 1.0 / 255.0;
	} else if (mode == 5) {
		fragData.x = 2.0 / 255.0;
	} else if (mode == 6) {
		fragData.x = 2.0 / 255.0;
	} else if (mode ==  2147483647) {
		fragColor /= 4.0;
		fragNormal = vec4(0.0);
		fragData.z = 1.0 / 255.0;
	} else if (mode ==  2147483646) {
		fragColor /= 8.0;
		fragNormal = vec4(0.0);
	}
}
