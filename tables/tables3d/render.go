package tables3d

import (
	_ "embed"
	"reflect"
	"time"
	"unsafe"

	"git.lubar.me/ben/spy-cards/gfx"
	"git.lubar.me/ben/spy-cards/gfx/sprites"
	"golang.org/x/mobile/gl"
)

//go:embed shader2.vs
var vertex2 string

//go:embed shader2.fs
var fragment2 string

//go:embed shader3.vs
var vertex3 string

//go:embed shader3.fs
var fragment3 string

var (
	shader             = gfx.Shader("tables", vertex2, fragment2, vertex3, fragment3)
	uniformMode        = shader.Uniform("mode")
	uniformPass        = shader.Uniform("pass")
	uniformTime        = shader.Uniform("time")
	uniformPerspective = shader.Uniform("perspective")
	uniformCamera      = shader.Uniform("camera")
	uniformModelView   = shader.Uniform("modelView")
	uniformClampZ      = shader.Uniform("clampZ")
	attribVertex       = shader.Attrib("vertex")
	attribTexCoord     = shader.Attrib("texCoord")
	attribNormal       = shader.Attrib("normal")
)

var arbitrary time.Time = time.Now()

func (baked *BakedModelObject) Render(transform *gfx.Matrix, skipTransparent bool) {
	if skipTransparent && baked.trans {
		return
	}

	if baked.buf == (gl.Buffer{}) {
		var buffer []byte

		bufferHdr := (*reflect.SliceHeader)(unsafe.Pointer(&buffer))
		bufferHdr.Data = uintptr(unsafe.Pointer(&baked.data[0]))
		bufferHdr.Len = len(baked.data) * int(unsafe.Sizeof(baked.data[0]))
		bufferHdr.Cap = len(baked.data) * int(unsafe.Sizeof(baked.data[0]))

		baked.buf = gfx.GL.CreateBuffer()
		gfx.GL.BindBuffer(gl.ARRAY_BUFFER, baked.buf)
		gfx.GL.BufferData(gl.ARRAY_BUFFER, buffer, gl.STATIC_DRAW)
		gfx.StatRecordBufferData(len(buffer))
	}

	_, haveVAO := gfx.GL.(gl.Context3)
	if haveVAO && baked.vao == (gl.VertexArray{}) {
		baked.vao = gfx.GL.CreateVertexArray()

		gfx.GL.BindVertexArray(baked.vao)
		gfx.GL.BindBuffer(gl.ARRAY_BUFFER, baked.buf)
		gfx.GL.EnableVertexAttribArray(attribVertex.Attrib)
		gfx.GL.EnableVertexAttribArray(attribTexCoord.Attrib)
		gfx.GL.EnableVertexAttribArray(attribNormal.Attrib)
		gfx.GL.VertexAttribPointer(attribVertex.Attrib, 3, gl.FLOAT, false, 28, 0)
		gfx.GL.VertexAttribPointer(attribTexCoord.Attrib, 2, gl.UNSIGNED_SHORT, true, 28, 12)
		gfx.GL.VertexAttribPointer(attribNormal.Attrib, 3, gl.FLOAT, false, 28, 16)
	}

	if baked.fastRender(transform) {
		return
	}

	// compute the determinant of the matrix so we can see if we need to render inside-out
	if transform.Determinant() < 0 {
		gfx.GL.FrontFace(gl.CW)
	} else {
		gfx.GL.FrontFace(gl.CCW)
	}

	gfx.StatRecordTextureUse(baked.tex)
	gfx.GL.BindTexture(gl.TEXTURE_2D, baked.tex.LazyTexture(sprites.Blank.Texture()))
	gfx.GL.Uniform1i(uniformMode.Uniform, baked.mode)
	gfx.GL.UniformMatrix4fv(uniformModelView.Uniform, transform[:])

	if haveVAO {
		gfx.GL.BindVertexArray(baked.vao)
	} else {
		gfx.GL.BindBuffer(gl.ARRAY_BUFFER, baked.buf)
		gfx.GL.EnableVertexAttribArray(attribVertex.Attrib)
		gfx.GL.EnableVertexAttribArray(attribTexCoord.Attrib)
		gfx.GL.EnableVertexAttribArray(attribNormal.Attrib)
		gfx.GL.VertexAttribPointer(attribVertex.Attrib, 3, gl.FLOAT, false, 28, 0)
		gfx.GL.VertexAttribPointer(attribTexCoord.Attrib, 2, gl.UNSIGNED_SHORT, true, 28, 12)
		gfx.GL.VertexAttribPointer(attribNormal.Attrib, 3, gl.FLOAT, false, 28, 16)
	}

	if baked.mode > 1<<30 {
		gfx.GL.BlendFunc(gl.ONE, gl.ONE)
		gfx.GL.DepthMask(false)

		defer func() {
			gfx.GL.BlendFunc(gl.ONE, gl.ONE_MINUS_SRC_ALPHA)
			gfx.GL.DepthMask(true)
		}()
	}

	gfx.GL.DrawArrays(gl.TRIANGLES, 0, len(baked.data))
	gfx.StatRecordTriangleBatch(len(baked.data))
}

func (i *ModelInstance) ensureInit() bool {
	if i.obj == nil {
		iobj := Object(i.Name)
		if iobj == nil {
			return false
		}

		i.obj = iobj
		i.setMatrix()
	}

	return true
}

func (i *ModelInstance) setMatrix() {
	var m0, m1, m2 gfx.Matrix

	m0.Translation(i.X, i.Y, i.Z)
	m1.RotationY(i.Ry)
	m2.Multiply(&m0, &m1)
	m0.RotationZ(i.Rz)
	m1.Multiply(&m2, &m0)
	m2.RotationX(i.Rx)
	m0.Multiply(&m1, &m2)
	m1.Scale(i.Sx, i.Sy, i.Sz)
	i.mat.Multiply(&m0, &m1)
}
