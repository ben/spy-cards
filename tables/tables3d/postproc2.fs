#extension GL_EXT_frag_depth : enable

// SSAO based on https://github.com/McNopper/OpenGL/blob/master/Example28/shader/ssao.frag.glsl

#ifdef GL_FRAGMENT_PRECISION_HIGH
precision highp float;
precision highp sampler2D;
#else
precision mediump float;
precision mediump sampler2D;
#endif

#define NUM_LIGHTS 3
#define MAX_KERNEL_SIZE 64

uniform sampler2D color;
uniform sampler2D normal;
uniform sampler2D data;
uniform sampler2D depth[2];
uniform sampler2D light[NUM_LIGHTS];

uniform mat4 perspective;
uniform mat4 invPerspective;
uniform mat4 lightView[NUM_LIGHTS];
uniform mat4 invLightView[NUM_LIGHTS];
uniform vec3 lightNorm[NUM_LIGHTS];
uniform vec3 lightIntensity[NUM_LIGHTS];

uniform vec3 kernel[MAX_KERNEL_SIZE];
uniform int kernelSize;
uniform vec3 aoSettings; // radius, min distance, max distance
uniform vec4 lightSettings; // min/max distance to consider shadow, nearZ, farZ
uniform vec3 lightAmbient;
uniform float time;

varying vec2 texCoord;

// https://oeis.org/A001622
const float PHI = 1.61803398874989484820458683436563811772030917980576286213544862;

float noise(vec2 xy, float seed) {
	return fract(tan(distance(xy * PHI, xy) * seed) * xy.x);
}

float linearize(float z) {
	return 2.0 * lightSettings.z * lightSettings.w / (lightSettings.w + lightSettings.z - z * (lightSettings.w - lightSettings.z));
}

void main() {
	float viewDepth = texture2D(depth[0], texCoord).r;
	vec4 posView = vec4(texCoord, viewDepth, 1.0);
	posView = posView * 2.0 - 1.0;
	posView = invPerspective * posView;
	posView /= posView.w;
	vec3 viewNormal = normalize(texture2D(normal, texCoord).xyz * 2.0 - 1.0);
	vec3 viewTangent = normalize(cross(viewNormal, vec3(0, 0, 1)));
	vec3 viewBitangent = normalize(cross(viewNormal, viewTangent));
	mat3 viewMatrix = mat3(viewTangent, viewBitangent, viewNormal);
	vec4 baseColor = texture2D(color, texCoord);
	ivec4 fragData = ivec4(texture2D(data, texCoord) * 255.0);

	float occlusion = 0.0;

	for (int i = 0; i < MAX_KERNEL_SIZE; i++) {
		if (i >= kernelSize) {
			break;
		}

		// Reorient sample vector in view space ...
		vec3 sampleVectorView = viewMatrix * kernel[i];

		// ... and calculate sample point.
		vec4 samplePointView = posView + aoSettings.x * vec4(sampleVectorView, 0.0);

		// Project point and calculate NDC.

		vec4 samplePointNDC = perspective * samplePointView;

		samplePointNDC /= samplePointNDC.w;

		// Create texture coordinate out of it.

		vec2 samplePointTexCoord = samplePointNDC.xy * 0.5 + 0.5;

		// Get sample out of depth texture

		float zSceneNDC = texture2D(depth[1], samplePointTexCoord).r * 2.0 - 1.0;

		float delta = linearize(samplePointNDC.z) - linearize(zSceneNDC);

		// If scene fragment is before (smaller in z) sample point, increase occlusion.
		if (delta > aoSettings.y && delta < aoSettings.z) {
			occlusion += 1.0 / float(kernelSize);
		}
	}

	vec3 lightContribution = lightAmbient;

	for (int i = 0; i < NUM_LIGHTS; i++) {
		if (kernelSize == 0) {
			float lightDot = dot(viewNormal, normalize(lightNorm[i]));
			lightContribution += sqrt(lightDot * 0.25 + 0.75) * lightIntensity[i];
		}

		for (int j = 0; j < MAX_KERNEL_SIZE; j++) {
			if (j >= kernelSize) {
				break;
			}

			vec3 sampleVectorView = viewMatrix * kernel[j];
			vec4 sampleView = posView + 0.05 * vec4(sampleVectorView, 0.0);

			vec4 lightTexCoord = lightView[i] * sampleView;
			lightTexCoord /= lightTexCoord.w;
			lightTexCoord.st = lightTexCoord.st * 0.5 + 0.5;
			vec4 lightWorldCoord = vec4(lightTexCoord.st * 2.0 - 1.0, 0.0, 1.0);

			if (i == 0) {
				lightWorldCoord.z = texture2D(light[0], lightTexCoord.st).r * 2.0 - 1.0;
			} else if (i == 1) {
				lightWorldCoord.z = texture2D(light[1], lightTexCoord.st).r * 2.0 - 1.0;
			} else if (i == 2) {
				lightWorldCoord.z = texture2D(light[2], lightTexCoord.st).r * 2.0 - 1.0;
			}

			lightWorldCoord = invLightView[i] * lightWorldCoord;
			lightWorldCoord /= lightWorldCoord.w;

			float lightDot = dot(viewNormal, normalize(lightNorm[i]));
			float lightDist = length(lightWorldCoord - sampleView);

			if (fragData.z > 0 && lightDist > 0.0) {
				lightDist /= 32.0 * float(fragData.z);
			}

			lightContribution += clamp(lightDot * 2.0, 0.0, 1.0) * clamp(1.0 - (lightSettings.x - lightDist) / (lightSettings.x - lightSettings.y), 0.0, 1.0) * lightIntensity[i] / float(kernelSize);
		}
	}

	if (fragData.x == 1) {
		float watersurf = 0.0;

		for (int i = 0; i < MAX_KERNEL_SIZE; i++) {
			if (i >= kernelSize) {
				break;
			}

			vec3 sampleVectorView = viewMatrix * -kernel[i];

			vec4 samplePointView = posView + aoSettings.x * vec4(sampleVectorView, 0.0);
			vec4 samplePointNDC = perspective * samplePointView;
			samplePointNDC /= samplePointNDC.w;
			samplePointNDC.z = linearize(samplePointNDC.z);
			vec2 samplePointTexCoord = samplePointNDC.xy * 0.5 + 0.5;
			float zSceneNDC = linearize(texture2D(depth[1], samplePointTexCoord).r * 2.0 - 1.0);
			float delta = samplePointNDC.z - zSceneNDC;
			if (abs(delta) < 2.0) {
				watersurf += 1.0 / float(kernelSize);
			}
		}

		baseColor = mix(baseColor, vec4(1.0), watersurf);
	}

	for (int dx = -3; dx <= 3; dx++) {
		for (int dy = -3; dy <= 3; dy++) {
			if (dx*dx + dy*dy <= 9 && (dx != 0 || dy != 0)) {
				vec2 tc = texCoord + vec2(float(dx), float(dy)) * 0.001;
				ivec4 nearFragData = ivec4(texture2D(data, tc) * 255.0);
				vec4 nearColorData = texture2D(color, tc);

				if (nearFragData.x == 2) {
					lightContribution += nearColorData.rgb / 16.0;
				}
			}
		}
	}

	gl_FragColor = vec4(vec3(1.0 - occlusion) * lightContribution, 1.0) * baseColor;
#ifdef GL_EXT_frag_depth
	gl_FragDepthEXT = viewDepth;
#endif
}
