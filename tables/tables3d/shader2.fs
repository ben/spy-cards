#extension GL_EXT_draw_buffers : enable

#ifdef GL_FRAGMENT_PRECISION_HIGH
precision highp float;
precision highp int;
precision highp sampler2D;
#else
precision mediump float;
precision mediump int;
precision mediump sampler2D;
#endif

uniform sampler2D tex;
uniform int mode;
uniform int pass;
uniform float time;

varying vec2 tc;
varying vec3 norm;

void main() {
	int make_sure_pass_is_referenced = pass;

	vec2 texCoord = tc;

	if (mode == 2) {
		texCoord *= 2.0;
		texCoord += time / 256.0;
	} else if (mode == 3) {
		texCoord.y -= time;
	} else if (mode == 4) {
		texCoord.y -= time / 16.0;
	}

	vec4 color = texture2D(tex, texCoord);
	if (color.a < 0.5) {
		discard;
	}

	if (mode == 1) {
		color.r *= 157.0 / 255.0;
		color.g = 0.0;
	}

	vec4 fragData[3];

	fragData[0] =  vec4(color.rgb / color.a, 1.0);
	fragData[1] = vec4(norm * 0.5 + 0.5, 1.0);
	fragData[2] = vec4(0.0, 0.0, 0.0, 1.0);

	if (mode == 2) {
		fragData[2].x = 1.0 / 255.0;
	} else if (mode == 5) {
		fragData[2].x = 2.0 / 255.0;
	} else if (mode == 6) {
		fragData[2].x = 2.0 / 255.0;
	} else if (mode ==  2147483647) {
		fragData[0] /= 4.0;
		fragData[1] = vec4(0.0);
		fragData[2].z = 1.0 / 255.0;
	} else if (mode ==  2147483646) {
		fragData[0] /= 8.0;
		fragData[1] = vec4(0.0);
	}

	vec4 passFrag = fragData[0];
	if (pass == 1) {
		passFrag = fragData[1];
	} else if (pass == 2) {
		passFrag = fragData[2];
	}

#ifdef GL_EXT_draw_buffers
	gl_FragData[0] = passFrag;
	gl_FragData[1] = fragData[1];
	gl_FragData[2] = fragData[2];
#else
	gl_FragColor = passFrag;
#endif
}
