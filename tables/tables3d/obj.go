package tables3d

import (
	"bufio"
	"embed"
	"encoding/binary"
	"errors"
	"fmt"
	"io"
	"log"
	"math"
	"reflect"
	"strconv"
	"strings"
	"sync"
	"unsafe"

	"git.lubar.me/ben/spy-cards/gfx"
	"git.lubar.me/ben/spy-cards/gfx/sprites"
)

//go:embed model/*.mdl
var mdlFiles embed.FS // in the model directory, create a .mdl file and run go generate

var (
	objModelsOnce sync.Once
	objModels     map[string]*ModelObject
	warnedSprite  = make(map[string]bool)
)

func initObjModels() {
	objModelsOnce.Do(func() {
		objModels = parseModels(&mdlFiles)
	})
}

func NewObject(name string) *ModelObject {
	initObjModels()

	if _, ok := objModels[name]; ok {
		panic("tables3d: duplicate object name " + name)
	}

	o := &ModelObject{
		Name: name,
	}

	objModels[name] = o

	return o
}

func SceneBaseObject(name string) *ModelObject {
	return sceneObject(name, "_Base", false)
}

func SceneFurnitureObject(name string) *ModelObject {
	return sceneObject(name, "_Furniture", true)
}

func sceneObject(name, suffix string, furniture bool) *ModelObject {
	initObjModels()

	if m, ok := objModels[name+suffix]; ok {
		return m
	}

	base := Object(name)

	m := NewObject(name + suffix)

	if !furniture {
		m.Material = base.Material
		m.ShaderMode = base.ShaderMode
		m.Transparent = base.Transparent
		m.Tex = base.Tex

		m.Buffer = make([]BufferData, len(base.Buffer))
		copy(m.Buffer, base.Buffer)
	}

	m.Instances = make([]*ModelInstance, 0, len(base.Instances))

	for _, i := range base.Instances {
		if strings.HasPrefix(i.Name, "Furniture") == furniture {
			m.Instances = append(m.Instances, &ModelInstance{
				Name: i.Name,

				X:  i.X,
				Y:  i.Y,
				Z:  i.Z,
				Rx: i.Rx,
				Ry: i.Ry,
				Rz: i.Rz,
				Sx: i.Sx,
				Sy: i.Sy,
				Sz: i.Sz,
			})
		}
	}

	return m
}

func Object(name string) *ModelObject {
	initObjModels()

	if strings.HasPrefix(name, "Sprite") {
		s := SpriteObject(name[len("Sprite"):], nil)
		if s != nil {
			return s
		}

		if !warnedSprite[name] {
			warnedSprite[name] = true

			log.Println("WARNING: missing sprite object:", name)
		}

		return nil
	}

	o, ok := objModels[name]
	if !ok {
		objModels[name] = nil // only warn once

		log.Println("WARNING: missing object model:", name)
	}

	return o
}

func parseModels(fs *embed.FS) map[string]*ModelObject {
	files, err := fs.ReadDir("model")
	if err != nil {
		panic(err)
	}

	m := make(map[string]*ModelObject, len(files))

	objects := make([]ModelObject, len(files))

	for i, fi := range files {
		b, err := fs.ReadFile("model/" + fi.Name())
		if err != nil {
			panic(err)
		}

		nameLen, n := binary.Uvarint(b)
		b = b[n:]
		objects[i].Name = string(b[:nameLen])
		b = b[nameLen:]

		if _, ok := m[objects[i].Name]; ok {
			panic("tables3d: multiple definitions of model: " + objects[i].Name)
		}

		m[objects[i].Name] = &objects[i]

		matLen, n := binary.Uvarint(b)
		b = b[n:]
		objects[i].Material = string(b[:matLen])
		b = b[matLen:]

		numInstances, n := binary.Uvarint(b)
		b = b[n:]

		instances := make([]ModelInstance, numInstances)
		objects[i].Instances = make([]*ModelInstance, numInstances)

		for j := range objects[i].Instances {
			objects[i].Instances[j] = &instances[j]

			nameLen, n := binary.Uvarint(b)
			b = b[n:]
			objects[i].Instances[j].Name = string(b[:nameLen])
			b = b[nameLen:]

			params := (*[3 * 3 * unsafe.Sizeof(float32(0))]byte)(unsafe.Pointer(&objects[i].Instances[j].X))
			b = b[copy(params[:], b):]
		}

		if len(b) != 0 {
			if len(b)%(3*int(unsafe.Sizeof(BufferData{}))) != 0 {
				panic("tables3d: loading object " + objects[i].Name + ": invalid buffer length")
			}

			objects[i].Buffer = make([]BufferData, len(b)/int(unsafe.Sizeof(BufferData{})))

			var bufferBytes []byte
			bufferHdr := (*reflect.SliceHeader)(unsafe.Pointer(&bufferBytes))
			bufferHdr.Data = uintptr(unsafe.Pointer(&objects[i].Buffer[0]))
			bufferHdr.Len = len(b)
			bufferHdr.Cap = len(b)

			copy(bufferBytes, b)
		}

		if len(objects[i].Buffer) != 0 {
			if err := objects[i].setMaterial(); err != nil {
				panic("tables3d: failed to set material for model: " + objects[i].Name + ": " + err.Error())
			}
		}
	}

	for _, o := range m {
		for _, i := range o.Instances {
			if _, ok := m[i.Name]; !ok {
				panic("tables3d: missing model referenced by instance: " + i.Name)
			}
		}
	}

	return m
}

type Model struct {
	// v: List of geometric vertices, with (x, y, z [,w]) coordinates.
	//    w is optional and defaults to 1.0.
	Vertex []gfx.Vector
	// vt: List of texture coordinates, in (u, [,v ,w]) coordinates.
	//     These will vary between 0 and 1. v, w are optional and default to 0.
	//     We don't use w, so it is omitted from the parsed data.
	TexCoord [][2]float32
	// vn: List of vertex Normals in (x,y,z) form; Normals might not be unit vectors.
	Normals [][3]float32
	Objects []*ModelObjectFaces
}

type ModelObjectFaces struct {
	*ModelObject

	// f: List of Faces. For simplicity, we assume Faces are always triangles.
	Faces [][3]ModelFace
}

type ModelObject struct {
	Name        string
	Material    string
	Tex         *gfx.AssetTexture
	ShaderMode  int
	Transparent bool

	// i: vendor-specific (Spy Cards Online) extension: instance of other object.
	//    i Name x y z rx ry rz sx sy sz
	Instances []*ModelInstance

	Buffer []BufferData

	baked []*BakedModelObject
}

type ModelFace struct {
	// each of these are 1 more than an index into the corresponding slice or 0.
	Vertex   int
	TexCoord int
	Normal   int
}

type ModelInstance struct {
	Name       string
	X, Y, Z    float32
	Rx, Ry, Rz float32
	Sx, Sy, Sz float32

	obj *ModelObject
	mat gfx.Matrix
}

type BufferData struct {
	X, Y, Z    float32
	S, T       uint16
	Nx, Ny, Nz float32
}

func ParseObj(r io.Reader) (*Model, error) {
	s := bufio.NewScanner(r)

	m := &Model{}

	var currentObj *ModelObjectFaces

	for s.Scan() {
		split := strings.Split(s.Text(), " ")

		if len(split) == 0 {
			continue // ignore blank lines
		}

		switch split[0] {
		case "#":
			// ignore comments
		case "mtllib":
			// ignore material library (the materials are hard-coded)
		case "o":
			currentObj = &ModelObjectFaces{
				ModelObject: &ModelObject{
					Name: strings.Join(split[1:], " "),
				},
			}

			m.Objects = append(m.Objects, currentObj)
		case "i":
			// vendor (Spy Cards Online) specific extension: instance
			inst := &ModelInstance{}

			if len(split) != 11 {
				return nil, errors.New("tables3d: invalid instance")
			}

			inst.Name = split[1]

			x, err := strconv.ParseFloat(split[2], 32)
			if err != nil {
				return nil, fmt.Errorf("tables3d: in instance x coordinate: %w", err)
			}

			y, err := strconv.ParseFloat(split[3], 32)
			if err != nil {
				return nil, fmt.Errorf("tables3d: in instance y coordinate: %w", err)
			}

			z, err := strconv.ParseFloat(split[4], 32)
			if err != nil {
				return nil, fmt.Errorf("tables3d: in instance z coordinate: %w", err)
			}

			inst.X = float32(x)
			inst.Y = float32(y)
			inst.Z = float32(z)

			rx, err := strconv.ParseFloat(split[5], 32)
			if err != nil {
				return nil, fmt.Errorf("tables3d: in instance rx coordinate: %w", err)
			}

			ry, err := strconv.ParseFloat(split[6], 32)
			if err != nil {
				return nil, fmt.Errorf("tables3d: in instance ry coordinate: %w", err)
			}

			rz, err := strconv.ParseFloat(split[7], 32)
			if err != nil {
				return nil, fmt.Errorf("tables3d: in instance rz coordinate: %w", err)
			}

			inst.Rx = float32(rx)
			inst.Ry = float32(ry)
			inst.Rz = float32(rz)

			sx, err := strconv.ParseFloat(split[8], 32)
			if err != nil {
				return nil, fmt.Errorf("tables3d: in instance sx coordinate: %w", err)
			}

			sy, err := strconv.ParseFloat(split[9], 32)
			if err != nil {
				return nil, fmt.Errorf("tables3d: in instance sy coordinate: %w", err)
			}

			sz, err := strconv.ParseFloat(split[10], 32)
			if err != nil {
				return nil, fmt.Errorf("tables3d: in instance sz coordinate: %w", err)
			}

			inst.Sx = float32(sx)
			inst.Sy = float32(sy)
			inst.Sz = float32(sz)

			currentObj.Instances = append(currentObj.Instances, inst)
		case "v":
			vec := gfx.Vector{0, 0, 0, 1}

			if len(split) == 5 {
				w, err := strconv.ParseFloat(split[4], 32)
				if err != nil {
					return nil, fmt.Errorf("tables3d: in vertex w coordinate: %w", err)
				}

				if w != 1 {
					return nil, errors.New("tables3d: w coordinate must equal 1")
				}

				vec[3] = float32(w)
			} else if len(split) != 4 {
				return nil, errors.New("tables3d: invalid format for vertex")
			}

			x, err := strconv.ParseFloat(split[1], 32)
			if err != nil {
				return nil, fmt.Errorf("tables3d: in vertex x coordinate: %w", err)
			}

			y, err := strconv.ParseFloat(split[2], 32)
			if err != nil {
				return nil, fmt.Errorf("tables3d: in vertex y coordinate: %w", err)
			}

			z, err := strconv.ParseFloat(split[3], 32)
			if err != nil {
				return nil, fmt.Errorf("tables3d: in vertex z coordinate: %w", err)
			}

			vec[0] = float32(x)
			vec[1] = float32(y)
			vec[2] = float32(z)

			m.Vertex = append(m.Vertex, vec)
		case "vt":
			var texCoord [2]float32

			if len(split) == 4 {
				return nil, errors.New("tables3d: 3-component texture coordinates are not supported")
			}

			if len(split) == 3 {
				y, err := strconv.ParseFloat(split[2], 32)
				if err != nil {
					return nil, fmt.Errorf("tables3d: in texture t coordinate: %w", err)
				}

				texCoord[1] = 1 - float32(y)
			} else if len(split) != 2 {
				return nil, errors.New("tables3d: invalid format for texture coordinate")
			}

			x, err := strconv.ParseFloat(split[1], 32)
			if err != nil {
				return nil, fmt.Errorf("tables3d: in texture s coordinate: %w", err)
			}

			texCoord[0] = float32(x)

			m.TexCoord = append(m.TexCoord, texCoord)
		case "vn":
			var normal [3]float32

			if len(split) != 4 {
				return nil, errors.New("tables3d: invalid format for normal")
			}

			x, err := strconv.ParseFloat(split[1], 32)
			if err != nil {
				return nil, fmt.Errorf("tables3d: in normal x coordinate: %w", err)
			}

			y, err := strconv.ParseFloat(split[2], 32)
			if err != nil {
				return nil, fmt.Errorf("tables3d: in normal y coordinate: %w", err)
			}

			z, err := strconv.ParseFloat(split[3], 32)
			if err != nil {
				return nil, fmt.Errorf("tables3d: in normal z coordinate: %w", err)
			}

			normal[0] = float32(x)
			normal[1] = float32(y)
			normal[2] = float32(z)

			m.Normals = append(m.Normals, normal)
		case "usemtl":
			if len(split) != 2 {
				return nil, errors.New("tables3d: invalid format for material")
			}

			if currentObj.Tex != nil {
				return nil, errors.New("tables3d: multiple materials per object are not supported")
			}

			currentObj.Material = split[1]

			if err := currentObj.setMaterial(); err != nil {
				return nil, err
			}
		case "s":
			// ignore smoothing groups
		case "l":
			// ignore lines
		case "f":
			if len(split) != 4 {
				return nil, errors.New("tables3d: invalid format for face")
			}

			var face [3]ModelFace

			for i := range face {
				faceSplit := strings.Split(split[i+1], "/")

				var err error

				face[i].Vertex, err = strconv.Atoi(faceSplit[0])
				if err != nil {
					return nil, fmt.Errorf("tables3d: invalid vertex index: %w", err)
				}

				if len(faceSplit[1]) != 0 {
					face[i].TexCoord, err = strconv.Atoi(faceSplit[1])
					if err != nil {
						return nil, fmt.Errorf("tables3d: invalid texture coordinate index: %w", err)
					}
				}

				if len(faceSplit) == 3 {
					face[i].Normal, err = strconv.Atoi(faceSplit[2])
					if err != nil {
						return nil, fmt.Errorf("tables3d: invalid normal index: %w", err)
					}
				} else if len(faceSplit) != 2 {
					return nil, errors.New("tables3d: invalid format for face")
				}
			}

			currentObj.Faces = append(currentObj.Faces, face)
		default:
			return nil, fmt.Errorf("tables3d: unhandled obj command type: %q", split[0])
		}
	}

	if err := s.Err(); err != nil {
		return nil, fmt.Errorf("tables3d: error reading obj file: %w", err)
	}

	for i := range m.Objects {
		if len(m.Objects[i].Faces) == 0 {
			continue
		}

		if m.Objects[i].Tex == nil {
			return nil, fmt.Errorf("tables3d: object missing material")
		}

		m.Objects[i].Buffer = make([]BufferData, len(m.Objects[i].Faces)*3)

		for j := range m.Objects[i].Faces {
			for k := range m.Objects[i].Faces[j] {
				vertex := &m.Objects[i].Buffer[j*3+k]

				if m.Objects[i].Faces[j][k].Vertex == 0 {
					return nil, errors.New("tables3d: missing vertex index")
				}

				if m.Objects[i].Faces[j][k].Vertex > len(m.Vertex) {
					return nil, errors.New("tables3d: out-of-range vertex index")
				}

				vertex.X = m.Vertex[m.Objects[i].Faces[j][k].Vertex-1][0]
				vertex.Y = m.Vertex[m.Objects[i].Faces[j][k].Vertex-1][1]
				vertex.Z = m.Vertex[m.Objects[i].Faces[j][k].Vertex-1][2]

				if m.Objects[i].Faces[j][k].TexCoord > len(m.TexCoord) {
					return nil, errors.New("tables3d: out-of-range texture coordinate index")
				}

				if m.Objects[i].Faces[j][k].TexCoord != 0 {
					vertex.S = uint16(math.Mod(float64(m.TexCoord[m.Objects[i].Faces[j][k].TexCoord-1][0]*0xffff), 0x10000))
					vertex.T = uint16(math.Mod(float64(m.TexCoord[m.Objects[i].Faces[j][k].TexCoord-1][1]*0xffff), 0x10000))
				}

				if m.Objects[i].Faces[j][k].Normal > len(m.Normals) {
					return nil, errors.New("tables3d: out-of-range normal index")
				}

				if m.Objects[i].Faces[j][k].Normal != 0 {
					vertex.Nx = m.Normals[m.Objects[i].Faces[j][k].Normal-1][0]
					vertex.Ny = m.Normals[m.Objects[i].Faces[j][k].Normal-1][1]
					vertex.Nz = m.Normals[m.Objects[i].Faces[j][k].Normal-1][2]
				}
			}
		}
	}

	return m, nil
}

func (o *ModelObject) setMaterial() error {
	switch o.Material {
	case "3DMain":
		o.Transparent = false
		o.Tex = sprites.Main1
	case "3DMain2":
		o.Transparent = false
		o.Tex = sprites.Main2
	case "MainPlane":
		o.Transparent = true
		o.Tex = sprites.Main1
	case "PurplePlane":
		o.Transparent = true
		o.Tex = sprites.Main1
		o.ShaderMode = 1
	case "TreeTex":
		o.Transparent = true
		o.Tex = sprites.TreeTex
	case "OutlineWater":
		o.Transparent = true
		o.Tex = sprites.Water3
		o.ShaderMode = 2
	case "Waterfall":
		o.Transparent = true
		o.Tex = sprites.Water1
		o.ShaderMode = 3
	case "CheapWater":
		o.Transparent = true
		o.Tex = sprites.Water3
		o.ShaderMode = 4
	case "Crystal":
		o.Transparent = false
		o.Tex = sprites.Hue
		o.ShaderMode = 5
	case "glass":
		o.Transparent = true
		o.Tex = sprites.Hue
		o.ShaderMode = 1<<31 - 1
	case "LightBeam":
		o.Transparent = true
		o.Tex = sprites.Hue
		o.ShaderMode = 1<<31 - 2
	case "Grass":
		o.Transparent = true
		o.Tex = sprites.Grass
	default:
		return fmt.Errorf("tables3d: unhandled material name: %q", o.Material)
	}

	return nil
}

var (
	spriteObjects = make(map[*sprites.Sprite]*ModelObject)
	spriteByName  = make(map[string]*ModelObject)
)

func SpriteObject(name string, s *sprites.Sprite) *ModelObject {
	if s == nil {
		return spriteByName[name]
	}

	if o, ok := spriteObjects[s]; ok {
		return o
	}

	if name == "" {
		return nil
	}

	o := &ModelObject{
		Name:        "Sprite" + name,
		Tex:         s.AssetTexture,
		Transparent: true,
		Buffer:      make([]BufferData, 12),
	}

	o.Buffer[0] = BufferData{
		X: s.X0, Y: s.Y0, Z: 0,
		S: uint16(s.S0 * 0xffff), T: uint16(s.T1 * 0xffff),
		Nx: 0, Ny: 0, Nz: 1,
	}
	o.Buffer[1] = BufferData{
		X: s.X1, Y: s.Y0, Z: 0,
		S: uint16(s.S1 * 0xffff), T: uint16(s.T1 * 0xffff),
		Nx: 0, Ny: 0, Nz: 1,
	}
	o.Buffer[2] = BufferData{
		X: s.X0, Y: s.Y1, Z: 0,
		S: uint16(s.S0 * 0xffff), T: uint16(s.T0 * 0xffff),
		Nx: 0, Ny: 0, Nz: 1,
	}
	o.Buffer[3] = o.Buffer[2]
	o.Buffer[4] = o.Buffer[1]
	o.Buffer[5] = BufferData{
		X: s.X1, Y: s.Y1, Z: 0,
		S: uint16(s.S1 * 0xffff), T: uint16(s.T0 * 0xffff),
		Nx: 0, Ny: 0, Nz: 1,
	}
	o.Buffer[6] = BufferData{
		X: s.X0, Y: s.Y0, Z: 0,
		S: uint16(s.S0 * 0xffff), T: uint16(s.T1 * 0xffff),
		Nx: 0, Ny: 0, Nz: -1,
	}
	o.Buffer[7] = BufferData{
		X: s.X0, Y: s.Y1, Z: 0,
		S: uint16(s.S0 * 0xffff), T: uint16(s.T0 * 0xffff),
		Nx: 0, Ny: 0, Nz: -1,
	}
	o.Buffer[8] = BufferData{
		X: s.X1, Y: s.Y0, Z: 0,
		S: uint16(s.S1 * 0xffff), T: uint16(s.T1 * 0xffff),
		Nx: 0, Ny: 0, Nz: -1,
	}
	o.Buffer[9] = o.Buffer[8]
	o.Buffer[10] = o.Buffer[7]
	o.Buffer[11] = BufferData{
		X: s.X1, Y: s.Y1, Z: 0,
		S: uint16(s.S1 * 0xffff), T: uint16(s.T0 * 0xffff),
		Nx: 0, Ny: 0, Nz: -1,
	}

	spriteObjects[s] = o
	spriteByName[name] = o

	return o
}

func init() {
	SpriteObject("TablesFry", sprites.TablesFry)
	SpriteObject("TablesDoppel", sprites.TablesDoppel)
	SpriteObject("TablesCrisbee", sprites.TablesCrisbee)
	SpriteObject("TablesKut", sprites.TablesKut)
	SpriteObject("TablesJayde", sprites.TablesJayde)
	SpriteObject("TablesMasterSlice", sprites.TablesMasterSlice)
	SpriteObject("TablesMicrowave", sprites.TablesMicrowave)

	SpriteObject("TablesMadameButterfly", sprites.TablesMadameButterfly)
	SpriteObject("TablesCricketly", sprites.TablesCricketly)
	SpriteObject("TablesSandy", sprites.TablesSandy)
	SpriteObject("TablesClaire", sprites.TablesClaire)
	SpriteObject("TablesNeil", sprites.TablesNeil)
	SpriteObject("TablesXic", sprites.TablesXic)
	SpriteObject("TablesGein", sprites.TablesGein)
	SpriteObject("TablesYumnum", sprites.TablesYumnum)
	SpriteObject("TablesSkirby", sprites.TablesSkirby)
	SpriteObject("TablesCherryGuy", sprites.TablesCherryGuy)
	SpriteObject("TablesTyna", sprites.TablesTyna)
	SpriteObject("TablesRina", sprites.TablesRina)
	SpriteObject("TablesSnail", sprites.TablesSnail)
	SpriteObject("TablesSnailTired", sprites.TablesSnailTired)

	SpriteObject("TablesAnt", sprites.TablesAnt)
	SpriteObject("TablesCelia", sprites.TablesCelia)
	SpriteObject("TablesMothiva", sprites.TablesMothiva)
	SpriteObject("TablesSlacker", sprites.TablesSlacker)
	SpriteObject("TablesMaki", sprites.TablesMaki)
	SpriteObject("TablesSnakemouth", sprites.TablesSnakemouth)

	for i, s := range sprites.TablesItems {
		SpriteObject("TablesItems["+strconv.Itoa(i)+"]", s)
	}
}
