package tables3d

import (
	"sort"

	"git.lubar.me/ben/spy-cards/gfx"
	"golang.org/x/mobile/gl"
)

var bakedSkybox = BakedModelObject{
	data: []BufferData{
		{
			X:  -1,
			Y:  1,
			Z:  0.999999,
			S:  0,
			T:  0,
			Nx: 0,
			Ny: 0,
			Nz: -1,
		},
		{
			X:  -1,
			Y:  -1,
			Z:  0.999999,
			S:  0,
			T:  1<<16 - 1,
			Nx: 0,
			Ny: 0,
			Nz: -1,
		},
		{
			X:  1,
			Y:  -1,
			Z:  0.999999,
			S:  1<<16 - 1,
			T:  1<<16 - 1,
			Nx: 0,
			Ny: 0,
			Nz: -1,
		},
		{
			X:  -1,
			Y:  1,
			Z:  0.999999,
			S:  0,
			T:  0,
			Nx: 0,
			Ny: 0,
			Nz: -1,
		},
		{
			X:  1,
			Y:  -1,
			Z:  0.999999,
			S:  1<<16 - 1,
			T:  1<<16 - 1,
			Nx: 0,
			Ny: 0,
			Nz: -1,
		},
		{
			X:  1,
			Y:  1,
			Z:  0.999999,
			S:  1<<16 - 1,
			T:  0,
			Nx: 0,
			Ny: 0,
			Nz: -1,
		},
	},
	mode: 6,
}

type BakedModelObject struct {
	tex   *gfx.AssetTexture
	data  []BufferData
	mode  int
	vao   gl.VertexArray
	buf   gl.Buffer
	trans bool
}

func (o *ModelObject) MarkDirty() {
	for _, b := range o.baked {
		if b.vao != (gl.VertexArray{}) {
			gfx.GL.DeleteVertexArray(b.vao)
		}

		if b.buf != (gl.Buffer{}) {
			gfx.GL.DeleteBuffer(b.buf)
		}
	}

	o.baked = nil
}

func (o *ModelObject) Bake() []*BakedModelObject {
	if o.baked == nil {
		var identity gfx.Matrix

		identity.Identity()

		o.baked = o.bake(nil, &identity)
	}

	return o.baked
}

func (o *ModelObject) bake(objs []*BakedModelObject, transform *gfx.Matrix) []*BakedModelObject {
	if len(o.Buffer) != 0 {
		var baked *BakedModelObject

		for i := range objs {
			if objs[i].tex == o.Tex && objs[i].mode == o.ShaderMode && objs[i].trans == o.Transparent {
				baked = objs[i]

				break
			}
		}

		if baked == nil {
			baked = &BakedModelObject{
				tex:   o.Tex,
				mode:  o.ShaderMode,
				trans: o.Transparent,
			}

			objs = append(objs, baked)
			sort.SliceStable(objs, func(i, j int) bool {
				return objs[i].mode < objs[j].mode
			})
		} else if baked.buf != (gl.Buffer{}) {
			gfx.GL.DeleteBuffer(baked.buf)
			baked.buf = gl.Buffer{}
		}

		start := len(baked.data)
		baked.data = append(baked.data, o.Buffer...)

		if transform.Determinant() < 0 {
			// if we're inside-out, flip the polygons to make the front faces render
			for i := start; i < len(baked.data); i += 3 {
				baked.data[i], baked.data[i+1] = baked.data[i+1], baked.data[i]
			}
		}

		var pos, norm gfx.Vector

		for i := start; i < len(baked.data); i++ {
			pos.Multiply(transform, &gfx.Vector{baked.data[i].X, baked.data[i].Y, baked.data[i].Z, 1})
			norm.Multiply(transform, &gfx.Vector{baked.data[i].Nx, baked.data[i].Ny, baked.data[i].Nz, 0})

			baked.data[i].X, baked.data[i].Y, baked.data[i].Z = pos[0], pos[1], pos[2]
			baked.data[i].Nx, baked.data[i].Ny, baked.data[i].Nz = norm[0], norm[1], norm[2]
		}
	}

	for _, inst := range o.Instances {
		if inst.ensureInit() {
			var m gfx.Matrix

			m.Multiply(transform, &inst.mat)

			objs = inst.obj.bake(objs, &m)
		}
	}

	return objs
}
