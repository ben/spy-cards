//go:build js
// +build js

package tables3d

import (
	"runtime"
	"syscall/js"
	"unsafe"

	"git.lubar.me/ben/spy-cards/gfx"
	"git.lubar.me/ben/spy-cards/gfx/sprites"
	"git.lubar.me/ben/spy-cards/internal"
	"golang.org/x/mobile/gl"
)

var (
	fastRenderMatrix      = internal.Float32Array.New(16)
	fastRenderMatrixBytes = internal.Uint8Array.New(fastRenderMatrix.Get("buffer"), 0)
	fastRenderModel       js.Value

	_ = gfx.Custom("tables-fast-render-model", fastRenderModelInit, fastRenderModelRelease)
)

func (baked *BakedModelObject) fastRender(transform *gfx.Matrix) bool {
	if !fastRenderModel.Truthy() {
		return false
	}

	js.CopyBytesToJS(fastRenderMatrixBytes, (*[unsafe.Sizeof(*transform)]byte)(unsafe.Pointer(transform))[:])
	runtime.KeepAlive(transform)

	gfx.StatRecordTextureUse(baked.tex)
	gfx.StatRecordTriangleBatch(len(baked.data))

	var vao js.Value

	if baked.vao.Value != nil {
		vao = *baked.vao.Value
	}

	fastRenderModel.Invoke(
		*baked.tex.LazyTexture(sprites.Blank.Texture()).Value,
		baked.mode,
		*baked.buf.Value,
		vao,
		len(baked.data),
	)

	return true
}

func fastRenderModelInit() {
	fastRenderModel = internal.Function.New("gl,transform,umode,umodelview,avertex,atex,anormal", `function determinant(m) {
	const a = m[0]*m[5] - m[1]*m[4];
	const b = m[0]*m[6] - m[2]*m[4];
	const c = m[0]*m[7] - m[3]*m[4];
	const d = m[1]*m[6] - m[2]*m[5];
	const e = m[1]*m[7] - m[3]*m[5];
	const f = m[2]*m[7] - m[3]*m[6];
	const g = m[8]*m[13] - m[9]*m[12];
	const h = m[8]*m[14] - m[10]*m[12];
	const i = m[8]*m[15] - m[11]*m[12];
	const j = m[9]*m[14] - m[10]*m[13];
	const k = m[9]*m[15] - m[11]*m[13];
	const l = m[10]*m[15] - m[11]*m[14];

	return a*l - b*k + c*j + d*i - e*h + f*g;
}

return function fastRenderModel(tex, mode, buf, vao, count) {
	gl.frontFace(determinant(transform) < 0 ? gl.CW : gl.CCW);

	if (vao) {
		gl.bindVertexArray(vao);
	} else {
		gl.bindBuffer(gl.ARRAY_BUFFER, buf);
		gl.enableVertexAttribArray(avertex);
		gl.enableVertexAttribArray(atex);
		gl.enableVertexAttribArray(anormal);
		gl.vertexAttribPointer(avertex, 3, gl.FLOAT, false, 28, 0);
		gl.vertexAttribPointer(atex, 2, gl.UNSIGNED_SHORT, true, 28, 12);
		gl.vertexAttribPointer(anormal, 3, gl.FLOAT, false, 28, 16);
	}

	gl.bindTexture(gl.TEXTURE_2D, tex);
	gl.uniform1i(umode, mode);
	gl.uniformMatrix4fv(umodelview, false, transform);

	if (mode > (1<<30)) {
		gl.blendFunc(gl.ONE, gl.ONE);
		gl.depthMask(false);
	}

	gl.drawArrays(gl.TRIANGLES, 0, count);

	if (mode > (1<<30)) {
		gl.blendFunc(gl.ONE, gl.ONE_MINUS_SRC_ALPHA);
		gl.depthMask(true);
	}
}`).Invoke(
		gfx.GL.(gl.JSWrapper).JSValue(),
		fastRenderMatrix,
		*uniformMode.Uniform.Value,
		*uniformModelView.Uniform.Value,
		*attribVertex.Attrib.Value,
		*attribTexCoord.Attrib.Value,
		*attribNormal.Attrib.Value,
	)
}

func fastRenderModelRelease() {
	fastRenderModel = js.Undefined()
}
