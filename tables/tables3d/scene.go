package tables3d

import (
	"math"
	"time"

	"git.lubar.me/ben/spy-cards/gfx"
	"golang.org/x/mobile/gl"
)

const MaxLights = 3

type Scene struct {
	CamPos         [3]float32
	CamDist        float32
	CamAngle       [2]float32
	LightAngles    [MaxLights][2]float32
	LightAmbient   [3]float32
	LightIntensity [MaxLights][3]float32

	Objects []*SceneObject
	Skybox  *gfx.AssetTexture

	lightProj    [MaxLights]gfx.Matrix
	invLightProj [MaxLights]gfx.Matrix
	lightNorm    [3 * MaxLights]float32
	lightUpdate  uint8

	FOV           float32
	HorizontalFOV bool
}

type SceneObject struct {
	Model     *ModelObject
	Transform gfx.Matrix
}

var Identity = func() gfx.Matrix {
	var identity gfx.Matrix

	identity.Identity()

	return identity
}()

func (s *Scene) Render() {
	postprocCheckResize()

	var cam gfx.Camera

	var m0, m1, proj, invProj gfx.Matrix

	frameTime := time.Now()

	w, h := gfx.PixelSize()

	fov := s.FOV
	if s.HorizontalFOV {
		fov = -gfx.HorizontalFOV(float64(fov))
	} else {
		fov *= math.Pi / 180
	}

	cam.SetDefaults()
	cam.Invert = false
	cam.Perspective.Perspective(fov, float32(w)/float32(h), nearZ, farZ)
	invProj.Inverse(&cam.Perspective)

	cam.Offset.Translation(0, 0, s.CamDist)
	m0.RotationX(s.CamAngle[0])
	m1.RotationY(s.CamAngle[1])
	cam.Rotation.Multiply(&m0, &m1)
	cam.Position.Translation(s.CamPos[0], s.CamPos[1], s.CamPos[2])

	cam.Combined(&m1)
	proj.Multiply(&cam.Perspective, &m1)
	invProj.Inverse(&proj)

	gfx.GL.Enable(gl.DEPTH_TEST)
	gfx.GL.UseProgram(shader.Program)
	gfx.GL.UniformMatrix4fv(uniformPerspective.Uniform, cam.Perspective[:])
	gfx.GL.UniformMatrix4fv(uniformCamera.Uniform, m1[:])
	gfx.GL.Uniform1f(uniformTime.Uniform, float32((frameTime.Sub(arbitrary) % time.Hour).Seconds()))
	gfx.GL.Uniform1i(uniformClampZ.Uniform, 0)

	passes := 3
	if !noDrawBuffers {
		passes = 1
	}

	bakedSkybox.tex = s.Skybox

	for i := 0; i < passes; i++ {
		gfx.GL.Uniform1i(uniformPass.Uniform, i)
		gfx.GL.BindFramebuffer(gl.FRAMEBUFFER, frameBuffer[i])
		gfx.GL.Viewport(0, 0, w, h)
		gfx.GL.Clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT)

		if s.Skybox != nil {
			bakedSkybox.Render(&Identity, false)
		}

		s.doRender(false)
	}

	gfx.GL.ColorMask(false, false, false, false)

	if skipTransparencyForAO {
		gfx.GL.BindFramebuffer(gl.FRAMEBUFFER, depth2FrameBuffer)

		gfx.GL.Clear(gl.DEPTH_BUFFER_BIT)

		s.doRender(skipTransparencyForAO)
	}

	cam.Perspective.Perspective(2.5*math.Pi/180, 1, 1175, 1250)
	cam.Offset.Translation(0, 0, -1200)

	gfx.GL.Uniform1i(uniformClampZ.Uniform, 1)

	for update := 0; update <= int(gfx.GPULevel); update++ {
		m0.RotationX(s.LightAngles[s.lightUpdate][0])
		m1.RotationY(s.LightAngles[s.lightUpdate][1])
		cam.Rotation.Multiply(&m0, &m1)

		cam.MarkDirty()

		cam.Combined(&m1)
		s.lightProj[s.lightUpdate].Multiply(&cam.Perspective, &m1)
		s.invLightProj[s.lightUpdate].Inverse(&s.lightProj[s.lightUpdate])

		var norm gfx.Vector

		norm.Multiply(&m1, &gfx.Vector{0, 0, 1, 0})

		norm[0] = -norm[0]
		norm[1] = -norm[1]

		copy(s.lightNorm[int(s.lightUpdate)*3:], norm[:3])

		gfx.GL.UniformMatrix4fv(uniformPerspective.Uniform, cam.Perspective[:])
		gfx.GL.UniformMatrix4fv(uniformCamera.Uniform, m1[:])

		gfx.GL.BindFramebuffer(gl.FRAMEBUFFER, lightFrameBuffer[s.lightUpdate])
		gfx.GL.Viewport(0, 0, 512<<gfx.GPULevel, 512<<gfx.GPULevel)

		gfx.GL.Clear(gl.DEPTH_BUFFER_BIT)

		s.doRender(false)

		s.lightUpdate++
		if s.lightUpdate >= uint8(len(s.lightProj)) {
			s.lightUpdate = 0
		}
	}

	gfx.GL.FrontFace(gl.CCW)
	gfx.GL.ColorMask(true, true, true, true)

	gfx.GL.BindFramebuffer(gl.FRAMEBUFFER, gl.Framebuffer{})
	gfx.GL.Viewport(0, 0, w, h)

	var lightIntensity [3 * MaxLights]float32

	for i := range s.LightIntensity {
		copy(lightIntensity[3*i:], s.LightIntensity[i][:])
	}

	postprocess(&proj, &invProj, &s.lightProj, &s.invLightProj, &s.LightAmbient, &s.lightNorm, &lightIntensity)
}

func (s *Scene) doRender(skipTransparent bool) {
	for _, o := range s.Objects {
		for _, baked := range o.Model.Bake() {
			baked.Render(&o.Transform, skipTransparent)
		}
	}
}

func (s *Scene) Clone() *Scene {
	objects := make([]*SceneObject, len(s.Objects))

	for i, o := range s.Objects {
		objects[i] = &SceneObject{
			Model:     o.Model,
			Transform: o.Transform,
		}
	}

	return &Scene{
		CamPos:         s.CamPos,
		CamDist:        s.CamDist,
		CamAngle:       s.CamAngle,
		LightAngles:    s.LightAngles,
		LightAmbient:   s.LightAmbient,
		LightIntensity: s.LightIntensity,

		Objects: objects,
		Skybox:  s.Skybox,

		FOV:           s.FOV,
		HorizontalFOV: s.HorizontalFOV,
	}
}
