//go:build js
// +build js

package tables3d

import (
	"syscall/js"
	"time"
	"unsafe"

	"git.lubar.me/ben/spy-cards/gfx"
	"git.lubar.me/ben/spy-cards/internal"
	"golang.org/x/mobile/gl"
)

var noDrawBuffers bool

func setupDrawBuffers() {
	if _, is3 := gfx.GL.(gl.Context3); is3 {
		gfx.GL.(gl.JSWrapper).JSValue().Call("drawBuffers", []interface{}{
			gl.COLOR_ATTACHMENT0,
			gl.COLOR_ATTACHMENT1,
			gl.COLOR_ATTACHMENT2,
		})
	} else {
		ext := gfx.GL.(gl.JSWrapper).JSValue().Call("getExtension", "WEBGL_draw_buffers")
		if !ext.Truthy() {
			noDrawBuffers = true
		}

		ext.Call("drawBuffersWEBGL", []interface{}{
			gl.COLOR_ATTACHMENT0,
			gl.COLOR_ATTACHMENT1,
			gl.COLOR_ATTACHMENT2,
		})
	}
}

func srgbSupported() bool {
	if _, is3 := gfx.GL.(gl.Context3); is3 {
		return true
	}

	return gfx.GL.(gl.JSWrapper).JSValue().Call("getExtension", "EXT_sRGB").Truthy()
}

var (
	fastPostprocessDataProj           = internal.Uint8Array.New(16 * 4)
	fastPostprocessDataInvProj        = internal.Uint8Array.New(16 * 4)
	fastPostprocessDataLightProj      = internal.Uint8Array.New(MaxLights * 16 * 4)
	fastPostprocessDataInvLightProj   = internal.Uint8Array.New(MaxLights * 16 * 4)
	fastPostprocessDataLightAmbient   = internal.Uint8Array.New(3 * 4)
	fastPostprocessDataLightNorm      = internal.Uint8Array.New(MaxLights * 3 * 4)
	fastPostprocessDataLightIntensity = internal.Uint8Array.New(MaxLights * 3 * 4)
	fastPostprocessImpl               js.Value
)

func fastPostprocess(proj, invProj *gfx.Matrix, lightProj, invLightProj *[MaxLights]gfx.Matrix, lightAmbient *[3]float32, lightNorm, lightIntensity *[3 * MaxLights]float32) bool {
	if !fastPostprocessImpl.Truthy() {
		return false
	}

	js.CopyBytesToJS(fastPostprocessDataProj, (*[unsafe.Sizeof(*proj)]byte)(unsafe.Pointer(proj))[:])
	js.CopyBytesToJS(fastPostprocessDataInvProj, (*[unsafe.Sizeof(*invProj)]byte)(unsafe.Pointer(invProj))[:])
	js.CopyBytesToJS(fastPostprocessDataLightProj, (*[unsafe.Sizeof(*lightProj)]byte)(unsafe.Pointer(lightProj))[:])
	js.CopyBytesToJS(fastPostprocessDataInvLightProj, (*[unsafe.Sizeof(*invLightProj)]byte)(unsafe.Pointer(invLightProj))[:])
	js.CopyBytesToJS(fastPostprocessDataLightAmbient, (*[unsafe.Sizeof(*lightAmbient)]byte)(unsafe.Pointer(lightAmbient))[:])
	js.CopyBytesToJS(fastPostprocessDataLightNorm, (*[unsafe.Sizeof(*lightNorm)]byte)(unsafe.Pointer(lightNorm))[:])
	js.CopyBytesToJS(fastPostprocessDataLightIntensity, (*[unsafe.Sizeof(*lightIntensity)]byte)(unsafe.Pointer(lightIntensity))[:])

	fastPostprocessImpl.Invoke(
		skipTransparencyForAO,
		float32((time.Since(postprocReferenceTime) % time.Hour).Seconds()),
	)

	return true
}

func fastPostprocInit() {
	var timeUniform, vao js.Value

	if postprocTimeUniform.Uniform.Value != nil {
		timeUniform = *postprocTimeUniform.Uniform.Value
	}

	if postprocSquare.VAO.Value != nil {
		vao = *postprocSquare.VAO.Value
	}

	fastPostprocessImpl = internal.Function.New("gl, shader, udepth, utime, ukernelsize, kernelSize, uaosettings, aoRadius, aoMinDist, aoMaxDist, ulightsettings, lightMinDist, lightMaxDist, nearZ, farZ, ulightambient, lightAmbient, ulightintensity, lightIntensity, tframe, tnormal, tdepth, tdepth2, tlight0, tlight1, tlight2, tdata, ulightview, lightProj, uinvlightview, invLightProj, ulightnorm, lightNorm, uperspective, proj, uinvperspective, invProj, buf, ebuf, vao, count, apos, atex", `const fLightAmbient = new Float32Array(lightAmbient.buffer, 0);
const fLightIntensity = new Float32Array(lightIntensity.buffer, 0);
const fLightProj = new Float32Array(lightProj.buffer, 0);
const fInvLightProj = new Float32Array(invLightProj.buffer, 0);
const fLightNorm = new Float32Array(lightNorm.buffer, 0);
const fProj = new Float32Array(proj.buffer, 0);
const fInvProj = new Float32Array(invProj.buffer, 0);

return function fastPostprocess(skipTransparencyForAO, time) {
	gl.clear(gl.COLOR_BUFFER_BIT, gl.DEPTH_BUFFER_BIT);
	gl.useProgram(shader);

	gl.uniform1iv(udepth, [2, skipTransparencyForAO ? 3 : 2]);
	gl.uniform1f(utime, time);
	gl.uniform1i(ukernelsize, kernelSize);
	gl.uniform3f(uaosettings, aoRadius, aoMinDist, aoMaxDist);
	gl.uniform4f(ulightsettings, lightMinDist, lightMaxDist, nearZ, farZ);

	gl.uniform3fv(ulightambient, fLightAmbient);
	gl.uniform3fv(ulightintensity, fLightIntensity);

	gl.activeTexture(gl.TEXTURE7);
	gl.bindTexture(gl.TEXTURE_2D, tdata);
	gl.activeTexture(gl.TEXTURE6);
	gl.bindTexture(gl.TEXTURE_2D, tlight2);
	gl.activeTexture(gl.TEXTURE5);
	gl.bindTexture(gl.TEXTURE_2D, tlight1);
	gl.activeTexture(gl.TEXTURE4);
	gl.bindTexture(gl.TEXTURE_2D, tlight0);
	gl.activeTexture(gl.TEXTURE3);
	gl.bindTexture(gl.TEXTURE_2D, tdepth2);
	gl.activeTexture(gl.TEXTURE2);
	gl.bindTexture(gl.TEXTURE_2D, tdepth);
	gl.activeTexture(gl.TEXTURE1);
	gl.bindTexture(gl.TEXTURE_2D, tnormal);
	gl.activeTexture(gl.TEXTURE0);
	gl.bindTexture(gl.TEXTURE_2D, tframe);

	gl.uniformMatrix4fv(ulightview, false, fLightProj);
	gl.uniformMatrix4fv(uinvlightview, false, fInvLightProj);
	gl.uniform3fv(ulightnorm, fLightNorm);
	gl.uniformMatrix4fv(uperspective, false, fProj);
	gl.uniformMatrix4fv(uinvperspective, false, fInvProj);

	if (vao) {
		gl.bindVertexArray(vao);
	} else {
		gl.bindBuffer(gl.ARRAY_BUFFER, buf);
		gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, ebuf);

		gl.enableVertexAttribArray(apos);
		gl.enableVertexAttribArray(atex);

		gl.vertexAttribPointer(apos, 4, gl.BYTE, false, 6, 0);
		gl.vertexAttribPointer(atex, 2, gl.BYTE, false, 6, 4);
	}

	gl.drawElements(gl.TRIANGLES, count, gl.UNSIGNED_SHORT, 0);

	if (!vao) {
		gl.disableVertexAttribArray(apos);
		gl.disableVertexAttribArray(atex);
	}
}`).Invoke(
		gfx.GL.(gl.JSWrapper).JSValue(),
		*postprocShader.Program.Value,
		*postprocDepthUniform.Uniform.Value,
		timeUniform,
		*postprocKernelSizeUniform.Uniform.Value,
		kernelSize,
		*postprocAOSettingsUniform.Uniform.Value,
		aoRadius,
		aoMinDist,
		aoMaxDist,
		*postprocLightSettingsUniform.Uniform.Value,
		lightMinDist,
		lightMaxDist,
		nearZ,
		farZ,
		*postprocLightAmbientUniform.Uniform.Value,
		fastPostprocessDataLightAmbient,
		*postprocLightIntensityUniform.Uniform.Value,
		fastPostprocessDataLightIntensity,
		*frameTexture.Value,
		*normalTexture.Value,
		*depthTexture.Value,
		*depth2Texture.Value,
		*lightTexture[0].Value,
		*lightTexture[1].Value,
		*lightTexture[2].Value,
		*dataTexture.Value,
		*postprocLightViewUniform.Uniform.Value,
		fastPostprocessDataLightProj,
		*postprocInvLightViewUniform.Uniform.Value,
		fastPostprocessDataInvLightProj,
		*postprocLightNormUniform.Uniform.Value,
		fastPostprocessDataLightNorm,
		*postprocPerspectiveUniform.Uniform.Value,
		fastPostprocessDataProj,
		*postprocInvPerspectiveUniform.Uniform.Value,
		fastPostprocessDataInvProj,
		*postprocSquare.Data.Value,
		*postprocSquare.Element.Value,
		vao,
		postprocSquare.Count,
		*postprocPosAttrib.Attrib.Value,
		*postprocTexAttrib.Attrib.Value,
	)
}

func fastPostprocRelease() {
	fastPostprocessImpl = js.Undefined()
}
