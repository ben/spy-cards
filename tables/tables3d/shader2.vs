#ifdef GL_FRAGMENT_PRECISION_HIGH
precision highp float;
precision highp int;
precision highp sampler2D;
#else
precision mediump float;
precision mediump int;
precision mediump sampler2D;
#endif

uniform mat4 perspective;
uniform mat4 camera;
uniform mat4 modelView;
uniform int mode;
uniform bool clampZ;

attribute vec4 vertex;
attribute vec2 texCoord;
attribute vec3 normal;

varying vec2 tc;
varying vec3 norm;

void main() {
	tc = texCoord;
	norm = normalize((camera * modelView * vec4(normal, 0.0)).xyz);

	gl_Position = perspective * camera * modelView * vertex;

	if (clampZ) {
		// clamp nearZ and farZ instead of clipping
		gl_Position.z = clamp(gl_Position.z, -gl_Position.w, gl_Position.w);
	}

	if (mode == 6) {
		gl_Position = vertex;
		norm = normal;
	}
}
