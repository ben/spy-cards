//go:build !js
// +build !js

package tables3d

import "git.lubar.me/ben/spy-cards/gfx"

const noDrawBuffers = true

func setupDrawBuffers() {
	// TODO: setupDrawBuffers on non-WebGL
}

func srgbSupported() bool {
	return false
}

func fastPostprocess(proj, invProj *gfx.Matrix, lightProj, invLightProj *[MaxLights]gfx.Matrix, lightAmbient *[3]float32, lightNorm, lightIntensity *[3 * MaxLights]float32) bool {
	return false
}

func fastPostprocInit()    {}
func fastPostprocRelease() {}
