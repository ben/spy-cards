//go:generate go run .

package main

import (
	"encoding/binary"
	"os"
	"path/filepath"

	"git.lubar.me/ben/spy-cards/tables/tables3d"
)

func main() {
	previousModels, err := filepath.Glob("*.mdl")
	if err != nil {
		panic(err)
	}

	for _, name := range previousModels {
		err = os.Remove(name)
		if err != nil {
			panic(err)
		}
	}

	files, err := filepath.Glob("*.obj")
	if err != nil {
		panic(err)
	}

	written := make(map[string]string)

	for _, name := range files {
		mdl := readModel(name)

		for _, o := range mdl.Objects {
			if prev, ok := written[o.Name]; ok {
				panic("duplicate model: " + o.Name + " (in both " + name + " and " + prev + ")")
			}

			written[o.Name] = name

			writeModel(o.ModelObject)
		}
	}
}

func readModel(name string) *tables3d.Model {
	f, err := os.Open(name)
	if err != nil {
		panic(err)
	}
	defer f.Close()

	mdl, err := tables3d.ParseObj(f)
	if err != nil {
		panic(err)
	}

	return mdl
}

func writeModel(o *tables3d.ModelObject) {
	f, err := os.Create(o.Name + ".mdl")
	if err != nil {
		panic(err)
	}
	defer f.Close()

	var varint [binary.MaxVarintLen64]byte

	n := binary.PutUvarint(varint[:], uint64(len(o.Name)))

	_, err = f.Write(varint[:n])
	if err != nil {
		panic(err)
	}

	_, err = f.Write([]byte(o.Name))
	if err != nil {
		panic(err)
	}

	n = binary.PutUvarint(varint[:], uint64(len(o.Material)))

	_, err = f.Write(varint[:n])
	if err != nil {
		panic(err)
	}

	_, err = f.Write([]byte(o.Material))
	if err != nil {
		panic(err)
	}

	n = binary.PutUvarint(varint[:], uint64(len(o.Instances)))

	_, err = f.Write(varint[:n])
	if err != nil {
		panic(err)
	}

	for _, i := range o.Instances {
		n = binary.PutUvarint(varint[:], uint64(len(i.Name)))

		_, err = f.Write(varint[:n])
		if err != nil {
			panic(err)
		}

		_, err = f.Write([]byte(i.Name))
		if err != nil {
			panic(err)
		}

		err = binary.Write(f, binary.LittleEndian, &[9]float32{
			i.X, i.Y, i.Z,
			i.Rx, i.Ry, i.Rz,
			i.Sx, i.Sy, i.Sz,
		})
		if err != nil {
			panic(err)
		}
	}

	err = binary.Write(f, binary.LittleEndian, &o.Buffer)
	if err != nil {
		panic(err)
	}
}
