#ifdef GL_FRAGMENT_PRECISION_HIGH
precision highp float;
#else
precision mediump float;
#endif

attribute vec4 pos;
attribute vec2 tex;

varying vec2 texCoord;

void main() {
	texCoord = tex;
	gl_Position = pos;
}
