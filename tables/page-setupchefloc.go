package tables

import (
	"context"
	"math"
	"strconv"
	"strings"

	"git.lubar.me/ben/spy-cards/audio"
	"git.lubar.me/ben/spy-cards/gfx"
	"git.lubar.me/ben/spy-cards/gfx/sprites"
	"git.lubar.me/ben/spy-cards/input"
	"git.lubar.me/ben/spy-cards/internal/router"
	"git.lubar.me/ben/spy-cards/tables/tables3d"
)

func (s *State) updateSetupRank() {
	w, h := gfx.Size()
	mx, my, _ := s.ictx.Mouse()
	rank := int(s.Persistent.Rank())

	s.rankHover = sprites.TextHitTest(&s.cam, sprites.FontD3Streetism, "Rank: "+strconv.Itoa(rank), -float32(w)/64+0.5, float32(h)/64-1.5, 0, 2, 2, mx, my, false) || sprites.SpriteHitTest(&s.cam, sprites.Blank, -float32(w)/64+3, float32(h)/64-2, 0, 0.075*(PointsPerRank+1)+PointsPerRank, 0.4, mx, my)
}

func (s *State) renderSetupRank() {
	w, h := gfx.Size()

	rank := int(s.Persistent.Rank())

	rankTint := sprites.White

	if s.rankHover {
		rankTint = sprites.Yellow
	}

	sprites.DrawTextBorder(s.batch, sprites.FontD3Streetism, "Rank: "+strconv.Itoa(rank), -float32(w)/64+0.5, float32(h)/64-1.5, 0, 2, 2, rankTint, sprites.Black, false)

	s.batch.Append(sprites.Blank, -float32(w)/64+3, float32(h)/64-2, 0, 0.075*(PointsPerRank+1)+PointsPerRank, 0.4, sprites.Black)

	for i := uint64(0); i < PointsPerRank; i++ {
		tint := sprites.Yellow
		if s.Persistent.RankPoints%PointsPerRank < i+1 {
			tint = sprites.Gray
		}

		s.batch.Append(sprites.Blank, -float32(w)/64+3+float32(i)*1.075-((PointsPerRank-1)*1.075)/2, float32(h)/64-2, 0, 1, 0.25, tint)
	}

	if s.rankHover {
		s.batch.Append(sprites.Blank, -float32(w)/64+4.65, float32(h)/64-3.4, 0, 8.5, 1.8, sprites.DGray)

		for i, line := range []string{
			"Earn points to increase your rank by",
			"completing scenarios. Earn more points",
			"by successfully completing objectives.",
		} {
			sprites.DrawTextBorder(s.batch, sprites.FontBubblegumSans, line, -float32(w)/64+0.5, float32(h)/64-3-float32(i)*0.75*0.7, 0, 0.75, 0.75, sprites.White, sprites.Black, false)
		}
	}
}

func (s *State) updateSetupChefLoc(ctx context.Context) (bool, *router.PageInfo, error) {
	w, h := gfx.Size()

	mx, my, click := s.ictx.Mouse()
	lastX, lastY, lastClick := s.ictx.LastMouse()

	rank := int(s.Persistent.Rank())

	for i := range s.offset {
		if s.inputIndex == uint8(i+1) && click && lastClick {
			s.offset[i] += (lastX - mx) * float32(w) / 32 / 8

			continue
		}

		target := s.offset[i]
		if s.useTargetOffset[i] {
			if math.Round(float64(target)) == math.Round(float64(s.targetOffset[i])) {
				s.useTargetOffset[i] = false
			}

			target = s.targetOffset[i]
		}

		target = float32(math.Round(float64(target)))
		if target < 0 {
			target = 0
			s.targetOffset[i] = target
		}

		switch i {
		case 0:
			if int(target) > len(Chefs)-2 {
				target = float32(len(Chefs)) - 2
				s.targetOffset[i] = target
			}
		case 1:
			if int(target) > len(Locations)-2 {
				target = float32(len(Locations)) - 2
				s.targetOffset[i] = target
			}
		}

		s.offset[i] = s.offset[i]*0.925 + target*0.075
	}

	if wheel := s.ictx.Wheel(); wheel != 0 && s.inputIndex != 0 && s.inputIndex <= uint8(len(s.offset)) {
		if !s.useTargetOffset[s.inputIndex-1] {
			s.useTargetOffset[s.inputIndex-1] = true
			s.targetOffset[s.inputIndex-1] = s.offset[s.inputIndex-1]
		}

		if wheel > 0 {
			s.targetOffset[s.inputIndex-1]++
		} else {
			s.targetOffset[s.inputIndex-1]--
		}
	}

	chefOK, locOK := false, false

	chefIndex := int(math.Round(float64(s.offset[0])))
	if off := s.offset[0] - float32(chefIndex); chefIndex >= 0 && off > -0.25 && off < 0.25 {
		for i := range Unlocks {
			if i > rank {
				break
			}

			for _, c := range Unlocks[i].Chefs {
				chefIndex--

				if chefIndex < 0 {
					chefOK = true

					if m := c.Model(); m != ChefModel.Instances[0].Name {
						ChefModel.Instances[0] = &tables3d.ModelInstance{
							Name: m,
							Sx:   1,
							Sy:   1,
							Sz:   1,
						}
						ChefModel.MarkDirty()
					}

					break
				}
			}

			if chefIndex < 0 {
				break
			}
		}
	}

	locIndex := int(math.Round(float64(s.offset[1])))
	if off := s.offset[1] - float32(locIndex); locIndex >= 0 && off > -0.25 && off < 0.25 {
		for i := range Unlocks {
			if i > rank {
				break
			}

			for _, l := range Unlocks[i].Locations {
				locIndex--

				if locIndex < 0 {
					locOK = true

					if s.lastLocation != l {
						if s.lastLocation == LocationNone {
							if Locations[l].Scene == nil {
								s.scene = nil
							} else {
								s.scene = Locations[l].Scene.Clone()
							}

							s.locationLerp = 0
						} else {
							s.locationLerp = 60
						}

						s.lastLocation = l
					}

					break
				}
			}

			if locIndex < 0 {
				break
			}
		}
	}

	if s.inputIndex == 3 && (!chefOK || !locOK) {
		s.inputIndex = 0
	}

	if s.inputIndex <= uint8(len(s.offset)) && s.ictx.ConsumeAllowRepeat(input.BtnRight, 60, 5) {
		if s.inputIndex == 0 {
			s.inputIndex = 1
		}

		if !s.useTargetOffset[s.inputIndex-1] {
			s.useTargetOffset[s.inputIndex-1] = true
			s.targetOffset[s.inputIndex-1] = s.offset[s.inputIndex-1]
		}

		s.targetOffset[s.inputIndex-1]++
	}

	if s.inputIndex <= uint8(len(s.offset)) && s.ictx.ConsumeAllowRepeat(input.BtnLeft, 60, 5) {
		if s.inputIndex == 0 {
			s.inputIndex = 1
		}

		if !s.useTargetOffset[s.inputIndex-1] {
			s.useTargetOffset[s.inputIndex-1] = true
			s.targetOffset[s.inputIndex-1] = s.offset[s.inputIndex-1]
		}

		s.targetOffset[s.inputIndex-1]--
	}

	if s.inputIndex <= uint8(len(s.offset)) && s.ictx.ConsumeAllowRepeat(input.BtnUp, 60, 5) {
		if s.inputIndex == 0 {
			s.inputIndex = 1
		}

		if s.inputIndex == 1 && chefOK && locOK {
			s.inputIndex = 3

			audio.Confirm1.PlaySoundGlobal(0, 0, 0)
		} else if s.inputIndex > 1 {
			s.inputIndex--

			audio.Confirm1.PlaySoundGlobal(0, 0, 0)
		}
	}

	if s.inputIndex <= uint8(len(s.offset)) && s.ictx.ConsumeAllowRepeat(input.BtnDown, 60, 5) {
		if s.inputIndex < uint8(len(s.offset)) {
			s.inputIndex++

			audio.Confirm1.PlaySoundGlobal(0, 0, 0)
		}
	}

	if s.inputIndex == 3 && s.ictx.ConsumeAllowRepeat(input.BtnDown, 60, 5) {
		s.inputIndex = 1

		audio.Confirm1.PlaySoundGlobal(0, 0, 0)
	}

	if ((mx != lastX || my != lastY) && !click) || (!lastClick && click) {
		switch y := (0.5 - my) * float32(h) / 32; {
		case y > 0.5 && y < 8.25:
			if s.inputIndex != 1 {
				audio.Confirm1.PlaySoundGlobal(0, 0, 0)
			}

			s.inputIndex = 1
		case y > -8.5 && y < -1:
			if s.inputIndex != 2 {
				audio.Confirm1.PlaySoundGlobal(0, 0, 0)
			}

			s.inputIndex = 2
		case chefOK && locOK && sprites.TextHitTest(&s.cam, sprites.FontD3Streetism, "Next", float32(w)/64-3, float32(h)/64-1.5, 0, 2, 2, mx, my, true):
			if s.inputIndex != 3 {
				audio.Confirm1.PlaySoundGlobal(0, 0, 0)
			}

			s.inputIndex = 3
		default:
			s.inputIndex = 0
		}

		s.updateSetupRank()
	}

	if sprites.SpriteHitTest(&s.cam, &sprites.Sprite{
		X0: sprites.WideArrow.Y0,
		X1: sprites.WideArrow.Y1,
		Y0: -sprites.WideArrow.X1,
		Y1: -sprites.WideArrow.X0,
	}, -4, 3.5, 0, 1.5, 1, mx, my) && lastClick && !click && !s.ictx.IsMouseDrag() {
		s.ictx.ConsumeClick()

		if !s.useTargetOffset[0] {
			s.useTargetOffset[0] = true
			s.targetOffset[0] = s.offset[0]
		}

		s.targetOffset[0]--
	}

	if sprites.SpriteHitTest(&s.cam, &sprites.Sprite{
		X0: -sprites.WideArrow.Y1,
		X1: -sprites.WideArrow.Y0,
		Y0: sprites.WideArrow.X0,
		Y1: sprites.WideArrow.X1,
	}, 4, 3.5, 0, 1.5, 1, mx, my) && lastClick && !click && !s.ictx.IsMouseDrag() {
		s.ictx.ConsumeClick()

		if !s.useTargetOffset[0] {
			s.useTargetOffset[0] = true
			s.targetOffset[0] = s.offset[0]
		}

		s.targetOffset[0]++
	}

	if sprites.SpriteHitTest(&s.cam, &sprites.Sprite{
		X0: sprites.WideArrow.Y0,
		X1: sprites.WideArrow.Y1,
		Y0: -sprites.WideArrow.X1,
		Y1: -sprites.WideArrow.X0,
	}, -4, -3, 0, 1.5, 1, mx, my) && lastClick && !click && !s.ictx.IsMouseDrag() {
		s.ictx.ConsumeClick()

		if !s.useTargetOffset[1] {
			s.useTargetOffset[1] = true
			s.targetOffset[1] = s.offset[1]
		}

		s.targetOffset[1]--
	}

	if sprites.SpriteHitTest(&s.cam, &sprites.Sprite{
		X0: -sprites.WideArrow.Y1,
		X1: -sprites.WideArrow.Y0,
		Y0: sprites.WideArrow.X0,
		Y1: sprites.WideArrow.X1,
	}, 4, -3, 0, 1.5, 1, mx, my) && lastClick && !click && !s.ictx.IsMouseDrag() {
		s.ictx.ConsumeClick()

		if !s.useTargetOffset[1] {
			s.useTargetOffset[1] = true
			s.targetOffset[1] = s.offset[1]
		}

		s.targetOffset[1]++
	}

	if s.inputIndex == 3 && chefOK && locOK && (s.ictx.Consume(input.BtnConfirm) || (lastClick && !click && !s.ictx.IsMouseDrag())) {
		s.ictx.ConsumeClick()

		var offset [len(s.offset)]int

		for i := range s.offset {
			if router.FlagDisableExploits.IsSet() || !s.useTargetOffset[i] {
				s.targetOffset[i] = s.offset[i]
			}

			offset[i] = int(math.Round(float64(s.targetOffset[i])))
		}

		for i := range Unlocks {
			for _, c := range Unlocks[i].Chefs {
				if offset[0] == 0 {
					s.Data.Chef = c
				}

				offset[0]--
			}

			for _, l := range Unlocks[i].Locations {
				if offset[1] == 0 {
					s.Data.Location = l
				}

				offset[1]--
			}
		}

		s.Page = PageSetupMenu
		s.inputIndex = 0
		s.menuCandidates = nil

		audio.Confirm.PlaySoundGlobal(0, 0, 0)

		s.Save()

		return true, nil, nil
	}

	for i := range s.offset {
		if off := uint8(math.Round(float64(s.offset[i]))); s.offset[i]-float32(off) > -0.25 && s.offset[i]-float32(off) < 0.25 && off != s.lastOffset[i] {
			s.lastOffset[i] = off

			ok := true

			switch i {
			case 0:
				ok = chefOK
			case 1:
				ok = locOK
			}

			if ok {
				audio.Confirm1.PlaySoundGlobal(0, 0, 0)
			} else {
				audio.Buzzer.PlaySoundGlobal(0, 0, 0.5)
			}
		}
	}

	if s.locationLerp != 0 {
		s.locationLerp--

		if s.locationLerp == 15 {
			if Locations[s.lastLocation].Scene == nil {
				s.scene = nil
			} else {
				s.scene = Locations[s.lastLocation].Scene.Clone()
			}
		} else if s.locationLerp > 15 {
			s.scene = &SceneBugariaFull
		}
	}

	distLerp := (float32(s.locationLerp) - 30) / 30
	SceneBugariaFull.CamDist = -10 * (2 - distLerp*distLerp)

	if s.locationLerp <= 15 && s.scene != nil {
		distLerp = float32(s.locationLerp) / 15
		distLerp *= distLerp * distLerp
		s.scene.CamDist = -100*distLerp + Locations[s.lastLocation].Scene.CamDist*(1-distLerp)
	}

	for i := range SceneBugariaFull.CamPos {
		SceneBugariaFull.CamPos[i] = SceneBugariaFull.CamPos[i]*0.9 + Locations[s.lastLocation].FullLoc[i]*0.1
	}

	targetAngle := float32(math.Pi)
	if s.locationLerp > 35 {
		targetAngle = float32(math.Atan2(
			float64(Locations[s.lastLocation].FullLoc[1]-SceneBugariaFull.CamPos[1]),
			float64(Locations[s.lastLocation].FullLoc[0]-SceneBugariaFull.CamPos[0]-1),
		))
	}

	SceneBugariaFull.CamAngle[1] = SceneBugariaFull.CamAngle[1]*0.9 + targetAngle*0.1

	return false, nil, nil
}

func (s *State) renderSetupChefLoc(ctx context.Context) error {
	s.renderBackground()

	w, h := gfx.Size()

	rank := int(s.Persistent.Rank())

	s.renderSetupRank()

	sprites.DrawTextBorder(s.batch, sprites.FontD3Streetism, "Objective: "+s.Data.Objective.String(), 0, float32(h)/64-1.25, 0, 1, 1, sprites.White, sprites.Black, true)

	chefOK, locOK := false, false

	chefOffset := s.offset[0]

	for i := range Unlocks {
		for _, c := range Unlocks[i].Chefs {
			name := "???"
			textTint := sprites.Gray
			tint := sprites.Black

			if chefOffset > -0.0625 && chefOffset < 0.0625 && s.inputIndex == 1 {
				if s.offset[0] > 0.5 {
					s.batch.AppendEx(sprites.WideArrow, -4, 3.5, 0, 1, 1.5, sprites.White, sprites.FlagNoDiscard, 0, 0, math.Pi/2)
				}

				if s.offset[0] < float32(len(Chefs))-2.5 {
					s.batch.AppendEx(sprites.WideArrow, 4, 3.5, 0, 1, 1.5, sprites.White, sprites.FlagNoDiscard, 0, 0, -math.Pi/2)
				}
			}

			if i <= rank {
				name = c.String()
				textTint = sprites.White
				tint = sprites.Gray

				if chefOffset > -0.25 && chefOffset < 0.25 {
					if s.inputIndex == 1 {
						textTint = sprites.Yellow
					}

					tint = sprites.White
					chefOK = true
				}
			}

			s.batch.AppendEx(Chefs[c].Sprite, -8*chefOffset, 1.5, 0, 2.25, 2.25, tint, sprites.FlagNoDiscard, 0, 0, 0)

			sprites.DrawTextBorder(s.batch, sprites.FontBubblegumSans, name, -8*chefOffset, 1, 0, 2, 2, textTint, sprites.Black, true)

			if i > rank {
				sprites.DrawTextBorder(s.batch, sprites.FontBubblegumSans, "Req. Rank "+strconv.Itoa(i), -8*chefOffset, 1-0.7, 0, 1, 1, textTint, sprites.Black, true)
			}

			chefOffset--
		}
	}

	locOffset := s.offset[1]

	for i := range Unlocks {
		for _, l := range Unlocks[i].Locations {
			name := "???"
			textTint := sprites.Gray
			tint := sprites.Black
			textScale := float32(2)

			if locOffset > -0.0625 && locOffset < 0.0625 && s.inputIndex == 2 {
				if s.offset[1] > 0.5 {
					s.batch.AppendEx(sprites.WideArrow, -4, -3, 0, 1, 1.5, sprites.White, sprites.FlagNoDiscard, 0, 0, math.Pi/2)
				}

				if s.offset[1] < float32(len(Locations))-2.5 {
					s.batch.AppendEx(sprites.WideArrow, 4, -3, 0, 1, 1.5, sprites.White, sprites.FlagNoDiscard, 0, 0, -math.Pi/2)
				}
			}

			if i <= rank {
				name = l.String()
				textTint = sprites.White
				tint = sprites.Gray
				textScale = 1.5

				if locOffset > -0.25 && locOffset < 0.25 {
					if s.inputIndex == 2 {
						textTint = sprites.Yellow
					}

					tint = sprites.White
					locOK = true
				}
			}

			s.batch.AppendEx(Locations[l].Sprite, -8*locOffset, -5, 0, 2.5, 2.5, tint, sprites.FlagNoDiscard, 0, 0, 0)

			for j, word := range strings.Split(name, " ") {
				sprites.DrawTextBorder(s.batch, sprites.FontBubblegumSans, word, -8*locOffset, -6.3-float32(j)*textScale*0.7, 0, textScale, textScale, textTint, sprites.Black, true)
			}

			if i > rank {
				sprites.DrawTextBorder(s.batch, sprites.FontBubblegumSans, "Req. Rank "+strconv.Itoa(i), -8*locOffset, -7, 0, 1, 1, textTint, sprites.Black, true)
			}

			locOffset--
		}
	}

	nextColor := sprites.Gray
	if chefOK && locOK {
		nextColor = sprites.White

		if s.inputIndex == 3 {
			nextColor = sprites.Yellow
		}
	}

	sprites.DrawTextBorder(s.batch, sprites.FontD3Streetism, "Next", float32(w)/64-3, float32(h)/64-1.5, 0, 2, 2, nextColor, sprites.Black, true)

	return nil
}
