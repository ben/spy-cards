package tables

import (
	"math"
	"sync"

	"git.lubar.me/ben/spy-cards/gfx"
	"git.lubar.me/ben/spy-cards/gfx/sprites"
	"git.lubar.me/ben/spy-cards/tables/tables3d"
)

var (
	initScenesOnce sync.Once

	ChefModel              *tables3d.ModelObject
	SceneBugariaFull       tables3d.Scene
	SceneBugariaCommercial tables3d.Scene
	SceneUndergroundBar    tables3d.Scene
	SceneDefiantRootDining tables3d.Scene
)

func initScenes() {
	ChefModel = tables3d.NewObject("TablesChef")

	ChefModel.Instances = []*tables3d.ModelInstance{
		{
			Name: "SpriteTablesFry",
			Sx:   1,
			Sy:   1,
			Sz:   1,
		},
	}

	SceneBugariaFull = tables3d.Scene{
		CamPos:   [3]float32{0, 0, 0},
		CamDist:  -50,
		CamAngle: [2]float32{-math.Pi / 6, math.Pi},
		LightAngles: [tables3d.MaxLights][2]float32{
			{-math.Pi / 4, -math.Pi / 8},
			{-math.Pi / 4, -math.Pi / 7},
			{-math.Pi / 4, -math.Pi / 6},
		},
		LightAmbient: [3]float32{0.6, 0.6, 0.6},
		LightIntensity: [tables3d.MaxLights][3]float32{
			{0.4, 0.4, 0.4},
			{0.4, 0.4, 0.4},
			{0.4, 0.4, 0.4},
		},
		Objects: []*tables3d.SceneObject{
			{
				Model:     tables3d.Object("BugariaFull"),
				Transform: tables3d.Identity,
			},
		},
		Skybox:        sprites.Skybox1.AssetTexture,
		FOV:           45,
		HorizontalFOV: true,
	}

	SceneBugariaCommercial = tables3d.Scene{
		CamPos:   [3]float32{5, 2, 0},
		CamDist:  -50,
		CamAngle: [2]float32{-18.8 * math.Pi / 180, -12 * math.Pi / 180},
		LightAngles: [tables3d.MaxLights][2]float32{
			{-math.Pi / 4, 0},
			{-math.Pi / 4, -math.Pi / 2},
			{-math.Pi / 4, math.Pi / 24},
		},
		LightAmbient: [3]float32{0.6, 0.6, 0.6},
		LightIntensity: [tables3d.MaxLights][3]float32{
			{0.4, 0.4, 0.4},
			{0.4, 0.4, 0.4},
			{0.4, 0.4, 0.4},
		},
		Objects: []*tables3d.SceneObject{
			{
				Model:     tables3d.SceneBaseObject("BugariaCommercial"),
				Transform: tables3d.Identity,
			},
			{
				Model:     tables3d.SceneFurnitureObject("BugariaCommercial"),
				Transform: tables3d.Identity,
			},
			{
				Model: ChefModel,
				Transform: func() (m gfx.Matrix) {
					m.Translation(-16, 0, 18)

					return
				}(),
			},
			{
				Model: &tables3d.ModelObject{
					Name: "GeneralStoreSprites",
					Instances: []*tables3d.ModelInstance{
						{
							Name: "SpriteTablesItems[0]",
							X:    5.4,
							Y:    1.07,
							Z:    -2.6,
							Ry:   math.Pi / 2,
							Sx:   1,
							Sy:   1,
							Sz:   1,
						},
						{
							Name: "SpriteTablesItems[1]",
							X:    5.4,
							Y:    1.07,
							Z:    -4.25,
							Ry:   math.Pi / 2,
							Sx:   1,
							Sy:   1,
							Sz:   1,
						},
						{
							Name: "SpriteTablesItems[16]",
							X:    5.4,
							Y:    1.07,
							Z:    -6.07,
							Ry:   math.Pi / 2,
							Sx:   1,
							Sy:   1,
							Sz:   1,
						},
						{
							Name: "SpriteTablesItems[12]",
							X:    5.4,
							Y:    1.07,
							Z:    -7.94,
							Ry:   math.Pi / 2,
							Sx:   1,
							Sy:   1,
							Sz:   1,
						},
						{
							Name: "SpriteTablesItems[25]",
							X:    5.4,
							Y:    1.07,
							Z:    -9.86,
							Ry:   math.Pi / 2,
							Sx:   1,
							Sy:   1,
							Sz:   1,
						},
						{
							Name: "SpriteTablesMadameButterfly",
							X:    4,
							Y:    0,
							Z:    -2.25,
							Sx:   1,
							Sy:   1,
							Sz:   1,
						},
					},
				},
				Transform: tables3d.Identity,
			},
		},
		Skybox:        sprites.Skybox1.AssetTexture,
		FOV:           45,
		HorizontalFOV: true,
	}

	SceneUndergroundBar = tables3d.Scene{
		CamPos:   [3]float32{-2, -2.5, 5},
		CamDist:  -38,
		CamAngle: [2]float32{-math.Pi / 12, -math.Pi / 32},
		LightAngles: [tables3d.MaxLights][2]float32{
			{-math.Pi / 8, 0},
			{-math.Pi * 4 / 24, -math.Pi / 24},
			{-math.Pi * 2 / 24, math.Pi / 24},
		},
		LightAmbient: [3]float32{0.4, 0.2, 0.3},
		LightIntensity: [tables3d.MaxLights][3]float32{
			{0.2, 0.2, 0.2},
			{0.2, 0.2, 0.2},
			{0.2, 0.2, 0.2},
		},
		Objects: []*tables3d.SceneObject{
			{
				Model:     tables3d.SceneBaseObject("UndergroundBar"),
				Transform: tables3d.Identity,
			},
			{
				Model:     tables3d.SceneFurnitureObject("UndergroundBar"),
				Transform: tables3d.Identity,
			},
			{
				Model: ChefModel,
				Transform: func() (m gfx.Matrix) {
					m.Translation(0, 0, -5.62)

					return
				}(),
			},
			{
				Model: &tables3d.ModelObject{
					Name: "UndergroundShopSprites",
					Instances: []*tables3d.ModelInstance{
						{
							Name: "SpriteTablesCherryGuy",
							X:    17.18,
							Y:    0,
							Z:    2.07,
							Sx:   1,
							Sy:   1,
							Sz:   1,
						},
					},
				},
				Transform: tables3d.Identity,
			},
		},
		Skybox:        nil,
		FOV:           30,
		HorizontalFOV: false,
	}

	SceneDefiantRootDining = tables3d.Scene{
		CamPos:   [3]float32{-4, 0, 1.5},
		CamDist:  -30,
		CamAngle: [2]float32{-20 * math.Pi / 180, 12 * math.Pi / 180},
		LightAngles: [tables3d.MaxLights][2]float32{
			{-11 * math.Pi / 24, 0},
			{-8 * math.Pi / 18, math.Pi / 12},
			{-math.Pi / 4, 15 * math.Pi / 180},
		},
		LightAmbient: [3]float32{0.6, 0.6, 0.6},
		LightIntensity: [tables3d.MaxLights][3]float32{
			{0.4, 0.4, 0.4},
			{0.4, 0.4, 0.4},
			{0.4, 0.4, 0.4},
		},
		Objects: []*tables3d.SceneObject{
			{
				Model:     tables3d.SceneBaseObject("DefiantRootDining"),
				Transform: tables3d.Identity,
			},
			{
				Model:     tables3d.SceneFurnitureObject("DefiantRootDining"),
				Transform: tables3d.Identity,
			},
			{
				Model: ChefModel,
				Transform: func() (m gfx.Matrix) {
					m.Translation(3.5, 0, -3.5)

					return
				}(),
			},
		},
		Skybox:        sprites.Skybox4.AssetTexture,
		FOV:           45,
		HorizontalFOV: false,
	}
}
