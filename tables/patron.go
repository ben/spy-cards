//go:generate go run golang.org/x/tools/cmd/stringer -type=PatronID -linecomment

package tables

type PatronID uint8

const (
	PatronNone PatronID = iota // (none)
)

type PatronFlags uint32

const (
	FlagUnique PatronFlags = 1 << iota
)

type PatronData struct {
	Flags PatronFlags
}

var Patrons = [...]PatronData{}
