//go:generate go run golang.org/x/tools/cmd/stringer -type=ExplorerID -linecomment

package tables

import "git.lubar.me/ben/spy-cards/gfx/sprites"

type ExplorerID uint8

const (
	ExplorerNone       ExplorerID = iota // (none)
	ExplorerAnt                          // Team Ant
	ExplorerCelia                        // Team Celia
	ExplorerMothiva                      // Team Mothiva
	ExplorerSlacker                      // Team Slacker
	ExplorerMaki                         // Team Maki
	ExplorerSnakemouth                   // Team Snakemouth
)

type ExplorerData struct {
	Sprite           *sprites.Sprite
	Members          []string
	Cost             uint64
	NotAllowed       ForagingID
	ForagingModifier [7]int8
}

var Explorers = [...]ExplorerData{
	ExplorerAnt: {
		Sprite:     sprites.TablesAnt,
		Members:    []string{"Gen", "Eri"},
		Cost:       100,
		NotAllowed: ForagingForsakenLands,
		ForagingModifier: [...]int8{
			ForagingBugariaOutskirts: 1,
			ForagingSnakemouthDen:    -2,
			ForagingHoneyFactory:     1,
			ForagingLostSands:        0,
			ForagingGoldenHills:      0,
			ForagingForsakenLands:    0,
		},
	},
	ExplorerCelia: {
		Sprite:  sprites.TablesCelia,
		Members: []string{"Levi", "Celia"},
		Cost:    100,
		ForagingModifier: [...]int8{
			ForagingBugariaOutskirts: -1,
			ForagingSnakemouthDen:    0,
			ForagingHoneyFactory:     0,
			ForagingLostSands:        1,
			ForagingGoldenHills:      2,
			ForagingForsakenLands:    -1,
		},
	},
	ExplorerMothiva: {
		Sprite:     sprites.TablesMothiva,
		Members:    []string{"Mothiva", "Zasp"},
		Cost:       300,
		NotAllowed: ForagingHoneyFactory,
		ForagingModifier: [...]int8{
			ForagingBugariaOutskirts: 0,
			ForagingSnakemouthDen:    -1,
			ForagingHoneyFactory:     0,
			ForagingLostSands:        0,
			ForagingGoldenHills:      2,
			ForagingForsakenLands:    -1,
		},
	},
	ExplorerSlacker: {
		Sprite:  sprites.TablesSlacker,
		Members: []string{"Stratos", "Delilah"},
		Cost:    200,
		ForagingModifier: [...]int8{
			ForagingBugariaOutskirts: 0,
			ForagingSnakemouthDen:    0,
			ForagingHoneyFactory:     0,
			ForagingLostSands:        0,
			ForagingGoldenHills:      0,
			ForagingForsakenLands:    0,
		},
	},
	ExplorerMaki: {
		Sprite:  sprites.TablesMaki,
		Members: []string{"Maki", "Kina"},
		Cost:    200,
		ForagingModifier: [...]int8{
			ForagingBugariaOutskirts: 0,
			ForagingSnakemouthDen:    1,
			ForagingHoneyFactory:     -2,
			ForagingLostSands:        0,
			ForagingGoldenHills:      -1,
			ForagingForsakenLands:    1,
		},
	},
	ExplorerSnakemouth: {
		Sprite:  sprites.TablesSnakemouth,
		Members: []string{"Vi", "Kabbu", "Leif"},
		Cost:    400,
		ForagingModifier: [...]int8{
			ForagingBugariaOutskirts: 1,
			ForagingSnakemouthDen:    1,
			ForagingHoneyFactory:     1,
			ForagingLostSands:        1,
			ForagingGoldenHills:      1,
			ForagingForsakenLands:    1,
		},
	},
}
