//go:generate go run golang.org/x/tools/cmd/stringer -type=MerchantID -linecomment

package tables

import "git.lubar.me/ben/spy-cards/gfx/sprites"

type MerchantID uint8

const (
	MerchantNone            MerchantID = iota // (none)
	MerchantMadameButterfly                   // Madame Butterfly
	MerchantCricketly                         // Cricketly
	MerchantSandy                             // Sandy
	MerchantClaire                            // Claire
	MerchantCrisbee                           // Crisbee
	MerchantNeil                              // Neil
	MerchantXic                               // Xic
	MerchantGein                              // Gein
	MerchantYumnum                            // Yumnum
	MerchantSkirby                            // Skirby
	MerchantCherryGuy                         // Shady Contact
	MerchantJayde                             // Jayde
	MerchantTyna                              // Tyna
	MerchantRina                              // Rina
)

type MerchantData struct {
	Pronouns Pronoun
	Sprite   *sprites.Sprite
	Items    []ItemCost
}

type ItemCost struct {
	Item ItemID `json:"i"`
	Cost uint8  `json:"c"`
}

var Merchants = [...]MerchantData{
	MerchantMadameButterfly: {
		Pronouns: PronounSheHer,
		Items: []ItemCost{
			{
				Item: CrunchyLeaf,
				Cost: 3,
			},
			{
				Item: HoneyDrop,
				Cost: 4,
			},
			{
				Item: AphidEgg,
				Cost: 2,
			},
			{
				Item: Mushroom,
				Cost: 3,
			},
			{
				Item: DangerShroom,
				Cost: 3,
			},
		},
		Sprite: sprites.TablesMadameButterfly,
	},
	MerchantCricketly: {
		Pronouns: PronounSheHer,
		Items: []ItemCost{
			{
				Item: SpicyBerry,
				Cost: 12,
			},
			{
				Item: BurlyBerry,
				Cost: 12,
			},
			{
				Item: MagicSeed,
				Cost: 18,
			},
			{
				Item: SucculentBerry,
				Cost: 10,
			},
		},
		Sprite: sprites.TablesCricketly,
	},
	MerchantSandy: {
		Pronouns: PronounSheHer,
		Items: []ItemCost{
			{
				Item: DryBread,
				Cost: 7,
			},
			{
				Item: CrunchyLeaf,
				Cost: 3,
			},
			{
				Item: ClearWater,
				Cost: 3,
			},
			{
				Item: AgaricShroom,
				Cost: 7,
			},
			{
				Item: HustleBerry,
				Cost: 18,
			},
		},
		Sprite: sprites.TablesSandy,
	},
	MerchantClaire: {
		Pronouns: PronounSheHer,
		Items: []ItemCost{
			{
				Item: HoneydLeaf,
				Cost: 10,
			},
			{
				Item: LeafOmelet,
				Cost: 10,
			},
			{
				Item: GlazedHoney,
				Cost: 9,
			},
			{
				Item: HoneyIceCream,
				Cost: 20,
			},
			{
				Item: ShockBerry,
				Cost: 11,
			},
		},
		Sprite: sprites.TablesClaire,
	},
	MerchantCrisbee: {
		Pronouns: PronounHeHim,
		Items: []ItemCost{
			{
				Item: HoneyDrop,
				Cost: 4,
			},
			{
				Item: BagOfFlour,
				Cost: 4,
			},
			{
				Item: NuttyCake,
				Cost: 10,
			},
		},
		Sprite: sprites.TablesCrisbee,
	},
	MerchantNeil: {
		Pronouns: PronounHeHim,
		Items: []ItemCost{
			{
				Item: MagicIce,
				Cost: 10,
			},
		},
		Sprite: sprites.TablesNeil,
	},
	MerchantXic: {
		Pronouns: PronounHeHim,
		Items: []ItemCost{
			{
				Item: DangerSpud,
				Cost: 3,
			},
			{
				Item: DangerShroom,
				Cost: 3,
			},
			{
				Item: PoisonBomb,
				Cost: 28,
			},
			{
				Item: PoisonDart,
				Cost: 8,
			},
		},
		Sprite: sprites.TablesXic,
	},
	MerchantGein: {
		Pronouns: PronounHeHim,
		Items: []ItemCost{
			{
				Item: ClearWater,
				Cost: 3,
			},
			{
				Item: HardSeed,
				Cost: 4,
			},
			{
				Item: HoneyDrop,
				Cost: 4,
			},
			{
				Item: AphidDew,
				Cost: 9,
			},
			{
				Item: AphidEgg,
				Cost: 3,
			},
		},
		Sprite: sprites.TablesGein,
	},
	MerchantYumnum: {
		Pronouns: PronounHeHim,
		Items: []ItemCost{
			{
				Item: NumbnailDart,
				Cost: 8,
			},
			{
				Item: SleepBomb,
				Cost: 30,
			},
			{
				Item: DrowsyCake,
				Cost: 10,
			},
		},
		Sprite: sprites.TablesYumnum,
	},
	MerchantSkirby: {
		Pronouns: PronounHeHim,
		Items: []ItemCost{
			{
				Item: TangyBerry,
				Cost: 40,
			},
		},
		Sprite: sprites.TablesSkirby,
	},
	MerchantCherryGuy: {
		Pronouns: PronounHeHim,
		Items: []ItemCost{
			{
				Item: DarkCherries,
				Cost: 50,
			},
		},
		Sprite: sprites.TablesCherryGuy,
	},
	MerchantJayde: {
		Pronouns: PronounSheHer,
		Items: []ItemCost{
			{
				Item: JaydesStew,
				Cost: 25,
			},
			{
				Item: MagicSeed,
				Cost: 18,
			},
			{
				Item: MushroomSalad,
				Cost: 7,
			},
		},
		Sprite: sprites.TablesJayde,
	},
	MerchantTyna: {
		Pronouns: PronounSheHer,
		Items: []ItemCost{
			{
				Item: Squash,
				Cost: 9,
			},
			{
				Item: MegaRush,
				Cost: 15,
			},
			{
				Item: AphidShake,
				Cost: 15,
			},
			{
				Item: SpicyFries,
				Cost: 12,
			},
			{
				Item: MiteBurger,
				Cost: 15,
			},
		},
		Sprite: sprites.TablesTyna,
	},
	MerchantRina: {
		Pronouns: PronounSheHer,
		Items: []ItemCost{
			{
				Item: MagicSeed,
				Cost: 17,
			},
			{
				Item: MysteryBerry,
				Cost: 23,
			},
		},
		Sprite: sprites.TablesRina,
	},
}
