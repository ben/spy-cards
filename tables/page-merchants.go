package tables

import (
	"context"

	"git.lubar.me/ben/spy-cards/gfx/sprites"
	"git.lubar.me/ben/spy-cards/internal/router"
)

func (s *State) updateMerchants(ctx context.Context) (bool, *router.PageInfo, error) {
	// TODO: merchants setup

	return false, nil, nil
}

func (s *State) renderMerchants(ctx context.Context) error {
	// TODO: merchants setup

	sprites.DrawTextBorder(s.batch, sprites.FontBubblegumSans, "TODO: merchants setup", 0, 0, 0, 2, 2, sprites.White, sprites.Black, true)

	return nil
}
