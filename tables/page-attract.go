package tables

import (
	"context"
	"crypto/sha256"
	"strconv"

	"git.lubar.me/ben/spy-cards/gfx/sprites"
	"git.lubar.me/ben/spy-cards/internal/router"
	"git.lubar.me/ben/spy-cards/rng"
	"git.lubar.me/ben/spy-cards/tables/tables3d"
)

var cookingMinigame CookingMinigameSequence

func (s *State) updateAttract(ctx context.Context) (bool, *router.PageInfo, error) {
	newScene := false

	switch {
	case s.attractProgress == 0, s.attractProgress == 3600:
		s.attractProgress = 0
		s.scene = SceneBugariaCommercial.Clone()
		ChefModel.Instances[0] = &tables3d.ModelInstance{
			Name: ChefFry.Model(),
			Sx:   1,
			Sy:   1,
			Sz:   1,
		}
		ChefModel.MarkDirty()

		newScene = true

		cookingMinigame = NewCookingMinigameSequence(rng.New(sha256.New, ""), CookRandom)
	case s.attractProgress == 1200:
		s.scene = SceneDefiantRootDining.Clone()
		ChefModel.Instances[0] = &tables3d.ModelInstance{
			Name: ChefCrisbee.Model(),
			Sx:   1,
			Sy:   1,
			Sz:   1,
		}
		ChefModel.MarkDirty()

		newScene = true
	case s.attractProgress == 2400:
		s.scene = SceneUndergroundBar.Clone()
		ChefModel.Instances[0] = &tables3d.ModelInstance{
			Name: ChefDoppel.Model(),
			Sx:   1,
			Sy:   1,
			Sz:   1,
		}
		ChefModel.MarkDirty()

		newScene = true
	}

	if newScene {
		s.scene.CamAngle[1] -= 0.2
	} else {
		s.scene.CamAngle[1] += 0.4 / 1200
	}

	s.attractProgress++

	cookingMinigame.Update(s.ictx)

	return false, nil, nil
}

func (s *State) renderAttract(ctx context.Context) error {
	// TODO: attract

	sprites.DrawTextBorder(s.batch, sprites.FontBubblegumSans, strconv.FormatUint(uint64(s.attractProgress), 10), 0, 0, 0, 2, 2, sprites.White, sprites.Black, true)

	cookingMinigame.Render(s.batch)

	return nil
}
