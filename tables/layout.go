package tables

// Furniture:
//   0 1 2 3
// 0 fpfpfpf
//   p p p p
// 1 fpfpfpf
//
// VPartition:
//    0 1 2
// 0 fpfpfpf
//   p p p p
// 1 fpfpfpf
//
// HPartition:
//   0 1 2 3
//   fpfpfpf
// 0 p p p p
//   fpfpfpf
//
// Therefore, len(Furniture) == w * h, len(VPartition) == (w-1) * h, len(HPartition) == w * (h-1)
// Furniture[x + y * w] is to the left of VPartition[x + y * w] and above HPartition[x + y * w]

type Layout struct {
	Furniture  []FurnitureID `json:"f"`
	VPartition []PartitionID `json:"v"`
	HPartition []PartitionID `json:"h"`
}

type FurnitureID uint8

const (
	FurnitureNone  FurnitureID = iota // (none)
	FurnitureStool                    // Stool
	FurnitureTable                    // Table
)

type PartitionID uint8

const (
	PartitionNone    PartitionID = iota // (none)
	PartitionGeneric                    // Partition
)
