//go:generate go run golang.org/x/tools/cmd/stringer -type LocationID -linecomment

package tables

import (
	"git.lubar.me/ben/spy-cards/gfx/sprites"
	"git.lubar.me/ben/spy-cards/tables/tables3d"
)

type LocationID uint8

const (
	LocationNone                 LocationID = iota // (none)
	LocationBugariaCommercial                      // Bugaria Commercial District
	LocationDefiantRoot                            // Defiant Root
	LocationUndergroundTavern                      // Underground Tavern
	LocationBeeCafeteria                           // Bee Kingdom Cafeteria
	LocationGoldenSettlementFine                   // Golden Settlement Fine Dining
	LocationWaspKingdomCart                        // Wasp Kingdom Food Cart
	LocationDineMite                               // DineMite™
	LocationMetalIsland                            // Metal Island Machine Cafe
	LocationApartment                              // Apartment 301-B
	LocationColosseum                              // Termite Colosseum Food Court
)

type LocationFlags uint32

const (
	FlagOutdoor LocationFlags = 1 << iota
	FlagFancy
	FlagConveyorBelt
	FlagCarryOut
	FlagBeeOnly
	FlagNoAnts
	FlagNoLadybugs
)

type LocationData struct {
	Flags        LocationFlags
	Noise        uint8
	Rent         uint8
	NearMerchant []MerchantID
	Sprite       *sprites.Sprite
	FullLoc      [3]float32
	Scene        *tables3d.Scene
}

var Locations = [...]LocationData{
	LocationBugariaCommercial: {
		Flags: FlagOutdoor | FlagNoLadybugs,
		Noise: 35,
		Rent:  10,
		NearMerchant: []MerchantID{
			MerchantMadameButterfly,
			MerchantCricketly,
		},
		Sprite:  sprites.TablesBugariaCommercial,
		FullLoc: [3]float32{-3, 0, 14.5},
		Scene:   &SceneBugariaCommercial,
	},
	LocationDefiantRoot: {
		Flags: FlagOutdoor,
		Noise: 15,
		Rent:  10,
		NearMerchant: []MerchantID{
			MerchantCricketly,
			MerchantSandy,
			MerchantCrisbee,
			MerchantNeil,
			MerchantXic,
		},
		Sprite:  sprites.TablesDefiantRoot,
		FullLoc: [3]float32{-4.75, 0, -6},
		Scene:   &SceneDefiantRootDining,
	},
	LocationUndergroundTavern: {
		Flags: 0,
		Noise: 25,
		Rent:  10,
		NearMerchant: []MerchantID{
			MerchantMadameButterfly,
			MerchantCricketly,
			MerchantCherryGuy,
		},
		Sprite:  sprites.TablesUndergroundTavern,
		FullLoc: [3]float32{-2, 0, 14.5},
		Scene:   &SceneUndergroundBar,
	},
	LocationBeeCafeteria: {
		Flags: FlagConveyorBelt | FlagBeeOnly,
		Noise: 40,
		Rent:  10,
		NearMerchant: []MerchantID{
			MerchantClaire,
		},
		Sprite:  sprites.TablesBeeCafeteria,
		FullLoc: [3]float32{-11.5, -37, -17},
	},
	LocationGoldenSettlementFine: {
		Flags: FlagOutdoor | FlagFancy,
		Noise: 5,
		Rent:  20,
		NearMerchant: []MerchantID{
			MerchantCricketly,
			MerchantGein,
			MerchantYumnum,
			MerchantJayde,
		},
		Sprite:  sprites.TablesGoldenSettlementFine,
		FullLoc: [3]float32{-13, 0, 11},
	},
	LocationWaspKingdomCart: {
		Flags: FlagCarryOut,
		Noise: 15,
		Rent:  5,
		NearMerchant: []MerchantID{
			MerchantJayde,
		},
		Sprite:  sprites.TablesWaspKingdomCart,
		FullLoc: [3]float32{-0.75, 0, -28},
	},
	LocationDineMite: {
		Flags: FlagCarryOut | FlagNoAnts,
		Noise: 200,
		Rent:  10,
		NearMerchant: []MerchantID{
			MerchantTyna,
		},
		Sprite:  sprites.TablesDineMite,
		FullLoc: [3]float32{6, 0, 27.5},
	},
	LocationMetalIsland: {
		Flags: FlagFancy | FlagConveyorBelt,
		Noise: 10,
		Rent:  25,
		NearMerchant: []MerchantID{
			MerchantSkirby,
		},
		Sprite:  sprites.TablesMetalIsland,
		FullLoc: [3]float32{29.5, 0, 25},
	},
	LocationApartment: {
		Flags: FlagNoAnts,
		Noise: 25,
		Rent:  5,
		NearMerchant: []MerchantID{
			MerchantTyna,
		},
		Sprite:  sprites.TablesApartment,
		FullLoc: [3]float32{6, 0, 27.5},
	},
	LocationColosseum: {
		Flags: FlagNoAnts,
		Noise: 50,
		Rent:  15,
		NearMerchant: []MerchantID{
			MerchantCricketly,
			MerchantTyna,
		},
		Sprite:  sprites.TablesColosseum,
		FullLoc: [3]float32{6, 0, 27.5},
	},
}
