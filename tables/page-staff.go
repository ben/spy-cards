package tables

import (
	"context"

	"git.lubar.me/ben/spy-cards/gfx/sprites"
	"git.lubar.me/ben/spy-cards/internal/router"
)

func (s *State) updateStaff(ctx context.Context) (bool, *router.PageInfo, error) {
	// TODO: staff setup

	return false, nil, nil
}

func (s *State) renderStaff(ctx context.Context) error {
	// TODO: staff setup

	sprites.DrawTextBorder(s.batch, sprites.FontBubblegumSans, "TODO: staff setup", 0, 0, 0, 2, 2, sprites.White, sprites.Black, true)

	return nil
}
