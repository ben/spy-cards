//go:generate go run golang.org/x/tools/cmd/stringer -type=CookType -trimprefix=Cook

package tables

import (
	"git.lubar.me/ben/spy-cards/gfx/sprites"
	"git.lubar.me/ben/spy-cards/input"
	"git.lubar.me/ben/spy-cards/rng"
)

const (
	successSafetyFrames = 10
	requiredSuccesses   = 10
	wrongDazeTime       = 10
)

type CookType uint8

const (
	CookNone CookType = iota
	CookRandom
	CookStir
	CookChop
	CookTimed
	CookWiggle
)

type CookData struct {
	Sequence [][]input.Button
	Check    []input.Button
}

var Minigames = [...]CookData{
	CookStir: {
		Sequence: [][]input.Button{
			{input.BtnUp, input.BtnRight, input.BtnDown, input.BtnLeft},
			{input.BtnUp, input.BtnLeft, input.BtnDown, input.BtnRight},
		},
		Check: []input.Button{input.BtnUp, input.BtnDown, input.BtnLeft, input.BtnRight},
	},
	CookChop: {
		Sequence: [][]input.Button{
			{input.BtnConfirm},
			{input.BtnCancel},
			{input.BtnSwitch},
		},
		Check: []input.Button{input.BtnConfirm, input.BtnCancel, input.BtnSwitch},
	},
	CookTimed: {
		Sequence: [][]input.Button{
			{input.BtnConfirm},
			{input.BtnCancel},
			{input.BtnSwitch},
		},
		Check: []input.Button{input.BtnConfirm, input.BtnCancel, input.BtnSwitch},
	},
	CookWiggle: {
		Sequence: [][]input.Button{
			{input.BtnUp, input.BtnDown},
			{input.BtnLeft, input.BtnRight},
		},
		Check: []input.Button{input.BtnUp, input.BtnDown, input.BtnLeft, input.BtnRight},
	},
}

type CookingMinigameFlags uint8

const (
	MinigameActive CookingMinigameFlags = 1 << iota
	MinigameDone
)

type CookingMinigame struct {
	Flags    CookingMinigameFlags
	Type     CookType
	Seed     uint8
	Progress uint8
	Dazed    uint8
	Safety   uint8
	Time     uint16
	MaxTime  uint16
}

func NewCookingMinigame(r *rng.RNG, ty CookType) *CookingMinigame {
	if ty == CookRandom {
		ty = CookType(r.RangeInt(int(CookStir), int(CookWiggle+1)))
	}

	return &CookingMinigame{
		Type:    ty,
		Seed:    uint8(r.RangeInt(0, 256)),
		MaxTime: 7200,
	}
}

func (c *CookingMinigame) Update(ictx *input.Context) {
	if c.Time >= c.MaxTime {
		return
	}

	if c.Flags&(MinigameActive|MinigameDone) == MinigameActive {
		c.Time++

		if c.Dazed != 0 {
			c.Dazed--

			return
		}

		correct := c.nextButton(0)
		if correct != input.NumButtons && ictx.Consume(correct) {
			c.Progress++

			if c.Progress >= requiredSuccesses {
				c.Flags |= MinigameDone
			}

			c.Safety = successSafetyFrames
		}

		if c.Safety != 0 {
			c.Safety--
		} else {
			any := false

			for _, b := range Minigames[c.Type].Check {
				any = ictx.Consume(b) || any
			}

			if any {
				c.Dazed = wrongDazeTime
			}
		}
	}
}

func (c *CookingMinigame) nextButton(offset int) input.Button {
	mg := &Minigames[c.Type]
	if len(mg.Sequence) == 0 {
		return input.NumButtons
	}

	// TODO: timed

	seq := mg.Sequence[c.Seed%uint8(len(mg.Sequence))]

	n := int(c.Progress) + int(c.Seed)/len(mg.Sequence) + offset
	n %= len(seq)

	if n < 0 {
		n += len(seq)
	}

	return seq[n]
}

func (c *CookingMinigame) Render(batch *sprites.Batch) {
	sprites.DrawText(batch, sprites.FontBubblegumSans, sprites.Button(c.nextButton(0)), 0, 0, 0, 3, 3, sprites.White)
}

type CookingMinigameSequence []*CookingMinigame

func NewCookingMinigameSequence(r *rng.RNG, ty ...CookType) CookingMinigameSequence {
	s := make([]*CookingMinigame, 0, len(ty))

	for _, t := range ty {
		if t != CookNone {
			s = append(s, NewCookingMinigame(r, t))
		}
	}

	return s
}

func (s CookingMinigameSequence) Update(ictx *input.Context) {
	for _, c := range s {
		c.Update(ictx)
	}
}

func (s CookingMinigameSequence) Render(batch *sprites.Batch) {
	for _, c := range s {
		c.Render(batch)
	}
}
