// Code generated by "stringer -type=CookType -trimprefix=Cook"; DO NOT EDIT.

package tables

import "strconv"

func _() {
	// An "invalid array index" compiler error signifies that the constant values have changed.
	// Re-run the stringer command to generate them again.
	var x [1]struct{}
	_ = x[CookNone-0]
	_ = x[CookRandom-1]
	_ = x[CookStir-2]
	_ = x[CookChop-3]
	_ = x[CookTimed-4]
	_ = x[CookWiggle-5]
}

const _CookType_name = "NoneRandomStirChopTimedWiggle"

var _CookType_index = [...]uint8{0, 4, 10, 14, 18, 23, 29}

func (i CookType) String() string {
	if i >= CookType(len(_CookType_index)-1) {
		return "CookType(" + strconv.FormatInt(int64(i), 10) + ")"
	}
	return _CookType_name[_CookType_index[i]:_CookType_index[i+1]]
}
