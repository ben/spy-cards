package tables

import (
	"context"
	"strconv"

	"git.lubar.me/ben/spy-cards/gfx/sprites"
	"git.lubar.me/ben/spy-cards/internal/router"
)

func (s *State) updatePreDay(ctx context.Context) (bool, *router.PageInfo, error) {
	// TODO: pre-day setup

	return false, nil, nil
}

func (s *State) renderPreDay(ctx context.Context) error {
	// TODO: pre-day setup

	rank := int(s.Persistent.Rank())

	sprites.DrawTextBorder(s.batch, sprites.FontBubblegumSans, "Staff", -8, 6, 0, 2, 2, sprites.White, sprites.Black, true)

	for i := range Unlocks {
		if len(Unlocks[i].Explorers) != 0 {
			if i > rank {
				sprites.DrawTextBorder(s.batch, sprites.FontBubblegumSans, "Unlocks at Rank "+strconv.Itoa(i), 8, 6, 0, 1.5, 1.5, sprites.Gray, sprites.Black, true)

				break
			}

			sprites.DrawTextBorder(s.batch, sprites.FontBubblegumSans, "Explorers", 8, 6, 0, 2, 2, sprites.White, sprites.Black, true)

			break
		}
	}

	sprites.DrawTextBorder(s.batch, sprites.FontBubblegumSans, "Merchants", -8, -6, 0, 2, 2, sprites.White, sprites.Black, true)

	if rank < 4 {
		sprites.DrawTextBorder(s.batch, sprites.FontBubblegumSans, "Unlocks at Rank 4", 8, -6, 0, 1.5, 1.5, sprites.Gray, sprites.Black, true)
	} else {
		sprites.DrawTextBorder(s.batch, sprites.FontBubblegumSans, "Layout", 8, -6, 0, 2, 2, sprites.White, sprites.Black, true)
	}

	sprites.DrawTextBorder(s.batch, sprites.FontD3Streetism, "Preparing for Day "+strconv.FormatUint(s.Data.Day+1, 10), 0, 0, 0, 2, 2, sprites.White, sprites.Black, true)

	return nil
}
