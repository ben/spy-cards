package tables

import (
	"context"
	"encoding/json"
	"fmt"
	"log"

	"git.lubar.me/ben/spy-cards/gfx"
	"git.lubar.me/ben/spy-cards/gfx/sprites"
	"git.lubar.me/ben/spy-cards/input"
	"git.lubar.me/ben/spy-cards/internal"
	"git.lubar.me/ben/spy-cards/internal/router"
	"git.lubar.me/ben/spy-cards/screenreader"
	"git.lubar.me/ben/spy-cards/tables/tables3d"
	"golang.org/x/mobile/gl"
)

type PageID uint8

const (
	PageNone PageID = iota
	PageAttract
	PageSaves
	PageSetupChefLoc
	PageSetupMenu
	PagePreDay
	PageForaging
	PageMerchants
	PageStaff
	PageLayout
)

type State struct {
	ictx  *input.Context
	cam   gfx.Camera
	batch *sprites.Batch
	Page  PageID `json:"s"`

	scene *tables3d.Scene

	// attract
	attractProgress uint32

	// setup screens
	rankHover  bool
	inputIndex uint8

	// chef/location
	useTargetOffset [2]bool
	lastOffset      [2]uint8
	lastLocation    LocationID
	locationLerp    uint8
	targetOffset    [2]float32
	offset          [2]float32

	// menu
	menuOffset     [2]uint8
	inEditCost     bool
	menuCandidates []menuItemCandidate

	// pre-day

	Persistent *PersistentData `json:"p"`
	Data       *GameData       `json:"d"`
}

func New(ctx context.Context) (*State, error) {
	initScenesOnce.Do(initScenes)

	s := &State{
		ictx:       input.GetContext(ctx),
		scene:      &SceneBugariaFull,
		Persistent: &PersistentData{},
		Data:       NewGameData(),
	}

	s.cam.SetDefaults()
	s.cam.Offset.Identity()
	s.cam.Rotation.Identity()
	s.cam.Position.Identity()

	s.Page = PageAttract

	if false {
		if b, err := internal.LoadData("spy-cards-bug-tables-v0"); err != nil {
			return nil, fmt.Errorf("tables: loading save data: %w", err)
		} else if b != nil {
			err = json.Unmarshal(b, s)
			if err != nil {
				return nil, fmt.Errorf("tables: decoding save data: %w", err)
			}
		}

		if s.Page == PageNone {
			s.Page = PageSetupChefLoc
			s.Data.Objective.Target = ObjectiveMeals
			s.Data.Objective.TargetNum = 10
			s.Data.Objective.Limit = ObjectiveNoLimit
			s.Data.Objective.Days = 30
		}
	}

	return s, nil
}

func (s *State) Save() {
	b, err := json.Marshal(s)
	if err != nil {
		log.Println("WARNING: marshaling Bug Tables state for autosave:", err)

		return
	}

	err = internal.StoreData("spy-cards-bug-tables-v0", b)
	if err != nil {
		log.Println("WARNING: Bug Tables autosave failed:", err)
	}
}

func (s *State) MakeScreenReaderUI() *screenreader.UI {
	return &screenreader.UI{
		// TODO
	}
}

func (s *State) Resize(ctx context.Context) error {
	w, h := gfx.Size()

	s.cam.Perspective.Scale(-64/float32(w), -64/float32(h), -0.01)
	s.cam.MarkDirty()

	return nil
}

func (s *State) Update(ctx context.Context, frameAdvance bool) (*router.PageInfo, error) {
	if !frameAdvance {
		return nil, nil
	}

	for {
		var (
			again    bool
			redirect *router.PageInfo
			err      error
		)

		switch s.Page {
		case PageAttract:
			again, redirect, err = s.updateAttract(ctx)
		case PageSetupChefLoc:
			again, redirect, err = s.updateSetupChefLoc(ctx)
		case PageSetupMenu:
			again, redirect, err = s.updateSetupMenu(ctx)
		case PagePreDay:
			again, redirect, err = s.updatePreDay(ctx)
		case PageForaging:
			again, redirect, err = s.updateForaging(ctx)
		case PageMerchants:
			again, redirect, err = s.updateMerchants(ctx)
		case PageStaff:
			again, redirect, err = s.updateStaff(ctx)
		case PageLayout:
			again, redirect, err = s.updateLayout(ctx)
		default:
			return nil, fmt.Errorf("tables: unexpected page in Update: %v", s.Page)
		}

		if !again || err != nil {
			return redirect, err
		}
	}
}

func (s *State) renderBackground() {
	w, h := gfx.Size()

	s.batch.Append(sprites.Blank, 0, 0, 0, float32(w)/32, float32(h)/32, sprites.Color{R: 95, G: 63, B: 31, A: 224})
}

func (s *State) Render(ctx context.Context) error {
	if s.scene != nil {
		s.scene.Render()
	}

	gfx.GL.Disable(gl.DEPTH_TEST)

	if s.batch == nil {
		s.batch = sprites.NewBatch(&s.cam)
	} else {
		s.batch.Reset(&s.cam)
	}

	var err error

	switch s.Page {
	case PageAttract:
		err = s.renderAttract(ctx)
	case PageSetupChefLoc:
		err = s.renderSetupChefLoc(ctx)
	case PageSetupMenu:
		err = s.renderSetupMenu(ctx)
	case PagePreDay:
		err = s.renderPreDay(ctx)
	case PageForaging:
		err = s.renderForaging(ctx)
	case PageMerchants:
		err = s.renderMerchants(ctx)
	case PageStaff:
		err = s.renderStaff(ctx)
	case PageLayout:
		err = s.renderLayout(ctx)
	default:
		err = fmt.Errorf("tables: unexpected page in Render: %v", s.Page)
	}

	s.batch.Render()

	return err
}
