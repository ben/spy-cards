//go:generate go run golang.org/x/tools/cmd/stringer -type=ItemID -linecomment

package tables

import "git.lubar.me/ben/spy-cards/gfx/sprites"

type ItemID uint8

const (
	ItemNone            ItemID = iota // (none)
	Mistake                           // Mistake
	BigMistake                        // Big Mistake
	CrunchyLeaf                       // Crunchy Leaf
	HoneyDrop                         // Honey Drop
	AphidEgg                          // Aphid Egg
	Mushroom                          // Mushroom
	DangerShroom                      // Danger Shroom
	AgaricShroom                      // Agaric Shroom
	AphidDew                          // Aphid Dew
	HardSeed                          // Hard Seed
	BagOfFlour                        // Bag of Flour
	ClearWater                        // Clear Water
	MagicIce                          // Magic Ice
	DangerSpud                        // Danger Spud
	Squash                            // Squash
	SpicyBerry                        // Spicy Berry
	BurlyBerry                        // Burly Berry
	ShockBerry                        // Shock Berry
	SucculentBerry                    // Succulent Berry
	HustleBerry                       // Hustle Berry
	TangyBerry                        // Tangy Berry
	DarkCherries                      // Dark Cherries
	MagicSeed                         // Magic Seed
	NumbnailDart                      // Numbnail Dart
	PoisonDart                        // Poison Dart
	MysteryBerry                      // Mystery Berry
	LeafSalad                         // Leaf Salad
	GlazedHoney                       // Glazed Honey
	Abomihoney                        // Abomihoney
	CookedShroom                      // Cooked Shroom
	CookedDanger                      // Cooked Danger
	SweetShroom                       // Sweet Shroom
	SweetDanger                       // Sweet Danger
	FriedEgg                          // Fried Egg
	LeafOmelet                        // Leaf Omelet
	HoneydLeaf                        // Honey'd Leaf
	RoastedBerries                    // Roasted Berries
	SucculentPlatter                  // Succulent Platter
	SucculentCookies                  // Succulent Cookies
	BerryJuice                        // Berry Juice
	MushroomSkewer                    // Mushroom Skewer
	HeartyBreakfast                   // Hearty Breakfast
	MushroomSalad                     // Mushroom Salad
	SweetDew                          // Sweet Dew
	BakedYam                          // Baked Yam
	SpicyFries                        // Spicy Fries
	BurlyChips                        // Burly Chips
	DangerDish                        // Danger Dish
	ShavedIce                         // Shaved Ice
	ColdSalad                         // Cold Salad
	IceCream                          // Ice Cream
	HoneyIceCream                     // Honey Ice Cream
	DryBread                          // Dry Bread
	NuttyCake                         // Nutty Cake
	PoisonCake                        // Poison Cake
	FrostPie                          // Frost Pie
	YamBread                          // Yam Bread
	ShockCandy                        // Shock Candy
	MushroomGummies                   // Mushroom Gummies
	SweetPudding                      // Sweet Pudding
	LeafCroissant                     // Leaf Croissant
	HoneyPancakes                     // Honey Pancakes
	DrowsyCake                        // Drowsy Cake
	HustleCandy                       // Hustle Candy
	SpicyCandy                        // Spicy Candy
	BurlyCandy                        // Burly Candy
	BerryJam                          // Berry Jam
	HotDrink                          // Hot Drink
	PlainTea                          // Plain Tea
	SpicyTea                          // Spicy Tea
	BurlyTea                          // Burly Tea
	AphidShake                        // Aphid Shake
	SquashPuree                       // Squash Puree
	SquashCandy                       // Squash Candy
	SquashTart                        // Squash Tart
	PlumplingPie                      // Plumpling Pie
	TangyJam                          // Tangy Jam
	TangyJuice                        // Tangy Juice
	TangyPie                          // Tangy Pie
	SpicyBomb                         // Spicy Bomb
	BurlyBomb                         // Burly Bomb
	PoisonBomb                        // Poison Bomb
	SleepBomb                         // Sleep Bomb
	NumbBomb                          // Numb Bomb
	FrostBomb                         // Frost Bomb
	ClearBomb                         // Clear Bomb
	Abombination                      // Abombination
	CherryBombs                       // Cherry Bombs
	CherryPie                         // Cherry Pie
	BerrySmoothie                     // Berry Smoothie
	MiracleShake                      // Miracle Shake
	TangyCarpaccio                    // Tangy Carpaccio
	CrisbeeDonut                      // Crisbee Donut
	QueensDinner                      // Queen's Dinner
	MegaRush                          // Mega-Rush™
	MiteBurger                        // Mite Burger
	JaydesStew                        // Jayde's Stew
	RandomMysteryBerry1               // MB1 (should not be displayed)
	RandomMysteryBerry2               // MB2 (should not be displayed)
)

type ItemFlags uint32

const (
	FlagGarbage    ItemFlags = 1 << iota // patrons will not order. -2 quality.
	FlagCold                             // complaint for leaving it out will be "warm" rather than "cold". sells better at warm locations.
	FlagHard                             // can injure patrons, causing -2 overall quality.
	FlagIce                              // chance of freezing patrons, causing slower eating and -1 quality.
	FlagPoison                           // if not ordered, -2 quality. only brave patrons will order.
	FlagSleep                            // chance of making patrons fall asleep. +1 quality, but slower eating.
	FlagNumb                             // chance of numbing patrons. moves overall visit quality towards zero by 1 point.
	FlagExplodes                         // patrons will not order. if eaten, causes an AOE effect.
	FlagRaw                              // patrons will not order. -1 quality.
	FlagIngredient                       // -1 quality if price is greater than or equal to value.
	FlagSticky                           // makes a mess.
	FlagCrunchy                          // flavor. preferred by ants.
	FlagSavory                           // flavor.
	FlagSweet                            // flavor. preferred by bees and children.
	FlagSpicy                            // flavor. brave patrons will order.
	FlagBitter                           // flavor. children do not order. +1 quality if ordered, -1 quality if not ordered.
	FlagCaffeine                         // faster eating. preferred by termites.
	FlagLiquid                           // patrons can drink this item.
	FlagCleanse                          // +2 to overall quality if overall quality is negative.
	FlagTangy                            // contains tangy berries.
)

type ItemData struct {
	Flags        ItemFlags
	Value        uint8
	RequiredChef ChefID
	Sprite       *sprites.Sprite
}

var Items = [...]ItemData{
	Mistake: {
		Flags:  FlagGarbage | FlagPoison,
		Value:  2,
		Sprite: sprites.TablesItems[23],
	},
	BigMistake: {
		Flags:  FlagGarbage | FlagPoison,
		Value:  1,
		Sprite: sprites.TablesItems[51],
	},
	CrunchyLeaf: {
		Flags:  FlagIngredient | FlagCrunchy | FlagSavory,
		Value:  3,
		Sprite: sprites.TablesItems[0],
	},
	HoneyDrop: {
		Flags:  FlagIngredient | FlagSticky | FlagSweet,
		Value:  4,
		Sprite: sprites.TablesItems[1],
	},
	AphidEgg: {
		Flags:  FlagIngredient | FlagRaw,
		Value:  2,
		Sprite: sprites.TablesItems[16],
	},
	Mushroom: {
		Flags:  FlagIngredient | FlagSavory,
		Value:  3,
		Sprite: sprites.TablesItems[12],
	},
	DangerShroom: {
		Flags:  FlagIngredient | FlagPoison,
		Value:  3,
		Sprite: sprites.TablesItems[25],
	},
	AgaricShroom: {
		Flags:  FlagIngredient | FlagNumb,
		Value:  7,
		Sprite: sprites.TablesItems[95],
	},
	AphidDew: {
		Flags:  FlagIngredient | FlagLiquid | FlagSavory,
		Value:  5,
		Sprite: sprites.TablesItems[47],
	},
	HardSeed: {
		Flags:  FlagIngredient | FlagRaw | FlagCrunchy | FlagHard,
		Value:  3,
		Sprite: sprites.TablesItems[22],
	},
	BagOfFlour: {
		Flags:  FlagIngredient | FlagRaw | FlagSavory,
		Value:  4,
		Sprite: sprites.TablesItems[67],
	},
	ClearWater: {
		Flags:  FlagIngredient | FlagLiquid | FlagCleanse | FlagCold,
		Value:  3,
		Sprite: sprites.TablesItems[11],
	},
	MagicIce: {
		Flags:  FlagIngredient | FlagHard | FlagRaw | FlagCrunchy | FlagIce | FlagCold,
		Value:  10,
		Sprite: sprites.TablesItems[41],
	},
	DangerSpud: {
		Flags:  FlagIngredient | FlagPoison | FlagSavory,
		Value:  3,
		Sprite: sprites.TablesItems[63],
	},
	Squash: {
		Flags:  FlagIngredient | FlagRaw | FlagSavory,
		Value:  9,
		Sprite: sprites.TablesItems[58],
	},
	SpicyBerry: {
		Flags:  FlagIngredient | FlagSpicy,
		Value:  12,
		Sprite: sprites.TablesItems[2],
	},
	BurlyBerry: {
		Flags:  FlagIngredient | FlagBitter,
		Value:  12,
		Sprite: sprites.TablesItems[3],
	},
	ShockBerry: {
		Flags:  FlagIngredient | FlagNumb,
		Value:  11,
		Sprite: sprites.TablesItems[42],
	},
	SucculentBerry: {
		Flags:  FlagIngredient | FlagSweet,
		Value:  10,
		Sprite: sprites.TablesItems[89],
	},
	HustleBerry: {
		Flags:  FlagIngredient | FlagBitter | FlagCaffeine,
		Value:  18,
		Sprite: sprites.TablesItems[26],
	},
	TangyBerry: {
		Flags:  FlagIngredient | FlagSavory | FlagTangy,
		Value:  40,
		Sprite: sprites.TablesItems[76],
	},
	DarkCherries: {
		Flags:  FlagIngredient | FlagSweet | FlagRaw,
		Value:  50,
		Sprite: sprites.TablesItems[54],
	},
	MagicSeed: {
		Flags:  FlagIngredient | FlagRaw,
		Value:  18,
		Sprite: sprites.TablesItems[10],
	},
	NumbnailDart: {
		Flags:  FlagIngredient | FlagGarbage | FlagSleep,
		Value:  8,
		Sprite: sprites.TablesItems[39],
	},
	PoisonDart: {
		Flags:  FlagIngredient | FlagGarbage | FlagPoison,
		Value:  8,
		Sprite: sprites.TablesItems[87],
	},
	MysteryBerry: {
		Flags:  FlagIngredient | FlagGarbage | FlagRaw,
		Value:  23,
		Sprite: sprites.TablesItems[100],
	},
	LeafSalad: {
		Flags:  FlagCrunchy | FlagSavory,
		Value:  6,
		Sprite: sprites.TablesItems[15],
	},
	GlazedHoney: {
		Flags:  FlagSticky | FlagSweet,
		Value:  8,
		Sprite: sprites.TablesItems[19],
	},
	Abomihoney: {
		Flags:  FlagGarbage | FlagSticky | FlagSweet,
		Value:  8,
		Sprite: sprites.TablesItems[34],
	},
	CookedShroom: {
		Flags:  FlagSavory,
		Value:  5,
		Sprite: sprites.TablesItems[13],
	},
	CookedDanger: {
		Flags:  FlagSavory | FlagPoison,
		Value:  7,
		Sprite: sprites.TablesItems[27],
	},
	SweetShroom: {
		Flags:  FlagSweet | FlagSticky,
		Value:  10,
		Sprite: sprites.TablesItems[20],
	},
	SweetDanger: {
		Flags:  FlagSweet | FlagPoison | FlagSticky,
		Value:  11,
		Sprite: sprites.TablesItems[21],
	},
	FriedEgg: {
		Flags:  FlagSavory,
		Value:  7,
		Sprite: sprites.TablesItems[17],
	},
	LeafOmelet: {
		Flags:  FlagSavory | FlagCrunchy,
		Value:  9,
		Sprite: sprites.TablesItems[24],
	},
	HoneydLeaf: {
		Flags:  FlagSweet | FlagSticky | FlagCrunchy,
		Value:  9,
		Sprite: sprites.TablesItems[9],
	},
	RoastedBerries: {
		Flags:  FlagSavory,
		Value:  9,
		Sprite: sprites.TablesItems[33],
	},
	SucculentPlatter: {
		Flags:  FlagSavory,
		Value:  16,
		Sprite: sprites.TablesItems[90],
	},
	SucculentCookies: {
		Flags:  FlagSweet | FlagSavory,
		Value:  17, // was 1 in bug fables
		Sprite: sprites.TablesItems[91],
	},
	BerryJuice: {
		Flags:  FlagLiquid | FlagSweet | FlagCold,
		Value:  10,
		Sprite: sprites.TablesItems[38],
	},
	MushroomSkewer: {
		Flags:  FlagSavory,
		Value:  12,
		Sprite: sprites.TablesItems[32],
	},
	HeartyBreakfast: {
		Flags:  FlagSavory,
		Value:  12,
		Sprite: sprites.TablesItems[18],
	},
	MushroomSalad: {
		Flags:  FlagSavory | FlagCrunchy,
		Value:  9,
		Sprite: sprites.TablesItems[14],
	},
	SweetDew: {
		Flags:  FlagSweet | FlagSticky | FlagLiquid | FlagCold,
		Value:  20,
		Sprite: sprites.TablesItems[49],
	},
	BakedYam: {
		Flags:  FlagSavory,
		Value:  9,
		Sprite: sprites.TablesItems[64],
	},
	SpicyFries: {
		Flags:  FlagSpicy,
		Value:  25,
		Sprite: sprites.TablesItems[65],
	},
	BurlyChips: {
		Flags:  FlagBitter,
		Value:  25,
		Sprite: sprites.TablesItems[66],
	},
	DangerDish: {
		Flags:  FlagPoison,
		Value:  15,
		Sprite: sprites.TablesItems[40],
	},
	ShavedIce: {
		Flags:  FlagCrunchy | FlagCold | FlagIce,
		Value:  10,
		Sprite: sprites.TablesItems[46],
	},
	ColdSalad: {
		Flags:  FlagCrunchy | FlagCold | FlagIce,
		Value:  17,
		Sprite: sprites.TablesItems[52],
	},
	IceCream: {
		Flags:  FlagCold | FlagIce,
		Value:  15,
		Sprite: sprites.TablesItems[50],
	},
	HoneyIceCream: {
		Flags:  FlagSweet | FlagCold | FlagIce,
		Value:  18,
		Sprite: sprites.TablesItems[48],
	},
	DryBread: {
		Flags:  FlagSavory,
		Value:  7,
		Sprite: sprites.TablesItems[71],
	},
	NuttyCake: {
		Flags:  FlagSweet | FlagSavory,
		Value:  10,
		Sprite: sprites.TablesItems[72],
	},
	PoisonCake: {
		Flags:  FlagSweet | FlagPoison,
		Value:  12,
		Sprite: sprites.TablesItems[73],
	},
	FrostPie: {
		Flags:  FlagSweet | FlagCold | FlagIce,
		Value:  18,
		Sprite: sprites.TablesItems[81],
	},
	YamBread: {
		Flags:  FlagSavory,
		Value:  15,
		Sprite: sprites.TablesItems[68],
	},
	ShockCandy: {
		Flags:  FlagNumb | FlagSweet,
		Value:  20,
		Sprite: sprites.TablesItems[74],
	},
	MushroomGummies: {
		Flags:  FlagSweet,
		Value:  9,
		Sprite: sprites.TablesItems[94],
	},
	SweetPudding: {
		Flags:  FlagSweet | FlagSticky,
		Value:  15,
		Sprite: sprites.TablesItems[92],
	},
	LeafCroissant: {
		Flags:  FlagSavory | FlagCrunchy,
		Value:  9,
		Sprite: sprites.TablesItems[37],
	},
	HoneyPancakes: {
		Flags:  FlagSticky | FlagSweet,
		Value:  16,
		Sprite: sprites.TablesItems[88],
	},
	DrowsyCake: {
		Flags:  FlagSleep | FlagSweet,
		Value:  10,
		Sprite: sprites.TablesItems[36],
	},
	HustleCandy: {
		Flags:  FlagBitter | FlagSweet | FlagCaffeine,
		Value:  17,
		Sprite: sprites.TablesItems[28],
	},
	SpicyCandy: {
		Flags:  FlagSpicy | FlagSweet,
		Value:  25,
		Sprite: sprites.TablesItems[69],
	},
	BurlyCandy: {
		Flags:  FlagBitter | FlagSweet,
		Value:  25,
		Sprite: sprites.TablesItems[70],
	},
	BerryJam: {
		Flags:  FlagSavory,
		Value:  25,
		Sprite: sprites.TablesItems[62],
	},
	HotDrink: {
		Flags:  FlagBitter | FlagCaffeine | FlagLiquid,
		Value:  15,
		Sprite: sprites.TablesItems[102],
	},
	PlainTea: {
		Flags:  FlagLiquid | FlagSavory,
		Value:  10,
		Sprite: sprites.TablesItems[75],
	},
	SpicyTea: {
		Flags:  FlagLiquid | FlagSpicy,
		Value:  26,
		Sprite: sprites.TablesItems[79],
	},
	BurlyTea: {
		Flags:  FlagLiquid | FlagBitter,
		Value:  26,
		Sprite: sprites.TablesItems[80],
	},
	AphidShake: {
		Flags:  FlagSavory | FlagLiquid,
		Value:  15,
		Sprite: sprites.TablesItems[99],
	},
	SquashPuree: {
		Flags:  FlagSavory | FlagSticky,
		Value:  7,
		Sprite: sprites.TablesItems[97],
	},
	SquashCandy: {
		Flags:  FlagSavory | FlagSweet,
		Value:  17,
		Sprite: sprites.TablesItems[59],
	},
	SquashTart: {
		Flags:  FlagSpicy | FlagSavory,
		Value:  13,
		Sprite: sprites.TablesItems[103],
	},
	PlumplingPie: {
		Flags:  FlagSweet | FlagSavory | FlagBitter,
		Value:  25,
		Sprite: sprites.TablesItems[104],
	},
	TangyJam: {
		Flags:  FlagSavory | FlagSticky | FlagTangy | FlagCold,
		Value:  40,
		Sprite: sprites.TablesItems[77],
	},
	TangyJuice: {
		Flags:  FlagSavory | FlagLiquid | FlagTangy | FlagCold,
		Value:  35,
		Sprite: sprites.TablesItems[78],
	},
	TangyPie: {
		Flags:  FlagSavory | FlagSweet | FlagTangy,
		Value:  26,
		Sprite: sprites.TablesItems[84],
	},
	SpicyBomb: {
		Flags:  FlagSpicy | FlagExplodes,
		Value:  20,
		Sprite: sprites.TablesItems[29],
	},
	BurlyBomb: {
		Flags:  FlagBitter | FlagExplodes,
		Value:  21,
		Sprite: sprites.TablesItems[60],
	},
	PoisonBomb: {
		Flags:  FlagPoison | FlagExplodes,
		Value:  28,
		Sprite: sprites.TablesItems[30],
	},
	SleepBomb: {
		Flags:  FlagSleep | FlagExplodes,
		Value:  30,
		Sprite: sprites.TablesItems[45],
	},
	NumbBomb: {
		Flags:  FlagNumb | FlagExplodes,
		Value:  30,
		Sprite: sprites.TablesItems[44],
	},
	FrostBomb: {
		Flags:  FlagIce | FlagCold | FlagExplodes,
		Value:  30,
		Sprite: sprites.TablesItems[43],
	},
	ClearBomb: {
		Flags:  FlagCleanse | FlagCold | FlagExplodes,
		Value:  20,
		Sprite: sprites.TablesItems[35],
	},
	Abombination: {
		Flags:  FlagSweet | FlagSticky | FlagExplodes,
		Value:  26,
		Sprite: sprites.TablesItems[61],
	},
	CherryBombs: {
		Flags:  FlagHard | FlagExplodes,
		Value:  75,
		Sprite: sprites.TablesItems[96],
	},
	CherryPie: {
		Flags:  FlagSavory | FlagSweet,
		Value:  65,
		Sprite: sprites.TablesItems[55],
	},
	BerrySmoothie: {
		Flags:  FlagLiquid | FlagSweet | FlagCold,
		Value:  35,
		Sprite: sprites.TablesItems[57],
	},
	MiracleShake: {
		Flags:  FlagCleanse | FlagLiquid | FlagSweet | FlagCold | FlagTangy,
		Value:  70,
		Sprite: sprites.TablesItems[56],
	},
	TangyCarpaccio: {
		Flags:        FlagSavory | FlagTangy,
		RequiredChef: ChefKut,
		Value:        26,
		Sprite:       sprites.TablesItems[86],
	},
	CrisbeeDonut: {
		Flags:        FlagSweet | FlagSticky,
		RequiredChef: ChefCrisbee,
		Value:        26,
		Sprite:       sprites.TablesItems[85],
	},
	QueensDinner: {
		Flags:        FlagSavory | FlagSweet | FlagSticky | FlagTangy,
		RequiredChef: ChefFry,
		Value:        40,
		Sprite:       sprites.TablesItems[31],
	},
	MegaRush: {
		Flags:  FlagLiquid | FlagSweet | FlagBitter | FlagCaffeine | FlagCold,
		Value:  15,
		Sprite: sprites.TablesItems[93],
	},
	MiteBurger: {
		Flags:  FlagSavory,
		Value:  20,
		Sprite: sprites.TablesItems[98],
	},
	JaydesStew: {
		Flags:  FlagSavory | FlagLiquid,
		Value:  25, // was 10 in bug fables
		Sprite: sprites.TablesItems[53],
	},
	RandomMysteryBerry1: {
		Flags:  FlagGarbage,
		Sprite: sprites.TablesItems[100],
	},
	RandomMysteryBerry2: {
		Flags:  FlagGarbage,
		Sprite: sprites.TablesItems[100],
	},
}
