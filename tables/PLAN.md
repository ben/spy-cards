# Development Roadmap

- [x] Plan Project
- [ ] MVP
	- UI
		- [ ] Attract Screen and Save/Load
		- [ ] Setup Wizard
		- [ ] Pre-Day Screen
		- [ ] Merchant
		- [ ] Foraging
		- [ ] Staff
		- [ ] Layout Editor
	- Logic
		- [ ] Generate Patrons
		- [ ] Generate Staff
		- [ ] Generate Objectives
		- [ ] Patron Chat/Overheard
	- Maps
		- [x] Bugaria Full
		- Bugaria Commercial
			- [x] Converted
			- [ ] Editable
			- [x] Icon
		- Underground Bar
			- [x] Converted
			- [ ] Editable
			- [x] Icon
		- Defiant Root
			- [x] Built
			- [ ] Editable
			- [ ] Icon
		- Bee Kingdom Cafeteria
			- [ ] Built
			- [ ] Editable
			- [ ] Icon
		- Golden Settlement Fine Dining
			- [ ] Converted
			- [ ] Editable
			- [ ] Icon
		- Wasp Kingdom Food Cart
			- [ ] Built
			- [ ] Editable
			- [ ] Icon
		- DineMite™
			- [ ] Converted
			- [ ] Editable
			- [ ] Icon
		- Metal Island Machine Cafe
			- [ ] Converted
			- [ ] Editable
			- [ ] Icon
		- Apartment 301-B
			- [ ] Built/Converted?
			- [ ] Editable
			- [ ] Icon
		- Termite Colosseum Food Court
			- [ ] Built
			- [ ] Editable
			- [ ] Icon
- [ ] Optimization & Testing on Low-End Hardware

# Core Concepts

## Chefs

TODO

### Fry

- he/him
- +1 food quality if cooking multiple different recipes simultaneously

### Doppel

- he/him
- +1 food quality for berry juice
- summons a free berry juice every X seconds

### Crisbee

- he/him
- +1 food qaulity if recipe is same as previous
- cannot purchase from Crisbee (Merchant)
- bag of flour is free

### Kut

- he/him
- +1 food quality (passive)

### Jayde

- she/her
- +1 food quality for jayde's stew
- cannot purchase from Jayde (Merchant)
- can produce jayde's stew for 7 berries

### Master Slice

- he/him
- +1 food quality for recipes containing Tangy Berry
- 10 berry discount on Tangy Berry and Dark Cherries

### Microwave

- it/its
- +1 food quality for Mistake, Big Mistake, Abomihoney, and Mite Burger
- food is automatically ejected when fully cooked
- can only cook 1 recipe at a time
- 50% chance that recipe resulting in Mistake or Big Mistake will instead create Mite Burger

## Locations

TODO

### Bugaria Commercial District

- outdoor
- medium floor space
- rent: normal
- patrons: no ladybugs
- nearby merchant: Madame Butterfly
- nearby merchant: Cricketly

### Defiant Root

- outdoor
- medium floor space
- rent: normal
- nearby merchant: Cricketly
- nearby merchant: Sandy
- nearby merchant: Crisbee
- nearby merchant: Neil
- nearby merchant: Xic

### Underground Tavern

- medium floor space
- rent: normal
- nearby merchant: Madame Butterfly
- nearby merchant: Cricketly
- nearby merchant: Shady Contact

### Bee Kingdom Cafeteria

- no servers; patrons will order from the counter
- large floor space
- rent: normal
- patrons: only bees
- nearby merchant: Claire

### Golden Settlement Fine Dining

- classy
- medium-small floor space
- rent: high
- nearby merchant: Cricketly
- nearby merchant: Gein
- nearby merchant: Yumnum
- nearby merchant: Jayde

### Wasp Kingdom Food Cart

- no servers; patrons will order from the counter
- medium-large floor space
- rent: low
- nearby merchant: Jayde

### DineMite™

- no servers; patrons will order from the counter
- medium floor space
- rent: normal
- patrons: no ants
- nearby merchant: Tyna

### Metal Island Machine Cafe

- classy
- medium floor space
- rent: high
- nearby merchant: Skirby

### Apartment 301-B

- extremely small floor space
- rent: low
- patrons: no ants
- nearby merchant: Tyna

### Termite Colosseum Food Court

- very large floor space
- rent: normal
- patrons: no ants
- nearby merchant: Cricketly
- nearby merchant: Tyna

## Explorers

TODO

### Team Ant

- Gen & Eri
- cannot forage in Forsaken Lands
- price: low
- +1 Bugaria Outskirts
- -2 Snakemouth Den
- +1 Honey Factory

### Team Celia

- Levi & Celia
- price: low
- -1 Bugaria Outskirts
- +1 Lost Sands
- +2 Golden Hills
- -1 Forsaken Lands

### Team Mothiva

- Mothiva & Zasp
- cannot forage in Honey Factory
- price: high
- -1 Snakemouth Den
- +2 Golden Hills
- -1 Forsaken Lands

### Team Slacker

- Stratos & Delilah
- price: medium

### Team Maki

- Maki & Kina
- price: medium
- +1 Snakemouth Den
- -2 Honey Factory
- -1 Golden Hills
- +1 Forsaken Lands

### Team Snakemouth

- Vi & Kabbu & Leif
- price: very high
- +1 to all foraging locations

## Foraging

TODO

### Bugaria Outskirts

- Common: Crunchy Leaf
- Common: Danger Spud
- Uncommon: Honey Drop
- Uncommon: Spicy Berry
- Uncommon: Burly Berry
- Rare: Aphid Egg
- Rare: Mushroom
- Rare: Hard Seed
- Rare: Clear Water
- Rare: Numbnail Dart
- Rare: Mystery Berry
- Super Rare: Danger Shroom
- Super Rare: Tangy Berry
- Super Rare: Dark Cherries
- Super Rare: Magic Seed

### Snakemouth Den

- Common: Crunchy Leaf
- Common: Honey Drop
- Common: Aphid Egg
- Common: Mushroom
- Uncommon: Spicy Berry
- Uncommon: Burly Berry
- Rare: Hard Seed
- Rare: Clear Water
- Rare: Mystery Berry
- Super Rare: Danger Shroom
- Super Rare: Danger Spud
- Super Rare: Tangy Berry
- Super Rare: Dark Cherries
- Super Rare: Magic Seed

### Honey Factory

- Common: Honey Drop
- Common: Shock Berry
- Uncommon: Crunchy Leaf
- Uncommon: Spicy Berry
- Uncommon: Burly Berry
- Rare: Aphid Egg
- Rare: Mushroom
- Rare: Hard Seed
- Rare: Clear Water
- Rare: Magic Seed
- Rare: Mystery Berry
- Super Rare: Danger Shroom
- Super Rare: Danger Spud
- Super Rare: Tangy Berry
- Super Rare: Dark Cherries

### Lost Sands

- Common: Agaric Shroom
- Common: Succulent Berry
- Uncommon: Crunchy Leaf
- Uncommon: Honey Drop
- Uncommon: Hard Seed
- Uncommon: Spicy Berry
- Uncommon: Burly Berry
- Rare: Aphid Egg
- Rare: Mushroom
- Rare: Clear Water
- Rare: Hustle Berry
- Rare: Mystery Berry
- Super Rare: Danger Shroom
- Super Rare: Danger Spud
- Super Rare: Tangy Berry
- Super Rare: Dark Cherries
- Super Rare: Magic Seed

### Golden Hills

- Common: Hard Seed
- Common: Clear Water
- Common: Numbnail Dart
- Uncommon: Crunchy Leaf
- Uncommon: Honey Drop
- Uncommon: Spicy Berry
- Uncommon: Burly Berry
- Rare: Aphid Egg
- Rare: Mushroom
- Rare: Aphid Dew
- Rare: Poison Dart
- Rare: Mystery Berry
- Super Rare: Danger Shroom
- Super Rare: Danger Spud
- Super Rare: Tangy Berry
- Super Rare: Dark Cherries
- Super Rare: Magic Seed

### Forsaken Lands

- Common: Squash
- Uncommon: Crunchy Leaf
- Uncommon: Honey Drop
- Uncommon: Spicy Berry
- Uncommon: Burly Berry
- Rare: Aphid Egg
- Rare: Mushroom
- Rare: Hard Seed
- Rare: Clear Water
- Rare: Mystery Berry
- Super Rare: Danger Shroom
- Super Rare: Danger Spud
- Super Rare: Tangy Berry
- Super Rare: Dark Cherries
- Super Rare: Magic Seed

## Merchants

TODO

### Madame Butterfly

- she/her
- Crunchy Leaf: 3 berries
- Honey Drop: 4 berries
- Aphid Egg: 2 berries
- Mushroom: 3 berries
- Danger Shroom: 3 berries

### Cricketly

- she/her
- Spicy Berry: 12 berries
- Burly Berry: 12 berries
- Magic Seed: 18 berries
- Succulent Berry: 10 berries

### Sandy

- she/her
- Dry Bread: 7 berries
- Crunchy Leaf: 3 berries
- Clear Water: 3 berries
- Agaric Shroom: 7 berries
- Hustle Berry: 18 berries

### Claire

- she/her
- Honey'd Leaf: 10 berries
- Leaf Omelet: 10 berries
- Glazed Honey: 9 berries
- Honey Ice Cream: 20 berries
- Shock Berry: 11 berries

### Crisbee

- he/him
- Honey Drop: 4 berries
- Bag of Flour: 4 berries
- Nutty Cake: 10 berries

### Neil

- he/him
- Magic Ice: 10 berries

### Xic

- he/him
- Danger Spud: 3 berries
- Danger Shroom: 3 berries
- Poison Bomb: 28 berries
- Poison Dart: 8 berries

### Gein

- he/him
- Clear Water: 3 berries
- Hard Seed: 4 berries
- Honey Drop: 4 berries
- Aphid Dew: 9 berries
- Aphid Egg: 3 berries

### Yumnum

- he/him
- Numbnail Dart: 8 berries
- Sleep Bomb: 30 berries
- Drowsy Cake: 10 berries

### Skirby

- he/him
- Tangy Berry: 40 berries

### Shady Contact

- he/him
- Dark Cherries: 50 berries

### Jayde

- she/her
- Jayde's Stew: 25 berries
- Magic Seed: 18 berries
- Mushroom Salad: 7 berries

### Tyna

- she/her
- Squash: 9 berries
- Mega Rush: 15 berries
- Aphid Shake: 15 berries
- Spicy Fries: 12 berries
- Mite Burger: 15 berries

### Rina

- she/her
- Magic Seed: 17 berries
- Mystery Berry: 23 berries

## Patrons

- Unique patrons and randomly generated patrons
- Patrons prefer specific foods and furniture types

## Servers

TODO

## Cleaners

TODO

## Furniture

TODO

## Recipes

TODOne

## Berries

- Start with X berries.
- Use berries to pay for things and staff.
- Patrons pay berries for menu items.
- Transfer berries to "restaurant improvement fund" to gain exp.

## Days

TODO

## Objectives

TODO

## Rank

TODO

# Gameplay

## Setup

- Player is given an objective.
- Select a Chef and Location.
- Player is given a starting amount of Berries.
- Select foods to be on menu.

## Pre-Day

- Edit restaurant furniture layout.
- Hire servers/cleaners.
- Send explorers to foraging locations.
- Order from merchants.

## Cooking

- Minigames:
	- Stirring
	- Chopping
	- Time-based
	- Frying (shake it)

## Restaurant View

- Patrons enter restaurant, look at menu, select an item to order or leave.
- Patrons pay for the meal they ordered regardless of what they receive as long as they receive something.
- Can see what patrons are saying in chat log.
- If patrons like their time at the restaurant, they tell their friends, increasing patronage.
