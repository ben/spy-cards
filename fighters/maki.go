package fighters

import (
	"git.lubar.me/ben/spy-cards/audio"
	"git.lubar.me/ben/spy-cards/gfx/sprites"
	"git.lubar.me/ben/spy-cards/input"
	"git.lubar.me/ben/spy-cards/rollback"
)

const (
	makiIdle0 = iota
	makiIdle1
	makiIdle2
	makiBlock
	makiDashBack
	makiDashFore
	makiWalk0
	makiWalk1
	makiBasicAtkIdle0
	makiBasicAtkIdle1
	makiBasicAtk0
	makiBasicAtk1
	makiBasicAtk2
	makiDashLeft
	makiDashRight
)

var maki = Character{
	RoomID:  "maki",
	Sprites: sprites.ELFMaki[:],
	States: []CharacterState{
		makiIdle0: {
			Sprite: 18,
			Update: func(_ *State, ps *PlayerState, inputs rollback.InputFrame) {
				switch {
				case inputs.Held(input.BtnCancel):
					ps.SetState(makiBlock)
				case inputs.Held(input.BtnConfirm):
					ps.SetState(makiBasicAtkIdle0)
				case inputs.Held(input.BtnLeft) != inputs.Held(input.BtnRight):
					ps.SetState(makiWalk0)
				case ps.Count >= 15:
					ps.SetState(makiIdle1)
				}
			},
			Hitboxes: []HitboxDef{
				{
					X0: -0.7,
					X1: 0.6,
					Y0: 0.1,
					Y1: 1,
				},
				{
					X0: -0.5,
					X1: 0.35,
					Y0: 1,
					Y1: 1.35,
				},
				{
					X0: -0.35,
					X1: 0.15,
					Y0: 1.35,
					Y1: 2.5,
				},
				{
					X0: -0.8,
					X1: -0.2,
					Y0: 1.9,
					Y1: 2.1,
				},
			},
		},
		makiIdle1: {
			Sprite: 19,
			Update: func(_ *State, ps *PlayerState, inputs rollback.InputFrame) {
				switch {
				case inputs.Held(input.BtnCancel):
					ps.SetState(makiBlock)
				case inputs.Held(input.BtnConfirm):
					ps.SetState(makiBasicAtkIdle0)
				case inputs.Held(input.BtnLeft) != inputs.Held(input.BtnRight):
					ps.SetState(makiWalk0)
				case ps.Count >= 15:
					ps.SetState(makiIdle2)
				}
			},
			Hitboxes: []HitboxDef{
				{
					X0: -0.7,
					X1: 0.6,
					Y0: 0.1,
					Y1: 1,
				},
				{
					X0: -0.5,
					X1: 0.35,
					Y0: 1,
					Y1: 1.35,
				},
				{
					X0: -0.35,
					X1: 0.15,
					Y0: 1.35,
					Y1: 2.5,
				},
				{
					X0: -0.8,
					X1: -0.2,
					Y0: 1.9,
					Y1: 2.1,
				},
			},
		},
		makiIdle2: {
			Sprite: 20,
			Update: func(_ *State, ps *PlayerState, inputs rollback.InputFrame) {
				switch {
				case inputs.Held(input.BtnCancel):
					ps.SetState(makiBlock)
				case inputs.Held(input.BtnConfirm):
					ps.SetState(makiBasicAtkIdle0)
				case inputs.Held(input.BtnLeft) != inputs.Held(input.BtnRight):
					ps.SetState(makiWalk0)
				case ps.Count >= 15:
					ps.SetState(makiIdle0)
				}
			},
			Hitboxes: []HitboxDef{
				{
					X0: -0.7,
					X1: 0.6,
					Y0: 0.1,
					Y1: 1,
				},
				{
					X0: -0.5,
					X1: 0.35,
					Y0: 1,
					Y1: 1.35,
				},
				{
					X0: -0.375,
					X1: 0.15,
					Y0: 1.35,
					Y1: 2.5,
				},
				{
					X0: -0.8,
					X1: -0.2,
					Y0: 1.9,
					Y1: 2.1,
				},
			},
		},
		makiBlock: {
			Sprite: 27,
			Update: func(s *State, ps *PlayerState, inputs rollback.InputFrame) {
				ops := &s.Player[0]
				if ops == ps {
					ops = &s.Player[1]
				}

				ps.Flip = ps.Pos[0] < ops.Pos[0]

				switch {
				case !inputs.Held(input.BtnCancel):
					ps.SetState(makiIdle0)
				case inputs.Held(input.BtnLeft):
					ps.SetState(makiDashLeft)
				case inputs.Held(input.BtnRight):
					ps.SetState(makiDashRight)
				}
			},
			Hitboxes: []HitboxDef{
				{
					X0: -0.5,
					X1: 0.5,
					Y0: 0,
					Y1: 1.5,
				},
				{
					X0: -0.4,
					X1: 0.3,
					Y0: 1.5,
					Y1: 2.6,
				},
				{
					X0:    -0.7,
					X1:    -0.3,
					Y0:    0.6,
					Y1:    1.5,
					Block: true,
				},
				{
					X0:    -0.6,
					X1:    -0.1,
					Y0:    1.5,
					Y1:    2.5,
					Block: true,
				},
			},
		},
		makiDashBack: {
			Sprite: 40,
			Sound:  audio.PageFlip,
			Update: func(s *State, ps *PlayerState, _ rollback.InputFrame) {
				ops := &s.Player[0]
				if ops == ps {
					ops = &s.Player[1]
				}

				if ps.Flip != (ps.Pos[0] < ops.Pos[0]) {
					ps.Flip = !ps.Flip
					ps.State = 5 // don't run change callbacks
				}

				if ps.Count >= 15 {
					ps.SetState(makiBlock)
				}
			},
			Hitboxes: []HitboxDef{
				{
					X0: -0.5,
					X1: 0.5,
					Y0: 0,
					Y1: 1.5,
				},
				{
					X0: -0.4,
					X1: 0.3,
					Y0: 1.5,
					Y1: 2.6,
				},
			},
		},
		makiDashFore: {
			Sprite: 41,
			Sound:  audio.PageFlip,
			Update: func(s *State, ps *PlayerState, _ rollback.InputFrame) {
				ops := &s.Player[0]
				if ops == ps {
					ops = &s.Player[1]
				}

				if ps.Flip != (ps.Pos[0] < ops.Pos[0]) {
					ps.Flip = !ps.Flip
					ps.State = 4 // don't run change callbacks
				}

				if ps.Count >= 15 {
					ps.SetState(makiBlock)
				}
			},
			Hitboxes: []HitboxDef{
				{
					X0: -0.5,
					X1: 0.5,
					Y0: 0,
					Y1: 1.5,
				},
				{
					X0: -0.4,
					X1: 0.3,
					Y0: 1.5,
					Y1: 2.6,
				},
			},
		},
		makiWalk0: {
			Sprite: 3,
			Update: func(_ *State, ps *PlayerState, inputs rollback.InputFrame) {
				switch {
				case inputs.Held(input.BtnLeft) == inputs.Held(input.BtnRight):
					ps.SetState(makiIdle0)
				case inputs.Held(input.BtnCancel):
					ps.SetState(makiBlock)
					ps.Count = -15
				case inputs.Held(input.BtnConfirm):
					ps.SetState(makiBasicAtkIdle0)
				case ps.Count >= 15:
					ps.SetState(makiWalk1)
				case inputs.Held(input.BtnLeft):
					ps.Flip = false
					ps.Vel[0] -= 1.0 / 48.0
				case inputs.Held(input.BtnRight):
					ps.Flip = true
					ps.Vel[0] += 1.0 / 48.0
				}
			},
			Hitboxes: []HitboxDef{
				{
					X0: -0.5,
					X1: 0.5,
					Y0: 0,
					Y1: 1.5,
				},
				{
					X0: -0.4,
					X1: 0.3,
					Y0: 1.5,
					Y1: 2.6,
				},
			},
		},
		makiWalk1: {
			Sprite: 4,
			Update: func(_ *State, ps *PlayerState, inputs rollback.InputFrame) {
				switch {
				case inputs.Held(input.BtnLeft) == inputs.Held(input.BtnRight):
					ps.SetState(makiIdle0)
				case inputs.Held(input.BtnCancel):
					ps.SetState(makiBlock)
					ps.Count = -15
				case inputs.Held(input.BtnConfirm):
					ps.SetState(makiBasicAtkIdle0)
				case ps.Count >= 15:
					ps.SetState(makiWalk0)
				case inputs.Held(input.BtnLeft):
					ps.Flip = false
					ps.Vel[0] -= 1.0 / 48.0
				case inputs.Held(input.BtnRight):
					ps.Flip = true
					ps.Vel[0] += 1.0 / 48.0
				}
			},
			Hitboxes: []HitboxDef{
				{
					X0: -0.5,
					X1: 0.5,
					Y0: 0,
					Y1: 1.5,
				},
				{
					X0: -0.4,
					X1: 0.3,
					Y0: 1.5,
					Y1: 2.6,
				},
			},
		},
		makiBasicAtkIdle0: {
			Sprite: 23,
			Update: func(_ *State, ps *PlayerState, inputs rollback.InputFrame) {
				switch {
				case !inputs.Held(input.BtnConfirm):
					ps.SetState(makiBasicAtk0)
				case ps.Count >= 5:
					ps.SetState(makiBasicAtkIdle1)
				}
			},
			Hitboxes: []HitboxDef{
				{
					X0: -0.5,
					X1: 0.5,
					Y0: 0,
					Y1: 1.5,
				},
				{
					X0: -0.4,
					X1: 0.3,
					Y0: 1.5,
					Y1: 2.6,
				},
			},
		},
		makiBasicAtkIdle1: {
			Sprite: 24,
			Update: func(_ *State, ps *PlayerState, inputs rollback.InputFrame) {
				switch {
				case !inputs.Held(input.BtnConfirm):
					ps.SetState(makiBasicAtk0)
				case ps.Count >= 5:
					ps.SetState(makiBasicAtkIdle0)
				}
			},
			Hitboxes: []HitboxDef{
				{
					X0: -0.5,
					X1: 0.5,
					Y0: 0,
					Y1: 1.5,
				},
				{
					X0: -0.4,
					X1: 0.3,
					Y0: 1.5,
					Y1: 2.6,
				},
			},
		},
		makiBasicAtk0: {
			Sprite: 24,
			Update: func(_ *State, ps *PlayerState, _ rollback.InputFrame) {
				switch {
				case ps.Count >= 5:
					ps.SetState(makiBasicAtk1)
				}
			},
			Hitboxes: []HitboxDef{
				{
					X0: -0.5,
					X1: 0.5,
					Y0: 0,
					Y1: 1.5,
				},
				{
					X0: -0.4,
					X1: 0.3,
					Y0: 1.5,
					Y1: 2.6,
				},
			},
		},
		makiBasicAtk1: {
			Sprite: 25,
			Update: func(_ *State, ps *PlayerState, _ rollback.InputFrame) {
				switch {
				case ps.Count >= 3:
					ps.SetState(makiBasicAtk2)
				}
			},
			Hitboxes: []HitboxDef{
				{
					X0: -0.5,
					X1: 1.5,
					Y0: 0,
					Y1: 1.5,
				},
				{
					X0: -0.4,
					X1: 0.5,
					Y0: 1.5,
					Y1: 2.6,
				},
				{
					X0: -2.5,
					X1: -1,
					Y0: 0.8,
					Y1: 2.5,

					Hurt: 5,
				},
				{
					X0: -2,
					X1: -0.7,
					Y0: 2.5,
					Y1: 3,

					Hurt: 5,
				},
				{
					X0: -1.5,
					X1: 0.5,
					Y0: 3,
					Y1: 3.5,

					Hurt: 5,
				},
			},
		},
		makiBasicAtk2: {
			Sprite: 26,
			Sound:  audio.Damage0,
			Update: func(_ *State, ps *PlayerState, _ rollback.InputFrame) {
				switch {
				case ps.Count >= 10:
					ps.SetState(makiIdle0)
				}
			},
			Hitboxes: []HitboxDef{
				{
					X0: -0.5,
					X1: 0.5,
					Y0: 0,
					Y1: 1.5,
				},
				{
					X0: -0.4,
					X1: 0.3,
					Y0: 1.5,
					Y1: 2.6,
				},
			},
		},
		makiDashLeft: {
			Sprite: 27,
			Update: func(_ *State, ps *PlayerState, _ rollback.InputFrame) {
				switch {
				case ps.Count >= 5:
					ps.Vel[0] = -1
					if ps.Flip {
						ps.SetState(makiDashBack)
					} else {
						ps.SetState(makiDashFore)
					}
				}
			},
			Hitboxes: []HitboxDef{
				{
					X0: -0.5,
					X1: 0.5,
					Y0: 0,
					Y1: 1.5,
				},
				{
					X0: -0.4,
					X1: 0.3,
					Y0: 1.5,
					Y1: 2.6,
				},
			},
		},
		makiDashRight: {
			Sprite: 27,
			Update: func(_ *State, ps *PlayerState, _ rollback.InputFrame) {
				switch {
				case ps.Count >= 5:
					ps.Vel[0] = 1
					if ps.Flip {
						ps.SetState(makiDashFore)
					} else {
						ps.SetState(makiDashBack)
					}
				}
			},
			Hitboxes: []HitboxDef{
				{
					X0: -0.5,
					X1: 0.5,
					Y0: 0,
					Y1: 1.5,
				},
				{
					X0: -0.4,
					X1: 0.3,
					Y0: 1.5,
					Y1: 2.6,
				},
			},
		},
	},
	MaxHP:    100,
	MaxMeter: 100,
}
