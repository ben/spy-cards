// Package fighters is a prototype of The Everlasting Fighters.
package fighters

import (
	"context"
	"fmt"
	"math"
	"strconv"
	"time"

	"git.lubar.me/ben/spy-cards/audio"
	"git.lubar.me/ben/spy-cards/gfx"
	"git.lubar.me/ben/spy-cards/gfx/sprites"
	"git.lubar.me/ben/spy-cards/input"
	"git.lubar.me/ben/spy-cards/internal/router"
	"git.lubar.me/ben/spy-cards/rollback"
	"git.lubar.me/ben/spy-cards/room"
	"git.lubar.me/ben/spy-cards/scnet"
	"git.lubar.me/ben/spy-cards/screenreader"
)

type Game struct {
	ictx   *input.Context
	conn   *rollback.Conn
	rb     rollback.Context
	cam    gfx.Camera
	tcam   gfx.Camera
	sky    *sprites.Batch
	sprite *sprites.Batch
	hud    *sprites.Batch
	stage  *room.Stage
	camPos [3]float32
}

func New(ctx context.Context) (*Game, error) {
	g := &Game{}

	var err error

	g.cam.SetDefaults()
	g.cam.Rotation.RotationX(5 * math.Pi / 180)

	g.stage = room.NewStage(ctx, "img/room/bugaria-main.stage", nil)
	g.stage.Paper = false
	g.stage.Before = func(*room.CustomStageProperties, uint64, int, *gfx.Camera) {
		g.sky.SetCamera(&g.cam)
		g.sky.Render()
	}
	g.stage.UpdateCamera = func(_ *room.CustomStageProperties, _ uint64, _ int, cam *gfx.Camera) bool {
		*cam = g.cam

		cam.Position.Translation(5, 0.5, 3.5)
		cam.MarkDirty()

		return false
	}

	g.sky = sprites.NewBatch(&g.cam)
	g.sky.Append(sprites.Skybox1, 0, -25, 100, 50, 50, sprites.White)

	g.ictx = input.GetContext(ctx)

	g.conn, err = rollback.NewConn(ctx, scnet.UnknownPerspective, "-elf")
	if err != nil {
		return nil, fmt.Errorf("fighters: net error: %w", err)
	}

	known := &State{}

	known.Player[0].Pos[0] = -7
	known.Player[0].Char = &maki
	known.Player[1].Pos[0] = 7
	known.Player[1].Char = &tanjerin
	known.Player[0].Flip = true
	known.Player[0].HP = known.Player[0].Char.MaxHP
	known.Player[1].HP = known.Player[1].Char.MaxHP

	g.rb.Known = known
	g.rb.Predicted = known.Clone()

	if router.FlagShowHitbox.IsSet() {
		hitboxEditInit()
	}

	return g, nil
}

func (g *Game) Resize(ctx context.Context) error {
	w, h := gfx.Size()

	// use horizontal FOV rather than vertical to ensure both players are the
	// same distance from the screen edge
	g.cam.Perspective.Perspective(gfx.HorizontalFOV(90), float32(w)/float32(h), 0.3, 150)

	g.tcam.SetDefaults()
	g.tcam.Offset.Identity()
	g.tcam.Rotation.Identity()
	g.tcam.Position.Identity()
	g.tcam.Perspective.Scale(-64/float32(w), -64/float32(h), -0.01)

	return nil
}

func (g *Game) Update(ctx context.Context, frameAdvance bool) (*router.PageInfo, error) {
	if !frameAdvance {
		return nil, nil
	}

	if router.FlagShowHitbox.IsSet() {
		hitboxEditUpdate()
	}

	predicted := g.rb.Predicted.(*State)

	g.camPos[0] = g.camPos[0]*0.9 + (predicted.Player[0].Pos[0]+predicted.Player[1].Pos[0])/2*0.1
	g.camPos[1] = g.camPos[1]*0.9 + (predicted.Player[0].Pos[1]+predicted.Player[1].Pos[1])/2*0.1
	dist := predicted.Player[0].Pos[0] - predicted.Player[1].Pos[0]
	g.camPos[2] = g.camPos[2]*0.9 + -dist*dist/100*0.1

	sx := 3 - float32(g.conn.Game.Perspective)*2
	g.cam.Position.Translation(sx*g.camPos[0], g.camPos[1], g.camPos[2])
	g.cam.MarkDirty()
	g.stage.Update()

	audio.SetListenerPosition(sx*g.camPos[0], g.camPos[1], g.camPos[2], 5*math.Pi/180, 0, 0)

	for g.conn.UpdateState() {
		switch g.conn.State {
		case rollback.StateMatchReady:
			audio.Battle4.PlayMusic(0, false)
		case rollback.StateConnectionFailed:
			audio.StopMusic()
		}
	}

	if g.conn.State == rollback.StateMatchReady {
		if err := g.rb.Sync(g.conn.Game, g.ictx); err != nil {
			return nil, fmt.Errorf("fighters: net error: %w", err)
		}
	}

	return nil, nil
}

func (g *Game) Render(ctx context.Context) error {
	g.stage.Render()

	if g.hud == nil {
		g.hud = sprites.NewBatch(&g.tcam)
	} else {
		g.hud.Reset(&g.tcam)
	}

	if g.sprite == nil {
		g.sprite = sprites.NewBatch(&g.cam)
	} else {
		g.sprite.Reset(&g.cam)
	}

	switch g.conn.State {
	case rollback.StateMatchmakingWait:
		sprites.DrawTextCentered(g.hud, sprites.FontD3Streetism, "Finding Opponent...", 0, 0, 0, 3, 3, sprites.White, false)
	case rollback.StateMatchmakingConnecting:
		cs, _ := g.conn.Game.State()
		sprites.DrawTextCentered(g.hud, sprites.FontD3Streetism, cs.String(), 0, 0, 0, 3, 3, sprites.White, false)
	case rollback.StateMatchReady:
		w, h := gfx.Size()
		state := g.rb.Predicted.(*State)

		g.hud.Append(sprites.Meter, -float32(w)/64+4, -float32(h)/64+1.5, 0, 1.5, -1.5, sprites.White)
		g.hud.Append(sprites.Meter, float32(w)/64-4, -float32(h)/64+1.5, 0, -1.5, -1.5, sprites.White)

		health := float32(state.Player[g.conn.Game.Perspective-1].HP) / float32(state.Player[g.conn.Game.Perspective-1].Char.MaxHP)
		healthFlash := float32(state.Player[g.conn.Game.Perspective-1].HPChanged) / float32(state.Player[g.conn.Game.Perspective-1].Char.MaxHP)

		g.hud.Append(sprites.Blank, -float32(w)/64+4+health*3.75, -float32(h)/64+1.5, 0, health*7.5, 0.6, sprites.Green)
		g.hud.Append(sprites.Blank, -float32(w)/64+4+(health*2+healthFlash)*3.75, -float32(h)/64+1.5, 0, healthFlash*7.5, 0.6, sprites.White)

		health = float32(state.Player[2-g.conn.Game.Perspective].HP) / float32(state.Player[2-g.conn.Game.Perspective].Char.MaxHP)
		healthFlash = float32(state.Player[2-g.conn.Game.Perspective].HPChanged) / float32(state.Player[2-g.conn.Game.Perspective].Char.MaxHP)

		g.hud.Append(sprites.Blank, float32(w)/64-4-health*3.75, -float32(h)/64+1.5, 0, health*7.5, 0.6, sprites.Green)
		g.hud.Append(sprites.Blank, float32(w)/64-4-(health*2+healthFlash)*3.75, -float32(h)/64+1.5, 0, healthFlash*7.5, 0.6, sprites.White)

		g.hud.Append(sprites.WoodenOrb.Crop(0.45, 0, 0.45, 0), 0.93, -float32(h)/64+2.5, 0, -21.5, 1.5, sprites.White)
		g.hud.Append(sprites.WoodenOrb.Crop(0, 0, 0.55, 0), 3, -float32(h)/64+2.5, 0, -1.5, 1.5, sprites.White)
		g.hud.Append(sprites.WoodenOrb.Crop(0.55, 0, 0, 0), -3, -float32(h)/64+2.5, 0, -1.5, 1.5, sprites.White)

		g.hud.Append(sprites.Blank, 0, -float32(h)/64+2.5, 0, 8.2, 2.7, sprites.Black)
		g.hud.Append(sprites.Meter.Crop(0.06, 0.25, 0.21, 0.54), -4, -float32(h)/64+2.5, 0, 1.6, 6, sprites.White)

		meter := float32(state.Player[g.conn.Game.Perspective-1].Meter) / float32(state.Player[g.conn.Game.Perspective-1].Char.MaxMeter)

		absMeter := meter
		if meter < 0 {
			absMeter = -meter
		}

		meterFlash := sprites.Green

		if state.Player[g.conn.Game.Perspective-1].Meter < 0 {
			if state.Player[g.conn.Game.Perspective-1].Meter&16 != 0 {
				meterFlash = sprites.Color{R: 23, G: 23, B: 255, A: 255}
			} else {
				meterFlash = sprites.White
			}
		}

		g.hud.Append(sprites.Blank, (1-absMeter)*-4, -float32(h)/64+2.5, 0, absMeter*8, 2.5, meterFlash)

		g.hud.Append(room.CharacterByName[state.Player[g.conn.Game.Perspective-1].Char.RoomID].Portrait(), -float32(w)/64+1.5, -float32(h)/64+1.5, 0, -3, 3, sprites.White)
		g.hud.Append(room.CharacterByName[state.Player[2-g.conn.Game.Perspective].Char.RoomID].Portrait(), float32(w)/64-1.5, -float32(h)/64+1.5, 0, 3, 3, sprites.White)

		sprites.DrawText(g.hud, sprites.FontD3Streetism, "Ping: "+g.conn.Game.LastPing().Truncate(time.Millisecond).String(), -float32(w)/64+0.125, -float32(h)/64+0.125, 0, 0.5, 0.5, sprites.White)
		sprites.DrawText(g.hud, sprites.FontD3Streetism, "Frame: "+strconv.FormatUint(g.rb.Predicted.Frame(), 10), -float32(w)/64+0.125, -float32(h)/64+0.475, 0, 0.5, 0.5, sprites.White)

		ahead, behind := g.rb.Desync()
		sprites.DrawText(g.hud, sprites.FontD3Streetism, "A: "+strconv.FormatInt(ahead, 10), -float32(w)/64+0.125, -float32(h)/64+0.825, 0, 0.5, 0.5, sprites.White)
		sprites.DrawText(g.hud, sprites.FontD3Streetism, "B: "+strconv.FormatInt(behind, 10), -float32(w)/64+1.625, -float32(h)/64+0.825, 0, 0.5, 0.5, sprites.White)

		sx := 3 - float32(g.conn.Game.Perspective)*2
		state.Render(g.sprite, sx)

		if g.rb.Dropped {
			sprites.DrawText(g.hud, sprites.FontD3Streetism, "Dropping Inputs!", -float32(w)/64+0.125, -float32(h)/64+1.175, 0, 0.5, 0.5, sprites.Red)
		}
	case rollback.StateConnectionFailed:
		cs, _ := g.conn.Game.State()
		sprites.DrawTextCentered(g.hud, sprites.FontD3Streetism, cs.String(), 0, 0, 0, 3, 3, sprites.White, false)
	}

	g.sprite.Render()
	g.hud.Render()

	return nil
}

func (g *Game) MakeScreenReaderUI() *screenreader.UI {
	// Probably can't make Fighters accessible in this way.
	return &screenreader.UI{}
}
