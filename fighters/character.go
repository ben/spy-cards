package fighters

import (
	"git.lubar.me/ben/spy-cards/audio"
	"git.lubar.me/ben/spy-cards/gfx/sprites"
	"git.lubar.me/ben/spy-cards/internal/router"
	"git.lubar.me/ben/spy-cards/rollback"
)

type Character struct {
	RoomID   string
	Sprites  []*sprites.Sprite
	States   []CharacterState
	MaxHP    uint8
	MaxMeter int8
}

type CharacterState struct {
	Sprite   int
	Sound    *audio.Sound
	Update   func(s *State, ps *PlayerState, inputs rollback.InputFrame)
	Hitboxes []HitboxDef
}

type HitboxDef struct {
	X0, X1 float32
	Y0, Y1 float32
	Block  bool
	Push   bool
	Hurt   uint8
}

func (c *Character) Update(s *State, ps *PlayerState, inputs rollback.InputFrame) {
	ps.Count++
	c.States[ps.State].Update(s, ps, inputs)
}

func (c *Character) Render(ps *PlayerState, b *sprites.Batch, sx float32) {
	ssx := sx
	if ps.Flip {
		ssx = -ssx
	}

	state := &c.States[ps.State]
	sprite := c.Sprites[state.Sprite]
	b.Append(sprite, sx*ps.Pos[0], ps.Pos[1], 0, ssx, 1, sprites.White)

	if router.FlagShowHitbox.IsSet() {
		for _, h := range state.Hitboxes {
			var tint sprites.Color

			switch {
			case h.Hurt != 0:
				tint = sprites.Color{R: 191, G: 63, B: 63, A: 191}
			case h.Block:
				tint = sprites.Color{R: 63, G: 191, B: 63, A: 191}
			case h.Push:
				tint = sprites.Color{R: 191, G: 191, B: 31, A: 191}
			default:
				tint = sprites.Color{R: 31, G: 63, B: 63, A: 63}
			}

			b.Append(sprites.Blank, sx*ps.Pos[0]+ssx*(h.X0+h.X1)/2, ps.Pos[1]+(h.Y0+h.Y1)/2, 0, h.X1-h.X0, h.Y1-h.Y0, tint)
		}
	}
}
