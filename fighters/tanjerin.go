package fighters

import (
	"git.lubar.me/ben/spy-cards/gfx/sprites"
	"git.lubar.me/ben/spy-cards/input"
	"git.lubar.me/ben/spy-cards/rollback"
)

const (
	tanjerinIdle0 = iota
	tanjerinIdle1
	tanjerinBlock
	tanjerinWalk0
	tanjerinWalk1
	tanjerinWalk2
	tanjerinWalk3
	tanjerinJumpIdle0
	tanjerinJump0
	tanjerinJump1
)

var tanjerin = Character{
	RoomID:  "tanjerin",
	Sprites: sprites.ELFTanjerin[:],
	States: []CharacterState{
		tanjerinIdle0: {
			Sprite: 0,
			Update: func(_ *State, ps *PlayerState, inputs rollback.InputFrame) {
				switch {
				case inputs.Held(input.BtnCancel):
					ps.SetState(tanjerinBlock)
				case inputs.Held(input.BtnUp):
					ps.SetState(tanjerinJumpIdle0)
				case inputs.Held(input.BtnLeft) != inputs.Held(input.BtnRight):
					ps.SetState(3)
				case ps.Count >= 15:
					ps.SetState(tanjerinIdle1)
				}
			},
			Hitboxes: []HitboxDef{
				{
					X0: -0.4,
					X1: 0.5,
					Y0: 0.1,
					Y1: 1.3,

					Push: true,
				},
				{
					X0: -0.6,
					X1: 0.6,
					Y0: 0.3,
					Y1: 1.05,

					Push: true,
				},
			},
		},
		tanjerinIdle1: {
			Sprite: 1,
			Update: func(_ *State, ps *PlayerState, inputs rollback.InputFrame) {
				switch {
				case inputs.Held(input.BtnCancel):
					ps.SetState(tanjerinBlock)
				case inputs.Held(input.BtnUp):
					ps.SetState(tanjerinJumpIdle0)
				case inputs.Held(input.BtnLeft) != inputs.Held(input.BtnRight):
					ps.SetState(3)
				case ps.Count >= 15:
					ps.SetState(tanjerinIdle0)
				}
			},
			Hitboxes: []HitboxDef{
				{
					X0: -0.4,
					X1: 0.5,
					Y0: 0.075,
					Y1: 1.275,

					Push: true,
				},
				{
					X0: -0.6,
					X1: 0.6,
					Y0: 0.275,
					Y1: 1.025,

					Push: true,
				},
			},
		},
		tanjerinBlock: {
			Sprite: 6,
			Update: func(_ *State, ps *PlayerState, inputs rollback.InputFrame) {
				if !inputs.Held(input.BtnCancel) {
					ps.SetState(tanjerinIdle0)
				}
			},
			Hitboxes: []HitboxDef{
				{
					X0: -0.4,
					X1: 0.5,
					Y0: 0.1,
					Y1: 1.3,

					Block: true,
					Push:  true,
				},
				{
					X0: -0.6,
					X1: 0.6,
					Y0: 0.3,
					Y1: 1.05,

					Block: true,
					Push:  true,
				},
			},
		},
		tanjerinWalk0: {
			Sprite: 7,
			Update: func(_ *State, ps *PlayerState, inputs rollback.InputFrame) {
				switch {
				case inputs.Held(input.BtnLeft) == inputs.Held(input.BtnRight):
					ps.SetState(tanjerinIdle0)
				case inputs.Held(input.BtnCancel):
					ps.SetState(tanjerinBlock)
				case inputs.Held(input.BtnUp):
					ps.SetState(tanjerinJumpIdle0)
				case ps.Count >= 15:
					ps.SetState(tanjerinWalk1)
				case inputs.Held(input.BtnLeft):
					ps.Flip = false
					ps.Vel[0] -= 1.0 / 64.0
				case inputs.Held(input.BtnRight):
					ps.Flip = true
					ps.Vel[0] += 1.0 / 64.0
				}
			},
			Hitboxes: []HitboxDef{
				{
					X0: -0.4,
					X1: 0.5,
					Y0: 0.1,
					Y1: 1.3,

					Push: true,
				},
				{
					X0: -0.6,
					X1: 0.6,
					Y0: 0.3,
					Y1: 1.05,

					Push: true,
				},
			},
		},
		tanjerinWalk1: {
			Sprite: 8,
			Update: func(_ *State, ps *PlayerState, inputs rollback.InputFrame) {
				switch {
				case inputs.Held(input.BtnLeft) == inputs.Held(input.BtnRight):
					ps.SetState(tanjerinIdle0)
				case inputs.Held(input.BtnCancel):
					ps.SetState(tanjerinBlock)
				case inputs.Held(input.BtnUp):
					ps.SetState(tanjerinJumpIdle0)
				case ps.Count >= 15:
					ps.SetState(tanjerinWalk2)
				case inputs.Held(input.BtnLeft):
					ps.Flip = false
					ps.Vel[0] -= 1.0 / 64.0
				case inputs.Held(input.BtnRight):
					ps.Flip = true
					ps.Vel[0] += 1.0 / 64.0
				}
			},
			Hitboxes: []HitboxDef{
				{
					X0: -0.4,
					X1: 0.5,
					Y0: 0.1,
					Y1: 1.3,

					Push: true,
				},
				{
					X0: -0.6,
					X1: 0.6,
					Y0: 0.3,
					Y1: 1.05,

					Push: true,
				},
			},
		},
		tanjerinWalk2: {
			Sprite: 7,
			Update: func(_ *State, ps *PlayerState, inputs rollback.InputFrame) {
				switch {
				case inputs.Held(input.BtnLeft) == inputs.Held(input.BtnRight):
					ps.SetState(tanjerinIdle0)
				case inputs.Held(input.BtnCancel):
					ps.SetState(tanjerinBlock)
				case inputs.Held(input.BtnUp):
					ps.SetState(tanjerinJumpIdle0)
				case ps.Count >= 15:
					ps.SetState(tanjerinWalk3)
				case inputs.Held(input.BtnLeft):
					ps.Flip = false
					ps.Vel[0] -= 1.0 / 64.0
				case inputs.Held(input.BtnRight):
					ps.Flip = true
					ps.Vel[0] += 1.0 / 64.0
				}
			},
			Hitboxes: []HitboxDef{
				{
					X0: -0.4,
					X1: 0.5,
					Y0: 0.1,
					Y1: 1.3,

					Push: true,
				},
				{
					X0: -0.6,
					X1: 0.6,
					Y0: 0.3,
					Y1: 1.05,

					Push: true,
				},
			},
		},
		tanjerinWalk3: {
			Sprite: 9,
			Update: func(_ *State, ps *PlayerState, inputs rollback.InputFrame) {
				switch {
				case inputs.Held(input.BtnLeft) == inputs.Held(input.BtnRight):
					ps.SetState(tanjerinIdle0)
				case inputs.Held(input.BtnCancel):
					ps.SetState(tanjerinBlock)
				case inputs.Held(input.BtnUp):
					ps.SetState(tanjerinJumpIdle0)
				case ps.Count >= 15:
					ps.SetState(tanjerinWalk0)
				case inputs.Held(input.BtnLeft):
					ps.Flip = false
					ps.Vel[0] -= 1.0 / 64.0
				case inputs.Held(input.BtnRight):
					ps.Flip = true
					ps.Vel[0] += 1.0 / 64.0
				}
			},
			Hitboxes: []HitboxDef{
				{
					X0: -0.4,
					X1: 0.5,
					Y0: 0.1,
					Y1: 1.3,

					Push: true,
				},
				{
					X0: -0.6,
					X1: 0.6,
					Y0: 0.3,
					Y1: 1.05,

					Push: true,
				},
			},
		},
		tanjerinJumpIdle0: {
			Sprite: 26,
			Update: func(_ *State, ps *PlayerState, inputs rollback.InputFrame) {
				switch {
				case !inputs.Held(input.BtnUp) || ps.Count >= 60:
					ps.Vel[1] += float32(ps.Count) / 120
					ps.SetState(tanjerinJump0)
				}
			},
			Hitboxes: []HitboxDef{
				{
					X0: -0.8,
					X1: 0.35,
					Y0: 0.1,
					Y1: 1.2,

					Push: true,
				},
			},
		},
		tanjerinJump0: {
			Sprite: 10,
			Update: func(_ *State, ps *PlayerState, inputs rollback.InputFrame) {
				switch {
				case ps.Pos[1] <= 0:
					ps.SetState(tanjerinIdle0)
				case ps.Count >= 15:
					ps.SetState(tanjerinJump1)
				case inputs.Held(input.BtnLeft) && !inputs.Held(input.BtnRight):
					ps.Flip = false
					ps.Vel[0] -= 1.0 / 64.0
				case !inputs.Held(input.BtnLeft) && inputs.Held(input.BtnRight):
					ps.Flip = true
					ps.Vel[0] += 1.0 / 64.0
				}
			},
			Hitboxes: []HitboxDef{
				{
					X0: -0.4,
					X1: 0.5,
					Y0: 0.1,
					Y1: 1.3,

					Push: true,
				},
				{
					X0: -0.6,
					X1: 0.6,
					Y0: 0.3,
					Y1: 1.05,

					Push: true,
				},
			},
		},
		tanjerinJump1: {
			Sprite: 11,
			Update: func(_ *State, ps *PlayerState, inputs rollback.InputFrame) {
				switch {
				case ps.Pos[1] <= 0:
					ps.SetState(tanjerinIdle0)
				case ps.Count >= 15:
					ps.SetState(tanjerinJump0)
				case inputs.Held(input.BtnLeft) && !inputs.Held(input.BtnRight):
					ps.Flip = false
					ps.Vel[0] -= 1.0 / 64.0
				case !inputs.Held(input.BtnLeft) && inputs.Held(input.BtnRight):
					ps.Flip = true
					ps.Vel[0] += 1.0 / 64.0
				}
			},
			Hitboxes: []HitboxDef{
				{
					X0: -0.4,
					X1: 0.5,
					Y0: 0.1,
					Y1: 1.3,

					Push: true,
				},
				{
					X0: -0.6,
					X1: 0.6,
					Y0: 0.3,
					Y1: 1.05,

					Push: true,
				},
			},
		},
	},
	MaxHP:    100,
	MaxMeter: 100,
}
