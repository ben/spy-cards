//go:build js && wasm
// +build js,wasm

package fighters

import "syscall/js"

var hitboxCharacters = map[string]*Character{
	"t": &tanjerin,
	"m": &maki,
}

func hitboxEditInit() {
	elfHitbox := make(map[string]interface{})

	for k, c := range hitboxCharacters {
		states := make([]interface{}, len(c.States))

		for i := range c.States {
			hitboxes := make([]interface{}, len(c.States[i].Hitboxes))

			for j := range c.States[i].Hitboxes {
				hitboxes[j] = map[string]interface{}{
					"X0":    c.States[i].Hitboxes[j].X0,
					"X1":    c.States[i].Hitboxes[j].X1,
					"Y0":    c.States[i].Hitboxes[j].Y0,
					"Y1":    c.States[i].Hitboxes[j].Y1,
					"Block": c.States[i].Hitboxes[j].Block,
				}
			}

			states[i] = hitboxes
		}

		elfHitbox[k] = states
	}

	js.Global().Set("elfEditingHitbox", false)
	js.Global().Set("elfHitbox", elfHitbox)
}

func hitboxEditUpdate() {
	if !js.Global().Get("elfEditingHitbox").Truthy() {
		return
	}

	elfHitbox := js.Global().Get("elfHitbox")

	safeFloat := func(v js.Value) float32 {
		if !v.Truthy() {
			return 0
		}

		return float32(v.Float())
	}

	for k, c := range hitboxCharacters {
		states := elfHitbox.Get(k)

		for i := range c.States {
			hitboxes := states.Index(i)
			if !hitboxes.Truthy() {
				continue
			}

			c.States[i].Hitboxes = make([]HitboxDef, hitboxes.Length())

			for j := range c.States[i].Hitboxes {
				c.States[i].Hitboxes[j].X0 = safeFloat(hitboxes.Index(j).Get("X0"))
				c.States[i].Hitboxes[j].X1 = safeFloat(hitboxes.Index(j).Get("X1"))
				c.States[i].Hitboxes[j].Y0 = safeFloat(hitboxes.Index(j).Get("Y0"))
				c.States[i].Hitboxes[j].Y1 = safeFloat(hitboxes.Index(j).Get("Y1"))
				c.States[i].Hitboxes[j].Block = hitboxes.Index(j).Get("Block").Truthy()
			}
		}
	}
}
