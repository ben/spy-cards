package fighters

import (
	"git.lubar.me/ben/spy-cards/audio"
	"git.lubar.me/ben/spy-cards/gfx/sprites"
	"git.lubar.me/ben/spy-cards/input"
	"git.lubar.me/ben/spy-cards/rollback"
)

const MaxSoundFrames = 120

type State struct {
	FrameNum uint64
	Player   [2]PlayerState
}

type PlayerState struct {
	Char      *Character
	Pos       [2]float32
	Vel       [2]float32
	HP        uint8
	Meter     int8
	HPDelay   uint8
	HPChanged uint8
	Combo     uint8
	Flip      bool
	State     int
	Count     int
	Sound     [MaxSoundFrames]SoundRecord
}

type SoundRecord struct {
	Sound *audio.Sound
	Pos   [3]float32
}

func (ps *PlayerState) SetState(i int) {
	ps.State = i
	ps.Count = 0

	ps.Sound[0].Sound = ps.Char.States[i].Sound
	if ps.Sound[0].Sound != nil {
		ps.Sound[0].Pos = [3]float32{ps.Pos[0], ps.Pos[1], 0}
	}
}

func (s *State) Render(b *sprites.Batch, sx float32) {
	for p := range s.Player {
		s.Player[p].Char.Render(&s.Player[p], b, sx)
	}
}

func (s *State) Frame() uint64 {
	return s.FrameNum
}

func (s *State) Update(p1, p2 rollback.InputFrame) {
	s.FrameNum++

	// swap left and right buttons for player 2
	left := (p2 >> input.BtnLeft) & 1
	right := (p2 >> input.BtnRight) & 1
	p2 &^= (1 << input.BtnLeft) | (1 << input.BtnRight)
	p2 |= (left << input.BtnRight) | (right << input.BtnLeft)

	for p := range s.Player {
		s.Player[p].Pos[0] += s.Player[p].Vel[0]

		if s.Player[p].Pos[0] < -10 {
			s.Player[p].Pos[0] = -10
		}

		if s.Player[p].Pos[0] > 10 {
			s.Player[p].Pos[0] = 10
		}

		s.Player[p].Pos[1] += s.Player[p].Vel[1]

		if s.Player[p].Pos[1] <= 0 {
			s.Player[p].Pos[1] = 0
			s.Player[p].Vel[0] *= 0.8
			s.Player[p].Vel[1] = 0
		} else {
			s.Player[p].Vel[0] *= 0.9
			s.Player[p].Vel[1] -= 0.01
		}

		switch {
		case s.Player[p].HPDelay > 0:
			s.Player[p].HPDelay--
		case s.Player[p].HPChanged > 0:
			s.Player[p].HPChanged--
		}

		copy(s.Player[p].Sound[1:], s.Player[p].Sound[:])
		s.Player[p].Sound[0].Sound = nil
	}

	s.Player[0].Char.Update(s, &s.Player[0], p1)
	s.Player[1].Char.Update(s, &s.Player[1], p2)

	bestHitbox := [2]*HitboxDef{nil, nil}

	p1s := &s.Player[0].Char.States[s.Player[0].State]
	p2s := &s.Player[1].Char.States[s.Player[1].State]

	for i := range p1s.Hitboxes {
		p1x0 := s.Player[0].Pos[0] + p1s.Hitboxes[i].X0
		p1x1 := s.Player[0].Pos[0] + p1s.Hitboxes[i].X1
		p1y0 := s.Player[0].Pos[1] + p1s.Hitboxes[i].Y0
		p1y1 := s.Player[0].Pos[1] + p1s.Hitboxes[i].Y1

		if s.Player[0].Flip {
			p1x0 = s.Player[0].Pos[0] - p1s.Hitboxes[i].X1
			p1x1 = s.Player[0].Pos[0] - p1s.Hitboxes[i].X0
		}

		for j := range p2s.Hitboxes {
			p2x0 := s.Player[1].Pos[0] + p2s.Hitboxes[j].X0
			p2x1 := s.Player[1].Pos[0] + p2s.Hitboxes[j].X1
			p2y0 := s.Player[1].Pos[1] + p2s.Hitboxes[j].Y0
			p2y1 := s.Player[1].Pos[1] + p2s.Hitboxes[j].Y1

			if s.Player[1].Flip {
				p2x0 = s.Player[1].Pos[0] - p2s.Hitboxes[j].X1
				p2x1 = s.Player[1].Pos[0] - p2s.Hitboxes[j].X0
			}

			if p1x1 < p2x0 || p1x0 > p2x1 || p1y1 < p2y0 || p1y0 > p2y1 {
				continue
			}

			bestHitbox[0] = betterHitbox(bestHitbox[0], &p1s.Hitboxes[i])
			bestHitbox[1] = betterHitbox(bestHitbox[1], &p2s.Hitboxes[j])
		}
	}

	if bestHitbox[0] != nil && bestHitbox[1] != nil {
		// TODO: handle combo/KO
		if bestHitbox[0].Hurt > 0 && !bestHitbox[1].Block && s.Player[0].Count == 1 {
			hurtAmount := bestHitbox[0].Hurt
			if s.Player[1].HP < hurtAmount {
				hurtAmount = s.Player[1].HP
			}

			s.Player[1].HPDelay = 30
			s.Player[1].HPChanged += hurtAmount
			s.Player[1].HP -= hurtAmount
		}

		if bestHitbox[1].Hurt > 0 && !bestHitbox[0].Block && s.Player[1].Count == 1 {
			hurtAmount := bestHitbox[1].Hurt
			if s.Player[0].HP < hurtAmount {
				hurtAmount = s.Player[0].HP
			}

			s.Player[0].HPDelay = 30
			s.Player[0].HPChanged += hurtAmount
			s.Player[0].HP -= hurtAmount
		}
	}
}

func betterHitbox(a, b *HitboxDef) *HitboxDef {
	switch {
	case a == nil:
		return b
	case a.Hurt > b.Hurt:
		return a
	case a.Hurt < b.Hurt:
		return b
	case a.Block:
		return a
	case b.Block:
		return b
	case a.Push:
		return a
	case b.Push:
		return b
	default:
		return a
	}
}

func (s *State) Clone() rollback.State {
	s1 := &State{}
	*s1 = *s

	return s1
}

func (s *State) OnRollback() {
	audio.StopSounds()

	for p := range s.Player {
		for i, sound := range s.Player[p].Sound {
			if sound.Sound != nil {
				sound.Sound.PlaySound(-float64(i+1)/60, 0, 0, sound.Pos[0], sound.Pos[1], sound.Pos[2])
			}
		}
	}
}

func (s *State) OnFrame() {
	for p := range s.Player {
		if sound := s.Player[p].Sound[0]; sound.Sound != nil {
			sound.Sound.PlaySound(0, 0, 0, sound.Pos[0], sound.Pos[1], sound.Pos[2])
		}
	}
}
