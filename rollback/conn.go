package rollback

import (
	"context"
	"fmt"

	"git.lubar.me/ben/spy-cards/scnet"
)

type MatchState int

// MatchState constants.
const (
	StateMatchmakingWait MatchState = iota
	StateMatchmakingConnecting
	StateMatchReady
	StateConnectionFailed
)

type Conn struct {
	State  MatchState
	Signal *scnet.Signal
	Game   *scnet.Game
}

func NewConn(ctx context.Context, playerNumber scnet.Perspective, versionSuffix string) (*Conn, error) {
	var err error

	c := &Conn{}

	c.Signal, err = scnet.NewSignal(ctx)
	if err != nil {
		return nil, fmt.Errorf("creating signaling connection: %w", err)
	}

	err = c.Signal.Init(playerNumber, versionSuffix)
	if err != nil {
		return nil, fmt.Errorf("initializing session: %w", err)
	}

	c.Game, err = scnet.NewGame(ctx)
	if err != nil {
		return nil, fmt.Errorf("creating peer connection: %w", err)
	}

	var discard string

	c.Game.UseSignal(c.Signal, &discard)

	return c, nil
}

func (c *Conn) UpdateState() bool {
	switch c.State {
	case StateMatchmakingWait:
		if cs, _ := c.Game.State(); cs != scnet.StateMatchmakingWait {
			c.State = StateMatchmakingConnecting

			return true
		}
	case StateMatchmakingConnecting:
		if _, ls := c.Game.State(); ls == scnet.StateMatchReady {
			c.State = StateMatchReady

			return true
		}
	case StateMatchReady:
		if cs, _ := c.Game.State(); cs != scnet.StateConnected {
			c.State = StateConnectionFailed

			return true
		}
	case StateConnectionFailed:
		if cs, _ := c.Game.State(); cs == scnet.StateConnected {
			c.State = StateMatchmakingConnecting

			return true
		}
	}

	return false
}
