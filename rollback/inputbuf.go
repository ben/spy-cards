package rollback

import (
	"encoding/binary"
	"fmt"

	"git.lubar.me/ben/spy-cards/input"
)

// number of bits required to uniquely represent each button.
const buttonBits = 4

// ensure buttonBits is high enough.
var _ [1<<buttonBits - input.NumButtons]struct{}

// InputFrame is an array of held buttons for a frame.
type InputFrame uint16

// ensure InputFrame is high enough.
var _ [^InputFrame(0) - (1<<input.NumButtons - 1)]struct{}

func (f InputFrame) Held(btn input.Button) bool {
	return f&(1<<btn) != 0
}

// InputFrames is a slice of InputFrame. The index in the slice corresponds
// to the frame number.
type InputFrames []InputFrame

// Predict returns the predicted input for a specified frame number. If the
// frame is known, the actual input is returned. Otherwise, the last known
// input is returned.
func (f InputFrames) Predict(num uint64) InputFrame {
	if num < uint64(len(f)) {
		return f[num]
	}

	if len(f) == 0 {
		return InputFrame(0)
	}

	return f[len(f)-1]
}

type InputBuf struct {
	StartFrame uint64
	Length     uint64
	FrameAdv   int64
	Packed     []byte
	Bits       uint8 // number of bits remaining in final byte of packed buffer
}

type errBufferStartInFuture struct {
	KnownFrames uint64
	StartFrame  uint64
}

func (err *errBufferStartInFuture) Error() string {
	return fmt.Sprintf("rollback: next frame is %d, but buffer starts at frame %d", err.KnownFrames, err.StartFrame)
}

func (b *InputBuf) Decode(frames *InputFrames) error {
	knownFrames := uint64(len(*frames))

	if b.StartFrame > knownFrames {
		return &errBufferStartInFuture{
			KnownFrames: knownFrames,
			StartFrame:  b.StartFrame,
		}
	}

	if b.StartFrame+b.Length <= knownFrames {
		// don't need any of these frames.
		return nil
	}

	offset, skip := 0, knownFrames-b.StartFrame

	for skip != 0 {
		for b.readBit(&offset) {
			offset += 1 + buttonBits
		}

		skip--
	}

	toDecode := b.StartFrame + b.Length - knownFrames

	var prev InputFrame

	if len(*frames) != 0 {
		prev = (*frames)[len(*frames)-1]
	}

	for toDecode != 0 {
		for b.readBit(&offset) {
			on := b.readBit(&offset)
			btn := b.readButton(&offset)

			if on {
				prev |= 1 << btn
			} else {
				prev &^= 1 << btn
			}
		}

		*frames = append(*frames, prev)
		toDecode--
	}

	return nil
}

func (b *InputBuf) Skip(frames uint64) *InputBuf {
	if frames >= b.Length {
		return &InputBuf{
			StartFrame: b.StartFrame + frames,
		}
	}

	buf := &InputBuf{
		StartFrame: b.StartFrame + frames,
		Length:     b.Length - frames,
	}
	offset := 0
	maxOffset := len(b.Packed)*8 - int(b.Bits)

	for frames > 0 {
		for b.readBit(&offset) {
			offset += 1 + buttonBits
		}

		frames--
	}

	buf.Packed = make([]byte, 0, (maxOffset-offset+7)/8)

	for offset < maxOffset {
		buf.appendBit(b.readBit(&offset))
	}

	return buf
}

func (b *InputBuf) MarshalBinary() ([]byte, error) {
	buf := make([]uint8, binary.MaxVarintLen64*3+len(b.Packed)+1)

	i := binary.PutUvarint(buf, b.StartFrame)
	i += binary.PutUvarint(buf[i:], b.Length)
	i += binary.PutVarint(buf[i:], b.FrameAdv)
	buf = append(buf[:i], b.Packed...)
	buf = append(buf, b.Bits) //nolint:makezero

	return buf, nil
}

func (b *InputBuf) UnmarshalBinary(buf []byte) error {
	var i int

	b.StartFrame, i = binary.Uvarint(buf)
	buf = buf[i:]
	b.Length, i = binary.Uvarint(buf)
	buf = buf[i:]
	b.FrameAdv, i = binary.Varint(buf)
	buf = buf[i:]

	b.Packed = buf[:len(buf)-1]
	b.Bits = buf[len(buf)-1]

	return nil
}

func (b *InputBuf) Append(prev *InputFrame, ictx *input.Context) {
	var frame InputFrame

	for i := input.Button(0); i < input.NumButtons; i++ {
		if ictx.Held(i) {
			frame |= 1 << i
		}
	}

	b.AppendFrame(*prev, frame)
	*prev = frame
}

func (b *InputBuf) AppendFrame(prev, frame InputFrame) {
	b.Length++

	for i := input.Button(0); i < input.NumButtons; i++ {
		if prev.Held(i) != frame.Held(i) {
			b.appendBit(true)
			b.appendBit(frame.Held(i))
			b.appendButton(i)
		}
	}

	b.appendBit(false)
}

func (b *InputBuf) appendBit(on bool) {
	if b.Bits == 0 {
		b.Bits = 8
		b.Packed = append(b.Packed, 0)
	}

	b.Bits--

	if on {
		b.Packed[len(b.Packed)-1] |= 1 << b.Bits
	}
}

func (b *InputBuf) appendButton(btn input.Button) {
	for i := 0; i < buttonBits; i++ {
		b.appendBit(btn&(1<<i) != 0)
	}
}

func (b *InputBuf) readBit(offset *int) bool {
	on := b.Packed[*offset/8]&(128>>(*offset%8)) != 0

	*offset++

	return on
}

func (b *InputBuf) readButton(offset *int) input.Button {
	var btn input.Button

	for i := 0; i < buttonBits; i++ {
		if b.readBit(offset) {
			btn |= 1 << i
		}
	}

	return btn
}
