package rollback

import (
	"encoding/binary"
	"fmt"
	"io"
	"log"
	"time"

	"git.lubar.me/ben/spy-cards/input"
	"git.lubar.me/ben/spy-cards/scnet"
)

type Context struct {
	outgoing     InputBuf
	incoming     InputBuf
	LocalInputs  InputFrames
	RemoteInputs InputFrames
	Known        State
	Predicted    State
	ahead        [40]int64
	behind       [40]int64
	advSlot      uint8
	lastInput    InputFrame
	checkDrop    uint8
	Drop         uint8
	Dropped      bool
}

func (c *Context) Sync(g *scnet.Game, ictx *input.Context) error {
	if err := c.recvBuffer(g); err != nil {
		return err
	}

	if err := c.checkRollback(g); err != nil {
		return err
	}

	if err := c.checkInput(g, ictx); err != nil {
		return err
	}

	return c.sendBuffer(g)
}

func (c *Context) recvBuffer(g *scnet.Game) error {
	buf := g.RecvInputs()
	if buf == nil {
		return nil
	}

	if len(buf) < 8 {
		return fmt.Errorf("rollback: received buffer too short: %w", io.ErrUnexpectedEOF)
	}

	ack := binary.LittleEndian.Uint64(buf)
	c.outgoing = *c.outgoing.Skip(ack - c.outgoing.StartFrame)
	buf = buf[8:]

	err := c.incoming.UnmarshalBinary(buf)
	if err != nil {
		return fmt.Errorf("rollback: decoding remote buffer: %w", err)
	}

	err = c.incoming.Decode(&c.RemoteInputs)
	if err != nil {
		return fmt.Errorf("rollback: decompressing remote inputs: %w", err)
	}

	return nil
}

func (c *Context) checkRollback(g *scnet.Game) error {
	inRollback := false

	frame := c.Known.Frame()

	for frame < uint64(len(c.RemoteInputs)) && frame < uint64(len(c.LocalInputs)) {
		var prevRemoteInput InputFrame
		if frame != 0 {
			prevRemoteInput = c.RemoteInputs[frame-1]
		}

		if c.RemoteInputs[frame] != prevRemoteInput {
			inRollback = true
		}

		p1, p2 := c.LocalInputs[frame], c.RemoteInputs[frame]
		if g.Perspective == scnet.Player2 {
			p1, p2 = p2, p1
		}

		c.Known.Update(p1, p2)

		frame = c.Known.Frame()
	}

	c.Predicted = c.Known.Clone()

	for i := len(c.RemoteInputs); i < len(c.LocalInputs); i++ {
		p1, p2 := c.LocalInputs[i], c.RemoteInputs.Predict(uint64(i))
		if g.Perspective == scnet.Player2 {
			p1, p2 = p2, p1
		}

		c.Predicted.Update(p1, p2)
	}

	if inRollback {
		c.Predicted.OnRollback()
	}

	return nil
}

func (c *Context) checkInput(g *scnet.Game, ictx *input.Context) error {
	remoteFrame := int64(len(c.RemoteInputs)) + int64(g.LastPing()*60/time.Second)
	c.outgoing.FrameAdv = remoteFrame - int64(len(c.LocalInputs))

	c.advSlot++
	if len(c.ahead) <= int(c.advSlot) {
		c.advSlot = 0
	}

	c.ahead[c.advSlot] = c.outgoing.FrameAdv
	c.behind[c.advSlot] = c.incoming.FrameAdv

	if c.checkDrop == 0 {
		c.checkDrop = uint8(len(c.ahead) * 3)
	}

	c.checkDrop--
	if c.checkDrop == 0 {
		c.checkDrop = uint8(len(c.ahead))

		var total float32
		for i := range c.ahead {
			total -= float32(c.ahead[i])
			total += float32(c.behind[i])
		}

		total /= float32(len(c.ahead)) * 2

		if total >= 3 {
			if total > 9 {
				c.Drop = 9
			} else {
				c.Drop = uint8(total)
			}

			log.Println("DEBUG: dropping", c.Drop, "frames to try to sync time state with opponent")
		}
	}

	if c.Drop != 0 {
		c.Dropped = true
		c.Drop--
	} else {
		c.Dropped = false
		c.outgoing.Append(&c.lastInput, ictx)
		c.LocalInputs = append(c.LocalInputs, c.lastInput)
	}

	return nil
}

func (c *Context) sendBuffer(g *scnet.Game) error {
	b, err := c.outgoing.MarshalBinary()
	if err != nil {
		return fmt.Errorf("rollback: encoding input buffer: %w", err)
	}

	var header [8]byte

	binary.LittleEndian.PutUint64(header[:], uint64(len(c.RemoteInputs)))

	err = g.SendInputs(append(header[:], b...))
	if err != nil {
		log.Println("ERROR: failed to send inputs:", err)
	}

	if !c.Dropped {
		p1, p2 := c.lastInput, c.RemoteInputs.Predict(uint64(len(c.LocalInputs)-1))
		if g.Perspective == scnet.Player2 {
			p1, p2 = p2, p1
		}

		c.Predicted.Update(p1, p2)
		c.Predicted.OnFrame()
	}

	return nil
}

func (c *Context) Desync() (ahead, behind int64) {
	return c.ahead[c.advSlot], c.behind[c.advSlot]
}
