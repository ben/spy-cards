package rollback

type State interface {
	// Frame returns the current frame number.
	Frame() uint64

	// Update advances the frame number by 1 and processes the game's logic.
	// There should be no externally visible effects of this function call.
	Update(p1, p2 InputFrame)

	// Clone makes a copy of the State.
	Clone() State

	// OnRollback is called when a rollback occurs. The game should reset
	// all externally visible state. (eg. sound effects)
	OnRollback()

	// OnFrame is called after Update if the changes to the state should be
	// made externally visible.
	OnFrame()
}
