package scnet

import (
	"context"
	"encoding/hex"
	"fmt"
	"log"
	"math/rand"
	"sync"
	"time"

	"git.lubar.me/ben/spy-cards/card"
	"git.lubar.me/ben/spy-cards/internal/router"
)

type Perspective uint8

// Constants for Perspective.
const (
	UnknownPerspective Perspective = 0
	Player1            Perspective = 1
	Player2            Perspective = 2
)

var defaultConfig = rtcConfig(
	4,
	[]string{
		"stun:stunserver.stunprotocol.org:3478",
		"stun:stun1.l.google.com:19302",
		"stun:stun1.l.google.com:19305",
	},
	"",
	"",
	"",
)

var emptyConfig = rtcConfig(0, nil, "", "", "")

// Game is the peer-to-peer game connection.
type Game struct {
	ctx    context.Context
	conn   *rtcConnection
	mock   *Game
	signal *Signal

	secure  *rtcDataChannel
	control *rtcDataChannel
	unrel   *rtcDataChannel

	Perspective          Perspective
	logicalState         LogicalState
	connectPeer          Perspective
	receivedCards        bool
	receivedSpoilerGuard bool
	sentReady            bool
	recvReady            bool
	wantUndoReady        bool
	opponentQuit         bool
	sentRematch          bool
	recvRematch          bool
	BetweenMatches       bool

	SCG                *SecureCardGame
	Cards              *card.Set
	mode               *string
	secureBuf          chan []byte
	remoteSpoilerGuard []byte
	remoteCosmetic     *card.CosmeticData
	remoteCardBacks    chan card.UnknownDeck
	recvModifyCards    chan []byte
	recvModifyCardsAck chan struct{}
	recvInHand         chan []byte
	opponentReady      chan []byte
	previewReady       uint64
	fakeRNG            *rand.Rand
	lastPing           time.Duration
	unrelInputs        []byte
	pong               chan uint64
	lock               sync.Mutex
	cond               *sync.Cond
}

func NewGame(ctx context.Context) (*Game, error) {
	conn, err := rtcNewPeerConnection(rtcRelayConfig(defaultConfig, false))
	if err != nil {
		return nil, fmt.Errorf("scnet: creating game connection: %w", err)
	}

	g := &Game{
		ctx:  ctx,
		conn: conn,

		connectPeer: Player1,
	}

	g.cond = sync.NewCond(&g.lock)

	if conn != nil {
		g.registerCrashCleanupHandler()

		// secure channel: used for synchronizing keys.
		// messages are sent from both sides at known synchronization points.
		g.secure, err = conn.CreateDataChannel("secure", rtcDataChannelInit(0, true))
		if err != nil {
			return nil, fmt.Errorf("scnet: creating data channels: %w", err)
		}

		// control channel: used for game messages. format depends on the game.
		g.control, err = conn.CreateDataChannel("control", rtcDataChannelInit(1, true))
		if err != nil {
			return nil, fmt.Errorf("scnet: creating data channels: %w", err)
		}

		// unreliable channel: used for game messages in rollback modes.
		g.unrel, err = conn.CreateDataChannel("unreliable", rtcDataChannelInit(2, false))
		if err != nil {
			return nil, fmt.Errorf("scnet: creating data channels: %w", err)
		}
	}

	g.SCG, err = NewSecureCardGame(nil)
	if err != nil {
		return nil, fmt.Errorf("scnet: creating secure card game context: %w", err)
	}

	g.secureBuf = make(chan []byte, 2)

	g.recvModifyCards = make(chan []byte, 1)
	g.recvModifyCardsAck = make(chan struct{}, 1)
	g.recvInHand = make(chan []byte, 1)
	g.opponentReady = make(chan []byte, 1)
	g.remoteCardBacks = make(chan card.UnknownDeck)

	if FakeDrop != 0 || FakeLatency != 0 || FakeJitter != 0 {
		/*#nosec*/
		g.fakeRNG = rand.New(rand.NewSource(time.Now().UnixNano()))
	}

	g.pong = make(chan uint64, 1)

	if conn != nil {
		g.conn.OnNegotiationNeeded(g.onNegotiationNeeded)
		g.conn.OnICECandidate(g.onICECandidate)

		g.secure.OnMessage(g.onSecureMessage)
		g.control.OnMessage(g.onControlMessage)
		g.unrel.OnMessage(g.onUnreliableMessage)

		g.conn.OnConnectionStateChange(g.onConnectionStateChange)
		g.conn.OnICEConnectionStateChange(g.onICEConnectionStateChange)
		g.secure.OnOpen(g.onChannelOpen)
		g.control.OnOpen(g.onChannelOpen)
		g.unrel.OnOpen(g.onChannelOpen)

		go g.statsWatcher()
	}

	return g, nil
}

func (g *Game) simulateNetworkConditions() bool {
	if g.fakeRNG != nil {
		if g.fakeRNG.Float64() < FakeDrop {
			return true
		}

		delay := FakeLatency
		if FakeJitter != 0 {
			delay += time.Duration(g.fakeRNG.Int63n(int64(FakeJitter)))
		}

		time.Sleep(delay)
	}

	return false
}

func (g *Game) sendOnChannel(channel **rtcDataChannel, b []byte) error {
	if router.FlagRTCSpew.IsSet() {
		channelName := "???"
		switch channel {
		case &g.secure:
			channelName = "secure"
		case &g.control:
			channelName = "control"
		case &g.unrel:
			channelName = "unrel"
		}
		log.Println("DEBUG: send on channel", channelName, hex.Dump(b))
	}

	if g.conn != nil {
		err := (*channel).Send(b)
		if err != nil {
			return fmt.Errorf("scnet: send failed: %w", err)
		}

		return nil
	}

	if g.mock == nil {
		panic("scnet: mock channel send without mock partner")
	}

	switch channel {
	case &g.secure:
		go g.mock.onSecureMessage(rtcDataChannelMessage{Data: b})
	case &g.control:
		go g.mock.onControlMessage(rtcDataChannelMessage{Data: b})
	case &g.unrel:
		go g.mock.onUnreliableMessage(rtcDataChannelMessage{Data: b})
	default:
		panic("scnet: mock channel send on unhandled channel")
	}

	return nil
}

func (g *Game) onLAN() {
	conn, err := rtcNewPeerConnection(rtcRelayConfig(emptyConfig, false))
	if err != nil {
		log.Println("ERROR: scnet: recreating game connection:", err)

		return
	}

	_ = g.conn.Close()

	g.conn = conn

	g.registerCrashCleanupHandler()

	g.secure, err = conn.CreateDataChannel("secure", rtcDataChannelInit(0, true))
	if err != nil {
		log.Println("ERROR: scnet: creating data channels:", err)

		return
	}

	g.control, err = conn.CreateDataChannel("control", rtcDataChannelInit(1, true))
	if err != nil {
		log.Println("ERROR: scnet: creating data channels:", err)

		return
	}

	g.unrel, err = conn.CreateDataChannel("unreliable", rtcDataChannelInit(2, false))
	if err != nil {
		log.Println("ERROR: scnet: creating data channels:", err)

		return
	}

	g.conn.OnNegotiationNeeded(g.onNegotiationNeeded)
	g.conn.OnICECandidate(g.onICECandidate)

	g.secure.OnMessage(g.onSecureMessage)
	g.control.OnMessage(g.onControlMessage)
	g.unrel.OnMessage(g.onUnreliableMessage)

	g.conn.OnConnectionStateChange(g.onConnectionStateChange)
	g.conn.OnICEConnectionStateChange(g.onICEConnectionStateChange)
	g.secure.OnOpen(g.onChannelOpen)
	g.control.OnOpen(g.onChannelOpen)
	g.unrel.OnOpen(g.onChannelOpen)
}
