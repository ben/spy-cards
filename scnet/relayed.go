package scnet

import (
	"encoding/json"
	"log"
)

type relayedMessageType string

const (
	relayedDescription relayedMessageType = "d"
	relayedCandidate   relayedMessageType = "i"
)

type relayed struct {
	Message     relayedMessageType `json:"m"`
	Description rawMessage         `json:"d,omitempty"`
	Candidate   rawMessage         `json:"i,omitempty"`
}

func (s *Signal) sendRelayed(m *relayed) {
	if s.mock != nil {
		go s.mock.partner.dispatchRelayed(m)

		return
	}

	err := s.ensureConn()
	if err != nil {
		log.Println("ERROR: failed to get connection for relayed signal message:", err)

		return
	}

	if !s.didHandshake {
		return
	}

	b, err := json.Marshal(m)
	if err != nil {
		log.Println("ERROR: failed to encode relayed signal message:", err)

		return
	}

	b = append([]byte{signalRelayMesg}, b...)

	err = s.writeText(b)
	if err != nil {
		log.Println("ERROR: failed to send relayed signal message:", err)
	}
}

func (s *Signal) dispatchRelayed(r *relayed) {
	switch r.Message {
	case relayedDescription:
		s.onDescription(r.Description)
	case relayedCandidate:
		s.onCandidate(r.Candidate)
	default:
		log.Println("ERROR: unhandled relayed signal message:", r.Message)
	}
}
