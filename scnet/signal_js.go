//go:build js && wasm
// +build js,wasm

package scnet

import (
	"log"
	"syscall/js"

	"git.lubar.me/ben/spy-cards/internal"
)

type wsConn struct {
	value js.Value
}

func (c wsConn) Close(code int, reason string) error {
	c.value.Call("close", code, reason)

	return nil
}

func (c wsConn) Subprotocol() string {
	return c.value.Get("protocol").String()
}

func (c wsConn) SetReadLimit(x int64) {}

func (s *Signal) haveConn() bool {
	return s.conn.value.Truthy()
}

func (s *Signal) dial() (wsConn, bool, error) {
	u := internal.GetConfig(s.ctx).MatchmakingServer

	internal.PerformanceMark("signalDialStart")

	ws, err := internal.Await(js.Global().Get("Promise").New(js.Global().Get("Function").New("u", `return function openWebSocket(resolve, reject) {
	var ws = new WebSocket(u, ["match"]);
	ws.binaryType = "arraybuffer";
	ws.onerror = reject;
	ws.onopen = function() {
		resolve(ws);
	};
};`).Invoke(u)))

	internal.PerformanceMark("signalDialFinish")
	internal.PerformanceMeasure("signalDial", "signalDialStart", "signalDialFinish")

	if err == nil {
		return wsConn{ws}, false, nil
	}

	return wsConn{}, true, err //nolint:wrapcheck
}

func (s *Signal) writeText(b []byte) (err error) {
	defer func() {
		if r := recover(); r != nil {
			err = r.(js.Error)
		}
	}()

	s.conn.value.Call("send", string(b))

	return nil
}

func (s *Signal) reader(conn wsConn) {
	var onClose js.Func

	onMessage := js.FuncOf(func(_ js.Value, args []js.Value) interface{} {
		s.onMessage([]byte(args[0].Get("data").String()))

		return js.Undefined()
	})
	onClose = js.FuncOf(func(_ js.Value, args []js.Value) interface{} {
		onMessage.Release()
		onClose.Release()

		s.lock.Lock()

		s.conn = wsConn{}
		s.closeCode = args[0].Get("code").Int()
		s.closeReason = args[0].Get("reason").String()

		if s.closeCode == internal.MMNotFound {
			s.wantClose = true
		}

		if s.closeCode == internal.NormalClosure {
			// just in case
			go s.reconnect()
		}

		if !s.wantClose {
			log.Println("ERROR: signal connection closed with code", s.closeCode, "and reason", s.closeReason)
		}

		s.cond.Broadcast()
		s.lock.Unlock()

		return js.Undefined()
	})

	s.conn.value.Set("onmessage", onMessage)
	s.conn.value.Set("onclose", onClose)
}
