//go:build !js || !wasm
// +build !js !wasm

package scnet

func (g *Game) statsWatcher() {
	// no API for stats-getting in Pion
}
