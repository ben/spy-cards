//go:build js && wasm
// +build js,wasm

package scnet

import (
	"encoding/json"
	"log"
	"runtime"
	"syscall/js"

	"git.lubar.me/ben/spy-cards/internal"
	"git.lubar.me/ben/spy-cards/internal/router"
)

const (
	rtcUnknown                       = "unknown"
	rtcDataChannelStateConnecting    = "connecting"
	rtcDataChannelStateOpen          = "open"
	rtcSignalingStateStable          = "stable"
	rtcPeerConnectionStateNew        = "new"
	rtcPeerConnectionStateConnecting = "connecting"
	rtcPeerConnectionStateConnected  = "connected"
	rtcPeerConnectionStateFailed     = "failed"
	rtcICEConnectionStateNew         = "new"
	rtcICEConnectionStateChecking    = "checking"
	rtcICEConnectionStateConnected   = "connected"
	rtcICEConnectionStateFailed      = "failed"
	rtcSDPTypeOffer                  = "offer"
)

type (
	rtcICECandidate        = js.Value
	rtcPeerConnectionState = string
	rtcICEConnectionState  = string
	rawMessage             = json.RawMessage
)

var rtcRollbackSDP = js.ValueOf(map[string]interface{}{
	"type": "rollback",
})

type rtcConnection struct {
	conn                       js.Value
	onNegotiationNeeded        js.Func
	onICECandidate             js.Func
	onConnectionStateChange    js.Func
	onICEConnectionStateChange js.Func
}

func rtcNewPeerConnection(opts js.Value) (*rtcConnection, error) {
	if router.FlagMockRTC.IsSet() {
		return nil, nil
	}

	peerConnectionConstructor := js.Global().Get("RTCPeerConnection")
	if !peerConnectionConstructor.Truthy() {
		log.Println("ERROR: a required feature (WebRTC) is not supported by this browser")

		router.FlagMockRTC.ForceSet(true)

		return nil, nil
	}

	c := &rtcConnection{
		conn: peerConnectionConstructor.New(opts),
	}

	c.conn.Call("addEventListener", "icegatheringstatechange", internal.Function.New("e", `if (e.target.iceGatheringState === "gathering") {
	performance.mark("iceGatherStart");
} else if (e.target.iceGatheringState === "complete") {
	performance.mark("iceGatherFinish");
	performance.measure("iceGather", "iceGatherStart", "iceGatherFinish");
}`))

	js.Global().Get("SpyCards").Get("crashCleanup").Call("push", c.conn.Get("close").Call("bind", c.conn))

	runtime.SetFinalizer(c, (*rtcConnection).release)

	return c, nil
}

func (c *rtcConnection) release() {
	c.onNegotiationNeeded.Release()
	c.onICECandidate.Release()
	c.onConnectionStateChange.Release()
	c.onICEConnectionStateChange.Release()
}

func (c *rtcConnection) Close() error {
	c.conn.Call("close")

	return nil
}

func (c *rtcConnection) SignalingState() string {
	return c.conn.Get("signalingState").String()
}

func (c *rtcConnection) ConnectionState() string {
	cs := c.conn.Get("connectionState")
	if cs.Truthy() {
		return cs.String()
	}

	return rtcUnknown
}

func (c *rtcConnection) ICEConnectionState() string {
	return c.conn.Get("iceConnectionState").String()
}

func (c *rtcConnection) ICEGatheringState() string {
	return c.conn.Get("iceGatheringState").String()
}

func (c *rtcConnection) LocalDescription() js.Value {
	return c.conn.Get("localDescription")
}

func (c *rtcConnection) CreateOffer(opts js.Value) (js.Value, error) {
	return internal.AwaitNoRandomFail(c.conn.Call("createOffer", opts)) //nolint:wrapcheck
}

func (c *rtcConnection) CreateAnswer(_ *struct{}) (js.Value, error) {
	return internal.AwaitNoRandomFail(c.conn.Call("createAnswer", js.Null())) //nolint:wrapcheck
}

func (c *rtcConnection) SetLocalDescription(sdp js.Value) error {
	_, err := internal.AwaitNoRandomFail(c.conn.Call("setLocalDescription", sdp))

	return err //nolint:wrapcheck
}

func (c *rtcConnection) SetRemoteDescription(sdp js.Value) error {
	_, err := internal.AwaitNoRandomFail(c.conn.Call("setRemoteDescription", sdp))

	return err //nolint:wrapcheck
}

func (c *rtcConnection) AddICECandidate(candidate js.Value) error {
	_, err := internal.AwaitNoRandomFail(c.conn.Call("addIceCandidate", candidate))

	return err //nolint:wrapcheck
}

func (c *rtcConnection) OnNegotiationNeeded(f func()) {
	if c.onNegotiationNeeded.Truthy() {
		c.conn.Call("removeEventListener", "negotiationeeded", c.onNegotiationNeeded)
		c.onNegotiationNeeded.Release()
	}

	c.onNegotiationNeeded = js.FuncOf(func(js.Value, []js.Value) interface{} {
		go f()

		return js.Undefined()
	})
	c.conn.Call("addEventListener", "negotiationneeded", c.onNegotiationNeeded)
}

func (c *rtcConnection) OnICECandidate(f func(rtcICECandidate)) {
	if c.onICECandidate.Truthy() {
		c.conn.Call("removeEventListener", "icecandidate", c.onICECandidate)
		c.onICECandidate.Release()
	}

	c.onICECandidate = js.FuncOf(func(_ js.Value, args []js.Value) interface{} {
		go f(args[0].Get("candidate"))

		return js.Undefined()
	})
	c.conn.Call("addEventListener", "icecandidate", c.onICECandidate)
}

func (c *rtcConnection) OnConnectionStateChange(f func(rtcPeerConnectionState)) {
	if c.onConnectionStateChange.Truthy() {
		c.conn.Call("removeEventListener", "connectionstatechange", c.onConnectionStateChange)
		c.onConnectionStateChange.Release()
	}

	c.onConnectionStateChange = js.FuncOf(func(js.Value, []js.Value) interface{} {
		go f(c.ConnectionState())

		return js.Undefined()
	})
	c.conn.Call("addEventListener", "connectionstatechange", c.onConnectionStateChange)
}

func (c *rtcConnection) OnICEConnectionStateChange(f func(rtcICEConnectionState)) {
	if c.onICEConnectionStateChange.Truthy() {
		c.conn.Call("removeEventListener", "iceconnectionstatechange", c.onICEConnectionStateChange)
		c.onICEConnectionStateChange.Release()
	}

	c.onICEConnectionStateChange = js.FuncOf(func(js.Value, []js.Value) interface{} {
		go f(c.ICEConnectionState())

		return js.Undefined()
	})
	c.conn.Call("addEventListener", "iceconnectionstatechange", c.onICEConnectionStateChange)
}

func (c *rtcConnection) CreateDataChannel(label string, opts js.Value) (*rtcDataChannel, error) {
	dc := &rtcDataChannel{
		ch: c.conn.Call("createDataChannel", label, opts),
	}

	dc.ch.Set("binaryType", "arraybuffer")

	runtime.SetFinalizer(dc, (*rtcDataChannel).release)

	return dc, nil
}

func rtcDataChannelInit(id int, reliable bool) js.Value {
	if reliable {
		return js.ValueOf(map[string]interface{}{
			"id":         id,
			"negotiated": true,
		})
	}

	return js.ValueOf(map[string]interface{}{
		"id":             id,
		"negotiated":     true,
		"ordered":        false,
		"maxRetransmits": 0,
	})
}

type rtcDataChannel struct {
	ch        js.Value
	onMessage js.Func
	onOpen    js.Func
}

func (dc *rtcDataChannel) release() {
	dc.onMessage.Release()
	dc.onOpen.Release()
}

func (dc *rtcDataChannel) OnOpen(f func()) {
	if dc.onOpen.Truthy() {
		dc.ch.Call("removeEventListener", "open", dc.onOpen)
		dc.onOpen.Release()
	}

	dc.onOpen = js.FuncOf(func(js.Value, []js.Value) interface{} {
		go f()

		return js.Undefined()
	})
	dc.ch.Call("addEventListener", "open", dc.onOpen)
}

func (dc *rtcDataChannel) OnMessage(f func(rtcDataChannelMessage)) {
	if dc.onMessage.Truthy() {
		dc.ch.Call("removeEventListener", "message", dc.onMessage)
		dc.onMessage.Release()
	}

	dc.onMessage = js.FuncOf(func(_ js.Value, args []js.Value) interface{} {
		js.Global().Get("console").Call("log", args[0])
		data := internal.Uint8Array.New(args[0].Get("data"), 0)

		b := make([]byte, data.Length())
		js.CopyBytesToGo(b, data)

		go f(rtcDataChannelMessage{
			Data: b,
		})

		return js.Undefined()
	})
	dc.ch.Call("addEventListener", "message", dc.onMessage)
}

func (dc *rtcDataChannel) Send(b []byte) (err error) {
	defer func() {
		if r := recover(); r != nil {
			err = r.(js.Error)
		}
	}()

	buf := internal.Uint8Array.New(len(b))

	js.CopyBytesToJS(buf, b)

	dc.ch.Call("send", buf)

	return
}

func (dc *rtcDataChannel) BufferedAmount() uint64 {
	return uint64(dc.ch.Get("bufferedAmount").Int())
}

func (dc *rtcDataChannel) ReadyState() string {
	return dc.ch.Get("readyState").String()
}

func rtcConfig(candidates int, stunURLs []string, turnURL, turnUser, turnKey string) map[string]interface{} {
	servers := make([]interface{}, len(stunURLs), len(stunURLs)+1)

	for i, u := range stunURLs {
		servers[i] = map[string]interface{}{
			"urls": u,
		}
	}

	if turnURL != "" {
		servers = append(servers, map[string]interface{}{
			"urls":           turnURL,
			"username":       turnUser,
			"credential":     turnKey,
			"credentialType": "password",
		})
	}

	return map[string]interface{}{
		"iceServers":           servers,
		"iceCandidatePoolSize": candidates,
	}
}

func rtcRelayConfig(config map[string]interface{}, relayOnly bool) js.Value {
	copied := make(map[string]interface{}, len(config)+1)

	for k, v := range config {
		copied[k] = v
	}

	if relayOnly {
		servers := copied["iceServers"].([]interface{})
		copied["iceServers"] = servers[len(servers)-1:]
		copied["iceTransportPolicy"] = "relay"
	} else {
		copied["iceTransportPolicy"] = "all"
	}

	return js.ValueOf(copied)
}

func rtcOfferOptions(iceRestart bool) js.Value {
	return js.ValueOf(map[string]interface{}{
		"iceRestart": iceRestart,
	})
}

func sdpType(v js.Value) string {
	return v.Get("type").String()
}

func sdpToJSON(v js.Value) rawMessage {
	if !v.Truthy() {
		return nil
	}

	return rawMessage(js.Global().Get("JSON").Call("stringify", v).String())
}

func sdpFromJSON(v rawMessage) js.Value {
	if v == nil {
		return js.Null()
	}

	return js.Global().Get("JSON").Call("parse", string(v))
}

func iceCandidateToJSON(v js.Value) rawMessage {
	return sdpToJSON(v)
}

func iceCandidateFromJSON(v rawMessage) (js.Value, error) {
	return sdpFromJSON(v), nil
}

type rtcDataChannelMessage struct {
	Data []byte
}
