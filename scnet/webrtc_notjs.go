//go:build !js || !wasm
// +build !js !wasm

package scnet

import (
	"encoding/json"

	"git.lubar.me/ben/spy-cards/internal"
	"github.com/pion/webrtc/v3"
)

const (
	rtcDataChannelStateConnecting    = webrtc.DataChannelStateConnecting
	rtcSignalingStateStable          = webrtc.SignalingStateStable
	rtcUnknown                       = webrtc.Unknown
	rtcPeerConnectionStateNew        = webrtc.PeerConnectionStateNew
	rtcPeerConnectionStateConnecting = webrtc.PeerConnectionStateConnecting
	rtcPeerConnectionStateConnected  = webrtc.PeerConnectionStateConnected
	rtcPeerConnectionStateFailed     = webrtc.PeerConnectionStateFailed
	rtcICEConnectionStateNew         = webrtc.ICEConnectionStateNew
	rtcICEConnectionStateChecking    = webrtc.ICEConnectionStateChecking
	rtcICEConnectionStateConnected   = webrtc.ICEConnectionStateConnected
	rtcICEConnectionStateFailed      = webrtc.ICEConnectionStateFailed
	rtcSDPTypeOffer                  = webrtc.SDPTypeOffer
)

type (
	rtcConnection          = webrtc.PeerConnection
	rtcDataChannel         = webrtc.DataChannel
	rtcICECandidate        = *webrtc.ICECandidate
	rtcDataChannelMessage  = webrtc.DataChannelMessage
	rtcPeerConnectionState = webrtc.PeerConnectionState
	rtcICEConnectionState  = webrtc.ICEConnectionState
	rawMessage             = json.RawMessage
)

var rtcRollbackSDP = webrtc.SessionDescription{Type: webrtc.SDPTypeRollback}

func rtcNewPeerConnection(config webrtc.Configuration) (*webrtc.PeerConnection, error) {
	return webrtc.NewPeerConnection(config) //nolint:wrapcheck
}

func rtcConfig(candidates uint8, stunURLs []string, turnURL, turnUser, turnKey string) webrtc.Configuration {
	servers := make([]webrtc.ICEServer, len(stunURLs), len(stunURLs)+1)

	for i, u := range stunURLs {
		servers[i] = webrtc.ICEServer{
			URLs: []string{u},
		}
	}

	if turnURL != "" {
		servers = append(servers, webrtc.ICEServer{
			URLs:           []string{turnURL},
			Username:       turnUser,
			Credential:     turnKey,
			CredentialType: webrtc.ICECredentialTypePassword,
		})
	}

	return webrtc.Configuration{
		ICEServers:           servers,
		ICECandidatePoolSize: candidates,
	}
}

func rtcRelayConfig(config webrtc.Configuration, relayOnly bool) webrtc.Configuration {
	if relayOnly {
		config.ICEServers = config.ICEServers[len(config.ICEServers)-1:]
		config.ICETransportPolicy = webrtc.ICETransportPolicyRelay
	} else {
		config.ICETransportPolicy = webrtc.ICETransportPolicyAll
	}

	return config
}

func rtcOfferOptions(iceRestart bool) *webrtc.OfferOptions {
	return &webrtc.OfferOptions{ICERestart: iceRestart}
}

func rtcDataChannelInit(id uint16, reliable bool) *webrtc.DataChannelInit {
	negotiated := true

	if reliable {
		return &webrtc.DataChannelInit{
			Negotiated: &negotiated,
			ID:         &id,
		}
	}

	return &webrtc.DataChannelInit{
		Negotiated:     &negotiated,
		ID:             &id,
		Ordered:        new(bool),
		MaxRetransmits: new(uint16),
	}
}

func sdpType(sdp webrtc.SessionDescription) webrtc.SDPType {
	return sdp.Type
}

func sdpToJSON(sdp *webrtc.SessionDescription) rawMessage {
	if sdp == nil {
		return nil
	}

	b, err := json.Marshal(sdp)
	if err != nil {
		panic(err)
	}

	return b
}

func sdpFromJSON(m rawMessage) webrtc.SessionDescription {
	var sdp webrtc.SessionDescription

	err := json.Unmarshal(m, &sdp)
	if err != nil {
		panic(err)
	}

	return sdp
}

func iceCandidateToJSON(c *webrtc.ICECandidate) rawMessage {
	if c == nil {
		return nil
	}

	b, err := json.Marshal(c.ToJSON())
	if err != nil {
		panic(err)
	}

	return b
}

func iceCandidateFromJSON(m rawMessage) (webrtc.ICECandidateInit, error) {
	var i webrtc.ICECandidateInit

	err := internal.UnmarshalJSON(m, &i)

	return i, err //nolint:wrapcheck
}
