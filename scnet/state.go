//go:generate stringer -type ConnectionState -trimprefix State
//go:generate stringer -type LogicalState -trimprefix State

package scnet

type ConnectionState uint8

// Constants for ConnectionState.
const (
	StateMatchmakingWait ConnectionState = iota
	StateConnecting
	StateConnected
	StateConnectionLost
)

type LogicalState uint8

// Constants for LogicalState.
const (
	StateUninitialized LogicalState = iota
	StateSecureInitializing
	StateSecureReady
	StateMatchReady
)

func (g *Game) State() (ConnectionState, LogicalState) {
	if g.conn == nil {
		if g.mock == nil {
			return StateMatchmakingWait, StateUninitialized
		}

		return StateConnected, g.logicalState
	}

	switch g.conn.ConnectionState() {
	case rtcPeerConnectionStateNew:
		return StateMatchmakingWait, StateUninitialized
	case rtcPeerConnectionStateConnecting:
		// Safari doesn't return "new" when it should.
		if g.conn.SignalingState() != rtcSignalingStateStable {
			return StateMatchmakingWait, StateUninitialized
		}

		return StateConnecting, StateUninitialized
	case rtcPeerConnectionStateConnected:
		return StateConnected, g.logicalState
	case rtcUnknown:
		// firefox
		switch g.conn.ICEConnectionState() {
		case rtcICEConnectionStateNew:
			return StateMatchmakingWait, StateUninitialized
		case rtcICEConnectionStateChecking:
			return StateConnecting, StateUninitialized
		case rtcICEConnectionStateConnected:
			return StateConnected, g.logicalState
		default:
			return StateConnectionLost, StateUninitialized
		}
	default:
		return StateConnectionLost, StateUninitialized
	}
}

func (g *Game) onConnectionStateChange(state rtcPeerConnectionState) {
	if state == rtcPeerConnectionStateConnected {
		g.onConnected()
	}

	if state == rtcPeerConnectionStateFailed {
		g.connectPeer = 3 - g.connectPeer
		g.negotiationNeeded(true)
	}

	g.cond.Broadcast()
}

func (g *Game) onICEConnectionStateChange(state rtcICEConnectionState) {
	if g.conn.ConnectionState() == rtcUnknown {
		if state == rtcICEConnectionStateConnected {
			g.onConnected()
		}

		if state == rtcICEConnectionStateFailed {
			g.connectPeer = 3 - g.connectPeer
			g.negotiationNeeded(true)
		}
	}

	g.cond.Broadcast()
}

func (g *Game) onChannelOpen() {
	g.cond.Broadcast()
}

func (g *Game) onConnected() {
	g.lock.Lock()

	if g.logicalState == StateUninitialized {
		g.logicalState = StateSecureInitializing

		go g.initSecure(false)

		if g.conn != nil {
			go g.pinger()
		}
	}

	g.lock.Unlock()
}

func (g *Game) WaitMatchReady() {
	g.lock.Lock()

	for g.logicalState != StateMatchReady {
		g.cond.Wait()
	}

	g.lock.Unlock()
}
