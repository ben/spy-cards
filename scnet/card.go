package scnet

import (
	"encoding/binary"
	"encoding/json"
	"fmt"
	"log"
	"math/bits"

	"git.lubar.me/ben/spy-cards/card"
	"git.lubar.me/ben/spy-cards/format"
	"git.lubar.me/ben/spy-cards/internal"
	"git.lubar.me/ben/spy-cards/internal/router"
)

func (g *Game) SendCosmeticData(c card.CosmeticData) error {
	b, err := json.Marshal(c)
	if err != nil {
		return fmt.Errorf("scnet: encoding cosmetic data: %w", err)
	}

	err = g.sendOnChannel(&g.control, append([]byte{controlCosmeticData}, b...))
	if err != nil {
		return fmt.Errorf("scnet: sending cosmetic data: %w", err)
	}

	return nil
}

func (g *Game) RecvCosmeticData() *card.CosmeticData {
	g.lock.Lock()
	c := g.remoteCosmetic
	g.lock.Unlock()

	return c
}

func (g *Game) SendDeck(deck card.Deck, useEffectBack bool) {
	go func() {
		backs := make(card.UnknownDeck, len(deck))
		for i, c := range deck {
			backs[i] = g.Cards.Card(c).Rank.Back(useEffectBack)
		}

		opponentBacks, err := g.SCG.SetDeck(g.exchangeData, deck, backs)
		if err != nil {
			panic(err) // TODO
		}

		g.remoteCardBacks <- opponentBacks
	}()
}

func (g *Game) RecvDeck() (card.UnknownDeck, bool) {
	select {
	case d := <-g.remoteCardBacks:
		return d, true
	default:
		return nil, false
	}
}

func (g *Game) WaitRecvDeck() card.UnknownDeck {
	internal.WarnIfCalledFromRenderLoop("WaitRecvDeck")

	return <-g.remoteCardBacks
}

func (g *Game) ExchangeInHand(data []byte) []byte {
	internal.WarnIfCalledFromRenderLoop("ExchangeInHand")

	err := g.sendOnChannel(&g.control, append([]byte{controlInHand}, data...))
	if err != nil {
		log.Println("ERROR: failed to send InHand message:", err)
	}

	return <-g.recvInHand
}

func (g *Game) SendReady(turnData *card.TurnData) {
	var tap format.Writer

	tap.UVarInt(turnData.Ready[g.Perspective-1])

	for _, id := range turnData.Played[g.Perspective-1] {
		tap.UVarInt(uint64(id))
	}

	promise, err := g.SCG.PrepareTurn(tap.Data())
	if err != nil {
		log.Println("ERROR: preparing ready:", err)

		return
	}

	err = g.sendOnChannel(&g.control, append([]byte{controlReady}, promise...))
	if err != nil {
		log.Println("ERROR: sending ready:", err)
	}

	g.setReady(&g.sentReady)
}

func (g *Game) UndoReady() {
	g.lock.Lock()

	if g.sentReady && !g.wantUndoReady {
		err := g.sendOnChannel(&g.control, []byte{controlUndoReady, 0})
		if err != nil {
			log.Println("ERROR: sending controlUndoReady request:", err)
		} else {
			g.wantUndoReady = true
		}
	}

	g.lock.Unlock()
}

func (g *Game) RecvReady(turnData *card.TurnData) bool {
	select {
	case b := <-g.opponentReady:
		g.decodeReady(turnData, b)

		return true
	default:
		return false
	}
}

func (g *Game) WaitRecvReady(turnData *card.TurnData) {
	internal.WarnIfCalledFromRenderLoop("WaitRecvReady")

	b := <-g.opponentReady

	g.decodeReady(turnData, b)
}

func (g *Game) decodeReady(turnData *card.TurnData, b []byte) {
	var err error

	defer func() {
		if err != nil {
			panic(err)
		}
	}()

	defer format.Catch(&err)

	var r format.Reader

	r.Init(b)

	turnData.Ready[2-g.Perspective] = r.UVarInt()
	turnData.Played[2-g.Perspective] = make([]card.ID, bits.OnesCount64(turnData.Ready[2-g.Perspective]))

	for i := range turnData.Played[2-g.Perspective] {
		turnData.Played[2-g.Perspective][i] = card.ID(r.UVarInt())
	}
}

func (g *Game) setReady(b *bool) {
	g.lock.Lock()

	*b = true

	if g.recvReady && g.sentReady {
		g.sentReady, g.recvReady, g.wantUndoReady = false, false, false

		go func() {
			data, err := g.SCG.ConfirmTurn(g.exchangeData)
			if err != nil {
				log.Println("ERROR: confirming turn:", err)
			}

			g.opponentReady <- data
		}()
	}

	g.lock.Unlock()
}

func (g *Game) SentReady() (sent, wantUndo bool) {
	g.lock.Lock()
	sent = g.sentReady
	wantUndo = g.wantUndoReady
	g.lock.Unlock()

	return
}

func (g *Game) OpponentReady() bool {
	g.lock.Lock()
	r := g.recvReady
	g.lock.Unlock()

	return r
}

func (g *Game) SendModifiedCards(m []card.ModifiedCardPosition, revealCard bool) error {
	internal.WarnIfCalledFromRenderLoop("SendModifiedCards")

	if router.FlagDebugRevealModified.IsSet() {
		revealCard = true
	}

	var w format.Writer

	w.Write(uint8(controlModifyCards))
	w.UVarInt(uint64(len(m)))

	for _, c := range m {
		if int64(c.Position) < 0 {
			panic("scnet: card position underflow")
		}

		if c.InHand {
			w.SVarInt(int64(c.Position))
		} else {
			w.SVarInt(^int64(c.Position))
		}

		if revealCard {
			w.UVarInt(uint64(c.CardID))
		}
	}

	err := g.sendOnChannel(&g.control, w.Data())
	if err != nil {
		return fmt.Errorf("scnet: sending modified cards: %w", err)
	}

	<-g.recvModifyCardsAck

	return nil
}

func (g *Game) RecvModifiedCards(revealCard bool) []card.ModifiedCardPosition {
	internal.WarnIfCalledFromRenderLoop("RecvModifiedCards")

	if router.FlagDebugRevealModified.IsSet() {
		revealCard = true
	}

	b := <-g.recvModifyCards

	var r format.Reader

	r.Init(b)

	m := make([]card.ModifiedCardPosition, r.UVarInt())

	for i := range m {
		pos := r.SVarInt()

		if pos >= 0 {
			m[i].InHand = true
			m[i].Position = uint64(pos)
		} else {
			pos = ^pos
			m[i].InHand = false
			m[i].Position = uint64(pos)
		}

		if revealCard {
			m[i].CardID = card.ID(r.UVarInt())
		}
	}

	if err := g.sendOnChannel(&g.control, []byte{controlModifyCardsAck}); err != nil {
		log.Println("WARNING: failed to ack modified cards:", err)
	}

	return m
}

func (g *Game) BeginTurn(ch chan<- *card.TurnData) {
	internal.WarnIfCalledFromRenderLoop("BeginTurn")

	data, err := g.SCG.BeginTurn(g.exchangeData)
	if err != nil {
		log.Println("ERROR: synchronizing start of turn:", err)
	}

	g.lock.Lock()
	g.previewReady = 0
	g.lock.Unlock()

	ch <- data
}

func (g *Game) SendPreviewReady(tap uint64) {
	var b [9]byte
	b[0] = controlPreviewReady
	binary.LittleEndian.PutUint64(b[1:], tap)

	if err := g.sendOnChannel(&g.control, b[:]); err != nil {
		log.Println("WARNING: sending card selection preview:", err)
	}
}

func (g *Game) PreviewReady() uint64 {
	g.lock.Lock()
	tap := g.previewReady
	g.lock.Unlock()

	return tap
}

type (
	VerifyInitFunc = func(card.Deck, card.Deck, card.UnknownDeck, []byte, []byte, []byte) error
	VerifyTurnFunc = func(seed, seed2 []byte, turn *card.TurnData) error
)

func (g *Game) FinalizeMatch(verifyInit VerifyInitFunc, verifyTurn VerifyTurnFunc) error {
	return g.SCG.Finalize(g.exchangeData, func(deck card.Deck, backs card.UnknownDeck) error {
		return verifyInit(g.SCG.localDeckInitial, deck, backs, g.SCG.Seed(RNGShared), g.SCG.Seed(RNGLocal), g.SCG.Seed(RNGRemote))
	}, func(t *scgTurn) error {
		return verifyTurn(t.seed, t.seed2, &t.data)
	})
}

func (g *Game) OfferRematch() error {
	g.lock.Lock()

	if g.recvRematch && !g.sentRematch {
		g.doRematch()
	}

	g.sentRematch = true
	g.cond.Broadcast()
	g.lock.Unlock()

	err := g.sendOnChannel(&g.control, []byte{controlRematch})
	if err != nil {
		return fmt.Errorf("scnet: sending rematch offer: %w", err)
	}

	return nil
}

func (g *Game) doRematch() {
	var err error

	g.SCG, err = NewSecureCardGame(nil)
	if err != nil {
		log.Printf("ERROR: re-initializing secure card game: %+v", err)
	}

	g.BetweenMatches = false

	g.logicalState = StateSecureInitializing
	go g.initSecure(true)
}

func (g *Game) RematchStatus() (connected, recv, sent bool) {
	g.lock.Lock()
	connected = !g.opponentQuit
	recv = g.recvRematch
	sent = g.sentRematch
	g.lock.Unlock()

	return
}

func (g *Game) WaitAcceptRematch() bool {
	g.lock.Lock()
	for !g.opponentQuit && !g.recvRematch {
		g.cond.Wait()
	}

	if g.opponentQuit {
		g.lock.Unlock()

		return false
	}

	g.lock.Unlock()

	if err := g.OfferRematch(); err != nil {
		log.Printf("WARNING: accepting rematch: %+v", err)

		return false
	}

	return true
}

func (g *Game) ClearRematchStatus() {
	g.lock.Lock()
	g.sentRematch = false
	g.recvRematch = false
	g.cond.Broadcast()
	g.lock.Unlock()
}
