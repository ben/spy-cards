//go:build !js || !wasm
// +build !js !wasm

package scnet

import (
	"errors"
	"log"

	"git.lubar.me/ben/spy-cards/internal"
	"github.com/coder/websocket"
)

type wsConn = *websocket.Conn

func (s *Signal) haveConn() bool {
	return s.conn != nil
}

func (s *Signal) dial() (wsConn, bool, error) {
	u := internal.GetConfig(s.ctx).MatchmakingServer
	opts := &websocket.DialOptions{
		Subprotocols: []string{"match"},
	}

	var ce websocket.CloseError

	conn, _, err := websocket.Dial(s.ctx, u, opts)

	return conn, errors.As(err, &ce), err
}

func (s *Signal) writeText(b []byte) error {
	return s.conn.Write(s.ctx, websocket.MessageText, b) //nolint:wrapcheck
}

func (s *Signal) reader(conn *websocket.Conn) {
	defer func() {
		go func() {
			s.lock.Lock()

			if s.conn == conn {
				s.conn = nil
				s.cond.Broadcast()
			}

			s.lock.Unlock()
		}()
	}()

	for {
		mt, b, err := conn.Read(s.ctx)
		if err != nil {
			var ce websocket.CloseError
			if errors.As(err, &ce) {
				if ce.Code == websocket.StatusNormalClosure {
					// just in case
					go s.reconnect()

					return
				}

				s.closeCode = int(ce.Code)
				s.closeReason = ce.Reason

				if ce.Code == internal.MMNotFound {
					s.wantClose = true
				}
			}

			log.Println("ERROR: failed read from signal:", err)

			_ = conn.Close(internal.MMConnectionError, "read encountered error: "+err.Error())

			go s.reconnect()

			return
		}

		if mt != websocket.MessageText {
			log.Println("ERROR: read unexpected message type from signal:", mt)

			_ = conn.Close(internal.MMConnectionError, "read received message type "+mt.String())

			return
		}

		if len(b) == 0 {
			log.Println("ERROR: read empty signal message")

			_ = conn.Close(internal.MMConnectionError, "read empty message")

			return
		}

		s.onMessage(b)
	}
}
