//go:build js && wasm
// +build js,wasm

package scnet

import (
	"log"
	"syscall/js"
)

func ensureStateUpdate() <-chan struct{} {
	ch := make(chan struct{})

	var f js.Func

	f = js.FuncOf(func(_ js.Value, _ []js.Value) interface{} {
		f.Release()

		close(ch)

		return js.Undefined()
	})

	js.Global().Call("setTimeout", f, 0)

	return ch
}

func (g *Game) registerCrashCleanupHandler() {
	conn := g.conn.conn
	js.Global().Get("SpyCards").Get("crashCleanup").Call("push", conn.Get("close").Call("bind", conn))

	// This leaks, but again, there are a very small number of game connections
	// per page load, so it should be fine.
	f := js.FuncOf(func(_ js.Value, args []js.Value) interface{} {
		if args[0].Get("type").String() == "unload" {
			if g.control != nil && g.control.ReadyState() == rtcDataChannelStateOpen {
				err := g.control.Send([]byte{controlGameQuit})
				if err != nil {
					log.Println("WARNING: sending tab death notification:", err)
				}
			}
		} else if cs, ls := g.State(); cs == StateConnected && ls == StateMatchReady && !g.opponentQuit && !g.BetweenMatches {
			args[0].Call("preventDefault")
		}

		return js.Undefined()
	})

	js.Global().Call("addEventListener", "beforeunload", f)
	js.Global().Call("addEventListener", "unload", f)
}
