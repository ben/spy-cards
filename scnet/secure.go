package scnet

import (
	"encoding/hex"
	"fmt"
	"log"
	"strconv"

	"git.lubar.me/ben/spy-cards/card"
	"git.lubar.me/ben/spy-cards/internal"
	"git.lubar.me/ben/spy-cards/internal/router"
	"git.lubar.me/ben/spy-cards/spoilerguard"
)

func (g *Game) onSecureMessage(msg rtcDataChannelMessage) {
	if router.FlagRTCSpew.IsSet() {
		log.Println("DEBUG: recv on channel secure", hex.Dump(msg.Data))
	}

	select {
	case g.secureBuf <- msg.Data:
		if len(msg.Data) != 1 {
			err := g.sendOnChannel(&g.secure, []byte{0})
			if err != nil {
				log.Println("ERROR: sending secure card game ack:", err)
			}
		}
	default:
		log.Println("WARNING: dropping secure card game message: message already buffered")
	}
}

func (g *Game) exchangeData(b []byte) ([]byte, error) {
	if err := g.sendOnChannel(&g.secure, b); err != nil {
		return nil, fmt.Errorf("scnet: synchronizing secure card game state (outgoing): %w", err)
	}

	a, b := <-g.secureBuf, <-g.secureBuf

	// if the simulation is in the message handler, it causes a race condition
	g.simulateNetworkConditions()

	if len(a) == 1 && len(b) != 1 {
		return b, nil
	}

	if len(b) == 1 && len(a) != 1 {
		return a, nil
	}

	panic("scnet: secure card game received packets of length " + strconv.Itoa(len(a)) + " and " + strconv.Itoa(len(b)) + " but exactly one should be 1 byte")
}

func (g *Game) initSecure(isRematch bool) {
	g.lock.Lock()

	for g.conn != nil && (g.secure.ReadyState() == rtcDataChannelStateConnecting || g.control.ReadyState() == rtcDataChannelStateConnecting) {
		g.cond.Wait()
	}

	versionBuf := []byte{controlVersion}
	versionBuf = append(versionBuf, internal.VersionNumber()...)

	if *g.mode != "" {
		versionBuf = append(versionBuf, '-')
		versionBuf = append(versionBuf, *g.mode...)
	}

	g.lock.Unlock()

	err := g.sendOnChannel(&g.control, versionBuf)
	if err != nil {
		panic(err) // TODO
	}

	err = g.SCG.Init(uint8(g.Perspective), g.exchangeData)
	if err != nil {
		panic(err) // TODO
	}

	if !isRematch {
		g.lock.Lock()
		g.logicalState = StateSecureReady
		g.cond.Broadcast()
		g.lock.Unlock()

		err = g.initCards()
		if err != nil {
			panic(err) // TODO
		}

		sg := spoilerguard.LoadData()
		sgBuf := []byte{controlSpoilerGuard}

		if sg != nil && len(sg.SeenSpiedEnemies) == 256/8 {
			sgBuf = append(sgBuf, sg.SeenSpiedEnemies...)
			sgBuf = append(sgBuf, uint8(sg.Menu))
		}

		err = g.sendOnChannel(&g.control, sgBuf)
		if err != nil {
			panic(err) // TODO
		}

		g.lock.Lock()
		for !g.receivedSpoilerGuard {
			g.cond.Wait()
		}
		g.lock.Unlock()

		if len(g.remoteSpoilerGuard) >= 256/8 {
			spoilerguard.Apply(g.ctx, g.Cards, g.remoteSpoilerGuard[:256/8], false)
		} else {
			spoilerguard.Apply(g.ctx, g.Cards, nil, false)
		}
	}

	g.lock.Lock()
	g.logicalState = StateMatchReady
	g.cond.Broadcast()
	g.lock.Unlock()

	_ = g.signal.Close()
}

func (g *Game) initCards() error {
	if g.Perspective == Player1 {
		if g.Cards == nil {
			g.Cards = &card.Set{}
		}

		g.Cards.Mode, _ = g.Cards.Mode.Variant(g.Cards.Variant)

		b, err := g.Cards.MarshalText()
		if err != nil {
			return fmt.Errorf("scnet: encoding cards to send to opponent: %w", err)
		}

		err = g.sendOnChannel(&g.control, append([]byte{controlCustomCards}, b...))
		if err != nil {
			return fmt.Errorf("scnet: sending cards to opponent: %w", err)
		}

		return nil
	}

	g.lock.Lock()

	for !g.receivedCards {
		g.cond.Wait()
	}

	g.lock.Unlock()

	return nil
}
