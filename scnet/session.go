package scnet

import (
	"fmt"
	"log"
)

func (s *Signal) Perspective() Perspective {
	s.lock.Lock()
	p := s.perspective
	s.lock.Unlock()

	return p
}

func (s *Signal) Init(perspective Perspective, suffix string) error {
	s.suffix = suffix

	if s.mock != nil {
		s.lock.Lock()
		defer s.lock.Unlock()

		if perspective == UnknownPerspective {
			if s.mock.primary {
				s.perspective = Player1
			} else {
				s.perspective = Player2
			}

			s.session = "M0CK M0CK M0CK M0CK M0CK M0CK"
		} else {
			s.perspective = perspective

			if perspective == Player1 {
				s.session = "M0CKM0CKM0CKM0CKM0CKM0CK"
			}
		}

		if s.game != nil {
			s.game.Perspective = s.perspective
		}

		s.cond.Broadcast()

		return nil
	}

	if err := s.ensureConn(); err != nil {
		log.Println("ERROR: initializing signal connection:", err)

		return err
	}

	s.lock.Lock()
	s.perspective = perspective

	if s.game != nil {
		s.game.Perspective = s.perspective
	}
	s.lock.Unlock()

	buf := make([]byte, len(suffix)+2)
	buf[0] = signalHandshake
	copy(buf[2:], suffix)

	switch perspective {
	case UnknownPerspective:
		buf[1] = 'q'
	case Player1, Player2:
		buf[1] = uint8('0' + perspective)
	default:
		panic("scnet: invalid value for perspective")
	}

	err := s.writeText(buf)
	if err != nil {
		return fmt.Errorf("scnet: failed to send signal init packet: %w", err)
	}

	return nil
}

func (s *Signal) Session() string {
	s.lock.Lock()
	sess := s.session
	s.lock.Unlock()

	return sess
}

func (s *Signal) SetSession(sess string) error {
	s.lock.Lock()

	if s.session != "" {
		s.lock.Unlock()

		panic("scnet: already set session")
	}

	if s.perspective != Player2 {
		s.lock.Unlock()

		panic("scnet: cannot set session ID as player number other than 2")
	}

	s.session = sess

	s.cond.Broadcast()

	if s.mock != nil {
		if !s.didHandshake {
			if s.game != nil {
				if s.game.conn != nil {
					err := s.game.conn.Close()
					if err != nil {
						log.Println("WARNING: closing rtc connection:", err)
					}

					s.game.conn = nil
					s.game.secure = nil
					s.game.control = nil
					s.game.unrel = nil
				}

				s.game.mock = s.mock.partner.game
				if s.game.mock != nil {
					s.game.onConnected()
				}
			}

			s.didHandshake = true
		}

		if !s.mock.partner.didHandshake {
			if s.mock.partner.game != nil {
				if s.mock.partner.game.conn != nil {
					err := s.mock.partner.game.conn.Close()
					if err != nil {
						log.Println("WARNING: closing rtc connection:", err)
					}

					s.mock.partner.game.conn = nil
					s.mock.partner.game.secure = nil
					s.mock.partner.game.control = nil
					s.mock.partner.game.unrel = nil
				}

				s.mock.partner.game.mock = s.game
				if s.game != nil {
					s.mock.partner.game.onConnected()
				}
			}

			s.mock.partner.didHandshake = true
		}

		s.lock.Unlock()

		return nil
	}

	s.lock.Unlock()

	err := s.ensureConn()
	if err != nil {
		return err
	}

	err = s.writeText(append([]byte{signalSessionID}, sess...))
	if err != nil {
		return fmt.Errorf("scnet: joining session: %w", err)
	}

	return nil
}

func (s *Signal) WaitSession() string {
	s.lock.Lock()

	sess := s.session

	for sess == "" && s.haveConn() {
		s.cond.Wait()

		sess = s.session
	}

	s.lock.Unlock()

	return sess
}
