package scnet

func (s *Signal) onUsed() {
	s.game.Perspective = s.perspective

	if s.lan {
		s.game.onLAN()
	}

	if s.didHandshake {
		if desc := sdpToJSON(s.game.conn.LocalDescription()); desc != nil {
			s.sendRelayed(&relayed{
				Message:     relayedDescription,
				Description: desc,
			})
		}
	}

	if s.description != nil {
		s.game.onDescription(s.description)

		s.description = nil
	}

	if len(s.candidates) != 0 {
		for _, c := range s.candidates {
			s.game.onCandidate(c)
		}

		s.candidates = nil
	}
}

func (s *Signal) sendDescription(desc rawMessage) {
	s.sendRelayed(&relayed{
		Message:     relayedDescription,
		Description: desc,
	})
}

func (s *Signal) onDescription(desc rawMessage) {
	s.lock.Lock()

	g := s.game
	if g == nil {
		s.description = desc
	}

	s.lock.Unlock()

	if g != nil {
		g.onDescription(desc)
	}
}

func (s *Signal) sendCandidate(candidate rawMessage) {
	if s.wantClose {
		// don't reopen the signaling connection for this
		return
	}

	s.sendRelayed(&relayed{
		Message:   relayedCandidate,
		Candidate: candidate,
	})
}

func (s *Signal) onCandidate(candidate rawMessage) {
	s.lock.Lock()

	g := s.game
	if g == nil {
		s.candidates = append(s.candidates, candidate)
	}

	s.lock.Unlock()

	if g != nil {
		g.onCandidate(candidate)
	}
}
