package scnet

import (
	"log"
)

func (g *Game) onNegotiationNeeded() {
	g.negotiationNeeded(false)
}

func (g *Game) negotiationNeeded(iceRestart bool) {
	if iceRestart {
		log.Printf("INFO: restarting connection negotiation with P%d as leader", g.connectPeer)
	}

	desc, err := g.conn.CreateOffer(rtcOfferOptions(iceRestart))
	if err != nil {
		log.Println("ERROR: creating game connection offer:", err)

		return
	}

	<-ensureStateUpdate()

	if g.Perspective != g.connectPeer && g.conn.SignalingState() != rtcSignalingStateStable {
		log.Println("DEBUG: discarding outgoing connection offer")

		return
	}

	err = g.conn.SetLocalDescription(desc)
	if err != nil {
		log.Println("ERROR: setting local game connection offer:", err)

		return
	}

	if g.signal != nil {
		g.signal.sendDescription(sdpToJSON(g.conn.LocalDescription()))
	}
}

func (g *Game) onICECandidate(candidate rtcICECandidate) {
	if c := iceCandidateToJSON(candidate); g.signal != nil && c != nil {
		g.signal.sendCandidate(c)
	}
}

func (g *Game) onDescription(desc rawMessage) {
	<-ensureStateUpdate()

	sdp := sdpFromJSON(desc)

	if sdpType(sdp) == rtcSDPTypeOffer && g.conn.SignalingState() != rtcSignalingStateStable {
		if g.Perspective == g.connectPeer {
			log.Println("DEBUG: discarding incoming connection offer")

			return
		}

		err := g.conn.SetLocalDescription(rtcRollbackSDP)
		if err != nil {
			log.Println("WARNING: error rolling back game connection state:", err)
		}
	}

	err := g.conn.SetRemoteDescription(sdp)
	if err != nil {
		log.Println("ERROR: setting remote connection offer:", err)

		return
	}

	if sdpType(sdp) == rtcSDPTypeOffer {
		answer, err := g.conn.CreateAnswer(nil)
		if err != nil {
			log.Println("ERROR: creating game connection response:", err)

			return
		}

		err = g.conn.SetLocalDescription(answer)
		if err != nil {
			log.Println("ERROR: setting game connection response:", err)

			return
		}

		g.signal.sendDescription(sdpToJSON(g.conn.LocalDescription()))
	}
}

func (g *Game) onCandidate(candidate rawMessage) {
	c, err := iceCandidateFromJSON(candidate)
	if err == nil {
		err = g.conn.AddICECandidate(c)
	}

	if err != nil {
		log.Println("WARNING: processing game connection candidate:", err)
	}
}

func (g *Game) UseSignal(s *Signal, mode *string) {
	if g.signal != nil {
		panic("scnet: already using a signal connection")
	}

	s.lock.Lock()

	if s.game != nil {
		s.lock.Unlock()

		panic("scnet: signal connection already in use")
	}

	g.mode = mode

	g.signal = s
	s.game = g
	s.onUsed()

	s.lock.Unlock()
}
