package scnet

import (
	"encoding/json"
	"log"
	"strconv"

	"git.lubar.me/ben/spy-cards/internal"
)

const (
	signalHandshake = 'p'
	signalSessionID = 's'
	signalRelayMesg = 'r'
	signalKeepAlive = 'k'
	signalTerminate = 'q'
)

func (s *Signal) onMessage(b []byte) {
	switch b[0] {
	case signalSessionID:
		s.lock.Lock()

		if s.perspective == UnknownPerspective {
			s.perspective = Perspective(b[1] - '0')

			if s.game != nil {
				s.game.Perspective = s.perspective
			}
		} else {
			s.session = string(b[1:])
			s.cond.Broadcast()
		}

		s.lock.Unlock()
	case signalKeepAlive:
		// received keepalive response
	case signalHandshake:
		s.lock.Lock()

		if len(b) == 2 && b[1] == 'l' {
			// LAN indicator
			s.lan = true

			if s.game != nil {
				s.game.onLAN()
			}
		} else if !s.didHandshake {
			if s.game != nil {
				desc := sdpToJSON(s.game.conn.LocalDescription())
				if desc != nil {
					go s.sendRelayed(&relayed{
						Message:     relayedDescription,
						Description: desc,
					})
				}
			}

			s.didHandshake = true
		}

		s.lock.Unlock()
	case signalRelayMesg:
		r := &relayed{}

		err := json.Unmarshal(b[1:], r)
		if err != nil {
			log.Println("ERROR: decoding relayed message:", err)
		}

		go s.dispatchRelayed(r)
	case signalTerminate:
		log.Println("ERROR: server reports matchmaking connection interrupted")

		_ = s.conn.Close(internal.MMServerError, "received quit packet")

		go s.reconnect()

		return
	default:
		log.Println("ERROR: unhandled signal message:", strconv.Quote(string(b)))
		panic("scnet: unhandled message type " + strconv.QuoteRune(rune(b[0])))
	}
}
