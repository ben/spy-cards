//go:build !js || !wasm
// +build !js !wasm

package scnet

func ensureStateUpdate() <-chan struct{} {
	ch := make(chan struct{})

	close(ch)

	return ch
}

func (g *Game) registerCrashCleanupHandler() {}
