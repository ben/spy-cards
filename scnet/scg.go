//go:generate stringer -type syncPoint -trimprefix sync
//go:generate stringer -type RNGSharedType -trimprefix RNG

package scnet

import (
	"bytes"
	"crypto/rand"
	"crypto/sha256"
	"encoding/base64"
	"errors"
	"fmt"
	"hash"
	"sync"

	"git.lubar.me/ben/spy-cards/card"
	"git.lubar.me/ben/spy-cards/format"
	"git.lubar.me/ben/spy-cards/rng"
)

// ExchangeDataFunc is a simplified networking API where all messages are sent
// in both directions simultaneously.
type ExchangeDataFunc func([]byte) ([]byte, error)

// RNGSharedType determines which random number generator state is being used.
type RNGSharedType uint8

// Constants for RNGSharedType.
const (
	RNGLocal RNGSharedType = iota
	RNGShared
	RNGRemote
)

type scgTurn struct {
	seed    []byte
	data    card.TurnData
	seed2   []byte
	promise []byte
	confirm [2][]byte
}

// SecureCardGameOptions modifies the behavior of SecureCardGame.
type SecureCardGameOptions struct {
	NewHash    func() hash.Hash
	SeedLength int
	RandLength int
	ForReplay  *[3]uint64
}

type syncPoint uint8

const (
	syncInitialSeed syncPoint = iota
	syncDeckPromise
	syncTurnSeedPromise
	syncTurnSeed
	syncConfirmTurn
	syncFinalize
)

func doExchangeData(sp syncPoint, si int, exchangeData ExchangeDataFunc, sendData []byte) (b []byte, err error) {
	defer func() {
		if err != nil {
			err = fmt.Errorf("while exchanging data for %v[%d]: %w", sp, si, err)
		}
	}()
	defer format.Catch(&err)

	var w format.Writer

	w.UVarInt(uint64(sp))
	w.UVarInt(uint64(si))
	w.Bytes(sendData)

	recvData, err := exchangeData(w.Data())
	if err != nil {
		return nil, err
	}

	var r format.Reader

	r.Init(recvData)

	recvSyncPoint := syncPoint(r.UVarInt())
	recvSyncIndex := int(r.UVarInt())

	if recvSyncPoint != sp || recvSyncIndex != si {
		return nil, fmt.Errorf("received data for %v[%d]: %s", recvSyncPoint, recvSyncIndex, base64.StdEncoding.EncodeToString(recvData))
	}

	return r.Bytes(r.Len()), nil
}

// SecureCardGame provides secrecy and verifiability to the networked card game.
type SecureCardGame struct {
	newHash func() hash.Hash
	hash    hash.Hash
	seedLen int
	randLen int
	compat  *[3]uint64

	finalized bool
	player    uint8
	turns     []*scgTurn

	seed [3][]byte
	rng  [3]*rng.RNG

	localDeckInitial  card.Deck
	remoteDeckInitial card.Deck
	remoteInitialHash []byte
	remoteCardBacks   card.UnknownDeck

	lock sync.Mutex
}

// NewSecureCardGame creates a SecureCardGame instance.
func NewSecureCardGame(opt *SecureCardGameOptions) (*SecureCardGame, error) {
	if opt == nil {
		opt = &SecureCardGameOptions{}
	}

	scg := &SecureCardGame{
		newHash: opt.NewHash,
		seedLen: opt.SeedLength,
		randLen: opt.RandLength,
	}

	if scg.newHash == nil {
		scg.newHash = sha256.New
	}

	scg.hash = scg.newHash()

	if scg.seedLen == 0 {
		scg.seedLen = 16
	}

	if scg.randLen == 0 {
		scg.randLen = 4
	}

	scg.seed[RNGShared] = make([]byte, scg.seedLen*2)

	if opt.ForReplay != nil {
		scg.compat = new([3]uint64)
		*scg.compat = *opt.ForReplay
		scg.turns = make([]*scgTurn, 1)

		return scg, nil
	}

	_, err := rand.Read(scg.seed[RNGShared][:scg.seedLen])
	if err != nil {
		return nil, fmt.Errorf("scnet: reading random state: %w", err)
	}

	scg.seed[RNGLocal] = make([]byte, scg.randLen)

	_, err = rand.Read(scg.seed[RNGLocal])
	if err != nil {
		return nil, fmt.Errorf("scnet: reading random state: %w", err)
	}

	return scg, nil
}

// Init starts a secure card game session.
func (scg *SecureCardGame) Init(playerNumber uint8, exchangeData ExchangeDataFunc) error {
	scg.lock.Lock()
	defer scg.lock.Unlock()

	if scg.player != 0 {
		return errors.New("scnet: init called multiple times on a single connection")
	}

	if playerNumber != 1 && playerNumber != 2 {
		return errors.New("scnet: playerNumber must be either 1 or 2")
	}

	scg.player = playerNumber

	sendSeed := scg.seed[RNGShared][:scg.seedLen]

	if playerNumber == 2 {
		copy(scg.seed[RNGShared][scg.seedLen:], sendSeed)
	}

	recvSeed, err := doExchangeData(syncInitialSeed, 0, exchangeData, sendSeed)
	if err != nil {
		return err
	}

	if len(recvSeed) != scg.seedLen {
		return fmt.Errorf("scnet: initial handshake: expected %d bytes of data, but received %d bytes", scg.seedLen, len(recvSeed))
	}

	if playerNumber == 1 {
		copy(scg.seed[RNGShared][scg.seedLen:], recvSeed)
	} else {
		copy(scg.seed[RNGShared], recvSeed)
	}

	return nil
}

// SetDeck sets the current player's deck and returns the backs of the opponent's cards.
func (scg *SecureCardGame) SetDeck(exchangeData ExchangeDataFunc, deck card.Deck, backs card.UnknownDeck) (card.UnknownDeck, error) {
	scg.lock.Lock()
	defer scg.lock.Unlock()

	if scg.player == 0 {
		return nil, errors.New("scnet: init must be called before setDeck")
	}

	if scg.localDeckInitial != nil {
		return nil, errors.New("scnet: setDeck has already been called")
	}

	if deck == nil {
		return nil, errors.New("scnet: missing deck")
	}

	typedDeck, err := deck.MarshalBinary()
	if err != nil {
		return nil, fmt.Errorf("scnet: encoding deck: %w", err)
	}

	scg.localDeckInitial = make(card.Deck, len(deck))
	copy(scg.localDeckInitial, deck)

	scg.hash.Reset()
	_, _ = scg.hash.Write(scg.seed[RNGShared])
	_, _ = scg.hash.Write(scg.seed[RNGLocal])
	_, _ = scg.hash.Write(typedDeck)
	bufferHash := scg.hash.Sum(nil)

	cardBacks, err := backs.MarshalBinary()
	if err != nil {
		return nil, fmt.Errorf("scnet: encoding card backs: %w", err)
	}

	sendData := make([]byte, 0, len(bufferHash)+len(cardBacks))
	sendData = append(sendData, bufferHash...)
	sendData = append(sendData, cardBacks...)

	recvData, err := doExchangeData(syncDeckPromise, 0, exchangeData, sendData)
	if err != nil {
		return nil, err
	}

	if len(recvData) < len(bufferHash) {
		return nil, fmt.Errorf("scnet: while exchanging deck information: expected at least %d bytes of data, but received %d bytes", len(bufferHash), len(recvData))
	}

	scg.remoteInitialHash = recvData[:len(bufferHash)]
	scg.remoteCardBacks = nil

	err = scg.remoteCardBacks.UnmarshalBinary(recvData[len(bufferHash):])
	if err != nil {
		return nil, fmt.Errorf("scnet: decoding opponent card backs: %w", err)
	}

	remoteCardBacks := make(card.UnknownDeck, len(scg.remoteCardBacks))
	copy(remoteCardBacks, scg.remoteCardBacks)

	scg.turns = make([]*scgTurn, 0, 32)

	return remoteCardBacks, nil
}

// BeginTurn starts a round of the card game.
func (scg *SecureCardGame) BeginTurn(exchangeData ExchangeDataFunc) (*card.TurnData, error) {
	scg.lock.Lock()
	defer scg.lock.Unlock()

	if scg.turns == nil {
		return nil, errors.New("scnet: init and setDeck must be called before beginTurn may be called")
	}

	if scg.finalized {
		return nil, errors.New("scnet: cannot begin turn on finalized game")
	}

	turn := &scgTurn{
		seed: make([]byte, scg.randLen*2),
	}

	sendSeed := turn.seed[int(scg.player-1)*scg.randLen:][:scg.randLen]

	if _, err := rand.Read(sendSeed); err != nil {
		return nil, fmt.Errorf("scnet: reading random state: %w", err)
	}

	scg.hash.Reset()
	_, _ = scg.hash.Write(scg.seed[RNGShared])
	_, _ = scg.hash.Write(sendSeed)
	sendHash := scg.hash.Sum(nil)

	recvHash, err := doExchangeData(syncTurnSeedPromise, len(scg.turns)+1, exchangeData, sendHash)
	if err != nil {
		return nil, err
	}

	if len(sendHash) != len(recvHash) {
		return nil, fmt.Errorf("scnet: setting up turn (1): expected %d bytes of data, but received %d bytes", len(sendHash), len(recvHash))
	}

	recvSeed, err := doExchangeData(syncTurnSeed, len(scg.turns)+1, exchangeData, sendSeed)
	if err != nil {
		return nil, err
	}

	if len(sendSeed) != len(recvSeed) {
		return nil, fmt.Errorf("scnet: setting up turn (2): expected %d bytes of data, but received %d bytes", len(sendHash), len(recvHash))
	}

	scg.hash.Reset()
	_, _ = scg.hash.Write(scg.seed[RNGShared])
	_, _ = scg.hash.Write(recvSeed)
	verifyHash := scg.hash.Sum(nil)

	if !bytes.Equal(recvHash, verifyHash) {
		return nil, errors.New("scnet: remote hash did not match value. possible implementation error or cheating attempt")
	}

	copy(turn.seed[int(2-scg.player)*scg.randLen:], recvSeed)
	scg.turns = append(scg.turns, turn)

	scg.InitTurnSeed(turn.seed, scg.seed[RNGLocal], nil)

	return &turn.data, nil
}

// PrepareTurn computes a promise for the provided data.
func (scg *SecureCardGame) PrepareTurn(data []byte) ([]byte, error) {
	scg.lock.Lock()
	defer scg.lock.Unlock()

	if len(scg.turns) == 0 {
		return nil, errors.New("scnet: init, setDeck, and beginTurn must be called before prepareTurn may be called")
	}

	if scg.finalized {
		return nil, errors.New("scnet: cannot prepare turn on finalized game")
	}

	turn := scg.turns[len(scg.turns)-1]
	if turn.seed2 == nil {
		turn.seed2 = make([]byte, scg.randLen*2)
	}

	confirm := make([]byte, scg.randLen, scg.randLen+len(data))

	if _, err := rand.Read(confirm); err != nil {
		return nil, fmt.Errorf("scnet: reading random state: %w", err)
	}

	copy(turn.seed2[int(scg.player-1)*scg.randLen:], confirm)

	confirm = append(confirm, data...)

	turn.confirm[scg.player-1] = confirm

	scg.hash.Reset()
	_, _ = scg.hash.Write(scg.seed[RNGShared])
	_, _ = scg.hash.Write(confirm)

	return scg.hash.Sum(nil), nil
}

// PromiseTurn stores the opponent's promised turn data.
func (scg *SecureCardGame) PromiseTurn(promise []byte) error {
	scg.lock.Lock()
	defer scg.lock.Unlock()

	if len(scg.turns) == 0 {
		return errors.New("scnet: init, setDeck, and beginTurn must be called before promiseTurn may be called")
	}

	if scg.finalized {
		return errors.New("scnet: cannot modify turn on finalized game")
	}

	turn := scg.turns[len(scg.turns)-1]

	turn.promise = make([]byte, len(promise))
	copy(turn.promise, promise)

	return nil
}

// ConfirmTurn finishes the turn and returns the opponent's promised data.
func (scg *SecureCardGame) ConfirmTurn(exchangeData ExchangeDataFunc) ([]byte, error) {
	scg.lock.Lock()
	defer scg.lock.Unlock()

	if len(scg.turns) == 0 {
		return nil, errors.New("scnet: init, setDeck, and beginTurn must be called before confirmTurn may be called")
	}

	if scg.finalized {
		return nil, errors.New("scnet: cannot modify turn on finalized game")
	}

	turn := scg.turns[len(scg.turns)-1]
	if turn.promise == nil {
		return nil, errors.New("scnet: promiseTurn must be called before confirmTurn")
	}

	confirmBuf, err := doExchangeData(syncConfirmTurn, len(scg.turns), exchangeData, turn.confirm[scg.player-1])
	if err != nil {
		return nil, err
	}

	if len(confirmBuf) < scg.randLen {
		return nil, errors.New("scnet: received turn confirmation buffer that is too short")
	}

	scg.hash.Reset()
	_, _ = scg.hash.Write(scg.seed[RNGShared])
	_, _ = scg.hash.Write(confirmBuf)
	confirmHash := scg.hash.Sum(nil)

	if !bytes.Equal(confirmHash, turn.promise) {
		return nil, errors.New("scnet: turn confirmation validation failed (implementation error or possible cheating)")
	}

	turn.confirm[2-scg.player] = confirmBuf
	copy(turn.seed2[int(2-scg.player)*scg.randLen:], confirmBuf[:scg.randLen])

	scg.WhenConfirmedTurn(turn.seed2)

	return confirmBuf[scg.randLen:], nil
}

// WhenConfirmedTurn initializes the shared seed for the processing stage of a
// round of Spy Cards Online.
func (scg *SecureCardGame) WhenConfirmedTurn(seed2 []byte) {
	scg.rng[RNGShared] = scg.newRNG(string(scg.seed[RNGShared]) + string(seed2))
}

// Finalize completes and verifies the card game.
func (scg *SecureCardGame) Finalize(exchangeData ExchangeDataFunc, verifyDeck func(card.Deck, card.UnknownDeck) error, verifyTurn func(*scgTurn) error) error {
	scg.lock.Lock()
	defer scg.lock.Unlock()

	if scg.finalized {
		return errors.New("scnet: finalize already called")
	}

	if scg.turns == nil {
		return errors.New("scnet: finalize called on uninitialized SecureCardGame instance")
	}

	scg.finalized = true

	localDeckInitial, err := scg.localDeckInitial.MarshalBinary()
	if err != nil {
		return fmt.Errorf("scnet: finalizing: encoding own deck: %w", err)
	}

	sendData := make([]byte, 0, scg.randLen+len(localDeckInitial))
	sendData = append(sendData, scg.seed[RNGLocal]...)
	sendData = append(sendData, localDeckInitial...)

	recvData, err := doExchangeData(syncFinalize, 0, exchangeData, sendData)
	if err != nil {
		return err
	}

	if len(recvData) < scg.randLen {
		return fmt.Errorf("scnet: finalizing: expected to receive at least %d bytes of data, but received %d bytes", scg.randLen, len(recvData))
	}

	scg.hash.Reset()
	_, _ = scg.hash.Write(scg.seed[RNGShared])
	_, _ = scg.hash.Write(recvData)
	recvHash := scg.hash.Sum(nil)

	if !bytes.Equal(recvHash, scg.remoteInitialHash) {
		return errors.New("scnet: finalizing: handshake hash mismatch (implementation error or possible cheating)")
	}

	scg.seed[RNGRemote] = recvData[:scg.randLen]

	err = scg.remoteDeckInitial.UnmarshalBinary(recvData[scg.randLen:])
	if err != nil {
		return fmt.Errorf("scnet: finalizing: decoding opponent's deck: %w", err)
	}

	if err := verifyDeck(scg.remoteDeckInitial, scg.remoteCardBacks); err != nil {
		return fmt.Errorf("verifying decks: %w", err)
	}

	for i, turn := range scg.turns {
		scg.InitTurnSeed(turn.seed, scg.seed[RNGLocal], scg.seed[RNGRemote])

		if err := verifyTurn(turn); err != nil {
			return fmt.Errorf("verifying round %d: %w", i+1, err)
		}
	}

	return nil
}

func (scg *SecureCardGame) newRNG(seed string) *rng.RNG {
	r := rng.New(scg.newHash, seed)

	if scg.compat != nil && scg.compat[0] == 0 && scg.compat[1] == 2 && scg.compat[2] <= 74 {
		r.SeparateUpdateSeed = scg.seed[RNGShared]
		r.MaxValueFencepost = true
		r.MaxValueShift = true
	}

	return r
}

// InitTurnSeed initializes the random number generator for a turn.
func (scg *SecureCardGame) InitTurnSeed(turnSeed, localSeed, remoteSeed []byte) {
	var buf []byte

	buf = append(buf[:0], scg.seed[RNGShared]...)
	buf = append(buf, turnSeed...)

	scg.rng[RNGShared] = scg.newRNG(string(buf))

	var seedPrefix [32]byte

	if scg.compat != nil && scg.compat[0] == 0 && scg.compat[1] == 2 && scg.compat[2] <= 74 {
		seedPrefix = sha256.Sum256(buf)
		buf = append(seedPrefix[:], localSeed...)
	} else {
		buf = append(buf[:0], scg.seed[RNGShared]...)
		buf = append(buf, turnSeed...)
		buf = append(buf, localSeed...)
	}

	scg.rng[RNGLocal] = scg.newRNG(string(buf))

	if remoteSeed == nil {
		scg.rng[RNGRemote] = nil
	} else {
		if scg.compat != nil && scg.compat[0] == 0 && scg.compat[1] == 2 && scg.compat[2] <= 74 {
			buf = append(seedPrefix[:], remoteSeed...)
		} else {
			buf = append(buf[:0], scg.seed[RNGShared]...)
			buf = append(buf, turnSeed...)
			buf = append(buf, remoteSeed...)
		}

		scg.rng[RNGRemote] = scg.newRNG(string(buf))
	}
}

// Shuffle shuffles an arbitrary set of data.
func (scg *SecureCardGame) Shuffle(shared RNGSharedType, length int, swap func(i, j int)) {
	for i := 1; i < length; i++ {
		j := scg.rng[shared].RangeInt(0, i+1)

		swap(i, j)
	}
}

// Seed returns the seed for the specified random state type.
func (scg *SecureCardGame) Seed(shared RNGSharedType) []byte {
	return scg.seed[shared]
}

// Rand returns the *rng.RNG for the specified random state type.
func (scg *SecureCardGame) Rand(shared RNGSharedType) *rng.RNG {
	return scg.rng[shared]
}
