package scnet

import (
	"bytes"
	"encoding/binary"
	"encoding/hex"
	"encoding/json"
	"log"

	"git.lubar.me/ben/spy-cards/card"
	"git.lubar.me/ben/spy-cards/internal"
	"git.lubar.me/ben/spy-cards/internal/router"
)

const (
	controlVersion        = 'v'
	controlGameQuit       = 'q'
	controlReady          = 'r'
	controlUndoReady      = 'R'
	controlRematch        = 'a'
	controlCustomCards    = 'c'
	controlCosmeticData   = 'o'
	controlSpoilerGuard   = 'g'
	controlInHand         = 'i'
	controlModifyCards    = 'm'
	controlModifyCardsAck = 'M'
	controlPreviewReady   = 't'
)

func (g *Game) onControlMessage(msg rtcDataChannelMessage) {
	if router.FlagRTCSpew.IsSet() {
		log.Println("DEBUG: recv on channel control", hex.Dump(msg.Data))
	}

	if len(msg.Data) == 0 {
		log.Println("WARNING: empty control message")

		return
	}

	g.simulateNetworkConditions()

	switch msg.Data[0] {
	case controlVersion:
		g.lock.Lock()

		i := bytes.IndexByte(msg.Data, '-')
		if i != -1 && g.Perspective == Player2 {
			*g.mode = string(msg.Data[i+1:])
		}

		g.lock.Unlock()

		if i == -1 {
			i = len(msg.Data)
		}

		if !bytes.Equal(msg.Data[1:i], internal.VersionNumber()) {
			log.Println("WARNING: version mismatch", string(msg.Data[1:i]), " != ", string(internal.VersionNumber()))
		}
	case controlCustomCards:
		if g.Perspective != Player2 {
			log.Println("WARNING: discarding custom cards packet: not player 2")

			return
		}

		g.lock.Lock()

		if g.receivedCards {
			log.Println("WARNING: discarding custom cards packet: already received this session")
		} else {
			g.receivedCards = true

			if g.Cards == nil {
				g.Cards = &card.Set{}
			}

			err := g.Cards.UnmarshalText(msg.Data[1:])
			if err != nil {
				log.Println("ERROR: received malformed custom card data:", err)
			}

			g.cond.Broadcast()
		}

		g.lock.Unlock()
	case controlCosmeticData:
		cosmetic := &card.CosmeticData{}

		err := json.Unmarshal(msg.Data[1:], cosmetic)
		if err != nil {
			log.Println("WARNING: cosmetic data parsing failed:", err)

			return
		}

		g.lock.Lock()
		g.remoteCosmetic = cosmetic
		g.cond.Broadcast()
		g.lock.Unlock()
	case controlSpoilerGuard:
		g.lock.Lock()

		if g.receivedSpoilerGuard {
			log.Println("WARNING: discarding spoiler guard data: already received this session")
		} else {
			g.receivedSpoilerGuard = true

			g.remoteSpoilerGuard = msg.Data[1:]

			g.cond.Broadcast()
		}

		g.lock.Unlock()
	case controlReady:
		if g.recvReady {
			log.Println("ERROR: duplicate controlReady message from opponent")

			return
		}

		err := g.SCG.PromiseTurn(msg.Data[1:])
		if err != nil {
			log.Println("ERROR: receiving opponent ready message:", err)
		}

		g.setReady(&g.recvReady)
	case controlUndoReady:
		switch msg.Data[1] {
		case 0: // request
			g.lock.Lock()
			if g.recvReady {
				if err := g.sendOnChannel(&g.control, []byte{controlUndoReady, 1}); err != nil {
					log.Println("ERROR: sending ack for opponent's controlUndoReady")
				}

				g.recvReady = false
			} else {
				log.Println("DEBUG: ignoring controlUndoReady request from opponent")
			}
			g.lock.Unlock()
		case 1: // ack
			g.lock.Lock()
			if g.wantUndoReady {
				g.wantUndoReady = false
				g.sentReady = false
			} else {
				log.Println("DEBUG: ignoring controlUndoReady ack from opponent")
			}
			g.lock.Unlock()
		}
	case controlGameQuit:
		g.lock.Lock()
		g.opponentQuit = true
		g.cond.Broadcast()
		g.lock.Unlock()
	case controlRematch:
		g.lock.Lock()

		if g.sentRematch && !g.recvRematch {
			g.doRematch()
		}

		g.recvRematch = true
		g.cond.Broadcast()
		g.lock.Unlock()
	case controlInHand:
		g.recvInHand <- msg.Data[1:]
	case controlModifyCards:
		g.recvModifyCards <- msg.Data[1:]
	case controlModifyCardsAck:
		g.recvModifyCardsAck <- struct{}{}
	case controlPreviewReady:
		g.lock.Lock()
		g.previewReady = binary.LittleEndian.Uint64(msg.Data[1:])
		g.lock.Unlock()
	default:
		panic("unhandled control data channel message type " + string(msg.Data[0]))
	}
}
