package scnet

import (
	"encoding/binary"
	"encoding/hex"
	"fmt"
	"log"
	"time"

	"git.lubar.me/ben/spy-cards/internal/router"
)

// Network code testing variables.
var (
	FakeDrop    float64
	FakeLatency time.Duration
	FakeJitter  time.Duration
)

const (
	unreliablePing   = 'p'
	unreliablePong   = 'o'
	unreliableInputs = 'i'
)

func (g *Game) onUnreliableMessage(msg rtcDataChannelMessage) {
	if router.FlagRTCSpew.IsSet() {
		log.Println("DEBUG: recv on channel unrel", hex.Dump(msg.Data))
	}

	if len(msg.Data) == 0 {
		log.Println("WARNING: empty unreliable message")

		return
	}

	if g.simulateNetworkConditions() {
		return
	}

	switch msg.Data[0] {
	case unreliablePing:
		if len(msg.Data) != 9 {
			log.Println("WARNING: unexpected length for ping packet:", len(msg.Data))

			return
		}

		msg.Data[0] = unreliablePong

		err := g.sendOnChannel(&g.unrel, msg.Data)
		if err != nil {
			log.Println("WARNING: replying to ping:", err)
		}
	case unreliablePong:
		if len(msg.Data) != 9 {
			log.Println("WARNING: unexpected length for pong packet:", len(msg.Data))

			return
		}

		select {
		case g.pong <- binary.LittleEndian.Uint64(msg.Data[1:]):
		default:
			log.Println("WARNING: dropping pong")
		}
	case unreliableInputs:
		b := make([]byte, len(msg.Data)-1)
		copy(b, msg.Data[1:])

		g.lock.Lock()
		g.unrelInputs = b
		g.lock.Unlock()
	default:
		panic("unhandled unreliable data channel message type " + string(msg.Data[0]))
	}
}

func (g *Game) pinger() {
	for g.unrel.ReadyState() == rtcDataChannelStateConnecting {
		time.Sleep(time.Second / 10)
	}

	for {
		t := time.Now().UnixNano()

		var msg [9]byte

		msg[0] = 'p'
		binary.LittleEndian.PutUint64(msg[1:], uint64(t))

		if err := g.sendOnChannel(&g.unrel, msg[:]); err != nil {
			log.Println("ERROR: ping send failed:", err)

			return
		}

		wait := time.After(5 * time.Second)

		select {
		case p := <-g.pong:
			if t != int64(p) {
				log.Printf("WARNING: pong packet did not match ping (%016x != %016x)", p, t)
			} else {
				g.lock.Lock()
				g.lastPing = time.Duration(time.Now().UnixNano() - int64(p))
				g.lock.Unlock()
			}
		case <-time.After(5 * time.Second):
			log.Println("WARNING: no reply to ping")
			log.Println("DEBUG: buffer length", g.unrel.BufferedAmount())

			g.lock.Lock()
			g.lastPing = g.lastPing.Truncate(5*time.Second) + 5*time.Second
			g.lock.Unlock()
		}

		for {
			select {
			case p := <-g.pong:
				log.Printf("WARNING: spurious pong packet: %016x", p)

				continue
			case <-wait:
			}

			break
		}
	}
}

func (g *Game) SendQ() uint64 {
	return g.unrel.BufferedAmount()
}

func (g *Game) LastPing() time.Duration {
	g.lock.Lock()
	ping := g.lastPing
	g.lock.Unlock()

	return ping
}

func (g *Game) RecvInputs() []byte {
	g.lock.Lock()

	inputs := g.unrelInputs
	g.unrelInputs = nil

	g.lock.Unlock()

	return inputs
}

func (g *Game) SendInputs(b []byte) error {
	if g.unrel.BufferedAmount() > 1<<15 {
		// more than 32k of buffered packets; drop
		return nil
	}

	err := g.sendOnChannel(&g.unrel, append([]byte{unreliableInputs}, b...))
	if err != nil {
		return fmt.Errorf("scnet: sending input data to opponent: %w", err)
	}

	return nil
}
