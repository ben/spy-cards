package scnet

import (
	"context"
	"fmt"
	"log"
	"sync"
	"time"

	"git.lubar.me/ben/spy-cards/internal"
)

// Signal is a connection to the signaling server.
type Signal struct {
	ctx          context.Context
	conn         wsConn
	closeCode    int
	closeReason  string
	mock         *mockSignal
	game         *Game
	perspective  Perspective
	didHandshake bool
	wantClose    bool
	havePinger   bool
	lan          bool
	session      string
	suffix       string
	description  rawMessage
	candidates   []rawMessage
	lock         sync.Mutex
	cond         *sync.Cond
}

type mockSignal struct {
	partner *Signal
	primary bool
}

func NewSignal(ctx context.Context) (*Signal, error) {
	s := &Signal{ctx: ctx}
	s.cond = sync.NewCond(&s.lock)

	return s, nil
}

func MockSignal(ctx context.Context) (*Signal, *Signal) {
	s1 := &Signal{ctx: ctx, mock: &mockSignal{primary: true}}
	s2 := &Signal{ctx: ctx, mock: &mockSignal{primary: false}}
	s1.cond = sync.NewCond(&s1.lock)
	s2.cond = sync.NewCond(&s2.lock)
	s1.mock.partner, s2.mock.partner = s2, s1

	return s1, s2
}

func (s *Signal) IsClosed() (int, string) {
	return s.closeCode, s.closeReason
}

func (s *Signal) ensureConn() error {
	if s.mock != nil {
		panic("scnet: internal error: ensureConn should not have been called on mock connection")
	}

	s.lock.Lock()
	if s.haveConn() {
		s.lock.Unlock()

		return nil
	}

	s.wantClose = false

	retries := 5

	conn, retry, err := s.dial()

	for retry && retries > 0 {
		log.Println("ERROR: signal connection failed:", err)

		retries--

		time.Sleep(time.Second)

		conn, retry, err = s.dial()
	}

	if err != nil {
		s.lock.Unlock()

		return fmt.Errorf("scnet: connecting to matchmaking server: %w", err)
	}

	conn.SetReadLimit(1 << 20)

	if proto := conn.Subprotocol(); proto != "match" {
		log.Println("WARNING: unexpected signaling connection subprotocol:", proto)
	}

	if !s.havePinger {
		s.havePinger = true
		go s.pinger()
	}

	go s.reader(conn)

	s.closeCode = 0
	s.closeReason = ""
	s.conn = conn

	s.lock.Unlock()

	if s.session != "" {
		err = s.writeText([]byte{signalHandshake, 'r'})
		if err != nil {
			log.Println("ERROR: setting up reconnection attempt:", err)
		}

		err = s.writeText(append([]byte{signalSessionID}, s.session...))
		if err != nil {
			log.Println("ERROR: setting up reconnection attempt:", err)
		}

		s.didHandshake = false
	}

	return nil
}

func (s *Signal) pinger() {
	for {
		time.Sleep(15 * time.Second)

		s.lock.Lock()

		if !s.haveConn() {
			if s.wantClose {
				s.lock.Unlock()

				return
			}

			s.lock.Unlock()

			continue
		}

		err := s.writeText([]byte{signalKeepAlive})
		if err != nil {
			log.Println("WARNING: failed to send signal connection ping:", err)
		}

		s.lock.Unlock()
	}
}

func (s *Signal) Close() error {
	s.lock.Lock()
	s.wantClose = true

	var err error

	s.closeCode = internal.NormalClosure
	s.closeReason = "matchmaking connection no longer needed"

	if s.haveConn() {
		err = s.writeText([]byte{signalTerminate})

		if err == nil {
			err = s.conn.Close(internal.NormalClosure, "matchmaking connection no longer needed")
		}
	}

	s.lock.Unlock()

	if err != nil {
		return fmt.Errorf("scnet: closing signal connection: %w", err)
	}

	return nil
}

func (s *Signal) reconnect() {
	s.lock.Lock()
	if s.wantClose {
		s.lock.Unlock()

		return
	}
	s.lock.Unlock()

	time.Sleep(time.Second)

	if err := s.ensureConn(); err != nil {
		log.Println("WARNING: failed to reconnect to matchmaking server:", err)
	}
}
