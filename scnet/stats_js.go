//go:build js && wasm
// +build js,wasm

package scnet

import (
	"fmt"
	"log"
	"net"
	"strconv"
	"syscall/js"
	"time"

	"git.lubar.me/ben/spy-cards/internal"
	"git.lubar.me/ben/spy-cards/internal/router"
)

var statsEl = js.Global().Get("document").Call("createElement", "pre")

func (g *Game) statsWatcher() {
	if !router.FlagRTCStats.IsSet() {
		return
	}

	statsEl.Get("style").Set("position", "fixed")
	statsEl.Get("style").Set("top", "10vh")
	statsEl.Get("style").Set("textAlign", "left")
	statsEl.Get("style").Set("color", "#fff")
	statsEl.Get("style").Set("backgroundColor", "#000c")
	statsEl.Get("style").Set("pointerEvents", "none")
	statsEl.Get("style").Set("userSelect", "none")
	js.Global().Get("document").Get("body").Call("appendChild", statsEl)

	var (
		buf           []byte
		channelBuf    []byte
		connectionBuf []byte

		channelBytes [3]uint64
	)

	for {
		stats, err := internal.AwaitNoRandomFail(g.conn.conn.Call("getStats", js.Null()))
		if err != nil {
			log.Println("ERROR: updating connection stats:", err)

			return
		}

		buf = buf[:0]
		channelBuf = channelBuf[:0]
		connectionBuf = connectionBuf[:0]

		js.Global().Set("rtcStats", stats)

		iter := stats.Call("values")

		for {
			it := iter.Call("next")
			if it.Get("done").Bool() {
				break
			}

			val := it.Get("value")

			switch typ := val.Get("type").String(); typ {
			case "peer-connection", "transport", "certificate", "media-playout":
				// not used
			case "data-channel":
				var id int

				if identifier := val.Get("dataChannelIdentifier"); !identifier.IsUndefined() {
					// standard
					id = identifier.Int()
				} else if identifier = val.Get("datachannelid"); !identifier.IsUndefined() {
					// safari
					id = identifier.Int()
				}

				totalBytes := uint64(val.Get("bytesSent").Int()) + uint64(val.Get("bytesReceived").Int())
				deltaBytes := totalBytes - channelBytes[id]
				channelBytes[id] = totalBytes

				channelBuf = append(channelBuf, "channel "...)
				channelBuf = strconv.AppendInt(channelBuf, int64(id), 10)
				channelBuf = append(channelBuf, ':')
				channelBuf = append(channelBuf, val.Get("label").String()...)
				channelBuf = append(channelBuf, ' ')
				channelBuf = append(channelBuf, val.Get("state").String()...)
				channelBuf = append(channelBuf, " b:"...)
				channelBuf = strconv.AppendInt(channelBuf, int64(val.Get("bytesSent").Int()), 10)
				channelBuf = append(channelBuf, '/')
				channelBuf = strconv.AppendInt(channelBuf, int64(val.Get("bytesReceived").Int()), 10)
				channelBuf = append(channelBuf, " m:"...)
				channelBuf = strconv.AppendInt(channelBuf, int64(val.Get("messagesSent").Int()), 10)
				channelBuf = append(channelBuf, '/')
				channelBuf = strconv.AppendInt(channelBuf, int64(val.Get("messagesReceived").Int()), 10)
				channelBuf = append(channelBuf, " delta:"...)
				channelBuf = strconv.AppendUint(channelBuf, deltaBytes, 10)
				channelBuf = append(channelBuf, '\n')
			case "local-candidate", "remote-candidate":
				// not used directly (used indirectly by candidate-pair)
			case "candidate-pair":
				local := stats.Call("get", val.Get("localCandidateId"))
				remote := stats.Call("get", val.Get("remoteCandidateId"))

				connectionBuf = append(connectionBuf, "\nconnection "...)

				if !val.Get("nominated").Bool() {
					connectionBuf = append(connectionBuf, "candidate "...)
				}

				connectionBuf = append(connectionBuf, val.Get("state").String()...)
				connectionBuf = append(connectionBuf, " b:"...)
				connectionBuf = strconv.AppendInt(connectionBuf, int64(val.Get("bytesSent").Int()), 10)
				connectionBuf = append(connectionBuf, '/')
				connectionBuf = strconv.AppendInt(connectionBuf, int64(val.Get("bytesReceived").Int()), 10)
				connectionBuf = append(connectionBuf, " rtt:"...)

				if rtt := val.Get("currentRoundTripTime"); rtt.IsUndefined() {
					connectionBuf = append(connectionBuf, '?')
				} else {
					connectionBuf = strconv.AppendFloat(connectionBuf, rtt.Float(), 'f', 3, 64)
				}

				connectionBuf = append(connectionBuf, '/')

				if rtt := val.Get("totalRoundTripTime"); rtt.IsUndefined() {
					connectionBuf = append(connectionBuf, '?')
				} else {
					connectionBuf = strconv.AppendFloat(connectionBuf, rtt.Float(), 'f', 3, 64)
				}

				if val.Get("requestsSent").IsUndefined() {
					connectionBuf = append(connectionBuf, " req:?+?/?"...)
				} else {
					connectionBuf = append(connectionBuf, " req:"...)
					connectionBuf = strconv.AppendInt(connectionBuf, int64(val.Get("requestsSent").Int()), 10)
					connectionBuf = append(connectionBuf, '+')
					connectionBuf = strconv.AppendInt(connectionBuf, int64(val.Get("consentRequestsSent").Int()), 10)
					connectionBuf = append(connectionBuf, '/')
					connectionBuf = strconv.AppendInt(connectionBuf, int64(val.Get("requestsReceived").Int()), 10)
				}

				if val.Get("responsesSent").IsUndefined() {
					connectionBuf = append(connectionBuf, " resp:?/?"...)
				} else {
					connectionBuf = append(connectionBuf, " resp:"...)
					connectionBuf = strconv.AppendInt(connectionBuf, int64(val.Get("responsesSent").Int()), 10)
					connectionBuf = append(connectionBuf, '/')
					connectionBuf = strconv.AppendInt(connectionBuf, int64(val.Get("responsesReceived").Int()), 10)
				}

				connectionBuf = append(connectionBuf, '\n')
				connectionBuf = append(connectionBuf, describeCandidate(local)...)
				connectionBuf = append(connectionBuf, '\n')
				connectionBuf = append(connectionBuf, describeCandidate(remote)...)
				connectionBuf = append(connectionBuf, '\n')
			default:
				log.Println("TODO: RTCStats type", typ)
			}
		}

		buf = append(buf, channelBuf...)
		buf = append(buf, connectionBuf...)
		buf = append(buf, "\nstate signal:"...)
		buf = append(buf, g.conn.SignalingState()...)
		buf = append(buf, " conn:"...)

		if g.conn.ConnectionState() == "unknown" {
			buf = append(buf, g.conn.ICEConnectionState()...)
		} else {
			buf = append(buf, g.conn.ConnectionState()...)
		}

		buf = append(buf, " gather:"...)
		buf = append(buf, g.conn.ICEGatheringState()...)

		buf = append(buf, "\nlast ping: "...)
		buf = append(buf, g.lastPing.String()...)

		statsEl.Set("textContent", string(buf))

		time.Sleep(time.Second)
	}
}

func describeCandidate(v js.Value) string {
	return fmt.Sprintf("%s (%s, %s) p=%d",
		net.JoinHostPort(v.Get("address").String(), strconv.Itoa(v.Get("port").Int())),
		v.Get("protocol").String(),
		candidateTypeName(v.Get("candidateType").String()),
		v.Get("priority").Int(),
	)
}

func candidateTypeName(s string) string {
	switch s {
	case "host":
		return "local (direct)"
	case "prflx":
		return "peer (direct)"
	case "srflx":
		return "STUN (direct)"
	case "relay":
		return "TURN (relay)"
	default:
		return s
	}
}
