//go:build js && wasm && !headless
// +build js,wasm,!headless

package audio

import (
	"fmt"
	"log"
	"math"
	"strings"
	"sync"
	"sync/atomic"
	"syscall/js"
	"time"

	"git.lubar.me/ben/spy-cards/internal"
	"git.lubar.me/ben/spy-cards/internal/router"
)

var (
	actx           js.Value
	musicGain      js.Value
	musicFFT       js.Value
	soundsGain     js.Value
	softDecodeOpus bool

	onFetchEnd = sync.NewCond(&sync.Mutex{})

	suspendedWarning = func() js.Value {
		el := js.Global().Get("document").Call("createElement", "div")
		el.Get("style").Set("display", "none")

		return el
	}()

	globalLock sync.Mutex
)

func implInit() {
	noCleanup := false

	if ac := js.Global().Get("SpyCards").Get("Audio").Get("actx"); ac.Truthy() {
		noCleanup = true
		actx = ac
	} else if ac := js.Global().Get("AudioContext"); ac.Truthy() {
		actx = ac.New()
	} else if ac := js.Global().Get("webkitAudioContext"); ac.Truthy() {
		actx = ac.New()
	}

	if !actx.Truthy() || math.IsNaN(actx.Get("currentTime").Float()) {
		// give up. just give up.
		actx = js.Null()

		return
	}

	if !noCleanup {
		js.Global().Get("SpyCards").Get("crashCleanup").Call("push", actx.Get("close").Call("bind", actx))
	}

	if actx.Get("createGain").Type() == js.TypeFunction && actx.Get("createAnalyser").Type() == js.TypeFunction {
		musicGain = actx.Call("createGain")
		musicGain.Call("connect", actx.Get("destination"))
		musicFFT = actx.Call("createAnalyser")
		musicFFT.Set("smoothingTimeConstant", 0)
		musicFFT.Call("connect", musicGain)
		soundsGain = actx.Call("createGain")
		soundsGain.Call("connect", actx.Get("destination"))
	} else {
		actx = js.Undefined()
	}

	time.AfterFunc(1*time.Second, func() {
		if !actx.Truthy() || actx.Get("state").String() == "running" {
			return
		}

		suspendedWarning.Set("className", "update-available audiocontext-warning")
		suspendedWarning.Get("style").Set("pointerEvents", "none")
		suspendedWarning.Set("innerHTML", "<p>Your web browser isn't allowing<br>Spy Cards Online to decode audio files.<br>Try clicking anywhere the page.</p>")
		js.Global().Get("document").Get("body").Call("appendChild", suspendedWarning)

		_, err := internal.AwaitNoRandomFail(actx.Call("resume"))
		log.Println("DEBUG: actx", err)

		defer func() {
			// not sure why the removeChild sometimes fails, but just ignore it and move on
			_ = recover()
		}()

		js.Global().Get("document").Get("body").Call("removeChild", suspendedWarning)
	})

	capabilities := js.Global().Get("navigator").Get("mediaCapabilities")
	if !capabilities.Truthy() {
		log.Println("WARNING: unable to test whether browser supports ogg/opus audio (missing navigator.mediaCapabilities API; assuming no support)")

		softDecodeOpus = true

		return
	}

	if router.FlagForceSoftOpus.IsSet() {
		softDecodeOpus = true

		return
	}

	canOpus, err := internal.AwaitNoRandomFail(capabilities.Call("decodingInfo", map[string]interface{}{
		"type": "file",
		"audio": map[string]interface{}{
			"contentType": "audio/ogg;codecs=opus",
		},
	}))
	if err != nil {
		log.Println("ERROR: while testing ogg/opus audio decoding capability:", err)

		return
	}

	if !canOpus.Get("supported").Bool() {
		log.Println("INFO: using software decoding of opus (not supported natively by this browser)")

		softDecodeOpus = true
	}
}

// UnsuspendContext requests that the browser audio context resume,
// which browsers may prevent until user input.
func UnsuspendContext() {
	if actx.Truthy() && actx.Get("state").String() == "suspended" {
		actx.Call("resume")
	}
}

const (
	fetchNone     = 0
	fetchComplete = 1
	fetchActive   = 2
	fetchFailed   = 3
)

type implData struct {
	initOnce   sync.Once
	fetchState int32
	ready      chan struct{}
	loadErr    error
	buffer     js.Value
	gain       js.Value
	musicGain  js.Value
}

func (s *Sound) init() {
	audioSettingsOnce.Do(initAudioSettings)

	if !actx.Truthy() {
		return
	}

	s.impl.ready = make(chan struct{})

	if s.Volume == 0 {
		s.Volume = 1
	}

	if s.Pitch == 0 {
		s.Pitch = 1
	}

	if s.MaxDelay == 0 {
		s.MaxDelay = 1.5
	}
}

func (s *Sound) maybeFetch() {
	s.impl.initOnce.Do(s.init)

	if !actx.Truthy() {
		return
	}

	if SoundsVolume == 0 && MusicVolume == 0 {
		return
	}

	switch state := atomic.LoadInt32(&s.impl.fetchState); state {
	case fetchComplete, fetchActive:
		// nothing to do
	case fetchNone, fetchFailed:
		if atomic.CompareAndSwapInt32(&s.impl.fetchState, state, fetchActive) {
			go s.doFetch()
		}
	}
}

func (s *Sound) doFetch() {
	url := s.Name

	if !strings.Contains(url, "/") {
		url = "/audio/" + url + ".opus"
	}

	var (
		buf js.Value
		err error
	)

	native := js.Global().Get("SpyCards").Get("Native")

	if promise := native.Get("preloadedAudio").Get(url); promise.Truthy() {
		native.Get("preloadedAudio").Delete(url)

		buf, err = internal.Await(promise)
		if err == nil {
			goto done
		}
	}

	if promise := native.Get("preloadedGeneric").Get(url); promise.Truthy() {
		native.Get("preloadedGeneric").Delete(url)

		buf, _ = internal.Await(promise)
	}

	if !buf.Truthy() {
		buf, err = internal.Await(internal.Fetch.Invoke(url, internal.FetchOpts))
	}

	if err == nil {
		buf, err = internal.Await(buf.Call("arrayBuffer"))
	}

	if err == nil {
		switch {
		case softDecodeOpus:
			buf, err = opusWorker(s.Name, buf)
		case actx.Get("decodeAudioData").Length() == 1 && !router.FlagForceSafariCompat.IsSet():
			buf, err = internal.Await(actx.Call("decodeAudioData", buf))
		default:
			// Safari compat
			var f js.Func

			f = js.FuncOf(func(_ js.Value, args []js.Value) interface{} {
				f.Release()

				reject := internal.Function.New("reject", `return function(err) { reject(err || new Error("no error provided by browser")); }`).Invoke(args[1])

				actx.Call("decodeAudioData", buf, args[0], reject)

				return js.Undefined()
			})

			buf, err = internal.Await(internal.Promise.New(f))
		}
	}

done:
	if err != nil {
		log.Printf("ERROR: failed to fetch audio %q: %+v", s.Name, err)

		onFetchEnd.L.Lock()

		s.impl.loadErr = err
		atomic.StoreInt32(&s.impl.fetchState, fetchFailed)

		onFetchEnd.Broadcast()
		onFetchEnd.L.Unlock()

		return
	}

	onFetchEnd.L.Lock()

	s.impl.loadErr = nil
	s.impl.buffer = buf
	atomic.StoreInt32(&s.impl.fetchState, fetchComplete)
	close(s.impl.ready)

	onFetchEnd.Broadcast()
	onFetchEnd.L.Unlock()
}

// Preload loads the Sound and returns the error encountered, if any.
func (s *Sound) Preload() error {
	s.maybeFetch()

	onFetchEnd.L.Lock()

	for atomic.LoadInt32(&s.impl.fetchState) == fetchActive {
		onFetchEnd.Wait()
	}

	err := s.impl.loadErr

	onFetchEnd.L.Unlock()

	return err
}

// StartPreload creates a new goroutine to preload the sound.
func (s *Sound) StartPreload() {
	s.impl.initOnce.Do(s.init)

	globalLock.Lock()

	anySound := SoundsVolume != 0 || MusicVolume != 0

	globalLock.Unlock()

	if anySound {
		s.maybeFetch()
	}
}

func (s *Sound) createSource(skip float64) js.Value {
	buffer := s.impl.buffer

	if skip != 0 {
		l := buffer.Get("length").Int()
		sr := buffer.Get("sampleRate").Int()
		channels := buffer.Get("numberOfChannels").Int()
		offset := int(float64(sr) * skip)

		if l <= offset {
			return js.Null()
		}

		partBuf := actx.Call("createBuffer", channels, l-offset, sr)

		for i := 0; i < channels; i++ {
			partBuf.Call("getChannelData", i).Call("set", buffer.Call("getChannelData", i).Call("subarray", offset))
		}

		buffer = partBuf
	}

	source := actx.Call("createBufferSource")
	source.Set("buffer", buffer)
	source.Get("playbackRate").Call("setValueAtTime", s.Pitch, actx.Get("currentTime"))

	if s.Loop {
		source.Set("loop", true)
		source.Set("loopStart", s.Start)
		source.Set("loopEnd", s.End)
	}

	return source
}

// PlaySound plays a sound, optionally modifying its pitch and volume.
func (s *Sound) PlaySound(delay, overridePitch, overrideVolume float64, x, y, z float32) {
	suspendedWarning.Get("style").Set("display", "")
	s.impl.initOnce.Do(s.init)

	if !actx.Truthy() {
		return
	}

	if SoundsVolume == 0 {
		return
	}

	if IgnoreAudio > 0 {
		return
	}

	negDelay := 0.0
	if delay < 0 {
		negDelay = -delay
		delay = 0
	}

	curTime := actx.Get("currentTime").Float()
	startTime := curTime + delay

	select {
	case <-s.impl.ready:
		s.playSound(startTime, negDelay, overridePitch, overrideVolume, x, y, z)
	default:
		s.maybeFetch()

		go func() {
			select {
			case <-s.impl.ready:
				curTime = actx.Get("currentTime").Float()
				if startTime < curTime {
					log.Println("WARNING: sound loading delayed", s.Name, "by", time.Duration((curTime-startTime)*float64(time.Second)))
					startTime = curTime
				}

				s.playSound(startTime, negDelay, overridePitch, overrideVolume, x, y, z)
			case <-time.After(time.Duration((delay + s.MaxDelay) * float64(time.Second))):
				log.Println("WARNING: discarding sound: exceeded load deadline:", s.Name)
			}
		}()
	}
}

func (s *Sound) playSound(start, skip, overridePitch, overrideVolume float64, x, y, z float32) {
	src := s.createSource(skip)
	if !src.Truthy() {
		return
	}

	dest := soundsGain
	ct := actx.Get("currentTime")

	if s.Volume != 1 {
		if !s.impl.gain.Truthy() {
			s.impl.gain = actx.Call("createGain")
			s.impl.gain.Call("connect", dest)
			s.impl.gain.Get("gain").Call("setValueAtTime", s.Volume, ct)
		}

		dest = s.impl.gain
	}

	if overrideVolume != 0 {
		gain := actx.Call("createGain")
		gain.Call("connect", dest)
		gain.Get("gain").Call("setValueAtTime", overrideVolume, ct)
		dest = gain
	}

	if overridePitch != 0 {
		src.Get("playbackRate").Call("setValueAtTime", overridePitch*s.Pitch, ct)
	}

	if !math.IsNaN(float64(x)) {
		panner := actx.Call("createPanner")

		panner.Set("refDistance", 100)
		panner.Set("coneInnerAngle", 360)

		if panner.Get("positionX").Truthy() {
			panner.Get("positionX").Call("setValueAtTime", x, ct)
			panner.Get("positionY").Call("setValueAtTime", y, ct)
			panner.Get("positionZ").Call("setValueAtTime", z, ct)
		} else {
			panner.Call("setPosition", x, y, z)
		}

		panner.Call("connect", dest)

		dest = panner
	}

	src.Call("connect", dest)
	src.Call("start", start)
}

// PlayMusic plays a sound as the current music track.
func (s *Sound) PlayMusic(delay float64, restart bool) {
	suspendedWarning.Get("style").Set("display", "")
	s.impl.initOnce.Do(s.init)

	if !actx.Truthy() {
		return
	}

	globalLock.Lock()

	if IgnoreAudio > 0 {
		globalLock.Unlock()

		return
	}

	if activeSong == s && activeMusic.Truthy() && !restart {
		globalLock.Unlock()

		return
	}

	if activeMusic.Truthy() {
		activeMusic.Call("stop")
	}

	activeMusic = js.Null()
	activeSong = s

	curTime := actx.Get("currentTime").Float()
	startTime := curTime + delay

	dest := musicFFT

	if s.Volume != 1 {
		if !s.impl.musicGain.Truthy() {
			s.impl.musicGain = actx.Call("createGain")
			s.impl.musicGain.Call("connect", dest)
			s.impl.musicGain.Get("gain").Call("setValueAtTime", s.Volume, curTime)
		}

		dest = s.impl.musicGain
	}

	select {
	case <-s.impl.ready:
		src := s.createSource(0)

		src.Call("connect", dest)
		src.Call("start", startTime)

		activeMusic = src
	default:
		s.maybeFetch()

		go func() {
			<-s.impl.ready

			globalLock.Lock()

			if activeSong != s || activeMusic.Truthy() {
				globalLock.Unlock()

				log.Println("WARNING: discarding music: replaced while loading:", s.Name)

				return
			}

			curTime = actx.Get("currentTime").Float()
			if startTime < curTime {
				log.Println("WARNING: music loading delayed", s.Name, "by", time.Duration((curTime-startTime)*float64(time.Second)))
				startTime = curTime
			}

			src := s.createSource(0)

			src.Call("connect", dest)
			src.Call("start", startTime)

			activeMusic = src

			globalLock.Unlock()
		}()
	}

	globalLock.Unlock()
}

var (
	activeMusic js.Value
	activeSong  *Sound
)

// StopMusic stops the current music track.
func StopMusic() {
	globalLock.Lock()

	if activeMusic.Truthy() {
		activeMusic.Call("stop")
	}

	activeMusic = js.Null()
	activeSong = nil

	globalLock.Unlock()
}

// StopSounds stops all active sounds.
func StopSounds() {
	if !actx.Truthy() {
		return
	}

	globalLock.Lock()

	// just throw away the entire gain node graph
	soundsGain.Call("disconnect", actx.Get("destination"))

	soundsGain = actx.Call("createGain")
	soundsGain.Call("connect", actx.Get("destination"))
	soundsGain.Get("gain").Call("setValueAtTime", SoundsVolume, actx.Get("currentTime"))

	globalLock.Unlock()
}

func onVolumeChanged() {
	if !actx.Truthy() {
		return
	}

	globalLock.Lock()

	musicGain.Get("gain").Call("setValueAtTime", MusicVolume, actx.Get("currentTime"))
	soundsGain.Get("gain").Call("setValueAtTime", SoundsVolume, actx.Get("currentTime"))

	globalLock.Unlock()
}

func MusicVolumeTemp(scale float64) {
	if !actx.Truthy() {
		return
	}

	globalLock.Lock()

	musicGain.Get("gain").Call("setValueAtTime", MusicVolume*scale, actx.Get("currentTime"))

	globalLock.Unlock()
}

var (
	musicFFTBuf   []uint8
	musicFFTArray js.Value
)

func MusicFFT() []uint8 {
	audioSettingsOnce.Do(initAudioSettings)

	if !actx.Truthy() || MusicVolume == 0 {
		return nil
	}

	if musicFFTBuf == nil {
		binCount := musicFFT.Get("frequencyBinCount").Int()
		musicFFTBuf = make([]uint8, binCount)
		musicFFTArray = internal.Uint8Array.New(binCount)
	}

	musicFFT.Call("getByteFrequencyData", musicFFTArray)
	js.CopyBytesToGo(musicFFTBuf, musicFFTArray)

	return musicFFTBuf
}

func IsCurrentMusicLoading() bool {
	if activeSong == nil {
		return false
	}

	return atomic.LoadInt32(&activeSong.impl.fetchState) == fetchActive
}

var (
	opusWorkerInstance    js.Value
	opusWorkerRequests    = make(map[int]chan<- js.Value)
	nextOpusWorkerRequest int
)

func opusWorker(name string, buf js.Value) (js.Value, error) {
	if !opusWorkerInstance.Truthy() {
		opusWorkerInstance = internal.Worker.New("/script/opus-decoder-worker.js", map[string]interface{}{
			"name": "opus-decoder",
		})

		opusWorkerInstance.Call("addEventListener", "message", js.FuncOf(func(_ js.Value, args []js.Value) interface{} {
			data := args[0].Get("data")
			id := data.Get("id").Int()

			ch, ok := opusWorkerRequests[id]
			if !ok {
				log.Println("ERROR: no matching opus decoding request for ID", id)

				return js.Undefined()
			}

			delete(opusWorkerRequests, id)
			ch <- data

			return js.Undefined()
		}))
	}

	id := nextOpusWorkerRequest
	nextOpusWorkerRequest++

	ch := make(chan js.Value, 1)
	opusWorkerRequests[id] = ch

	opusWorkerInstance.Call("postMessage", map[string]interface{}{
		"id":     id,
		"name":   name,
		"buffer": buf,
	}, []interface{}{buf})

	response := <-ch
	if config := response.Get("config"); config.Truthy() {
		abuf := actx.Call("createBuffer", config.Get("numberOfChannels"), config.Get("length"), config.Get("sampleRate"))

		for i := 0; i < response.Get("channels").Length(); i++ {
			abuf.Call("getChannelData", i).Call("set", internal.Float32Array.New(response.Get("channels").Index(i), 0), 0)
			internal.TryToDetachBuffer.Invoke(response.Get("channels").Index(i))
		}

		return abuf, nil
	}

	if errno := response.Get("error"); errno.Truthy() {
		return js.Undefined(), fmt.Errorf("audio: opusfile decoding error %d", -errno.Int())
	}

	return js.Undefined(), fmt.Errorf("audio: unknown error")
}

var setListenerPosition = internal.Function.New("", `return function setListenerPosition(actx, x, y, z, fx, fy, fz, ux, uy, uz) {
	const ct = actx.currentTime;

	if (actx.listener.forwardX) {
		actx.listener.forwardX.setValueAtTime(fx, ct);
		actx.listener.forwardY.setValueAtTime(fy, ct);
		actx.listener.forwardZ.setValueAtTime(fz, ct);
		actx.listener.upX.setValueAtTime(ux, ct);
		actx.listener.upY.setValueAtTime(uy, ct);
		actx.listener.upZ.setValueAtTime(uz, ct);
		actx.listener.positionX.setValueAtTime(x, ct);
		actx.listener.positionY.setValueAtTime(y, ct);
		actx.listener.positionZ.setValueAtTime(z, ct);
	} else {
		actx.listener.setOrientation(fx, fy, fz, ux, uy, uz);
		actx.listener.setPosition(x, y, z);
	}
}`).Invoke()

func SetListenerPosition(x, y, z, rx, ry, rz float32) {
	audioSettingsOnce.Do(initAudioSettings)

	if !actx.Truthy() {
		return
	}

	fx, fy, fz, ux, uy, uz := forwardUp(rx, ry, rz)

	setListenerPosition.Invoke(actx, x, y, z, fx, fy, fz, ux, uy, uz)
}
