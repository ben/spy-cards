//go:build headless
// +build headless

package audio

type implData struct{}

func implInit() {
}

// Preload loads the Sound and returns the error encountered, if any.
func (s *Sound) Preload() error {
	return nil
}

// StartPreload creates a new goroutine to preload the sound.
func (s *Sound) StartPreload() {
	// do nothing
}

// PlaySound plays a sound, optionally modifying its pitch and volume.
func (s *Sound) PlaySound(delay, overridePitch, overrideVolume float64, x, y, z float32) {
	// do nothing
}

// PlayMusic plays a sound as the current music track.
func (s *Sound) PlayMusic(delay float64, restart bool) {
	// do nothing
}

// Release releases any system resources used by the Sound. It is an error to
// use the Sound after calling Release.
func (s *Sound) Release() {
	// do nothing
}

// StopMusic stops the current music track.
func StopMusic() {
	// do nothing
}

// StopSounds stops all active sounds.
func StopSounds() {
	// do nothing
}

func onVolumeChanged() {
	// do nothing
}

func MusicVolumeTemp(scale float64) {
	// do nothing
}

func Tick() {
	// do nothing
}

func MusicFFT() []uint8 {
	return nil
}

func IsCurrentMusicLoading() bool {
	return false
}

func SetListenerPosition(x, y, z, rx, ry, rz float32) {
	// do nothing
}
