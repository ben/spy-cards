//go:build (!js || !wasm) && !headless
// +build !js !wasm
// +build !headless

package audio

import (
	"encoding/binary"
	"fmt"
	"io"
	"log"
	"math"
	"strings"
	"sync"
	"sync/atomic"

	"git.lubar.me/ben/spy-cards/internal"
	"github.com/BenLubar/opus"
	"github.com/mjibson/go-dsp/fft"
	"github.com/mjibson/go-dsp/window"
	"golang.org/x/mobile/exp/audio/al"
)

const alPitch = 0x1003

var (
	musicSource  al.Source
	sources      []al.Source
	curMusic     *Sound
	musicLooping bool
	silentTick1  al.Buffer
	silentTick2  al.Buffer
	tempBuffers  = make(map[al.Buffer]struct{})
)

func implInit() {
	if err := al.OpenDevice(); err != nil {
		panic(err)
	}

	sources = al.GenSources(17)
	musicSource = sources[0]
	sources = sources[1:]

	silentTick1 = al.GenBuffers(1)[0]
	silentTick1.BufferData(al.FormatMono16, make([]byte, 48000/60*2), 48000)
	silentTick2 = al.GenBuffers(1)[0]
	silentTick2.BufferData(al.FormatStereo16, make([]byte, 48000/60*4), 48000)
}

type implData struct {
	loadOnce sync.Once
	loadLazy int32
	buf      al.Buffer
	loop     al.Buffer
	stereo   bool
	pcm      []byte
	loadErr  error
}

func (s *Sound) loadData() {
	audioSettingsOnce.Do(initAudioSettings)

	if s.Volume == 0 {
		s.Volume = 1
	}

	if s.Pitch == 0 {
		s.Pitch = 1
	}

	url := s.Name
	if !strings.Contains(url, "/") {
		url = "audio/" + url + ".opus"
	}

	f, err := internal.OpenAsset(url)
	if err != nil {
		s.impl.loadErr = fmt.Errorf("audio: failed to load %q: %w", s.Name, err)

		return
	}
	defer f.Close()

	b, err := io.ReadAll(f)
	if err != nil {
		s.impl.loadErr = fmt.Errorf("audio: failed to load %q: %w", s.Name, err)

		return
	}

	pcm, channels, err := opus.DecodeOpus(b)
	if err != nil {
		s.impl.loadErr = fmt.Errorf("audio: failed to load %q: %w", s.Name, err)

		return
	}

	s.impl.pcm = pcm

	format := uint32(al.FormatMono16)
	if channels > 1 {
		format = al.FormatStereo16
		s.impl.stereo = true
	}

	s.impl.buf = al.GenBuffers(1)[0]

	if s.Loop {
		start := int(s.Start*48000) * 2 * channels
		end := int(s.End*48000) * 2 * channels

		if end > len(pcm) {
			end = len(pcm)
		}

		if start >= len(pcm) || start == 0 {
			s.impl.loop = s.impl.buf
			pcm = pcm[:end]
		} else {
			s.impl.loop = al.GenBuffers(1)[0]
			s.impl.loop.BufferData(format, pcm[start:end], 48000)
			pcm = pcm[:start]
		}
	}

	s.impl.buf.BufferData(format, pcm, 48000)
}

// Preload loads the Sound and returns the error encountered, if any.
func (s *Sound) Preload() error {
	s.impl.loadOnce.Do(s.loadData)

	return s.impl.loadErr
}

// StartPreload creates a new goroutine to preload the sound.
func (s *Sound) StartPreload() {
	if atomic.CompareAndSwapInt32(&s.impl.loadLazy, 0, 1) {
		go s.impl.loadOnce.Do(s.loadData)
	}
}

// PlaySound plays a sound, optionally modifying its pitch and volume.
func (s *Sound) PlaySound(delay, overridePitch, overrideVolume float64, x, y, z float32) {
	if SoundsVolume == 0 {
		return
	}

	if IgnoreAudio > 0 {
		return
	}

	s.impl.loadOnce.Do(func() {
		log.Println("WARNING: non-preloaded sound:", s.Name)

		s.loadData()
	})

	if s.impl.loadErr != nil {
		log.Println("sound load error", s.Name, s.impl.loadErr)
	}

	buf := s.impl.buf

	if delay < 0 {
		offset := int(-48000 * delay)
		if s.impl.stereo {
			offset -= offset % (48000 / 60 * 4)
		} else {
			offset -= offset % (48000 / 60 * 2)
		}

		if offset >= len(s.impl.pcm) {
			return // entire sound has already played
		}

		buf = al.GenBuffers(1)[0]
		if s.impl.stereo {
			buf.BufferData(al.FormatStereo16, s.impl.pcm[offset:], 48000)
		} else {
			buf.BufferData(al.FormatMono16, s.impl.pcm[offset:], 48000)
		}

		tempBuffers[buf] = struct{}{}
		delay = 0
	}

	for _, source := range sources {
		if source.BuffersQueued() != 0 {
			continue
		}

		volume := overrideVolume
		if volume == 0 {
			volume = s.Volume
		}

		source.SetPosition(al.Vector{x, y, z})
		source.SetGain(float32(SoundsVolume * volume))

		for d := 0.0; d < delay; d += 1.0 / 60.0 {
			if s.impl.stereo {
				source.QueueBuffers(silentTick2)
			} else {
				source.QueueBuffers(silentTick1)
			}
		}

		pitch := overridePitch
		if pitch == 0 {
			pitch = s.Pitch
		}

		source.Setf(alPitch, float32(pitch))

		source.QueueBuffers(buf)

		al.PlaySources(source)

		return
	}

	log.Println("no available audio channel for sound", s.Name)
}

// PlayMusic plays a sound as the current music track.
func (s *Sound) PlayMusic(delay float64, restart bool) {
	if IgnoreAudio > 0 {
		return
	}

	s.impl.loadOnce.Do(func() {
		log.Println("WARNING: non-preloaded sound:", s.Name)

		s.loadData()
	})

	if s.impl.loadErr != nil {
		log.Println("ERROR: music load error", s.Name, s.impl.loadErr)
	}

	if curMusic == s && !restart {
		return
	}

	StopMusic()

	curMusic = s

	musicSource.SetGain(float32(MusicVolume * s.Volume))

	for d := 0.0; d < delay; d += 1.0 / 60.0 {
		if s.impl.stereo {
			musicSource.QueueBuffers(silentTick2)
		} else {
			musicSource.QueueBuffers(silentTick1)
		}
	}

	musicSource.Setf(alPitch, float32(s.Pitch))

	musicSource.QueueBuffers(s.impl.buf)

	al.PlaySources(musicSource)
}

// Release releases any system resources used by the Sound. It is an error to
// use the Sound after calling Release.
func (s *Sound) Release() {
	s.impl.loadErr = fmt.Errorf("audio: sound %q used after free", s.Name)
	al.DeleteBuffers(s.impl.buf)
	s.impl.buf = al.Buffer(0)
}

// StopMusic stops the current music track.
func StopMusic() {
	curMusic = nil
	musicLooping = false

	const alBuffer = 0x1009

	al.StopSources(musicSource)

	musicSource.Seti(alBuffer, 0)
}

// StopSounds stops all active sounds.
func StopSounds() {
	const alBuffer = 0x1009

	for _, source := range sources {
		al.StopSources(source)

		source.Seti(alBuffer, 0)
	}

	for buf := range tempBuffers {
		delete(tempBuffers, buf)
		al.DeleteBuffers(buf)
	}
}

// Tick is an internal function that performs bookkeeping for the audio package.
func Tick() {
	audioSettingsOnce.Do(initAudioSettings)

	var buffer [1]al.Buffer

	for _, source := range sources {
		for source.BuffersProcessed() != 0 {
			source.UnqueueBuffers(buffer[:])

			if _, ok := tempBuffers[buffer[0]]; ok {
				delete(tempBuffers, buffer[0])
				al.DeleteBuffers(buffer[0])
			}
		}
	}

	for musicSource.BuffersProcessed() != 0 {
		musicSource.UnqueueBuffers(buffer[:])
		musicLooping = true
	}

	if curMusic != nil && curMusic.impl.loop.Valid() && musicSource.BuffersQueued() < 2 {
		musicSource.QueueBuffers(curMusic.impl.loop)
	}
}

func onVolumeChanged() {
	if curMusic != nil {
		musicSource.SetGain(float32(MusicVolume * curMusic.Volume))
	}
}

func MusicVolumeTemp(scale float64) {
	if curMusic != nil {
		musicSource.SetGain(float32(MusicVolume * curMusic.Volume * scale))
	}
}

const (
	fftSize           = 2048
	frequencyBinCount = fftSize / 2
	minDecibels       = -100.0
	maxDecibels       = -30.0
)

var (
	fftBuffer        [frequencyBinCount]uint8
	fftInputBuffer   [fftSize]float64
	fftOutputBuffer  [fftSize]complex128
	fftOutputBuffer2 [fftSize]float64
)

func MusicFFT() []uint8 {
	if curMusic == nil {
		return nil
	}

	channels := 1
	if curMusic.impl.stereo {
		channels = 2
	}

	offset := int(musicSource.OffsetByte())

	startOffset := int(curMusic.Start*48000) * channels * 2
	if startOffset > len(curMusic.impl.pcm) {
		startOffset = 0
	}

	endOffset := int(curMusic.End*48000) * channels * 2
	if endOffset > len(curMusic.impl.pcm) {
		endOffset = len(curMusic.impl.pcm)
	}

	if musicLooping {
		offset += startOffset
	}

	offset -= fftSize * channels * 2

	for i := 0; i < len(fftInputBuffer); i, offset = i+1, offset+channels*2 {
		if offset < 0 {
			fftInputBuffer[i] = 0

			continue
		}

		if offset >= endOffset {
			offset = startOffset
		}

		fftInputBuffer[i] = float64(int16(binary.LittleEndian.Uint16(curMusic.impl.pcm[offset:]))) / math.MaxInt16

		if channels == 2 {
			fftInputBuffer[i] += float64(int16(binary.LittleEndian.Uint16(curMusic.impl.pcm[offset+2:]))) / math.MaxInt16
		}
	}

	// This function is a port of what Firefox does.

	window.Apply(fftInputBuffer[:], window.Blackman)

	copy(fftOutputBuffer[:], fft.FFTReal(fftInputBuffer[:]))

	const magnitudeScale = 1.0 / fftSize

	for i, v := range fftOutputBuffer {
		scalarMagnitude := math.Hypot(real(v), imag(v)) * magnitudeScale
		fftOutputBuffer2[i] = scalarMagnitude
	}

	const rangeScaleFactor = 1.0 / (maxDecibels - minDecibels)

	for i := range fftBuffer {
		decibels := minDecibels
		if fftOutputBuffer2[i] != 0 {
			decibels = 20.0 * math.Log10(fftOutputBuffer2[i])
		}

		scaled := (decibels + 100.0) * rangeScaleFactor
		scaled = math.Min(math.Max(255*scaled, 0), 255)
		fftBuffer[i] = uint8(scaled)
	}

	return fftBuffer[:]
}

func IsCurrentMusicLoading() bool {
	return false
}

func SetListenerPosition(x, y, z, rx, ry, rz float32) {
	al.SetListenerPosition(al.Vector{x, y, z})

	fx, fy, fz, ux, uy, uz := forwardUp(rx, ry, rz)

	al.SetListenerOrientation(al.Orientation{
		Forward: al.Vector{fx, fy, fz},
		Up:      al.Vector{ux, uy, uz},
	})
}
