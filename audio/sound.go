// Package audio handles music and sound effects for Spy Cards Online.
package audio

import (
	"math"
	"sync"

	"git.lubar.me/ben/spy-cards/internal"
)

var (
	// SoundsVolume is the user-set volume (0..1) of sound effects.
	SoundsVolume float64 = 0.6
	// MusicVolume is the user-set volume (0..1) of music.
	MusicVolume float64 = 0.6

	// IgnoreAudio causes calls to audio functions to be ignored
	// if it is nonzero.
	IgnoreAudio uint64

	audioSettingsOnce sync.Once
)

func initAudioSettings() {
	implInit()

	settings := internal.LoadSettings()
	MusicVolume = settings.Audio.Music
	SoundsVolume = settings.Audio.Sounds

	onVolumeChanged()

	internal.OnSettingsChanged = append(internal.OnSettingsChanged, func(s *internal.Settings) {
		MusicVolume = s.Audio.Music
		SoundsVolume = s.Audio.Sounds

		onVolumeChanged()
	})
}

func forwardUp(rx, ry, rz float32) (fx, fy, fz, ux, uy, uz float32) {
	sx, cx := math.Sincos(float64(rx))
	sy, cy := math.Sincos(float64(ry))
	sz, cz := math.Sincos(float64(rz))

	// right would be:
	// x = sy*sx*sz+cy*cz
	// y = cx*sz
	// z = cy*sx*sz-sy*cz

	return float32(sy * cx),
		float32(-sx),
		float32(cy * cx),
		float32(sy*sx*cz - cy*sz),
		float32(cx * cz),
		float32(cy*sx*cz + sy*sz)
}

// Sound is an audio clip.
type Sound struct {
	Name     string
	Pitch    float64
	Volume   float64
	MaxDelay float64

	Loop  bool
	Start float64
	End   float64

	impl implData
}

func (s *Sound) PlaySoundGlobal(delay, overridePitch, overrideVolume float64) {
	s.PlaySound(delay, overridePitch, overrideVolume, float32(math.NaN()), float32(math.NaN()), float32(math.NaN()))
}

// Sound effects.
var (
	AtkFail = &Sound{
		Name: "AtkFail",
	}
	AtkSuccess = &Sound{
		Name: "AtkSuccess",
	}
	BattleStart0 = &Sound{
		Name: "BattleStart0",
	}
	Buzzer = &Sound{
		Name: "Buzzer",
	}
	CardSound2 = &Sound{
		Name:   "CardSound2",
		Pitch:  1.2,
		Volume: 0.5,
	}
	Charge = &Sound{
		Name: "Charge",
	}
	Coin = &Sound{
		Name: "Coin",
	}
	Confirm = &Sound{
		Name: "Confirm",
	}
	Confirm1 = &Sound{
		Name:   "Confirm1",
		Pitch:  0.4,
		Volume: 0.5,
	}
	CrowdCheer2 = &Sound{
		Name:  "CrowdCheer2",
		Pitch: 1.2,
	}
	CrowdClap = &Sound{
		Name: "CrowdClap",
	}
	CrowdGasp = &Sound{
		Name: "CrowdGasp",
	}
	Damage0 = &Sound{
		Name: "Damage0",
	}
	Death3 = &Sound{
		Name: "Death3",
	}
	Fail = &Sound{
		Name: "Fail",
	}
	Heal = &Sound{
		Name: "Heal",
	}
	Lazer = &Sound{
		Name: "Lazer",
	}
	PageFlip = &Sound{
		Name: "PageFlip",
	}
	Toss11 = &Sound{
		Name: "Toss11",
	}
	FBCountdown = &Sound{
		Name: "FBCountdown",
	}
	FBDeath = &Sound{
		Name: "FBDeath",
	}
	FBFlower = &Sound{
		Name: "FBFlower",
	}
	FBGameOver = &Sound{
		Name: "FBGameOver",
	}
	FBPoint = &Sound{
		Name: "FBPoint",
	}
	FBStart = &Sound{
		Name: "FBStart",
	}
	MiteKnightIntro = &Sound{
		Name: "MiteKnightIntro",
	}
	MKDeath = &Sound{
		Name: "MKDeath",
	}
	MKGameOver = &Sound{
		Name: "MKGameOver",
	}
	MKHit = &Sound{
		Name: "MKHit",
	}
	MKHit2 = &Sound{
		Name: "MKHit2",
	}
	MKKey = &Sound{
		Name: "MKKey",
	}
	MKOpen = &Sound{
		Name: "MKOpen",
	}
	MKPotion = &Sound{
		Name: "MKPotion",
	}
	MKShield = &Sound{
		Name: "MKShield",
	}
	MKStairs = &Sound{
		Name: "MKStairs",
	}
	MKWalk = &Sound{
		Name: "MKWalk",
	}
	PeacockSpiderNPCSummonSuccess = &Sound{
		Name: "PeacockSpiderNPCSummonSuccess",
	}
	Shot2 = &Sound{
		Name: "Shot2",
	}
	Inn = &Sound{
		Name: "Inn",
	}
	Kut1 = &Sound{
		Name: "Kut1",
	}
	Kut2 = &Sound{
		Name: "Kut2",
	}
	Damage1 = &Sound{
		Name: "Damage1",
	}
	Damage2 = &Sound{
		Name: "Damage2",
	}
	FunnyStep = &Sound{
		Name: "FunnyStep",
	}
	StatUp = &Sound{
		Name: "StatUp",
	}
	StatDown = &Sound{
		Name: "StatDown",
	}
)

// Music tracks.
var (
	Miniboss = &Sound{
		Name:  "Miniboss",
		Loop:  true,
		Start: 20.55,
		End:   87,
	}
	Bounty = &Sound{
		Name:  "Bounty",
		Loop:  true,
		Start: 6.7,
		End:   52.5,
	}
	Inside2 = &Sound{
		Name:  "Inside2",
		Loop:  true,
		Start: 10.3,
		End:   72,
	}
	FlyingBee = &Sound{
		Name:  "FlyingBee",
		Loop:  true,
		Start: 999,
		End:   999,
	}
	MiteKnight = &Sound{
		Name:  "MiteKnight",
		Loop:  true,
		Start: 999,
		End:   999,
	}
	TermiteLoop = &Sound{
		Name:  "TermiteLoop",
		Loop:  true,
		Start: 999,
		End:   999,
	}
	Battle4 = &Sound{
		Name:  "Battle4",
		Loop:  true,
		Start: 12.95,
		End:   83.68,
	}
	Battle0 = &Sound{
		Name:  "Battle0",
		Loop:  true,
		Start: 2.08,
		End:   68.1,
	}
	Chef1 = &Sound{
		Name:  "Chef1",
		Loop:  true,
		Start: 1.503,
		End:   30,
	}
	Field1 = &Sound{
		Name:  "Field1",
		Loop:  true,
		Start: 5.11,
		End:   86.44,
	}
)
