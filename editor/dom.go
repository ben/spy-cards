//go:build js && wasm
// +build js,wasm

package editor

import (
	"fmt"
	"strconv"
	"syscall/js"

	"git.lubar.me/ben/spy-cards/gfx/sprites"
)

var (
	window   = js.Global()
	document = window.Get("document")
	body     = element{document.Get("body")}
)

type element struct {
	js.Value
}

func createEl(name string, classes ...string) element {
	el := element{document.Call("createElement", name)}

	for _, class := range classes {
		el.addClass(class)
	}

	return el
}

func createInput(typ string) element {
	el := createEl("input")

	el.Set("type", typ)

	return el
}

func createOption(label, value string) element {
	el := createEl("option")

	el.setText(label)
	el.setValue(value)

	return el
}

func (el element) addClass(class string) {
	el.Get("classList").Call("add", class)
}

func (el element) removeClass(class string) {
	el.Get("classList").Call("remove", class)
}

func (el element) hasClass(class string) bool {
	return el.Get("classList").Call("contains", class).Bool()
}

func (el element) appendChild(child element) {
	el.Call("appendChild", child.Value)
}

func (el element) removeChild(child element) {
	el.Call("removeChild", child.Value)
}

func (el element) clear() {
	for {
		child := element{el.Get("firstChild")}
		if !child.Truthy() {
			break
		}

		el.removeChild(child)
	}
}

func (el element) setText(text string) {
	el.Set("textContent", text)
}

func (el element) value() string {
	return el.Get("value").String()
}

func (el element) setValue(value string) {
	el.Set("value", value)
}

func (el element) setBackground(c sprites.Color) {
	fg, bg := "#fff", fmt.Sprintf("#%02x%02x%02x", c.R, c.G, c.B)
	if int(c.R)+int(c.G)+int(c.B) > 255*3/2 {
		fg = "#000"
	}

	style := el.Get("style")
	style.Set("color", fg)
	style.Set("backgroundColor", bg)
}

func (el element) addEventListener(name string, callback js.Func) {
	el.Call("addEventListener", name, callback)
}

var nextUniqueID uint64

func uniqueID() string {
	id := nextUniqueID
	nextUniqueID++

	return "editor-uniq-" + strconv.FormatUint(id, 10)
}
