//go:build js && wasm
// +build js,wasm

package editor

import (
	"sort"
	"strconv"
	"syscall/js"

	"git.lubar.me/ben/spy-cards/card"
)

func (e *editor) describeFilter(filter card.Filter) element {
	fragment := element{document.Call("createDocumentFragment")}

	if filter.IsSingleCard() {
		c := e.set.Card(filter[0].CardID)

		fragment.Call("appendChild", document.Call("createTextNode", "card: "))
		fragment.appendChild(e.makePortrait(c.Portrait, c.CustomPortrait))
		fragment.Call("appendChild", document.Call("createTextNode", c.DisplayName()))

		return fragment
	}

	if len(filter) == 1 && filter[0].Type == card.FilterTarget {
		fragment.Call("appendChild", document.Call("createTextNode", "target card"))

		return fragment
	}

	var tp []int64

	for _, fc := range filter {
		if fc.Type == card.FilterTP {
			tp = append(tp, fc.TP)
		}
	}

	if len(tp) != 0 {
		sort.Slice(tp, func(i, j int) bool { return tp[i] < tp[j] })

		var tpDesc []byte

		tpDesc = strconv.AppendInt(tpDesc, tp[0], 10)

		if len(tp) > 2 {
			for i := 1; i < len(tp)-1; i++ {
				tpDesc = append(tpDesc, ", "...)
				tpDesc = strconv.AppendInt(tpDesc, tp[i], 10)
			}

			tpDesc = append(tpDesc, ", "...)
		}

		if len(tp) != 1 {
			tpDesc = append(tpDesc, " or "...)
			tpDesc = strconv.AppendInt(tpDesc, tp[len(tp)-1], 10)
		}

		tpDesc = append(tpDesc, " TP "...)

		fragment.Call("appendChild", document.Call("createTextNode", string(tpDesc)))
	}

	var (
		anyTribes bool
		ranks     uint64
	)

	for _, fc := range filter {
		if fc.Type == card.FilterRank {
			ranks |= 1 << fc.Rank

			continue
		}

		if fc.Type != card.FilterTribe && fc.Type != card.FilterNotTribe {
			continue
		}

		if anyTribes {
			fragment.Call("appendChild", document.Call("createTextNode", "/"))
		}

		if fc.Type == card.FilterNotTribe {
			fragment.Call("appendChild", document.Call("createTextNode", "NOT "))
		}

		anyTribes = true
		td := e.findTribe(fc.Tribe, fc.CustomTribe)

		tribe := createEl("span")
		tribe.setBackground(td.Color())
		tribe.setText(td.Name())
		fragment.appendChild(tribe)
	}

	if ranks == 0 {
		if !anyTribes {
			if len(tp) == 0 {
				fragment.Call("appendChild", document.Call("createTextNode", "any card"))
			} else {
				fragment.Call("appendChild", document.Call("createTextNode", "card"))
			}
		}

		return fragment
	}

	if anyTribes {
		fragment.Call("appendChild", document.Call("createTextNode", " "))
	}

	appendRank := func(r card.Rank) {
		rank := createEl("span", "rank-"+r.String())
		rank.setText(r.String())
		fragment.appendChild(rank)
	}

	if ranks == 1<<card.Attacker|1<<card.Effect {
		fragment.Call("appendChild", document.Call("createTextNode", "Enemy ("))
		appendRank(card.Attacker)
		fragment.Call("appendChild", document.Call("createTextNode", " or "))
		appendRank(card.Effect)
		fragment.Call("appendChild", document.Call("createTextNode", ")"))
	} else {
		first := true
		for r := card.Attacker; r < card.RankNone; r++ {
			if ranks&(1<<r) != 0 {
				if first {
					first = false
				} else {
					fragment.Call("appendChild", document.Call("createTextNode", " or "))
				}
				appendRank(r)
			}
		}
	}

	return fragment
}

func (e *editor) findTribe(tribe card.Tribe, custom string) card.TribeDef {
	td := card.TribeDef{
		Tribe:      tribe,
		CustomName: custom,
	}
	if td.Tribe != card.TribeCustom {
		return td
	}

	td.Red = 0x80
	td.Green = 0x80
	td.Blue = 0x80

	for _, c := range e.set.Cards {
		for _, t := range c.Tribes {
			if t.Tribe == td.Tribe && t.CustomName == td.CustomName {
				return t
			}
		}
	}

	return td
}

func (e *editor) editFilterModal(filter *card.Filter, allowTarget bool) {
	e.modal(func(modal element) {
		done := make(chan struct{})

		doneButton := createEl("button")
		cardLabel := createEl("label")
		cardSelect := createEl("select")
		cardButton := createEl("button")
		targetButton := createEl("button")
		tribeLabel := createEl("label")
		tribeSelect := createEl("select")
		tribeButton := createEl("button")
		notTribeButton := createEl("button")
		rankLabel := createEl("label")
		rankSelect := createEl("select")
		rankButton := createEl("button")
		tpLabel := createEl("label")
		tpInput := createInput("number")
		tpButton := createEl("button")

		var reset func()
		reset = func() {
			modal.clear()

			ul, createItem := e.filterComponents.setup(e, func(li element) {
				i := defaultIndexOf(li)
				*filter = append((*filter)[:i], (*filter)[i+1:]...)
				reset()
			}, defaultIndexOf, func(dst, src int, _, _ element, after bool) bool {
				f := (*filter)[src]
				if after {
					copy((*filter)[dst+1:], (*filter)[dst:src])
				} else {
					copy((*filter)[src:], (*filter)[src+1:dst+1])
				}
				(*filter)[dst] = f

				return true
			})
			modal.appendChild(ul)
			for _, fc := range *filter {
				li := createItem(true)
				switch fc.Type {
				case card.FilterSingleCard:
					c := e.set.Card(fc.CardID)
					li.setText(c.DisplayName())
					li.Call("insertBefore", e.makePortrait(c.Portrait, c.CustomPortrait).Value, li.Get("firstChild"))
				case card.FilterNotTribe:
					li.setText("NOT ")

					fallthrough
				case card.FilterTribe:
					td := e.findTribe(fc.Tribe, fc.CustomTribe)
					tribe := createEl("span")
					tribe.setBackground(td.Color())
					tribe.setText(td.Name())
					li.appendChild(tribe)
				case card.FilterRank:
					li.addClass("rank-" + fc.Rank.String())
					li.setText(fc.Rank.String())
				case card.FilterTP:
					li.setText(strconv.FormatInt(fc.TP, 10) + " TP")
				case card.FilterTarget:
					li.setText("Target Card")
				}
			}

			if len(*filter) == 0 {
				cardSelect.clear()
				e.fillCardSelect(cardSelect, nil)
				modal.appendChild(cardLabel)
			}

			if !filter.IsSingleCard() && (len(*filter) != 1 || (*filter)[0].Type != card.FilterTarget) {
				tribeSelect.clear()
				anyTribes := false
				for t := card.Tribe(0); t < card.TribeCustom; t++ {
					found := false
					for _, fc := range *filter {
						if (fc.Type == card.FilterTribe || fc.Type == card.FilterNotTribe) && fc.Tribe == t {
							found = true

							break
						}
					}
					if !found {
						anyTribes = true
						opt := createOption(t.String(), strconv.Itoa(int(t)))
						opt.setBackground(card.TribeDef{Tribe: t}.Color())
						tribeSelect.appendChild(opt)
					}
				}
				seenTribe := make(map[string]bool)
				for _, fc := range *filter {
					if (fc.Type == card.FilterTribe || fc.Type == card.FilterNotTribe) && fc.Tribe == card.TribeCustom {
						seenTribe[fc.CustomTribe] = true
					}
				}
				for _, c := range e.set.Cards {
					for _, td := range c.Tribes {
						if td.Tribe == card.TribeCustom && !seenTribe[td.CustomName] {
							anyTribes = true
							opt := createOption(td.Name(), "!"+td.CustomName)
							opt.setBackground(td.Color())
							tribeSelect.appendChild(opt)
							seenTribe[td.CustomName] = true
						}
					}
				}
				if anyTribes {
					modal.appendChild(tribeLabel)
				}
				rankSelect.clear()
				rankCount := 0
				for r := card.Rank(0); r < card.Enemy; r++ {
					found := false
					for _, fc := range *filter {
						if fc.Type == card.FilterRank && fc.Rank == r {
							found = true

							break
						}
					}
					if !found {
						rankCount++
						opt := createOption(r.String(), strconv.Itoa(int(r)))
						opt.addClass("rank-" + r.String())
						rankSelect.appendChild(opt)
					}
				}
				if rankCount > 1 {
					modal.appendChild(rankLabel)
				}

				modal.appendChild(tpLabel)
			}

			modal.appendChild(createEl("br"))
			modal.appendChild(doneButton)
		}

		cardLabel.setText("Card")
		cardLabel.appendChild(cardSelect)
		cardButton.setText("Require")
		cardLabel.appendChild(cardButton)
		onCardClick := js.FuncOf(func(_ js.Value, args []js.Value) interface{} {
			args[0].Call("preventDefault")
			if id, err := strconv.ParseUint(cardSelect.Get("value").String(), 10, 64); err == nil {
				*filter = append(*filter, card.FilterComponent{
					Type:   card.FilterSingleCard,
					CardID: card.ID(id),
				})
			}
			reset()

			return js.Undefined()
		})
		defer onCardClick.Release()
		cardButton.addEventListener("click", onCardClick)

		if allowTarget {
			onTargetClick := js.FuncOf(func(_ js.Value, args []js.Value) interface{} {
				args[0].Call("preventDefault")

				*filter = append(*filter, card.FilterComponent{
					Type: card.FilterTarget,
				})

				reset()

				return js.Undefined()
			})
			defer onTargetClick.Release()
			targetButton.addEventListener("click", onTargetClick)
			targetButton.setText("Target Card")
			cardLabel.appendChild(createEl("br"))
			cardLabel.appendChild(targetButton)
		}

		tribeLabel.setText("Tribe")
		tribeLabel.appendChild(tribeSelect)
		tribeButton.setText("Require")
		tribeLabel.appendChild(tribeButton)
		notTribeButton.setText("Forbid")
		tribeLabel.appendChild(notTribeButton)
		onTribeClick := js.FuncOf(func(this js.Value, args []js.Value) interface{} {
			args[0].Call("preventDefault")
			typ := card.FilterTribe
			if notTribeButton.Equal(this) {
				typ = card.FilterNotTribe
			}
			val := tribeSelect.Get("value").String()
			if val[0] == '!' {
				*filter = append(*filter, card.FilterComponent{
					Type:        typ,
					Tribe:       card.TribeCustom,
					CustomTribe: val[1:],
				})
			} else {
				*filter = append(*filter, card.FilterComponent{
					Type:  typ,
					Tribe: card.Tribe(window.Call("parseInt", val, 10).Int()),
				})
			}
			reset()

			return js.Undefined()
		})
		defer onTribeClick.Release()
		tribeButton.addEventListener("click", onTribeClick)
		notTribeButton.addEventListener("click", onTribeClick)

		rankLabel.setText("Rank")
		rankLabel.appendChild(rankSelect)
		rankButton.setText("Permit")
		rankLabel.appendChild(rankButton)
		onRankClick := js.FuncOf(func(_ js.Value, args []js.Value) interface{} {
			args[0].Call("preventDefault")
			*filter = append(*filter, card.FilterComponent{
				Type: card.FilterRank,
				Rank: card.Rank(window.Call("parseInt", rankSelect.Get("value"), 10).Int()),
			})
			reset()

			return js.Undefined()
		})
		defer onRankClick.Release()
		rankButton.addEventListener("click", onRankClick)

		tpLabel.setText("TP")
		tpLabel.appendChild(tpInput)
		tpInput.Set("min", -999999999999)
		tpInput.Set("max", 999999999999)
		tpInput.Set("step", 1)
		tpInput.setValue("1")
		tpButton.setText("Permit")
		tpLabel.appendChild(tpButton)
		onTPClick := js.FuncOf(func(_ js.Value, args []js.Value) interface{} {
			args[0].Call("preventDefault")

			tp, _ := strconv.ParseInt(tpInput.value(), 10, 64)

			haveTP := false

			for _, fc := range *filter {
				if fc.Type == card.FilterTP && fc.TP == tp {
					haveTP = true

					break
				}
			}

			if !haveTP {
				*filter = append(*filter, card.FilterComponent{
					Type: card.FilterTP,
					TP:   tp,
				})
			}

			reset()

			return js.Undefined()
		})
		defer onTPClick.Release()
		tpButton.addEventListener("click", onTPClick)

		doneButton.setText("Done")
		onDoneClick := js.FuncOf(func(_ js.Value, args []js.Value) interface{} {
			args[0].Call("preventDefault")
			close(done)

			return js.Undefined()
		})
		defer onDoneClick.Release()
		doneButton.addEventListener("click", onDoneClick)

		reset()
		<-done
		e.encodeCardCodes()
	})
}
