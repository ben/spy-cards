//go:build js && wasm
// +build js,wasm

package editor

import (
	"encoding/base64"
	"encoding/hex"
	"fmt"
	"strconv"
	"syscall/js"

	"git.lubar.me/ben/spy-cards/card"
	"git.lubar.me/ben/spy-cards/format"
	"git.lubar.me/ben/spy-cards/gfx/sprites"
)

func (e *editor) editEffectModal(effect **card.EffectDef, parents []*card.EffectDef, def *card.Def) {
	// one less than a trillion should be good enough for any
	// game mode without having to worry about overflow
	const bigAmount = 999999999999

	e.modal(func(modal element) {
		var editorFuncs []js.Func
		defer func() {
			for _, f := range editorFuncs {
				f.Release()
			}
		}()

		done := make(chan struct{})
		doneButton := createEl("button")
		doneButton.setText("Close")
		onDoneClick := js.FuncOf(func(_ js.Value, args []js.Value) interface{} {
			args[0].Call("preventDefault")
			close(done)

			return js.Undefined()
		})
		defer onDoneClick.Release()
		doneButton.addEventListener("click", onDoneClick)

		deleteButton := createEl("button")
		deleteButton.setText("Delete Effect")

		effectCode := createInput("text")
		setCode := func() {
			if (*effect).Complete() {
				var w format.Writer
				w.UVarInt(5) // format version
				if err := (*effect).Marshal(&w); err == nil {
					effectCode.Set("value", base64.StdEncoding.EncodeToString(w.Data()))
				}
			} else {
				effectCode.Set("value", "")
			}
		}

		priorityLabel := createEl("label")
		priorityLabel.setText("Priority")
		priorityInput := createInput("number")
		priorityInput.Set("step", 1)
		priorityLabel.appendChild(priorityInput)
		priorityHint := createEl("p", "subtle")
		priorityLabel.appendChild(priorityHint)
		updatePriorityHint := func() {
			effectOrCondition := "effect"
			occur := "occur"
			if (*effect).Type.IsCondition() {
				effectOrCondition = "condition"
				occur = "be checked"
			}
			switch {
			case (*effect).Type.IsRoot():
				priorityHint.setText("Root effects must have a priority of 0.")
			case (*effect).Type.IsPassive():
				priorityHint.setText("Passive effects must have a priority of 0.")
			case (*effect).Priority == 0:
				priorityHint.setText("This " + effectOrCondition + " will not be visible or audible and will resolve as soon as possible.")
			case len(parents) != 0 && (parents[len(parents)-1].Type == card.CondLastEffect || parents[len(parents)-1].Type == card.CondOnNumb):
				priorityHint.setText("This " + effectOrCondition + " will be visible and audible.")
			case (*effect).Priority < 64:
				priorityHint.setText("This " + effectOrCondition + " will " + occur + " before stats are displayed during a round.")
			case (*effect).Priority < 224:
				priorityHint.setText("This " + effectOrCondition + " will " + occur + " during the main part of a round.")
			default:
				priorityHint.setText("This " + effectOrCondition + " will " + occur + " after the winner of the round has been determined.")
			}
		}

		var resetUI func()
		resetUI = func() {
			for _, f := range editorFuncs {
				f.Release()
			}
			editorFuncs = editorFuncs[:0]

			modal.clear()
			modal.appendChild(effectCode)

			if *effect == nil {
				addEffectOption := func(name, desc string, template card.EffectDef) {
					label := createEl("label", "button")
					button := createEl("button")
					button.setText(name)
					onClick := js.FuncOf(func(_ js.Value, args []js.Value) interface{} {
						args[0].Call("preventDefault")
						*effect = &template
						setCode()
						resetUI()

						return js.Undefined()
					})
					editorFuncs = append(editorFuncs, onClick)
					button.addEventListener("click", onClick)
					label.appendChild(button)
					hint := createEl("p", "subtle")
					hint.setText(desc)
					label.appendChild(hint)
					modal.appendChild(label)
				}

				allowPassive := false
				if len(parents) > 0 {
					directParent := parents[len(parents)-1]
					if (directParent.Type == card.CondApply && directParent.Flags&card.FlagCondApplyNextRound != 0) || directParent.Type == card.CondInHand {
						allowPassive = true
					} else if directParent.Type == card.CondLimit && len(parents) > 1 {
						indirectParent := parents[len(parents)-2]
						allowPassive = (indirectParent.Type == card.CondApply && indirectParent.Flags&card.FlagCondApplyNextRound != 0) || indirectParent.Type == card.CondInHand
					}
				}

				if def != nil && len(parents) == 0 {
					addEffectOption("Flavor Text", "Add or replace text on a card (no effect).", card.EffectDef{
						Type:     card.FlavorText,
						Priority: 0,
					})
				}
				if def != nil {
					addEffectOption("Card Stat", "Modify a card's ATK or DEF.", card.EffectDef{
						Type:     card.EffectStat,
						Priority: 65,
						Amount:   1,
					})
				}
				addEffectOption("Empower", "Modify the ATK or DEF of cards based on a filter.", card.EffectDef{
					Type:     card.EffectEmpower,
					Priority: 80,
					Amount:   1,
				})
				addEffectOption("Summon", "Add a card to the field.", card.EffectDef{
					Type:     card.EffectSummon,
					Priority: 25,
					Amount:   1,
				})
				addEffectOption("Heal", "Directly modify a player's HP.", card.EffectDef{
					Type:     card.EffectHeal,
					Priority: 40,
					Amount:   1,
				})
				if allowPassive {
					addEffectOption("TP", "Add or remove TP from a player.", card.EffectDef{
						Type:     card.EffectTP,
						Priority: 0,
						Amount:   1,
					})
				}
				addEffectOption("Numb", "Nullify a card's stats and prevent it from executing further effects.", card.EffectDef{
					Type:     card.EffectNumb,
					Priority: 90,
					Flags:    card.FlagNumbAvoidZero,
					Amount:   1,
					Filter: card.Filter{
						{
							Type: card.FilterRank,
							Rank: card.Attacker,
						},
					},
				})
				addEffectOption("Raw Stat", "Directly modify ATK or DEF of a player.", card.EffectDef{
					Type:     card.EffectRawStat,
					Priority: 95,
					Amount:   1,
				})
				addEffectOption("Multiply Healing", "Apply a multiplier to the healing or damage a player receives this round.", card.EffectDef{
					Type:     card.EffectMultiplyHealing,
					Priority: 40,
					Amount:   0,
				})
				addEffectOption("Prevent Numb", "Ignore an incoming numb effect.", card.EffectDef{
					Type:     card.EffectPreventNumb,
					Priority: 1,
					Amount:   1,
				})
				addEffectOption("Modify Available Cards", "Add or remove cards from the match.", card.EffectDef{
					Type:     card.EffectModifyAvailableCards,
					Priority: 50,
					Amount:   1,
				})
				if allowPassive {
					addEffectOption("Modify Card Cost", "Change the TP cost of a card.", card.EffectDef{
						Type:     card.EffectModifyCardCost,
						Priority: 0,
						Amount:   1,
					})
				}
				addEffectOption("Draw Card", "Force a player to draw or discard cards.", card.EffectDef{
					Type:     card.EffectDrawCard,
					Priority: 45,
					Amount:   1,
				})
				addEffectOption("Modify Game Rule", "Change game rule values for a player.", card.EffectDef{
					Type:     card.EffectModifyGameRule,
					Priority: 5,
					Amount:   5,
					Amount2:  1,
				})
				if len(parents) == 0 || (parents[len(parents)-1].Type != card.CondLastEffect && parents[len(parents)-1].Type != card.CondOnNumb) {
					// even though this is an effect, it can't be used here as there is no valid priority
					addEffectOption("Delay Setup", "Change the number of rounds a setup effect is delayed by.", card.EffectDef{
						Type:     card.EffectDelaySetup,
						Priority: 55,
						Amount:   1,
					})

					addEffectOption("Card (Condition)", "Apply an effect based on the presence of another card.", card.EffectDef{
						Type:     card.CondCard,
						Priority: 35,
						Amount:   1,
					})
					addEffectOption("Limit (Condition)", "Apply an effect a limited number of times.", card.EffectDef{
						Type:     card.CondLimit,
						Priority: 10,
						Amount:   1,
					})
					addEffectOption("Winner (Condition)", "Apply an effect based on who wins the round.", card.EffectDef{
						Type:     card.CondWinner,
						Priority: 225,
					})
				}
				addEffectOption("Setup (Condition)", "Apply an effect on a later turn.", card.EffectDef{
					Type:     card.CondApply,
					Priority: 5,
					Flags:    card.FlagCondApplyNextRound,
				})
				if len(parents) == 0 || (parents[len(parents)-1].Type != card.CondLastEffect && parents[len(parents)-1].Type != card.CondOnNumb) {
					addEffectOption("Coin (Condition)", "Apply an effect based on a coin flip.", card.EffectDef{
						Type:     card.CondCoin,
						Priority: 35,
						Amount:   1,
						Amount2:  1,
						Amount3:  1,
					})
					addEffectOption("Stat (Condition)", "Apply an effect based on a statistic.", card.EffectDef{
						Type:     card.CondStat,
						Priority: 85,
						Amount:   1,
					})
					if def != nil && len(parents) == 0 {
						addEffectOption("In Hand (Condition)", "Apply an effect when this card is in a player's hand.", card.EffectDef{
							Type:     card.CondInHand,
							Priority: 0,
							Flags:    card.FlagCondInHandHide,
							Amount:   0,
						})
					}
					if def != nil {
						addEffectOption("Last Effect (Condition)", "Apply an effect, then remove all pending effects from this card.", card.EffectDef{
							Type:     card.CondLastEffect,
							Priority: 1,
						})
					}
					if def != nil && len(parents) == 0 {
						addEffectOption("On Numb (Condition)", "Apply an effect when this card is numbed.", card.EffectDef{
							Type:     card.CondOnNumb,
							Priority: 0,
						})
					}
					addEffectOption("Multiple Effects (Condition)", "Apply two effects at once.", card.EffectDef{
						Type:     card.CondMultipleEffects,
						Priority: 0,
					})
					if def != nil && len(parents) == 0 {
						addEffectOption("On Discard (Condition)", "Apply an effect when this card is discarded by a Draw Card effect.", card.EffectDef{
							Type:     card.CondOnDiscard,
							Priority: 0,
						})
					}
					if len(parents) != 0 {
						addEffectOption("Compare Target Card (Condition)", "Check if the current Target Card matches a filter.", card.EffectDef{
							Type:     card.CondCompareTargetCard,
							Priority: 0,
						})
					}
					if def != nil && len(parents) == 0 {
						addEffectOption("On Exile (Condition)", "Apply an effect when this card is exiled.", card.EffectDef{
							Type:     card.CondOnExile,
							Priority: 0,
						})
					}
					addEffectOption("Lifesteal", "Gain health for winning the round.", card.EffectDef{
						Type:     card.CondWinner,
						Priority: 225,
						Result: &card.EffectDef{
							Type:     card.EffectHeal,
							Priority: 40,
							Amount:   1,
						},
					})
				}
				if def != nil {
					addEffectOption("Passive", "Keep this card on the field during future rounds unless it is numbed.", card.EffectDef{
						Type:     card.CondApply,
						Priority: 254,
						Flags:    card.FlagCondApplyNextRound | card.FlagCondApplyShowOriginalText,
						Result: &card.EffectDef{
							Type:     card.EffectSummon,
							Priority: 0,
							Flags:    card.FlagSummonReplace,
							Amount:   1,
							Filter: card.Filter{
								{
									Type:   card.FilterSingleCard,
									CardID: def.ID,
								},
							},
						},
					})
				}
			} else {
				min, max := 0, 254
				switch {
				case (*effect).Type.IsRoot() || (*effect).Type.IsPassive():
					max = 0
				case (*effect).Type == card.CondWinner:
					min = 224
				case (*effect).Type == card.EffectDelaySetup:
					min = 2
				case len(parents) == 1 && (parents[0].Type == card.CondLastEffect || parents[0].Type == card.CondOnNumb):
					max = 1
				}

				priorityInput.Set("valueAsNumber", int((*effect).Priority))
				priorityInput.Set("min", min)
				priorityInput.Set("max", max)
				updatePriorityHint()
				if min != max {
					modal.appendChild(priorityLabel)
				}

				addCheckbox := func(name string, checked bool, set func(bool)) {
					label := createEl("label")
					label.setText(" " + name)
					checkbox := createInput("checkbox")
					label.Call("insertBefore", checkbox.Value, label.Get("firstChild"))
					checkbox.Set("checked", checked)
					onChange := js.FuncOf(func(_ js.Value, _ []js.Value) interface{} {
						set(checkbox.Get("checked").Bool())
						setCode()

						return js.Undefined()
					})
					editorFuncs = append(editorFuncs, onChange)
					checkbox.addEventListener("change", onChange)
					modal.appendChild(label)
				}

				addText := func(name string, str *string, area bool) {
					label := createEl("label")
					label.setText(name)

					var input element

					if area {
						input = createEl("textarea")
					} else {
						input = createInput("text")
					}

					label.appendChild(input)
					input.Set("value", *str)
					onChange := js.FuncOf(func(_ js.Value, _ []js.Value) interface{} {
						*str = input.value()
						setCode()

						if area {
							input.Call("setCustomValidity", "")

							for _, letter := range *str {
								if !sprites.LetterSupported(sprites.FontBubblegumSans, letter) && letter != '\n' {
									input.Call("setCustomValidity", "Warning: The letter '"+string(letter)+"' cannot be rendered by Spy Cards and will display as a blank space.")

									break
								}
							}
						}

						return js.Undefined()
					})
					editorFuncs = append(editorFuncs, onChange)
					input.addEventListener("input", onChange)
					modal.appendChild(label)
				}

				addToggleFlag := func(flag card.EffectFlag, name string) {
					addCheckbox(name, (*effect).Flags&flag != 0, func(checked bool) {
						if checked {
							(*effect).Flags |= flag
						} else {
							(*effect).Flags &^= flag
						}
					})
				}

				type flagName struct {
					flag card.EffectFlag
					name string
				}
				addRadioFlagCustom := func(mask card.EffectFlag, name string, options []flagName, initial card.EffectFlag, setFlag func(mask, set card.EffectFlag)) {
					container := createEl("div", "radio")
					container.setText(name)
					container.appendChild(createEl("br"))
					radios := make([]element, len(options))
					onChange := js.FuncOf(func(_ js.Value, _ []js.Value) interface{} {
						for i, r := range radios {
							if r.Get("checked").Bool() {
								setFlag(mask, options[i].flag)

								break
							}
						}
						setCode()

						return js.Undefined()
					})
					editorFuncs = append(editorFuncs, onChange)
					groupName := uniqueID()
					for i, opt := range options {
						label := createEl("label")
						labelText := createEl("span")
						labelText.setText(opt.name)
						radio := createInput("radio")
						radio.Set("name", groupName)
						if initial&mask == opt.flag {
							radio.Set("checked", true)
						}
						radio.addEventListener("change", onChange)
						label.appendChild(radio)
						label.appendChild(labelText)
						container.appendChild(label)
						radios[i] = radio
					}
					modal.appendChild(container)
				}
				addRadioFlag := func(mask card.EffectFlag, name string, options []flagName) {
					addRadioFlagCustom(mask, name, options, (*effect).Flags, func(mask, set card.EffectFlag) {
						(*effect).Flags &^= mask
						(*effect).Flags |= set
					})
				}

				addAttackOrDefenseFlag := func() {
					addRadioFlag(card.FlagStatDEF, "Stat", []flagName{
						{0, "ATK"},
						{card.FlagStatDEF, "DEF"},
					})
				}

				addSelfOrOpponentFlag := func(flag card.EffectFlag, defaultOpponent bool) {
					if defaultOpponent {
						addRadioFlag(flag, "Player", []flagName{
							{flag, "Self"},
							{0, "Opponent"},
						})
					} else {
						addRadioFlag(flag, "Player", []flagName{
							{0, "Self"},
							{flag, "Opponent"},
						})
					}
				}

				addAmount := func(num *int64, name string, infinity card.EffectFlag, min, max int64) {
					label := createEl("label")
					label.setText(name)
					group := createEl("span", "input-group")
					posInf := createEl("button")
					posInf.setText("+" + card.PosInf)
					unInf := createEl("button", "wide")
					negInf := createEl("button")
					negInf.setText(card.NegInf)
					input := createInput("number")
					input.setValue(strconv.FormatInt(*num, 10))
					input.Set("min", strconv.FormatInt(min, 10))
					input.Set("max", strconv.FormatInt(max, 10))
					input.Set("step", "1")
					onChange := js.FuncOf(func(_ js.Value, _ []js.Value) interface{} {
						n, err := strconv.ParseInt(input.value(), 10, 64)
						if err == nil && n >= min && n <= max {
							*num = n
							setCode()
						}

						return js.Undefined()
					})
					editorFuncs = append(editorFuncs, onChange)
					input.addEventListener("input", onChange)
					posInfClick := js.FuncOf(func(_ js.Value, args []js.Value) interface{} {
						args[0].Call("preventDefault")
						(*effect).Flags |= infinity
						*num = 1
						unInf.setText("Positive Infinity")
						if input.Get("parentNode").Truthy() {
							group.Call("insertBefore", unInf.Value, input.Value)
							group.removeChild(input)
						}
						setCode()

						return js.Undefined()
					})
					editorFuncs = append(editorFuncs, posInfClick)
					posInf.addEventListener("click", posInfClick)
					negInfClick := js.FuncOf(func(_ js.Value, args []js.Value) interface{} {
						args[0].Call("preventDefault")
						(*effect).Flags |= infinity
						*num = -1
						unInf.setText("Negative Infinity")
						if input.Get("parentNode").Truthy() {
							group.Call("insertBefore", unInf.Value, input.Value)
							group.removeChild(input)
						}
						setCode()

						return js.Undefined()
					})
					editorFuncs = append(editorFuncs, negInfClick)
					negInf.addEventListener("click", negInfClick)
					unInfClick := js.FuncOf(func(_ js.Value, args []js.Value) interface{} {
						args[0].Call("preventDefault")
						(*effect).Flags &^= infinity
						group.Call("insertBefore", input.Value, unInf.Value)
						group.removeChild(unInf)
						input.Call("dispatchEvent", window.Get("InputEvent").New("input"))

						return js.Undefined()
					})
					editorFuncs = append(editorFuncs, unInfClick)
					unInf.addEventListener("click", unInfClick)
					inputID := uniqueID()
					input.Set("id", inputID)
					label.Call("setAttribute", "for", inputID)
					if infinity != 0 && min < 0 {
						group.appendChild(negInf)
					}
					if (*effect).Flags&infinity != 0 {
						switch {
						case *num == 1:
							unInf.setText("Positive Infinity")
						case *num == -1:
							unInf.setText("Negative Infinity")
						default:
							unInf.setText("??? Infinity")
						}
						group.appendChild(unInf)
					} else {
						group.appendChild(input)
					}
					if infinity != 0 && max > 0 {
						group.appendChild(posInf)
					}
					label.appendChild(group)
					modal.appendChild(label)
				}

				addFilter := func() {
					label := createEl("label", "button")
					label.setText("Filter")
					desc := createEl("p", "subtle")
					desc.appendChild(e.describeFilter((*effect).Filter))
					label.appendChild(desc)
					button := createEl("button")
					button.setText("Change Filter")
					onClick := js.FuncOf(func(_ js.Value, args []js.Value) interface{} {
						args[0].Call("preventDefault")
						go func() {
							e.editFilterModal(&(*effect).Filter, true)
							desc.clear()
							desc.appendChild(e.describeFilter((*effect).Filter))
						}()

						return js.Undefined()
					})
					editorFuncs = append(editorFuncs, onClick)
					button.addEventListener("click", onClick)
					label.appendChild(button)
					modal.appendChild(label)
				}

				addEffect := func(name string, result **card.EffectDef) {
					label := createEl("label", "button")
					label.setText(name)
					desc := createEl("p", "subtle")
					desc.setText((&card.RichDescription{
						Content: (*result).Description(def, e.set),
					}).String())
					label.appendChild(desc)
					button := createEl("button")
					button.setText("Edit Effect")
					onClick := js.FuncOf(func(_ js.Value, args []js.Value) interface{} {
						args[0].Call("preventDefault")
						go func() {
							e.editEffectModal(result, append(parents[:len(parents):len(parents)], *effect), def)
							desc.setText((&card.RichDescription{
								Content: (*result).Description(def, e.set),
							}).String())
							setCode()
						}()

						return js.Undefined()
					})
					editorFuncs = append(editorFuncs, onClick)
					button.addEventListener("click", onClick)
					label.appendChild(button)
					modal.appendChild(label)
				}

				switch (*effect).Type {
				case card.FlavorText:
					addText("Flavor Text", &(*effect).Text, true)
					label := createEl("label")
					label.setText("Color")
					input := createInput("color")
					label.appendChild(input)
					input.setValue("#226688")
					if (*effect).Flags&card.FlagFlavorTextCustomColor != 0 {
						red := uint8((*effect).Amount)
						green := uint8((*effect).Amount >> 8)
						blue := uint8((*effect).Amount >> 16)
						input.setValue(fmt.Sprintf("#%02x%02x%02x", red, green, blue))
					}
					onChange := js.FuncOf(func(_ js.Value, _ []js.Value) interface{} {
						if input.value() == "#226688" {
							(*effect).Flags &^= card.FlagFlavorTextCustomColor

							input.Call("setCustomValidity", "")
						} else if b, err := hex.DecodeString(input.value()[1:]); err == nil {
							(*effect).Flags |= card.FlagFlavorTextCustomColor
							(*effect).Amount = int64(b[0]) | (int64(b[1]) << 8) | (int64(b[2]) << 16)

							if l := ((uint(b[0]) << 1) + uint(b[0]) + (uint(b[1]) << 2) + uint(b[2])) >> 3; l > 140 {
								input.Call("setCustomValidity", "Warning: This color may be too light to easily read.")
							} else {
								input.Call("setCustomValidity", "")
							}
						}

						setCode()

						return js.Undefined()
					})
					editorFuncs = append(editorFuncs, onChange)
					input.addEventListener("input", onChange)
					modal.appendChild(label)
					addToggleFlag(card.FlagFlavorTextHideRemaining, "Hide remaining effects")
				case card.EffectStat:
					addAttackOrDefenseFlag()
					addAmount(&(*effect).Amount, "Amount", card.FlagStatInfinity, -bigAmount, bigAmount)
				case card.EffectEmpower:
					addAttackOrDefenseFlag()
					addSelfOrOpponentFlag(card.FlagStatOpponent, false)
					addAmount(&(*effect).Amount, "Amount", card.FlagStatInfinity, -bigAmount, bigAmount)
					addFilter()
				case card.EffectSummon:
					addSelfOrOpponentFlag(card.FlagSummonOpponent, false)
					addToggleFlag(card.FlagSummonInvisible, "Make summoned card invisible")
					addToggleFlag(card.FlagSummonReplace, "Make this card invisible")
					addAmount(&(*effect).Amount, "Count", 0, 1, bigAmount)
					addFilter()
				case card.EffectHeal:
					addSelfOrOpponentFlag(card.FlagHealOpponent, false)
					addToggleFlag(card.FlagHealRaw, "Ignore healing multipliers")
					addAmount(&(*effect).Amount, "Amount", card.FlagHealInfinity, -bigAmount, bigAmount)
				case card.EffectTP:
					addAmount(&(*effect).Amount, "Amount", card.FlagTPInfinity, -bigAmount, bigAmount)
				case card.EffectNumb:
					addSelfOrOpponentFlag(card.FlagNumbSelf, true)
					addToggleFlag(card.FlagNumbHighest, "Target card with highest stats")
					addToggleFlag(card.FlagNumbIgnoreATK, "Ignore card ATK stat")
					addToggleFlag(card.FlagNumbIgnoreDEF, "Ignore card DEF stat")
					addToggleFlag(card.FlagNumbAvoidZero, "Avoid targeting cards with zero ATK and DEF")
					addToggleFlag(card.FlagNumbSummon, "Summon numbed card to other side of field")
					addToggleFlag(card.FlagNumbIgnorePrevent, "Ignore Prevent Numb")
					addAmount(&(*effect).Amount, "Count", card.FlagNumbInfinity, 1, bigAmount)
					addFilter()
					addEffect("On Success", &(*effect).Result)
					addEffect("On Failure", &(*effect).Result2)
				case card.EffectRawStat:
					addAttackOrDefenseFlag()
					addSelfOrOpponentFlag(card.FlagStatOpponent, false)
					addAmount(&(*effect).Amount, "Amount", card.FlagStatInfinity, -bigAmount, bigAmount)
				case card.EffectMultiplyHealing:
					addSelfOrOpponentFlag(card.FlagMultiplyHealingOpponent, false)
					addToggleFlag(card.FlagMultiplyHealingFuture, "Do not apply retroactively")
					addRadioFlag(card.FlagMultiplyHealingTypeMask, "Affect", []flagName{
						{card.FlagMultiplyHealingTypePositive, "Heals Only"},
						{card.FlagMultiplyHealingTypeNegative, "Damage Only"},
						{card.FlagMultiplyHealingTypeAll, "All Health Changes"},
					})
					addAmount(&(*effect).Amount, "Factor", 0, -bigAmount, bigAmount)
				case card.EffectPreventNumb:
					addSelfOrOpponentFlag(card.FlagPreventNumbOpponent, false)
					addAmount(&(*effect).Amount, "Count", 0, 1, bigAmount)
					addFilter()
				case card.EffectModifyAvailableCards:
					addSelfOrOpponentFlag(card.FlagModifyCardsOpponent, false)
					label := createEl("label")
					label.setText("Mode")
					// shorten names
					const (
						TypeMask        = card.FlagModifyCardsTypeMask
						TypeAdd         = card.FlagModifyCardsTypeAdd
						TypeRemove      = card.FlagModifyCardsTypeRemove
						TypeMove        = card.FlagModifyCardsTypeMove
						TypeReplace     = card.FlagModifyCardsTypeReplace
						TargetMask      = card.FlagModifyCardsTargetMask
						TargetAny       = card.FlagModifyCardsTargetAny
						TargetHand      = card.FlagModifyCardsTargetHand
						TargetDeck      = card.FlagModifyCardsTargetDeck
						TargetField     = card.FlagModifyCardsTargetField
						RequireMask     = card.FlagModifyCardsRequireMask
						RequireNone     = card.FlagModifyCardsRequireNone
						RequireSelf     = card.FlagModifyCardsRequireSelf
						RequireOpponent = card.FlagModifyCardsRequireOpponent
					)
					sel := createEl("select")
					sel.appendChild(createOption("Summon To Hand", strconv.FormatUint(uint64(TypeAdd|TargetAny|RequireNone), 10)))
					sel.appendChild(createOption("Add To Hand", strconv.FormatUint(uint64(TypeAdd|TargetHand|RequireNone), 10)))
					sel.appendChild(createOption("Add To Deck", strconv.FormatUint(uint64(TypeAdd|TargetDeck|RequireNone), 10)))
					sel.appendChild(createOption("Add To Field", strconv.FormatUint(uint64(TypeAdd|TargetField|RequireNone), 10)))
					sel.appendChild(createOption("Exile", strconv.FormatUint(uint64(TypeRemove|TargetAny|RequireNone), 10)))
					sel.appendChild(createOption("Exile From Hand", strconv.FormatUint(uint64(TypeRemove|TargetHand|RequireNone), 10)))
					sel.appendChild(createOption("Exile From Deck", strconv.FormatUint(uint64(TypeRemove|TargetDeck|RequireNone), 10)))
					sel.appendChild(createOption("Exile From Field", strconv.FormatUint(uint64(TypeRemove|TargetField|RequireNone), 10)))
					sel.appendChild(createOption("Play", strconv.FormatUint(uint64(TypeMove|TargetAny|RequireNone), 10)))
					sel.appendChild(createOption("Play From Hand", strconv.FormatUint(uint64(TypeMove|TargetHand|RequireNone), 10)))
					sel.appendChild(createOption("Play From Deck", strconv.FormatUint(uint64(TypeMove|TargetDeck|RequireNone), 10)))
					sel.appendChild(createOption("Clone Summon Self To Hand", strconv.FormatUint(uint64(TypeAdd|TargetAny|RequireSelf), 10)))
					sel.appendChild(createOption("Clone Summon Opponent To Hand", strconv.FormatUint(uint64(TypeAdd|TargetAny|RequireOpponent), 10)))
					sel.appendChild(createOption("Clone Self To Hand", strconv.FormatUint(uint64(TypeAdd|TargetHand|RequireSelf), 10)))
					sel.appendChild(createOption("Clone Opponent To Hand", strconv.FormatUint(uint64(TypeAdd|TargetHand|RequireOpponent), 10)))
					sel.appendChild(createOption("Clone Self To Deck", strconv.FormatUint(uint64(TypeAdd|TargetDeck|RequireSelf), 10)))
					sel.appendChild(createOption("Clone Opponent To Deck", strconv.FormatUint(uint64(TypeAdd|TargetDeck|RequireOpponent), 10)))
					sel.appendChild(createOption("Clone Self To Field", strconv.FormatUint(uint64(TypeAdd|TargetField|RequireSelf), 10)))
					sel.appendChild(createOption("Clone Opponent To Field", strconv.FormatUint(uint64(TypeAdd|TargetField|RequireOpponent), 10)))
					sel.appendChild(createOption("Temporary Return From Exile To Hand", strconv.FormatUint(uint64(TypeRemove|TargetAny|RequireSelf), 10)))
					sel.appendChild(createOption("Temporary Return From Opponent's Exile To Hand", strconv.FormatUint(uint64(TypeRemove|TargetAny|RequireOpponent), 10)))
					sel.appendChild(createOption("Return From Exile To Hand", strconv.FormatUint(uint64(TypeRemove|TargetHand|RequireSelf), 10)))
					sel.appendChild(createOption("Return From Opponent's Exile To Hand", strconv.FormatUint(uint64(TypeRemove|TargetHand|RequireOpponent), 10)))
					sel.appendChild(createOption("Return From Exile To Deck", strconv.FormatUint(uint64(TypeRemove|TargetDeck|RequireSelf), 10)))
					sel.appendChild(createOption("Return From Opponent's Exile To Deck", strconv.FormatUint(uint64(TypeRemove|TargetDeck|RequireOpponent), 10)))
					sel.appendChild(createOption("Return From Exile To Field", strconv.FormatUint(uint64(TypeRemove|TargetField|RequireSelf), 10)))
					sel.appendChild(createOption("Return From Opponent's Exile To Field", strconv.FormatUint(uint64(TypeRemove|TargetField|RequireOpponent), 10)))
					sel.appendChild(createOption("Follow Self", strconv.FormatUint(uint64(TypeMove|TargetAny|RequireSelf), 10)))
					sel.appendChild(createOption("Follow Opponent", strconv.FormatUint(uint64(TypeMove|TargetAny|RequireOpponent), 10)))
					sel.appendChild(createOption("Follow Self From Hand", strconv.FormatUint(uint64(TypeMove|TargetHand|RequireSelf), 10)))
					sel.appendChild(createOption("Follow Opponent From Hand", strconv.FormatUint(uint64(TypeMove|TargetHand|RequireOpponent), 10)))
					sel.appendChild(createOption("Follow Self From Deck", strconv.FormatUint(uint64(TypeMove|TargetDeck|RequireSelf), 10)))
					sel.appendChild(createOption("Follow Opponent From Deck", strconv.FormatUint(uint64(TypeMove|TargetDeck|RequireOpponent), 10)))
					sel.appendChild(createOption("Replace", strconv.FormatUint(uint64(TypeReplace|TargetAny|RequireNone), 10)))
					sel.appendChild(createOption("Replace Played", strconv.FormatUint(uint64(TypeReplace|TargetField|RequireNone), 10)))
					sel.appendChild(createOption("Replace In Hand", strconv.FormatUint(uint64(TypeReplace|TargetHand|RequireNone), 10)))
					sel.appendChild(createOption("Replace In Deck", strconv.FormatUint(uint64(TypeReplace|TargetDeck|RequireNone), 10)))
					sel.setValue(strconv.FormatUint(uint64((*effect).Flags&(TypeMask|TargetMask|RequireMask)), 10))
					replaceLabel := createEl("label")
					onChange := js.FuncOf(func(js.Value, []js.Value) interface{} {
						i, _ := strconv.ParseUint(sel.value(), 10, 64)
						(*effect).Flags &^= TypeMask | TargetMask | RequireMask
						(*effect).Flags |= card.EffectFlag(i)
						setCode()

						if card.EffectFlag(i)&TypeMask == TypeReplace {
							replaceLabel.Get("style").Set("display", "")
						} else {
							replaceLabel.Get("style").Set("display", "none")
						}

						return js.Undefined()
					})
					editorFuncs = append(editorFuncs, onChange)
					sel.addEventListener("input", onChange)
					label.appendChild(sel)
					modal.appendChild(label)
					replaceLabel.setText("Replace With")
					replaceSel := createEl("select")
					e.fillCardSelect(replaceSel, nil)
					replaceSel.setValue(strconv.FormatUint(uint64((*effect).Amount3), 10))
					onReplaceChange := js.FuncOf(func(js.Value, []js.Value) interface{} {
						i, _ := strconv.ParseUint(replaceSel.value(), 10, 64)
						(*effect).Amount3 = int64(i)
						setCode()

						return js.Undefined()
					})
					editorFuncs = append(editorFuncs, onReplaceChange)
					replaceSel.addEventListener("input", onReplaceChange)
					replaceLabel.appendChild(replaceSel)
					if (*effect).Flags&TypeMask == TypeReplace {
						replaceLabel.Get("style").Set("display", "")
					} else {
						replaceLabel.Get("style").Set("display", "none")
					}
					modal.appendChild(replaceLabel)
					addAmount(&(*effect).Amount, "Count", 0, 1, bigAmount)
					addFilter()
					addEffect("On Success", &(*effect).Result)
					addEffect("On Failure", &(*effect).Result2)
					addText("Exile Group", &(*effect).Text, false)
				case card.EffectModifyCardCost:
					addRadioFlag(card.FlagModifyCardCostSet, "Modification", []flagName{
						{0, "Relative"},
						{card.FlagModifyCardCostSet, "Absolute"},
					})
					addAmount(&(*effect).Amount, "TP Cost", card.FlagModifyCardCostInfinity, -bigAmount, bigAmount)
					addFilter()
				case card.EffectDrawCard:
					addSelfOrOpponentFlag(card.FlagDrawCardOpponent, false)
					addToggleFlag(card.FlagDrawCardIgnoreMax, "Allow exceeding maximum hand size")
					addAmount(&(*effect).Amount, "Amount", 0, -bigAmount, bigAmount)
					addFilter()
					addEffect("On Draw/Discard", &(*effect).Result)
					addEffect("On Fail", &(*effect).Result2)
				case card.EffectModifyGameRule:
					addRadioFlag(card.FlagModifyGameRulePlayerMask, "Player", []flagName{
						{card.FlagModifyGameRulePlayerSelf, "Self"},
						{card.FlagModifyGameRulePlayerOpponent, "Opponent"},
						{card.FlagModifyGameRulePlayerBoth, "Both Players"},
					})
					addRadioFlag(card.FlagModifyGameRuleRelative, "Change", []flagName{
						{0, "Absolute"},
						{card.FlagModifyGameRuleRelative, "Relative"},
					})
					addAmount(&(*effect).Amount, "Amount", 0, -bigAmount, bigAmount)
					label := createEl("label")
					label.setText("Rule")
					sel := createEl("select")
					sel.appendChild(createOption("Max HP", "1"))
					sel.appendChild(createOption("Min Hand Size", "2"))
					sel.appendChild(createOption("Max Hand Size", "3"))
					sel.appendChild(createOption("Draw Per Turn", "4"))
					sel.appendChild(createOption("Min TP", "6"))
					sel.appendChild(createOption("Max TP", "7"))
					sel.appendChild(createOption("TP Per Turn", "8"))
					sel.setValue(strconv.FormatInt((*effect).Amount2, 10))
					onChange := js.FuncOf(func(_ js.Value, _ []js.Value) interface{} {
						i, _ := strconv.ParseInt(sel.value(), 10, 64)
						(*effect).Amount2 = i
						setCode()

						return js.Undefined()
					})
					editorFuncs = append(editorFuncs, onChange)
					sel.addEventListener("input", onChange)
					label.appendChild(sel)
					modal.appendChild(label)
				case card.EffectDelaySetup:
					addSelfOrOpponentFlag(card.FlagDelaySetupOpponent, false)
					addRadioFlag(card.FlagDelaySetupDelete, "Setups That Reach 0 Rounds", []flagName{
						{0, "Apply"},
						{card.FlagDelaySetupDelete, "Delete"},
					})
					addAmount(&(*effect).Amount, "Number of Rounds", 0, -bigAmount, bigAmount)
					addFilter()
				case card.CondCard:
					addSelfOrOpponentFlag(card.FlagCondCardOpponent, false)
					addRadioFlag(card.FlagCondCardTypeMask, "Condition", []flagName{
						{card.FlagCondCardTypeGreaterEqual, "At Least"},
						{card.FlagCondCardTypeLessThan, "Fewer Than"},
						{card.FlagCondCardTypeEach, "Each"},
					})
					addToggleFlag(card.FlagCondCardAllowNumb, "Include numbed cards")
					addRadioFlag(card.FlagCondCardLocationMask, "Location", []flagName{
						{card.FlagCondCardLocationField, "Field"},
						{card.FlagCondCardLocationHand, "Hand"},
						{card.FlagCondCardLocationDeck, "Deck"},
					})
					addAmount(&(*effect).Amount, "Count", 0, 1, bigAmount)
					addFilter()
					addEffect("Result", &(*effect).Result)
				case card.CondLimit:
					addRadioFlag(card.FlagCondLimitGreaterThan, "Condition", []flagName{
						{0, "Maximum"},
						{card.FlagCondLimitGreaterThan, "Skip First"},
					})
					addAmount(&(*effect).Amount, "Count", 0, 1, bigAmount)
					addEffect("Result", &(*effect).Result)
				case card.CondWinner:
					addRadioFlag(card.FlagCondWinnerTypeMask, "Condition", []flagName{
						{card.FlagCondWinnerTypeWinner, "Won Round"},
						{card.FlagCondWinnerTypeLoser, "Lost Round"},
						{card.FlagCondWinnerTypeTie, "Tied Round"},
						{card.FlagCondWinnerTypeNotTie, "Not Tie"},
						{card.FlagCondWinnerTypeNotWinner, "Lost or Tied Round"},
						{card.FlagCondWinnerTypeNotLoser, "Won or Tied Round"},
					})
					addEffect("Result", &(*effect).Result)
				case card.CondApply:
					delayTurns := int64(0)
					lastEffect := *effect
					actualResult := &(*effect).Result
					show := lastEffect.Flags & card.FlagCondApplyShowMask

					for lastEffect.Flags&card.FlagCondApplyNextRound != 0 {
						delayTurns++

						if lastEffect.Flags&card.FlagCondApplyOpponent != 0 {
							break
						}

						if *actualResult == nil || (*actualResult).Type != card.CondApply || (*actualResult).Flags&card.FlagCondApplyShowMask != show {
							break
						}

						lastEffect = *actualResult
						actualResult = &lastEffect.Result
					}

					label := createEl("label")
					label.setText("Delay (Rounds)")
					input := createInput("number")
					input.setValue(strconv.FormatInt(delayTurns, 10))
					input.Set("min", "0")
					input.Set("max", strconv.FormatInt(bigAmount, 10))
					input.Set("step", "1")
					onChange := js.FuncOf(func(_ js.Value, _ []js.Value) interface{} {
						n, err := strconv.ParseInt(input.value(), 10, 64)
						if err == nil && n >= 0 && n <= bigAmount {
							delayTurns = n
							lastEffect.Priority = (*effect).Priority
							*effect = lastEffect
							if n == 0 {
								lastEffect.Flags &^= card.FlagCondApplyNextRound
							} else {
								lastEffect.Flags |= card.FlagCondApplyNextRound
								for i := int64(1); i < n; i++ {
									*effect = &card.EffectDef{
										Type:     card.CondApply,
										Priority: lastEffect.Priority,
										Flags:    lastEffect.Flags &^ card.FlagCondApplyOpponent,
										Result:   *effect,
									}
									(*effect).Result.Priority = 1
								}
							}
							setCode()
						}

						return js.Undefined()
					})
					editorFuncs = append(editorFuncs, onChange)
					input.addEventListener("input", onChange)
					label.appendChild(input)
					modal.appendChild(label)
					addRadioFlagCustom(card.FlagCondApplyOpponent, "Player", []flagName{
						{0, "Self"},
						{card.FlagCondApplyOpponent, "Opponent"},
					}, lastEffect.Flags, func(mask, set card.EffectFlag) {
						lastEffect.Flags &^= mask
						lastEffect.Flags |= set
					})
					addRadioFlagCustom(card.FlagCondApplyShowMask, "Ghost Card", []flagName{
						{card.FlagCondApplyShowJustEffect, "Just Effect"},
						{card.FlagCondApplyShowOriginalText, "Original Description"},
						{card.FlagCondApplyShowNothing, "Hidden"},
					}, show, func(mask, set card.EffectFlag) {
						for e := *effect; e != *actualResult; e = e.Result {
							e.Flags &^= mask
							e.Flags |= set
						}
					})
					addEffect("Result", actualResult)
				case card.CondCoin:
					addRadioFlag(card.FlagCondCoinStat|card.FlagCondCoinNegative, "Heads Icon", []flagName{
						{0, "Happy Face"},
						{card.FlagCondCoinNegative, "Sad Face"},
						{card.FlagCondCoinStat, "ATK Icon"},
						{card.FlagCondCoinStat | card.FlagCondCoinNegative, "DEF Icon"},
					})
					addAmount(&(*effect).Amount, "Number of Coins", 0, 1, bigAmount)
					addAmount(&(*effect).Amount2, "Chance of Heads", 0, 1, 999999)
					addAmount(&(*effect).Amount3, "Chance of Tails", 0, 0, 999999)
					addEffect("Heads Effect", &(*effect).Result)
					addEffect("Tails Effect", &(*effect).Result2)
				case card.CondStat:
					addSelfOrOpponentFlag(card.FlagCondStatOpponent, false)
					addRadioFlag(card.FlagCondStatTypeMask, "Stat", []flagName{
						{card.FlagCondStatTypeATK, "ATK"},
						{card.FlagCondStatTypeDEF, "DEF"},
						{card.FlagCondStatTypeHP, "HP"},
						{card.FlagCondStatTypeTP, "TP"},
					})
					addRadioFlag(card.FlagCondStatMathMask, "Math", []flagName{
						{card.FlagCondStatMathNone, "None"},
						{card.FlagCondStatMathAdd, "Add"},
						{card.FlagCondStatMathSubtract, "Subtract"},
					})
					addRadioFlag(card.FlagCondStatMathOpponent, "Math Player", []flagName{
						{0, "Self"},
						{card.FlagCondStatMathOpponent, "Opponent"},
					})
					addRadioFlag(card.FlagCondStatMathTypeMask, "Math Stat", []flagName{
						{card.FlagCondStatMathTypeATK, "ATK"},
						{card.FlagCondStatMathTypeDEF, "DEF"},
						{card.FlagCondStatMathTypeHP, "HP"},
						{card.FlagCondStatMathTypeTP, "TP"},
					})
					addRadioFlag(card.FlagCondStatLessThan, "Condition", []flagName{
						{0, "At Least"},
						{card.FlagCondStatLessThan, "Less Than"},
					})
					addAmount(&(*effect).Amount, "Amount", 0, -bigAmount, bigAmount)
					addToggleFlag(card.FlagCondStatMultiple, "Apply effect multiple times")
					addEffect("Result", &(*effect).Result)
				case card.CondInHand:
					addRadioFlag(card.FlagCondInHandHide, "Display To Opponent", []flagName{
						{0, "Flip Face-Up"},
						{card.FlagCondInHandHide, "Keep Face-Down"},
					})
					addRadioFlag(card.FlagCondInHandOnPlayMask, "When Played", []flagName{
						{card.FlagCondInHandOnPlayNone, "No Effect"},
						{card.FlagCondInHandOnPlayAlso, "Apply Regardless"},
						{card.FlagCondInHandOnPlayOnly, "Only When Played"},
					})
					addAmount(&(*effect).Amount, "Skip Rounds", 0, 0, bigAmount)
					addAmount(&(*effect).Amount2, "Max. Rounds", 0, 0, bigAmount)
					addEffect("Result", &(*effect).Result)
				case card.CondLastEffect:
					addEffect("Effect", &(*effect).Result)
				case card.CondOnNumb:
					addEffect("Result", &(*effect).Result)
				case card.CondMultipleEffects:
					addEffect("Effect 1", &(*effect).Result)
					addEffect("Effect 2", &(*effect).Result2)
				case card.CondOnDiscard:
					addEffect("Result", &(*effect).Result)
				case card.CondCompareTargetCard:
					addRadioFlag(card.FlagCondCompareTargetCardInvert, "Apply Result If", []flagName{
						{0, "Target Matches"},
						{card.FlagCondCompareTargetCardInvert, "Target Does Not Match"},
					})
					addFilter()
					addEffect("Result", &(*effect).Result)
				case card.CondOnExile:
					addToggleFlag(card.FlagCondOnExileGroup, "Filter by Group")
					addText("Group", &(*effect).Text, false)
					addEffect("Result", &(*effect).Result)
				default:
					panic("missing editor for effect type " + (*effect).Type.String())
				}
			}

			modal.appendChild(createEl("br"))
			if *effect != nil {
				modal.appendChild(deleteButton)
			}
			modal.appendChild(doneButton)
		}

		onEffectCodeChange := js.FuncOf(func(_ js.Value, _ []js.Value) interface{} {
			b, err := base64.StdEncoding.DecodeString(effectCode.Get("value").String())
			if err != nil {
				effectCode.Call("setCustomValidity", err.Error())
				effectCode.Call("reportValidity")

				return js.Undefined()
			}
			if len(b) == 0 {
				effectCode.Call("setCustomValidity", "")
				*effect = nil
				resetUI()

				return js.Undefined()
			}
			defer func() {
				format.Catch(&err)
				if err != nil {
					effectCode.Call("setCustomValidity", err.Error())
					effectCode.Call("reportValidity")
				}
			}()
			var r format.Reader
			r.Init(b)
			formatVersion := r.UVarInt()
			var eff card.EffectDef
			if err := eff.Unmarshal(&r, formatVersion); err != nil {
				effectCode.Call("setCustomValidity", err.Error())
				effectCode.Call("reportValidity")

				return js.Undefined()
			}
			if r.Len() != 0 {
				effectCode.Call("setCustomValidity", "extra data after end of effect")
				effectCode.Call("reportValidity")

				return js.Undefined()
			}
			effectCode.Call("setCustomValidity", "")
			*effect = &eff
			resetUI()

			return js.Undefined()
		})
		defer onEffectCodeChange.Release()
		effectCode.addEventListener("change", onEffectCodeChange)

		onPriorityChange := js.FuncOf(func(_ js.Value, _ []js.Value) interface{} {
			if priorityInput.Call("reportValidity").Bool() {
				(*effect).Priority = uint8(priorityInput.Get("valueAsNumber").Int())
				setCode()
				updatePriorityHint()
			}

			return js.Undefined()
		})
		defer onPriorityChange.Release()
		priorityInput.addEventListener("input", onPriorityChange)

		onDeleteClick := js.FuncOf(func(_ js.Value, args []js.Value) interface{} {
			args[0].Call("preventDefault")
			*effect = nil
			setCode()
			resetUI()

			return js.Undefined()
		})
		defer onDeleteClick.Release()
		deleteButton.addEventListener("click", onDeleteClick)

		setCode()
		resetUI()
		<-done
	})
}
