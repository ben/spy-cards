//go:build js && wasm
// +build js,wasm

package editor

import (
	"context"
	"fmt"
	"net/url"
	"strconv"
	"strings"
	"syscall/js"
	"time"

	"git.lubar.me/ben/spy-cards/card"
	"git.lubar.me/ben/spy-cards/internal/router"
	"git.lubar.me/ben/spy-cards/match"
)

type editor struct {
	ctx                      context.Context
	exit                     chan *router.PageInfo
	edit                     string
	encodeDebounce           *time.Timer
	mainWrapper              element
	header                   element
	editModeButton           element
	addCardButton            element
	publishButton            element
	cardCodes                element
	set                      *card.Set
	nav                      element
	main                     element
	cardList                 draggable
	cardTribes               draggable
	cardEffects              draggable
	cardExtensions           draggable
	modeFields               draggable
	variantRules             draggable
	filterComponents         draggable
	createCardClick          js.Func
	deleteCardClick          js.Func
	cardNameChanged          js.Func
	cardRankChanged          js.Func
	cardTPChanged            js.Func
	duplicateCardClick       js.Func
	changePortraitClick      js.Func
	selectPortraitClick      js.Func
	uploadPortraitClick      js.Func
	uploadPortraitChange     js.Func
	addCardTribe             js.Func
	addCardEffect            js.Func
	addCardExtension         js.Func
	cardRestrictionsChanged  js.Func
	modeAuthorChanged        js.Func
	modeDescriptionChanged   js.Func
	modeLatestChangesChanged js.Func
	modeAddField             js.Func
	ignoreHashChange         bool
	resetNavOnSave           bool
}

func Run(ctx context.Context, initialCodes, initialMode string) (*router.PageInfo, error) {
	go getCommunityPortraits(ctx)
	go getCustomExtensions(ctx)

	e := &editor{ctx: ctx, exit: make(chan *router.PageInfo, 1)}

	e.mainWrapper = createEl("form", "card-editor")

	body.appendChild(e.mainWrapper)
	defer body.removeChild(e.mainWrapper)

	onBadClick := js.FuncOf(func(_ js.Value, args []js.Value) interface{} {
		args[0].Call("preventDefault")
		window.Call("alert", "Oh no! Ben forgot to wire up this button. Tell Ben which button you just clicked.")

		return js.Undefined()
	})
	defer onBadClick.Release()
	e.mainWrapper.addEventListener("submit", onBadClick)

	e.header = createEl("header")
	e.mainWrapper.appendChild(e.header)

	e.nav = createEl("nav")
	e.mainWrapper.appendChild(e.nav)

	e.main = createEl("main")
	e.mainWrapper.appendChild(e.main)

	e.cardCodes = createInput("text")
	e.cardCodes.Set("placeholder", "Card Codes")
	e.header.appendChild(e.cardCodes)

	onClick := func(el element, cb func()) js.Func {
		f := js.FuncOf(func(_ js.Value, args []js.Value) interface{} {
			args[0].Call("preventDefault")
			go cb()

			return js.Undefined()
		})

		el.addEventListener("click", f)

		return f
	}

	e.editModeButton = createEl("button")
	e.editModeButton.Set("textContent", "Edit Mode")
	e.header.appendChild(e.editModeButton)

	defer onClick(e.editModeButton, e.setModeEditor).Release()

	e.addCardButton = createEl("button")
	e.addCardButton.Set("textContent", "Add Card")
	e.header.appendChild(e.addCardButton)

	defer onClick(e.addCardButton, e.onAddCardClicked).Release()

	// TODO: make this into a menu with options like "Publish"
	e.publishButton = createEl("button")
	e.publishButton.Set("textContent", "Close Editor")
	e.header.appendChild(e.publishButton)

	defer onClick(e.publishButton, e.onPublishClicked).Release()

	onCardCodesFocus := js.FuncOf(func(_ js.Value, _ []js.Value) interface{} {
		if e.encodeDebounce != nil {
			e.encodeCardCodes()
		}
		e.cardCodes.Call("select")

		return js.Undefined()
	})
	defer onCardCodesFocus.Release()
	e.cardCodes.addEventListener("focus", onCardCodesFocus)

	var debounceCodeChange *time.Timer

	onCardCodesChanged := js.FuncOf(func(_ js.Value, _ []js.Value) interface{} {
		if debounceCodeChange != nil {
			debounceCodeChange.Stop()
		}
		debounceCodeChange = time.AfterFunc(time.Second, func() {
			e.parseCardCodes(false)
		})

		return js.Undefined()
	})
	defer onCardCodesChanged.Release()
	e.cardCodes.addEventListener("input", onCardCodesChanged)

	onCardCodesChanged2 := js.FuncOf(func(_ js.Value, _ []js.Value) interface{} {
		if debounceCodeChange != nil {
			debounceCodeChange.Stop()
			debounceCodeChange = nil
			go e.parseCardCodes(true)
		}

		return js.Undefined()
	})
	defer onCardCodesChanged2.Release()
	e.cardCodes.addEventListener("blur", onCardCodesChanged2)

	onHashChange := js.FuncOf(func(_ js.Value, _ []js.Value) interface{} {
		if e.ignoreHashChange {
			e.ignoreHashChange = false

			return js.Undefined()
		}

		hash := window.Get("location").Get("hash").String()
		e.cardCodes.Set("value", strings.TrimPrefix(hash, "#"))
		go e.parseCardCodes(true)

		return js.Undefined()
	})
	defer onHashChange.Release()
	window.Call("addEventListener", "hashchange", onHashChange)

	initialize := func(codes, mode string) {
		e.cardCodes.Set("value", codes)
		e.parseCardCodes(true)

		if mode == "mode" {
			e.setModeEditor()
		} else if id, err := strconv.ParseUint(mode, 10, 64); err == nil {
			e.setCardEditor(card.ID(id))
		} else {
			e.reset("")
		}
	}

	onPopState := js.FuncOf(func(_ js.Value, _ []js.Value) interface{} {
		hash := window.Get("location").Get("hash").String()
		query := window.Get("location").Get("search").String()
		q, _ := url.ParseQuery(strings.TrimPrefix(query, "?"))
		go initialize(strings.TrimPrefix(hash, "#"), q.Get("mode"))

		return js.Undefined()
	})
	defer onPopState.Release()
	window.Call("addEventListener", "popstate", onPopState)

	e.createCardClick = js.FuncOf(func(_ js.Value, args []js.Value) interface{} {
		args[0].Call("preventDefault")
		go e.createCard(card.Rank(window.Call("parseInt", args[0].Get("target").Get("value"), 10).Int()))

		return js.Undefined()
	})
	defer e.createCardClick.Release()

	initialize(initialCodes, initialMode)

	return <-e.exit, nil
}

func (e *editor) onPublishClicked() {
	b, err := e.set.MarshalText()
	if err != nil {
		panic(err)
	}

	e.exit <- &router.PageInfo{
		Page:   router.PageCardsHome,
		Mode:   match.ModeCustom,
		Custom: string(b),
	}
}

func (e *editor) reset(edit string) {
	e.main.Set("innerHTML", "")

	if e.edit != edit {
		e.edit = edit
		e.encodeCardCodes()
	}

	e.resetNav()
}

func (e *editor) resetNav() {
	e.nav.Set("innerHTML", "")

	if e.set == nil {
		return
	}

	vanillaBanned := false

	for _, f := range e.set.Mode.GetAll(card.FieldBannedCards) {
		b := f.(*card.BannedCards)
		if b.Flags&card.BannedCardTypeMask == card.BannedCardTypeBanned && len(b.Cards) == 0 {
			vanillaBanned = true

			break
		}
	}

	groupFields := e.set.Mode.GetAll(card.FieldGroup)
	groups := make([]struct {
		group *card.Group
		ul    element
		add   element
	}, len(groupFields))

	for i, f := range groupFields {
		groups[i].group = f.(*card.Group)
	}

	cards, newCard := e.cardList.setup(e, func(el element) {
		if el.hasClass("card-id-conflict") {
			index, err := strconv.Atoi(el.Call("getAttribute", "data-card-conflict-index").String())
			if err == nil {
				e.set.Cards[index].ID = e.nextCardID(e.set.Cards[index].Rank)
				e.encodeCardCodes()
			}
		}

		if el.hasClass("group") {
			e.setModeEditor()
			i, err := strconv.Atoi(el.Call("getAttribute", "data-group").String())
			if err == nil {
				go e.setFieldEditor(groups[i].group, nil)
			}

			return
		}

		id, err := strconv.ParseUint(el.Call("getAttribute", "data-id").String(), 10, 64)
		if err == nil {
			e.setCardEditor(card.ID(id))
		}
	}, func(el element) int {
		for i := range groups {
			if el.Equal(groups[i].ul.Get("parentNode")) {
				if len(groups[i].group.Cards) == 0 {
					before := 0
					for j := range groups {
						if j < i || len(groups[j].group.Cards) != 0 {
							before++
						}
					}

					return len(e.set.Cards) + before*2
				}

				for j, c := range e.set.Cards {
					if c.ID == groups[i].group.Cards[0] {
						return j + i*2
					}
				}

				panic("internal editor error: could not find first card in group")
			}

			if el.Equal(groups[i].add.Value) {
				if len(groups[i].group.Cards) == 0 {
					before := 0
					for j := range groups {
						if j < i || len(groups[j].group.Cards) != 0 {
							before++
						}
					}

					return len(e.set.Cards) + before*2 + 1
				}

				for j, c := range e.set.Cards {
					if c.ID == groups[i].group.Cards[len(groups[i].group.Cards)-1] {
						return j + i*2 + len(groups[i].group.Cards) + 1
					}
				}

				panic("internal editor error: could not find last card in group")
			}
		}

		id, err := strconv.ParseUint(el.Call("getAttribute", "data-id").String(), 10, 64)
		if err != nil {
			return -1
		}

		groupOffset := 0
		groupIndex := -1
		for i, c := range e.set.Cards {
			if groupIndex == -1 {
				for j := range groups {
					for _, id := range groups[j].group.Cards {
						if id == c.ID {
							groupOffset++
							groupIndex = j

							break
						}
					}

					if groupIndex != -1 {
						break
					}
				}
			} else {
				found := false
				for _, id := range groups[groupIndex].group.Cards {
					if c.ID == id {
						found = true

						break
					}
				}

				if !found {
					groupOffset++
					groupIndex = -1
				}
			}

			if c.ID == card.ID(id) {
				return i + groupOffset
			}
		}

		return -1
	}, func(_, _ int, del, sel element, after bool) bool {
		find := func(el element) (group, groupIndex, cardIndex int) {
			if el.hasClass("group-placeholder") {
				group, err := strconv.Atoi(el.Get("parentNode").Get("parentNode").Call("getAttribute", "data-group").String())
				if err == nil {
					return group, -2, -1
				}
			}

			if el.hasClass("group") {
				group, err := strconv.Atoi(el.Call("getAttribute", "data-group").String())
				if err == nil {
					return group, -1, -1
				}
			}

			id, err := strconv.ParseUint(el.Call("getAttribute", "data-id").String(), 10, 64)
			if err != nil {
				return -1, -1, -1
			}

			group, groupIndex, cardIndex = -1, -1, -1
			for i, c := range e.set.Cards {
				if c.ID == card.ID(id) {
					cardIndex = i

					break
				}
			}

			if cardIndex == -1 {
				return
			}

			for i := range groups {
				for j, cid := range groups[i].group.Cards {
					if cid == card.ID(id) {
						group = i
						groupIndex = j

						return
					}
				}
			}

			return
		}

		dg, dgi, dci := find(del)
		sg, sgi, sci := find(sel)

		defer func() {
			if e.resetNavOnSave {
				return
			}

			go func() {
				list := del
				for !list.hasClass("drag-list") {
					list.Value = list.Get("parentNode")
				}

				actual := make([]string, 0, len(groups)+len(e.set.Cards))
				expected := make([]string, 0, len(groups)+len(e.set.Cards))

				for el := list.Get("firstChild"); el.Truthy(); el = el.Get("nextSibling") {
					if el.Get("classList").Call("contains", "is-vanilla").Bool() {
						continue
					}

					if el.Get("classList").Call("contains", "group").Bool() {
						label := el.Get("firstChild")
						actual = append(actual, "Group: "+label.Get("textContent").String())

						for gel := label.Get("nextSibling").Get("firstChild"); gel.Truthy(); gel = gel.Get("nextSibling") {
							if gel.Get("classList").Call("contains", "group-placeholder").Bool() {
								actual = append(actual, "Group-End")

								continue
							}

							id, _ := strconv.ParseUint(gel.Call("getAttribute", "data-id").String(), 10, 64)
							actual = append(actual, "Card: "+strconv.FormatUint(id, 10)+" "+e.set.Card(card.ID(id)).DisplayName())
						}

						continue
					}

					id, _ := strconv.ParseUint(el.Call("getAttribute", "data-id").String(), 10, 64)
					actual = append(actual, "Card: "+strconv.FormatUint(id, 10)+" "+e.set.Card(card.ID(id)).DisplayName())
				}

				for _, c := range e.set.Cards {
					for _, g := range groups {
						if len(g.group.Cards) != 0 && g.group.Cards[0] == c.ID {
							expected = append(expected, "Group: "+g.group.Title)
						}
					}

					expected = append(expected, "Card: "+strconv.FormatUint(uint64(c.ID), 10)+" "+c.DisplayName())

					for _, g := range groups {
						if len(g.group.Cards) != 0 && g.group.Cards[len(g.group.Cards)-1] == c.ID {
							expected = append(expected, "Group-End")
						}
					}
				}

				for _, g := range groups {
					if len(g.group.Cards) != 0 {
						continue
					}
					expected = append(expected, "Group: "+g.group.Title)
					expected = append(expected, "Group-End")
				}

				if len(actual) != len(expected) {
					panic("Card order invariant violation! (length mismatch)")
				}

				for i := 0; i < len(actual); i++ {
					if actual[i] != expected[i] {
						var b []byte

						b = append(b, "Card order invariant violation!\n\n"...)
						b = strconv.AppendInt(append(b, "dg "...), int64(dg), 10)
						b = strconv.AppendInt(append(b, " dgi "...), int64(dgi), 10)
						b = strconv.AppendInt(append(b, " dci "...), int64(dci), 10)
						b = strconv.AppendInt(append(b, " sg "...), int64(sg), 10)
						b = strconv.AppendInt(append(b, " sgi "...), int64(sgi), 10)
						b = strconv.AppendInt(append(b, " sci "...), int64(sci), 10)
						b = append(b, "\n\n"...)
						for j := 0; j < len(actual); j++ {
							b = strconv.AppendInt(b, int64(j), 10)
							b = append(b, ": "...)
							b = append(b, actual[j]...)
							b = append(b, " | "...)
							b = append(b, expected[j]...)
							b = append(b, '\n')
						}

						panic(string(b))
					}
				}
			}()
		}()

		var def *card.Def
		if sci != -1 {
			def = e.set.Cards[sci]
		}

		dragGroup := func() {
			for i, c := range e.set.Cards {
				if c.ID == groups[sg].group.Cards[0] {
					sci = i

					break
				}
			}

			if sci == -1 {
				panic("internal editor error: could not find first card in source group")
			}

			if dci >= sci && dci < sci+len(groups[sg].group.Cards) {
				// target intersects group's current position
				return
			}

			toMove := make([]*card.Def, len(groups[sg].group.Cards))
			copy(toMove, e.set.Cards[sci:])

			if sci < dci {
				copy(e.set.Cards[sci:], e.set.Cards[sci+len(toMove):dci])
				copy(e.set.Cards[dci-len(toMove):], toMove)
			} else {
				copy(e.set.Cards[dci+len(toMove):], e.set.Cards[dci:sci])
				copy(e.set.Cards[dci:], toMove)
			}

			if after {
				// move navigation node
				del.Get("parentNode").Call("insertBefore", sel.Value, del.Value)
			} else {
				// move navigation node
				del.Get("parentNode").Call("insertBefore", sel.Value, del.Get("nextSibling"))
			}
		}

		switch {
		case dg == -1 && dci == -1:
			// dragging onto something that isn't a group or card (disallowed)

			return false
		case sci != -1 && dci != -1:
			// drag a card to card

			if sg != -1 {
				// remove from old group
				groups[sg].group.Cards = append(groups[sg].group.Cards[:sgi], groups[sg].group.Cards[sgi+1:]...)
			}

			if dg != -1 {
				if sg != dg && sci < dci {
					dgi++
				}

				// add to new group
				groups[dg].group.Cards = append(groups[dg].group.Cards[:dgi], append([]card.ID{def.ID}, groups[dg].group.Cards[dgi:]...)...)
			}

			// move card within set
			e.set.Cards = append(e.set.Cards[:sci], e.set.Cards[sci+1:]...)
			e.set.Cards = append(e.set.Cards[:dci], append([]*card.Def{def}, e.set.Cards[dci:]...)...)

			// move navigation node
			return true
		case dg != -1 && sg == dg && dgi == -1 && sci != -1:
			// drag a card out the top of a group

			// remove from group
			groups[sg].group.Cards = append(groups[sg].group.Cards[:sgi], groups[sg].group.Cards[sgi+1:]...)

			if len(groups[sg].group.Cards) == 0 {
				// group may need to move after other groups
				e.resetNavOnSave = true
			}

			dci = sci - sgi

			// move card within set
			e.set.Cards = append(e.set.Cards[:sci], e.set.Cards[sci+1:]...)
			e.set.Cards = append(e.set.Cards[:dci], append([]*card.Def{def}, e.set.Cards[dci:]...)...)

			// move navigation node
			groups[sg].ul.Get("parentNode").Get("parentNode").Call("insertBefore", sel.Value, groups[sg].ul.Get("parentNode"))

			return false
		case dg != -1 && dgi == -1 && sci != -1:
			// drag a card in the top of a group

			if sg != -1 {
				// remove from source group
				groups[sg].group.Cards = append(groups[sg].group.Cards[:sgi], groups[sg].group.Cards[sgi+1:]...)

				if len(groups[sg].group.Cards) == 0 {
					// group may need to move after other groups
					e.resetNavOnSave = true
				}
			}

			if len(groups[dg].group.Cards) == 0 {
				// group may need to move above other empty groups
				e.resetNavOnSave = true

				// move card to end of set
				e.set.Cards = append(e.set.Cards[:sci], e.set.Cards[sci+1:]...)
				e.set.Cards = append(e.set.Cards, def)
			} else {
				// move card before first card in group
				e.set.Cards = append(e.set.Cards[:sci], e.set.Cards[sci+1:]...)

				found := false

				for i, c := range e.set.Cards {
					if c.ID == groups[dg].group.Cards[0] {
						e.set.Cards = append(e.set.Cards[:i], append([]*card.Def{def}, e.set.Cards[i:]...)...)
						found = true

						break
					}
				}

				if !found {
					panic("internal editor error: could not find first card in group")
				}
			}

			// insert card at start of group
			groups[dg].group.Cards = append([]card.ID{def.ID}, groups[dg].group.Cards...)

			// move navigation node
			groups[dg].ul.Call("insertBefore", sel.Value, groups[dg].ul.Get("firstChild"))

			return false
		case dg != -1 && sg == dg && dgi == -2 && sci != -1:
			// drag a card out the bottom of a group

			// remove from group
			groups[sg].group.Cards = append(groups[sg].group.Cards[:sgi], groups[sg].group.Cards[sgi+1:]...)

			if len(groups[sg].group.Cards) == 0 {
				// group may need to move after other groups
				e.resetNavOnSave = true
			}

			dci = sci + len(groups[sg].group.Cards) - sgi

			// move card within set
			e.set.Cards = append(e.set.Cards[:sci], e.set.Cards[sci+1:]...)
			e.set.Cards = append(e.set.Cards[:dci], append([]*card.Def{def}, e.set.Cards[dci:]...)...)

			// move navigation node
			groups[sg].ul.Get("parentNode").Get("parentNode").Call("insertBefore", sel.Value, groups[sg].ul.Get("parentNode").Get("nextSibling"))

			return false
		case dg != -1 && dgi == -2 && sci != -1:
			// drag a card in the bottom of a group

			if sg != -1 {
				// remove from source group
				groups[sg].group.Cards = append(groups[sg].group.Cards[:sgi], groups[sg].group.Cards[sgi+1:]...)

				if len(groups[sg].group.Cards) == 0 {
					// group may need to move after other groups
					e.resetNavOnSave = true
				}
			}

			if len(groups[dg].group.Cards) == 0 {
				// group may need to move above other empty groups
				e.resetNavOnSave = true

				// move card to end of set
				e.set.Cards = append(e.set.Cards[:sci], e.set.Cards[sci+1:]...)
				e.set.Cards = append(e.set.Cards, def)
			} else {
				// move card before first card in group
				e.set.Cards = append(e.set.Cards[:sci], e.set.Cards[sci+1:]...)

				found := false

				for i, c := range e.set.Cards {
					if c.ID == groups[dg].group.Cards[len(groups[dg].group.Cards)-1] {
						e.set.Cards = append(e.set.Cards[:i+1], append([]*card.Def{def}, e.set.Cards[i+1:]...)...)
						found = true

						break
					}
				}

				if !found {
					panic("internal editor error: could not find last card in group")
				}
			}

			// insert card at end of group
			groups[dg].group.Cards = append(groups[dg].group.Cards, def.ID)

			// move navigation node
			groups[dg].ul.Call("insertBefore", sel.Value, groups[dg].add.Value)

			return false
		case sg != -1 && sgi == -1 && len(groups[sg].group.Cards) == 0:
			// move an empty group (not allowed)

			return false
		case sg != -1 && sgi == -1 && dg != -1 && len(groups[dg].group.Cards) == 0:
			// move a group to an empty group (not allowed)

			return false
		case sg != -1 && dg != -1 && dgi == -1 && sci == -1:
			// drag a group to the top of a group

			if !after {
				return false
			}

			for i, c := range e.set.Cards {
				if c.ID == groups[dg].group.Cards[0] {
					dci = i

					break
				}
			}

			if dci == -1 {
				panic("internal editor error: could not find first card in destination group")
			}

			dragGroup()

			return false
		case sg != -1 && dg != -1 && dgi == -2 && sci == -1:
			// drag a group to the bottom of a group

			if after {
				return false
			}

			for i, c := range e.set.Cards {
				if c.ID == groups[dg].group.Cards[len(groups[dg].group.Cards)-1] {
					dci = i + 1

					break
				}
			}

			if dci == -1 {
				panic("internal editor error: could not find last card in destination group")
			}

			del.Value = del.Get("parentNode").Get("parentNode")

			dragGroup()

			return false
		case sg != -1 && sgi == -1 && dci != -1 && dg != -1:
			// drag a group to a card in a group (not allowed)

			return false
		case sg != -1 && sgi == -1 && dci != -1 && dg == -1:
			// drag a group to a card

			if !after {
				dci++
			}

			dragGroup()

			return false
		default:
			panic(fmt.Sprint("internal editor error: unhandled navigation drag case: dg ", dg, " dgi ", dgi, " dci ", dci, " sg ", sg, " sgi ", sgi, " sci ", sci))
		}
	})
	cards.Call("setAttribute", "role", "treegrid")
	e.nav.appendChild(cards)

	seenIDs := make(map[card.ID]bool)

	makeGroup := func(g *card.Group) (ul, add element) {
		group := newCard(true)
		group.addClass("group")
		group.Call("setAttribute", "role", "group")

		for i := range groups {
			if groups[i].group == g {
				group.Call("setAttribute", "data-group", i)

				break
			}
		}

		title := createEl("span", "group-title")
		title.setText(g.Title)
		group.appendChild(title)

		ul = createEl("ul", "sub-drag-list")
		group.appendChild(ul)

		add = newCard(true)
		add.addClass("group-placeholder")
		ul.appendChild(add)

		return
	}

	makeCard := func(i int, c *card.Def, isCustom bool) {
		el := newCard(isCustom)
		el.addClass("card-nav")
		el.Call("setAttribute", "role", "treeitem")

		if seenIDs[c.ID] {
			el.addClass("card-id-conflict")
			el.Set("title", "This card's ID is already used by a different card. Click to fix.")
			el.Call("setAttribute", "data-card-conflict-index", i)
		}

		seenIDs[c.ID] = true

		if isCustom {
			el.addClass("is-custom")
		} else {
			el.addClass("is-vanilla")
		}

		el.addClass("rank-" + c.Rank.String())
		el.setText(c.DisplayName())

		el.Call("setAttribute", "data-id", strconv.FormatUint(uint64(c.ID), 10))

		el.Call("insertBefore", e.makePortrait(c.Portrait, c.CustomPortrait).Value, el.Get("firstChild"))

		for i := range groups {
			found := false

			for _, id := range groups[i].group.Cards {
				if c.ID == id {
					found = true

					break
				}
			}

			if !found {
				continue
			}

			if !groups[i].ul.Truthy() {
				groups[i].ul, groups[i].add = makeGroup(groups[i].group)
			}

			groups[i].ul.Call("insertBefore", el.Value, groups[i].add.Value)

			break
		}
	}

	if !vanillaBanned {
		for _, back := range []card.Rank{card.Boss, card.MiniBoss, card.Enemy} {
			for _, id := range card.VanillaOrder(back) {
				overridden := false

				for _, c := range e.set.Cards {
					if c.ID == id {
						overridden = true

						break
					}
				}

				if !overridden {
					makeCard(-1, e.set.Card(id), false)
				}
			}
		}
	}

	for i, c := range e.set.Cards {
		makeCard(i, c, true)
	}

	for i := range groups {
		if !groups[i].ul.Truthy() {
			groups[i].ul, groups[i].add = makeGroup(groups[i].group)
		}
	}
}

func (e *editor) fillCardSelect(sel element, variant *card.Variant) {
	vanillaBanned := false

	for _, f := range e.set.Mode.GetAll(card.FieldBannedCards) {
		bc := f.(*card.BannedCards)
		if bc.Flags == 0 && len(bc.Cards) == 0 {
			vanillaBanned = true

			break
		}
	}

	if !vanillaBanned && variant != nil {
		for _, r := range variant.Rules {
			if bc, ok := r.(*card.BannedCards); ok && bc.Flags == 0 && len(bc.Cards) == 0 {
				vanillaBanned = true

				break
			}
		}
	}

	if !vanillaBanned {
		for _, back := range []card.Rank{card.Boss, card.MiniBoss, card.Enemy} {
			for _, id := range card.VanillaOrder(back) {
				overridden := false

				for _, c := range e.set.Cards {
					if c.ID == id {
						overridden = true

						break
					}
				}

				if !overridden {
					sel.appendChild(createOption(e.set.Card(id).DisplayName(), strconv.FormatUint(uint64(id), 10)))
				}
			}
		}
	}

	for _, c := range e.set.Cards {
		sel.appendChild(createOption(c.DisplayName(), strconv.FormatUint(uint64(c.ID), 10)))
	}
}
