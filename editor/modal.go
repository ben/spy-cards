//go:build js && wasm
// +build js,wasm

package editor

import "syscall/js"

func (e *editor) modal(cb func(element)) {
	e.mainWrapper.addClass("modal-open")

	defer func() {
		if !document.Call("querySelector", ".card-editor-modal").Truthy() {
			e.mainWrapper.removeClass("modal-open")
		}
	}()

	modal := createEl("form", "card-editor-modal")

	onBadClick := js.FuncOf(func(_ js.Value, args []js.Value) interface{} {
		args[0].Call("preventDefault")
		window.Call("alert", "Oh no! Ben forgot to wire up this button. Tell Ben which button you just clicked.")

		return js.Undefined()
	})
	defer onBadClick.Release()
	modal.Call("addEventListener", "submit", onBadClick)

	body.appendChild(modal)
	defer body.removeChild(modal)

	cb(modal)
}
