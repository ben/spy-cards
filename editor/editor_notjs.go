//go:build !js || !wasm
// +build !js !wasm

package editor

import (
	"context"
	"errors"

	"git.lubar.me/ben/spy-cards/internal/router"
)

func Run(context.Context, string, string) (*router.PageInfo, error) {
	return nil, errors.New("not implemented")
}
