//go:build js && wasm
// +build js,wasm

package editor

import (
	"bytes"
	"log"
	"syscall/js"
	"time"

	"git.lubar.me/ben/spy-cards/card"
)

func (e *editor) parseCardCodes(setCodeInput bool) {
	raw := e.cardCodes.Get("value").String()

	var sets card.Sets
	err := sets.UnmarshalText([]byte(raw))

	var set *card.Set
	if err == nil {
		set, err = sets.Apply()
	}

	if err != nil {
		e.cardCodes.Call("setCustomValidity", err.Error())
		e.cardCodes.Call("reportValidity")

		return
	}

	e.cardCodes.Call("setCustomValidity", "")
	e.set = set
	e.edit = ""
	e.reset("")

	if setCodeInput {
		e.encodeCardCodes()
	}
}

func (e *editor) encodeCardCodes() {
	if e.encodeDebounce != nil {
		e.encodeDebounce.Stop()
		e.encodeDebounce = nil
	}

	b, err := e.set.MarshalText()
	if err != nil {
		log.Println("ERROR: marshaling card codes failed:", err)

		return
	}

	emptyModePrefix := []byte("AwA=,")
	blankMode := []byte("AwEABAAAAAA=")

	switch {
	case bytes.Equal(b, blankMode):
		b = nil
	case bytes.HasPrefix(b, blankMode) && b[len(blankMode)] == ',':
		b = b[len(blankMode)+1:]
	default:
		b = bytes.TrimPrefix(b, emptyModePrefix)
	}

	buf := []byte("/game.html?custom=editor")

	if e.edit != "" {
		buf = append(buf, "&edit="...)
		buf = append(buf, e.edit...)
	}

	if len(b) != 0 {
		buf = append(buf, '#')
		buf = append(buf, b...)
	}

	if window.Get("location").Get("search").String()+window.Get("location").Get("hash").String() != string(buf) {
		window.Get("history").Call("pushState", js.Null(), "", string(buf))
	}

	e.cardCodes.Set("value", string(b))
	e.cardCodes.Call("setCustomValidity", "")

	if e.resetNavOnSave {
		e.resetNavOnSave = false
		e.resetNav()
	}
}

func (e *editor) encodeCardCodesSoon() {
	if e.encodeDebounce != nil {
		e.encodeDebounce.Stop()
	}

	e.encodeDebounce = time.AfterFunc(2*time.Second, e.encodeCardCodes)
}
