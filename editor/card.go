//go:build js && wasm
// +build js,wasm

package editor

import (
	"bytes"
	"context"
	"encoding/base32"
	"fmt"
	"log"
	"strconv"
	"strings"
	"sync"
	"syscall/js"

	"git.lubar.me/ben/spy-cards/card"
	"git.lubar.me/ben/spy-cards/gfx/sprites"
	"git.lubar.me/ben/spy-cards/internal"
)

func (e *editor) onAddCardClicked() {
	e.reset("")

	selectRank := createEl("p")
	selectRank.setText("Select a rank for the new card.")
	e.main.appendChild(selectRank)

	for _, rank := range []card.Rank{card.Boss, card.MiniBoss, card.Effect, card.Attacker} {
		btn := createEl("button")
		btn.setText(rank.String())
		btn.setValue(strconv.Itoa(int(rank)))
		btn.addEventListener("click", e.createCardClick)
		e.main.appendChild(btn)
	}

	mayChange := createEl("p", "subtle")
	mayChange.setText("You may also change a card's rank after it has been created.")
	e.main.appendChild(mayChange)
}

func (e *editor) nextCardID(rank card.Rank) card.ID {
	if rank <= card.Boss {
		for id0 := 128 + card.ID(rank)<<5; ; id0 += 128 {
			for id, i := id0, 0; i < 32; id, i = id+1, i+1 {
				found := false

				for _, c := range e.set.Cards {
					if c.ID == id {
						found = true

						break
					}
				}

				if !found {
					return id
				}
			}
		}
	}

	for id := card.ID(128); ; id++ {
		found := false

		for _, c := range e.set.Cards {
			if c.ID == id {
				found = true

				break
			}
		}

		if !found {
			return id
		}
	}
}

func (e *editor) createCard(rank card.Rank) {
	id := e.nextCardID(rank)
	c := card.Vanilla(e.set.VanillaVersion(), id)
	c.Rank = rank
	e.set.Cards = append(e.set.Cards, c)
	e.encodeCardCodes()
	e.setCardEditor(id)
}

type customStage struct {
	ID   int64                  `json:"-"`
	Name string                 `json:"n"`
	CID  card.ContentIdentifier `json:"c"`
}

type customMusic struct {
	ID    int64                  `json:"-"`
	Name  string                 `json:"n"`
	CID   card.ContentIdentifier `json:"c"`
	Start float32                `json:"s"`
	End   float32                `json:"e"`
}

type customExtensions struct {
	Stages []customStage `json:"s"`
	Music  []customMusic `json:"m"`
}

var (
	customExtensionsCache customExtensions
	customExtensionsOnce  sync.Once
)

func getCustomExtensions(ctx context.Context) *customExtensions {
	customExtensionsOnce.Do(func() {
		err := internal.FetchJSON(ctx, internal.GetConfig(ctx).CustomCardAPIBaseURL+"extensions.json", &customExtensionsCache)
		if err != nil {
			log.Println("ERROR: loading extensions:", err)
		}
	})

	return &customExtensionsCache
}

func (e *editor) setCardEditor(id card.ID) {
	if e.set == nil {
		return
	}

	refreshEditor := func() {
		prevScroll := e.main.Get("scrollTop")
		e.encodeCardCodes()
		e.setCardEditor(id)
		e.main.Set("scrollTop", prevScroll)
	}

	var def *card.Def

	for _, c := range e.set.Cards {
		if c.ID == id {
			def = c

			break
		}
	}

	if def == nil {
		def = card.Vanilla(e.set.VanillaVersion(), id)
		e.set.Cards = append(e.set.Cards, def)
		e.encodeCardCodes()
	}

	idString := strconv.FormatUint(uint64(id), 10)
	e.reset(idString)

	nav := element{e.nav.Call("querySelector", ".card-nav[data-id=\""+idString+"\"]")}
	if !nav.Truthy() {
		errorMessage := createEl("p", "error")
		errorMessage.setText("Internal Editor Error: Could not locate card in sidebar: " + idString)
		e.main.appendChild(errorMessage)

		return
	}

	nav.Get("classList").Call("add", "active")
	nav.Call("setAttribute", "aria-current", "page")

	e.cardNameChanged.Release()
	e.cardRankChanged.Release()
	e.cardTPChanged.Release()
	e.deleteCardClick.Release()
	e.changePortraitClick.Release()
	e.duplicateCardClick.Release()

	nameInput := createInput("text")
	codeInput := createInput("text")

	basic := e.fieldset("Basic")

	cardDisplay := createEl("div", "card-display", "large-right")
	refreshPreview := func() {
		code, _ := def.MarshalText()
		codeInput.setValue(string(code))

		cardDisplay.Set("innerHTML", def.ToHTML(e.ctx, e.set))
		js.Global().Call("setTimeout", js.Global().Get("doSquish"), 0, cardDisplay.Value)
	}
	refreshPreview()
	basic.appendChild(cardDisplay)

	codeLabel := createEl("label")
	codeLabel.setText("Code")
	codeInput.Set("readOnly", true)
	codeLabel.appendChild(codeInput)
	basic.appendChild(codeLabel)

	e.cardNameChanged = js.FuncOf(func(_ js.Value, _ []js.Value) interface{} {
		if name := nameInput.value(); def.Name != name {
			def.Name = name
			nav.Get("lastChild").Set("nodeValue", def.DisplayName())
			e.encodeCardCodesSoon()
			refreshPreview()
		}

		nameInput.Call("setCustomValidity", "")

		for _, letter := range nameInput.value() {
			if !sprites.LetterSupported(sprites.FontBubblegumSans, letter) {
				nameInput.Call("setCustomValidity", "Warning: The letter '"+string(letter)+"' cannot be rendered by Spy Cards and will display as a blank space.")

				break
			}
		}

		return js.Undefined()
	})

	nameLabel := createEl("label")
	nameLabel.setText("Name")
	nameInput.Set("placeholder", id.String())
	nameInput.setValue(def.Name)
	nameInput.addEventListener("input", e.cardNameChanged)
	nameLabel.appendChild(nameInput)
	basic.appendChild(nameLabel)

	rankSelect := createEl("select")

	e.cardRankChanged = js.FuncOf(func(_ js.Value, _ []js.Value) interface{} {
		ri, err := strconv.Atoi(rankSelect.value())
		if err != nil {
			return js.Undefined()
		}

		r := card.Rank(ri)
		if r != def.Rank {
			nav.removeClass("rank-" + def.Rank.String())
			def.Rank = r
			nav.addClass("rank-" + def.Rank.String())
			e.encodeCardCodesSoon()
			refreshPreview()
		}

		return js.Undefined()
	})

	rankLabel := createEl("label")
	rankLabel.setText("Rank")

	for _, r := range []card.Rank{card.Boss, card.MiniBoss, card.Effect, card.Attacker, card.Token} {
		opt := createOption(r.String(), strconv.Itoa(int(r)))
		opt.addClass("rank-" + r.String())
		rankSelect.appendChild(opt)
	}

	rankSelect.setValue(strconv.Itoa(int(def.Rank)))
	rankSelect.addEventListener("change", e.cardRankChanged)
	rankLabel.appendChild(rankSelect)
	basic.appendChild(rankLabel)

	tpInput := createInput("number")

	e.cardTPChanged = js.FuncOf(func(_ js.Value, _ []js.Value) interface{} {
		tp, err := strconv.ParseInt(tpInput.value(), 10, 64)
		if err == nil && def.TP != tp {
			def.TP = tp
			e.encodeCardCodesSoon()
			refreshPreview()
		}

		return js.Undefined()
	})

	tpLabel := createEl("label")
	tpLabel.setText("Cost (Teamwork Points)")
	tpInput.Set("step", "1")
	tpInput.setValue(strconv.FormatInt(def.TP, 10))
	tpInput.addEventListener("input", e.cardTPChanged)
	tpLabel.appendChild(tpInput)
	basic.appendChild(tpLabel)

	e.changePortraitClick = js.FuncOf(func(_ js.Value, args []js.Value) interface{} {
		args[0].Call("preventDefault")
		go e.selectPortrait(e.main, &def.Portrait, &def.CustomPortrait, def.DisplayName(), func() {
			e.setCardEditor(id)
		})

		return js.Undefined()
	})

	portraitLabel := createEl("label", "thin", "button")
	portraitLabel.setText("Portrait")
	portraitLabel.appendChild(createEl("br"))
	portraitLabel.appendChild(e.makePortrait(def.Portrait, def.CustomPortrait))

	portraitButton := createEl("button")
	portraitButton.setText("Change")
	portraitButton.addEventListener("click", e.changePortraitClick)
	portraitLabel.appendChild(portraitButton)
	basic.appendChild(portraitLabel)

	e.deleteCardClick = js.FuncOf(func(_ js.Value, args []js.Value) interface{} {
		args[0].Call("preventDefault")

		question := "Really delete this card? '" + def.DisplayName() + "'"
		if id < 128 {
			question = "Really reset card to defaults? '" + id.String() + "'"
			if def.DisplayName() != id.String() {
				question += " ('" + def.DisplayName() + "')"
			}
		}

		if window.Call("confirm", question).Bool() {
			for i, c := range e.set.Cards {
				if c.ID == id {
					e.set.Cards = append(e.set.Cards[:i], e.set.Cards[i+1:]...)

					break
				}
			}

			for _, f := range e.set.Mode.GetAll(card.FieldGroup) {
				cg := f.(*card.Group)
				for i, cid := range cg.Cards {
					if cid == id {
						cg.Cards = append(cg.Cards[:i], cg.Cards[i+1:]...)

						break
					}
				}
			}

			e.reset("")
		}

		return js.Undefined()
	})

	deleteButton := createEl("button", "delete")
	deleteButton.setText("Delete Card")
	deleteButton.addEventListener("click", e.deleteCardClick)
	basic.appendChild(deleteButton)

	e.duplicateCardClick = js.FuncOf(func(_ js.Value, args []js.Value) interface{} {
		args[0].Call("preventDefault")

		clone := &card.Def{}

		b, err := def.MarshalBinary()
		if err != nil {
			return js.Undefined()
		}

		err = clone.UnmarshalBinary(b)
		if err != nil {
			return js.Undefined()
		}

		clone.ID = e.nextCardID(clone.Rank)
		clone.UpdateID(def.ID, clone.ID)
		if def.Name == "" && def.ID < 128 {
			clone.Name = def.DisplayName()
		}
		for i, c := range e.set.Cards {
			if c == def {
				e.set.Cards = append(e.set.Cards[:i+1], append([]*card.Def{clone}, e.set.Cards[i+1:]...)...)

				break
			}
		}
		for _, f := range e.set.Mode.GetAll(card.FieldGroup) {
			cg := f.(*card.Group)
			found := false
			for i, id := range cg.Cards {
				if id == def.ID {
					cg.Cards = append(cg.Cards[:i+1], append([]card.ID{clone.ID}, cg.Cards[i+1:]...)...)
					found = true

					break
				}
			}
			if found {
				break
			}
		}
		e.encodeCardCodes()
		e.setCardEditor(clone.ID)

		return js.Undefined()
	})

	duplicateButton := createEl("button")
	duplicateButton.setText("Duplicate Card")
	duplicateButton.addEventListener("click", e.duplicateCardClick)
	basic.appendChild(duplicateButton)

	tribes := e.listFieldset("Tribes", &e.cardTribes, len(def.Tribes), func(dst, src int, _, _ element, after bool) bool {
		t := def.Tribes[src]
		if after {
			copy(def.Tribes[dst+1:], def.Tribes[dst:src])
		} else {
			copy(def.Tribes[src:], def.Tribes[src+1:dst+1])
		}
		def.Tribes[dst] = t

		refreshPreview()

		return true
	}, func(i int) {
		go func() {
			e.editTribeModal(def, i)
			refreshEditor()
		}()
	}, func(i int, li element) {
		if def.Tribes[i].Tribe == card.TribeCustom && strings.HasPrefix(def.Tribes[i].CustomName, "_") {
			li.addClass("hidden-item")
		}
		li.setBackground(def.Tribes[i].Color())
		li.setText(def.Tribes[i].Name())
	})

	addTribeLabel := createEl("label")
	addTribeLabel.Call("setAttribute", "for", "add-tribe-select")
	addTribeLabel.setText("Add Tribe")

	addTribeSelect := createEl("select")
	addTribeSelect.Set("id", "add-tribe-select")

	var otherTribes []card.TribeDef

	for t := card.Tribe(0); t < card.TribeCustom; t++ {
		found := false

		for _, td := range def.Tribes {
			if td.Tribe == t {
				found = true

				break
			}
		}

		if !found {
			otherTribes = append(otherTribes, card.TribeDef{
				Tribe: t,
			})
		}
	}

	seenCustomTribe := make(map[string]bool)

	for _, t := range def.Tribes {
		if t.Tribe == card.TribeCustom {
			seenCustomTribe[t.CustomName] = true
		}
	}

	for _, c := range e.set.Cards {
		for _, t := range c.Tribes {
			if t.Tribe == card.TribeCustom && !seenCustomTribe[t.CustomName] {
				otherTribes = append(otherTribes, t)
				seenCustomTribe[t.CustomName] = true
			}
		}
	}

	for i, t := range otherTribes {
		option := createOption(t.Name(), strconv.Itoa(i))
		option.setBackground(t.Color())
		addTribeSelect.appendChild(option)
	}

	customTribeOption := createOption("New Custom Tribe", "-1")
	customTribeOption.Get("style").Set("fontStyle", "italic")
	addTribeSelect.appendChild(customTribeOption)

	addTribeButton := createEl("button")
	addTribeButton.setText("Add")

	e.addCardTribe.Release()
	e.addCardTribe = js.FuncOf(func(_ js.Value, args []js.Value) interface{} {
		args[0].Call("preventDefault")
		index, err := strconv.Atoi(addTribeSelect.value())
		if err != nil {
			return js.Undefined()
		}

		if index == -1 {
			go func() {
				e.editTribeModal(def, -1)
				refreshEditor()
			}()
		} else {
			def.Tribes = append(def.Tribes, otherTribes[index])
			refreshEditor()
		}

		return js.Undefined()
	})

	addTribeButton.addEventListener("click", e.addCardTribe)

	addTribeLabel.appendChild(addTribeButton)
	addTribeLabel.appendChild(addTribeSelect)
	tribes.appendChild(addTribeLabel)

	remainingEffectsHidden := false
	effects := e.listFieldset("Effects", &e.cardEffects, len(def.Effects), func(dst, src int, _, _ element, after bool) bool {
		eff := def.Effects[src]
		if after {
			copy(def.Effects[dst+1:], def.Effects[dst:src])
		} else {
			copy(def.Effects[src:], def.Effects[src+1:dst+1])
		}
		def.Effects[dst] = eff

		refreshPreview()

		return true
	}, func(i int) {
		go func() {
			e.editEffectModal(&def.Effects[i], nil, def)
			if def.Effects[i] == nil {
				def.Effects = append(def.Effects[:i], def.Effects[i+1:]...)
			}
			refreshEditor()
		}()
	}, func(i int, li element) {
		if remainingEffectsHidden {
			li.addClass("hidden-item")
		}
		if def.Effects[i].Type == card.FlavorText {
			if def.Effects[i].Flags&card.FlagFlavorTextHideRemaining != 0 {
				remainingEffectsHidden = true
			}
			li.addClass("flavor-text")
		}
		li.setText((&card.RichDescription{
			Content: def.Effects[i].Description(def, e.set),
		}).String())
	})

	e.addCardEffect.Release()
	e.addCardEffect = js.FuncOf(func(_ js.Value, args []js.Value) interface{} {
		args[0].Call("preventDefault")
		go func() {
			var effect *card.EffectDef
			e.editEffectModal(&effect, nil, def)
			if effect != nil {
				def.Effects = append(def.Effects, effect)
				refreshEditor()
			}
		}()

		return js.Undefined()
	})

	addEffectButton := createEl("button")
	addEffectButton.setText("Add Effect")
	addEffectButton.addEventListener("click", e.addCardEffect)
	effects.appendChild(addEffectButton)

	banInput := createInput("checkbox")
	unpickableInput := createInput("checkbox")
	hideHomeInput := createInput("checkbox")
	unfilterInput := createInput("checkbox")

	var variantInputs [][4]element

	restrictions := e.fieldset("Restrictions")
	banLabel := createEl("label", "thin")
	banLabel.appendChild(banInput)
	banLabel.Call("appendChild", document.Call("createTextNode", " Ban Card"))
	restrictions.appendChild(banLabel)

	unpickableLabel := createEl("label", "thin")
	unpickableLabel.appendChild(unpickableInput)
	unpickableLabel.Call("appendChild", document.Call("createTextNode", " Hide In Deck Builder"))
	restrictions.appendChild(unpickableLabel)

	hideHomeLabel := createEl("label", "thin")
	hideHomeLabel.appendChild(hideHomeInput)
	hideHomeLabel.Call("appendChild", document.Call("createTextNode", " Hide On Home Screen"))
	restrictions.appendChild(hideHomeLabel)

	unfilterLabel := createEl("label", "thin")
	unfilterLabel.appendChild(unfilterInput)
	unfilterLabel.Call("appendChild", document.Call("createTextNode", " Ignore Generic Filters"))
	restrictions.appendChild(unfilterLabel)

	variants := e.set.Mode.GetAll(card.FieldVariant)
	if len(variants) != 0 {
		table := createEl("table")
		thead := createEl("thead")
		tr := createEl("tr")
		th := createEl("th")
		th.setText("Variant")
		tr.appendChild(th)
		th = createEl("th")
		th.setText("Banned")
		tr.appendChild(th)
		th = createEl("th")
		th.setText("Unpickable")
		tr.appendChild(th)
		th = createEl("th")
		th.setText("HideHome")
		tr.appendChild(th)
		th = createEl("th")
		th.setText("Unfilter")
		tr.appendChild(th)
		thead.appendChild(tr)
		table.appendChild(thead)

		tbody := createEl("tbody")

		for _, f := range variants {
			v := f.(*card.Variant)
			tr = createEl("tr")
			th = createEl("th")
			th.setText(v.Title)
			tr.appendChild(th)

			var inputs [4]element
			for i := range inputs {
				td := createEl("td")
				inputs[i] = createInput("checkbox")
				label := createEl("label")
				label.appendChild(inputs[i])
				td.appendChild(label)
				tr.appendChild(td)
			}

			variantInputs = append(variantInputs, inputs)

			tbody.appendChild(tr)
		}

		table.appendChild(tbody)
		restrictions.appendChild(table)
	}

	getRestrictions := func(fields []card.Field) (banned, bannedLocked, unpickable, unpickableLocked, hideHome, hideHomeLocked, unfilter bool) {
		for _, field := range fields {
			switch f := field.(type) {
			case *card.BannedCards:
				locked := len(f.Cards) == 0 && id < 128
				found := locked

				for _, i := range f.Cards {
					if i == id {
						found = true

						break
					}
				}

				switch f.Flags & card.BannedCardTypeMask {
				case card.BannedCardTypeBanned:
					banned = banned || found
					bannedLocked = bannedLocked || locked

					if locked {
						hideHome, hideHomeLocked = true, true
					}
				case card.BannedCardTypeUnpickable:
					unpickable = unpickable || found
					unpickableLocked = unpickableLocked || locked
				case card.BannedCardTypeHiddenHome:
					hideHome = hideHome || found
					hideHomeLocked = hideHomeLocked || locked
				}
			case *card.UnfilterCard:
				if f.ID == id {
					unfilter = true
				}
			}
		}

		return
	}

	setupRestrictions := func(value, locked bool, input element) {
		input.Set("checked", value)
		input.Set("disabled", locked)
	}
	resetRestrictions := func() {
		var mode card.GameMode
		if e.set.Mode != nil {
			mode = *e.set.Mode
		}

		globalBanned, globalBannedLock, globalUnpickable, globalUnpickableLock, globalHideHome, globalHideHomeLock, globalUnfilter := getRestrictions(mode.Fields)
		setupRestrictions(globalBanned, globalBannedLock, banInput)
		setupRestrictions(globalUnpickable, globalUnpickableLock, unpickableInput)
		setupRestrictions(globalHideHome, globalHideHomeLock, hideHomeInput)
		setupRestrictions(globalUnfilter, false, unfilterInput)

		for i, f := range variants {
			v := f.(*card.Variant)
			b, bl, up, upl, hh, hhl, uf := getRestrictions(v.Rules)
			setupRestrictions(b || globalBanned, bl || globalBanned, variantInputs[i][0])
			setupRestrictions(up || globalUnpickable, upl || globalUnpickable, variantInputs[i][1])
			setupRestrictions(hh || globalHideHome, hhl || globalHideHome, variantInputs[i][2])
			setupRestrictions(uf || globalUnfilter, globalUnfilter, variantInputs[i][3])
		}
	}
	resetRestrictions()

	setBanState := func(fields *[]card.Field, banned bool, flags card.BannedCardsFlags) {
		defer refreshPreview()

		for i, f := range *fields {
			if bc, ok := f.(*card.BannedCards); ok && bc.Flags == flags && len(bc.Cards) != 0 {
				for j, c := range bc.Cards {
					if c == id {
						if banned {
							return
						}

						if len(bc.Cards) != 1 {
							bc.Cards = append(bc.Cards[:j], bc.Cards[j+1:]...)

							return
						}

						*fields = append((*fields)[:i], (*fields)[i+1:]...)

						return
					}
				}

				if banned {
					bc.Cards = append(bc.Cards, id)

					return
				}
			}
		}

		if banned {
			*fields = append(*fields, &card.BannedCards{
				Flags: flags,
				Cards: []card.ID{id},
			})
		}
	}
	setUnfilterState := func(fields *[]card.Field, unfilter bool, flags card.UnfilterCardFlags) {
		for i, f := range *fields {
			if uf, ok := f.(*card.UnfilterCard); ok && uf.Flags == flags && uf.ID == id {
				if unfilter {
					return
				}

				*fields = append((*fields)[:i], (*fields)[i+1:]...)

				return
			}
		}

		if unfilter {
			*fields = append(*fields, &card.UnfilterCard{
				Flags: flags,
				ID:    id,
			})
		}
	}

	e.cardRestrictionsChanged.Release()
	e.cardRestrictionsChanged = js.FuncOf(func(this js.Value, _ []js.Value) interface{} {
		if e.set.Mode == nil {
			e.set.Mode = &card.GameMode{}
		}
		checked := this.Get("checked").Bool()
		if banInput.Equal(this) {
			setBanState(&e.set.Mode.Fields, checked, card.BannedCardTypeBanned)
		}
		if unpickableInput.Equal(this) {
			setBanState(&e.set.Mode.Fields, checked, card.BannedCardTypeUnpickable)
		}
		if hideHomeInput.Equal(this) {
			setBanState(&e.set.Mode.Fields, checked, card.BannedCardTypeHiddenHome)
		}
		if unfilterInput.Equal(this) {
			setUnfilterState(&e.set.Mode.Fields, checked, 0)
		}
		for i, f := range variants {
			if variantInputs[i][0].Equal(this) {
				setBanState(&f.(*card.Variant).Rules, checked, card.BannedCardTypeBanned)

				break
			}
			if variantInputs[i][1].Equal(this) {
				setBanState(&f.(*card.Variant).Rules, checked, card.BannedCardTypeUnpickable)

				break
			}
			if variantInputs[i][2].Equal(this) {
				setBanState(&f.(*card.Variant).Rules, checked, card.BannedCardTypeHiddenHome)

				break
			}
			if variantInputs[i][3].Equal(this) {
				setUnfilterState(&f.(*card.Variant).Rules, checked, 0)

				break
			}
		}
		e.encodeCardCodes()
		resetRestrictions()

		return js.Undefined()
	})
	banInput.addEventListener("change", e.cardRestrictionsChanged)
	unpickableInput.addEventListener("change", e.cardRestrictionsChanged)
	hideHomeInput.addEventListener("change", e.cardRestrictionsChanged)
	unfilterInput.addEventListener("change", e.cardRestrictionsChanged)

	for _, inputs := range variantInputs {
		inputs[0].addEventListener("change", e.cardRestrictionsChanged)
		inputs[1].addEventListener("change", e.cardRestrictionsChanged)
		inputs[2].addEventListener("change", e.cardRestrictionsChanged)
		inputs[3].addEventListener("change", e.cardRestrictionsChanged)
	}

	custom := getCustomExtensions(e.ctx)

	usedExtension := make([]bool, 4)
	extensions := e.listFieldset("Extensions", &e.cardExtensions, len(def.Extensions), func(dst, src int, _, _ element, after bool) bool {
		ext := def.Extensions[src]
		if after {
			copy(def.Extensions[dst+1:], def.Extensions[dst:src])
		} else {
			copy(def.Extensions[src:], def.Extensions[src+1:dst+1])
		}
		def.Extensions[dst] = ext

		return true
	}, func(i int) {
		go e.modal(func(modal element) {
			forceInput := createInput("checkbox")
			cidInput := createInput("text")
			startInput := createInput("number")
			endInput := createInput("number")
			deleteButton := createEl("button", "delete")
			saveButton := createEl("button")
			closeButton := createEl("button")

			onNumChanged := js.FuncOf(func(js.Value, []js.Value) interface{} {
				if closeButton.Get("textContent").String() == "Close" {
					saveButton.Set("disabled", false)
					closeButton.setText("Cancel")
				}

				return js.Undefined()
			})
			defer onNumChanged.Release()

			forceInput.Set("checked", def.Extensions[i].Flags&card.ExtensionOverrideCustom != 0)
			forceInput.Call("addEventListener", "input", onNumChanged)
			cidInput.setValue(def.Extensions[i].CID.String())
			startInput.setValue(strconv.FormatFloat(float64(def.Extensions[i].LoopStart), 'f', -1, 32))
			endInput.setValue(strconv.FormatFloat(float64(def.Extensions[i].LoopEnd), 'f', -1, 32))

			heading := createEl("h1")
			heading.setText(def.Extensions[i].Type.String())
			modal.appendChild(heading)

			switch def.Extensions[i].Type {
			case card.ExtensionSetStageIPFS, card.ExtensionSetMusic, card.ExtensionPlaySound:
				cidLabel := createEl("label")
				cidLabel.setText("Content Identifier (CIDv1)")
				cidInput.Set("required", true)
				onCIDChanged := js.FuncOf(func(_ js.Value, _ []js.Value) interface{} {
					closeButton.setText("Cancel")
					cid := cidInput.Get("value").String()
					if len(cid) == 59 && cid[0] == 'b' && strings.ToLower(cid) == cid {
						b, err := base32.StdEncoding.WithPadding(base32.NoPadding).DecodeString(strings.ToUpper(cid[1:]))
						if err == nil && len(b) == 36 && b[0] == 1 && b[1] == 0x70 && b[2] == 0x12 && b[3] == 0x20 {
							cidInput.Call("setCustomValidity", "")
							saveButton.Set("disabled", false)

							return js.Undefined()
						}
					}
					if len(cid) == 0 {
						cidInput.Call("setCustomValidity", "")
					} else {
						cidInput.Call("setCustomValidity", "Invalid CID.")
					}
					cidInput.Call("reportValidity")
					saveButton.Set("disabled", true)

					return js.Undefined()
				})
				defer onCIDChanged.Release()
				cidInput.addEventListener("input", onCIDChanged)
				cidLabel.appendChild(cidInput)
				modal.appendChild(cidLabel)

				cidHint := createEl("p", "subtle")
				cidHint.setText("For more information on CIDv1, see the ")
				cidHintA := createEl("a")
				cidHintA.Set("target", "_blank")
				cidHintA.Set("href", "https://github.com/multiformats/cid#cidv1")
				cidHintA.setText("multiformats documentation")
				cidHint.appendChild(cidHintA)
				cidHint.Call("appendChild", document.Call("createTextNode", ". For copyright reasons, files must be manually pinned to be accessible."))
				modal.appendChild(cidHint)

			case card.ExtensionSetStage:
				// TODO: name dropdown
			}

			switch def.Extensions[i].Type {
			case card.ExtensionSetStageIPFS:
				// no additional fields
			case card.ExtensionSetMusic:
				startLabel := createEl("label")
				startLabel.setText("Loop Start")
				startInput.Set("min", "0")
				startInput.Set("step", "0.01")
				startInput.addEventListener("input", onNumChanged)
				startLabel.appendChild(startInput)
				modal.appendChild(startLabel)

				endLabel := createEl("label")
				endLabel.setText("Loop End")
				endInput.Set("min", 0)
				endInput.Set("step", "0.01")
				endInput.addEventListener("input", onNumChanged)
				endLabel.appendChild(endInput)
				modal.appendChild(endLabel)
			case card.ExtensionPlaySound:
				// no additional fields
			case card.ExtensionSetStage:
				// no additional fields
			}

			forceLabel := createEl("label")
			forceLabel.appendChild(forceInput)
			switch def.Extensions[i].Type {
			case card.ExtensionSetStageIPFS:
				forceLabel.Call("appendChild", document.Call("createTextNode", " Set stage even if another custom stage is active."))
				modal.appendChild(forceLabel)
			case card.ExtensionSetMusic:
				forceLabel.Call("appendChild", document.Call("createTextNode", " Set music even if another custom song is active."))
				modal.appendChild(forceLabel)
			case card.ExtensionSetStage:
				forceLabel.Call("appendChild", document.Call("createTextNode", " Set stage even if another custom stage is active."))
				modal.appendChild(forceLabel)
			}

			done := make(chan struct{})

			onDeleteClick := js.FuncOf(func(_ js.Value, args []js.Value) interface{} {
				args[0].Call("preventDefault")
				if window.Call("confirm", "Really delete this extension? '"+def.Extensions[i].Type.String()+"'").Bool() {
					def.Extensions = append(def.Extensions[:i], def.Extensions[i+1:]...)
					close(done)
				}

				return js.Undefined()
			})
			defer onDeleteClick.Release()
			deleteButton.addEventListener("click", onDeleteClick)

			onSaveClick := js.FuncOf(func(_ js.Value, args []js.Value) interface{} {
				args[0].Call("preventDefault")
				b, err := base32.StdEncoding.WithPadding(base32.NoPadding).DecodeString(strings.ToUpper(cidInput.Get("value").String()[1:]))
				if err != nil {
					saveButton.Set("disabled", true)
					cidInput.Call("setCustomValidity", "Invalid CID.")
					cidInput.Call("reportValidity")

					return js.Undefined()
				}
				def.Extensions[i].Flags = 0
				if forceInput.Get("checked").Bool() {
					def.Extensions[i].Flags |= card.ExtensionOverrideCustom
				}
				def.Extensions[i].CID = card.ContentIdentifier(b)
				def.Extensions[i].LoopStart = float32(startInput.Get("valueAsNumber").Float())
				def.Extensions[i].LoopEnd = float32(endInput.Get("valueAsNumber").Float())
				close(done)

				return js.Undefined()
			})
			defer onSaveClick.Release()
			saveButton.addEventListener("click", onSaveClick)

			onCloseClick := js.FuncOf(func(_ js.Value, args []js.Value) interface{} {
				args[0].Call("preventDefault")
				close(done)

				return js.Undefined()
			})
			defer onCloseClick.Release()
			closeButton.addEventListener("click", onCloseClick)

			deleteButton.setText("Delete Extension")
			modal.appendChild(deleteButton)
			saveButton.setText("Save")
			saveButton.Set("disabled", true)
			modal.appendChild(saveButton)
			closeButton.setText("Close")
			modal.appendChild(closeButton)

			<-done
			e.encodeCardCodes()
			refreshEditor()
		})
	}, func(i int, li element) {
		if int(def.Extensions[i].Type) < len(usedExtension) {
			usedExtension[def.Extensions[i].Type] = true
		}
		name, punc := def.Extensions[i].CID.String(), ""
		if def.Extensions[i].Flags&card.ExtensionOverrideCustom != 0 {
			punc = "!"
		}
		switch def.Extensions[i].Type {
		case card.ExtensionSetMusic:
			for _, c := range custom.Music {
				if bytes.Equal(c.CID, def.Extensions[i].CID) {
					name = c.Name

					break
				}
			}
		case card.ExtensionSetStage:
			for _, s := range card.AssetStages {
				if def.Extensions[i].Name == s.Name {
					name = s.DisplayName

					break
				}
			}
		}
		li.setText(def.Extensions[i].Type.String() + punc + "(" + name + ")")
	})

	e.addCardExtension.Release()

	e.addCardExtension = js.FuncOf(func(this js.Value, args []js.Value) interface{} {
		args[0].Call("preventDefault")
		index := window.Call("parseInt", this.Get("nextElementSibling").Get("value"), 10).Int()
		switch t := card.ExtensionType(window.Call("parseInt", this.Call("getAttribute", "data-ext"), 10).Int()); t {
		case card.ExtensionSetStageIPFS:
			def.Extensions = append(def.Extensions, &card.Extension{
				Type: card.ExtensionSetStageIPFS,
				CID:  custom.Stages[index].CID,
			})
		case card.ExtensionSetMusic:
			def.Extensions = append(def.Extensions, &card.Extension{
				Type:      card.ExtensionSetMusic,
				CID:       custom.Music[index].CID,
				LoopStart: custom.Music[index].Start,
				LoopEnd:   custom.Music[index].End,
			})
		case card.ExtensionPlaySound:
			def.Extensions = append(def.Extensions, &card.Extension{
				Type: card.ExtensionPlaySound,
				CID:  nil,
			})
		case card.ExtensionSetStage:
			def.Extensions = append(def.Extensions, &card.Extension{
				Type: card.ExtensionSetStage,
				Name: card.AssetStages[index].Name,
			})
		}
		refreshEditor()

		return js.Undefined()
	})

	advanced := createEl("details")
	advancedLabel := createEl("summary")
	advancedLabel.setText("Advanced")
	advanced.appendChild(advancedLabel)

	if !usedExtension[card.ExtensionSetStage] {
		label := createEl("label")
		label.Call("setAttribute", "for", "add-card-stage-select")
		label.setText("Custom Stage")

		button := createEl("button")
		button.Call("setAttribute", "data-ext", int(card.ExtensionSetStage))
		button.addEventListener("click", e.addCardExtension)
		button.setText("Add")
		label.appendChild(button)

		sel := createEl("select")
		sel.Set("id", "add-card-stage-select")

		for i, s := range card.AssetStages {
			sel.appendChild(createOption(s.DisplayName, strconv.Itoa(i)))
		}

		label.appendChild(sel)
		advanced.appendChild(label)
		extensions.appendChild(advanced)
	}

	if !usedExtension[card.ExtensionSetMusic] {
		label := createEl("label")
		label.Call("setAttribute", "for", "add-card-music-select")
		label.setText("Custom Music")

		button := createEl("button")
		button.Call("setAttribute", "data-ext", int(card.ExtensionSetMusic))
		button.addEventListener("click", e.addCardExtension)
		button.setText("Add")
		label.appendChild(button)

		sel := createEl("select")
		sel.Set("id", "add-card-music-select")

		for i, c := range custom.Music {
			sel.appendChild(createOption(c.Name, strconv.Itoa(i)))
		}

		label.appendChild(sel)
		advanced.appendChild(label)
		extensions.appendChild(advanced)
	}
}

func (e *editor) fieldset(name string) element {
	fs := createEl("fieldset")
	label := createEl("legend")
	label.setText(name)
	fs.appendChild(label)
	e.main.appendChild(fs)

	return fs
}

func (e *editor) listFieldset(name string, d *draggable, count int, move func(int, int, element, element, bool) bool, click func(int), build func(int, element)) element {
	fs := e.fieldset(name)
	ul, newLi := d.setup(e, func(li element) {
		click(defaultIndexOf(li))
	}, defaultIndexOf, move)
	fs.appendChild(ul)

	for i := 0; i < count; i++ {
		build(i, newLi(true))
	}

	return fs
}

func (e *editor) editTribeModal(def *card.Def, index int) {
	e.modal(func(modal element) {
		nameInput := createInput("text")
		colorInput := createInput("color")
		changeAllInput := createInput("checkbox")
		saveButton := createEl("button")
		closeButton := createEl("button")

		nameLabel := createEl("label")
		nameLabel.setText("Tribe Name")
		nameLabel.appendChild(nameInput)
		modal.appendChild(nameLabel)
		if index == -1 {
			nameInput.Set("required", true)
			onNameChanged := js.FuncOf(func(_ js.Value, _ []js.Value) interface{} {
				customName := nameInput.Get("value").String()
				if customName == "" {
					saveButton.Set("disabled", true)
					closeButton.setText("Close")
					nameInput.Call("setCustomValidity", "")

					return js.Undefined()
				}
				closeButton.setText("Cancel")
				for i := card.Tribe(0); i < card.TribeCustom; i++ {
					if customName == i.String() {
						saveButton.Set("disabled", true)
						nameInput.Call("setCustomValidity", "This name is reserved for a built-in tribe.")
						nameInput.Call("reportValidity")

						return js.Undefined()
					}
				}
				for _, t := range def.Tribes {
					if t.Tribe == card.TribeCustom && t.CustomName == customName {
						saveButton.Set("disabled", true)
						nameInput.Call("setCustomValidity", "Tribe name is already used on this card.")
						nameInput.Call("reportValidity")

						return js.Undefined()
					}
				}
				for _, c := range e.set.Cards {
					found := false
					for _, t := range c.Tribes {
						if t.Tribe == card.TribeCustom && t.CustomName == customName {
							colorInput.Set("value", fmt.Sprintf("#%02x%02x%02x", t.Red, t.Green, t.Blue))
							found = true

							break
						}
					}
					if found {
						break
					}
				}
				saveButton.Set("disabled", false)
				nameInput.Call("setCustomValidity", "")

				for i, letter := range customName {
					if i == 0 && letter == '_' {
						continue
					}

					if !sprites.LetterSupported(sprites.FontD3Streetism, letter) {
						nameInput.Call("setCustomValidity", "Warning: The letter '"+string(letter)+"' cannot be rendered by Spy Cards and will display as a blank space.")

						break
					}
				}

				return js.Undefined()
			})
			defer onNameChanged.Release()
			nameInput.addEventListener("input", onNameChanged)
		} else {
			nameInput.Set("value", def.Tribes[index].Name())
			nameInput.Set("disabled", true)
		}

		colorLabel := createEl("label")
		colorLabel.setText("Tribe Color")
		colorLabel.appendChild(colorInput)
		modal.appendChild(colorLabel)
		if index == -1 {
			colorInput.setValue("#808080")
		} else {
			c := def.Tribes[index].Color()
			colorInput.setValue(fmt.Sprintf("#%02x%02x%02x", c.R, c.G, c.B))
			if def.Tribes[index].Tribe == card.TribeCustom {
				changeAllLabel := createEl("label")
				changeAllLabel.Get("style").Set("display", "none")
				changeAllLabel.appendChild(changeAllInput)
				changeAllLabel.Call("appendChild", document.Call("createTextNode", " Change color for all cards with this tribe."))
				modal.appendChild(changeAllLabel)
				onColorChanged := js.FuncOf(func(_ js.Value, _ []js.Value) interface{} {
					saveButton.Set("disabled", false)
					closeButton.setText("Cancel")
					changeAllLabel.Get("style").Set("display", "")

					return js.Undefined()
				})
				defer onColorChanged.Release()
				colorInput.addEventListener("input", onColorChanged)
			} else {
				colorInput.Set("disabled", true)
				colorHint := createEl("p", "subtle")
				colorHint.setText("The color of a built-in tribe cannot be edited.")
				modal.appendChild(colorHint)
			}
		}

		done := make(chan struct{})
		if index != -1 && len(def.Tribes) > 1 {
			deleteButton := createEl("button", "delete")
			deleteButton.setText("Remove Tribe")
			onDeleteClick := js.FuncOf(func(_ js.Value, args []js.Value) interface{} {
				args[0].Call("preventDefault")
				def.Tribes = append(def.Tribes[:index], def.Tribes[index+1:]...)
				e.encodeCardCodes()
				close(done)

				return js.Undefined()
			})
			defer onDeleteClick.Release()
			deleteButton.addEventListener("click", onDeleteClick)
			modal.appendChild(deleteButton)
		}

		if index == -1 || def.Tribes[index].Tribe == card.TribeCustom {
			saveButton.setText("Save")
			saveButton.Set("disabled", true)
			onSaveClick := js.FuncOf(func(_ js.Value, args []js.Value) interface{} {
				args[0].Call("preventDefault")
				var r, g, b uint8
				_, _ = fmt.Sscanf(colorInput.Get("value").String(), "#%02x%02x%02x", &r, &g, &b)
				if index == -1 {
					def.Tribes = append(def.Tribes, card.TribeDef{
						Tribe:      card.TribeCustom,
						CustomName: nameInput.Get("value").String(),
						Red:        r,
						Green:      g,
						Blue:       b,
					})
				} else {
					def.Tribes[index].Red = r
					def.Tribes[index].Green = g
					def.Tribes[index].Blue = b

					if changeAllInput.Get("checked").Bool() {
						for _, c := range e.set.Cards {
							for i := range c.Tribes {
								if c.Tribes[i].Tribe == card.TribeCustom && c.Tribes[i].CustomName == def.Tribes[index].CustomName {
									c.Tribes[i].Red = r
									c.Tribes[i].Green = g
									c.Tribes[i].Blue = b

									break
								}
							}
						}
					}
				}
				e.encodeCardCodes()
				close(done)

				return js.Undefined()
			})
			defer onSaveClick.Release()
			saveButton.addEventListener("click", onSaveClick)
			modal.appendChild(saveButton)
		}

		closeButton.setText("Close")
		onCloseClick := js.FuncOf(func(_ js.Value, args []js.Value) interface{} {
			args[0].Call("preventDefault")
			close(done)

			return js.Undefined()
		})
		defer onCloseClick.Release()
		closeButton.addEventListener("click", onCloseClick)
		modal.appendChild(closeButton)

		<-done
	})
}
