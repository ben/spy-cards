//go:build js && wasm
// +build js,wasm

package editor

import (
	"bytes"
	"context"
	"log"
	"sync"
	"syscall/js"

	"git.lubar.me/ben/spy-cards/card"
	"git.lubar.me/ben/spy-cards/format"
	"git.lubar.me/ben/spy-cards/internal"
)

func (e *editor) makePortrait(builtin uint8, custom []byte) element {
	portrait := createEl("span", "portrait")
	portrait.Call("setAttribute", "data-x", int(builtin&0xf))
	portrait.Call("setAttribute", "data-y", int(builtin>>4))

	switch builtin {
	case card.PortraitCustomEmbedded:
		portrait.addClass("embedded")
		portrait.Set("title", "Warning: This portrait is embedded in the card. This will cause the card code to be VERY long.")

		icon := createEl("img")
		portraitBytes := internal.Uint8Array.New(len(custom))
		js.CopyBytesToJS(portraitBytes, custom)
		portraitBlob := window.Get("Blob").New([]interface{}{
			portraitBytes,
		}, map[string]interface{}{
			"type": "image/png",
		})
		portraitURL := window.Get("URL").Call("createObjectURL", portraitBlob).String()

		var onLoad js.Func
		onLoad = js.FuncOf(func(_ js.Value, _ []js.Value) interface{} {
			icon.Call("removeEventListener", "load", onLoad)
			onLoad.Release()
			window.Get("URL").Call("revokeObjectURL", portraitURL)

			return js.Undefined()
		})
		icon.addEventListener("load", onLoad)

		icon.Set("alt", "")
		icon.Set("src", portraitURL)
		portrait.appendChild(icon)
	case card.PortraitCustomExternal:
		portrait.addClass("external")

		icon := createEl("img")
		icon.Set("alt", "")
		icon.Set("src", internal.GetConfig(e.ctx).UserImageBaseURL+format.Encode32(custom)+".webp")
		portrait.appendChild(icon)
	default:
		portrait.addClass("builtin")
	}

	return portrait
}

var (
	communityPortraitsCache [][]byte
	communityPortraitsOnce  sync.Once
)

func getCommunityPortraits(ctx context.Context) [][]byte {
	communityPortraitsOnce.Do(func() {
		var fileIDs []string

		err := internal.FetchJSON(ctx, internal.GetConfig(ctx).UserImageBaseURL+"community.json", &fileIDs)
		if err != nil {
			log.Println("ERROR: loading community portraits:", err)

			return
		}

		communityPortraitsCache = make([][]byte, len(fileIDs))
		for i := range communityPortraitsCache {
			communityPortraitsCache[i], err = format.Decode32(fileIDs[i])
			if err != nil {
				log.Println("ERROR: loading community portraits:", err)
				communityPortraitsCache = nil

				return
			}
		}
	})

	return communityPortraitsCache
}

func (e *editor) selectPortrait(container element, portrait *uint8, custom *[]byte, name string, back func()) {
	container.Set("innerHTML", "")

	loadingHint := createEl("p", "subtle")
	loadingHint.Set("textContent", "Loading community portrait list...")
	container.appendChild(loadingHint)

	community := getCommunityPortraits(e.ctx)

	container.Set("innerHTML", "")

	e.uploadPortraitChange.Release()
	e.uploadPortraitClick.Release()
	e.selectPortraitClick.Release()

	uploadPortrait := createInput("file")

	uploadPortraitError := createEl("p", "error")

	customButton := createEl("button")

	uploadError := func(err error) {
		uploadPortraitError.setText(err.Error())
		container.Call("insertBefore", uploadPortraitError.Value, customButton.Get("nextSibling"))
		e.encodeCardCodes()
	}

	e.uploadPortraitChange = js.FuncOf(func(_ js.Value, _ []js.Value) interface{} {
		file := uploadPortrait.Get("files").Index(0)
		bufPromise := file.Call("arrayBuffer")
		go func() {
			buf, err := internal.Await(bufPromise)
			if err != nil {
				uploadError(err)

				return
			}

			arr := internal.Uint8Array.New(buf, 0)
			body := make([]byte, arr.Length())
			js.CopyBytesToGo(body, arr)

			isPNG := bytes.HasPrefix(body, []byte("\x89PNG\r\n\x1a\n"))

			jsImg := window.Get("Image").New()
			u := window.Get("URL").Call("createObjectURL", file)

			var f js.Func

			f = js.FuncOf(func(_ js.Value, args []js.Value) interface{} {
				f.Release()
				jsImg.Set("onload", args[0])
				jsImg.Set("onerror", args[1])

				return js.Undefined()
			})

			jsImgPromise := window.Get("Promise").New(f)

			jsImg.Set("src", u)

			_, err = internal.AwaitNoRandomFail(jsImgPromise)

			window.Get("URL").Call("revokeObjectURL", u)

			if err != nil {
				uploadError(err)

				return
			}

			if w, h := jsImg.Get("width").Int(), jsImg.Get("height").Int(); !isPNG || w > 512 || h > 512 {
				destW, destH := w, h
				if w > 512 || h > 512 {
					max := w
					if max < h {
						max = h
					}

					destW = w * 512 / max
					destH = h * 512 / max
				}

				canvas := document.Call("createElement", "canvas")
				canvas.Set("width", destW)
				canvas.Set("height", destH)
				draw := canvas.Call("getContext", "2d")
				draw.Call("drawImage", jsImg, 0, 0, destW, destH)

				ch := make(chan js.Value, 1)

				f = js.FuncOf(func(_ js.Value, args []js.Value) interface{} {
					f.Release()

					ch <- args[0]

					return js.Undefined()
				})

				canvas.Call("toBlob", f, "image/png")

				blob := <-ch

				canvas.Set("width", 1)
				canvas.Set("height", 1)

				buffer, err := internal.AwaitNoRandomFail(blob.Call("arrayBuffer"))
				if err != nil {
					uploadError(err)

					return
				}
				u8 := internal.Uint8Array.New(buffer, 0)

				body = make([]byte, u8.Length())
				js.CopyBytesToGo(body, u8)
			}

			*portrait = card.PortraitCustomEmbedded
			*custom = body

			code, err := internal.DoPutRequest(e.ctx, internal.GetConfig(e.ctx).UserImageBaseURL+"upload", "image/png", body)
			if err != nil {
				uploadError(err)

				return
			}

			b, err := format.Decode32(string(code))
			if err != nil {
				uploadError(err)

				return
			}

			*portrait = card.PortraitCustomExternal
			*custom = b
			e.encodeCardCodes()
			back()
		}()

		return js.Undefined()
	})

	uploadPortrait.Set("accept", "image/*")
	uploadPortrait.Get("style").Set("display", "none")
	uploadPortrait.addEventListener("change", e.uploadPortraitChange)
	container.appendChild(uploadPortrait)

	e.uploadPortraitClick = js.FuncOf(func(_ js.Value, args []js.Value) interface{} {
		args[0].Call("preventDefault")
		uploadPortrait.Set("value", js.Null())
		uploadPortrait.Call("click")

		return js.Undefined()
	})

	e.selectPortraitClick = js.FuncOf(func(_ js.Value, args []js.Value) interface{} {
		el := args[0].Get("target")

		switch {
		case el.Get("classList").Call("contains", "builtin").Bool():
			x := uint8(window.Call("parseInt", el.Call("getAttribute", "data-x"), 10).Int())
			y := uint8(window.Call("parseInt", el.Call("getAttribute", "data-y"), 10).Int())
			*portrait = x | y<<4
			*custom = nil
		case el.Get("classList").Call("contains", "community").Bool():
			index := window.Call("parseInt", el.Call("getAttribute", "data-community-index"), 10).Int()
			*portrait = card.PortraitCustomExternal
			*custom = community[index]
		default:
			return js.Undefined()
		}

		e.encodeCardCodes()
		back()

		return js.Undefined()
	})

	customButton.setText("Custom Portrait")
	customButton.addEventListener("click", e.uploadPortraitClick)
	container.appendChild(customButton)

	customHelp := createEl("p", "subtle")
	customHelp.setText("For best results, use a 128x128 PNG image.")
	container.appendChild(customHelp)

	customDisclaimer := createEl("p", "subtle")
	customDisclaimer.setText("By uploading an image to this service, you assert that you have permission to use the image for non-commercial purposes.")
	container.appendChild(customDisclaimer)

	heading := createEl("h1")
	heading.setText("Choose a portrait for " + name)
	container.appendChild(heading)

	portraitSelect := createEl("div", "portrait-select")
	container.appendChild(portraitSelect)

	for i := 0; i < card.DefaultPortraitCount; i++ {
		portraitSelect.appendChild(e.makePortrait(uint8(i), nil))
	}

	for i := 254 - 7; i < 254; i++ {
		portraitSelect.appendChild(e.makePortrait(uint8(i), nil))
	}

	for i, custom := range community {
		portrait := e.makePortrait(card.PortraitCustomExternal, custom)
		portrait.addClass("community")
		portrait.Call("setAttribute", "data-community-index", i)
		portraitSelect.appendChild(portrait)
	}

	portraitSelect.addEventListener("click", e.selectPortraitClick)
}
