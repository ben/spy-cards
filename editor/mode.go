//go:build js && wasm
// +build js,wasm

package editor

import (
	"strconv"
	"syscall/js"

	"git.lubar.me/ben/spy-cards/card"
	"git.lubar.me/ben/spy-cards/gfx/sprites"
	"git.lubar.me/ben/spy-cards/match"
	"git.lubar.me/ben/spy-cards/match/npc"
	"git.lubar.me/ben/spy-cards/room"
)

func (e *editor) setModeEditor() {
	e.reset("mode")

	if e.set.Mode == nil {
		e.set.Mode = &card.GameMode{}
	}

	if len(e.set.Mode.Fields) == 0 {
		e.set.Mode.Fields = append(e.set.Mode.Fields, &card.Metadata{
			Portrait: card.PortraitCustomEmbedded,
		})
		e.encodeCardCodes()
	}

	if e.set.Mode.Fields[0].Type() != card.FieldMetadata {
		for i, f := range e.set.Mode.Fields {
			if md, ok := f.(*card.Metadata); ok {
				if md.Title == "" {
					e.set.Mode.Fields = append([]card.Field{f}, append(e.set.Mode.Fields[:i], e.set.Mode.Fields[i+1:]...)...)
					e.encodeCardCodes()
				}

				break
			}
		}
	}

	if md, ok := e.set.Mode.Fields[0].(*card.Metadata); !ok || md.Title != "" {
		e.set.Mode.Fields = append([]card.Field{
			&card.Metadata{
				Portrait: card.PortraitCustomEmbedded,
			},
		}, e.set.Mode.Fields...)
		e.encodeCardCodes()
	}

	md := e.set.Mode.Fields[0].(*card.Metadata)
	metadata := e.fieldset("Game Mode Metadata")

	e.modeAuthorChanged.Release()
	e.modeDescriptionChanged.Release()
	e.modeLatestChangesChanged.Release()

	authorInput := createInput("text")
	descriptionInput := createEl("textarea")
	latestChangesInput := createEl("textarea")

	e.modeAuthorChanged = js.FuncOf(func(this js.Value, _ []js.Value) interface{} {
		md.Author = this.Get("value").String()
		e.encodeCardCodesSoon()

		this.Call("setCustomValidity", "")

		for _, letter := range md.Author {
			if !sprites.LetterSupported(sprites.FontD3Streetism, letter) {
				this.Call("setCustomValidity", "Warning: The letter '"+string(letter)+"' cannot be rendered by Spy Cards and will display as a blank space.")

				break
			}
		}

		return js.Undefined()
	})
	e.modeDescriptionChanged = js.FuncOf(func(this js.Value, _ []js.Value) interface{} {
		md.Description = this.Get("value").String()
		e.encodeCardCodesSoon()

		this.Call("setCustomValidity", "")

		for _, letter := range md.Description {
			if !sprites.LetterSupported(sprites.FontBubblegumSans, letter) && letter != '\n' {
				this.Call("setCustomValidity", "Warning: The letter '"+string(letter)+"' cannot be rendered by Spy Cards and will display as a blank space.")

				break
			}
		}

		return js.Undefined()
	})
	e.modeLatestChangesChanged = js.FuncOf(func(this js.Value, _ []js.Value) interface{} {
		md.LatestChanges = this.Get("value").String()
		e.encodeCardCodesSoon()

		this.Call("setCustomValidity", "")

		for _, letter := range md.LatestChanges {
			if !sprites.LetterSupported(sprites.FontBubblegumSans, letter) && letter != '\n' {
				this.Call("setCustomValidity", "Warning: The letter '"+string(letter)+"' cannot be rendered by Spy Cards and will display as a blank space.")

				break
			}
		}

		return js.Undefined()
	})

	portraitLabel := createEl("label", "thin", "button")
	portraitLabel.setText("Portrait")
	portraitLabel.appendChild(createEl("br"))
	portraitButton := createEl("button")

	e.changePortraitClick.Release()

	if md.Portrait != card.PortraitCustomEmbedded {
		e.changePortraitClick = js.FuncOf(func(_ js.Value, args []js.Value) interface{} {
			args[0].Call("preventDefault")
			md.Portrait = card.PortraitCustomEmbedded
			md.CustomPortrait = nil
			e.encodeCardCodes()
			e.setModeEditor()

			return js.Undefined()
		})

		portraitButton.setText("Remove")
		portraitLabel.appendChild(e.makePortrait(md.Portrait, md.CustomPortrait))
	} else {
		e.changePortraitClick = js.FuncOf(func(_ js.Value, args []js.Value) interface{} {
			args[0].Call("preventDefault")
			go e.selectPortrait(e.main, &md.Portrait, &md.CustomPortrait, "your game mode", func() {
				e.setModeEditor()
			})

			return js.Undefined()
		})

		portraitButton.setText("Select Mode Portrait")
	}

	portraitButton.addEventListener("click", e.changePortraitClick)
	portraitLabel.appendChild(portraitButton)
	metadata.appendChild(portraitLabel)

	authorLabel := createEl("label")
	authorLabel.setText("Author")
	authorInput.setValue(md.Author)
	authorInput.Set("placeholder", "Your Name")
	authorInput.Set("required", true)
	authorInput.addEventListener("input", e.modeAuthorChanged)
	authorLabel.appendChild(authorInput)
	metadata.appendChild(authorLabel)

	descriptionLabel := createEl("label")
	descriptionLabel.setText("Description")
	descriptionInput.setValue(md.Description)
	descriptionInput.Set("placeholder", "The elevator pitch for your game mode.")
	descriptionInput.Set("required", true)
	descriptionInput.addEventListener("input", e.modeDescriptionChanged)
	descriptionLabel.appendChild(descriptionInput)
	metadata.appendChild(descriptionLabel)

	latestChangesLabel := createEl("label")
	latestChangesLabel.setText("Latest Changes")
	latestChangesInput.setValue(md.LatestChanges)
	latestChangesInput.Set("placeholder", "A list of changes since the previous version of this game mode.")
	latestChangesInput.addEventListener("input", e.modeLatestChangesChanged)
	latestChangesLabel.appendChild(latestChangesInput)
	metadata.appendChild(latestChangesLabel)

	fields := e.fieldset("Fields")

	ul, createItem := e.modeFields.setup(e, func(li element) {
		i := defaultIndexOf(li)
		if i == 0 {
			return // first field editor is already open above
		}

		go e.setFieldEditor(e.set.Mode.Fields[i], nil)
	}, defaultIndexOf, func(dst, src int, _, _ element, after bool) bool {
		f := e.set.Mode.Fields[src]
		if after {
			copy(e.set.Mode.Fields[dst+1:], e.set.Mode.Fields[dst:src])
		} else {
			copy(e.set.Mode.Fields[src:], e.set.Mode.Fields[src+1:dst+1])
		}
		e.set.Mode.Fields[dst] = f

		return true
	})
	fields.appendChild(ul)

	for i, f := range e.set.Mode.Fields {
		li := createItem(i != 0)

		if i == 0 {
			li.Get("style").Set("cursor", "default")
		}

		e.fieldName(f, li)

		if i == 0 {
			li.setText("")

			muted := createEl("span", "muted")
			muted.setText("Game Mode Metadata")
			li.appendChild(muted)
		}
	}

	e.modeAddField.Release()
	e.modeAddField = js.FuncOf(func(_ js.Value, args []js.Value) interface{} {
		args[0].Call("preventDefault")
		go e.addFieldModal(nil)

		return js.Undefined()
	})

	addFieldButton := createEl("button")
	addFieldButton.setText("Add Field")
	addFieldButton.addEventListener("click", e.modeAddField)
	fields.appendChild(addFieldButton)
}

func (e *editor) fieldName(f card.Field, el element) {
	prefix, suffix := f.Type().String(), ""

	switch field := f.(type) {
	case *card.Metadata:
		if field.Title != "" {
			prefix = "Metadata:"
			suffix = " " + field.Title
		}
	case *card.BannedCards:
		switch typ := field.Flags & card.BannedCardTypeMask; {
		case len(field.Cards) == 0 && typ == card.BannedCardTypeBanned:
			prefix = "Ban All Vanilla Cards"
		case len(field.Cards) == 0 && typ == card.BannedCardTypeUnpickable:
			prefix = "Make All Vanilla Cards Unpickable"
		case len(field.Cards) == 0 && typ == card.BannedCardTypeHiddenHome:
			prefix = "Hide All Vanilla Cards on Home Screen"
		case typ == card.BannedCardTypeBanned:
			prefix = "Banned Cards"
		case typ == card.BannedCardTypeUnpickable:
			prefix = "Unpickable Cards"
		case typ == card.BannedCardTypeHiddenHome:
			prefix = "Hidden-on-Home Cards"
		}
	case *card.GameRules:
		prefix = "Game Rules"
	case *card.SummonCard:
		prefix = "Summon Card:"
		suffix = " " + e.set.Card(field.ID).DisplayName()
	case *card.Variant:
		if field.Title != "" {
			prefix = "Variant:"
			suffix = " " + field.Title
		}
	case *card.UnfilterCard:
		prefix = "Unfilter Card:"
		suffix = " " + e.set.Card(field.ID).DisplayName()
	case *card.DeckLimitFilter:
		muted := createEl("span", "muted")
		muted.setText("Deck Limit:")
		el.setText(" " + strconv.FormatUint(field.Count, 10) + "× ")
		el.Call("insertBefore", muted.Value, el.Get("firstChild"))
		el.appendChild(e.describeFilter(field.Filter))

		if field.CondCount != 0 {
			el.Call("appendChild", document.Call("createTextNode", " (if "+strconv.FormatUint(field.CondCount, 10)+"× "))
			el.appendChild(e.describeFilter(field.Condition))
			el.Call("appendChild", document.Call("createTextNode", ")"))
		}

		return
	case *card.Turn0Effect:
		prefix = "Turn 0 Effect:"
		suffix = " " + (&card.RichDescription{
			Content: field.Effect.Description(nil, e.set),
		}).String()
	case *card.VanillaVersion:
		prefix = "Vanilla Version:"
		suffix = " " + field.Version.String()
	case *card.Group:
		if field.Title == "" {
			prefix = "Card Group"
		} else {
			prefix = "Card Group:"
			suffix = " " + field.Title
		}
	}
	el.setText(suffix)

	muted := createEl("span", "muted")
	muted.setText(prefix)
	el.Call("insertBefore", muted.Value, el.Get("firstChild"))
}

func (e *editor) setFieldEditor(field card.Field, variant *card.Variant) {
	e.modal(func(modal element) {
		var editorFuncs []js.Func
		defer func() {
			for _, f := range editorFuncs {
				f.Release()
			}
		}()

		editorShared := func(name, typ string, update func(element)) element {
			label := createEl("label")
			label.setText(name)
			input := createInput(typ)
			onChange := js.FuncOf(func(_ js.Value, _ []js.Value) interface{} {
				update(input)

				return js.Undefined()
			})
			editorFuncs = append(editorFuncs, onChange)
			input.addEventListener("input", onChange)
			label.appendChild(input)
			modal.appendChild(label)

			return input
		}
		editString := func(name string, str *string, font sprites.FontID) element {
			input := editorShared(name, "text", func(input element) {
				*str = input.value()
				e.encodeCardCodesSoon()

				input.Call("setCustomValidity", "")

				for _, letter := range *str {
					if !sprites.LetterSupported(font, letter) {
						input.Call("setCustomValidity", "Warning: The letter '"+string(letter)+"' cannot be rendered by Spy Cards and will display as a blank space.")

						break
					}
				}
			})
			input.setValue(*str)

			return input
		}
		editNumber := func(name string, num *uint64, defaultValue uint64) element {
			input := editorShared(name, "number", func(input element) {
				if n, err := strconv.ParseUint(input.value(), 10, 64); err == nil {
					*num = n
				}
				input.Get("classList").Call("toggle", "unchanged", *num == defaultValue)
				e.encodeCardCodesSoon()
			})
			input.Get("classList").Call("toggle", "unchanged", *num == defaultValue)
			input.setValue(strconv.FormatUint(*num, 10))
			input.Set("required", true)
			input.Set("min", 0)
			input.Set("step", 1)

			return input
		}

		done := make(chan bool, 1)
		forceDelete := false

		switch f := field.(type) {
		case *card.Metadata:
			portraitLabel := createEl("label", "button")
			portraitLabel.setText("Section Portrait")
			portraitLabel.appendChild(createEl("br"))
			portraitButton := createEl("button")
			var changePortraitClick js.Func
			if f.Portrait != card.PortraitCustomEmbedded {
				changePortraitClick = js.FuncOf(func(_ js.Value, args []js.Value) interface{} {
					args[0].Call("preventDefault")
					f.Portrait = card.PortraitCustomEmbedded
					f.CustomPortrait = nil
					e.encodeCardCodes()
					select {
					case done <- false:
						go e.setFieldEditor(field, variant)
					default:
					}

					return js.Undefined()
				})
				portraitButton.setText("Remove")
				portraitLabel.appendChild(e.makePortrait(f.Portrait, f.CustomPortrait))
			} else {
				changePortraitClick = js.FuncOf(func(_ js.Value, args []js.Value) interface{} {
					args[0].Call("preventDefault")
					go e.modal(func(portraitModal element) {
						ch := make(chan struct{})
						e.selectPortrait(portraitModal, &f.Portrait, &f.CustomPortrait, "this metadata section", func() {
							ch <- struct{}{}
							select {
							case done <- false:
								go e.setFieldEditor(field, variant)
							default:
							}
						})
						<-ch
					})

					return js.Undefined()
				})
				portraitButton.setText("Select")
			}
			defer changePortraitClick.Release()
			portraitButton.addEventListener("click", changePortraitClick)
			portraitLabel.appendChild(portraitButton)
			modal.appendChild(portraitLabel)
			editString("Section Title", &f.Title, sprites.FontD3Streetism)
			editString("Section Author", &f.Author, sprites.FontD3Streetism)
			descriptionLabel := createEl("label")
			descriptionLabel.setText("Description")
			descriptionInput := createEl("textarea")
			descriptionChanged := js.FuncOf(func(_ js.Value, _ []js.Value) interface{} {
				f.Description = descriptionInput.Get("value").String()
				e.encodeCardCodesSoon()

				descriptionInput.Call("setCustomValidity", "")

				for _, letter := range f.Description {
					if !sprites.LetterSupported(sprites.FontBubblegumSans, letter) && letter != '\n' {
						descriptionInput.Call("setCustomValidity", "Warning: The letter '"+string(letter)+"' cannot be rendered by Spy Cards and will display as a blank space.")

						break
					}
				}

				return js.Undefined()
			})
			defer descriptionChanged.Release()
			descriptionInput.addEventListener("input", descriptionChanged)
			descriptionInput.setValue(f.Description)
			descriptionLabel.appendChild(descriptionInput)
			modal.appendChild(descriptionLabel)
		case *card.BannedCards:
			p1, p2 := createEl("p"), createEl("p", "subtle")
			modal.appendChild(p1)
			modal.appendChild(p2)
			switch typ := f.Flags & card.BannedCardTypeMask; {
			case len(f.Cards) == 0 && typ == card.BannedCardTypeBanned:
				p1.setText("Ban All Vanilla Cards")
				p2.setText("This prevents cards from Bug Fables from being selected in the deck builder or random-summoned. These cards can still be explicitly summoned if they are referenced directly by an effect. It also prevents them from appearing on the home screen.")
			case len(f.Cards) == 0 && typ == card.BannedCardTypeUnpickable:
				p1.setText("Make All Vanilla Cards Unpickable")
				p2.setText("This prevents cards from Bug Fables from being selected in the deck builder.")
			case len(f.Cards) == 0 && typ == card.BannedCardTypeHiddenHome:
				p1.setText("Hide All Vanilla Cards")
				p2.setText("This prevents all cards from Bug Fables from being shown on the home screen, but has no effect during a match.")
			case typ == card.BannedCardTypeBanned:
				p1.setText("Banned Cards")
				p2.setText("This prevents the specified cards from being selected in the deck builder or random-summoned. These cards can still be explicitly summoned if they are referenced directly by an effect.")
			case typ == card.BannedCardTypeUnpickable:
				p1.setText("Unpickable Cards")
				p2.setText("This prevents the specified cards from being selected in the deck builder.")
			case typ == card.BannedCardTypeHiddenHome:
				p1.setText("Hidden Cards")
				p2.setText("This prevents the specified cards from being shown on the home screen, but has no effect during a match.")
			}
			affectedCardsHeading := createEl("h1")
			affectedCardsHeading.setText("Affected Cards")
			modal.appendChild(affectedCardsHeading)
			ul := createEl("ul", "multicol")
			modal.appendChild(ul)
			if len(f.Cards) == 0 {
				for id := card.ID(0); id < 128; id++ {
					if c := e.set.Card(id); c != nil {
						li := createEl("li")
						li.appendChild(e.makePortrait(c.Portrait, c.CustomPortrait))
						li.Call("appendChild", document.Call("createTextNode", " "+c.DisplayName()))
						ul.appendChild(li)
					}
				}
			} else {
				for _, id := range f.Cards {
					if c := e.set.Card(id); c != nil {
						li := createEl("li")
						li.appendChild(e.makePortrait(c.Portrait, c.CustomPortrait))
						li.Call("appendChild", document.Call("createTextNode", " "+c.DisplayName()))
						ul.appendChild(li)
					}
				}
			}
			hint := createEl("p", "subtle")
			hint.setText("To ban or unban specific cards, use the Restrictions section near the bottom of the card editor.")
			modal.appendChild(hint)
		case *card.GameRules:
			grid := createEl("div", "grid")
			modal.appendChild(grid)
			grid.appendChild(element{editNumber("Maximum HP", &f.MaxHP, card.DefaultGameRules.MaxHP).Get("parentNode")})
			grid.appendChild(element{editNumber("Minimum Hand Size", &f.HandMinSize, card.DefaultGameRules.HandMinSize).Get("parentNode")})
			maxHand := editNumber("Maximum Hand Size", &f.HandMaxSize, card.DefaultGameRules.HandMaxSize)
			maxHand.Set("max", 50)
			grid.appendChild(element{maxHand.Get("parentNode")})
			grid.appendChild(element{editNumber("Draw Per Round", &f.DrawPerRound, card.DefaultGameRules.DrawPerRound).Get("parentNode")})
			grid.appendChild(element{editNumber("Cards Per Deck", &f.CardsPerDeck, card.DefaultGameRules.CardsPerDeck).Get("parentNode")})
			grid.appendChild(element{editNumber("Boss Cards", &f.BossCards, card.DefaultGameRules.BossCards).Get("parentNode")})
			grid.appendChild(element{editNumber("Mini-Boss Cards", &f.MiniBossCards, card.DefaultGameRules.MiniBossCards).Get("parentNode")})
			grid.appendChild(element{editNumber("Minimum TP", &f.MinTP, card.DefaultGameRules.MinTP).Get("parentNode")})
			grid.appendChild(element{editNumber("Maximum TP", &f.MaxTP, card.DefaultGameRules.MaxTP).Get("parentNode")})
			grid.appendChild(element{editNumber("TP Per Round", &f.TPPerRound, card.DefaultGameRules.TPPerRound).Get("parentNode")})
			grid.appendChild(element{editNumber("Duplicate Bosses", &f.DuplicateBoss, card.DefaultGameRules.DuplicateBoss).Get("parentNode")})
			grid.appendChild(element{editNumber("Duplicate Mini-Bosses", &f.DuplicateMiniBoss, card.DefaultGameRules.DuplicateMiniBoss).Get("parentNode")})
			grid.appendChild(element{editNumber("Duplicate Effects", &f.DuplicateEffect, card.DefaultGameRules.DuplicateEffect).Get("parentNode")})
			grid.appendChild(element{editNumber("Duplicate Attackers", &f.DuplicateAttacker, card.DefaultGameRules.DuplicateAttacker).Get("parentNode")})
			grid.appendChild(element{editNumber("Max Cards Played Per Round", &f.CardsPerRound, card.DefaultGameRules.CardsPerRound).Get("parentNode")})
		case *card.SummonCard:
			flagLabel := createEl("label")
			flagLabel.setText("Summon For")
			flagSelect := createEl("select")
			if variant == nil || variant.NPC == "" {
				flagSelect.appendChild(createOption("Player 1 (Host)", "0"))
			} else {
				flagSelect.appendChild(createOption("Player 1 (NPC)", "0"))
			}
			flagSelect.appendChild(createOption("Both Players", strconv.FormatUint(uint64(card.SummonCardBothPlayers), 10)))
			flagSelect.setValue(strconv.FormatUint(uint64(f.Flags), 10))
			onFlagChanged := js.FuncOf(func(_ js.Value, _ []js.Value) interface{} {
				flag, err := strconv.ParseUint(flagSelect.value(), 10, 64)
				if err == nil {
					f.Flags = card.SummonCardFlags(flag)
					e.encodeCardCodesSoon()
				}

				return js.Undefined()
			})
			defer onFlagChanged.Release()
			flagSelect.addEventListener("change", onFlagChanged)
			flagLabel.appendChild(flagSelect)
			modal.appendChild(flagLabel)
			cardLabel := createEl("label")
			cardLabel.setText("Card")
			cardSelect := createEl("select")
			e.fillCardSelect(cardSelect, variant)
			onCardChanged := js.FuncOf(func(_ js.Value, _ []js.Value) interface{} {
				id, err := strconv.ParseUint(cardSelect.value(), 10, 64)
				if err == nil {
					f.ID = card.ID(id)
					e.encodeCardCodesSoon()
				}

				return js.Undefined()
			})
			defer onCardChanged.Release()
			cardSelect.addEventListener("change", onCardChanged)
			cardSelect.setValue(strconv.FormatUint(uint64(f.ID), 10))
			cardLabel.appendChild(cardSelect)
			modal.appendChild(cardLabel)
		case *card.Variant:
			editString("Variant Title", &f.Title, sprites.FontD3Streetism).Set("required", true)
			npcInput := editString("NPC", &f.NPC, sprites.FontBubblegumSans)
			npcDesc := createEl("p", "subtle")
			modal.appendChild(npcDesc)
			updateNPCDesc := func(name string) {
				npcInput.Call("setCustomValidity", "")
				if name == "" {
					npcDesc.setText("Leave blank for a multiplayer variant.")

					return
				}

				n, err := npc.Get(name)
				if err != nil {
					npcInput.Call("setCustomValidity", err.Error())
					npcInput.Call("reportValidity")
					npcDesc.setText(err.Error())

					return
				}

				npcDesc.setText(n.String())
			}
			updateNPCDesc(f.NPC)
			npcChanged := js.FuncOf(func(_ js.Value, _ []js.Value) interface{} {
				updateNPCDesc(npcInput.Get("value").String())

				return js.Undefined()
			})
			defer npcChanged.Release()
			npcInput.addEventListener("input", npcChanged)
			npcAutocomplete := createEl("datalist")
			for _, name := range []string{
				"generic", "tutorial", "carmina2", "chuck", "arie", "shay",
				"crow", "genow", "janet", "bu-gi", "johnny", "kage", "ritchee",
				"serene", "carmina", "saved-decks", "mender-spam",
				"tp2-generic", "tp2-janet", "tp2-bu-gi", "tp2-johnny",
				"tp2-kage", "tp2-ritchee", "tp2-serene", "tp2-carmina",
				"tp2-chuck", "tp2-arie", "tp2-shay", "tp2-crow",
			} {
				opt := createEl("option")
				opt.setValue(name)
				npcAutocomplete.appendChild(opt)
			}

			var variantSet card.Set
			variantSet.CopyFrom(e.set)
			variantSet.Mode = &card.GameMode{}
			variantSet.Mode.Fields = append(variantSet.Mode.Fields, e.set.Mode.Fields...)
			variantSet.Mode.Fields = append(variantSet.Mode.Fields, f.Rules...)
			for _, deck := range match.LoadDecks() {
				if err := deck.Validate(&variantSet); err != nil {
					continue
				}
				b, err := deck.MarshalText()
				if err != nil {
					continue
				}
				prefix := "cm-" + string(b)
				opt := createEl("option")
				opt.setValue(prefix)
				npcAutocomplete.appendChild(opt)

				prefix += "-"
				for _, c := range room.PlayerCharacters {
					if c.Hidden {
						continue
					}
					opt = createEl("option")
					opt.setValue(prefix + c.Name)
					npcAutocomplete.appendChild(opt)
				}
			}
			npcInput.Call("setAttribute", "list", "variant-npc-suggest")
			npcAutocomplete.Set("id", "variant-npc-suggest")
			modal.appendChild(npcAutocomplete)
			rulesHeading := createEl("h1")
			rulesHeading.setText("Additional Rules")
			modal.appendChild(rulesHeading)
			ul, createItem := e.variantRules.setup(e, func(li element) {
				i := defaultIndexOf(li)
				go e.setFieldEditor(f.Rules[i], f)
			}, defaultIndexOf, func(dst, src int, _, _ element, after bool) bool {
				r := f.Rules[src]
				if after {
					copy(f.Rules[dst+1:], f.Rules[dst:src])
				} else {
					copy(f.Rules[src:], f.Rules[src+1:dst+1])
				}
				f.Rules[dst] = r

				return true
			})
			modal.appendChild(ul)
			for _, r := range f.Rules {
				e.fieldName(r, createItem(true))
			}
			addRuleButton := createEl("button")
			addRuleButton.setText("Add Rule")
			onAddRule := js.FuncOf(func(_ js.Value, args []js.Value) interface{} {
				args[0].Call("preventDefault")
				go e.addFieldModal(f)

				return js.Undefined()
			})
			defer onAddRule.Release()
			addRuleButton.addEventListener("click", onAddRule)
			modal.appendChild(addRuleButton)
		case *card.UnfilterCard:
			p1 := createEl("p")
			p1.setText("Ignore Card in Generic Filters: " + e.set.Card(f.ID).DisplayName())
			modal.appendChild(p1)
			p2 := createEl("p", "subtle")
			p2.setText("This field can be edited in the restrictions section of the card editor.")
			modal.appendChild(p2)
		case *card.DeckLimitFilter:
			editNumber("Maximum Count", &f.Count, ^uint64(0))
			filterLabel := createEl("label", "button")
			filterLabel.setText("Filter")
			filterDesc := createEl("p", "subtle")
			filterDesc.appendChild(e.describeFilter(f.Filter))
			filterLabel.appendChild(filterDesc)
			filterButton := createEl("button")
			filterButton.setText("Change Filter")
			changeFilterClick := js.FuncOf(func(_ js.Value, args []js.Value) interface{} {
				args[0].Call("preventDefault")
				go func() {
					e.editFilterModal(&f.Filter, false)
					filterDesc.clear()
					filterDesc.appendChild(e.describeFilter(f.Filter))
				}()

				return js.Undefined()
			})
			defer changeFilterClick.Release()
			filterButton.addEventListener("click", changeFilterClick)
			filterLabel.appendChild(filterButton)
			modal.appendChild(filterLabel)
			editNumber("Requirement Count", &f.CondCount, 0)
			conditionLabel := createEl("label", "button")
			conditionLabel.setText("Requirement Filter")
			conditionDesc := createEl("p", "subtle")
			conditionDesc.appendChild(e.describeFilter(f.Condition))
			conditionLabel.appendChild(conditionDesc)
			conditionButton := createEl("button")
			conditionButton.setText("Change Filter")
			changeConditionClick := js.FuncOf(func(_ js.Value, args []js.Value) interface{} {
				args[0].Call("preventDefault")
				go func() {
					e.editFilterModal(&f.Condition, false)
					conditionDesc.clear()
					conditionDesc.appendChild(e.describeFilter(f.Condition))
				}()

				return js.Undefined()
			})
			defer changeConditionClick.Release()
			conditionButton.addEventListener("click", changeConditionClick)
			conditionLabel.appendChild(conditionButton)
			modal.appendChild(conditionLabel)
		case *card.Timer:
			grid := createEl("div", "grid")
			modal.appendChild(grid)
			startTime := editNumber("Initial Reserve Time", &f.StartTime, 0)
			startTime.Set("min", 1)
			grid.appendChild(element{startTime.Get("parentNode")})
			maxTime := editNumber("Maximum Reserve Time", &f.MaxTime, 0)
			maxTime.Set("min", 1)
			grid.appendChild(element{maxTime.Get("parentNode")})
			perTurn := editNumber("Add Time Per Turn", &f.PerTurn, 0)
			perTurn.Set("min", 1)
			grid.appendChild(element{perTurn.Get("parentNode")})
			maxPerTurn := editNumber("Maximum Time Per Turn", &f.MaxPerTurn, 0)
			maxPerTurn.Set("min", 1)
			grid.appendChild(element{maxPerTurn.Get("parentNode")})
		case *card.Turn0Effect:
			e.editEffectModal(&f.Effect, nil, nil)
			forceDelete = true
			done <- f.Effect == nil
		case *card.VanillaVersion:
			label := createEl("label")
			label.setText("Bug Fables Version")
			sel := createEl("select")
			onChange := js.FuncOf(func(_ js.Value, _ []js.Value) interface{} {
				f.Version = card.BugFablesVersion(window.Call("parseInt", sel.Get("value"), 10).Int())

				return js.Undefined()
			})
			defer onChange.Release()
			sel.addEventListener("change", onChange)
			for i := card.BugFablesVersion(0); i <= card.LatestVersion; i++ {
				sel.appendChild(createOption(i.String(), strconv.Itoa(int(i))))
			}
			sel.setValue(strconv.Itoa(int(f.Version)))
			label.appendChild(sel)
			modal.appendChild(label)
		case *card.Group:
			editString("Group Name", &f.Title, sprites.FontD3Streetism)
			ul := createEl("ul", "multicol")
			modal.appendChild(ul)
			for _, id := range f.Cards {
				if c := e.set.Card(id); c != nil {
					li := createEl("li")
					li.appendChild(e.makePortrait(c.Portrait, c.CustomPortrait))
					li.Call("appendChild", document.Call("createTextNode", " "+c.DisplayName()))
					ul.appendChild(li)
				}
			}
			hint := createEl("p", "subtle")
			hint.setText("To add or remove cards for this group, drag them in the card list.")
			modal.appendChild(hint)
		case *card.SpecialFlags:
			var parentSpecial map[card.SpecialFlag]bool
			if variant != nil {
				sf, ok := e.set.Mode.Get(card.FieldSpecialFlags).(*card.SpecialFlags)
				if ok {
					parentSpecial = sf.Set
				}
			}

			onFlagChanged := js.FuncOf(func(this js.Value, _ []js.Value) interface{} {
				flagID, err := strconv.ParseUint(this.Get("value").String(), 10, 64)
				if err != nil {
					return js.Undefined()
				}

				if this.Get("checked").Bool() {
					if f.Set == nil {
						f.Set = make(map[card.SpecialFlag]bool)
					}

					f.Set[card.SpecialFlag(flagID)] = true
				} else {
					delete(f.Set, card.SpecialFlag(flagID))
				}

				return js.Undefined()
			})
			defer onFlagChanged.Release()

			for flag := card.SpecialFlag(1); flag < card.SpecialFlagCount; flag++ {
				label := createEl("label")
				label.setText(" " + flag.String())
				checkbox := createInput("checkbox")
				checkbox.setValue(strconv.FormatUint(uint64(flag), 10))
				checkbox.Set("checked", f.Set[flag] || parentSpecial[flag])
				checkbox.Set("disabled", parentSpecial[flag])
				checkbox.addEventListener("input", onFlagChanged)
				label.Call("insertBefore", checkbox.Value, label.Get("firstChild"))
				modal.appendChild(label)
			}
		default:
			panic("Unhandled field editor: " + f.Type().String())
		}

		modal.appendChild(createEl("br"))

		deleteButton := createEl("button", "delete")
		doneButton := createEl("button")
		if variant == nil {
			deleteButton.setText("Delete Field")
		} else {
			deleteButton.setText("Delete Rule")
		}
		doneButton.setText("Close")
		doneClick := js.FuncOf(func(this js.Value, args []js.Value) interface{} {
			args[0].Call("preventDefault")
			select {
			case done <- deleteButton.Equal(this):
			default:
			}

			return js.Undefined()
		})
		defer doneClick.Release()
		deleteButton.addEventListener("click", doneClick)
		doneButton.addEventListener("click", doneClick)
		modal.appendChild(deleteButton)
		modal.appendChild(doneButton)

		if <-done && (forceDelete || window.Call("confirm", "Are you sure you want to delete this field? '"+field.Type().String()+"'").Bool()) {
			if variant != nil {
				for i, f := range variant.Rules {
					if f == field {
						variant.Rules = append(variant.Rules[:i], variant.Rules[i+1:]...)

						break
					}
				}
			} else {
				for i, f := range e.set.Mode.Fields {
					if f == field {
						e.set.Mode.Fields = append(e.set.Mode.Fields[:i], e.set.Mode.Fields[i+1:]...)

						break
					}
				}
			}
		}
		e.encodeCardCodes()
		e.setModeEditor()
	})
}

func (e *editor) addFieldModal(variant *card.Variant) {
	e.modal(func(modal element) {
		ch := make(chan card.Field)

		option := func(name, description string, create func() card.Field) js.Func {
			f := js.FuncOf(func(_ js.Value, args []js.Value) interface{} {
				args[0].Call("preventDefault")
				select {
				case ch <- create():
				default:
				}

				return js.Undefined()
			})
			button := createEl("button")
			button.Set("tabIndex", 0)
			button.setText(name)
			button.addEventListener("click", f)
			modal.appendChild(button)
			desc := createEl("p", "subtle")
			desc.setText(description)
			modal.appendChild(desc)

			return f
		}
		optionUnique := func(name, description string, fieldType card.FieldType) js.Func {
			if e.set.Mode.Get(fieldType) != nil {
				return js.Func{}
			}
			if variant == nil {
				for _, f := range e.set.Mode.GetAll(card.FieldVariant) {
					for _, r := range f.(*card.Variant).Rules {
						if r.Type() == fieldType {
							return js.Func{}
						}
					}
				}
			} else {
				for _, r := range variant.Rules {
					if r.Type() == fieldType {
						return js.Func{}
					}
				}
			}

			return option(name, description, func() card.Field {
				field, _ := card.NewField(fieldType)

				return field
			})
		}
		haveVanillaBan := func(flags card.BannedCardsFlags) bool {
			for _, f := range e.set.Mode.GetAll(card.FieldBannedCards) {
				bc := f.(*card.BannedCards)
				if bc.Flags == flags && len(bc.Cards) == 0 {
					return true
				}
			}

			if variant != nil {
				for _, r := range variant.Rules {
					if bc, ok := r.(*card.BannedCards); ok && bc.Flags == flags && len(bc.Cards) == 0 {
						return true
					}
				}
			}

			return false
		}

		prompt := createEl("h1")
		prompt.setText("Select a field type.")
		modal.appendChild(prompt)

		outerName := "game mode"
		if variant != nil {
			outerName = "variant"
		}

		defer option("Metadata", "Add an additional section to the "+outerName+"'s description.", func() card.Field {
			return &card.Metadata{
				Portrait: card.PortraitCustomEmbedded,
			}
		}).Release()
		if !haveVanillaBan(card.BannedCardTypeBanned) {
			defer option("Ban All Vanilla Cards", "Prevent cards from Bug Fables from appearing in a match unless directly summoned by an effect.", func() card.Field {
				return &card.BannedCards{
					Flags: card.BannedCardTypeBanned,
				}
			}).Release()
		}
		if !haveVanillaBan(card.BannedCardTypeUnpickable) {
			defer option("Make All Vanilla Cards Unpickable", "Prevent players from picking cards from Bug Fables in their deck.", func() card.Field {
				return &card.BannedCards{
					Flags: card.BannedCardTypeUnpickable,
				}
			}).Release()
		}
		if !haveVanillaBan(card.BannedCardTypeHiddenHome) {
			defer option("Hide All Vanilla Cards", "Hide cards from Bug Fables on the home screen. Ban All Vanilla Cards implies this option.", func() card.Field {
				return &card.BannedCards{
					Flags: card.BannedCardTypeHiddenHome,
				}
			}).Release()
		}
		defer option("Deck Limit", "Limit the amount of a card that can appear in a deck.", func() card.Field {
			return &card.DeckLimitFilter{}
		}).Release()
		defer option("Summon Card", "Summon a card at the start of the first round.", func() card.Field {
			return &card.SummonCard{}
		}).Release()
		defer option("Turn 0 Effect", "Run an effect before the match begins.", func() card.Field {
			return &card.Turn0Effect{}
		}).Release()
		if variant == nil {
			defer option("Variant", "Add additional game mode fields. If there are multiple variants, the host can select one when starting the match. This can also be used to create a singleplayer mode.", func() card.Field {
				return &card.Variant{}
			}).Release()
		}
		defer optionUnique("Game Rules", "Adjust player health, teamwork points, deck size, and hand size.", card.FieldGameRules).Release()
		defer optionUnique("Timer", "Limit the amount of time each player can take per round.", card.FieldTimer).Release()
		defer optionUnique("Vanilla Version", "Change what Bug Fables version this game mode is based on.", card.FieldVanillaVersion).Release()
		if variant == nil {
			defer option("Card Group", "A named group of cards.", func() card.Field {
				return &card.Group{}
			}).Release()
		}

		haveSpecialFlags := false
		if variant == nil {
			haveSpecialFlags = e.set.Mode.Get(card.FieldSpecialFlags) != nil
		} else {
			for _, r := range variant.Rules {
				if r.Type() == card.FieldSpecialFlags {
					haveSpecialFlags = true

					break
				}
			}
		}

		if !haveSpecialFlags {
			defer option("Special Flags", "Change advanced properties of the game.", func() card.Field {
				return &card.SpecialFlags{}
			}).Release()
		}

		field := <-ch
		if field != nil {
			if variant == nil {
				e.set.Mode.Fields = append(e.set.Mode.Fields, field)
			} else {
				variant.Rules = append(variant.Rules, field)
			}
			e.encodeCardCodes()
			go e.setFieldEditor(field, variant)
		}
	})
}
