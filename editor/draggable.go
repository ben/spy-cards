//go:build js && wasm
// +build js,wasm

package editor

import "syscall/js"

type draggable struct {
	el        js.Value
	click     js.Func
	dragStart js.Func
	dragOver  js.Func
	dragEnd   js.Func
}

func (d *draggable) setup(e *editor, click func(element), indexOf func(element) int, move func(int, int, element, element, bool) bool) (element, func(bool) element) {
	ul := createEl("ul", "drag-list")

	d.click.Release()
	d.dragStart.Release()
	d.dragOver.Release()
	d.dragEnd.Release()

	d.click = js.FuncOf(func(this js.Value, args []js.Value) interface{} {
		args[0].Call("stopPropagation")
		go click(element{this})

		return js.Undefined()
	})
	d.dragStart = js.FuncOf(func(this js.Value, args []js.Value) interface{} {
		args[0].Call("stopPropagation")
		args[0].Get("dataTransfer").Set("effectAllowed", "move")
		args[0].Get("dataTransfer").Call("setData", "text/plain", js.Null())
		d.el = this
		oldTop := this.Get("offsetTop").Float()
		ul.addClass("in-drag")
		if this.Get("classList").Call("contains", "group").Bool() {
			ul.addClass("dragging-group")
		}
		this.Get("classList").Call("add", "dragging")
		newTop := this.Get("offsetTop").Float()

		scrollContainer := this
		for scrollContainer.Truthy() && scrollContainer.Get("scrollHeight").Equal(scrollContainer.Get("clientHeight")) {
			scrollContainer = scrollContainer.Get("parentNode")
		}

		if scrollContainer.Truthy() {
			scrollContainer.Set("scrollTop", scrollContainer.Get("scrollTop").Float()-oldTop+newTop)
		}

		return js.Undefined()
	})
	d.dragOver = js.FuncOf(func(this js.Value, args []js.Value) interface{} {
		args[0].Call("stopPropagation")
		args[0].Call("preventDefault")
		drag := indexOf(element{d.el})
		over := indexOf(element{this})
		target := this

		if drag == -1 || over == -1 {
			return js.Undefined()
		}

		ok := false
		if drag < over {
			target = target.Get("nextSibling")
			ok = move(over, drag, element{this}, element{d.el}, false)
		} else if drag > over {
			ok = move(over, drag, element{this}, element{d.el}, true)
		}

		if ok {
			this.Get("parentNode").Call("insertBefore", d.el, target)
		}

		return js.Undefined()
	})
	d.dragEnd = js.FuncOf(func(_ js.Value, args []js.Value) interface{} {
		args[0].Call("stopPropagation")

		var oldTop, newTop float64

		if d.el.Truthy() {
			oldTop = d.el.Get("offsetTop").Float()
			d.el.Get("classList").Call("remove", "dragging")
		}
		ul.removeClass("in-drag")
		ul.removeClass("dragging-group")
		if d.el.Truthy() {
			newTop = d.el.Get("offsetTop").Float()

			scrollContainer := d.el
			for scrollContainer.Truthy() && scrollContainer.Get("scrollHeight").Equal(scrollContainer.Get("clientHeight")) {
				scrollContainer = scrollContainer.Get("parentNode")
			}

			if scrollContainer.Truthy() {
				scrollContainer.Set("scrollTop", scrollContainer.Get("scrollTop").Float()-oldTop+newTop)
			}
		}

		d.el = js.Undefined()

		e.encodeCardCodes()

		return js.Undefined()
	})

	return ul, func(canDrag bool) element {
		li := createEl("li")
		li.Call("addEventListener", "click", d.click)

		if canDrag {
			li.Set("draggable", true)
			li.Call("addEventListener", "dragstart", d.dragStart)
			li.Call("addEventListener", "dragover", d.dragOver)
			li.Call("addEventListener", "dragend", d.dragEnd)
		}

		ul.appendChild(li)

		return li
	}
}

func defaultIndexOf(li element) int {
	if !li.Truthy() {
		return -1
	}

	indexOf := js.Global().Get("Array").Get("prototype").Get("indexOf")
	items := li.Get("parentNode").Get("children")

	return indexOf.Call("call", items, li.Value).Int()
}
